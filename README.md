# adpt_core #

This package contains the core tools of the Automatic DFTB Parameterization Toolkit (ADPT).

All libraries and programs of this package are distributed with LGPLv3 license.

## Citations ##
If you use this package for your research, please cite the following papers:

1. Michael Gaus, Chien-Pin Chou, Henryk Witek, Marcus Elstner, Automatized Parametrization of SCC-DFTB Repulsive Potentials : Application to Hydrocarbons, *J. Phys. Chem. A* **113**: 43. 11866-11881. (2009)
2. Chien-Pin Chou, Yoshifumi Nishimura, Chin-Chai Fan, Grzegorz Mazur, Stephan Irle and Henryk A. Witek, Automatized Parameterization of DFTB using Particle Swarm Optimization, *J. Chem. Theory Comput.*, DOI: [10.1021/acs.jctc.5b00673](http://doi.org/10.1021/acs.jctc.5b00673). 


## 1. Requirements for compiling the code ##

1. A C++11 compiler
2. [CMake](https://cmake.org/)
3. [Qt](http://www.qt.io/) 5 library (including source code)
4. libyaml (libyaml-dev)
5. libcurl (libcurl4-openssl-dev)
6. libxml2 (libxml2-dev)

## 2. Obtain the source code ##

```
$ git clone https://bitbucket.org/solccp/adpt_core.git
```

### 2.1 Obtain submodules source code ###
```
$ cd adpt_core
$ git submodule init 
$ git submodule update
```

## 3. Compile the external libraries ##
```
$ cd external_libs
$ ./make-external-libs.sh  #build libvraiant library
```

## 4. Compile adpt_core ##
```
$ mkdir adpt_core_build        #or whatever you name it
$ cd adpt_core_build
$ /path/to/the/qmake PREFIX=/opt /path/to/adpt_core #(change /opt to wherever you like)
$ make
# make install 
```
The programs will be installed into $PREFIX/adpt/bin/