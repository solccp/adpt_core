#ifndef ONECENTERINPUT_H
#define ONECENTERINPUT_H

#include <QObject>
#include <QVector>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QFile>
#include <memory>



namespace SKBuilder
{
    namespace Input
    {
        class OneCenterParameter;
    }


class ConfiningPotential;

class AtomicInfo;
class OneCenterInput
{
public:
    explicit OneCenterInput();
    virtual ~OneCenterInput();
    virtual bool loadTemplateInput(const QString& filename) = 0;
    virtual bool loadTemplateInput(QFile *file) = 0;
    virtual bool initialize(const std::shared_ptr<const AtomicInfo>& atomicinfo) = 0;


    virtual void setParameters(const std::shared_ptr<Input::OneCenterParameter>& para) = 0;
    virtual bool setConfining(int l, const std::shared_ptr<const ConfiningPotential>& conf) = 0;
    virtual void reset() = 0;
    virtual bool getConfining(int l, std::shared_ptr<ConfiningPotential>& conf) const = 0 ;
    virtual QString getImplementationType() const = 0 ;
    virtual QString toString() const = 0 ;

    virtual std::shared_ptr<OneCenterInput> clone() const = 0;


    virtual const QStringList& getAllOrbitalNames() const = 0;
    virtual const QStringList& getValenceOrbitalNames() const = 0;
    virtual double getOrbitalOccupation(const QString& orbital) const = 0;
    virtual const QMap<QString, double>& getAllOrbitalOccupation() const = 0;
    virtual int getAtomicNumber() const = 0;
};
}

#endif // ONECENTERINPUT_H
