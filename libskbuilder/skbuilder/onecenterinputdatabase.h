#ifndef ONECENTERINPUTDATABASE_H
#define ONECENTERINPUTDATABASE_H

#include <atomicproperties.h>

#include <QObject>
#include <QString>
#include <QMap>

#include <memory>

namespace SKBuilder
{


class OneCenterInput;
class OneCenterInputDatabase : public QObject
{
    Q_OBJECT
public:
    explicit OneCenterInputDatabase(QObject *parent = 0);
    virtual ~OneCenterInputDatabase();


    virtual void registerInputsFromFolder(const QString& folder) = 0;
    bool registerInputs(const QString& symbol, std::shared_ptr<OneCenterInput> input);
    bool registerInputs(const ADPT::AtomicProperties* atom, std::shared_ptr<OneCenterInput> input);

    void clear();
    void removeInput(const ADPT::AtomicProperties* atom);
    std::shared_ptr<OneCenterInput> createInput(const ADPT::AtomicProperties* atom);
    std::shared_ptr<OneCenterInput> createInput(const ADPT::AtomicProperties& atom);

    virtual QList<std::shared_ptr<const OneCenterInput>> getAllInputs() const = 0;

    int numOfInputs();
signals:
    void inputAdded(const ADPT::AtomicProperties* atom, std::shared_ptr<const OneCenterInput> input);
    void inputRemoved(const ADPT::AtomicProperties* atom);
public slots:
    
protected:
    virtual bool testInput(const ADPT::AtomicProperties* atom, std::shared_ptr<OneCenterInput> input) = 0;
    QMap<const ADPT::AtomicProperties*, std::shared_ptr<OneCenterInput>> m_inputs;
};

}

#endif // ONECENTERINPUTDATABASE_H
