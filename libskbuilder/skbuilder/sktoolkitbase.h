#ifndef SKTOOLKITBASE_H
#define SKTOOLKITBASE_H

#include <memory>

#include <QString>
#include <QTextStream>
#include <QXmlStreamWriter>
#include <stdexcept>

namespace SKBuilder
{

    class AtomicInfo;
    class TwoCenterResult;

    namespace Input
    {
        class ToolchainInfo;
        class ConfiningInfo;
    }

    class AbstractSKToolkit
    {
    protected:
        QString m_rootPath;
        QTextStream& m_output_stream;
        const Input::ToolchainInfo& m_toolkit_info;
    public:
        AbstractSKToolkit(QTextStream& output_stream, const Input::ToolchainInfo& toolkit_info);

        virtual std::shared_ptr<TwoCenterResult> computeTwoCenterIntegrals(const std::shared_ptr<AtomicInfo>& atomic_info_1,
                                                                           const std::shared_ptr<AtomicInfo>& atomic_info_2,
                                                                           const SKBuilder::Input::ConfiningInfo& confining_info_1,
                                                                           const SKBuilder::Input::ConfiningInfo& confining_info_2) = 0;
        virtual ~AbstractSKToolkit();
//        void writeOnecentBasisDocumentation(const std::shared_ptr<AtomicInfo>& atomic_info);

    public:
        bool setOneConterProgram(const QString& path, bool test = true);
        bool setTwoConterProgram(const QString& path, bool test = true);
        QString rootPath() const;
        void setRootPath(const QString &rootPath);
        virtual QString getOrbitalMetaData(const QString& element, const QString& orbital);
        virtual QStringList getOnecentElements() ;
    protected:
        virtual bool testOnecentProgram(const QString& path) = 0;
        virtual bool testTwocentProgram(const QString& path) = 0;

    protected:
        QString m_onecent_program;
        QString m_twocent_program;
    protected:
        bool m_oc_program_set = false;
        bool m_tc_program_set = false;

    public:
        static std::shared_ptr<AbstractSKToolkit> getInstance(const QString& toolkit_name, QTextStream& output_stream, const Input::ToolchainInfo& toolkit_info);
    };







}
#endif // SKTOOLKITBASE_H
