#ifndef EXCEPTION_H_6f16ec6d_1e2e_473f_8a8e_d9cc72129dd3
#define EXCEPTION_H_6f16ec6d_1e2e_473f_8a8e_d9cc72129dd3

#include <QException>

namespace SKBuilder
{


    class OrbitalNameNotFoundException : public QException
    {
    public:
        void raise() const { throw *this; }
        OrbitalNameNotFoundException *clone() const { return new OrbitalNameNotFoundException(*this); }
    };

}



#endif // EXCEPTION_H
