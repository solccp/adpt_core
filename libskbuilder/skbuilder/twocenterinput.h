#ifndef TWOCENTERINPUT_H
#define TWOCENTERINPUT_H

#include <QObject>
#include <QDir>
#include <memory>

namespace SKBuilder
{

namespace Input
{
    class TwoCenterParameter;
}

class AtomicResult;
class TwoCenterInput
{
public:

    enum class SuperPositionType : int
    {
        PotentialSuperposition = 0x1,
        DensitySuperposition = 0x2
    };

    explicit TwoCenterInput();
    ~TwoCenterInput();

    virtual QPair<bool, QString> createInput(const QDir& root,
                                             std::shared_ptr<const AtomicResult> atomic_result_1,
                                             std::shared_ptr<const AtomicResult> atomic_result_2) const = 0;

    SuperPositionType getSuperPositionType() const;
    void setSuperPositionType(const SuperPositionType &value);

    virtual void setParameter(const std::shared_ptr<Input::TwoCenterParameter>& para) = 0;

    virtual double getStartPoint() const = 0;
    virtual double getInterval() const = 0;
protected:
    SuperPositionType sptype = SuperPositionType::DensitySuperposition;//DensitySuperposition;

};
}

#endif // TWOCENTERINPUT_H
