#ifndef SLATERKOSTERFILECOLLETION_H
#define SLATERKOSTERFILECOLLETION_H

#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QMap>
#include <QPair>
#include <memory>

namespace SKBuilder
{
class SlaterKosterFile;
class SlaterKosterFileColletion
{
public:
    SlaterKosterFileColletion();
    SlaterKosterFileColletion(const SlaterKosterFileColletion &rhs);
    virtual ~SlaterKosterFileColletion();

    void setAtomicInfo(const QString& elem1, const QString& elem2,
                              const QList<QStringList>& elem1_shells, const QList<QStringList>& elem2_shells);

    void setSKFile(int elem1_shell_index,
                   int elem2_shell_index,
                   std::shared_ptr<SlaterKosterFile> skfile);

    QMap<QPair<int, int>, std::shared_ptr<SlaterKosterFile> > getSKFiles() const;

private:

    QString m_elem1;
    QString m_elem2;
    QList<QStringList> m_elem1_shells;
    QList<QStringList> m_elem2_shells;
    QMap<QPair<int, int>, std::shared_ptr<SlaterKosterFile> > m_skfiles;


};


}
#endif // SLATERKOSTERFILECOLLETION_H
