#ifndef ONECENTERRESULT_H
#define ONECENTERRESULT_H

#include <memory>
#include <QPair>
#include <QString>

namespace SKBuilder
{

class OneCenterInput;
class OneCenterResult
{
public:
    OneCenterResult(std::shared_ptr<const OneCenterInput> oc_info);
    virtual ~OneCenterResult();
    virtual QPair<bool, QString> getData(const QString& type, const QString& argument = QString()) const = 0;
protected:
    std::shared_ptr<const OneCenterInput> m_oc_info;
};

}
#endif // ONECENTERRESULT_H
