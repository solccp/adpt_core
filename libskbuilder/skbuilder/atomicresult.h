#ifndef ATOMICRESULT_H
#define ATOMICRESULT_H

#include <QObject>
#include "atomicinfo.h"

#include <memory>

namespace SKBuilder
{

class OneCenterResult;
class AtomicResult
{
    friend class AtomicInfo;
public:
    AtomicResult(const std::shared_ptr<const AtomicInfo>& info);
    ~AtomicResult();

//    void addResult(AtomicInfo::ConfiningPotentialType type, const ConfiningPotential* conf, const OneCenterResult* oc_result, const QStringList& orbitals = QStringList());
//    QPair<bool, QString> getData(const QString& type, const QString& argument = QString()) const;


    std::weak_ptr<const AtomicInfo> getAtomicInfo() const;

    void addOrbitalResult(const QString& orbitalname, const QString& orbitaldata, const QString& orbitalmomentum);
    void addResult(const QString& name, const QString& data);
    QPair<bool, QString> getOrbitalMomentum(const QString &orbitalname) const;
    QPair<bool, QString> getOrbitalData(const QString &orbitalname) const;
    QPair<bool, QString> getData(const QString &name, bool useFreeAtom = true) const;

private:
//   QList<const OneCenterResult*> m_results;
//   QMap<const OneCenterResult*, AtomicInfo::ConfiningPotentialType> m_result_type;
//   QMap<QString, const OneCenterResult*> m_orb_result;

   QMap<QString, QString> m_orb_data;
   QMap<QString, QString> m_orb_momentum;

   QString m_potnc;
   QString m_pottot;

   QString m_dens;

   std::weak_ptr<const AtomicInfo> m_info;
    
};

}
#endif // ATOMICRESULT_H
