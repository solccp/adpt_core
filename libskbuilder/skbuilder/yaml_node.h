#ifndef YAML_NODE_H
#define YAML_NODE_H

#include <yaml-cpp/yaml.h>

namespace YAML
{

    template<typename T>
    struct update_if
    {
        explicit update_if(const Node& node_, T& rhs_): node(node_), rhs(rhs_) {}
        const Node& node;
        T& rhs;

        void operator()() const
        {
            if(!node.IsDefined())
                throw TypedBadConversion<T>();

            if(convert<T>::decode(node, rhs))
                return ;
            throw TypedBadConversion<T>();
        }
    };


    template<typename T>
    void update_node(Node& node, T& rhs)
    {
        return update_if<T>(node, rhs)();
    }


}


#endif // YAML_NODE_H
