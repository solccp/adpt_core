#ifndef TWNINPUT_YAML_H
#define TWNINPUT_YAML_H

#include "TWNinput.h"
#include <yaml-cpp/yaml.h>
#include "yaml_utils.h"
#include "yaml_qttypes.h"
#include <exception>


namespace YAML
{
    template<>
    struct convert<SKBuilder_Input::Confining>
    {
        static Node encode(const SKBuilder_Input::Confining &rhs)
        {
            Node node;
            node["w"] = rhs.w;
            node["a"] = rhs.a;
            node["r"] = rhs.r;
            node["type"] = rhs.type;
            if (rhs.type.toLower() == "orbital")
            {
                node["orbital_types"] = rhs.orbital_types;
            }
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::Confining &rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.w, node, "w", double, hasValue);
            GET_NODE_OPT(rhs.a, node, "a", double, hasValue);
            GET_NODE(rhs.r, node, "r", double);
            GET_NODE(rhs.type, node, "type", QString);
            GET_NODE_OPT(rhs.orbital_types, node, "orbital_types", QStringList, hasValue);
            hasValue = hasValue;
            return true;
        }
    };
    template<>
    struct convert<SKBuilder_Input::OneCenterParameter>
    {
        static Node encode(const SKBuilder_Input::OneCenterParameter &rhs)
        {
            Node node;
            node["ngrid"] = rhs.ngrid;
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::OneCenterParameter &rhs)
        {
            GET_NODE(rhs.ngrid, node, "ngrid", int);
            return true;
        }
    };

    template<>
    struct convert<SKBuilder_Input::ProgramParameters>
    {
        static Node encode(const SKBuilder_Input::ProgramParameters &rhs)
        {
            Node node;
            Node onecent_specfic_node;

            QMapIterator<QString, SKBuilder_Input::OneCenterParameter> it_oc(rhs.onecent_specific);
            while (it_oc.hasNext())
            {
                it_oc.next();
                onecent_specfic_node[it_oc.key()] = it_oc.value();
            }
            node["onecenter"]["specific"] = onecent_specfic_node;
            node["onecenter"]["default"] = rhs.onecent_default;

            Node twocent_specfic_node;

            QMapIterator<QString, SKBuilder_Input::TwoCenterParameter> it_tc(rhs.twocent_specific);
            while (it_tc.hasNext())
            {
                it_tc.next();
                twocent_specfic_node = it_tc.value();
                twocent_specfic_node["potential"] = it_tc.key();
            }
            node["twocenter"]["specific"] = twocent_specfic_node;
            node["twocenter"]["default"] = rhs.twocent_default;

            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::ProgramParameters &rhs)
        {
            if (node.IsDefined() && node["onecenter"].IsDefined())
            {
                if (node["onecenter"]["specific"].IsDefined() && node["onecenter"]["specific"].IsSequence())
                {
                    for (std::size_t i = 0; i < node["onecenter"]["specific"].size(); ++i)
                    {
                        if (node["onecenter"]["specific"][i]["element"].IsDefined())
                        {
                            QString elem = node["onecenter"]["specific"][i]["element"].as<QString>();
                            SKBuilder_Input::OneCenterParameter para = node["onecenter"]["specific"][i].as<SKBuilder_Input::OneCenterParameter>();
                            rhs.onecent_specific.insert(elem, para);
                        }
                    }
                }
                if (node["onecenter"]["default"].IsDefined())
                {
                    GET_NODE(rhs.onecent_default, node["onecenter"], "default", SKBuilder_Input::OneCenterParameter);
                }
            }

            if (node.IsDefined() && node["twocenter"].IsDefined())
            {
                if (node["twocenter"]["specific"].IsDefined() && node["twocenter"]["specific"].IsSequence())
                {
                    for (std::size_t i = 0; i < node["twocenter"]["specific"].size(); ++i)
                    {
                        if (node["twocenter"]["specific"][i]["potential"].IsDefined())
                        {
                            QString poten = node["twocenter"]["specific"][i]["potential"].as<QString>();
                            SKBuilder_Input::TwoCenterParameter para = node["twocenter"]["specific"][i].as<SKBuilder_Input::TwoCenterParameter>();
                            rhs.twocent_specific.insert(poten, para);
                        }
                    }
                }
                if (node["twocenter"]["default"].IsDefined())
                {
                    GET_NODE(rhs.twocent_default, node["twocenter"], "default", SKBuilder_Input::TwoCenterParameter);
                }
            }
            return true;
        }
    };

    template<>
    struct convert<SKBuilder_Input::ConfiningInfo>
    {
        static Node encode(const SKBuilder_Input::ConfiningInfo &rhs)
        {
            Node node;
            node["confining"] = rhs.confining;
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::ConfiningInfo &rhs)
        {
            GET_NODE(rhs.confining, node, "confining", QList<SKBuilder_Input::Confining>);
            return true;
        }
    };

    template<>
    struct convert<SKBuilder_Input::AtomicInfo>
    {
        static Node encode(const SKBuilder_Input::AtomicInfo &rhs)
        {
            Node node;
            if (!rhs.onecenter_input.isEmpty())
                node["onecenter_input"] = rhs.onecenter_input;


            for (int i = 0; i < rhs.orbitals.size(); ++i)
            {
                Node subnode;
                subnode = rhs.orbitals.at(i);
                subnode.SetStyle(YAML::FlowStyle);
                node["orbitals"].push_back(subnode);
            }



            node["hubbard"] = rhs.hubbard;
            node["orbital_energy"] = rhs.orbitalEnergy;
            node["occupation"] = rhs.occupation;
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::AtomicInfo &rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.onecenter_input, node, "onecenter_input", QString, hasValue);
            GET_NODE(rhs.orbitals, node, "orbitals", QList<QStringList>);
            typedef QMap<QString, double> map_hubb;
            GET_NODE(rhs.hubbard, node, "hubbard", map_hubb);
            GET_NODE(rhs.orbitalEnergy, node, "orbital_energy", map_hubb);
            GET_NODE(rhs.occupation, node, "occupation", map_hubb);
            return true;
        }
    };

    template<>
    struct convert<SKBuilder_Input::TwoCenterParameter>
    {
        static Node encode(const SKBuilder_Input::TwoCenterParameter &rhs)
        {
            Node node;
            QMapIterator<QString, QVariant> it(rhs.parameters);
            while (it.hasNext())
            {
                it.next();
                node[it.key()] = it.value().toString();
            }
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::TwoCenterParameter &rhs)
        {
            if (node.IsMap())
            {
                for (YAML::const_iterator it = node.begin(); it != node.end(); ++it)
                {
                    QString k1 = it->first.as<QString>();
                    if (k1 == "potential")
                        continue;
                    QString k2 = it->second.as<QString>();
                    rhs.parameters.insert(k1, k2);
                }
                return true;
            }
            return false;
        }
    };

    template<>
    struct convert<SKBuilder_Input::SKBuilderInput>
    {
        static Node encode(const SKBuilder_Input::SKBuilderInput &rhs)
        {
            Node node;
            if (rhs.isLoaded)
            {
                node["all_pairs"] = rhs.all_pairs;
                if (!rhs.all_pairs)
                {
                    node["desired_pairs"] = rhs.desired_pairs;
                }
                node["options"] = rhs.options;
                node["atomic_info"] = rhs.atomicinfo;
                node["confining_info"] = rhs.confininginfo;
                node["program_parameters"] = rhs.program_parameters;
                node["external_repulsive_potentials"] = rhs.external_repulsive_potentials;
            }
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::SKBuilderInput &rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.all_pairs, node, "all_pairs", bool, hasValue);
            if (!rhs.all_pairs)
            {
                typedef QList<QPair<QString, QString> > tmptype;
                tmptype tmp;

                GET_NODE_OPT(tmp, node, "desired_pairs", tmptype, hasValue);
                if (hasValue)
                {
                    for(int i=0; i<tmp.size();++i)
                    {
                        rhs.desired_pairs.append(QString("%1-%2").arg(tmp[i].first).arg(tmp[i].second));
                    }
                }
            }

            GET_NODE(rhs.options, node, "options", SKBuilder_Input::Option);
            typedef QMap<QString, SKBuilder_Input::AtomicInfo> onecentmap;
            typedef QMap<QString, SKBuilder_Input::ConfiningInfo> confingmap;
            GET_NODE(rhs.atomicinfo, node, "atomic_info", onecentmap);
            GET_NODE_OPT(rhs.confininginfo, node, "confining_info", confingmap, hasValue);
            GET_NODE_OPT(rhs.program_parameters, node, "program_parameters", SKBuilder_Input::ProgramParameters, hasValue);

            typedef QMap<QString, QString> mapss;
            GET_NODE_OPT(rhs.external_repulsive_potentials, node, "external_repulsive_potentials", mapss, hasValue);
            Q_UNUSED(hasValue);

            rhs.isLoaded = true;
            return true;
        }
    };
    template<>
    struct convert<SKBuilder_Input::Option>
    {
        static Node encode(const SKBuilder_Input::Option &rhs)
        {
            Node node;
            node["program_paths"] = rhs.program;
//            node["superposition"] = rhs.superpositionType;
            node["onecenter_onerun"] = rhs.onecenterOnerun;
            node["output_prefix"] = rhs.outputPrefix;
            node["save_intermediate"] = rhs.saveIntermediate;
            node["use_freeatom_in_onerun"] = rhs.useFreeAtomInOnerun;
            node["use_density_coulomb"] = rhs.useDensityCoulombPotential;
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::Option &rhs)
        {
            GET_NODE(rhs.program, node, "program_paths", SKBuilder_Input::Program);
            bool hasValue;
//            GET_NODE(rhs.superpositionType, node, "superposition", QString);
            GET_NODE_OPT(rhs.outputPrefix, node, "output_prefix", QString, hasValue);
            GET_NODE_OPT(rhs.saveIntermediate, node, "save_intermediate", bool, hasValue);
            GET_NODE_OPT(rhs.onecenterOnerun, node, "onecenter_onerun", bool, hasValue);
            GET_NODE_OPT(rhs.useFreeAtomInOnerun, node, "use_freeatom_in_onerun", bool, hasValue);
            GET_NODE_OPT(rhs.useDensityCoulombPotential, node, "use_density_coulomb", bool, hasValue);

            Q_UNUSED(hasValue);
            return true;
        }
    };
    template<>
    struct convert<SKBuilder_Input::Program>
    {
        static Node encode(const SKBuilder_Input::Program &rhs)
        {
            Node node;
            node["onecenter"] = rhs.onecenter;
            node["twocenter"] = rhs.twocenter;
            return node;
        }
        static bool decode(const Node &node, SKBuilder_Input::Program &rhs)
        {
            GET_NODE(rhs.onecenter, node, "onecenter", QString);
            GET_NODE(rhs.twocenter, node, "twocenter", QString);
            return true;
        }
    };
}




#endif // TWNINPUT_YAML_H
