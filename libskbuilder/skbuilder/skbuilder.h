#ifndef SKBUILDER_H
#define SKBUILDER_H

#include <QObject>
#include <QDir>
#include "twocenterinput.h"
#include "atomicproperties.h"
#include "atomicinfo.h"

#include "SlaterKosterFileColletion.h"

#include <QTextStream>
#include <QXmlStreamWriter>

#include <stdexcept>

namespace SKBuilder
{
namespace Input
{
    class SKBuilderInput;
    class ConfiningInfo;
}


class AbstractSKToolkit;
class SlaterKosterFile;

class MissingConfiningPotentialError : public std::runtime_error {
public:
    MissingConfiningPotentialError(const std::string& element ) : std::runtime_error("Confining potential for element " + element + " is missing.") {}
};

class OnecenterCodeNotWorkingError : public std::runtime_error {
public:
    OnecenterCodeNotWorkingError() : std::runtime_error("onecent code is not compatible.") {}
};

class TwocenterCodeNotWorkingError : public std::runtime_error {
public:
    TwocenterCodeNotWorkingError() : std::runtime_error("onecent code is not compatible.") {}
};


class BuilderInitializationError : public std::runtime_error {
public:
    BuilderInitializationError() : std::runtime_error("SKBuilder initialization error.") {}
};

class BuilderNotInitializedError : public std::runtime_error {
public:
    BuilderNotInitializedError() : std::runtime_error("SKBuilder not initialized.") {}
};


class SKToolkitNotImplementedError :  public std::runtime_error
{
public:
    SKToolkitNotImplementedError(const std::string& toolkit_name) : std::runtime_error(toolkit_name){}
};

class Builder
{
public:
    Builder(const Input::SKBuilderInput& input_, QTextStream& output_stream);
    Builder() = delete;
    Builder(const Builder&other) = delete;
    Builder& operator =(const Builder& other) = delete;

    QDir getRootDir() const;
    void setRootDir(const QDir &value);

    QDir getOutputDir() const;
    void setOutputDir(const QDir &value);

    QMap<QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *>, SlaterKosterFileColletion > getSKFiles() const;

    virtual ~Builder();

public:
    void init(bool testPrograms = true);

    void build();
    void write();

    void init_confining_info();
    void init_toolkit(bool testPrograms);
    void init_atomic_info();
    void init_ext_reps();

    bool initialized() const;

public:
    QStringList getMissingConfiningInfos() const;
    void removeFiles();


private:
    void clean();
    void init_root_dir();


    const Input::SKBuilderInput& input;
    QTextStream& m_output_stream;

    QDir rootDir;
    QDir outputDir;
    QDir initialDir;

    QMap<const ADPT::AtomicProperties*, std::shared_ptr<AtomicInfo> > all_atomic_infos;
    QList<QPair<const ADPT::AtomicProperties*, const ADPT::AtomicProperties*> > all_pairs;
    QMap<QPair<const ADPT::AtomicProperties*, const ADPT::AtomicProperties*>, QString > externalRepulsivePotentials;
    QMap<QString, Input::ConfiningInfo> all_confininginfo;


private:
    //Outputs
    QMap<QPair<const ADPT::AtomicProperties*, const ADPT::AtomicProperties*>, SlaterKosterFileColletion > m_skfiles;

private:
    QString getAbsolutePath(const QString& filepath);
    std::shared_ptr<AbstractSKToolkit> sktoolkit = nullptr;
    QString getXMLDocumentation(std::shared_ptr<AbstractSKToolkit> sktoolkit,
                                std::shared_ptr<AtomicInfo> atomic_info_1,
                                std::shared_ptr<AtomicInfo> atomic_info_2,
                                int shell1, int shell2,
                                Input::ConfiningInfo conf_info_1, Input::ConfiningInfo conf_info_2);

    bool built = false;

    bool toolkit_initialized = false;
    bool atomic_info_initialized = false;
    bool confining_info_initialized = false;
    bool ext_rep_initialized = false;

    void copyPath(const QString& src, const QString& dst);
    QString parseRepulsivePotential(const QString& filename);
    void writeWFCFile(const QString& filename);

    void printAtomicDoc(QXmlStreamWriter& stream, QString label, QStringList m_atom1_orbitals, Input::ConfiningInfo conf_info_1,
                        const ADPT::AtomicProperties* atom_1, std::shared_ptr<AbstractSKToolkit> sktoolkit);
};

}

#endif // SKBUILDER_H
