#ifndef YAML_QTTYPES_H
#define YAML_QTTYPES_H
#include <QList>
#include <yaml-cpp/yaml.h>
#include <QString>
#include <QPair>

#include "yaml_utils.h"
#include <QMap>
#include <QMapIterator>
#include <QStringList>
#include <QVector>

namespace YAML
{
    template<>
    struct convert<QString>
    {
        static Node encode(const QString& rhs)
        {
            Node node;
            node = rhs.toStdString();
            return node;
        }
        static bool decode(const Node& node, QString& rhs)
        {
            rhs = QString::fromStdString(node.as<std::string>());
            return true;
        }
    };

    template<>
    template<typename T, typename V>
    struct convert<QMap<T, V> >
    {
        static Node encode(const QMap<T, V>& rhs)
        {
            Node node;
            QMapIterator<T, V> it(rhs);
            while(it.hasNext())
            {
                it.next();
                node[it.key()] = it.value();
            }
            return node;
        }
        static bool decode(const Node& node, QMap<T, V>& rhs)
        {
            if (!node.IsDefined() || !node.IsMap())
            {
                return false;
            }
            for(YAML::const_iterator it=node.begin();it!=node.end();++it)
            {
                rhs.insert(it->first.as<T>(), it->second.as<V>());
            }
            return true;
        }
    };


    template<>
    template<typename T>
    struct convert<QList<T> >
    {
        static Node encode(const QList<T>& rhs)
        {
            Node node;
            QListIterator<T> it(rhs);
            while(it.hasNext())
            {
                node.push_back(it.next());
            }
            return node;
        }
        static bool decode(const Node& node, QList<T>& rhs)
        {
            if (!node.IsDefined() || !node.IsSequence())
            {
                throw std::exception();
            }
            for(std::size_t i=0; i<node.size();++i)
            {
                T tmp = node[i].as<T>();
                rhs.append(tmp);
            }
            return true;
        }
    };

    template<>
    struct convert<QStringList>
    {
        static Node encode(const QStringList& rhs)
        {
            Node node;
            QStringListIterator it(rhs);
            while(it.hasNext())
            {
                node.push_back(it.next());
            }
            return node;
        }
        static bool decode(const Node& node, QStringList& rhs)
        {
            if (!node.IsDefined() || !node.IsSequence())
            {
                throw std::exception();
            }
            for(std::size_t i=0; i<node.size();++i)
            {
                QString tmp = node[i].as<QString>();
                rhs.append(tmp);
            }
            return true;
        }
    };

    template<>
    template<typename T, typename N>
    struct convert<QPair<T, N> >
    {
        static Node encode(const QPair<T, N>& rhs)
        {
            Node node;
            node.push_back(rhs.first);
            node.push_back(rhs.second);
            return node;
        }
        static bool decode(const Node& node, QPair<T, N>& rhs)
        {
            if (!node.IsDefined() || !node.IsSequence() || node.size() < 2)
            {
                return false;
            }
            rhs.first  = node[0].as<T>();
            rhs.second = node[1].as<N>();
            return true;
        }
    };

    template<>
    template<typename T>
    struct convert<QVector<T> >
    {
        static Node encode(const QVector<T>& rhs)
        {
            Node node;
            QVectorIterator<T> it(rhs);
            while(it.hasNext())
            {
                node.push_back(it.next());
            }
            return node;
        }
        static bool decode(const Node& node, QVector<T>& rhs)
        {
            for(std::size_t i=0; i<node.size();++i)
            {
                T tmp = node[i].as<T>();
                rhs.append(tmp);
            }
            return true;
        }
    };

}

#endif // YAML_QTTYPES_H
