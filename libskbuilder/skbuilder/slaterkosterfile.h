#ifndef SLATERKOSTERFILE_H
#define SLATERKOSTERFILE_H

#include <QString>
#include <QMap>
#include <QDir>

#include <memory>

namespace SKBuilder
{


struct SlaterKosterIntegralType
{
    static const int NINT = 20;
    enum IntegralType
    {
        ERROR = -1,
        f_f_sigma = 0, f_f_pi = 1, f_f_delta = 2, f_f_phi = 3,
        d_f_sigma = 4, d_f_pi = 5, d_f_delta = 6,
        d_d_sigma = 7, d_d_pi = 8, d_d_delta = 9,
        p_f_sigma =10, p_f_pi =11,
        p_d_sigma =12, p_d_pi =13,
        p_p_sigma =14, p_p_pi =15,
        s_f_sigma =16, s_d_sigma =17, s_p_sigma =18, s_s_sigma =19
    };
    static const QString names[NINT];
    static QString toString(IntegralType type)
    {
        if (type != ERROR)
            return names[type];
        return QString();
    }
    static IntegralType fromString(const QString& type)
    {
        for(int i=0; i<NINT; ++i)
        {
            if (names[i].toLower() == type.toLower())
            {
                return IntegralType(i);
            }
        }
        return IntegralType(-1);
    }
};

class AtomicInfo;
class SlaterKosterFile
{
public:
    enum SuperPositionType
    {
        PotentialSuperposition = 0x1,
        DensitySuperposition = 0x2
    };

    SlaterKosterFile(SuperPositionType type, double start, double interval);
//    bool setAtomicInformation(const AtomicInfo* atomic_info_1, const QStringList& atom1_orbitals,
//                              const AtomicInfo* atomic_info_2, const QStringList& atom2_orbitals);


    bool setAtomicInformation(const std::shared_ptr<const AtomicInfo>& atomic_info_1, int atom1_shell,
                              const std::shared_ptr<const AtomicInfo>& atomic_info_2, int atom2_shell);

    void setDocumentation(const QString& docs);

    bool addIntegral(SlaterKosterIntegralType::IntegralType type, const std::vector<double> &hamiltonianIntegral, const std::vector<double> &overlapIntegral);
    bool writeToFile(const QDir& dir, const QString& filename) const;
    bool writeToFile(const QDir& dir, bool write_superpositiontype = true) const;
    const QString& getLastError();
    QString suffix() const;
    void setSuffix(const QString &suffix);

    QString prefix() const;
    void setPrefix(const QString &prefix);

    QString getFilename(bool write_superpositiontype = true) const;

    QString getRepulsive() const;
    void setRepulsive(const QString &repulsive);


    int atom1_shell() const;
    int atom2_shell() const;

    SuperPositionType getSuperPositionType() const;


    std::weak_ptr<const AtomicInfo> atomic_info_1() const;
    std::weak_ptr<const AtomicInfo> atomic_info_2() const;

    QStringList atom1_orbitals() const;
    QStringList atom2_orbitals() const;

    QString atom1_selectedShell() const;
    QString atom2_selectedShell() const;



    ~SlaterKosterFile();

    double getAtomEnergy() const;


    QString getDocumentation() const;

private:

    QString m_documentation;

    QString orbitalList2ShellString(const QStringList& orbitals) const;

    QString getSuperpositionString() const;

    SuperPositionType m_superposition;

    QMap<SlaterKosterIntegralType::IntegralType, std::vector<double> > m_hamiltonianIntegrals;
    QMap<SlaterKosterIntegralType::IntegralType, std::vector<double> > m_overlapIntegrals;

    int m_atom1_shell;
    int m_atom2_shell;

    QStringList m_atom1_orbitals;
    QStringList m_atom2_orbitals;
    std::weak_ptr<const AtomicInfo> m_atomic_info_1;
    std::weak_ptr<const AtomicInfo> m_atomic_info_2;
    bool isExtFormat = false;
    QString m_lastError;
    int integral_length = -1;
    double m_start;
    double m_interval;
    QString m_prefix;
    QString m_suffix = ".skf";
    QString m_repulsive;
};

}

#endif // SLATERKOSTERFILE_H
