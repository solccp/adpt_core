#ifndef ATOMICINFO_H
#define ATOMICINFO_H

#include <memory>
#include "atomicproperties.h"

#include <QMap>
#include <QStringList>

namespace SKBuilder
{

    class AtomicInfo
    {
    public:

        AtomicInfo(const ADPT::AtomicProperties* atom);
        virtual ~AtomicInfo();

        bool addShellOrbitals(const QStringList& orbitals);
        void clearShellOrbitals() ;

        int getShellFromOribital(const QString& orbital) const;
        QStringList getShellOrbitals(int shell) const;
        int getNumberOfShells() const;
        QList<QStringList> getShellOrbitals() const;

        bool setHubburd(const QString& orb, double value);
        bool getHubburd(const QString& orb, double *value) const;

        bool setOrbitalEnergy(const QString& orb, double value);
        bool getOrbitalEnergy(const QString& orb, double* energy) const;

        bool setOccupation(const QString& orb, double value);
        bool getOccupation(const QString& orb, double* occ) const;

        bool setFinalOccupation(const QString& orb, double value);
        bool getFinalOccupation(const QString& orb, double* occ) const;

        const ADPT::AtomicProperties *getAtom() const;
        QString getLastError() const;
        QStringList getAllOrbitals() const;

        QString getTemplate_onecenter_inputfile() const;
        void setTemplate_onecenter_inputfile(const QString &value);


        void setOrbitalFiles(const QMap<QString, QString>& orbital_files);
        const QMap<QString, QString> orbitalFiles() const;

        void setPotentialFile(const QString& potential_file);
        QString potentialFile() const;

        void setDensityFile(const QString& density_file);
        QString densityFile() const;

    private:
        QString m_errorMsg;
        const ADPT::AtomicProperties* m_atom;
        QList<QStringList> m_shell_orbitals;
        QMap<QString, double> m_hubburd;
        QMap<QString, double> m_occupation;
        QMap<QString, double> m_final_occupation;
        QMap<QString, double> m_orbitalEnergy;

        QString template_onecenter_inputfile;

        QMap<QString, QString> m_orbital_files;
        QString m_density_file;
        QString m_potential_file;

    };

}
#endif // ATOMICINFO_H
