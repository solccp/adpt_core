#ifndef YAML_UTILS_H
#define YAML_UTILS_H

//#include "yaml-cpp/yaml.h"
#include <iostream>
#define COMMA ,

#define GET_NODE(nodelhs, node, node_name, node_type) \
    try \
    { \
    nodelhs = node[node_name].as<node_type >(); \
    } \
    catch(std::exception &e) \
    { \
    std::cerr << " Error: parsing " << node_name << " failed" << std::endl;  \
    throw e; \
    } ;

#define GET_NODE_IN_VECTOR(nodelhs, node, node_name, index, node_type) \
    try \
    { \
    nodelhs = node[index].as<node_type >(); \
    } \
    catch(std::exception &e) \
    { \
    std::cerr << " Error: parsing " << node_name << "[" << index << "]" << " failed" << std::endl;  \
    throw e; \
    } ;


#define GET_NODE_OPT(nodelhs, node, node_name, node_type, hasValue) \
    { \
        hasValue = false; \
        if (node[node_name].IsDefined()) \
        { \
            try \
            { \
                nodelhs = node[node_name].as<node_type >(); \
                hasValue = true; \
            } \
            catch(std::exception &e) \
            { \
                std::cerr << " Error: parsing " << node_name << " failed" << std::endl;  \
                throw e; \
            } \
        } \
    }

#endif // YAML_UTILS_H
