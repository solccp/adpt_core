#ifndef TWOCENTERRESULT_H
#define TWOCENTERRESULT_H

#include <QHash>
#include "slaterkosterfile.h"
#include <memory>

namespace SKBuilder
{
class TwoCenterInput;
class AtomicResult;
class TwoCenterResult
{
public:
    TwoCenterResult(double starting_point, double interval);
//    TwoCenterResult(std::shared_ptr<const AtomicResult> atomic_result_1,
//                    std::shared_ptr<const AtomicResult> atomic_result_2,
//                    std::shared_ptr<const TwoCenterInput> tc_info);
    virtual ~TwoCenterResult();
    virtual void loadResult(const QString& folder) = 0;

public:
    const QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>>& getHamiltonianIntegral(int shell1, int shell2);
    const QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>>& getOverlapIntegral(int shell1, int shell2);

    double getStartPoint() const;
    double getInterval() const;

protected:
//    std::shared_ptr<const AtomicResult> m_atomic_result_1;
//    std::shared_ptr<const AtomicResult> m_atomic_result_2;
//    std::shared_ptr<const TwoCenterInput> m_tc_info;

    QHash<QPair<int, int>, QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>> > m_overlapIntegral;
    QHash<QPair<int, int>, QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>> > m_HamiltonianIntegral;

protected:
    void addHamiltonianIntegral(int shell1, int shell2, SlaterKosterIntegralType::IntegralType type, const std::vector<double>& integral);
    void addOverlapIntegral(int shell1, int shell2, SlaterKosterIntegralType::IntegralType type, const std::vector<double>& integral);

private:
    void addIntegral(QHash<QPair<int, int>,
                     QHash<SlaterKosterIntegralType::IntegralType, std::vector<double> > > &container,
                     int shell1, int shell2, SlaterKosterIntegralType::IntegralType type,
                     const std::vector<double> &integral);
protected:
    double m_starting_point = 0.4;
    double m_interval = 0.02;

};

}

#endif // TWOCENTERRESULT_H
