#-------------------------------------------------
#
# Project created by QtCreator 2013-08-22T20:48:32
#
#-------------------------------------------------

QT       -= gui

TARGET = skbuilder
TEMPLATE = lib

CONFIG += staticlib
CONFIG += c++11

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
#DEFINES += QT_NO_DEBUG_OUTPUT

GIT_VERSION = $$system(git --work-tree $$PWD describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

SOURCES += \
    src/sktoolkitbase.cpp \
    src/twocenterresult.cpp \
    src/atomicinfo.cpp \
    src/atomicresult.cpp \
    src/slaterkosterfile.cpp \
    src/skbuilder.cpp \
    src/SlaterKosterFileColletion.cpp \
    src/toolchain/nctu/sktoolkit_nctu.cpp \
    src/toolchain/nctu/onecenterresult_nctu.cpp \
    src/toolchain/nctu/onecenterinput_nctu.cpp \
    src/toolchain/nctu/twocenterinput_nctu.cpp \
    src/toolchain/nctu/twocenterresult_nctu.cpp \
    src/toolchain/bccms/sktoolkit_bccms.cpp \
    src/toolchain/bccms/onecenterresult_bccms.cpp \
    src/toolchain/bccms/onecenterinput_bccms.cpp \
    src/toolchain/bccms/twocenterinput_bccms.cpp \
    src/toolchain/bccms/twocenterresult_bccms.cpp \
    src/toolchain/mixed/sktoolkit_mixed.cpp

HEADERS += \
    skbuilder/sktoolkitbase.h \
    skbuilder/twocenterresult.h \
    skbuilder/twocenterinput.h \
    skbuilder/atomicinfo.h \
    skbuilder/atomicresult.h \
    skbuilder/slaterkosterfile.h \
    skbuilder/skbuilder.h \
    skbuilder/exception.h \
    skbuilder/Singleton.h \
    skbuilder/SlaterKosterFileColletion.h \
    src/toolchain/nctu/sktoolkit_nctu.h \
    src/toolchain/nctu/onecenterresult_nctu.h \
    src/toolchain/nctu/onecenterinput_nctu.h \
    src/toolchain/nctu/twocenterinput_nctu.h \
    src/toolchain/nctu/twocenterresult_nctu.h \
    src/toolchain/bccms/sktoolkit_bccms.h \
    src/toolchain/bccms/onecenterresult_bccms.h \
    src/toolchain/bccms/onecenterinput_bccms.h \
    src/toolchain/bccms/twocenterinput_bccms.h \
    src/toolchain/bccms/twocenterresult_bccms.h \
    src/toolchain/mixed/sktoolkit_mixed.h

INCLUDEPATH += skbuilder/

INCLUDEPATH += $$PWD/../external_libs/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/release/ -lSKBuilderCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/debug/ -lSKBuilderCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKBuilderCommon/ -lSKBuilderCommon

INCLUDEPATH += $$PWD/../libSKBuilderCommon
DEPENDPATH += $$PWD/../libSKBuilderCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/libSKBuilderCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/libSKBuilderCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/SKBuilderCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/SKBuilderCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/libSKBuilderCommon.a
