
#include "basic_types.h"
#include "skbuilder.h"
#include "skbuilder/input.h"
#include "sktoolkitbase.h"
#include "skbuilder/utils.h"
#include "toolchain/nctu/sktoolkit_nctu.h"

#include "twocenterresult.h"

#include <QTemporaryDir>
#include <QSet>
#include <array>
#include <QFileInfo>
#include <QXmlStreamWriter>
#include <QDateTime>
#include <QDebug>


namespace SKBuilder
{

inline QString centerString(const QString& str, int width = 80)
{
    int len = str.size();
    int rest = width - len;
    if (rest < 0)
    {
        return str;
    }
    int leftpad = rest / 2;
    int rightpad = rest - leftpad;
    return (QString("").fill(' ', leftpad) + str + QString("").fill(' ', rightpad));
}

const QString HYPHENLINE = QString("").fill('-',80);
const QString EQUALLINE  = QString("").fill('=',80);
const QString STARLINE   = QString("").fill('*',80);
const QString EXCLALINE   = QString("").fill('!',80);
const QString INDENT_SPACES = "    " ;


void print_header_and_version(QTextStream& os)
{
    os << EQUALLINE << endl << endl;
    os << centerString("SKBuilder: Tool for generating DFTB Electronic Slater-Koster (SK) files") << endl;
    os << endl;
    os << centerString("Powered by Chien-Pin Chou") << endl;
    os << endl << centerString(QString(GIT_VERSION))
       << endl << endl << endl;
    os << EQUALLINE << endl << endl;
}

void Builder::init_root_dir()
{
    QTemporaryDir tdir;
    rootDir = QDir(tdir.path());
    tdir.remove();
    rootDir.mkpath(rootDir.absolutePath());


}

QString Builder::getAbsolutePath(const QString &filepath)
{
    auto finfo = QFileInfo(filepath);
    if (finfo.exists())
        return finfo.absoluteFilePath();
    else
        return QString();
}

void Builder::printAtomicDoc(QXmlStreamWriter& stream, QString label, QStringList m_atom1_orbitals,
                             Input::ConfiningInfo conf_info_1, const ADPT::AtomicProperties* atom_1,
                             std::shared_ptr<AbstractSKToolkit> sktoolkit)
{
    stream.writeStartElement("Basis");
    stream.writeAttribute("atom", label);
    stream.writeTextElement("Shells", m_atom1_orbitals.join(" "));
    stream.writeStartElement("Potential");
    for (auto const & act : conf_info_1.confining_actions)
    {
        for (auto const& take : act.take)
        {
            if (take == "potential")
            {
                continue;
            }
            else if (take == "density")
            {
                for (auto const & conf : act.confinings)
                {
                    {
                        auto name = conf.parameters->name();
                        auto var = conf.parameters->toVariant();
                        stream.writeStartElement("Density");
                        stream.writeAttribute("OrbitalType", take);
                        if (name == "bccms")
                        {
                            stream.writeAttribute("Type", "bccms");
                            stream.writeTextElement("n", QString::number(var["n"].AsDouble(), 'E', 8));
                            stream.writeTextElement("r", QString::number(var["r"].AsDouble(), 'E', 8));
                        }
                        else if (name == "nctu")
                        {
                            stream.writeAttribute("Type", "nctu");
                            stream.writeTextElement("w", QString::number(var["w"].AsDouble(), 'E', 8));
                            stream.writeTextElement("a", QString::number(var["a"].AsDouble(), 'E', 8));
                            stream.writeTextElement("r", QString::number(var["r"].AsDouble(), 'E', 8));
                        }
                        stream.writeEndElement();
                    }

                }
            }
            else
            {
                for (auto const & conf : act.confinings)
                {
                    if (conf.orbital_types.contains(take))
                    {
                        auto name = conf.parameters->name();
                        auto var = conf.parameters->toVariant();
                        stream.writeStartElement("Wavefunction");
                        stream.writeAttribute("OrbitalType", take);
                        if (name == "bccms")
                        {
                            stream.writeAttribute("Type", "bccms");
                            stream.writeTextElement("n", QString::number(var["n"].AsDouble(), 'E', 8));
                            stream.writeTextElement("r", QString::number(var["r"].AsDouble(), 'E', 8));
                        }
                        else if (name == "nctu")
                        {
                            stream.writeAttribute("Type", "nctu");
                            stream.writeTextElement("w", QString::number(var["w"].AsDouble(), 'E', 8));
                            stream.writeTextElement("a", QString::number(var["a"].AsDouble(), 'E', 8));
                            stream.writeTextElement("r", QString::number(var["r"].AsDouble(), 'E', 8));
                        }
                        stream.writeEndElement();
                    }

                }
            }

        }

    }
    stream.writeEndElement();
    for (auto const& orb : m_atom1_orbitals)
    {
        auto str = sktoolkit->getOrbitalMetaData(atom_1->getSymbol(), orb);
        QTextStream sin(&str, QIODevice::ReadOnly);
        QStringList exponents;
        QStringList coeffs;
        while (!sin.atEnd())
        {
            QString line = sin.readLine();
            if (line.startsWith("@exponents"))
            {
                QStringList arr = line.split(":", QString::SkipEmptyParts);
                int dim = arr[2].toInt();
                int row = 1;
                int col = arr[3].toInt();

                if (dim > 1)
                {
                    QStringList rc = arr[3].split(",", QString::SkipEmptyParts);
                    row = rc[0].toInt();
                    col = rc[1].toInt();
                }
                int nelem = row*col;
                int rrows = nelem/3;
                if (nelem % 3 > 0)
                {
                    rrows += 1;
                }
                for (int i=0; i<rrows; ++i)
                {
                    line = sin.readLine();
                    arr = line.split(" ", QString::SkipEmptyParts);
                    for(auto const& a : arr)
                    {
                        exponents.append(QString("%1").arg(a.toDouble(),0,'f',12));
                    }
                }
            }
            if (line.startsWith("@coefficients"))
            {
                QStringList arr = line.split(":", QString::SkipEmptyParts);
                int dim = arr[2].toInt();
                int row = 1;
                int col = arr[3].toInt();

                if (dim > 1)
                {
                    QStringList rc = arr[3].split(",", QString::SkipEmptyParts);
                    row = rc[0].toInt();
                    col = rc[1].toInt();
                }
                int nelem = row*col;
                int rrows = nelem/3;
                if (nelem % 3 > 0)
                {
                    rrows += 1;
                }
                for (int i=0; i<rrows; ++i)
                {
                    line = sin.readLine();
                    arr = line.split(" ", QString::SkipEmptyParts);
                    for(auto const& a : arr)
                    {
                        coeffs.append(QString("%1").arg(a.toDouble(),0,'E',12));
                    }
                }
            }
        }
        stream.writeStartElement("Exponents");
        stream.writeAttribute("Orbital", orb);
        stream.writeCharacters(exponents.join(" "));
        stream.writeEndElement();
        stream.writeStartElement("Coefficients");
        stream.writeAttribute("Orbital", orb);
        stream.writeCharacters("\n");
        int nexp = exponents.size();
        for (int i=0; i<coeffs.size();++i)
        {
            stream.writeCharacters(coeffs[i]);
            stream.writeCharacters(" ");
            if (i%nexp == (nexp-1))
            {
                stream.writeCharacters("\n");
            }
        }

        stream.writeEndElement();
        if (exponents.size() > 0)
        {
            stream.writeTextElement("Power", QString::number(coeffs.size()/exponents.size()));
        }
    }

    stream.writeEndElement();
}

QString Builder::getXMLDocumentation(std::shared_ptr<AbstractSKToolkit> sktoolkit,
                                     std::shared_ptr<AtomicInfo> atomic_info_1,
                                     std::shared_ptr<AtomicInfo> atomic_info_2,
                                     int shell1, int shell2,
                                     Input::ConfiningInfo conf_info_1, Input::ConfiningInfo conf_info_2)
{
    QString res;
    QXmlStreamWriter stream(&res);
    stream.setAutoFormatting(true);
    stream.writeStartElement("Documentation");

    auto atom_1 = atomic_info_1->getAtom() ;
    auto atom_2 = atomic_info_2->getAtom() ;

    auto m_atom1_orbitals = atomic_info_1->getShellOrbitals(shell1);
    auto m_atom2_orbitals = atomic_info_2->getShellOrbitals(shell2);

    stream.writeStartElement("General");
        stream.writeTextElement("Identifier", QString("%1-%2.skf").arg(atom_1->getSymbol(), atom_2->getSymbol()));
        stream.writeTextElement("Creation", QDateTime::currentDateTime().toString());

        stream.writeTextElement("Element1", atom_1->getSymbol());

        if (atom_1 != atom_2)
        {
            stream.writeTextElement("Element2", atom_2->getSymbol());

        }
    stream.writeEndElement();

    stream.writeStartElement("SK_table");
        if (input.toolchain_info.name == "bccms")
        {
            stream.writeTextElement("Code", "bccms/slateratom");
        }
        else
        {
            stream.writeTextElement("Code", "nctu/onecent");
        }

        stream.writeTextElement("Functional", "pbe");


        if (input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::DensitySuperposition)
        {
            stream.writeTextElement("Superposition", "dense");
        }
        else
        {
            stream.writeTextElement("Superposition", "pot");
        }
        if (atom_1 == atom_2)
        {
            stream.writeTextElement("Hubbard", "ang");
        }


        printAtomicDoc(stream, "1", m_atom1_orbitals, conf_info_1, atom_1, sktoolkit);

        if (atom_1 != atom_2)
        {
            printAtomicDoc(stream, "2", m_atom2_orbitals, conf_info_2, atom_2, sktoolkit);
        }
        stream.writeEndElement();
        stream.writeStartElement("E_rep");
            stream.writeTextElement("Storage", "spline");
            stream.writeTextElement("SCC", "yes");
        stream.writeEndElement();
    stream.writeEndElement();
    return res;

}

Builder::Builder(const Input::SKBuilderInput &input_, QTextStream& output_stream):  input(input_), m_output_stream(output_stream)
{
    outputDir = QDir::current();
    initialDir = QDir::current();
    init_root_dir();
    qDebug() << "Builder::ctor " << this;


}

Builder::~Builder()
{
    clean();
    qDebug() << "Builder::dtor " << this;
}

void Builder::build()
{

    if (initialized() == false)
    {
        throw BuilderNotInitializedError();
    }

    if (built)
    {
        return;
    }


    for(int i=0; i<all_pairs.size(); ++i)
    {
        const QPair<const ADPT::AtomicProperties*, const ADPT::AtomicProperties*>& pair = all_pairs.at(i);

        std::shared_ptr<AtomicInfo> atomic_info_1 = all_atomic_infos[pair.first];
        std::shared_ptr<AtomicInfo> atomic_info_2 = all_atomic_infos[pair.second];

        std::shared_ptr<TwoCenterResult> tc_result = nullptr;

        tc_result = sktoolkit->computeTwoCenterIntegrals(atomic_info_1, atomic_info_2,
                                                 all_confininginfo[pair.first->getSymbol()],
                                                 all_confininginfo[pair.second->getSymbol()]);

        for(int shell1 = 0; shell1 < atomic_info_1->getNumberOfShells(); ++shell1)
        {
            for(int shell2 = 0; shell2 < atomic_info_2->getNumberOfShells(); ++shell2)
            {
                auto overlap_integrals = tc_result->getOverlapIntegral(shell1, shell2);

                if (input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::DensitySuperposition)
                {
                    auto hamil_integrals = tc_result->getHamiltonianIntegral(shell1, shell2);

                    std::shared_ptr<SlaterKosterFile> skfile = std::make_shared<SlaterKosterFile>(SlaterKosterFile::DensitySuperposition,
                                                                                                  tc_result->getStartPoint(), tc_result->getInterval());

                    skfile->setAtomicInformation(atomic_info_1, shell1,
                                                 atomic_info_2, shell2);

                    QHashIterator<SlaterKosterIntegralType::IntegralType, std::vector<double>> it(overlap_integrals);
                    while(it.hasNext())
                    {
                        it.next();
                        if (!hamil_integrals.contains(it.key()))
                        {
                            throw std::runtime_error("Intergral missing");
                        }

                        skfile->addIntegral(it.key(), hamil_integrals[it.key()], overlap_integrals[it.key()]);
                    }

                    if (externalRepulsivePotentials.contains(pair))
                    {
                        skfile->setRepulsive(externalRepulsivePotentials[pair]);
                    }

                    QString documentation = getXMLDocumentation(sktoolkit,
                                                                atomic_info_1, atomic_info_2,
                                                                shell1, shell2,
                                                                all_confininginfo[pair.first->getSymbol()],
                                                                all_confininginfo[pair.second->getSymbol()]
                                                                );
                    skfile->setDocumentation(documentation);

                    if (!m_skfiles.contains(pair))
                    {
                        SlaterKosterFileColletion coll;
                        coll.setAtomicInfo(atomic_info_1->getAtom()->getSymbol(),
                                                       atomic_info_2->getAtom()->getSymbol(),
                                                       atomic_info_1->getShellOrbitals(),
                                                       atomic_info_2->getShellOrbitals());
                        m_skfiles.insert(pair, coll);
                    }

                    SlaterKosterFileColletion& skfiles = m_skfiles[pair];
                    skfiles.setSKFile(shell1, shell2, skfile);
                }
                if (input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::PotentialSuperposition)
                {
                    auto hamil_integrals = tc_result->getHamiltonianIntegral(shell1, shell2);

                    std::shared_ptr<SlaterKosterFile> skfile = std::make_shared<SlaterKosterFile>(SlaterKosterFile::PotentialSuperposition,
                                                                                                  tc_result->getStartPoint(), tc_result->getInterval());
                    skfile->setAtomicInformation(atomic_info_1, shell1,
                                                 atomic_info_2, shell2);

                    QHashIterator<SlaterKosterIntegralType::IntegralType, std::vector<double> > it(overlap_integrals);
                    while(it.hasNext())
                    {
                        it.next();
                        skfile->addIntegral(it.key(), hamil_integrals[it.key()], overlap_integrals[it.key()]  );
                    }

                    if (externalRepulsivePotentials.contains(pair))
                    {
                        skfile->setRepulsive(externalRepulsivePotentials[pair]);
                    }

                    QString documentation = getXMLDocumentation(sktoolkit,
                                                                atomic_info_1, atomic_info_2,
                                                                shell1, shell2,
                                                                all_confininginfo[pair.first->getSymbol()],
                                                                all_confininginfo[pair.second->getSymbol()]
                                                                );
                    skfile->setDocumentation(documentation);

                    if (!m_skfiles.contains(pair))
                    {
                        SlaterKosterFileColletion  coll;
                        coll.setAtomicInfo(atomic_info_1->getAtom()->getSymbol(),
                                           atomic_info_2->getAtom()->getSymbol(),
                                           atomic_info_1->getShellOrbitals(),
                                           atomic_info_2->getShellOrbitals());
                        m_skfiles.insert(pair, coll);
                    }

                    SlaterKosterFileColletion& skfiles = m_skfiles[pair];
                    skfiles.setSKFile(shell1, shell2, skfile);
                }
            }
        }
    }
    if (input.options.saveIntermediate)
    {
        if (rootDir.absolutePath() != initialDir.absolutePath())
        {
            QDir targetDir(initialDir.absoluteFilePath("intermediate_files"));
            if (targetDir.exists())
            {
                targetDir.removeRecursively();
            }
            copyPath(rootDir.path(), initialDir.absoluteFilePath("intermediate_files"));
        }
    }

    built = true;
}

void Builder::write()
{

    if (!built)
    {
        build();
    }

    QMapIterator<QPair<const ADPT::AtomicProperties*, const ADPT::AtomicProperties*>, SlaterKosterFileColletion > it(m_skfiles);


    m_output_stream << endl;
    m_output_stream << "===================================================================" << endl;

    while(it.hasNext())
    {
        it.next();
        const SlaterKosterFileColletion& skfiles = it.value();
        QMapIterator<QPair<int, int>, std::shared_ptr<SlaterKosterFile> > it2(skfiles.getSKFiles());

        m_output_stream << endl;
        m_output_stream << "Writing final SK files of element pair " << it.key().first->getSymbol() << "-" << it.key().second->getSymbol() << endl;

        while(it2.hasNext())
        {
            it2.next();
            std::shared_ptr<SlaterKosterFile> skfile = it2.value();

            if (skfile->writeToFile(outputDir, false))
            {
                m_output_stream << "    [" << skfile->atom1_orbitals().join("") << " - " << skfile->atom2_orbitals().join("") << "]: "
                                << skfile->getFilename(false) << endl;
            }
            else
            {
                m_output_stream << "SKFiles written error" << endl;
//                throw std::runtime_error("SKFiles written error");
            }
        }


    }



    this->writeWFCFile(outputDir.absoluteFilePath("wfc.hsd"));

    return;
}

QMap<QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *>, SlaterKosterFileColletion > Builder::getSKFiles() const
{
    return m_skfiles;
}


QDir Builder::getOutputDir() const
{
    return outputDir;
}

void Builder::setOutputDir(const QDir &value)
{
    outputDir = value;
    outputDir.mkpath(outputDir.absolutePath());
}

QDir Builder::getRootDir() const
{
    return rootDir;
}

void Builder::setRootDir(const QDir &value)
{
    QFileInfo info(value.absolutePath());

    if (info.absoluteFilePath().contains(QDir::homePath()))
            return;

    if (info.isDir() && info.exists() && info.isWritable() )
    {
        rootDir.removeRecursively();
        rootDir = value;
    }
}

void Builder::init_confining_info()
{
    if (confining_info_initialized)
        return;


    all_confininginfo.clear();

    for(auto const & element :  all_atomic_infos.keys())
    {
        if (input.confininginfo.contains(element->getSymbol()))
        {
            all_confininginfo.insert(element->getSymbol(), input.confininginfo[element->getSymbol()]);
        }
        else if (!all_atomic_infos[element]->orbitalFiles().empty())
        {
            qDebug() << element->getSymbol() << " has ref oc info";
        }
        else
        {
            throw MissingConfiningPotentialError(element->getSymbol().toStdString());
        }
    }



    m_output_stream << endl;
    m_output_stream << endl;
    m_output_stream << "     Input Confining Potential Definitions" << endl;
    m_output_stream << "================================================" << endl;

    for(auto const & element :  all_confininginfo.keys())
    {

        QStringList taken_items;

        //2 steps
        //1. check if orbital/potential/density are uniquely taken.
        //2. check confining potentials are uniquely defined.


        m_output_stream << "  Element: [" << element << "]" << endl;

        for(auto & set : all_confininginfo[element].confining_actions)
        {

            m_output_stream << "    - Taking [" << set.take.join(", ") << "] with the following confining potentials:" << endl;


            QStringList confining_items;

            for(auto const & conf : set.confinings)
            {
                confining_items.append(conf.orbital_types);


                m_output_stream << "      * Angular Momentum: [" << conf.orbital_types.join(", ") << "]:" << endl;

                auto var = conf.parameters->toVariant();
                for(auto const & kv : var.AsMap())
                {
                    m_output_stream << "          " << QString::fromStdString(kv.first) << ": " << QString::fromStdString(kv.second.AsString()) << endl;
                }
            }

            if (confining_items.contains("all") && confining_items.size() > 1)
            {
                m_output_stream << " *** INFO: " << "Removing duplicated definition of confining potentials." << endl;
                for(int i=set.confinings.size()-1; i>=0; --i)
                {
                    if (set.confinings[i].orbital_types.contains("all"))
                    {
                        set.confinings[i].orbital_types.clear();
                        set.confinings[i].orbital_types.append("all");
                    }
                    else
                    {
                        set.confinings.removeAt(i);
                    }
                }
            }

            confining_items.sort();
            int duplicated_confs = confining_items.removeDuplicates();
            if (duplicated_confs > 0)
            {
                m_output_stream << "*** ERROR! Duplicated confining potential for the same orbital!" << endl;
                throw std::runtime_error("Duplicated confining potential for the same orbital!");
            }



            if (set.take.contains("density") && set.take.contains("potential") && set.take.size() > 2)
            {
                m_output_stream << " *** WARNING: " << "Taking density or potential with orbitals." << endl;
            }

            if (set.take.contains("density") &&
                    input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::PotentialSuperposition )
            {
                m_output_stream << " *** WARNING: " << "Density confining is defined while using PotentialSuperposition scheme. Removed." << endl;
                if (set.take.size() > 1)
                {
                    set.take.removeOne("density");
                }
                else
                {
                     all_confininginfo[element].confining_actions.removeOne(set);
                }
            }

            if (set.take.size() > 0)
            {
                taken_items.append(set.take);
            }
        }

        taken_items.sort();
        int duplicated_items = taken_items.removeDuplicates();
        if (duplicated_items > 0)
        {
            m_output_stream << "*** ERROR! Duplicated orbital/density/potential!" << endl;
            throw std::runtime_error("Duplicated orbital/density/potential!");
        }


        // if using DensitySuperposition, check density confining
        if (input.toolchain_info.twocent_info.superposition ==
                Input::TwoCenterInfo::SuperPositionType::DensitySuperposition)
        {

            //if density not exists but potential exists, use potential one
            if (!taken_items.contains("density") && taken_items.contains("potential"))
            {
                m_output_stream << " *** INFO: " << "No confining potential defined for [density], "
                                   "using [potential] confining." << endl;
                for(auto & set : all_confininginfo[element].confining_actions)
                {
                    if (set.take.contains("potential"))
                    {
                        set.take.append("density");
                        break;
                    }
                }
            }

            //if there is density but potential, then add potential in the density run
            if (taken_items.contains("density") && !taken_items.contains("potential"))
            {
                m_output_stream << " *** INFO: " << "No confining potential defined for [potential], "
                                   "using [density] confining." << endl;

                //add potential in the same action with density
                for(auto & set : all_confininginfo[element].confining_actions)
                {
                    if (set.take.contains("density"))
                    {
                        set.take.append("potential");
                        break;
                    }
                }
            }
        }

        // if no density and potential and all orbitals are in the same run, add them.
        if (!taken_items.contains("density") && !taken_items.contains("potential") &&
                 all_confininginfo[element].confining_actions.size() == 1)
        {
            m_output_stream << " *** INFO: " << "Added [density, potential] properties into this set." << endl;
            if (input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::DensitySuperposition)
            {
                all_confininginfo[element].confining_actions.first().take.append("density");
            }
            all_confininginfo[element].confining_actions.first().take.append("potential");
        }

        // if no density and potential but separated orbitals. Error!
        if (!taken_items.contains("density") && !taken_items.contains("potential") &&
                all_confininginfo[element].confining_actions.size() > 1 &&
                 input.toolchain_info.twocent_info.superposition == Input::TwoCenterInfo::SuperPositionType::DensitySuperposition)
        {
            m_output_stream << " *** ERROR: " << "Using DensitySuperPosition without defining density confining." << endl;
            throw std::runtime_error("Using DensitySuperPosition without defining density confining.");
        }


//        // if using DensitySuperposition, check density confining
//        if (input.toolchain_info.twocent_info.superposition ==
//                Input::TwoCenterInfo::SuperPositionType::PotentialSuperposition)
//        {

//            //if density not exists but potential exists, use potential one
//            if (!taken_items.contains("potential"))
//            {
//                m_output_stream << " *** INFO: " << "No confining potential defined for [potential], "
//                                   "using [potential] confining." << endl;
//                for(auto & set : all_confininginfo[element].confining_actions)
//                {
//                    if (!set.take.contains("potential") && )
//                    {
//                        set.take.append("density");
//                        break;
//                    }
//                }
//            }

    }

    m_output_stream << endl;
    m_output_stream << endl;
    m_output_stream << "                Input Summary" << endl;
    m_output_stream << "================================================" << endl;

    for(auto const & element :  all_confininginfo.keys())
    {
        m_output_stream << "  Element: [" << element << "]" << endl;
        auto atom = ADPT::AtomicProperties::fromSymbol(element);
        auto & atomic_info = all_atomic_infos[atom];

        for(int i=0; i< atomic_info->getNumberOfShells(); ++i)
        {
            m_output_stream << "    Shell # " << i+1 << ": [";

            QStringList list;
            for( auto const & orb : atomic_info->getShellOrbitals(i))
            {
                double occ;
                atomic_info->getOccupation(orb, &occ);
                list << QString("%1^%2").arg(orb).arg(occ, 0,'f',1);
            }
            m_output_stream << list.join(", ") << "] -> " ;

            m_output_stream << " (";

            list.clear();
            for( auto const & orb : atomic_info->getShellOrbitals(i))
            {
                double occ;
                if (!atomic_info->getFinalOccupation(orb, &occ))
                    atomic_info->getOccupation(orb, &occ);
                list << QString("%1^%2").arg(orb).arg(occ, 0,'f',1);
            }
            m_output_stream << list.join(", ") << ")" << endl;

        }



        for(auto & set : all_confininginfo[element].confining_actions)
        {

            m_output_stream << "    - Taking [" << set.take.join(", ") << "] with the following confining potentials:" << endl;
            for(auto const & conf : set.confinings)
            {
                m_output_stream << "      * Angular Momentum: [" << conf.orbital_types.join(", ") << "]:" << endl;

                auto var = conf.parameters->toVariant();
                for(auto const & kv : var.AsMap())
                {
                    m_output_stream << "          " << QString::fromStdString(kv.first) << ": " << QString::fromStdString(kv.second.AsString()) << endl;
                }
            }
        }
    }

    confining_info_initialized = true;
}

void Builder::init_toolkit(bool testPrograms)
{
    if (toolkit_initialized)
        return ;

    sktoolkit = AbstractSKToolkit::getInstance(input.toolchain_info.name, m_output_stream, input.toolchain_info);

    if (!sktoolkit)
    {
        throw SKToolkitNotImplementedError(input.toolchain_info.name.toLower().toStdString());
    }

    if (!sktoolkit->setOneConterProgram(input.toolchain_info.onecent_info.path, testPrograms))
    {
        throw OnecenterCodeNotWorkingError();
    }

    if (!sktoolkit->setTwoConterProgram(input.toolchain_info.twocent_info.path, testPrograms))
    {
        throw TwocenterCodeNotWorkingError();
    }
    sktoolkit->setRootPath(rootDir.absolutePath());
    toolkit_initialized = true;
}

void Builder::init_atomic_info()
{
    if (atomic_info_initialized)
        return;

    all_pairs.clear();
    all_atomic_infos.clear();

    for(auto const & element : input.atomicinfo.keys() )
    {
        auto atom = ADPT::AtomicProperties::fromSymbol(element);
        if (atom != &ADPT::AtomicProperties::ERROR_ATOM)
        {

            const Input::AtomicInfo& atomicinfo_input = input.atomicinfo[element];

            auto atomicinfo = std::make_shared<AtomicInfo>(atom);

            QMapIterator<QString, double> it_occ(atomicinfo_input.occupation);
            while(it_occ.hasNext())
            {
                it_occ.next();
                atomicinfo->setOccupation(it_occ.key(), it_occ.value());
            }

            QMapIterator<QString, double> it_finalocc(atomicinfo_input.final_occupation);
            while(it_finalocc.hasNext())
            {
                it_finalocc.next();
                atomicinfo->setFinalOccupation(it_finalocc.key(), it_finalocc.value());
            }

            for(int i=0; i< atomicinfo_input.orbitals.size(); ++i)
            {
                atomicinfo->addShellOrbitals(atomicinfo_input.orbitals.at(i));
            }
            QMapIterator<QString, double> it2(atomicinfo_input.hubbard);
            while(it2.hasNext())
            {
                it2.next();
                int lqn = getOrbitalLQuantumNumber(it2.key());
                if(lqn != -1)
                {
                    atomicinfo->setHubburd(it2.key(), it2.value());
                }
            }
            QMapIterator<QString, double> it_oe(atomicinfo_input.orbitalEnergy);
            while(it_oe.hasNext())
            {
                it_oe.next();
                atomicinfo->setOrbitalEnergy(it_oe.key(), it_oe.value());
            }

            atomicinfo->setTemplate_onecenter_inputfile(getAbsolutePath(atomicinfo_input.onecenter_input));

            all_atomic_infos.insert(atom, atomicinfo);

            QMap<QString, QString> temp_map;
            QMapIterator<QString, QString> it(atomicinfo_input.orbital_files);
            while (it.hasNext())
            {
                it.next();
                auto path = getAbsolutePath(it.value());
                if (!path.isEmpty())
                {
                    temp_map.insert(it.key(), path);
                }
            }
            atomicinfo->setOrbitalFiles(temp_map);

            atomicinfo->setPotentialFile(getAbsolutePath(atomicinfo_input.potential_file));
            atomicinfo->setDensityFile(getAbsolutePath(atomicinfo_input.density_file));

        }
    }

    //put all possible pairs into all_pairs
    QList<const ADPT::AtomicProperties*> elements = all_atomic_infos.keys();

    for (int i=0; i<elements.size();++i)
    {
        for(int j=0; j<elements.size(); ++j)
        {

            const ADPT::AtomicProperties *atom1, *atom2;
            atom1 = elements[i];
            atom2 = elements[j];

            auto pair = qMakePair(atom1, atom2);
            auto poten = ADPT::PotentialName(atom1->getSymbol(), atom2->getSymbol());



            if (input.desired_pairs.contains(poten.toString()))
            {
                all_pairs.append(pair);
            }
            else if (input.desired_pairs.contains("?-?"))
            {
                if (atom1->getSymbol() == atom2->getSymbol())
                {
                    all_pairs.append(pair);
                }
            }
            else if (input.all_pairs || input.desired_pairs.contains("*-*"))
            {
                all_pairs.append(pair);
            }
            else if (input.desired_pairs.contains(QString("%1-*").arg(atom1->getSymbol())))
            {
                all_pairs.append(pair);
            }
            else if (input.desired_pairs.contains(QString("*-%1").arg(atom2->getSymbol())))
            {
                all_pairs.append(pair);
            }
        }
    }

    atomic_info_initialized = true;
}

void Builder::init_ext_reps()
{
    if (ext_rep_initialized)
        return;

    {
        externalRepulsivePotentials.clear();
        QMapIterator<QString, QString> it(input.external_repulsive_potentials);
        while(it.hasNext())
        {
            it.next();
            try
            {
                ADPT::PotentialName poten = ADPT::PotentialName::fromString(it.key());
                const ADPT::AtomicProperties *atom1, *atom2;
                atom1 = ADPT::AtomicProperties::fromSymbol(poten.getElement1());
                atom2 = ADPT::AtomicProperties::fromSymbol(poten.getElement2());
                auto key = qMakePair(atom1, atom2);
                auto key21 = qMakePair(atom2, atom1);
                if (all_pairs.contains(key) || all_pairs.contains(key21))
                {
                    QString repul = parseRepulsivePotential(it.value());
                    if (!repul.isEmpty() && repul.startsWith("Spline"))
                    {
                        externalRepulsivePotentials.insert(key, repul);
                    }
                    if (atom1 != atom2)
                    {
                        externalRepulsivePotentials.insert(key21, externalRepulsivePotentials[key]);
                    }
                }
            }
            catch(...)
            {
                continue;
            }
        }

        if (!externalRepulsivePotentials.isEmpty())
        {
            m_output_stream << "         External Repulsive Potentials          " << endl;
            m_output_stream << "================================================" << endl;

            for(auto const & key : externalRepulsivePotentials.keys())
            {
                 ADPT::PotentialName poten(key.first->getSymbol(), key.second->getSymbol());
                 m_output_stream << "  " << poten.toString() << ": " << input.external_repulsive_potentials[poten.toString()] << endl;
            }
            m_output_stream << endl << endl;
        }
        ext_rep_initialized = true;
    }
}

bool Builder::initialized() const
{
    return (atomic_info_initialized && confining_info_initialized && ext_rep_initialized && toolkit_initialized);
}

QStringList Builder::getMissingConfiningInfos() const
{
    QStringList res;
    for(auto const & element :  all_atomic_infos.keys())
    {
        if (!input.confininginfo.contains(element->getSymbol()))
        {
            res.append(element->getSymbol());
        }
    }
    return res;
}

void Builder::clean()
{
    all_atomic_infos.clear();
    all_pairs.clear();
    externalRepulsivePotentials.clear();
    all_confininginfo.clear();
    m_skfiles.clear();

    atomic_info_initialized = confining_info_initialized = ext_rep_initialized = toolkit_initialized = false;
    built = false;
}

void Builder::removeFiles()
{
    rootDir.removeRecursively();
    init_root_dir();
}



void Builder::init(bool testPrograms)
{
    print_header_and_version(m_output_stream);
    if (initialized())
    {
        return;
    }   

    qDebug() << rootDir.absolutePath();

    try
    {
        //TODO make a factory for this if needed.
        //initialize the toolkit.
        init_toolkit(testPrograms);

        init_atomic_info();

        //check if all confining potential are in the input file.
        //
        init_confining_info();

        m_output_stream << endl;
        m_output_stream << endl;

        //init external repulsives
        init_ext_reps();

        m_output_stream << "     Electronic Parameter Pairs to be Built     " << endl;
        m_output_stream << "================================================" << endl;
        for (auto const& poten : all_pairs )
        {
            m_output_stream << "    " << poten.first->getSymbol() << "-" << poten.second->getSymbol() << endl;
        }

    }
    catch(std::exception &e)
    {
        m_output_stream << e.what() << endl;
        throw BuilderInitializationError();
    }
}

void Builder::copyPath(const QString& src, const QString& dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

QString Builder::parseRepulsivePotential(const QString &filename)
{
    QStringList data = readFileIntoStringList(filename);
    int index = -1;
    for(int i=0; i<data.size();++i)
    {
        if (data[i].trimmed().startsWith("Spline"))
        {
            index = i;
            break;
        }
    }
    if (index > -1)
    {
        bool ok;
        int nlines = data[index+1].split(" ", QString::SkipEmptyParts).first().toInt(&ok);
        if (ok)
        {
            QStringList res = data.mid(index,nlines+3);
            return res.join("\n");
        }
    }
    return "";
}



void Builder::writeWFCFile(const QString &filename)
{
    QFile outputfile(filename);
    if (outputfile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream fout(&outputfile);


        for (auto const& elem : sktoolkit->getOnecentElements())
        {
            std::shared_ptr<AtomicInfo> info = this->all_atomic_infos[ADPT::AtomicProperties::fromSymbol(elem)];
            fout << info->getAtom()->getSymbol() << "  {" << endl;
            fout << "  AtomicNumber = " << info->getAtom()->getNumber() << endl;
            for (auto const& orb : info->getShellOrbitals(0))
            {
                fout << "  Orbital {" << endl;
                int l = getOrbitalLQuantumNumber(orb);
                fout << "    AngularMomentum = " << l << endl;
                double occ = 0;
                info->getOccupation(orb, &occ);
                fout << "    Occupation = " << QString::number(occ, 'E', 12) << endl;
                fout << "    Cutoff = 10.00" << endl;
                fout << "    Exponents {" << endl;

                auto str = sktoolkit->getOrbitalMetaData(elem, orb);
                QTextStream sin(&str, QIODevice::ReadOnly);
                QStringList exponents;
                QStringList coeffs;
                while (!sin.atEnd())
                {
                    QString line = sin.readLine();
                    if (line.startsWith("@exponents"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        int row = 1;
                        int col = arr[3].toInt();

                        if (dim > 1)
                        {
                            QStringList rc = arr[3].split(",", QString::SkipEmptyParts);
                            row = rc[0].toInt();
                            col = rc[1].toInt();
                        }
                        int nelem = row*col;
                        int rrows = nelem/3;
                        if (nelem % 3 > 0)
                        {
                            rrows += 1;
                        }
                        for (int i=0; i<rrows; ++i)
                        {
                            line = sin.readLine();
                            arr = line.split(" ", QString::SkipEmptyParts);
                            for(auto const& a : arr)
                            {
                                exponents.append(QString("%1").arg(a.toDouble(),0,'f',12));
                            }
                        }
                    }
                    if (line.startsWith("@coefficients"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        int row = 1;
                        int col = arr[3].toInt();

                        if (dim > 1)
                        {
                            QStringList rc = arr[3].split(",", QString::SkipEmptyParts);
                            row = rc[0].toInt();
                            col = rc[1].toInt();
                        }
                        int nelem = row*col;
                        int rrows = nelem/3;
                        if (nelem % 3 > 0)
                        {
                            rrows += 1;
                        }
                        for (int i=0; i<rrows; ++i)
                        {
                            line = sin.readLine();
                            arr = line.split(" ", QString::SkipEmptyParts);
                            for(auto const& a : arr)
                            {
                                coeffs.append(QString("%1").arg(a.toDouble(),0,'E',12));
                            }
                        }
                    }
                }

                fout << "      " << exponents.join(" ")<< endl;
                fout << "    }" << endl;
                fout << "    Coefficients {" << endl;
                int ncol = exponents.size();
                for (int i=0; i<coeffs.size(); ++i)
                {
                    if (i % ncol == 0)
                    {
                        fout << "      ";
                    }
                    fout << coeffs[i] << " ";
                    if (i % ncol == ncol-1)
                    {
                        fout << endl;
                    }
                }
                fout << "    }" << endl;


                fout << "  }" << endl;
            }


            fout << "}" << endl;
        }


    }


}




}
