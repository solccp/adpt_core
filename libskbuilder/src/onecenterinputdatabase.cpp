

#include "onecenterinputdatabase.h"
#include "onecenterinput.h"

#include <QDebug>

namespace SKBuilder
{

OneCenterInputDatabase::OneCenterInputDatabase(QObject *parent) :
    QObject(parent)
{
}

OneCenterInputDatabase::~OneCenterInputDatabase()
{

}

bool OneCenterInputDatabase::registerInputs(const QString &symbol, std::shared_ptr<OneCenterInput> input)
{
    const ADPT::AtomicProperties* atom = ADPT::AtomicProperties::fromSymbol(symbol);
    if (atom != &ADPT::AtomicProperties::ERROR_ATOM)
    {
        return false;
    }
    return registerInputs(atom, input);
}

bool OneCenterInputDatabase::registerInputs(const ADPT::AtomicProperties *atom, std::shared_ptr<OneCenterInput> input)
{
    if (!testInput(atom, input))
    {
        return false;
    }
    if (m_inputs.contains(atom))
    {
        removeInput(atom);
    }
    m_inputs.insert(atom, input);
    emit inputAdded(atom, input);
        return true;

    return false;
}

void OneCenterInputDatabase::clear()
{

}

void OneCenterInputDatabase::removeInput(const ADPT::AtomicProperties *atom)
{
    m_inputs.remove(atom);
    emit inputRemoved(atom);
}

std::shared_ptr<OneCenterInput> OneCenterInputDatabase::createInput(const ADPT::AtomicProperties *atom)
{
    if (m_inputs.contains(atom))
    {
        std::shared_ptr<OneCenterInput> newInput = m_inputs[atom]->clone();
        return newInput;
    }
    return nullptr;
}

std::shared_ptr<OneCenterInput> OneCenterInputDatabase::createInput(const ADPT::AtomicProperties &atom)
{
    return createInput(&atom);
}

int OneCenterInputDatabase::numOfInputs()
{
    return m_inputs.size();
}

}
