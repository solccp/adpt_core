#include "sktoolkitbase.h"

#include "skbuilder/utils.h"

#include <QFileInfo>
#include <QDebug>
#include <QDir>

#include "toolchain/nctu/sktoolkit_nctu.h"
#include "toolchain/bccms/sktoolkit_bccms.h"
#include "toolchain/mixed/sktoolkit_mixed.h"

namespace SKBuilder
{



QString AbstractSKToolkit::rootPath() const
{
    return m_rootPath;
}

void AbstractSKToolkit::setRootPath(const QString &rootPath)
{
    m_rootPath = rootPath;
}

QString AbstractSKToolkit::getOrbitalMetaData(const QString &element, const QString &orbital)
{
    return "";
}

QStringList AbstractSKToolkit::getOnecentElements()
{
    return QStringList();
}

std::shared_ptr<AbstractSKToolkit> AbstractSKToolkit::getInstance(const QString &toolkit_name, QTextStream &output_stream, const Input::ToolchainInfo &toolkit_info)
{
    if (toolkit_name.toLower() == "nctu")
    {
        auto ptr = std::make_shared<SKBuilder::Toolchain::NCTU::SKToolkit_NCTU>(output_stream, toolkit_info);
        return std::static_pointer_cast<AbstractSKToolkit>(ptr);
    }
    else if (toolkit_name.toLower() == "bccms")
    {
        auto ptr = std::make_shared<SKBuilder::Toolchain::BCCMS::SKToolkit_BCCMS>(output_stream, toolkit_info);
        return std::static_pointer_cast<AbstractSKToolkit>(ptr);
    }
    else if (toolkit_name.toLower() == "mixed")
    {
        auto ptr = std::make_shared<SKBuilder::Toolchain::MIXED::SKToolkit_MIXED>(output_stream, toolkit_info);
        return std::static_pointer_cast<AbstractSKToolkit>(ptr);
    }
    return nullptr;
}

AbstractSKToolkit::AbstractSKToolkit(QTextStream &output_stream, const Input::ToolchainInfo& toolkit_info) :
    m_output_stream(output_stream), m_toolkit_info(toolkit_info)
{
    m_rootPath = QDir::currentPath();
    qDebug() << "SKToolKit::ctor " << this;
}

AbstractSKToolkit::~AbstractSKToolkit()
{
    qDebug() << "SKToolKit::dtor " << this;
}

bool AbstractSKToolkit::setOneConterProgram(const QString &path, bool test)
{
    if (!test)
    {
        m_onecent_program = path;
        m_oc_program_set = true;
        return true;
    }
    QFileInfo info(path);
    m_output_stream << "Testing onecenter program ... " ;

    if (info.exists() && info.isExecutable())
    {
        if(testOnecentProgram(path))
        {
            m_onecent_program = path;
            m_oc_program_set = true;
            m_output_stream << "(" << info.absoluteFilePath() << ") ... " ;
            m_output_stream << "success." << endl;
            return true;
        }
    }
    else
    {
        if (info.isAbsolute())
        {
            m_output_stream << "failed." << endl;
            m_output_stream << "  " << info.absoluteFilePath() << " is not a compatible onecenter program." << endl;
            return false;
        }

        QString auto_path = which(path);
        if (auto_path == info.absoluteFilePath())
        {
            m_output_stream << "failed." << endl;
            m_output_stream << "  No onecenter program named \"" << path << "\" found in $PATH" << endl;
            return false;
        }

        m_output_stream << "(" << auto_path << ") ... " ;

        info.setFile(auto_path);
        if (info.isExecutable())
        {
            if(testOnecentProgram(path))
            {
                m_onecent_program = path;
                m_oc_program_set = true;
                m_output_stream << "success." << endl;
                return true;
            }
        }
    }
    m_output_stream << "failed." << endl;
    return false;
}

bool AbstractSKToolkit::setTwoConterProgram(const QString &path, bool test)
{
    if (!test)
    {
        m_twocent_program = path;
        m_tc_program_set = true;
        return true;
    }
    QFileInfo info(path);
    m_output_stream << "Testing twocenter program ... " ;

    if (info.exists() && info.isExecutable())
    {
        if(testTwocentProgram(path))
        {
            m_twocent_program = path;
            m_tc_program_set = true;
            m_output_stream << "(" << info.absoluteFilePath() << ") ... " ;
            m_output_stream << "success." << endl;
            return true;
        }
    }
    else
    {
        if (info.isAbsolute())
        {
            m_output_stream << "failed." << endl;
            m_output_stream << "  " << info.absoluteFilePath() << " is not a compatible twocenter program." << endl;
            return false;
        }

        QString auto_path = which(path);
        if (auto_path == info.absoluteFilePath())
        {
            m_output_stream << "failed." << endl;
            m_output_stream << "  No twocenter program named \"" << path << "\" found in $PATH" << endl;
            return false;
        }

        m_output_stream << "(" << auto_path << ") ... " ;

        info.setFile(auto_path);
        if (info.isExecutable())
        {
            if(testTwocentProgram(path))
            {
                m_twocent_program = path;
                m_tc_program_set = true;
                m_output_stream << "success." << endl;
                return true;
            }
        }
    }
    m_output_stream << "failed." << endl;
    return false;
}



}
