#include "SlaterKosterFileColletion.h"

#include <QDebug>

namespace SKBuilder
{

SlaterKosterFileColletion::SlaterKosterFileColletion()
{
    qDebug() << "SlaterKosterFileColletion::ctor " << this;
}

SlaterKosterFileColletion::SlaterKosterFileColletion(const SlaterKosterFileColletion &rhs)
{
    qDebug() << "SlaterKosterFileColletion::copy_ctor " << this;
    m_elem1 = rhs.m_elem1;
    m_elem2 = rhs.m_elem2;
    m_elem1_shells = rhs.m_elem1_shells;
    m_elem2_shells = rhs.m_elem2_shells;
    m_skfiles = rhs.m_skfiles;
}

SlaterKosterFileColletion::~SlaterKosterFileColletion()
{
    qDebug() << "SlaterKosterFileColletion::dtor " << this;
}

void SlaterKosterFileColletion::setAtomicInfo(const QString &elem1, const QString &elem2, const QList<QStringList> &elem1_shells, const QList<QStringList> &elem2_shells)
{
    m_elem1 = elem1;
    m_elem2 = elem2;
    m_elem1_shells = elem1_shells;
    m_elem2_shells = elem2_shells;
}

void SlaterKosterFileColletion::setSKFile(int elem1_shell_index, int elem2_shell_index, std::shared_ptr<SlaterKosterFile> skfile)
{
    Q_ASSERT(elem1_shell_index<m_elem1_shells.size());
    Q_ASSERT(elem2_shell_index<m_elem2_shells.size());
    m_skfiles.insert(qMakePair(elem1_shell_index, elem2_shell_index), skfile);
}


QMap<QPair<int, int>, std::shared_ptr<SlaterKosterFile>> SlaterKosterFileColletion::getSKFiles() const
{
    return m_skfiles;
}

}
