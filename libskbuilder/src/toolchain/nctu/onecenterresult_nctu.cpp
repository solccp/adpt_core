#include "onecenterresult_nctu.h"

#include <stdexcept>

#include <QDir>
#include <QFile>
#include <QDebug>
#include <stdexcept>

namespace SKBuilder{ namespace Toolchain{ namespace NCTU
{


OneCenterResult::OneCenterResult()
{
        qDebug() << "OneCenterResult_NCTU::ctor " << this;
}

OneCenterResult::~OneCenterResult()
{
        qDebug() << "OneCenterResult_NCTU::dtor " << this;
}

QStringList OneCenterResult::getAllOrbitalNames() const
    {
        return m_orbitaldata.keys();
    }

    bool OneCenterResult::contains(const QString &orbitalname)
    {
        return m_orbitaldata.contains(orbitalname);
    }

    QByteArray OneCenterResult::loadFile(QString filename)
    {
        QFile file(filename);
        if (!file.exists() || !file.open(QIODevice::ReadOnly))
        {
            throw std::runtime_error(filename.toStdString() + " is not existing or readable.");
        }
        return file.readAll();
    }

    void OneCenterResult::loadOrbitalData(const QString &folder, const QString &orbital)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString orbital_filename = dir.absoluteFilePath(orbital.trimmed()+ "_orb");
        QString orbital_momentum_filename = orbital_filename + "_lapl";

        {
            auto data = loadFile(orbital_filename);
            auto data_lapl = loadFile(orbital_momentum_filename);

            this->m_orbitaldata.insert(orbital, data);
            this->m_orbitalmomentum.insert(orbital, data_lapl);
        }
    }

    void OneCenterResult::loadTotalPotential(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("pottot");
        this->m_pottot = loadFile(filename);
    }

    void OneCenterResult::loadDensity(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("dens");
        this->m_totalDensity = loadFile(filename);
    }

    void OneCenterResult::loadCoulombPotential(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("potnc");
        this->m_potnc = loadFile(filename);
    }

    QByteArray OneCenterResult::getOrbitalData(const QString &orbitalname) const
    {
        return m_orbitaldata[orbitalname];
    }

    QByteArray OneCenterResult::getOrbitalMomentum(const QString &orbitalname) const
    {
        return m_orbitalmomentum[orbitalname];
    }

    QByteArray OneCenterResult::getDens() const
    {
        return m_totalDensity;
    }

    QByteArray OneCenterResult::getPottot() const
    {
        return m_pottot;
    }

    QByteArray OneCenterResult::getPotnc() const
    {
        return m_potnc;
    }





}}}
