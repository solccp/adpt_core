#include "twocenterinput_nctu.h"

#include "atomicinfo.h"
#include "onecenterresult_nctu.h"
#include "skbuilder/input_nctu.h"

#include "io_utils.h"
#include "skbuilder/utils.h"

#include <QDebug>

namespace SKBuilder{    namespace Toolchain {   namespace NCTU
{



QString TwoCenterInput::processAtomic(int prefix, int atomic_number, QDir workdir, const std::shared_ptr<const OneCenterResult> &atomic_result) const
{
    QStringList line;


    line << QString::number(atomic_number);
    QString filename = QString("dens_%1").arg(prefix);
    line << filename;
    ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getDens()));
    filename = QString("potnc_%1").arg(prefix);
    line << filename;
    ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getPotnc()));
    filename = QString("pottot_%1").arg(prefix);
    line << filename;
    ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getPottot()));
    auto orbs = atomic_result->getAllOrbitalNames();
    line << QString("%1").arg(orbs.size());
    for(auto const & orb : orbs )
    {
        auto qn = getOrbitalQuantumNumbers(orb);
        line << QString::number(qn.first) << QString::number(qn.second);

        QString filename_orb = QString("%2_orb_%1").arg(prefix).arg(orb);
        ADPT::writeFile(workdir.absoluteFilePath(filename_orb), QString(atomic_result->getOrbitalData(orb)));
        line << filename_orb;


        QString filename_orb_lapl = QString("%2_orb_lapl_%1").arg(prefix).arg(orb);
        ADPT::writeFile(workdir.absoluteFilePath(filename_orb_lapl), QString(atomic_result->getOrbitalMomentum(orb)));
        line << filename_orb_lapl;

    }
    return line.join(" ");
}

TwoCenterInput::TwoCenterInput()
{
    qDebug() << "TwoCenterInput_NCTU::ctor " << this;
}

TwoCenterInput::~TwoCenterInput()
{
    qDebug() << "TwoCenterInput_NCTU::dtor " << this;
}

void TwoCenterInput::createInput(const QString &workDir_,
                                     SKBuilder::Input::TwoCenterInfo::SuperPositionType sptype,
                                     const std::shared_ptr<const AtomicInfo> & atomic_info_1,
                                     const std::shared_ptr<const AtomicInfo> & atomic_info_2,
                                     const std::shared_ptr<const OneCenterResult>& atomic_result_1,
                                     const std::shared_ptr<const OneCenterResult>& atomic_result_2,
                                     const std::shared_ptr<const SKBuilder::Input::TwoCenterParameter_NCTU> &params) const
    {
        QDir workdir(workDir_);
        QString input_filename = workdir.absoluteFilePath("input");

        QStringList lines;


        int iSpType = to_underlying(sptype);

        QString header = QString("%1 %2 %3 %4 %5 %6 %7 %8 %9 %10").arg(params->ngrid1).arg(params->ngrid2)
                .arg(params->rm).arg(params->c2)
                .arg(params->interval, 0, 'f', 4)
                .arg(params->end_distance, 0, 'f', 4)
                .arg(params->interval, 0, 'f', 4)
                .arg(params->functional_type).arg(params->functional_name).arg(iSpType);

        lines << header;


        lines << processAtomic(1, atomic_info_1->getAtom()->getNumber(), workdir, atomic_result_1);
        lines << processAtomic(2, atomic_info_2->getAtom()->getNumber(), workdir, atomic_result_2);

        ADPT::writeFile(input_filename, lines.join("\n"));

        return;
    }







}}}


