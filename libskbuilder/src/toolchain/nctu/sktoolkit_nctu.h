#ifndef SKTOOLKIT_NCTU_H
#define SKTOOLKIT_NCTU_H


#include "skbuilder/sktoolkitbase.h"
#include <memory>

#include <QMap>


namespace SKBuilder{
    class TwoCenterResult;
namespace Toolchain{    namespace NCTU
{

    class OneCenterResult;
    class OneCenterInput;
    class SKToolkit_NCTU : public SKBuilder::AbstractSKToolkit
    {
    public:
        SKToolkit_NCTU(QTextStream& output_stream, const SKBuilder::Input::ToolchainInfo& toolkit_info);
        virtual ~SKToolkit_NCTU();

        // AbstractSKToolkit interface
    public:
        virtual std::shared_ptr<SKBuilder::TwoCenterResult> computeTwoCenterIntegrals(
                const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_1,
                const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_2,
                const Input::ConfiningInfo &confining_info_1,
                const Input::ConfiningInfo &confining_info_2) override;

    protected:
        virtual bool testOnecentProgram(const QString &path) override;
        virtual bool testTwocentProgram(const QString &path) override;

    protected:
        void evaluateAtomicData(const std::shared_ptr<const SKBuilder::AtomicInfo> &atomic_info,
                          const Input::ConfiningInfo &confining_info
                          );

        void evaluateOnecenter(const QString &workingDirectory, const std::shared_ptr<const OneCenterInput>& oc_input);

        void evaluateTwoCenter(const std::shared_ptr<const SKBuilder::AtomicInfo> &atomic_info_1,
                               const std::shared_ptr<const SKBuilder::AtomicInfo> &atomic_info_2);

        QMap<QString, std::shared_ptr<OneCenterResult> > m_onecenter_results;
        QMap<QString, std::shared_ptr<SKBuilder::TwoCenterResult>> m_twocenter_results;
    };
}}}


#endif // SKTOOLKIT_NCTU_H

