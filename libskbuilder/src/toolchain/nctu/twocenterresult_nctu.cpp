#include "twocenterresult_nctu.h"

#include <iostream>
#include <QDir>
#include <QStringList>
#include <QVector>

#include "atomicinfo.h"
#include "atomicproperties.h"

#include <Variant/Variant.h>

#include <QDebug>

namespace SKBuilder
{

namespace Toolchain{    namespace NCTU{


TwoCenterResult_NCTU::TwoCenterResult_NCTU(double starting_point, double interval,
        const std::shared_ptr<const AtomicInfo>& atomic_info_1,
        const std::shared_ptr<const AtomicInfo>& atomic_info_2)
    : TwoCenterResult(starting_point, interval), m_atomic_info_1(atomic_info_1), m_atomic_info_2(atomic_info_2)
{
    qDebug() << "TwoCenterResult_NCTU::ctor " << this;
}

TwoCenterResult_NCTU::~TwoCenterResult_NCTU()
{
    qDebug() << "TwoCenterResult_NCTU::dtor " << this;
}


void TwoCenterResult_NCTU::loadResult(const QString &folder)
{
    QFile result_file(QDir(folder).absoluteFilePath("result.yaml"));
    if (!result_file.exists())
    {
        throw std::runtime_error("Cannot open result.yaml");
    }

    try
    {
        libvariant::Variant var = libvariant::DeserializeYAMLFile(QDir(folder).absoluteFilePath("result.yaml").toStdString().c_str());
        if (!var.Contains("twocent"))
        {
            throw std::runtime_error("No twocent section found in result.yaml");
        }
        else
        {
            for(auto i=0; i< var["twocent"].Size(); ++i)
            {
                QString atom1 = QString::fromStdString(var["twocent"][i]["pair"][0].AsString());
                QString atom2 = QString::fromStdString(var["twocent"][i]["pair"][1].AsString());
                if ((atom1.toLower() == this->m_atomic_info_1->getAtom()->getSymbol().toLower()) &&
                     (atom2.toLower() == this->m_atomic_info_2->getAtom()->getSymbol().toLower())
                   )
                {
                    auto integrals =var["twocent"][i]["integrals"];
                    for(auto j=0; j<integrals.Size(); ++j)
                    {
                        QString integral_type_str = QString::fromStdString(integrals[j]["type"].AsString());
                        SlaterKosterIntegralType::IntegralType integral_type =
                                SlaterKosterIntegralType::fromString(integral_type_str);
                        if (integral_type == SlaterKosterIntegralType::ERROR)
                        {
                            continue;
                        }

                        QString orb1 = QString::fromStdString(integrals[j]["orbitals"][0].AsString());
                        QString orb2 = QString::fromStdString(integrals[j]["orbitals"][1].AsString());

                        int shell1 = m_atomic_info_1->getShellFromOribital(orb1);
                        int shell2 = m_atomic_info_2->getShellFromOribital(orb2);


                        {
                            std::vector<double> integral = libvariant::VariantCaster<std::vector<double> >::Cast(integrals[j]["overlap"]);
                            addOverlapIntegral(shell1, shell2, integral_type, integral);
                        }


                        if (integrals[j].Contains("potential_hamil") && integrals[j].Contains("density_hamil") )
                        {
                            throw std::runtime_error("Twocent output has both potential_hamil and density_hamil!!");
                        }
                        if (integrals[j].Contains("potential_hamil"))
                        {
                            std::vector<double> integral = libvariant::VariantCaster<std::vector<double> >::Cast(integrals[j]["potential_hamil"]);
                            addHamiltonianIntegral(shell1, shell2, integral_type, integral);
                        }
                        else if (integrals[j].Contains("density_hamil"))
                        {
                            std::vector<double> integral = libvariant::VariantCaster<std::vector<double> >::Cast(integrals[j]["density_hamil"]);
                            addHamiltonianIntegral(shell1, shell2, integral_type, integral);
                        }
                    }
                    return;
                }
            }
            throw std::runtime_error("No correct twocent integral found.");
        }
    }
    catch(std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        throw std::runtime_error("Error parsing result.yaml");
    }
}


}}}
