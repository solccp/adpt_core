#ifndef TWOCENTERINPUT_NCTU_H
#define TWOCENTERINPUT_NCTU_H
#include "twocenterinput.h"
#include "skbuilder/input_nctu.h"

#include <QString>

template <typename E>
constexpr typename std::underlying_type<E>::type to_underlying(E e) {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

namespace SKBuilder
{
    class AtomicInfo;

    namespace Toolchain{    namespace NCTU{

    class OneCenterResult;

    class TwoCenterInput
    {
    private:
        QString processAtomic(int prefix, int atomic_number, QDir workdir,
                              const std::shared_ptr<const OneCenterResult>& atomic_result) const;
    public:

        TwoCenterInput();
        virtual ~TwoCenterInput();
        void createInput(const QString& workDir_,
                         SKBuilder::Input::TwoCenterInfo::SuperPositionType sptype,
                         const std::shared_ptr<const AtomicInfo> &atomic_info_1,
                         const std::shared_ptr<const AtomicInfo> &atomic_info_2,
                         const std::shared_ptr<const OneCenterResult>& atomic_result_1,
                         const std::shared_ptr<const OneCenterResult>& atomic_result_2,
                         const std::shared_ptr<const SKBuilder::Input::TwoCenterParameter_NCTU>& params
                         ) const;
    };

}}}

#endif // TWOCENTERINPUT_NCTU_H
