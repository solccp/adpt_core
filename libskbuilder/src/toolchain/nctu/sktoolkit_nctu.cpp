
//libadpt_common
#include "process_utils.h"
#include "io_utils.h"
#include "basic_types.h"


//SKBulder
#include "atomicinfo.h"
#include "slaterkosterfile.h"

#include "skbuilder/utils.h"


//nctu specific
#include "sktoolkit_nctu.h"
#include "onecenterinput_nctu.h"
#include "onecenterresult_nctu.h"
#include "twocenterinput_nctu.h"
#include "twocenterresult_nctu.h"

#include <QTemporaryDir>
#include <QProcess>
#include <QRegularExpression>
#include <QDebug>



namespace SKBuilder{    namespace Toolchain{    namespace NCTU
{

    SKToolkit_NCTU::SKToolkit_NCTU(QTextStream &output_stream, const SKBuilder::Input::ToolchainInfo &toolkit_info)
        : AbstractSKToolkit(output_stream, toolkit_info)
    {
        qDebug() << "SKToolkit_NCTU::ctor " << this;
    }

    SKToolkit_NCTU::~SKToolkit_NCTU()
    {
        qDebug() << "SKToolkit_NCTU::dtor " << this;
    }

    std::shared_ptr<TwoCenterResult> SKToolkit_NCTU::computeTwoCenterIntegrals(const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_1,
                                                                               const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_2,
                                                                               const SKBuilder::Input::ConfiningInfo &confining_info_1,
                                                                               const SKBuilder::Input::ConfiningInfo &confining_info_2)
    {
        //Check if it's already done.
        //If not, do onecent

        //Parse output
        //Then do twocent
        //Parse output

        evaluateAtomicData(atomic_info_1, confining_info_1);
        evaluateAtomicData(atomic_info_2, confining_info_2);


        evaluateTwoCenter(atomic_info_1, atomic_info_2);
        ADPT::PotentialName poten(atomic_info_1->getAtom()->getSymbol(), atomic_info_2->getAtom()->getSymbol());

        return m_twocenter_results[poten.toString()];

    }

    bool SKToolkit_NCTU::testOnecentProgram(const QString &path)
    {
        QByteArray output;
        QStringList args;
        args << "-v";
        bool succ = ADPT::ExecuteCommand(QDir::currentPath(), path, args, output, QString());

        if (succ)
        {
            bool ok;
            int version_number = QString(output).toInt(&ok);
            if (!ok)
                return false;

            if (version_number >= 20160224)
            {
                return true;
            }
        }
        return succ;
    }

    bool SKToolkit_NCTU::testTwocentProgram(const QString &path)
    {
        QByteArray output;
        QStringList args;
        args << "-v";
        bool succ = ADPT::ExecuteCommand(QDir::currentPath(), path, args, output, QString());

        if (succ)
        {
            bool ok;
            int version_number = QString(output).toInt(&ok);
            if (!ok)
                return false;

            if (version_number >= 20160224)
            {
                return true;
            }
        }
        return succ;
    }

    //! Running several onecent calculations for an element
    void SKToolkit_NCTU::evaluateAtomicData(const std::shared_ptr<const AtomicInfo> &atomic_info,
                                           const SKBuilder::Input::ConfiningInfo &confining_info)
    {
        QString element = atomic_info->getAtom()->getSymbol();

        QString atomic_rootpath = QDir(m_rootPath).absoluteFilePath(QString("Atomic_%1").arg(element));
        QDir(atomic_rootpath).mkpath(atomic_rootpath);

        if (m_onecenter_results.contains(element))
        {
            return;
        }

        std::shared_ptr<const SKBuilder::Input::OneCenterParameter> parameter =
                std::static_pointer_cast<const SKBuilder::Input::OneCenterParameter>(std::make_shared<SKBuilder::Input::OneCenterParameter_NCTU>());
        if (this->m_toolkit_info.onecent_info.default_parameter)
        {
            parameter = this->m_toolkit_info.onecent_info.default_parameter;
        }
        if (this->m_toolkit_info.onecent_info.specific_parameters.find(element) !=
                this->m_toolkit_info.onecent_info.specific_parameters.end())
        {
            parameter = m_toolkit_info.onecent_info.specific_parameters.at(element);
        }

        std::shared_ptr<OneCenterInput> template_input = std::make_shared<OneCenterInput>();
        template_input->initialize(atomic_info);
        template_input->setParameters(parameter);

        std::shared_ptr<OneCenterResult> oc_result = std::make_shared<OneCenterResult>();

        m_output_stream << "Atomic calculations for element " << element << endl;

        //Pre-handle the take-actions.
        //if density is existing, take dens, and potnc
        //if potential
        //if neither density and potential, take dens and potnc and pottot as well.
        //maybe check when init.



        for(auto const & oc_action : confining_info.confining_actions)
        {
            QString takeStr = oc_action.take.join("_");
            QString workDir = QDir(atomic_rootpath).absoluteFilePath(takeStr);

            std::shared_ptr<OneCenterInput> oc_input = template_input->clone();

            for(auto const & oc_conf: oc_action.confinings)
            {
                for(auto const & type : oc_conf.orbital_types)
                {
                    if (type.toLower() == "all")
                    {
                        oc_input->setConfining(-1, std::dynamic_pointer_cast<SKBuilder::Input::ConfiningParameter_NCTU>(oc_conf.parameters));
                        break;
                    }
                    else
                    {

                        auto qn = getOrbitalQuantumNumbers(type);
                        if (qn.first == -1)
                        {
                            int l = getOrbitalLQuantumNumber(type);
                            oc_input->setConfining(l, std::dynamic_pointer_cast<SKBuilder::Input::ConfiningParameter_NCTU>(oc_conf.parameters));
                        }
                        else
                        {
                            oc_input->setConfining(qn.first, qn.second, std::dynamic_pointer_cast<SKBuilder::Input::ConfiningParameter_NCTU>(oc_conf.parameters));
                        }

                    }
                }
            }

            //one runcent
            evaluateOnecenter(workDir, oc_input);


            for(auto const & take_item : oc_action.take)
            {
                if (take_item.toLower() == "density")
                {
                    oc_result->loadDensity(workDir);
                    m_output_stream << "  Onecent for density ... done" << endl;
                    if (oc_result->getPotnc().isEmpty())
                    {
                        oc_result->loadCoulombPotential(workDir);
                    }
                }
                else if (take_item.toLower() == "potential")
                {
                    oc_result->loadTotalPotential(workDir);
                    oc_result->loadCoulombPotential(workDir);
                    m_output_stream << "  Onecent for potential ... done" << endl;
                }
                else
                {
                    m_output_stream << "  Onecent for " << take_item << " ... done" << endl;
                    for(auto const & orb :atomic_info->getAllOrbitals())
                    {
                        if (orb.contains(take_item))
                        {
                            oc_result->loadOrbitalData(workDir, orb);
                        }
                    }
                }
            }
        }
        m_output_stream << endl;
        m_onecenter_results.insert(element, oc_result);
    }

    //! Actual function to run the onecent
    void SKToolkit_NCTU::evaluateOnecenter(const QString& workingDirectory,
                                           const std::shared_ptr<const OneCenterInput> &oc_input)
    {
        QDir workDir(workingDirectory);
        if (!workDir.mkpath(workDir.absolutePath()))
        {
            throw std::runtime_error("Cannot create folder " + workingDirectory.toStdString());
        }


        QString filename = workDir.absoluteFilePath("input");
        ADPT::writeFile(filename, oc_input->toString());

        QByteArray output;
        ADPT::ExecuteCommand(workDir.absolutePath(), this->m_onecent_program, QStringList(), output, QString());
    }

    void SKToolkit_NCTU::evaluateTwoCenter(const std::shared_ptr<const AtomicInfo> &atomic_info_1, const std::shared_ptr<const AtomicInfo> &atomic_info_2)
    {
        QString element1 = atomic_info_1->getAtom()->getSymbol();
        QString element2 = atomic_info_2->getAtom()->getSymbol();

        ADPT::PotentialName poten(element1, element2);
        if (m_twocenter_results.contains(poten.toString()))
        {
            return;
        }

        QString twocent_rootpath = QDir(m_rootPath).absoluteFilePath(QString("Twocenter_%1_%2").arg(element1).arg(element2));
        QDir(twocent_rootpath).mkpath(twocent_rootpath);

        auto atomic_result_1 = m_onecenter_results[element1];
        auto atomic_result_2 = m_onecenter_results[element2];


        std::shared_ptr<const SKBuilder::Input::TwoCenterParameter_NCTU> param = std::make_shared<SKBuilder::Input::TwoCenterParameter_NCTU>();
        if (this->m_toolkit_info.twocent_info.default_parameter)
        {
            param = std::dynamic_pointer_cast<const SKBuilder::Input::TwoCenterParameter_NCTU>(this->m_toolkit_info.twocent_info.default_parameter);
        }


        if (m_toolkit_info.twocent_info.specific_parameters.find(poten.toString()) !=
                m_toolkit_info.twocent_info.specific_parameters.end())
        {
            param = std::dynamic_pointer_cast<const SKBuilder::Input::TwoCenterParameter_NCTU>(
                        m_toolkit_info.twocent_info.specific_parameters.at(poten.toString()));
        }


        auto tc_input = std::make_shared<TwoCenterInput>();
        tc_input->createInput(twocent_rootpath, m_toolkit_info.twocent_info.superposition,
                              atomic_info_1, atomic_info_2,
                             atomic_result_1, atomic_result_2, param);

        m_output_stream << "Twocenter calculations for element pair " << element1 << "-" << element2 << " ... ";


        try
        {

            QByteArray output;
            ADPT::ExecuteCommand(QDir(twocent_rootpath).absolutePath(), this->m_twocent_program, QStringList() << "input", output, QString());

            {
                auto tc_result = std::make_shared<TwoCenterResult_NCTU>(
                            param->interval, param->interval, atomic_info_1, atomic_info_2);
                tc_result->loadResult(QDir(twocent_rootpath).absolutePath());
        
                this->m_twocenter_results.insert(poten.toString(), tc_result);
            }
            m_output_stream << "done." << endl;


            if (element1 != element2)
            {
                m_output_stream << "Twocenter calculations for element pair " << element2 << "-" << element1 << " ... ";
                ADPT::PotentialName poten_21(element2, element1);
                auto tc_result = std::make_shared<TwoCenterResult_NCTU>(
                            param->interval, param->interval, atomic_info_2, atomic_info_1);
                tc_result->loadResult(QDir(twocent_rootpath).absolutePath());

                this->m_twocenter_results.insert(poten_21.toString(), tc_result);
                m_output_stream << "done." << endl;
            }

        }
        catch(std::exception & e)
        {
            m_output_stream << "failed." << endl;
            m_output_stream << e.what() << endl;
            throw e;
        }
        m_output_stream.flush();
    }
}}}




