#ifndef ONECENTERINPUT_NCTU_H
#define ONECENTERINPUT_NCTU_H


#include <stdexcept>
#include <memory>
#include <array>

#include "skbuilder/input_nctu.h"


#include <QFile>
#include <QString>
#include <QList>
#include <QStringList>
#include <QMap>




namespace SKBuilder{
    class AtomicInfo;

namespace Toolchain{    namespace NCTU
{

    class InvalidFormat : public std::runtime_error {
    public:
      InvalidFormat() : std::runtime_error("InvalidFormat") {}
    };

    class OrbitalNameNotFoundException : public std::runtime_error {
    public:
      OrbitalNameNotFoundException(const QString& orb) : std::runtime_error(orb.toStdString()) {}
    };


    struct OneCenterSpinor
    {
        double inital_energy;
        int mainQuantumNumber;
        int spinorQuantumNumber;
        double occupation;
    };

    struct WoodsSaxonPotential
    {
        double w;
        double a;
        double r;
    };

    //! Input datastructure of onecenter
    class OneCenterInput
    {
    public:
        OneCenterInput();
        virtual ~OneCenterInput();

        const QStringList &getAllOrbitalNames() const;

    private:
        //! Atomic number
        int m_z;
        //! Atomic charge, should be consistent with the total number of electrons
        int m_ion = 0;
        //! Number of spinors
        int m_norb;
        //! Number of grid for numerical solution
        int m_ngrid = 3000;
        //! Functional
        QString m_igga = "pbe";
        //! SCF mixing coefficient
        double m_dcop = 0.2;

        //Confining potentials
        static const int maxl = 6;
        std::array<WoodsSaxonPotential, maxl> m_conf;


        QMap<QPair<int, int>, WoodsSaxonPotential> m_orb_conf;


        //! Spinors
        QList<OneCenterSpinor> m_spinors;

    public:
        void resetConfining();
        virtual QString toString() const;

    public:
        std::shared_ptr<OneCenterInput> clone() const;

        //! SCF mixing coefficient
        double dcop() const;
        //! SCF mixing coefficient
        void setDcop(double dcop);
        //! Number of grid for numerical solution
        int ngrid() const;
        //! Number of grid for numerical solution
        void setNgrid(int ngrid);

    private:
        QStringList m_orbitalNames;
        QStringList m_valenceOrbitalNames;
        QMap<QString, double> m_orbitalOccupations;


    private:
        //Helper functions
        QString spinorToOrbitalName(int mainQN, int spinQN);
        int spinorAngularToOrbitalAngular(int spinQN);
        void buildOrbitalInfo();
        QPair<int, int> getHighestOrbital(const QStringList& orbitals, const std::shared_ptr<const AtomicInfo> &atomicInfo );

    public:
        const QStringList &getValenceOrbitalNames() const;
        double getOrbitalOccupation(const QString &orbital) const;
        const QMap<QString, double> &getAllOrbitalOccupation() const;


    public:
        //! Initalization with AtomicInfo.
        void initialize(const std::shared_ptr<const AtomicInfo>& atomicinfo);
        //! Initalization with template input file.
        void loadTemplateInput(QFile *file);
        //! Initalization with template input file.
        void loadTemplateInput(const QString& filename);

        virtual bool setConfining(int l, const std::shared_ptr<const Input::ConfiningParameter_NCTU>& conf);
        virtual bool setConfining(int n, int l, const std::shared_ptr<const Input::ConfiningParameter_NCTU>& conf);

        virtual bool getConfining(int l, const std::shared_ptr<Input::ConfiningParameter_NCTU>& conf) const;


        virtual int getAtomicNumber() const;

    public:
        virtual void setParameters(const std::shared_ptr<const Input::OneCenterParameter> &para);
    };

}}}
#endif // OneCenterInput_NCTU_H
