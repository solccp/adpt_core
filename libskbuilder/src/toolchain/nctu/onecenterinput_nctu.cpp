#include "onecenterinput_nctu.h"

#include <QString>
#include <QTextStream>
#include <QByteArray>
#include <QFile>
#include <QStringList>
#include <QFileInfo>

#include "atomicinfo.h"

#include "skbuilder/utils.h"
#include "skbuilder/input.h"
#include "skbuilder/input_nctu.h"

#include <QDebug>

namespace SKBuilder{    namespace Toolchain{    namespace NCTU
{

OneCenterInput::OneCenterInput()
{
    qDebug() << "OneCenterInput_NCTU::ctor " << this;
}

OneCenterInput::~OneCenterInput()
{
    qDebug() << "OneCenterInput_NCTU::dtor " << this;
}

const QStringList& OneCenterInput::getAllOrbitalNames() const
    {
        return m_orbitalNames;
    }

    int OneCenterInput::ngrid() const
    {
        return m_ngrid;
    }

    void OneCenterInput::setNgrid(int ngrid)
    {
        m_ngrid = ngrid;
    }

    int OneCenterInput::spinorAngularToOrbitalAngular(int spinQN)
    {
        int lqn;
        if (spinQN < 0)
        {
            lqn = -spinQN-1;
        }
        else
        {
            lqn = spinQN;
        }
        return lqn;
    }

    QString OneCenterInput::spinorToOrbitalName(int mainQN, int spinQN)
    {
        static const QString names = "spdfghijkl";

        int lqn = spinorAngularToOrbitalAngular(spinQN);

        QString lname = QString("%1%2").arg(mainQN).arg(names.at(lqn));

        return lname;
    }

    void OneCenterInput::buildOrbitalInfo()
    {
        m_orbitalNames.clear();
        m_valenceOrbitalNames.clear();

        QString shellNames[4];

        m_orbitalOccupations.clear();
        for(int i=0; i<m_spinors.size(); ++i)
        {
            QString orbitalName = spinorToOrbitalName(m_spinors.at(i).mainQuantumNumber, m_spinors.at(i).spinorQuantumNumber);
            int lqn = spinorAngularToOrbitalAngular(m_spinors.at(i).spinorQuantumNumber);
            if (lqn < 4 && lqn >-1)
            {
                shellNames[lqn] = orbitalName;
            }

            m_orbitalNames.append(orbitalName);
            if (m_orbitalOccupations.contains(orbitalName))
            {
                m_orbitalOccupations[orbitalName] += m_spinors.at(i).occupation;
            }
            else
            {
                m_orbitalOccupations[orbitalName] = m_spinors.at(i).occupation;
            }
        }

        for(int i=0; i<4;++i)
        {
            if (!shellNames[i].isEmpty())
            {
                m_valenceOrbitalNames.append(shellNames[i]);
            }
        }
        m_orbitalNames.removeDuplicates();
    }

    bool OneCenterInput::setConfining(int l, const std::shared_ptr<const Input::ConfiningParameter_NCTU>& conf)
    {
        if (l>=maxl)
            return false;

        if (l==-1)
        {
            for(int i=0; i<maxl;++i)
            {
                m_conf[i].w = conf->w;
                m_conf[i].a = conf->a;
                m_conf[i].r = conf->r;
            }
        }
        else
        {
            m_conf[l].w = conf->w;
            m_conf[l].a = conf->a;
            m_conf[l].r = conf->r;
        }
        return true;
    }

    bool OneCenterInput::setConfining(int n, int l, const std::shared_ptr<const Input::ConfiningParameter_NCTU> &conf)
    {
        WoodsSaxonPotential pot;
        pot.w = conf->w;
        pot.a = conf->a;
        pot.r = conf->r;
        m_orb_conf.insert(qMakePair(n,l), pot);
        return true;
    }

    bool OneCenterInput::getConfining(int l, const std::shared_ptr<Input::ConfiningParameter_NCTU>& conf) const
    {
        if (l<0 || l >=maxl)
        {
            return false;
        }
        conf->w = m_conf[l].w;
        conf->a = m_conf[l].a;
        conf->r = m_conf[l].r;
        return true;
    }


    double OneCenterInput::dcop() const
    {
        return m_dcop;
    }

    void OneCenterInput::setDcop(double dcop)
    {
        m_dcop = dcop;
    }

    void OneCenterInput::loadTemplateInput(QFile *file)
    {
        QTextStream stream(file);

        QString line;

        int z, ion, norb, ngrid;
        QString igga;
        double dcop, w, a, r;
        double l_conf_w, l_conf_a, l_conf_r;


        line = stream.readLine();

        {
            QByteArray array = line.toLocal8Bit();
            QTextStream sins(&array);
            sins >> z >> ion >> norb >> ngrid >> igga;
        }

        line = stream.readLine();
        {
            QByteArray array = line.toLocal8Bit();
            QTextStream sins(&array);
            sins >> dcop >> w >> a >> r;
        }

        for(int i=0; i<maxl;++i)
        {
            m_conf[i].w = w;
            m_conf[i].a = a;
            m_conf[i].r = r;
        }
        QList<OneCenterSpinor> spinors;


        for(int i=0; i<norb; ++i)
        {
            bool ok;
            line = stream.readLine();
            QStringList strarray = line.split(" ", QString::SkipEmptyParts);
            if (strarray.size() < 4)
            {
                throw InvalidFormat();
            }
            int l = strarray[0].toInt(&ok);
            if (ok && l>=0 && l<maxl)
            {
                l_conf_w = strarray.at(1).toDouble(&ok);
                if (!ok)
                    throw InvalidFormat();
                l_conf_a = strarray.at(2).toDouble(&ok);
                if (!ok)
                    throw InvalidFormat();
                l_conf_r = strarray.at(3).toDouble(&ok);
                if (!ok)
                    throw InvalidFormat();
                m_conf[l].w = l_conf_w;
                m_conf[l].a = l_conf_a;
                m_conf[l].r = l_conf_r;
            }

            OneCenterSpinor spinor;

            QString tmp = strarray.at(0);
            spinor.inital_energy = tmp.replace("d","e").toDouble(&ok);
            if (!ok)
                throw InvalidFormat();
            spinor.mainQuantumNumber = strarray.at(1).toInt(&ok);
            if (!ok)
                throw InvalidFormat();
            spinor.spinorQuantumNumber = strarray.at(2).toInt(&ok);
            if (!ok)
                throw InvalidFormat();
            tmp = strarray.at(3);
            spinor.occupation = tmp.replace("d","e").toDouble(&ok);
            if (!ok)
                throw InvalidFormat();
            spinors.append(spinor);
        }

        if (norb != spinors.size())
            throw InvalidFormat();

        m_z = z;
        m_ion = ion;
        m_norb = norb;
        m_ngrid = ngrid;
        m_igga = igga;
        m_spinors = spinors;
        m_dcop = dcop;

        buildOrbitalInfo();
    }

    void OneCenterInput::loadTemplateInput(const QString &filename)
    {
        QFile file(filename);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            throw std::runtime_error("File cannot be read: " + filename.toStdString());
        }

        loadTemplateInput(&file);
    }

    void OneCenterInput::resetConfining()
    {
        m_dcop = 0.2;
        for(int i=0; i<maxl;++i)
        {
            m_conf[i].w = 0.0;
            m_conf[i].a = 3.0;
            m_conf[i].r = 5.0;
        }

        m_orb_conf.clear();

    }

    QString OneCenterInput::toString() const
    {
        QByteArray array;
        QTextStream sout(&array);
        sout << m_z << " " << m_ion << " " << m_norb << " " << m_ngrid << " " << m_igga << "\n";
        sout << m_dcop << " " << m_conf[0].w << " " << m_conf[0].a << " " << m_conf[0].r << "\n";

        int real_maxl = 0;
        for(int i=0; i<m_spinors.size();++i)
        {
            int ql = m_spinors.at(i).spinorQuantumNumber;
            if (ql < 0)
                ql = (-ql -1);
            real_maxl = std::max(real_maxl, ql);
        }

        for(int i=0; i<=real_maxl; ++i)
        {
            sout << i << " " << m_conf[i].w << " " << m_conf[i].a << " " << m_conf[i].r << "\n";
        }

        QMapIterator<QPair<int, int>, WoodsSaxonPotential> it(m_orb_conf);
        while(it.hasNext())
        {
            it.next();
            sout << it.key().first << " " << it.key().second << " " << it.value().w << " " << it.value().a << " " << it.value().r << endl;
        }

        for(int i=0; i<m_spinors.size();++i)
        {
            QString line = QString("%1 %2 %3 %4").arg(m_spinors.at(i).inital_energy, 0, 'f', 3)
                    .arg(m_spinors.at(i).mainQuantumNumber, 3)
                    .arg(m_spinors.at(i).spinorQuantumNumber, 3)
                    .arg(m_spinors.at(i).occupation, 0, 'f', 5);

            sout <<  line << endl;
        }
        sout.flush();

        return QString(array);
    }

//    QString OneCenterInput::getImplementationType() const
//    {
//        return "nctu";
//    }

    std::shared_ptr<OneCenterInput> OneCenterInput::clone() const
    {
        std::shared_ptr<OneCenterInput> newinput = std::make_shared<OneCenterInput>();
        newinput->m_dcop = this->m_dcop;
        newinput->m_igga = this->m_igga;
        newinput->m_ion = this->m_ion;
        newinput->m_ngrid = this->m_ngrid;
        newinput->m_norb = this->m_norb;
        newinput->m_z = this->m_z;
        newinput->m_spinors = this->m_spinors;
        newinput->m_conf = this->m_conf;
        newinput->m_orb_conf = this->m_orb_conf;

        newinput->m_orbitalNames = this->m_orbitalNames;
        newinput->m_orbitalOccupations = this->m_orbitalOccupations;
        newinput->m_valenceOrbitalNames = this->m_valenceOrbitalNames;
        return newinput;
    }


    int OneCenterInput::getAtomicNumber() const
    {
        return this->m_z;
    }



    const QStringList &OneCenterInput::getValenceOrbitalNames() const
    {
        return m_valenceOrbitalNames;
    }

    double OneCenterInput::getOrbitalOccupation(const QString &orbital) const
    {
        if (m_orbitalOccupations.contains(orbital))
            return m_orbitalOccupations.value(orbital);
        throw OrbitalNameNotFoundException(orbital);
    }

    const QMap<QString, double> &OneCenterInput::getAllOrbitalOccupation() const
    {
        return m_orbitalOccupations;
    }

    QPair<int, int> OneCenterInput::getHighestOrbital(const QStringList& orbitals, const std::shared_ptr<const AtomicInfo> &atomicinfo)
    {
        QPair<int, int> highestOrbital = getOrbitalQuantumNumbers(orbitals.first());
        for(int i=0; i<orbitals.size();++i)
        {
            double total_occ;
            atomicinfo->getOccupation(orbitals[i], &total_occ);
            if (total_occ <0.01)
                continue;
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);

            if (pair.first+pair.second > highestOrbital.first+highestOrbital.second)
            {
                highestOrbital = pair;
            }
            else if (pair.first > highestOrbital.first)
            {
                highestOrbital = pair;
            }
        }
        return highestOrbital;
    }


    void OneCenterInput::initialize(const std::shared_ptr<const AtomicInfo> &atomicinfo)
    {
        m_z = atomicinfo->getAtom()->getNumber();
        m_ion = 0;
        m_ngrid = 3000;
        m_igga = "pbe";

        for(int i=0; i<maxl;++i)
        {
            m_conf[i].a = 3.0;
            m_conf[i].r = 10.5;
            m_conf[i].w = 0.0;
        }
        this->m_dcop = 0.2;
        m_spinors.clear();

        const QStringList& orbitals = atomicinfo->getAllOrbitals();
        QPair<int, int> highestOrbital = getHighestOrbital(orbitals, atomicinfo);

        int max_npl = highestOrbital.first + highestOrbital.second;
//        bool stop = false;
        for (int npl = 1; npl <= max_npl; ++npl)
        {
//            if (stop)
//                break;
            //build input for orbitals not specified in the input file (core orbitals)
            for(int nqn = 1; nqn <= npl;++nqn)
            {
                int lqn = npl - nqn;
                if (lqn >= nqn)
                {
                    continue;
                }

                if (nqn > highestOrbital.first)
                {
                    continue;
                }

                {
                    bool foundDup = false;
                    for(int i=0; i<orbitals.size();++i)
                    {
                        auto pair = getOrbitalQuantumNumbers(orbitals[i]);
                        int l_nqn = pair.first;
                        int l_lqn = pair.second;

                        if (nqn == l_nqn && lqn == l_lqn)
                        {
                            foundDup = true;
                            break;
                        }

                    }
                    if (foundDup)
                    {
                        continue;
                    }


//                    if (nqn == lowest_orbital.first && lqn == lowest_orbital.second)
//                    {
//                        stop = true;
//                        break;
//                    }
                    if (lqn == 0)
                    {
                        OneCenterSpinor spinor;
                        spinor.inital_energy = -2.0;
                        spinor.mainQuantumNumber = nqn;
                        spinor.occupation = 2.0;
                        spinor.spinorQuantumNumber = -1;
                        m_spinors.append(spinor);
                    }
                    else
                    {
                        for(int ispin=0; ispin<=1; ++ispin)
                        {
                            OneCenterSpinor spinor;
                            spinor.inital_energy = -2.0;
                            spinor.mainQuantumNumber = nqn;
                            spinor.spinorQuantumNumber = std::pow(-1,ispin)*(lqn+ispin);
                            spinor.occupation = std::abs(spinor.spinorQuantumNumber)*2.0;
                            m_spinors.append(spinor);
                        }
                    }
                }
            }
        }


        //build input for orbitals specified in the input file
        for(int i=0; i<orbitals.size();++i)
        {
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);
            int nqn = pair.first;
            int lqn = pair.second;
            if (lqn == 0)
            {
                OneCenterSpinor spinor;
                spinor.inital_energy = -2.0;
                spinor.mainQuantumNumber = nqn;
                atomicinfo->getOccupation(orbitals[i], &spinor.occupation);
                spinor.spinorQuantumNumber = -1;
                m_spinors.append(spinor);
            }
            else
            {
                double totalOcc = 0.0;
                atomicinfo->getOccupation(orbitals[i], &totalOcc);
                for(int ispin=0; ispin<=1; ++ispin)
                {
                    OneCenterSpinor spinor;
                    spinor.mainQuantumNumber = nqn;
                    double maxOcc_lqn = (2*lqn+1)*2.0;
                    spinor.spinorQuantumNumber = std::pow(-1,ispin)*(lqn+ispin);
                    double maxOcc = std::abs(spinor.spinorQuantumNumber)*2.0;
                    double occ = totalOcc*(maxOcc/maxOcc_lqn);

                    spinor.occupation = occ;
                    spinor.inital_energy = -2.0;
                    if (nqn > highestOrbital.first)
                    {
                        spinor.inital_energy = -0.2;
                    }

                    m_spinors.append(spinor);
                }
            }
        }

        m_norb = m_spinors.size();
        buildOrbitalInfo();
    }


    void OneCenterInput::setParameters(const std::shared_ptr<const Input::OneCenterParameter> &para)
    {
        auto l_para = std::dynamic_pointer_cast<const Input::OneCenterParameter_NCTU>(para);
        if (l_para)
        {
            this->m_ngrid = l_para->ngrid;
            this->m_igga = l_para->functional.toLower();
        }
    }

}}}
