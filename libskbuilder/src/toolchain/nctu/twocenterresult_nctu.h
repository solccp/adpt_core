#ifndef TwoCenterResult_NCTU_H
#define TwoCenterResult_NCTU_H

#include "twocenterresult.h"
#include "slaterkosterfile.h"

namespace SKBuilder
{

namespace Toolchain{    namespace NCTU{

class TwoCenterResult_NCTU : public SKBuilder::TwoCenterResult
{
    friend class SKToolkit_NCTU;
public:
    TwoCenterResult_NCTU(double starting_point, double interval,
                         const std::shared_ptr<const SKBuilder::AtomicInfo>& atomic_info_1,
                         const std::shared_ptr<const SKBuilder::AtomicInfo>& atomic_info_2);
    ~TwoCenterResult_NCTU();
    void loadResult(const QString& folder) override;
private:
    std::shared_ptr<const SKBuilder::AtomicInfo> m_atomic_info_1;
    std::shared_ptr<const SKBuilder::AtomicInfo> m_atomic_info_2;
};

}}}

#endif // TwoCenterResult_NCTU_H
