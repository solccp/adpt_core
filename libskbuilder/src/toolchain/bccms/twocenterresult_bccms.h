#ifndef TwoCenterResult_BCCMS_H
#define TwoCenterResult_BCCMS_H

#include "twocenterresult.h"
#include "slaterkosterfile.h"

namespace SKBuilder
{

namespace Toolchain{    namespace BCCMS{

class TwoCenterResult_BCCMS : public SKBuilder::TwoCenterResult
{
    friend class SKToolkit_BCCMS;
public:
    TwoCenterResult_BCCMS(double starting_point, double interval,
                         const std::shared_ptr<const SKBuilder::AtomicInfo>& atomic_info_1,
                         const std::shared_ptr<const SKBuilder::AtomicInfo>& atomic_info_2, bool opposite);
    ~TwoCenterResult_BCCMS();
    void loadResult(const QString& folder) override;
private:
    std::shared_ptr<const SKBuilder::AtomicInfo> m_atomic_info_1;
    std::shared_ptr<const SKBuilder::AtomicInfo> m_atomic_info_2;
    bool m_opposite = false; // Is this DS reads the opposite Intergral
    void loadResult_partial(const QDir &workdir, bool hamil, bool opposite=false);
    QString getIntegralType(int ang1, int ang2, int inter);
};

}}}

#endif // TwoCenterResult_BCCMS_H
