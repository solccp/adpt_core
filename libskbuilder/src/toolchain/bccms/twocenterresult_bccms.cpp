#include "twocenterresult_bccms.h"

#include <iostream>
#include <QDir>
#include <QStringList>
#include <QVector>

#include "atomicinfo.h"
#include "atomicproperties.h"
#include "skbuilder/utils.h"

#include <Variant/Variant.h>

#include <QDebug>

namespace SKBuilder
{

namespace Toolchain{    namespace BCCMS{


TwoCenterResult_BCCMS::TwoCenterResult_BCCMS(double starting_point, double interval,
        const std::shared_ptr<const AtomicInfo>& atomic_info_1,
        const std::shared_ptr<const AtomicInfo>& atomic_info_2, bool opposite)
    : TwoCenterResult(starting_point, interval),
      m_atomic_info_1(atomic_info_1), m_atomic_info_2(atomic_info_2), m_opposite(opposite)
{
    qDebug() << "TwoCenterResult_BCCMS::ctor " << this;
}

TwoCenterResult_BCCMS::~TwoCenterResult_BCCMS()
{
    qDebug() << "TwoCenterResult_BCCMS::dtor " << this;
}




void TwoCenterResult_BCCMS::loadResult(const QString &folder)
{
    QDir root_dir(folder);
//    std::cerr << "Read twocenter integrals from folder: " << root_dir.absolutePath().toStdString() << std::endl;
//    std::cerr << "Reading " << m_atomic_info_1->getAtom()->getSymbol().toStdString() << "-" <<  m_atomic_info_2->getAtom()->getSymbol().toStdString() << ": oppo=" << m_opposite << std::endl;
    loadResult_partial(root_dir, true, m_opposite);
    loadResult_partial(root_dir, false, m_opposite);
}


void TwoCenterResult_BCCMS::loadResult_partial(const QDir &workdir, bool hamil, bool opposite)
{
    QString filesuffix = "ham";
    if (!hamil)
    {
        filesuffix = "over";
    }
    QFile result_file(workdir.absoluteFilePath(QString("at1-at2.%1.dat").arg(filesuffix)));
    if (!result_file.exists() || !result_file.open(QIODevice::Text | QIODevice::ReadOnly))
    {
        throw std::runtime_error("Cannot load " + result_file.fileName().toStdString());
    }

    QTextStream stream(&result_file);

    QString line = stream.readLine();
    bool ok;
    int npoints = line.toInt(&ok);
    if (!ok)
    {
        throw std::runtime_error("Invalid twocenter output");
    }
    line = stream.readLine();
    QStringList arr = line.split(" ", QString::SkipEmptyParts);
    int num_cols = arr.size();
    QVector<QVector<double>> data;
    data.resize(num_cols);
    for(int i=0; i<data.size();++i)
    {
        data[i].resize(npoints);
    }
    for(int j=0; j<arr.size(); ++j)
    {
        data[j][0] = arr[j].toDouble(&ok);
        if (!ok)
        {
            throw std::runtime_error("Invalid twocenter output");
        }
    }


    for(int i=1; i<npoints;++i)
    {
        line = stream.readLine();
        arr = line.split(" ", QString::SkipEmptyParts);
        for(int j=0; j<arr.size(); ++j)
        {
            data[j][i] = arr[j].toDouble(&ok);
            if (!ok)
            {
                throw std::runtime_error("Invalid twocenter output");
            }
        }
    }


    int current_index = 0;
    for(int iSh1 = 0; iSh1<m_atomic_info_1->getNumberOfShells(); ++iSh1)
    {
        for(int iSh2 = 0; iSh2<m_atomic_info_2->getNumberOfShells(); ++iSh2)
        {
            auto orbs_1 = m_atomic_info_1->getShellOrbitals(iSh1);
            auto orbs_2 = m_atomic_info_2->getShellOrbitals(iSh2);

            for(auto const & item1 : orbs_1)
            {
                auto qn1 = getOrbitalQuantumNumbers(item1);
                for(auto const & item2 : orbs_2)
                {
                    auto qn2 = getOrbitalQuantumNumbers(item2);
                    int max_ang = std::min(qn1.second, qn2.second);

                    for(int ang = 0; ang <= max_ang; ++ang)
                    {
                        int l1 = qn1.second;
                        int l2 = qn2.second;

                        int fSh1 = iSh1;
                        int fSh2 = iSh2;

                        bool change_sign = false;
                        if (opposite)
                        {
                            std::swap(fSh1, fSh2);
                            std::swap(l1, l2);
                            if ((l1+l2) % 2 !=0)
                            {
                                change_sign = true;
                            }
                        }

//                        std::cout << "----------begin---------------" << std::endl
//                                  << " Column " << current_index << std::endl
//                                  << " iSh1: " << iSh1 << " " << item1.toStdString() << " " << l1 << std::endl
//                                  << " iSh2: " << iSh2 << " " << item2.toStdString() << " " << l2 << std::endl
//                                  << "-----------end----------------" << std::endl;

//                        std::cout << "Column " << current_index << " : " << l1 << " " << l2 << " " << ang << " changesign:" << change_sign << std::endl;

                        QString int_type_str = getIntegralType(l1, l2, ang);
                        SlaterKosterIntegralType::IntegralType intType =
                                SlaterKosterIntegralType::fromString(int_type_str);

                        auto final_data = data[current_index].toStdVector();
                        if (change_sign)
                        {
                            std::transform(final_data.begin(), final_data.end(), final_data.begin(),
                                        std::bind2nd(std::multiplies<double>(), -1.0));
                        }

                        if (intType != SlaterKosterIntegralType::IntegralType::ERROR)
                        {
//                            std::cerr << " take " << int_type_str.toStdString() << std::endl;
                            if (hamil)
                            {
                                this->addHamiltonianIntegral(fSh1, fSh2, intType, final_data);
                            }
                            else
                            {
                               this->addOverlapIntegral(fSh1, fSh2, intType, final_data);
                            }
                        }
                        else
                        {
//                            std::cerr << " no-take " << std::endl;
                        }
                        ++current_index;
                    }

                }
            }

        }
    }
}

QString TwoCenterResult_BCCMS::getIntegralType(int ang1, int ang2, int inter)
{
    QString interType;
    if (inter == 0)
    {
        interType = "sigma";
    }
    else if (inter == 1)
    {
        interType = "pi";
    }
    else if (inter == 2)
    {
        interType = "delta";
    }
    else if (inter == 3)
    {
        interType = "phi";
    }

    const QString orbang = "spdf";
    QString res = QString("%1-%2-%3").arg(orbang[ang1])
            .arg(orbang[ang2]).arg(interType);
    return res;
}



}}}
