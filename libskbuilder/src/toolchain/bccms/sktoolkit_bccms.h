#ifndef SKTOOLKIT_BCCMS_H
#define SKTOOLKIT_BCCMS_H


#include "skbuilder/sktoolkitbase.h"
#include <memory>

#include <QMap>


namespace SKBuilder{
    class TwoCenterResult;
namespace Toolchain{    namespace BCCMS
{

    class OneCenterResult;
    class OneCenterInput;
    class SKToolkit_BCCMS : public SKBuilder::AbstractSKToolkit
    {
    public:
        SKToolkit_BCCMS(QTextStream& output_stream, const SKBuilder::Input::ToolchainInfo& toolkit_info);
        virtual ~SKToolkit_BCCMS() override;

        // AbstractSKToolkit interface
    public:
        virtual std::shared_ptr<SKBuilder::TwoCenterResult> computeTwoCenterIntegrals(const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_1,
                const std::shared_ptr<SKBuilder::AtomicInfo> &atomic_info_2,
                const Input::ConfiningInfo &confining_info_1,
                const Input::ConfiningInfo &confining_info_2) override;

    protected:
        virtual bool testOnecentProgram(const QString &path) override;
        virtual bool testTwocentProgram(const QString &path) override;

    protected:
        void evaluateAtomicData(const std::shared_ptr<
                                SKBuilder::AtomicInfo> &atomic_info,
                          const Input::ConfiningInfo &confining_info
                          );

        void evaluateOnecenter(const QString &workingDirectory, const std::shared_ptr<const OneCenterInput>& oc_input);

        void evaluateTwoCenter(const std::shared_ptr<const SKBuilder::AtomicInfo> &atomic_info_1,
                               const std::shared_ptr<const SKBuilder::AtomicInfo> &atomic_info_2);

        QMap<QString, std::shared_ptr<OneCenterResult> > m_onecenter_results;
        QMap<QString, std::shared_ptr<OneCenterInput> > m_onecenter_inputs;
        QMap<QString, std::shared_ptr<SKBuilder::TwoCenterResult>> m_twocenter_results;

        // AbstractSKToolkit interface
    public:
        virtual QString getOrbitalMetaData(const QString &element, const QString &orbital) override;

        // AbstractSKToolkit interface
    public:
        QStringList getOnecentElements();
    };
}}}


#endif // SKTOOLKIT_BCCMS_H

