#ifndef TWOCENTERINPUT_BCCMS_H
#define TWOCENTERINPUT_BCCMS_H
#include "twocenterinput.h"
#include "skbuilder/input_bccms.h"

#include <QString>

template <typename E>
constexpr typename std::underlying_type<E>::type to_underlying(E e) {
    return static_cast<typename std::underlying_type<E>::type>(e);
}

namespace SKBuilder
{
    class AtomicInfo;

    namespace Toolchain{    namespace BCCMS{

    class OneCenterResult;

    class TwoCenterInput
    {
    private:
        void processAtomic(QStringList& line, int prefix, bool potential_sp, QDir workdir,
                              const std::shared_ptr<const AtomicInfo> &atomic_info,
                           const std::shared_ptr<const OneCenterResult>& atomic_result) const;
    public:

        TwoCenterInput();
        virtual ~TwoCenterInput();
        void createInput(const QString& workDir_,
                         SKBuilder::Input::TwoCenterInfo::SuperPositionType sptype,
                         const std::shared_ptr<const AtomicInfo> &atomic_info_1,
                         const std::shared_ptr<const AtomicInfo> &atomic_info_2,
                         const std::shared_ptr<const OneCenterResult>& atomic_result_1,
                         const std::shared_ptr<const OneCenterResult>& atomic_result_2,
                         const std::shared_ptr<const SKBuilder::Input::TwoCenterParameter_BCCMS>& params
                         ) const;
    };

}}}

#endif // TWOCENTERINPUT_BCCMS_H
