#ifndef ONECENTERINPUT_BCCMS_H
#define ONECENTERINPUT_BCCMS_H


#include <stdexcept>
#include <memory>
#include <array>

#include "skbuilder/input_bccms.h"


#include <QFile>
#include <QString>
#include <QList>
#include <QStringList>
#include <QMap>




namespace SKBuilder{
    class AtomicInfo;

namespace Toolchain{    namespace BCCMS
{

    class InvalidFormat : public std::runtime_error {
    public:
      InvalidFormat() : std::runtime_error("InvalidFormat") {}
    };

    class OrbitalNameNotFoundException : public std::runtime_error {
    public:
      OrbitalNameNotFoundException(const QString& orb) : std::runtime_error(orb.toStdString()) {}
    };


    //! Input datastructure of onecenter
    class OneCenterInput
    {
    public:
        OneCenterInput();
        virtual ~OneCenterInput();

        const QStringList &getAllOrbitalNames() const;

    private:

        QList<QPair<int, int> > orbital_orders = {
            {1, 0},
            {2, 0}, {2, 1},
            {3, 0}, {3, 1},
            {4, 0}, {3, 2}, {4, 1},
            {5, 0}, {4, 2}, {5, 1},
            {6, 0}, {4, 3}, {5, 2}, {6, 1},
            {7, 0}, {5, 3}, {6, 2}, {7, 1},
            {8, 0}, {6, 3}, {7, 2}, {8, 1}, {8, 2}, {8, 3}
        };


        //! Atomic number
        int m_z;
        //!
        int m_max_ang;

        int m_max_scf = 1000;

        bool m_zora = false;
        //! Functional, 0=HF, 1=X-Alpha, 2=PW-LDA, 3=PBE
        int m_functional = 3;

        //Confining potentials
        static const int maxl = 6;
        std::array<double, maxl> m_conf_n;
        std::array<double, maxl> m_conf_r;

        std::array<int, maxl> m_num_occupied_shells;
        std::array<int, maxl> m_num_exponents;
        std::array<int, maxl> m_num_coeffs;
        bool m_generate_exponents = true;
        std::array<std::vector<double>, maxl> m_exponents;

        bool m_print_eigenvectors = false;
        bool m_broyden = true;
        double m_broyden_factor = 0.1;
        using pair_dd = std::pair<double, double>;
        using pair_ii = std::pair<int, int>;

        std::array<std::vector<pair_dd>, maxl> m_occupations;
        std::array<pair_ii, maxl> m_printing_orbitals;

    public:
        void resetConfining();
        virtual QString toString() const;

    public:
        std::shared_ptr<OneCenterInput> clone() const;

    private:
        QStringList m_orbitalNames;
        QStringList m_valenceOrbitalNames;
        QMap<QString, double> m_orbitalOccupations;


    private:
        //Helper functions
        void buildOrbitalInfo();
        QPair<int, int> getLowestOrbital(const QStringList& orbitals);
        QPair<int, int> getHighestOrbital(const QStringList& orbitals);

    public:
        const QStringList &getValenceOrbitalNames() const;
        double getOrbitalOccupation(const QString &orbital) const;
        const QMap<QString, double> &getAllOrbitalOccupation() const;


    public:
        //! Initalization with AtomicInfo.
        void initialize(const std::shared_ptr<const AtomicInfo>& atomicinfo);
        //! Initalization with template input file.
        void loadTemplateInput(QFile *file);
        //! Initalization with template input file.
        void loadTemplateInput(const QString& filename);

        virtual bool setConfining(int l, const std::shared_ptr<const Input::ConfiningParameter_BCCMS>& conf);
        virtual bool getConfining(int l, const std::shared_ptr<Input::ConfiningParameter_BCCMS>& conf) const;
        virtual int getAtomicNumber() const;

    public:
        virtual void setParameters(const std::shared_ptr<const Input::OneCenterParameter> &para);
    };

}}}
#endif // OneCenterInput_BCCMS_H
