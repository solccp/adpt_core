#include "onecenterresult_bccms.h"

#include <QDir>
#include <QFile>

#include <QDebug>
#include <stdexcept>

namespace SKBuilder{ namespace Toolchain{ namespace BCCMS
{


OneCenterResult::OneCenterResult()
{
        qDebug() << "OneCenterResult_BCCMS::ctor " << this;
}

OneCenterResult::~OneCenterResult()
{
        qDebug() << "OneCenterResult_BCCMS::dtor " << this;
}

QStringList OneCenterResult::getAllOrbitalNames() const
    {
        return m_orbitaldata.keys();
    }

    bool OneCenterResult::contains(const QString &orbitalname)
    {
        return m_orbitaldata.contains(orbitalname);
    }

    QByteArray OneCenterResult::loadFile(QString filename)
    {
        QFile file(filename);
        if (!file.exists() || !file.open(QIODevice::ReadOnly))
        {
            throw std::runtime_error(filename.toStdString() + " is not existing or readable.");
        }
        return file.readAll();
    }

    void OneCenterResult::loadOrbitalData(const QString &folder, const QString &orbital)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString orbital_filename = dir.absoluteFilePath(QString("%1%2%3").arg("wave_").arg(orbital.trimmed(),3,'0').arg("_up.dat"));

        loadOrbitalDataFile(orbital_filename, orbital);


        QString orbital_coeff_filename = dir.absoluteFilePath(QString("coeffs_%1.tag").arg(orbital.trimmed(),3,'0'));
        if (dir.exists(orbital_coeff_filename))
        {
            loadOrbitalMetaData(orbital_coeff_filename, orbital);
        }

    }

    void OneCenterResult::loadTotalPotential(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("pot.dat");
        loadTotalPotentialFile(filename);
    }

    void OneCenterResult::loadDensity(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("dens.dat");
        loadDensityFile(filename);
    }

    void OneCenterResult::loadCoulombPotential(const QString &folder)
    {
        QDir dir(folder);
        if (!dir.exists() || !dir.isReadable())
        {
            throw std::runtime_error(folder.toStdString() + " is not existing or readable.");
        }

        QString filename = dir.absoluteFilePath("pot.dat");
        loadCoulombPotentialFile(filename);
    }

    void OneCenterResult::loadOrbitalDataFile(const QString &file, const QString &orbital)
    {
        auto data = loadFile(file);
        this->m_orbitaldata.insert(orbital, data);
    }

    void OneCenterResult::loadTotalPotentialFile(const QString &file)
    {
        this->m_pottot = loadFile(file);
    }

    void OneCenterResult::loadDensityFile(const QString &file)
    {
        this->m_totalDensity = loadFile(file);
    }

    void OneCenterResult::loadCoulombPotentialFile(const QString &file)
    {
        this->m_potnc = loadFile(file);
    }

    void OneCenterResult::loadOrbitalMetaData(const QString &file, const QString &orbital)
    {
        auto data = loadFile(file);
        this->m_orbitalmetadata.insert(orbital, data);
    }

    QByteArray OneCenterResult::getOrbitalData(const QString &orbitalname) const
    {
        return m_orbitaldata[orbitalname];
    }

    QByteArray OneCenterResult::getOrbitalMetaData(const QString &orbitalname) const
    {
        return m_orbitalmetadata.value(orbitalname);
    }

    QByteArray OneCenterResult::getOrbitalMomentum(const QString &orbitalname) const
    {
        return m_orbitalmomentum[orbitalname];
    }

    QByteArray OneCenterResult::getDens() const
    {
        return m_totalDensity;
    }

    QByteArray OneCenterResult::getPottot() const
    {
        return m_pottot;
    }

    QByteArray OneCenterResult::getPotnc() const
    {
        return m_potnc;
    }





}}}
