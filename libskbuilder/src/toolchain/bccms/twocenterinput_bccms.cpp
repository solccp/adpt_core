#include "twocenterinput_bccms.h"

#include "atomicinfo.h"
#include "onecenterresult_bccms.h"
#include "skbuilder/input_bccms.h"

#include "io_utils.h"
#include "skbuilder/utils.h"

#include <QDebug>

namespace SKBuilder{    namespace Toolchain {   namespace BCCMS
{



void TwoCenterInput::processAtomic(QStringList& line, int prefix, bool potential_sp, QDir workdir, const std::shared_ptr<const AtomicInfo> &atomic_info, const std::shared_ptr<const OneCenterResult> &atomic_result) const
{
    QStringList orbs;
    for(int i=0; i<atomic_info->getNumberOfShells(); ++i)
    {
        orbs.append(atomic_info->getShellOrbitals(i));
    }
    line << QString("%1").arg(orbs.size());
    for(auto const & orb : orbs )
    {
        auto qn = getOrbitalQuantumNumbers(orb);
        QString filename_orb = QString("%2_orb_%1").arg(prefix).arg(orb);
        ADPT::writeFile(workdir.absoluteFilePath(filename_orb), QString(atomic_result->getOrbitalData(orb)));
        line << QString("'%1' %2").arg(filename_orb).arg(qn.second);
    }

    QString filename;
    if (potential_sp)
    {
        filename = QString("pottot_%1").arg(prefix);
        line << QString("'%1'").arg(filename);
        ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getPottot()));
        line << "'noread'";
    }
    else
    {
        filename = QString("potnc_%1").arg(prefix);
        line << QString("'%1'").arg(filename);
        ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getPotnc()));
        QString filename = QString("dens_%1").arg(prefix);
        line << QString("'%1'").arg(filename);
        ADPT::writeFile(workdir.absoluteFilePath(filename), QString(atomic_result->getDens()));
    }
    return ;
}

TwoCenterInput::TwoCenterInput()
{
    qDebug() << "TwoCenterInput_BCCMS::ctor " << this;
}

TwoCenterInput::~TwoCenterInput()
{
    qDebug() << "TwoCenterInput_BCCMS::dtor " << this;
}

void TwoCenterInput::createInput(const QString &workDir_,
                                     SKBuilder::Input::TwoCenterInfo::SuperPositionType sptype,
                                     const std::shared_ptr<const AtomicInfo> & atomic_info_1,
                                     const std::shared_ptr<const AtomicInfo> & atomic_info_2,
                                     const std::shared_ptr<const OneCenterResult>& atomic_result_1,
                                     const std::shared_ptr<const OneCenterResult>& atomic_result_2,
                                     const std::shared_ptr<const SKBuilder::Input::TwoCenterParameter_BCCMS> &params) const
{
    QDir workdir(workDir_);
    workdir.mkpath(workdir.absolutePath());
    QString input_filename = workdir.absoluteFilePath("sktwocnt.in");

    QStringList lines;

    bool isHomo = true;
    QString type = "homo";

    if (atomic_info_1->getAtom() != atomic_info_2->getAtom()) // || (iSh1 != iSh2))
    {
        type = "hetero";
        isHomo = false;
    }
    QString func = "potential";
    int iSpType = to_underlying(sptype);
    bool isPotential_sp = true;
    if (iSpType == 2) //density
    {
        func = "density_pbe";
        isPotential_sp = false;

    }

    lines.append(QString("%1 %2").arg(type).arg(func));

    lines.append(QString("%1 %2 %3 %4").arg(params->interval).arg(params->interval).arg(params->end_condition).arg(params->end_distance));
    lines.append(QString("%1 %2").arg(params->ngrid1).arg(params->ngrid2));


    processAtomic(lines, 1, isPotential_sp, workdir, atomic_info_1, atomic_result_1);
    if (!isHomo)
    {
        processAtomic(lines, 2, isPotential_sp, workdir, atomic_info_2, atomic_result_2);
    }

    ADPT::writeFile(input_filename, lines.join("\n"));

    return;
}

}}}


