#include "onecenterinput_bccms.h"

#include <QString>
#include <QTextStream>
#include <QByteArray>
#include <QFile>
#include <QStringList>
#include <QFileInfo>

#include "atomicinfo.h"

#include "skbuilder/utils.h"
#include "skbuilder/input.h"
#include "skbuilder/input_bccms.h"

#include <QDebug>

namespace SKBuilder{    namespace Toolchain{    namespace BCCMS
{

OneCenterInput::OneCenterInput()
{
    qDebug() << "OneCenterInput_BCCMS::ctor " << this;
}

OneCenterInput::~OneCenterInput()
{
    qDebug() << "OneCenterInput_BCCMS::dtor " << this;
}

const QStringList& OneCenterInput::getAllOrbitalNames() const
{
    return m_orbitalNames;
}


bool OneCenterInput::setConfining(int l, const std::shared_ptr<const Input::ConfiningParameter_BCCMS>& conf)
{
    if (l>=maxl)
        return false;

    if (l==-1)
    {
        for(int i=0; i<maxl;++i)
        {
            m_conf_n[i] = conf->n;
            m_conf_r[i] = conf->r;
        }
    }
    else
    {
        m_conf_n[l] = conf->n;
        m_conf_r[l] = conf->r;
    }
    return true;
}

bool OneCenterInput::getConfining(int l, const std::shared_ptr<Input::ConfiningParameter_BCCMS>& conf) const
{
    if (l<0 || l >=maxl)
    {
        return false;
    }
    conf->n = m_conf_n[l];
    conf->r = m_conf_r[l];
    return true;
}


    void OneCenterInput::loadTemplateInput(QFile *file)
    {
        QTextStream stream(file);

        QString line;

        int z;
        int max_ang;
        int max_scf;
        bool zora;
        int functional;

        std::array<double, maxl> conf_n;
        std::array<double, maxl> conf_r;

        std::array<int, maxl> num_occupied_shells;
        std::array<int, maxl> num_exponents;
        std::array<int, maxl> num_coeffs;
        bool generate_exponents;
        std::array<std::vector<double>, maxl> exponents;
        bool print_eigenvectors;
        bool broyden;
        double broyden_factor;
        using pair_dd = std::pair<double, double>;
        using pair_ii = std::pair<int, int>;

        std::array<std::vector<pair_dd>, maxl> occupations;
        std::array<pair_ii, maxl> printing_orbitals;

        bool failed = false;

        bool ok;
        QStringList arr;

        try
        {
            //ROW 1
            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (arr.size() < 4)
            {
                throw std::runtime_error("invalid format");
            }
            z = arr.at(0).toInt(&ok);
            if (!ok)
            {
                throw std::runtime_error("invalid format");
            }
            max_ang = arr.at(1).toInt(&ok);
            if (!ok)
            {
                throw std::runtime_error("invalid format");
            }
            max_scf = arr.at(2).toInt(&ok);;
            if (!ok)
            {
                throw std::runtime_error("invalid format");
            }

            QString dummy = arr.at(3).toLower();
            if (dummy == ".false.")
            {
                zora = false;
            }
            else if (dummy == ".true.")
            {
                zora = true;
            }
            else
            {
                throw std::runtime_error("invalid format");
            }

            //ROW 2
            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (arr.size() < 1)
            {
                throw std::runtime_error("invalid format");
            }
            functional = arr.at(0).toInt(&ok);
            if (!ok)
            {
                throw std::runtime_error("invalid format");
            }


            //ROW 3 to (3+max_ang)
            conf_n.fill(0);
            conf_r.fill(0.0);
            for(auto i = 0u; i<=max_ang; ++i)
            {
                line = stream.readLine();
                arr = line.split(" ", QString::SkipEmptyParts);
                if (arr.size() < 2)
                {
                    throw std::runtime_error("invalid format");
                }
                double r = arr.at(0).toDouble(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }
                double n = arr.at(1).toDouble(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }

                conf_n[i] = n;
                conf_r[i] = r;
            }

            //next ROWs
            num_occupied_shells.fill(0);
            for(auto i = 0u; i<=max_ang; ++i)
            {
                line = stream.readLine();
                arr = line.split(" ", QString::SkipEmptyParts);
                if (arr.size() < 1)
                {
                    throw std::runtime_error("invalid format");
                }
                int noc = arr.at(0).toInt(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }
                num_occupied_shells[i] = noc;
            }


            //next ROWs
            for(auto i = 0u; i<=max_ang; ++i)
            {
                line = stream.readLine();
                arr = line.split(" ", QString::SkipEmptyParts);
                if (arr.size() < 2)
                {
                    throw std::runtime_error("invalid format");
                }
                int nexp = arr.at(0).toInt(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }
                int ncoeff = arr.at(1).toInt(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }
                num_exponents[i] = nexp;
                num_coeffs[i] = ncoeff;
            }

            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (arr.size() < 1)
            {
                throw std::runtime_error("invalid format");
            }
            dummy = arr.at(0).toLower();
            if (dummy == ".false.")
            {
                generate_exponents = false;
            }
            else if (dummy == ".true.")
            {
                generate_exponents = true;
            }
            else
            {
                throw std::runtime_error("invalid format");
            }

            if (generate_exponents)
            {
                //read small and large exponents
                for(auto i = 0u; i<=max_ang; ++i)
                {
                    line = stream.readLine();
                    arr = line.split(" ", QString::SkipEmptyParts);
                    if (arr.size() < 2)
                    {
                        throw std::runtime_error("invalid format");
                    }
                    double sexp = arr.at(0).toDouble(&ok);
                    if (!ok)
                    {
                        throw std::runtime_error("invalid format");
                    }
                    double lexp = arr.at(1).toDouble(&ok);
                    if (!ok)
                    {
                        throw std::runtime_error("invalid format");
                    }
                    exponents[i].push_back(sexp);
                    exponents[i].push_back(lexp);
                }
            }
            else
            {
                //read small and large exponents
                for(auto i = 0u; i<=max_ang; ++i)
                {
                    for(auto j = 0u; j<num_exponents[i]; ++j)
                    {
                        line = stream.readLine();
                        arr = line.split(" ", QString::SkipEmptyParts);
                        if (arr.size() < 1)
                        {
                            throw std::runtime_error("invalid format");
                        }
                        double exp = arr.at(0).toDouble(&ok);
                        if (!ok)
                        {
                            throw std::runtime_error("invalid format");
                        }
                        exponents[i].push_back(exp);
                    }
                }
            }

            //read printing eigenvector setting
            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (arr.size() < 1)
            {
                throw std::runtime_error("invalid format");
            }
            dummy = arr.at(0).toLower();
            if (dummy == ".false.")
            {
                print_eigenvectors = false;
            }
            else if (dummy == ".true.")
            {
                print_eigenvectors = true;
            }
            else
            {
                throw std::runtime_error("invalid format");
            }

            //read bryden setting
            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (arr.size() < 2)
            {
                throw std::runtime_error("invalid format");
            }
            dummy = arr.at(0).toLower();
            if (dummy == ".false.")
            {
                broyden = false;
            }
            else if (dummy == ".true.")
            {
                broyden = true;
            }
            else
            {
                throw std::runtime_error("invalid format");
            }
            broyden_factor = arr.at(1).toDouble(&ok);
            if (!ok)
            {
                throw std::runtime_error("invalid format");
            }

            m_orbitalNames.clear();
            m_valenceOrbitalNames.clear();
            m_orbitalOccupations.clear();
            //read occupations
            for(auto i=0u; i<=max_ang; ++i)
            {
                for(auto j=0; j<num_occupied_shells[i]; ++j)
                {
                    QString orb_name = getOrbitalNameFromQuantumNumber(j+1+i,i);
                    line = stream.readLine();
                    arr = line.split(" ", QString::SkipEmptyParts);
                    if (arr.size() < 2)
                    {
                        throw std::runtime_error("invalid format");
                    }
                    double oc_up = arr.at(0).toDouble(&ok);
                    if (!ok)
                    {
                        throw std::runtime_error("invalid format");
                    }
                    double oc_down = arr.at(1).toDouble(&ok);
                    if (!ok)
                    {
                        throw std::runtime_error("invalid format");
                    }

                    m_orbitalNames.append(orb_name);
                    m_orbitalOccupations.insert(orb_name, oc_up+oc_down);
                    occupations[i].push_back(std::make_pair(oc_up, oc_down));
                }
            }


            //read output orbitals
            for(auto i=0u; i<=max_ang; ++i)
            {
                line = stream.readLine();
                arr = line.split(" ", QString::SkipEmptyParts);
                if (arr.size() < 2)
                {
                    throw std::runtime_error("invalid format");
                }
                int orb_start = arr.at(0).toInt(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }
                int orb_end = arr.at(1).toInt(&ok);
                if (!ok)
                {
                    throw std::runtime_error("invalid format");
                }

                printing_orbitals[i] = std::make_pair(orb_start, orb_end);
                for(auto j=orb_start; j<=orb_end; ++j)
                {
                    QString orb_name = getOrbitalNameFromQuantumNumber(j,i);
                    m_valenceOrbitalNames.append(orb_name);
                }
            }

        }
        catch(...)
        {
            failed = true;
        }

        if (!failed)
        {
            m_z = z;
            m_max_ang = max_ang;
            m_max_scf = max_scf;
            m_zora = zora;

            m_functional = functional;
            m_conf_n = conf_n;
            m_conf_r = conf_r;

            m_num_occupied_shells = num_occupied_shells;
            m_num_exponents = num_exponents;
            m_num_coeffs = num_coeffs;

            m_generate_exponents = generate_exponents;
            m_exponents = exponents;

            m_print_eigenvectors = print_eigenvectors;
            m_broyden = broyden;
            m_broyden_factor = broyden_factor;

            m_occupations = occupations;
            m_printing_orbitals = printing_orbitals;
        }


//        buildOrbitalInfo();
    }

    void OneCenterInput::loadTemplateInput(const QString &filename)
    {
        QFile file(filename);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            throw std::runtime_error("File cannot be read: " + filename.toStdString());
        }

        loadTemplateInput(&file);
    }

    void OneCenterInput::resetConfining()
    {
        for(int i=0; i<maxl;++i)
        {
            m_conf_n[i] = 0.0;
            m_conf_r[i] = 1.0e30;
        }
    }

    QString fortranBoolean(bool value)
    {
        if (value)
        {
            return ".true.";
        }
        else
        {
            return ".false.";
        }
    }

    QString OneCenterInput::toString() const
    {
        QByteArray array;
        QTextStream sout(&array);
        sout << m_z << " " << m_max_ang << " " << m_max_scf << " " << fortranBoolean(m_zora) << "  ! nuc_charge max_ang max_scf ZORA " << endl;
        sout << m_functional << "  ! xc functional, 0=HF, 1=X-Alpha, 2=PW-LDA, 3=PBE " <<  endl;

        for(auto i = 0; i<=m_max_ang;++i)
        {
            sout << m_conf_r[i] << " " << m_conf_n[i] << "  ! r_0 power, power=0 -> off, for l=" << i << endl;
        }

        for(auto i = 0; i<=m_max_ang;++i)
        {
            sout << m_num_occupied_shells[i]-i << "  ! number of occupied shells l=" << i << endl;
        }

        for(auto i = 0; i<=m_max_ang;++i)
        {
            if (m_generate_exponents)
            {
                sout << m_num_exponents[i] << " " << m_num_coeffs[i] << "  ! number of exponents and polynomial coefficients l=" << i << endl;
            }
            else
            {
                sout << m_exponents[i].size() << " " << m_num_coeffs[i] << "  ! number of exponents and polynomial coefficients l=" << i << endl;
            }
        }

        sout << fortranBoolean(m_generate_exponents) << "  ! generate exponents" << endl;
        if (m_generate_exponents)
        {
            for(auto i = 0; i<=m_max_ang;++i)
            {
                sout << m_exponents[i][0] << " " << m_exponents[i][1] << " ! min and max exponent, l="<< i << endl;
            }
        }
        else
        {
            for(auto i = 0; i<=m_max_ang;++i)
            {
                for(auto j : m_exponents[i])
                {
                    sout << j << endl;
                }
            }
        }
        sout << fortranBoolean(m_print_eigenvectors) << "  ! print eigenvectors etc." << endl;
        sout << fortranBoolean(m_broyden) << " " << m_broyden_factor << "  ! Broyden and mixing factor" << endl;

        for(auto i = 0; i<=m_max_ang;++i)
        {
            for(auto j = i ; j< m_occupations[i].size(); ++j)
            {
                sout << m_occupations[i][j].first << " " << m_occupations[i][j].second << "  ! occupation numbers n="<<j+1 << ", l=" << i << endl;
            }
        }

        for(auto i = 0; i<=m_max_ang;++i)
        {
            sout << m_printing_orbitals[i].first << " " << m_printing_orbitals[i].second << "  ! orbitals to print for l=" << i << endl;
        }

        sout.flush();

        return QString(array);
    }

    std::shared_ptr<OneCenterInput> OneCenterInput::clone() const
    {
        std::shared_ptr<OneCenterInput> newinput = std::make_shared<OneCenterInput>();
        newinput->m_z = this->m_z;
        newinput->m_max_ang = this->m_max_ang;
        newinput->m_max_scf = this->m_max_scf;

        newinput->m_zora = this->m_zora;
        newinput->m_functional = this->m_functional;

        newinput->m_conf_n = this->m_conf_n;
        newinput->m_conf_r = this->m_conf_r;

        newinput->m_broyden = this->m_broyden;
        newinput->m_broyden_factor = this->m_broyden_factor;
        newinput->m_exponents = this->m_exponents;


        newinput->m_generate_exponents = this->m_generate_exponents;
        newinput->m_num_coeffs = this->m_num_coeffs;
        newinput->m_num_exponents = this->m_num_exponents;
        newinput->m_num_occupied_shells = this->m_num_occupied_shells;
        newinput->m_occupations = this->m_occupations;

        newinput->m_printing_orbitals = this->m_printing_orbitals;
        newinput->m_print_eigenvectors = this->m_print_eigenvectors;

        newinput->m_orbitalNames = this->m_orbitalNames;
        newinput->m_orbitalOccupations = this->m_orbitalOccupations;
        newinput->m_valenceOrbitalNames = this->m_valenceOrbitalNames;

        return newinput;
    }


    int OneCenterInput::getAtomicNumber() const
    {
        return this->m_z;
    }



    const QStringList &OneCenterInput::getValenceOrbitalNames() const
    {
        return m_valenceOrbitalNames;
    }

    double OneCenterInput::getOrbitalOccupation(const QString &orbital) const
    {
        if (m_orbitalOccupations.contains(orbital))
            return m_orbitalOccupations.value(orbital);
        throw OrbitalNameNotFoundException(orbital);
    }

    const QMap<QString, double> &OneCenterInput::getAllOrbitalOccupation() const
    {
        return m_orbitalOccupations;
    }

    QPair<int, int> OneCenterInput::getLowestOrbital(const QStringList& orbitals)
    {
        QPair<int, int> lowestOrbital = getOrbitalQuantumNumbers(orbitals.first());
        for(int i=0; i<orbitals.size();++i)
        {
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);
            if ( (lowestOrbital.first + lowestOrbital.second) > (pair.first + pair.second) )
            {
                lowestOrbital = pair;
            }
            else if ((lowestOrbital.first + lowestOrbital.second) == (pair.first + pair.second) && lowestOrbital.first > pair.first)
            {
                lowestOrbital = pair;
            }
        }
        return lowestOrbital;
    }
    QPair<int, int> OneCenterInput::getHighestOrbital(const QStringList& orbitals)
    {
        QPair<int, int> lowestOrbital = getOrbitalQuantumNumbers(orbitals.first());
        for(int i=0; i<orbitals.size();++i)
        {
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);
            if ( (lowestOrbital.first + lowestOrbital.second) < (pair.first + pair.second) )
            {
                lowestOrbital = pair;
            }
            else if ((lowestOrbital.first + lowestOrbital.second) == (pair.first + pair.second) && lowestOrbital.first < pair.first)
            {
                lowestOrbital = pair;
            }
        }
        return lowestOrbital;
    }


    void OneCenterInput::initialize(const std::shared_ptr<const AtomicInfo> &atomicinfo)
    {
        const QStringList& orbitals = atomicinfo->getAllOrbitals();
        QPair<int, int> lowest_orbital = getLowestOrbital(orbitals);
        QPair<int, int> highest_orbital = getHighestOrbital(orbitals);

        m_z = atomicinfo->getAtom()->getNumber();

        m_max_ang = 0;
        std::array<int, maxl> max_nqn_shell;
        max_nqn_shell.fill(0);


        //build input for orbitals not specified in the input file (core orbitals)
        bool inside_orbitals = false;
        for(int i=0; i<orbital_orders.size(); ++i)
        {
            int nqn = orbital_orders[i].first;
            int lqn = orbital_orders[i].second;

            auto pair = std::make_pair(nqn, lqn);
            if (pair.second > m_max_ang )
            {
                m_max_ang = pair.second;
                if (inside_orbitals)
                    max_nqn_shell[pair.second] = pair.first;
            }
            if (inside_orbitals == false && pair.first > max_nqn_shell[pair.second])
            {
                max_nqn_shell[pair.second] = pair.first;
            }

            if (nqn == lowest_orbital.first && lqn == lowest_orbital.second)
            {
                inside_orbitals = true;
            }
            if (nqn == highest_orbital.first && lqn == highest_orbital.second)
            {
                break;
            }
        }

        for(int i=0; i<orbitals.size();++i)
        {
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);
            if (pair.second > m_max_ang )
            {
                m_max_ang = pair.second;
            }
            if (pair.first > max_nqn_shell[pair.second])
            {
                max_nqn_shell[pair.second] = pair.first;
            }
        }

        m_max_scf = 1000;
        m_functional = 3;



        for(int i=0; i<=m_max_ang; ++i)
        {
            m_conf_n[i] = 0.0;
            m_conf_r[i] = 1.0e30;

            m_num_occupied_shells[i] = max_nqn_shell[i];

            m_num_exponents[i] = 5;
            m_num_coeffs[i] = 3;

            m_exponents[i] = {{0.1, static_cast<double>(m_z)}};


            m_occupations[i].resize(m_num_occupied_shells[i]);
        }

        this->m_print_eigenvectors = false;
        this->m_broyden = true;
        this->m_broyden_factor = 0.1;



        //build input for orbitals not specified in the input file (core orbitals)
        double remaining_elecs = m_z;

        for(int i=0; i<orbital_orders.size(); ++i)
        {
            int nqn = orbital_orders[i].first;
            int lqn = orbital_orders[i].second;

            if (remaining_elecs < 0.1)
                break;

            if (lqn > m_max_ang)
            {
                continue;
            }
            if (lqn == 0)
            {
                double l_elec = std::min(2.0, remaining_elecs);
                m_occupations[lqn][nqn-1] = (std::make_pair(l_elec/2.0, l_elec/2.0));
                remaining_elecs -= l_elec;
            }
            else
            {
                double nelec = static_cast<double>(lqn*2+1)*2;
                double l_elec = std::min(nelec, remaining_elecs);
                m_occupations[lqn][nqn-1] = (std::make_pair(l_elec/2.0, l_elec/2.0));
                remaining_elecs -= l_elec;
            }

            if (nqn == highest_orbital.first && lqn == highest_orbital.second)
            {
                break;
            }
        }

        //build input for orbitals specified in the input file
        for(int i=0; i<orbitals.size();++i)
        {
            auto pair = getOrbitalQuantumNumbers(orbitals[i]);
            int nqn = pair.first;
            int lqn = pair.second;
            double occ;
            atomicinfo->getOccupation(orbitals[i], &occ);
            m_occupations[lqn][nqn-1] = std::make_pair(occ/2.0, occ/2.0);
        }

        for(int i=0; i<=m_max_ang; ++i)
        {
            m_printing_orbitals[i] = std::make_pair(m_num_occupied_shells[i], m_num_occupied_shells[i]);
            //std::make_pair(i+1,m_num_occupied_shells[i]);
        }


//        buildOrbitalInfo();
    }


    void OneCenterInput::setParameters(const std::shared_ptr<const Input::OneCenterParameter> &para)
    {
        auto l_para = std::dynamic_pointer_cast<const Input::OneCenterParameter_BCCMS>(para);
        if (l_para)
        {
            this->m_functional = l_para->functional;
            this->m_max_scf = l_para->max_scf;
            this->m_zora = l_para->zora;

            this->m_generate_exponents = l_para->automatic_exponents;
            if (m_generate_exponents)
            {
                this->m_num_exponents.fill(l_para->number_exponents);
                this->m_num_coeffs.fill(l_para->power);
                for(auto i=0; i<=m_max_ang; ++i)
                {
                    m_exponents[i][0] = l_para->minimal_component;
                }
            }
            else
            {
                this->m_num_coeffs.fill(l_para->power);
                for(auto i=0; i<=m_max_ang; ++i)
                {
                    m_exponents[i] = l_para->exponents;
                }
            }

            this->m_broyden = l_para->broyden;
            this->m_broyden_factor = l_para->broyden_factor;
        }
    }

}}}
