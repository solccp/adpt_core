#ifndef TWNONECENTERRESULT_BCCMS_H
#define TWNONECENTERRESULT_BCCMS_H

#include <QMap>
#include <QString>
#include <QByteArray>
#include <QStringList>

namespace SKBuilder{    namespace Toolchain{   namespace BCCMS
{
    //! Stores data produced by onecenter code, including orbitals,
    //! potential and density.
    class  OneCenterResult
    {
    public:
        OneCenterResult();
        virtual ~OneCenterResult();
    public:
        QStringList getAllOrbitalNames() const;
        bool contains(const QString& orbitalname);

        void loadOrbitalData(const QString &folder, const QString& orbital);
        void loadTotalPotential(const QString &folder);
        void loadDensity(const QString &folder);
        void loadCoulombPotential(const QString &folder);

        void loadOrbitalDataFile(const QString& file, const QString& orbital);
        void loadTotalPotentialFile(const QString &file);
        void loadDensityFile(const QString &file);
        void loadCoulombPotentialFile(const QString &file);
        void loadOrbitalMetaData(const QString &file, const QString& orbital);

    public:
        QByteArray getOrbitalData(const QString& orbitalname) const;
        QByteArray getOrbitalMetaData(const QString& orbitalname) const;
        QByteArray getOrbitalMomentum(const QString& orbitalname) const;
        QByteArray getDens() const;
        QByteArray getPottot() const;
        QByteArray getPotnc() const;
    private:
        QByteArray loadFile(QString filename);
        QMap<QString, QByteArray> m_orbitaldata;
        QMap<QString, QByteArray> m_orbitalmetadata;
        QMap<QString, QByteArray> m_orbitalmomentum;
        QByteArray m_totalDensity;
        QByteArray m_potnc;
        QByteArray m_pottot;
    };

}}}

#endif // TWNONECENTERRESULT_BCCMS_H
