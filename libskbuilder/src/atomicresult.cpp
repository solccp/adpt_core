#include "atomicresult.h"
#include "onecenterresult.h"

#include <QStringList>
#include <QFileInfo>

#include <QDebug>

namespace SKBuilder
{

AtomicResult::AtomicResult(const std::shared_ptr<const AtomicInfo> &info) :
    m_info(info)
{
    qDebug() << "AtomicResult::ctor " << this;
}

AtomicResult::~AtomicResult()
{
    qDebug() << "AtomicResult::dtor " << this;
}

QPair<bool, QString> AtomicResult::getOrbitalData(const QString &orbitalname) const
{
    if (m_orb_data.contains(orbitalname))
    {
        return qMakePair(true, m_orb_data[orbitalname]);
    }
    return qMakePair(false, QString());
}

QString Readfile(QString Filename)
{
    QFile mFile(Filename);
    if(!mFile.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "could not open file for read";
        return QString();
    }

    QTextStream in(&mFile);
    QString mText = in.readAll();

    mFile.close();
    return mText;
}

QStringList readFileIntoStringList(const QString& filename)
{
    QStringList list;
    if (QFileInfo(filename).exists())
    {
        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly))
        {
            return list;
        }
        QTextStream stream(&file);
        while(!stream.atEnd())
        {
            list.append(stream.readLine());
        }
    }
    return list;
}
QString readFileIntoString(const QString& filename)
{
    QStringList list = readFileIntoStringList(filename);
    //list.append("\n");
    return list.join("\n");
}

QPair<bool, QString> AtomicResult::getData(const QString &name, bool useFreeAtom) const
{
    if (name.toLower() == "coulombpotential")
    {
        if (!m_potnc.isEmpty())
            return qMakePair(true, m_potnc);
        else if (useFreeAtom)
        {
            int atomic_number = this->m_info.lock()->getAtom()->getNumber();
            QString resource_filename = QString(":/potential/potnc.%1").arg(atomic_number);
            return qMakePair(true, readFileIntoString(resource_filename));
        }
        else
        {
            return qMakePair(false, m_potnc);
        }
    }
    else if (name.toLower() == "totalpotential")
    {
        if (!m_pottot.isEmpty())
        {
            return qMakePair(true, m_pottot);
        }
        else if (useFreeAtom)
        {
            int atomic_number = this->m_info.lock()->getAtom()->getNumber();
            QString resource_filename = QString(":/potential/pottot.%1").arg(atomic_number);
            return qMakePair(true, readFileIntoString(resource_filename));
        }
        else
        {
            return qMakePair(false, m_potnc);
        }
    }
    else if (name.toLower() == "density")
    {
        if (!m_dens.isEmpty())
            return qMakePair(true, m_dens);
        else
        {
            return qMakePair(false, QString());
        }
    }
    return qMakePair(false, QString());
}

QPair<bool, QString> AtomicResult::getOrbitalMomentum(const QString &orbitalname) const
{
    if (m_orb_momentum.contains(orbitalname))
    {
        return qMakePair(true, m_orb_momentum[orbitalname]);
    }
    return qMakePair(false, QString());
}


void AtomicResult::addOrbitalResult(const QString& orbitalname, const QString& orbitaldata, const QString& orbitalmomentum)
{
    m_orb_data[orbitalname] = orbitaldata;
    m_orb_momentum[orbitalname] = orbitalmomentum;
}





//void AtomicResult::addResult(AtomicInfo::ConfiningPotentialType type,
//                             const ConfiningPotential *conf,
//                             const OneCenterResult *oc_result, const QStringList& orbitals)
//{
//    m_results.append(oc_result);
//    m_result_type.insert(oc_result, type);
//    if (type == AtomicInfo::Orbital)
//    {
//        QStringListIterator it(orbitals);
//        while(it.hasNext())
//        {
//            QString orb = it.next();
//            m_orb_result.insert(orb, oc_result);
//        }
//    }
//}

//QPair<bool, QString> AtomicResult::getData(const QString &type, const QString &argument) const
//{
//    QListIterator<const OneCenterResult*> it(m_results);
//    while(it.hasNext())
//    {
//        const OneCenterResult* result = it.next();
//        auto res = result->getData(type, argument);
//        if (res.first)
//        {
//            return res;
//        }
//    }
//    return qMakePair(false, QString());
//}

std::weak_ptr<const AtomicInfo> AtomicResult::getAtomicInfo() const
{
    return m_info;
}

void AtomicResult::addResult(const QString &name, const QString &data)
{
    if (name.toLower() == "coulombpotential")
    {
        m_potnc = data;
    }
    else if (name.toLower() == "totalpotential")
    {
        m_pottot = data;
    }
    else if (name.toLower() == "density")
    {
        m_dens = data;
    }
}



}
