
#include "twocenterresult.h"

#include <QDebug>

namespace SKBuilder
{

TwoCenterResult::TwoCenterResult(double starting_point, double interval) : m_starting_point(starting_point), m_interval(interval)
{
    qDebug() << "TwoCenterResult::ctor " << this;
}

TwoCenterResult::~TwoCenterResult()
{
    qDebug() << "TwoCenterResult::dtor " << this;
}

double TwoCenterResult::getStartPoint() const
{
    return m_starting_point;
}

double TwoCenterResult::getInterval() const
{
    return m_interval;
}

const QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>> &TwoCenterResult::getHamiltonianIntegral(int shell1, int shell2)
{
    return m_HamiltonianIntegral[qMakePair(shell1, shell2)];
}

const QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>> &TwoCenterResult::getOverlapIntegral(int shell1, int shell2)
{
    return m_overlapIntegral[qMakePair(shell1, shell2)];
}

void TwoCenterResult::addHamiltonianIntegral(int shell1, int shell2, SlaterKosterIntegralType::IntegralType type, const std::vector<double> &integral)
{
    addIntegral(m_HamiltonianIntegral, shell1, shell2, type, integral);
}

void TwoCenterResult::addOverlapIntegral(int shell1, int shell2, SlaterKosterIntegralType::IntegralType type, const std::vector<double> &integral)
{
    addIntegral(m_overlapIntegral, shell1, shell2, type, integral);
}

void TwoCenterResult::addIntegral(QHash<QPair<int, int>,
                                  QHash<SlaterKosterIntegralType::IntegralType, std::vector<double>> > &container,
                                  int shell1, int shell2, SlaterKosterIntegralType::IntegralType type,
                                  const std::vector<double> &integral)
{
    auto key = qMakePair(shell1, shell2);
    if (container.contains(key))
    {
        container[key].insert(type, integral);
    }
    else
    {
        QHash<SlaterKosterIntegralType::IntegralType, std::vector<double> > value;
        value.insert(type, integral);
        container.insert(key, value);
    }
}


}
