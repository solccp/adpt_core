#include "slaterkosterfile.h"
#include "atomicinfo.h"
#include "skbuilder/utils.h"

#include <QList>

#include <limits>


#include <QDebug>

namespace SKBuilder
{


const QString SlaterKosterIntegralType::names[SlaterKosterIntegralType::NINT] = {
    "f-f-sigma", "f-f-pi", "f-f-delta", "f-f-phi",
    "d-f-sigma", "d-f-pi", "d-f-delta",
    "d-d-sigma", "d-d-pi", "d-d-delta",
    "p-f-sigma", "p-f-pi",
    "p-d-sigma", "p-d-pi",
    "p-p-sigma", "p-p-pi",
    "s-f-sigma", "s-d-sigma", "s-p-sigma", "s-s-sigma"
};

SlaterKosterFile::SlaterKosterFile(SuperPositionType type, double start, double interval)
{
    m_start = start;
    m_interval = interval;
    m_superposition = type;
    qDebug() << "SlaterKosterFile::ctor " << this;
}

SlaterKosterFile::~SlaterKosterFile()
{
    qDebug() << "SlaterKosterFile::dtor " << this;
}

bool SlaterKosterFile::setAtomicInformation(const std::shared_ptr<const AtomicInfo> &atomic_info_1, int atom1_shell,
                                            const std::shared_ptr<const AtomicInfo> &atomic_info_2, int atom2_shell)
{
    m_atomic_info_1 = atomic_info_1;
    m_atomic_info_2 = atomic_info_2;
    m_atom1_shell = atom1_shell;
    m_atom2_shell = atom2_shell;
    m_atom1_orbitals = atomic_info_1->getShellOrbitals(atom1_shell);
    m_atom2_orbitals = atomic_info_2->getShellOrbitals(atom2_shell);
    return true;
}

void SlaterKosterFile::setDocumentation(const QString &docs)
{
    m_documentation = docs;
}

bool SlaterKosterFile::addIntegral(SlaterKosterIntegralType::IntegralType type,
                                   const std::vector<double> &hamiltonianIntegral,
                                   const std::vector<double> &overlapIntegral)
{
    if (type == SlaterKosterIntegralType::ERROR)
    {
        return false;
    }

    if (hamiltonianIntegral.size() != overlapIntegral.size())
    {
        m_lastError = "The Hamiltonian integral and overlap integral have diffierent length.";
        return false;
    }
    if (integral_length == -1)
    {
        integral_length = hamiltonianIntegral.size();
    }
    else
    {
        if (hamiltonianIntegral.size() != integral_length)
        {
            m_lastError = "The " + SlaterKosterIntegralType::toString(type) + " integral that is being added has diffierent length.";
            return false;
        }
    }


    QList<int> f_related = {0,1,2,3,4,5,6,10,11,16};
    if (f_related.contains(type))
    {
        isExtFormat = true;
    }

    m_hamiltonianIntegrals.insert(type, hamiltonianIntegral);
    m_overlapIntegrals.insert(type, overlapIntegral);

    return true;
}

bool SlaterKosterFile::writeToFile(const QDir &dir, const QString &filename) const
{
    QStringList output;

    int nempty = QString::number(m_start / m_interval,'f',0).toInt() -1;

    if (isExtFormat)
    {
        output << "@";
    }

    output << QString("%1 %2").arg(m_interval, 0,'f',6).arg(integral_length+nempty);
    QStringList header;

    auto atom_1 = m_atomic_info_1.lock()->getAtom();
    auto atom_2 = m_atomic_info_2.lock()->getAtom();


    if (atom_1->getSymbol() == atom_2->getSymbol() && m_atom1_orbitals == m_atom2_orbitals)
    {

        QMap<int, double> mapping_oe;
        QMap<int, double> mapping_hubb;
        QMap<int, double> mapping_occ;

        QListIterator<QString> it(m_atom1_orbitals);
        while(it.hasNext())
        {
            QString orbname = it.next();
            double oe;
            double hubb;
            double occ;

            int l = getOrbitalLQuantumNumber(orbname);
            if (!m_atomic_info_1.lock()->getOrbitalEnergy(orbname, &oe))
            {
                return false;
            }
            if (!m_atomic_info_1.lock()->getHubburd(orbname, &hubb))
            {
                return false;
            }
            if (!m_atomic_info_1.lock()->getFinalOccupation(orbname, &occ))
            {
                if (!m_atomic_info_1.lock()->getOccupation(orbname, &occ))
                    return false;
            }
            mapping_oe.insert(l, oe);
            mapping_hubb.insert(l, hubb);
            mapping_occ.insert(l, occ);
        }
        for(int i=3; i>=0; --i)
        {
            if (!isExtFormat && i == 3)
            {
                continue;
            }
            if (mapping_oe.contains(i))
            {
                header << QString::number(mapping_oe[i], 'f', 12);
            }
            else
            {
                header << "0.0";
            }
        }
        header << "0.0";
        for(int i=3; i>=0; --i)
        {
            if (!isExtFormat && i == 3)
            {
                continue;
            }
            if (mapping_hubb.contains(i))
            {
                header << QString::number(mapping_hubb[i], 'f', 12);
            }
            else
            {
                header << "0.0";
            }
        }
        for(int i=3; i>=0; --i)
        {
            if (!isExtFormat && i == 3)
            {
                continue;
            }
            if (mapping_occ.contains(i))
            {
                header << QString::number(mapping_occ[i], 'f', 12);
            }
            else
            {
                header << "0.0";
            }
        }
        output << header.join(" ");

        header.clear();
        header << QString::number(atom_1->getMass());
        header << "19*0.0";

        output << header.join(" ");


    }
    else
    {
        output << "10*0.0 10*1.0";
    }
    for(int i=0; i < nempty; ++i)
    {
        if (isExtFormat)
        {
            output << "20*0.0 20*1.0";
        }
        else
        {
            output << "10*0.0 10*1.0";
        }
    }

    QList<int> indexes;
    if (isExtFormat)
    {
        indexes = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    }
    else
    {
        indexes = {7,8,9,12,13,14,15,17,18,19};
    }

    for(int i=0; i<integral_length; ++i)
    {
        QStringList line;
        int n_null_integral = 0;
        for(int j=0; j<indexes.size();++j)
        {
            if (m_hamiltonianIntegrals.contains(SlaterKosterIntegralType::IntegralType(indexes[j])))
            {
                if (n_null_integral > 0)
                {
                    if (n_null_integral == 1)
                    {
                        line << "0.0";
                    }
                    else
                    {
                        line << QString("%1*0.0").arg(n_null_integral);
                    }
                    n_null_integral = 0;
                }
                double value = m_hamiltonianIntegrals[SlaterKosterIntegralType::IntegralType(indexes[j])].at(i);
//                if (std::abs(value) < std::numeric_limits<double>::epsilon())
//                    value = 0.0;
                line << QString("%1").arg(value,20,'E',12);
            }
            else
            {
                ++n_null_integral;
            }
        }
        if (n_null_integral > 0)
        {
            line << QString("%1*0.0").arg(n_null_integral);
            n_null_integral = 0;
        }

        n_null_integral = 0;
        for(int j=0; j<indexes.size();++j)
        {
            if (m_overlapIntegrals.contains(SlaterKosterIntegralType::IntegralType(indexes[j])))
            {
                if (n_null_integral > 0)
                {
                    if (n_null_integral == 1)
                    {
                        line << "0.0";
                    }
                    else
                    {
                        line << QString("%1*0.0").arg(n_null_integral);
                    }
                    n_null_integral = 0;
                }
                double value = m_overlapIntegrals[SlaterKosterIntegralType::IntegralType(indexes[j])].at(i);
//                if (std::abs(value) < std::numeric_limits<double>::epsilon())
//                    value = 0.0;
                line << QString("%1").arg(value,20,'E',12);
            }
            else
            {
                ++n_null_integral;
            }
        }
        if (n_null_integral > 0)
        {
            line << QString("%1*0.0").arg(n_null_integral);
            n_null_integral = 0;
        }
        output << line.join(" ");
    }

    if (m_repulsive.isEmpty())
    {
        output << "Spline" << "1 2.0" << "0.0 0.0 0.0" << "0.2 2.0 0.0 0.0 0.0 0.0 0.0 0.0";
    }
    else
    {
        output << m_repulsive;
    }
    if (!m_documentation.isEmpty())
    {
        output << m_documentation;
    }
    return writeFile(dir.absoluteFilePath(filename), output.join("\n"));
}

bool SlaterKosterFile::writeToFile(const QDir &dir, bool write_superpositiontype) const
{
    QString filename = getFilename(write_superpositiontype);
    return writeToFile(dir, filename);
}

const QString &SlaterKosterFile::getLastError()
{
    return m_lastError ;
}
QString SlaterKosterFile::suffix() const
{
    return m_suffix;
}

void SlaterKosterFile::setSuffix(const QString &suffix)
{
    m_suffix = suffix;
}
QString SlaterKosterFile::prefix() const
{
    return m_prefix;
}

void SlaterKosterFile::setPrefix(const QString &prefix)
{
    m_prefix = prefix;
}

QString SlaterKosterFile::getFilename(bool write_superpositiontype) const
{
    QString superposition = "";
    if (write_superpositiontype)
    {
        superposition = getSuperpositionString();
    }

    QString filename_atom1 = QString("%1_%2").arg(m_atomic_info_1.lock()->getAtom()->getSymbol()).arg(m_atom1_orbitals.join(""));
    QString filename_atom2 = QString("%1_%2").arg(m_atomic_info_2.lock()->getAtom()->getSymbol()).arg(m_atom2_orbitals.join(""));


    if (m_atom1_shell == 0)
    {
        filename_atom1 = QString("%1").arg(m_atomic_info_1.lock()->getAtom()->getSymbol());
    }
    if (m_atom2_shell == 0)
    {
        filename_atom2 = QString("%1").arg(m_atomic_info_2.lock()->getAtom()->getSymbol());
    }

    QString filename = QString("%1%5%2-%3%4").arg(m_prefix)
            .arg(filename_atom1)
            .arg(filename_atom2)
            .arg(m_suffix).arg(superposition);

    return filename;
}
QString SlaterKosterFile::getRepulsive() const
{
    return m_repulsive;
}

void SlaterKosterFile::setRepulsive(const QString &repulsive)
{
    m_repulsive = repulsive;
}
int SlaterKosterFile::atom1_shell() const
{
    return m_atom1_shell;
}
int SlaterKosterFile::atom2_shell() const
{
    return m_atom2_shell;
}

SlaterKosterFile::SuperPositionType SlaterKosterFile::getSuperPositionType() const
{
    return m_superposition;
}

QString SlaterKosterFile::getSuperpositionString() const
{
    if (m_superposition == PotentialSuperposition)
    {
        return "PotentialSuperposition_";
    }
    else
    {
        return "DensitySuperposition_";
    }
}
QStringList SlaterKosterFile::atom2_orbitals() const
{
    return m_atom2_orbitals;
}

QString SlaterKosterFile::atom1_selectedShell() const
{
    return orbitalList2ShellString(atom1_orbitals());
}

QString SlaterKosterFile::atom2_selectedShell() const
{
    return orbitalList2ShellString(atom2_orbitals());
}



double SlaterKosterFile::getAtomEnergy() const
{
    double res = 0.0;
    if (m_atomic_info_1.lock()->getAtom()->getSymbol() == m_atomic_info_2.lock()->getAtom()->getSymbol()
            && m_atom1_shell == m_atom2_shell)
    {
        QStringList orbs = m_atomic_info_1.lock()->getAllOrbitals();
        for(int i=0; i<orbs.size();++i)
        {
            double occ = 0.0;
            m_atomic_info_1.lock()->getOccupation(orbs[i], &occ);
            double ene = 0.0;
            m_atomic_info_1.lock()->getOrbitalEnergy(orbs[i], &ene);
            res += ene*occ;
        }

    }
    return res;
}

QString SlaterKosterFile::getDocumentation() const
{
    return m_documentation;
}


QString SlaterKosterFile::orbitalList2ShellString(const QStringList &orbitals) const
{
    QMap<int,bool> lnum;
    QStringListIterator it(orbitals);
    while(it.hasNext())
    {
        const QString& str = it.next();
        int l = getOrbitalLQuantumNumber(str);
        lnum.insert(l, true);
    }

    QString res;
    const QString orbs = "spdfghijkl";
    QMapIterator<int,bool> it_l(lnum);
    while(it_l.hasNext())
    {
        it_l.next();
        int l = it_l.key();
        res.append(orbs[l]);
    }
    return res;
}

QStringList SlaterKosterFile::atom1_orbitals() const
{
    return m_atom1_orbitals;
}


std::weak_ptr<const AtomicInfo> SlaterKosterFile::atomic_info_2() const
{
    return m_atomic_info_2;
}

std::weak_ptr<const AtomicInfo> SlaterKosterFile::atomic_info_1() const
{
    return m_atomic_info_1;
}








}
