#include "atomicinfo.h"

#include <QSet>
#include <array>
#include "skbuilder/utils.h"

#include <QDebug>



namespace SKBuilder
{

AtomicInfo::AtomicInfo(const ADPT::AtomicProperties* atom) : m_atom(atom)
{
    qDebug() << "AtomicInfo::ctor " << this;
}

AtomicInfo::~AtomicInfo()
{
    qDebug() << "AtomicInfo::dtor " << this;
}


bool AtomicInfo::addShellOrbitals(const QStringList &orbitals)
{
//    QStringList oc_orbitals = m_onecenter_input->getAllOrbitalNames();

    std::array<bool, 4> L_moment;
    L_moment.fill(false);

    QStringListIterator it(orbitals);
    while(it.hasNext())
    {
        QString orb = it.next();
//        if (!oc_orbitals.contains(orb))
//        {
//            m_errorMsg = "Orbital " + orb + " is not found in the onecenter input.";
//            return false;
//        }
        int l = getOrbitalLQuantumNumber(orb);
        QString lorb = "spdfghijkl";
        if (L_moment[l])
        {
            m_errorMsg = QString("ERROR: There are more than one ") + QString(lorb.at(l)) + QString(" orbital in the same valence shell.");
            return false;
        }
        else
        {
            L_moment[l] = true;
        }

    }
    m_shell_orbitals.append(orbitals);
    return true;
}

void AtomicInfo::clearShellOrbitals()
{
    m_shell_orbitals.clear();
}

int AtomicInfo::getShellFromOribital(const QString &orbital) const
{
    for(int i=0; i<m_shell_orbitals.size();++i)
    {
        const QStringList& orbs = m_shell_orbitals.at(i);
        if (orbs.contains(orbital))
        {
            return i;
        }
    }
    return -1;
}

QStringList AtomicInfo::getShellOrbitals(int shell) const
{
    if (shell>-1 && shell < m_shell_orbitals.size())
    {       
        return m_shell_orbitals.at(shell);
    }
    return QStringList();
}

int AtomicInfo::getNumberOfShells() const
{
    return m_shell_orbitals.size();
}

QList<QStringList> AtomicInfo::getShellOrbitals() const
{
    return m_shell_orbitals;
}

bool AtomicInfo::setHubburd(const QString &orb, double value)
{
    m_hubburd.insert(orb, value);
    return true;
}

bool AtomicInfo::getHubburd(const QString &orb, double *value) const
{
    if (!m_hubburd.contains(orb))
    {
        return false;
    }
    *value = m_hubburd[orb];
    return true;
}

bool AtomicInfo::setOrbitalEnergy(const QString &orb, double value)
{
    m_orbitalEnergy.insert(orb, value);
    return true;
}

bool AtomicInfo::getOrbitalEnergy(const QString &orb, double *energy) const
{
    if (!m_orbitalEnergy.contains(orb))
    {
        return false;
    }
    *energy = m_orbitalEnergy[orb];
    return true;
}

bool AtomicInfo::setOccupation(const QString &orb, double value)
{
    m_occupation.insert(orb, value);
    return true;
}

bool AtomicInfo::getOccupation(const QString &orb, double *occ) const
{
    if (!m_occupation.contains(orb))
    {
        return false;
    }
    *occ = m_occupation[orb];
    return true;
}

bool AtomicInfo::setFinalOccupation(const QString &orb, double value)
{
    m_final_occupation.insert(orb, value);
    return true;
}

bool AtomicInfo::getFinalOccupation(const QString &orb, double *occ) const
{
    if (!m_final_occupation.contains(orb))
    {
        return false;
    }
    *occ = m_final_occupation[orb];
    return true;
}



const ADPT::AtomicProperties *AtomicInfo::getAtom() const
{
    return m_atom;
}

QString AtomicInfo::getLastError() const
{
    return m_errorMsg;
}

QStringList AtomicInfo::getAllOrbitals() const
{
    QListIterator<QStringList> it(m_shell_orbitals);
    QStringList res;
    while(it.hasNext())
    {
        res.append(it.next());
    }
    res.removeDuplicates();
    return res;
}

QString AtomicInfo::getTemplate_onecenter_inputfile() const
{
    return template_onecenter_inputfile;
}

void AtomicInfo::setTemplate_onecenter_inputfile(const QString &value)
{
    template_onecenter_inputfile = value;
}

void AtomicInfo::setOrbitalFiles(const QMap<QString, QString> &orbital_files)
{
    this->m_orbital_files = orbital_files;
}

const QMap<QString, QString> AtomicInfo::orbitalFiles() const
{
    return m_orbital_files;
}

void AtomicInfo::setPotentialFile(const QString &potential_file)
{
    this->m_potential_file = potential_file;
}

QString AtomicInfo::potentialFile() const
{
    return this->m_potential_file;
}

void AtomicInfo::setDensityFile(const QString &density_file)
{
    this->m_density_file = density_file;
}

QString AtomicInfo::densityFile() const
{
    return this->m_density_file;
}




}
