#include "onecenterresult.h"
#include <QDebug>

namespace SKBuilder
{


OneCenterResult::OneCenterResult(std::shared_ptr<const OneCenterInput> oc_info)
{
    m_oc_info = oc_info;
//    qDebug() << "OneCenterResult::ctor " << this;
}

OneCenterResult::~OneCenterResult()
{
//    qDebug() << "OneCenterResult::dtor " << this;
}


}
