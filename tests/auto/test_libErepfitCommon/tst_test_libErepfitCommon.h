#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <QString>
#include "erepfit2_inputdata.h"

using namespace testing;

TEST(Geometry, loadgen)
{
    Erepfit2::Input::Geometry geometry;
    ASSERT_TRUE(geometry.loadGen(":/test_data/test_molecule.gen"));
    ASSERT_EQ(3, geometry.getAtoms().size());

    ASSERT_FALSE(geometry.isPBC);
    auto vec = geometry.getCoordinate(0);
    ASSERT_DOUBLE_EQ(0.0, vec[0]);
    ASSERT_DOUBLE_EQ(0.0, vec[1]);
    ASSERT_DOUBLE_EQ(0.1173, vec[2]);
    vec = geometry.getCoordinate(1);
    ASSERT_DOUBLE_EQ(0.0, vec[0]);
    ASSERT_DOUBLE_EQ(0.7572, vec[1]);
    ASSERT_DOUBLE_EQ(-0.4692, vec[2]);
    vec = geometry.getCoordinate(2);
    ASSERT_DOUBLE_EQ(0.0, vec[0]);
    ASSERT_DOUBLE_EQ(-0.7572, vec[1]);
    ASSERT_DOUBLE_EQ(-0.4692, vec[2]);

    ASSERT_FALSE(geometry.spin_polarization);
    ASSERT_EQ(1, geometry.spin);

    geometry.spin_polarization = true;

    auto atoms = QStringList() << "O" << "H" ;
    ASSERT_EQ(atoms, geometry.getElements());
    atoms << "H";
    ASSERT_EQ(atoms, geometry.getAtoms());


    ASSERT_TRUE(geometry.loadGen(":/test_data/test_crystal_fract.gen"));

    ASSERT_TRUE(geometry.isPBC);
    ASSERT_TRUE(geometry.fractional_coordinates);
    ASSERT_FALSE(geometry.spin_polarization);

    ASSERT_TRUE(geometry.loadGen(":/test_data/test_crystal_cart.gen"));
    ASSERT_TRUE(geometry.isPBC);
    ASSERT_FALSE(geometry.fractional_coordinates);
    ASSERT_FALSE(geometry.spin_polarization);

}

TEST(Geometry, getDCDFTBKXYZ)
{
    Erepfit2::Input::Geometry geometry;

    ASSERT_TRUE(geometry.loadGen(":/test_data/test_molecule.gen"));
    ASSERT_STREQ("3 0 1\n"
                 "O      0.00000000     0.00000000     0.11730000\n"
                 "H      0.00000000     0.75720000    -0.46920000\n"
                 "H      0.00000000    -0.75720000    -0.46920000\n\n",
                 geometry.getDCDFTBKXYZ().toStdString().c_str());


    ASSERT_TRUE(geometry.loadGen(":/test_data/test_crystal_fract.gen"));
    ASSERT_STREQ("2 0 1\n"
                 "Cu     0.00000000     0.00000000     0.00000000\n"
                 "Cu     1.25883441     0.72678838     2.06965664\n"
                 "TV     1.25883441    -2.18036515     0.00000000\n"
                 "TV     1.25883441     2.18036515     0.00000000\n"
                 "TV     0.00000000     0.00000000     4.13931329\n\n",
                 geometry.getDCDFTBKXYZ().toStdString().c_str());

    ASSERT_TRUE(geometry.loadGen(":/test_data/test_crystal_cart.gen"));


    ASSERT_STREQ("2 0 1\n"
                 "Cu     0.00000000     0.00000000     0.00000000\n"
                 "Cu     1.25883441     0.72678838     2.06965664\n"
                 "TV     1.25883441    -2.18036515     0.00000000\n"
                 "TV     1.25883441     2.18036515     0.00000000\n"
                 "TV     0.00000000     0.00000000     4.13931329\n\n",
                 geometry.getDCDFTBKXYZ().toStdString().c_str());
}

