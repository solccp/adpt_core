QT       += core

QT       -= gui

TARGET = erepfit_solver
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app
GIT_VERSION = $$system(git --work-tree ../ describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

SOURCES += main.cpp

INCLUDEPATH += $$PWD/../external_libs/Eigen
INCLUDEPATH += $$PWD/../external_libs/include
LIBS += $$PWD/../external_libs/lib/libVariant.a $$PWD/../external_libs/lib/libyaml.a


RESOURCES += \
    erepfit_solver.qrc


include(../settings.pri)
unix {
    target.path = $$PREFIX/adpt/bin
    INSTALLS += target
}



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../liberepfit/release/ -lerepfit
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../liberepfit/debug/ -lerepfit
else:unix: LIBS += -L$$OUT_PWD/../liberepfit/ -lerepfit

INCLUDEPATH += $$PWD/../liberepfit
DEPENDPATH += $$PWD/../liberepfit

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/release/liberepfit.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/debug/liberepfit.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/release/erepfit.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/debug/erepfit.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/liberepfit.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

HEADERS += \
    TextSKFile.h

MKL_PATH = $$(MKL_ROOT)
!isEmpty(MKL_PATH) {
LIBS +=-L$$(MKL_ROOT)/lib/intel64 -lmkl_rt -lpthread -lm
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../liberepfit_evaluator/release/ -lerepfit_evaluator
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../liberepfit_evaluator/debug/ -lerepfit_evaluator
else:unix: LIBS += -L$$OUT_PWD/../liberepfit_evaluator/ -lerepfit_evaluator

INCLUDEPATH += $$PWD/../liberepfit_evaluator
DEPENDPATH += $$PWD/../liberepfit_evaluator

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/release/liberepfit_evaluator.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/debug/liberepfit_evaluator.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/release/erepfit_evaluator.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/debug/erepfit_evaluator.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/liberepfit_evaluator.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
