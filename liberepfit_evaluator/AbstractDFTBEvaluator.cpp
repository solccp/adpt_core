#include "AbstractDFTBEvaluator.h"
#include "basic_types.h"


namespace Erepfit2
{
    AbstractDFTBEvaluator::AbstractDFTBEvaluator(int evaluation_timeout)
    {
        timeout = evaluation_timeout;
    }

    AbstractDFTBEvaluator::~AbstractDFTBEvaluator()
    {

    }

    void AbstractDFTBEvaluator::cropElectronicSKFile(const QString& src, const QString& dest, QTextStream& os)
    {
        QFile src_file(src);
        QFile dest_file(dest);

        if (src_file.open(QIODevice::Text | QIODevice::ReadOnly))
        {
             if (dest_file.open(QIODevice::Text | QIODevice::WriteOnly))
             {
                QTextStream src_stream(&src_file);
                QTextStream dest_stream(&dest_file);

                while (!src_stream.atEnd())
                {
                    QString line = src_stream.readLine();
                    if (line.startsWith("Spline"))
                    {
                        dest_stream << "Spline" << endl
                                    << "1 12.0" << endl
                                    << "0.0 0.0 0.0" << endl
                                    << "0.4 12.0 0.0 0.0 0.0 0.0 0.0 0.0" << endl;
                        break;
                    }
                    else
                    {
                        dest_stream << line << endl;
                        os << line << endl;
                    }
                }
             }
        }
        else
        {
            throw std::runtime_error("Cannot open file: " + src.toStdString());
        }
    }


    QStringList AbstractDFTBEvaluator::writeTempElecPart(const QString &scratch, const QString &elem1, const QString &elem2,
                                                 const ADPT::SKFileInfo* skfileinfo, QTextStream& os)
    {
        QDir dir(scratch);
        QString final_elem1 = elem1;
        QString final_elem2 = elem2;
        QStringList res;

        if (skfileinfo->type2Names)
        {
            if (skfileinfo->lowercase)
            {
                final_elem1 = elem1.toLower();
                final_elem2 = elem2.toLower();
            }
            QFileInfo info(QString("%1%2%3%4%5")
                           .arg(skfileinfo->prefix)
                           .arg(final_elem1).arg(skfileinfo->separator)
                           .arg(final_elem2).arg(skfileinfo->suffix));
            if (info.exists())
            {
                QString final_filename = dir.absoluteFilePath(QString("%1-%2_elec.skf").arg(elem1).arg(elem2));
                cropElectronicSKFile(info.absoluteFilePath(), final_filename, os);
                res.append(dir.relativeFilePath(final_filename));
            }
        }
        else
        {
            QMapIterator<QString, QStringList> it(skfileinfo->skfiles);
            while(it.hasNext())
            {
                it.next();
                auto poten = ADPT::PotentialName::fromString(it.key());
                if (elem1 == poten.getElement1() && elem2 == poten.getElement2() )
                {
                    for(auto const& item : it.value())
                    {
                        QFileInfo info(item);
                        QString final_filename = dir.absoluteFilePath(QString("%1_elec.skf").arg(info.baseName()));
                        cropElectronicSKFile(info.absoluteFilePath(), final_filename, os);
                        res.append(dir.relativeFilePath(final_filename));
                    }
                }
            }
        }

        if (res.isEmpty())
        {
            throw std::runtime_error(QString("Cannot find potential %1-%2").arg(elem1).arg(elem2).toStdString());
        }

        return res;
    }


}
