#ifndef OPTIONALEVLUATIONINFORMATION_H
#define OPTIONALEVLUATIONINFORMATION_H

#include <QMap>
#include <QString>
#include <QVariant>



namespace Erepfit2
{
    class OptionalEvaluationInformation
    {
        using dVector3 = std::array<double, 3> ;
    public:
        QMap<QString, dVector3> hubbardDerivatives;
        bool orbital_resolved_scc = false;
        bool shell_resolved_spin = false;
        QMap<QString, QVariantList> spinConstants;
        bool useDampingFactor = false;
        double dampingFactor;
        bool use_cache = false;
        QVariantMap dispersion;
    };
}

#endif // OPTIONALEVLUATIONINFORMATION_H
