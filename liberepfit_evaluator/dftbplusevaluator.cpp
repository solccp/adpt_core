#include "dftbplusevaluator.h"
#include "basic_types.h"

#include "newuoa.h"

#include "Singleton.h"

#include "electronicpartcachedatabase.h"
#include "erepfit2_inputdata.h"

#include "io_utils.h"
#include "optionalevluationinformation.h"
#include "process_utils.h"

#include <QCryptographicHash>
#include <QDataStream>
#include <QStringBuilder>

namespace Erepfit2
{

template <class F>
NewuoaClosure make_closure(F& function)
{
    struct Wrap
    {
        static double call(void* data, long n, const double* values)
        {
            return reinterpret_cast<F*>(data)->operator()(n, values);
        }
    };
    return NewuoaClosure{ &function, &Wrap::call };
}

DFTBPlusEvaluator::DFTBPlusEvaluator(QTextStream& output_stream, const QString& dftb_path, int evaluation_timeout)
    : AbstractDFTBEvaluator(evaluation_timeout), m_output_stream(output_stream)
    , m_dftb_path(dftb_path)
{
}

QList<QPair<QString, QString>> DFTBPlusEvaluator::getRequiredElecPairs(const Erepfit2::Input::System* system)
{
    QStringList elemts = system->geometry.getElements();
    elemts.removeDuplicates();
    QList<QPair<QString, QString>> res;
    for (int i = 0; i < elemts.size(); ++i)
    {
        for (int j = 0; j < elemts.size(); ++j)
        {
            res.append(qMakePair(elemts.at(i), elemts.at(j)));
        }
    }
    return res;
}

///
/// \brief DFTBPlusEvaluator::writeSKFilesInfo
/// Write SKFiles information in the input file
/// \param pairs All required pairs
/// \param sha_stream
/// \param tempFolder
/// \param skfileinfo
/// \param final_hsd
///
void DFTBPlusEvaluator::writeSKFilesInfo(const QList<QPair<QString, QString>>& pairs, QTextStream& sha_stream, QDir tempFolder, const ADPT::SKFileInfo* skfileinfo, QTextStream& final_hsd)
{
    QStringList elements;
    for ( auto const & pair : pairs)
    {
        elements.append(pair.first);
        elements.append(pair.second);
    }
    elements.removeDuplicates();

    if (skfileinfo->type2Names)
    {
        final_hsd << "+Hamiltonian = +DFTB {" << endl;
        if (!skfileinfo->maxAngularMomentum.isEmpty())
        {
            final_hsd << "    !MaxAngularMomentum = {" << endl;
            QMapIterator<QString, QString> it(skfileinfo->maxAngularMomentum);
            while (it.hasNext())
            {
                it.next();
                if (elements.contains(it.key()))
                {
                    final_hsd << "    " << it.key() << " = "
                          << "\"" << it.value() << "\"" << endl;
                }
            }
            final_hsd << "    }" << endl;
        }

        final_hsd << "    !SlaterKosterFiles = {" << endl;
        for (int i = 0; i < pairs.size(); ++i)
        {
            auto skfiles = writeTempElecPart(tempFolder.absolutePath(), pairs[i].first, pairs[i].second, skfileinfo, sha_stream);
            final_hsd << "    " << QString("%1-%2 = %3").arg(pairs[i].first).arg(pairs[i].second).arg(skfiles.join(" ")) << endl;
        }

        final_hsd << "    }" << endl;
        final_hsd << "}" << endl;
    }
    else
    {
        final_hsd << "+Hamiltonian = +DFTB {" << endl;
        if (!skfileinfo->selectedShells.isEmpty())
        {
            final_hsd << "    !MaxAngularMomentum = {" << endl;
            QMapIterator<QString, QStringList> it(skfileinfo->selectedShells);
            while (it.hasNext())
            {
                it.next();
                final_hsd << it.key() << " = "
                          << "SelectedShells{" << it.value().join(" ") << "}" << endl;
            }
            final_hsd << "    }" << endl;
        }

        final_hsd << "    !SlaterKosterFiles = {" << endl;
        for (int i = 0; i < pairs.size(); ++i)
        {
            auto skfiles = writeTempElecPart(tempFolder.absolutePath(), pairs[i].first, pairs[i].second, skfileinfo, sha_stream);
            final_hsd << QString("%1-%2 = %3").arg(pairs[i].first).arg(pairs[i].second).arg(skfiles.join(" ")) << endl;
        }
        final_hsd << "    }" << endl;
        final_hsd << "}" << endl; //Hamiltonian
    }
}

void DFTBPlusEvaluator::writeInput(QTextStream& final_hsd, QTextStream& sha_stream, const Erepfit2::OptionalEvaluationInformation* opt_info,
    const ADPT::SKFileInfo* skfileinfo, QDir tempFolder,
    const Erepfit2::Input::System* system)
{
    final_hsd << "#" << system->name << endl;
    final_hsd << QString("!Geometry = !GenFormat{") << endl;
    final_hsd << system->geometry.getGen();
    final_hsd << "}" << endl;

    QList<QPair<QString, QString>> pairs = getRequiredElecPairs(system);
    writeSKFilesInfo(pairs, sha_stream, tempFolder, skfileinfo, final_hsd);

    final_hsd << QString("*Options = {") << endl
              << "    !WriteAutotestTag = Yes" << endl
              << "}" << endl;

    final_hsd << QString("*Analysis = {") << endl
              << "    !CalculateForces = Yes" << endl
              << "}" << endl;

    if (!opt_info->hubbardDerivatives.isEmpty())
    {
        final_hsd << QString("+Hamiltonian = +DFTB{") << endl;

        if (opt_info->orbital_resolved_scc)
        {
            final_hsd << "    !OrbitalResolvedSCC = Yes" << endl;
        }
        else
        {
            final_hsd << "    !OrbitalResolvedSCC = No" << endl;
        }

        final_hsd << "    *HubbardDerivs = {" << endl;

        QMapIterator<QString, dVector3> it(opt_info->hubbardDerivatives);

        if (opt_info->orbital_resolved_scc)
        {
            while (it.hasNext())
            {
                it.next();
                int maxL = skfileinfo->getMaxAngularMomentum(it.key());
                final_hsd << "        !" << it.key() << " = " ;

                for(int i=0; i<=maxL; ++i)
                {
                    final_hsd << it.value()[i] << " " ;
                }
                final_hsd << endl;
            }
        }
        else
        {
            while (it.hasNext())
            {
                it.next();
                final_hsd << "        !" << it.key() << " = " << it.value()[2] << endl;
            }
        }
        final_hsd << "    }" << endl;
        final_hsd << "}" << endl;
    }

    if (!opt_info->dispersion.empty())
    {
        if (opt_info->dispersion["type"] == "lj")
        {
            final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                      << "    *Dispersion = *LennardJones{" << endl
                      << "      *Parameters {" << endl;

            auto ljlist = opt_info->dispersion["parameters"].toMap();

            QMapIterator<QString, QVariant> it(ljlist);
            while (it.hasNext())
            {
                it.next();
                auto keyvalue = it.value().toMap();
                double dis = keyvalue["distance"].toDouble();
                double energy = keyvalue["distance"].toDouble();
                final_hsd << "          *" << it.key() << " {" << endl
                          << "              *Distance [AA] = " << dis << endl
                          << "              *Energy [kcal/mol] = " << energy << endl
                          << "          }" << endl;
            }
            final_hsd << "          }"
                      << "      }"
                      << "}" << endl;
        }
        else if (opt_info->dispersion["type"] == "d3")
        {
            auto param = opt_info->dispersion["parameters"].toMap();
            final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                      << "    *Dispersion = *DftD3{" << endl
                      << "       /s6 = " << param["s6"].toDouble() << endl
                      << "       /s8 = " << param["s8"].toDouble() << endl;

            if (param["damping"].toString() == "bj")
            {
                final_hsd << "       *Damping = *BeckeJohnson{" << endl
                          << "         /a1 = " << param["a1"].toDouble() << endl
                          << "         /a2 = " << param["a2"].toDouble() << endl
                          << "       }" << endl;
            }
            else if (param["damping"].toString() == "zero")
            {
                final_hsd << "       *Damping = *ZeroDamping{" << endl
                          << "         /sr6 = " << param["sr6"].toDouble() << endl
                          << "         /alpha6 = " << param["alpha6"].toDouble() << endl
                          << "       }" << endl;
            }
            final_hsd << "    }" << endl
                      << "}" << endl;
        }
    }


    if (opt_info->useDampingFactor)
    {
        final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                  << "    !DampXH = Yes" << endl
                  << "    !DampXHExponent =" << opt_info->dampingFactor << endl;
        final_hsd << "}" << endl;
    }

    if (system->geometry.isPBC)
    {
        final_hsd << QString("+Hamiltonian = +DFTB{") << endl;
        if (system->geometry.kpoints.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
        {
            final_hsd << "    !KPointsAndWeights = !SupercellFolding{" << endl;
            for (int i = 0; i < 3; ++i)
            {
                final_hsd << "      ";
                for (int j = 0; j < 3; ++j)
                {
                    final_hsd << system->geometry.kpoints.m_kpoints_supercell.num_of_cells[i][j] << " ";
                }
                final_hsd << endl;
            }
            for (int i = 0; i < 3; ++i)
            {
                final_hsd << " " << system->geometry.kpoints.m_kpoints_supercell.shifts[i] << " ";
            }
            final_hsd << endl;
        }

        final_hsd << "    }" << endl;
        final_hsd << "}" << endl;
    }
    else
    {
        final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                  << "    !Charge = " << system->geometry.charge << endl
                  << "}" << endl;
    }

    final_hsd << "!ParserOptions {" << endl
              << "    !IgnoreUnprocessedNodes = Yes" << endl
              << "}" << endl;
}

void DFTBPlusEvaluator::writeInputSpinPolarization(QTextStream& final_hsd, QTextStream& sha_stream, const OptionalEvaluationInformation* opt_info, const Input::System* system, double crystal_upe)
{
    if (system->geometry.spin_polarization)
    {
        double upe = 0.0;
        if (system->geometry.isPBC)
        {
            upe = crystal_upe;
        }
        else
        {
            upe = (system->geometry.spin - 1.0);
        }

        final_hsd << "+Hamiltonian = +DFTB {" << endl
                  << "    !SpinPolarisation = !Colinear {" << endl
                  << "         !UnpairedElectrons = " << upe << endl
                  << "    }" << endl;
        final_hsd << "!SCC = Yes" << endl
                  << "!SCCTolerance = 1.0e-8" << endl
                  << "    *SpinConstants = {" << endl;
        if (opt_info->shell_resolved_spin)
        {
            final_hsd   << "       ShellResolvedSpin = Yes" << endl;
        }
        else
        {
            final_hsd   << "       ShellResolvedSpin = No" << endl;
        }

        QMapIterator<QString, QVariantList> it(opt_info->spinConstants);
        while (it.hasNext())
        {
            it.next();
            final_hsd << "        !" << it.key() << " = ";
            for (auto& value : it.value())
            {
                final_hsd << " " << value.toDouble();
            }
            final_hsd << endl;
        }
        final_hsd << "    }" << endl;
        final_hsd << "}" << endl;
    }
}

void parseDFTBFermiEnergies(const QString& filename, double& efermi_a,
    double& efermi_b)
{
    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        throw std::runtime_error("Cannot find file: " + filename.toStdString());
    }
    QTextStream fin(&detailed);
    int spin_stage = 0;

    bool scc_converged = false;
    bool run_scc = false;

    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Fermi energy:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;

            double efermi = tokens.at(4).toDouble(&ok);
            if (!ok)
            {
                throw std::runtime_error("Cannot read fermi energy from file");
            }
            else
            {
                if (std::isnan(efermi))
                {
                    throw std::runtime_error("Cannot read fermi energy from file");
                }
            }
            if (spin_stage == 0)
            {
                efermi_a = efermi;
            }
            else
            {
                efermi_b = efermi;
            }

            spin_stage += 1;
        }
        else if (str.contains("Self consistent charges:"))
        {
            run_scc = true;
        }
        else if (str.contains("SCC converged"))
        {
            scc_converged = true;
        }
    }
    if (run_scc && !scc_converged)
    {
        throw std::runtime_error("SCC not converged");
    }
}

///
/// \brief parseDFTBEnergy
/// Read the total energy from the output file (filename)
/// \param filename
/// \return
///
double parseDFTBEnergy(const QString& filename)
{
    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        throw std::runtime_error("Cannot find file: " + filename.toStdString());
    }
    QTextStream fin(&detailed);

    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Total energy:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;

            double energy = tokens.at(2).toDouble(&ok);
            if (!ok)
            {
                throw std::runtime_error("Cannot read energy from file");
            }
            else
            {
                if (std::isnan(energy))
                {
                    throw std::runtime_error("Cannot read energy from file");
                }
            }
            return energy;
        }
    }
    throw std::runtime_error("Cannot read energy from file");
}



Erepfit2::Input::SystemElectronicData DFTBPlusEvaluator::evaluate(
    QDir tempFolder,
    const ADPT::SKFileInfo* skfileinfo,
    const Erepfit2::Input::System* system,
    const Erepfit2::OptionalEvaluationInformation* opt_info)
{
    using MatrixX3d = Eigen::Matrix<double, Eigen::Dynamic, 3>;

    ElectronicPartCacheDatabase& db = Singleton<ElectronicPartCacheDatabase>::Instance();

    Erepfit2::Input::SystemElectronicData evaluated_system;

    double upe = (system->geometry.spin-1);
    if (system->geometry.isPBC)
    {
        upe = system->geometry.unpaired_electrons;
    }

    std::size_t function_calls_count = 0;
    int iter = 1;

    auto function = [&](long n, const double* x) -> double {
        ++function_calls_count;
        upe = std::abs(x[0]);

        QFile file(system->template_input);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QString errMsg = "ERROR: Cannot open template DFTB input file: " + system->template_input;
            throw std::runtime_error(errMsg.toStdString().c_str());
        }

        QTextStream fin(&file);

        QByteArray out_data;
        QByteArray sha_data;
        QTextStream final_hsd(&out_data);
        QTextStream sha_stream(&sha_data);

        while (!fin.atEnd())
        {
            QString line;
            line = fin.readLine();
            final_hsd << line << endl;
        }
        file.close();

        final_hsd << endl;

        writeInput(final_hsd, sha_stream, opt_info, skfileinfo, tempFolder, system);
        if (system->geometry.spin_polarization)
        {
            writeInputSpinPolarization(final_hsd, sha_stream, opt_info, system, upe);
        }



        QString sha1_key;
        {
            sha_stream << out_data;
            sha_stream.flush();
            QByteArray sha1 = QCryptographicHash::hash(sha_data, QCryptographicHash::Sha1);
            sha1_key = QString(sha1.toHex());
        }

        if (opt_info->use_cache && db.hasKey(sha1_key))
        {
            //load cache
            QByteArray data_array = db.getValue(sha1_key);
            QDataStream dstream(data_array);
            dstream >> evaluated_system;
        }
        else
        {
            ADPT::writeFile(tempFolder.absoluteFilePath("dftb_in.hsd"), QString(out_data));
            //start getting electronic forces
            QByteArray output;
            bool res = ADPT::ExecuteCommandStream(tempFolder.absolutePath(), m_dftb_path, QStringList(), output, QByteArray(), timeout);

            // read output results

            ADPT::writeFile(tempFolder.absoluteFilePath("dftb.out"), QString(output));

            {
                QTextStream stream(&output);
                while (!stream.atEnd())
                {
                    auto line = stream.readLine();
                    if (line.contains("ERROR!"))
                    {
                        QString errorMsg = line;
                        while (!stream.atEnd())
                        {
                            line = stream.readLine();
                            errorMsg = errorMsg + "\n" + line;
                        }
                        errorMsg = errorMsg + "\n";
                        throw std::runtime_error(errorMsg.toStdString().c_str());
                    }
                }
            }

            QFile autotest_tag(tempFolder.absoluteFilePath("autotest.tag"));
            if (!autotest_tag.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QByteArray errorMsg;
                QTextStream error_stream(&errorMsg);
                error_stream << autotest_tag.fileName();
                error_stream << endl
                             << "ERROR: " << endl
                             << endl;
                error_stream << "The system call: \"" << m_dftb_path << "\" did not produce the file: \"autotest.tag\" necessary for running erepfit"
                             << endl
                             << "calculation of electronic energy for system \"" << system->name << "\" failed."
                             << endl
                             << "exit erepfit" << endl
                             << endl;
                throw std::runtime_error(QString(errorMsg).toStdString());
            }

            QTextStream tin(&autotest_tag);
            bool foundEForce = false;
            bool foundEEl = false;
            bool foundEStress = false;

            double totalE = 0.0;
            double repulsiveE = 0.0;
            double elecE = 0.0;

            MatrixX3d elec_forces;
            MatrixX3d total_forces;
            MatrixX3d rep_forces;

            bool hasRepForces = false;
            bool hasTotalForces = false;

            bool hasRepEnergy = false;
            bool hasTotalEnergy = false;

            bool hasRepStress = false;
            bool hasTotalStress = false;

            dMatrix3x3 elec_stress;
            dMatrix3x3 total_stress;
            dMatrix3x3 rep_stress;

            while (!tin.atEnd())
            {
                QString line = tin.readLine();
                if (line.contains("repulsive_forces"))
                {
                    QStringList arr = line.split(":", QString::SkipEmptyParts);
                    int dim = arr[2].toInt();
                    QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                    if (dim == 2 && sizes.size() == dim)
                    {
                        int nat = sizes[1].toInt();
                        if (nat == system->geometry.coordinates.size())
                        {
                            rep_forces.resize(nat, 3);
                            for (int i = 0; i < nat; ++i)
                            {
                                double x, y, z;
                                tin >> x >> y >> z;
                                rep_forces(i, 0) = x;
                                rep_forces(i, 1) = y;
                                rep_forces(i, 2) = z;
                            }
                        }
                        else
                        {
                            throw std::runtime_error("Error reading repulsive forces: Invalid format.");
                        }
                    }
                    else
                    {
                        throw std::runtime_error("Error reading repulsive forces: Invalid format.");
                    }
                    hasRepForces = true;
                }
                else if (line.contains("forces"))
                {
                    QStringList arr = line.split(":", QString::SkipEmptyParts);
                    int dim = arr[2].toInt();
                    QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                    if (dim == 2 && sizes.size() == dim)
                    {
                        int nat = sizes[1].toInt();
                        if (nat == system->geometry.coordinates.size())
                        {
                            total_forces.resize(nat, 3);
                            for (int i = 0; i < nat; ++i)
                            {
                                double x, y, z;
                                tin >> x >> y >> z;
                                total_forces(i, 0) = x;
                                total_forces(i, 1) = y;
                                total_forces(i, 2) = z;
                            }
                        }
                        else
                        {
                            throw std::runtime_error("Error reading total forces: Invalid format.");
                        }
                    }
                    else
                    {
                        throw std::runtime_error("Error reading total forces: Invalid format.");
                    }
                    hasTotalForces = true;
                }
                else if (line.contains("repulsive_energy"))
                {
                    tin >> repulsiveE;
                    hasRepEnergy = true;
                }
                else if (line.contains("total_energy"))
                {
                    tin >> totalE;
                    hasTotalEnergy = true;
                }
                else if (line.contains("repulsive_stress"))
                {
                    QStringList arr = line.split(":", QString::SkipEmptyParts);
                    int dim = arr[2].toInt();
                    QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                    if (dim == 2 && sizes.size() == dim)
                    {
                        for (int i = 0; i < 3; ++i)
                        {
                            double x, y, z;
                            tin >> x >> y >> z;
                            rep_stress[i][0] = x;
                            rep_stress[i][1] = y;
                            rep_stress[i][2] = z;
                        }
                    }
                    hasRepStress = true;
                }
                else if (line.contains("stress"))
                {
                    QStringList arr = line.split(":", QString::SkipEmptyParts);
                    int dim = arr[2].toInt();
                    QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                    if (dim == 2 && sizes.size() == dim)
                    {
                        for (int i = 0; i < 3; ++i)
                        {
                            double x, y, z;
                            tin >> x >> y >> z;
                            total_stress[i][0] = x;
                            total_stress[i][1] = y;
                            total_stress[i][2] = z;
                        }
                    }
                    hasTotalStress = true;
                }

            }


            if (!hasTotalEnergy || !hasTotalForces)
            {
                throw std::runtime_error("Error reading electronic energy/forces: Invalid format.");
            }

            if (system->geometry.isPBC && (!hasTotalStress || !hasRepStress))
            {
                throw std::runtime_error("Error reading electronic stress: Invalid format.");
            }


            {
                evaluated_system.energy = totalE - repulsiveE;

                elec_forces = total_forces - rep_forces;
                evaluated_system.force.clear();
                for (int i = 0; i < elec_forces.rows(); ++i)
                {
                    dVector3 vec{ { elec_forces(i, 0), elec_forces(i, 1), elec_forces(i, 2) } };
                    evaluated_system.force.append(vec);
                }

                evaluated_system.pbc = system->geometry.isPBC;
                if (hasTotalStress && hasRepStress)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 3; ++j)
                        {
                            evaluated_system.stress[i][j] = total_stress[i][j] - rep_stress[i][j];
                        }
                    }
                }
            }           


            double efermi_a, efermi_b;
            parseDFTBFermiEnergies(tempFolder.absoluteFilePath("detailed.out"), efermi_a, efermi_b);

            QByteArray data_array;
            QDataStream dstream(&data_array, QIODevice::WriteOnly);
            dstream << evaluated_system;
            db.addKey(sha1_key, data_array);

            if (evaluated_system.pbc && system->opt_upe)
            {
                m_output_stream << QString("%1 %2 %3 %4 %5 %6")
                                       .arg(iter, 4)
                                       .arg(upe, 8, 'f', 4)
                                       .arg(evaluated_system.energy, 20, 'f', 12)
                                       .arg(efermi_a, 9, 'f', 4)
                                       .arg(efermi_b, 9, 'f', 4)
                                       .arg(std::abs(efermi_a - efermi_b), 8, 'f', 4)
                                << endl;
            }

            iter++;
        }
        return evaluated_system.energy;
    };

    if (system->geometry.isPBC && system->geometry.spin_polarization)
    {
        m_output_stream << endl;
        m_output_stream << QString("%1 %2 %3 %4 %5 %6")
                               .arg("Iter", 4)
                               .arg("UPE", 8)
                               .arg("Energy", 20)
                               .arg("EFermi(a)", 9)
                               .arg("EFermi(b)", 9)
                               .arg("Delta EF", 8)
                        << endl;

        if ( system->opt_upe)
        {
            const long variables_count = 1;
            const long number_of_interpolation_conditions = (variables_count + 1) * (variables_count + 2) / 2;
            double variables_values[] = { upe };
            const double initial_trust_region_radius = 0.2;
            const double final_trust_region_radius = 0.001;
            const long max_function_calls_count = 30;
            const size_t working_space_size = NEWUOA_WORKING_SPACE_SIZE(
                variables_count, number_of_interpolation_conditions);
            double working_space[working_space_size];

            auto closure = make_closure(function);

            newuoa_closure(
                &closure, variables_count, number_of_interpolation_conditions,
                variables_values, initial_trust_region_radius,
                final_trust_region_radius, max_function_calls_count, working_space);


            m_output_stream << QString("Final UPE: %1, mag= %2")
                               .arg(upe, 10, 'f', 4)
                               .arg(std::sqrt(upe * (upe + 2.0)), 10,
                                   'f', 5)
                        << endl;
            m_output_stream << "Finished." << endl;
        }

    }
    else
    {
        function(1, &upe);

    }

    return evaluated_system;
}

double DFTBPlusEvaluator::evaluateAtomicEnergy(const QString& elem, QDir tempFolder, const QString& template_input, const ADPT::SKFileInfo* skinfo, const OptionalEvaluationInformation* opt_info)
{
    QFile file(template_input);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString errMsg = "ERROR: Cannot open template DFTB input file: " + template_input;
        throw std::runtime_error(errMsg.toStdString().c_str());
    }

    QFile fout(tempFolder.absoluteFilePath("dftb_in.hsd"));
    if (!fout.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        {
            QString errMsg = "ERROR: Cannot open DFTB input file: " + tempFolder.absoluteFilePath("dftb_in.hsd");
            throw std::runtime_error(errMsg.toStdString().c_str());
        }
    }

    QTextStream final_hsd(&fout);
    fout.write(file.readAll());
    final_hsd << endl;

    QByteArray sha_data;
    QTextStream sha_stream(&sha_data);
    QList<QPair<QString, QString>> pairs;
    pairs.append(qMakePair(elem, elem));
    writeSKFilesInfo(pairs, sha_stream, tempFolder, skinfo, final_hsd);
    final_hsd << "!ParserOptions {" << endl
              << "    !IgnoreUnprocessedNodes = Yes" << endl
              << "}" << endl;

    QByteArray output;
    ADPT::ExecuteCommandStream(tempFolder.absolutePath(), m_dftb_path, QStringList(), output, QByteArray(), timeout);

    // read output results

    ADPT::writeFile(tempFolder.absoluteFilePath("dftb.out"), QString(output));


    double energy = parseDFTBEnergy(tempFolder.absoluteFilePath("detailed.out"));
    return energy;

}
}
