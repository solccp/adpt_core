#-------------------------------------------------
#
# Project created by QtCreator 2016-02-17T21:13:48
#
#-------------------------------------------------

QT       -= gui

TARGET = erepfit_evaluator
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    AbstractDFTBEvaluator.cpp \
    dftbplusevaluator.cpp \
    erepfitsystemevaluator.cpp \
    dcdftbkevaluator.cpp \
    electronicpartcachedatabase.cpp \
    newuoa.cpp

HEADERS += \
    AbstractDFTBEvaluator.h \
    dftbplusevaluator.h \
    optionalevluationinformation.h \
    erepfitsystemevaluator.h \
    dcdftbkevaluator.h \
    electronicpartcachedatabase.h \
    newuoa.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
GIT_VERSION = $$system(git --work-tree ../ describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"


INCLUDEPATH += $$PWD/../external_libs/Eigen
INCLUDEPATH += $$PWD/../external_libs/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/release/ -lDFTBPlusAdaptor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/debug/ -lDFTBPlusAdaptor
else:unix: LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/ -lDFTBPlusAdaptor

INCLUDEPATH += $$PWD/../libDFTBPlusAdaptor
DEPENDPATH += $$PWD/../libDFTBPlusAdaptor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/release/libDFTBPlusAdaptor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/debug/libDFTBPlusAdaptor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/release/DFTBPlusAdaptor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/debug/DFTBPlusAdaptor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/libDFTBPlusAdaptor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libHSDParser/release/ -lHSDParser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libHSDParser/debug/ -lHSDParser
else:unix: LIBS += -L$$OUT_PWD/../libHSDParser/ -lHSDParser

INCLUDEPATH += $$PWD/../libHSDParser
DEPENDPATH += $$PWD/../libHSDParser

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/release/libHSDParser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/debug/libHSDParser.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/release/HSDParser.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/debug/HSDParser.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/libHSDParser.a
