#include "dcdftbkevaluator.h"
#include "basic_types.h"

#include "electronicpartcachedatabase.h"
#include "erepfit2_inputdata.h"

#include "io_utils.h"
#include "optionalevluationinformation.h"
#include "process_utils.h"

#include <QRegularExpression>
#include <QStringBuilder>

#include <QCryptographicHash>
#include <QDataStream>

#include <QDebug>

namespace Erepfit2
{
DCDFTBKEvaluator::DCDFTBKEvaluator(QTextStream& output_stream, const QString& dftb_path, int timeout)
    : AbstractDFTBEvaluator(timeout), m_output_stream(output_stream)
    , m_dftb_path(dftb_path)
{
}

QList<QPair<QString, QString>> DCDFTBKEvaluator::getRequiredElecPairs(const Erepfit2::Input::System* system)
{
    QStringList elemts = system->geometry.getElements();
    elemts.removeDuplicates();
    QList<QPair<QString, QString>> res;
    for (int i = 0; i < elemts.size(); ++i)
    {
        for (int j = 0; j < elemts.size(); ++j)
        {
            res.append(qMakePair(elemts.at(i), elemts.at(j)));
        }
    }
    return res;
}

int getL(const QString& orbtype)
{
    QString orb_str = "spdfghijkl";
    return orb_str.indexOf(orbtype) + 1;
}

QString DCDFTBKEvaluator::prepare_SK_section(bool usingSpin, QTextStream& sha_stream, const Erepfit2::OptionalEvaluationInformation* opt_info, const ADPT::SKFileInfo* skfileinfo, bool usingORSCC, QDir tempFolder, bool usingDFTB3,
    const QStringList& elemList)
{
    QByteArray data;
    QTextStream final_input(&data);

    int nelems = elemList.size();

    final_input << nelems << endl;
    for (int i = 0; i < elemList.size(); ++i)
    {
        auto elem = elemList[i];
        auto maxl = skfileinfo->maxAngularMomentum[elem];
        int l = getL(maxl);
        final_input << elem << " " << l;
        if (usingDFTB3)
        {
            if (!opt_info->hubbardDerivatives.contains(elem))
            {
                throw std::runtime_error("ERROR! Requested DFTB3 calculation but hubbard derivatives not provided!");
            }
            auto hubb = opt_info->hubbardDerivatives[elem];
            if (usingORSCC)
            {
                final_input << " " << hubb[0] << " " << hubb[1] << " " << hubb[2];
            }
            else
            {
                final_input << " " << hubb[2];
            }
        }
        if (usingSpin)
        {
            if (!opt_info->spinConstants.contains(elem))
            {
                throw std::runtime_error("ERROR! Request spin-polarized calculation but spin constants not provided!");
            }

            auto fname = QString("%1.wll").arg(elem);
            QStringList spin_constants;
            for (auto value : opt_info->spinConstants[elem])
            {
                spin_constants.append(value.toString());
            }
            ADPT::writeFile(tempFolder.absoluteFilePath(fname), spin_constants.join("\n") + "\n");
            final_input << " " << fname;
        }
        final_input << endl;
        for (int j = 0; j < elemList.size(); ++j)
        {
            auto skfiles = writeTempElecPart(tempFolder.absolutePath(), elem, elemList[j], skfileinfo, sha_stream);
            final_input << " " << skfiles.join(" ");
        }
        final_input << endl;
    }
    return QString(data);
}

Input::SystemElectronicData DCDFTBKEvaluator::evaluate(
    QDir tempFolder,
    const ADPT::SKFileInfo* skfileinfo,
    const Erepfit2::Input::System* system,
    const Erepfit2::OptionalEvaluationInformation* opt_info)
{

    QByteArray sha_data;
    QTextStream sha_stream(&sha_data);

    Erepfit2::Input::SystemElectronicData evaluated_system;

    DCDFTBKInput input;

    input.parseDCDFTBKInput(system->template_input);

    auto& keyword_input = input.keyword_section;

    keyword_input["FORCE"] = true;
    keyword_input["DC"] = false;
    keyword_input.remove("OPT");
    if (system->geometry.isPBC)
    {
        QVariant tempsection = keyword_input.value("PBC", QVariantMap());
        QVariantMap section;
        if (tempsection.canConvert<QVariantMap>())
        {
            section = tempsection.toMap();
        }
        section["STRESS"] = true;
        section["MAXGAMMA"] = 10;
        keyword_input["PBC"] = section;
    }
    else
    {
        keyword_input.remove("PBC");
    }

    if (system->geometry.spin_polarization)
    {
        if (!keyword_input["SCC"].canConvert<QVariantMap>())
        {
            keyword_input["SCC"] = QVariantMap();
        }
        QVariantMap existingMap = qvariant_cast<QVariantMap>(keyword_input["SCC"]);
        existingMap["SPIN"] = true;
        keyword_input["SCC"] = QVariant(existingMap);
    }

    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;
    if (keyword_input["SCC"].canConvert<QVariantMap>())
    {
        auto map = keyword_input["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }

    input.title_section = system->name;
    QStringList elemList = system->geometry.getElements();
    elemList.removeDuplicates();

    input.SK_section = prepare_SK_section(usingSpin, sha_stream, opt_info, skfileinfo, usingORSCC, tempFolder, usingDFTB3, elemList);
    input.geometry_section = system->geometry.getDCDFTBKXYZ();

    QString sha1_key;
    ElectronicPartCacheDatabase& db = Singleton<ElectronicPartCacheDatabase>::Instance();
    {
        input.writeInput(sha_stream);
        sha_stream.flush();
        QByteArray sha1 = QCryptographicHash::hash(sha_data, QCryptographicHash::Sha1);
        sha1_key = QString(sha1.toHex());
    }

    {
        QFile fout(tempFolder.absoluteFilePath("dftb.inp"));
        if (!fout.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            throw std::runtime_error("Cannot open " + tempFolder.absoluteFilePath("dftb.inp").toStdString() + " for writing.");
        }

        QTextStream stream(&fout);
        input.writeInput(stream);

        if (opt_info->use_cache && db.hasKey(sha1_key))
        {
            //load cache
            QByteArray data_array = db.getValue(sha1_key);
            QDataStream dstream(data_array);
            dstream >> evaluated_system;
            m_output_stream << " (loaded) ";
        }
        else
        {

            //            //start getting electronic forces
            QByteArray output;
            ADPT::ExecuteCommandStream(tempFolder.absolutePath(), m_dftb_path, QStringList(), output, QByteArray(), timeout);

            QFile autotest_tag(tempFolder.absoluteFilePath("dftb.out"));
            if (!autotest_tag.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QByteArray errorMsg;
                QTextStream error_stream(&errorMsg);
                error_stream << autotest_tag.fileName();
                error_stream << endl
                             << "ERROR: " << endl
                             << endl;
                error_stream << "The system call: \"" << m_dftb_path << "\" did not produce the file: \"dftb.out\" necessary for running erepfit"
                             << endl
                             << "calculation of electronic energy for system \"" << system->name << "\" failed."
                             << endl
                             << "exit erepfit" << endl
                             << endl;
                throw std::runtime_error(QString(errorMsg).toStdString());
            }

            QTextStream tin(&autotest_tag);
            bool foundEForce = false;
            bool foundEEl = false;

            double elecE = 0.0;

            QList<dVector3> elec_force;

            while (!tin.atEnd())
            {
                QString line = tin.readLine();
                if (line.contains("Final SCC-DFTB Energy =") ||
                    line.contains("Final DFTB-3rd Energy =") ||
                    line.contains("Final DC-SCC-DFTB Energy =") ||
                    line.contains("Final DC-DFTB-3rd Energy =")
                        )
                {
                    QStringList arr = line.split(" ", QString::SkipEmptyParts);
                    elecE = arr[4].toDouble();
                    foundEEl = true;
                }
                else if (line.contains("NCC-DFTB Energy =") ||
                         line.contains("DC-NCC-DFTB Energy"))
                {
                     QStringList arr = line.split(" ", QString::SkipEmptyParts);
                     elecE = arr[3].toDouble();
                     foundEEl = true;
                }
                else if (line.contains("Total forces"))
                {
                    int nat = system->geometry.coordinates.size();
                    tin.readLine();
                    tin.readLine();
                    tin.readLine();
                    tin.readLine();
                    for (int i = 0; i < nat; ++i)
                    {
                        double x, y, z;
                        tin >> line >> x >> y >> z;
                        dVector3 vec{ x, y, z };
                        elec_force.append(vec);
                    }
                    foundEForce = true;
                }
            }
            if (!foundEEl || !foundEForce)
            {
                throw std::runtime_error("Error reading electronic energy/forces: Invalid format.");
            }

            evaluated_system.pbc = system->geometry.isPBC;
            evaluated_system.energy = elecE;
            evaluated_system.force = elec_force;

            QByteArray data_array;
            QDataStream dstream(&data_array, QIODevice::WriteOnly);
            dstream << evaluated_system;
            db.addKey(sha1_key, data_array);
        }
    }
    return evaluated_system;
}

double DCDFTBKEvaluator::evaluateAtomicEnergy(const QString& elem,
    QDir tempFolder,
    const QString& template_input,
    const ADPT::SKFileInfo* skinfo,
    const OptionalEvaluationInformation* opt_info)
{
    QByteArray sha_data;
    QTextStream sha_stream(&sha_data);

    DCDFTBKInput input;

    input.parseDCDFTBKInput(template_input);

    auto& keyword_input = input.keyword_section;

    keyword_input["DC"] = false;
    keyword_input.remove("OPT");
    keyword_input.remove("PBC");
    keyword_input.remove("FORCE");

    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;
    if (keyword_input["SCC"].canConvert<QVariantMap>())
    {
        auto map = keyword_input["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }

    input.title_section = elem;
    QStringList elemList;
    elemList << elem;
    input.SK_section = prepare_SK_section(usingSpin, sha_stream, opt_info, skinfo, usingORSCC, tempFolder, usingDFTB3, elemList);
    //        input.geometry_section = system->geometry.getDCDFTBKXYZ();

    double elecE = 0.0;
    {
        QFile fout(tempFolder.absoluteFilePath("dftb.inp"));
        if (!fout.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            throw std::runtime_error("Cannot open " + tempFolder.absoluteFilePath("dftb.inp").toStdString() + " for writing.");
        }

        QTextStream stream(&fout);
        input.writeInput(stream);

        {

            QByteArray output;
            ADPT::ExecuteCommandStream(tempFolder.absolutePath(), m_dftb_path, QStringList(), output, QByteArray(), timeout);

            QFile autotest_tag(tempFolder.absoluteFilePath("dftb.out"));
            if (!autotest_tag.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QByteArray errorMsg;
                QTextStream error_stream(&errorMsg);
                error_stream << autotest_tag.fileName();
                error_stream << endl
                             << "ERROR: " << endl
                             << endl;
                error_stream << "The system call: \"" << m_dftb_path << "\" did not produce the file: \"dftb.out\" necessary for running erepfit"
                             << endl
                             << "calculation of electronic energy for system \"" << elem << "\" failed."
                             << endl
                             << "exit erepfit" << endl
                             << endl;
                throw std::runtime_error(QString(errorMsg).toStdString());
            }

            QTextStream tin(&autotest_tag);
            bool foundEEl = false;

            while (!tin.atEnd())
            {
                QString line = tin.readLine();
                if (line.contains("Final SCC-DFTB Energy =") ||
                    line.contains("Final DFTB-3rd Energy =") ||
                    line.contains("Final DC-SCC-DFTB Energy =") ||
                    line.contains("Final DC-DFTB-3rd Energy =")
                        )
                {
                    QStringList arr = line.split(" ", QString::SkipEmptyParts);
                    elecE = arr[4].toDouble();
                    foundEEl = true;
                }
                else if (line.contains("NCC-DFTB Energy =") ||
                         line.contains("DC-NCC-DFTB Energy"))
                     {
                         QStringList arr = line.split(" ", QString::SkipEmptyParts);
                         elecE = arr[3].toDouble();
                         foundEEl = true;
                     }
            }
            if (!foundEEl)
            {
                throw std::runtime_error("Error reading electronic energy");
            }
        }
    }
    return elecE;
}

void DCDFTBKInput::parseDCDFTBKInput(const QString& filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString errMsg = "ERROR: Cannot open template DFTB input file: " + filename;
        throw std::runtime_error(errMsg.toStdString());
    }

    QTextStream fin(&file);

    this->keyword_section = parseDCDFTBKKeywordInput(fin);
    this->title_section = parseDCDFTBKTitleInput(fin);
    this->SK_section = parseDCDFTBKSKInput(fin);
    this->geometry_section = parseDCDFTBKGeometryInput(fin);
    this->final_section = parseDCDFTBKFinalInput(fin);

    file.close();
}

QVariantMap DCDFTBKInput::parseDCDFTBKKeywordInput(QTextStream& os)
{
    QRegularExpression keywordline_false("^\\s*(\\w+)\\s*=\\s*(FALSE|TRUE)\\s*$");
    QRegularExpression keywordline_subsection("\\s*(\\w+)=\\((.*)\\)");

    QVariantMap input;
    while (!os.atEnd())
    {
        QString line;
        line = os.readLine();
        if (line.trimmed().isEmpty())
        {
            break;
        }
        QRegularExpressionMatch match = keywordline_false.match(line);
        if (match.hasMatch())
        {
            auto value = match.captured(2).toUpper();
            if (value == "FALSE")
                input[match.captured(1)] = false;
            else
                input[match.captured(1)] = true;
        }
        match = keywordline_subsection.match(line);
        if (match.hasMatch())
        {
            auto section = match.captured(1);
            auto keyline = match.captured(2);
            QVariantMap dict;
            auto keys = keyline.split(" ", QString::SkipEmptyParts);
            for (auto item : keys)
            {
                auto values = item.split("=", QString::SkipEmptyParts);
                dict[values[0].trimmed()] = values[1].trimmed();
            }
            input[section] = dict;
        }
    }
    return input;
}

QString DCDFTBKInput::parseDCDFTBKStringSection(QTextStream& os)
{
    QByteArray data;
    QTextStream stream(&data);
    while (!os.atEnd())
    {
        QString line;
        line = os.readLine();
        if (line.trimmed().isEmpty())
        {
            break;
        }
        stream << line << endl;
    }
    return QString(data);
}

QString DCDFTBKInput::parseDCDFTBKTitleInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKSKInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKGeometryInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKFinalInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

void DCDFTBKInput::writeInput(QTextStream& os) const
{

    QMapIterator<QString, QVariant> it(keyword_section);
    while (it.hasNext())
    {
        it.next();
        if (it.value().canConvert<bool>())
        {
            bool value = it.value().toBool();
            if (value)
            {
                os << QString("%1=TRUE").arg(it.key()) << endl;
            }
            else
            {
                os << QString("%1=FALSE").arg(it.key()) << endl;
            }
        }
        else if (it.value().canConvert<QVariantMap>())
        {
            QStringList values;
            QMapIterator<QString, QVariant> it2(it.value().toMap());
            while (it2.hasNext())
            {
                it2.next();
                values.append(QString("%1=%2").arg(it2.key()).arg(it2.value().toString()));
            }
            os << QString("%1=(%2)").arg(it.key()).arg(values.join(" ")) << endl;
        }
    }

    os << endl;

    os << title_section.trimmed() << endl;
    os << endl;

    os << SK_section.trimmed() << endl;
    os << endl;

    os << geometry_section.trimmed() << endl;
    os << endl;

    os << final_section.trimmed() << endl;
    os << endl;
}
}
