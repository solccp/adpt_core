#ifndef ABSTRACTDFTBEVALUATOR_H
#define ABSTRACTDFTBEVALUATOR_H

#include "Eigen/Core"
#include <QDir>
#include <QMap>
#include <array>

#include <QTextStream>

namespace ADPT
{
class SKFileInfo;
}
namespace Erepfit2
{

typedef Eigen::Matrix<double, Eigen::Dynamic, 3> MatrixX3d;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXXd;

typedef std::array<int, 3> iVector3;
typedef std::array<double, 3> dVector3;
typedef std::array<dVector3, 3> dMatrix3x3;

namespace Input
{
    class System;
    class SystemElectronicData;
}
class EvaluatedSystem;
class OptionalEvaluationInformation;

class AbstractDFTBEvaluator
{
public:
    AbstractDFTBEvaluator(int evaluation_timeout);
    virtual ~AbstractDFTBEvaluator();
    virtual Input::SystemElectronicData evaluate(QDir tempFolder, const ADPT::SKFileInfo* skinfo, const Input::System* system, const OptionalEvaluationInformation* opt_info) = 0;
    virtual double evaluateAtomicEnergy(const QString& elem, QDir tempFolder, const QString& template_input, const ADPT::SKFileInfo* skinfo, const OptionalEvaluationInformation* opt_info) = 0;
protected:
    QStringList writeTempElecPart(const QString& scratch, const QString& elem1, const QString& elem2, const ADPT::SKFileInfo* skfileinfo, QTextStream& os);
    void cropElectronicSKFile(const QString& src, const QString& dest, QTextStream& os);
    int timeout = -1;
};
}

#endif // ABSTRACTDFTBEVALUATOR_H
