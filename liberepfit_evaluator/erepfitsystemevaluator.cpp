#include "erepfitsystemevaluator.h"
#include "electronicpartcachedatabase.h"
#include "optionalevluationinformation.h"


#include <QSet>
#include <set>
#include <dftbplusevaluator.h>
#include <dcdftbkevaluator.h>

#include <QFileInfo>
#include <QDir>

namespace Erepfit2
{

template<class Container>
int indexUUID(const Container& C, const QString& uuid)
{
    for(int i=0; i<C.size(); ++i)
    {
        if (C[i].uuid.toString() == uuid)
        {
            return i;
        }
    }
    return -1;
}

inline QString centerString(const QString& str, int width = 80)
{
    int len = str.size();
    int rest = width - len;
    if (rest < 0)
    {
        return str;
    }
    int leftpad = rest / 2;
    int rightpad = rest - leftpad;
    return (QString("").fill(' ', leftpad) + str + QString("").fill(' ', rightpad));
}

const QString HYPHENLINE = QString("").fill('-',80);
const QString EQUALLINE  = QString("").fill('=',80);
const QString STARLINE   = QString("").fill('*',80);
const QString EXCLALINE   = QString("").fill('!',80);
const QString INDENT_SPACES = "    " ;

void print_header_and_version(QTextStream& os)
{
    os << EQUALLINE << endl << endl;
    os << centerString("ErepfitEvaluator") << endl
       << centerString("Tool for Evaluating the Electronic Energy and Forces for the Erepfit program") << endl;
    os << endl;
    os << centerString("Powered by Chien-Pin Chou") << endl;
    os << endl << centerString(QString(GIT_VERSION))
       << endl << endl << endl;
    os << EQUALLINE << endl << endl;
}

ErepfitSystemEvaluator::ErepfitSystemEvaluator(QTextStream& output_stream) : m_output_stream(output_stream)
{
    scratchDir = QDir::temp();
    print_header_and_version(m_output_stream);
}

void ErepfitSystemEvaluator::evaluate(Input::Erepfit2_InputData *input)
{
    m_input_data = input;
    try
    {
        m_output_stream << "Initializing Erepfit ... " << endl;
        init_SK();
        init_evaluator();
        m_output_stream << "Erepfit initialized. " << endl;
    }
    catch(const std::exception &e)
    {
        m_output_stream << "Erepfit initialization error: " << e.what() << endl;
        throw;
    }

    prehandle();

    try
    {
        m_output_stream << "Start evaluating the electronic energy of atoms..." << endl;
        evaluateAtoms();
        m_output_stream << "Start evaluating the electronic energy/forces of systems..." << endl;
        evaluateSystems();
        update_result();
    }
    catch(const std::exception & e)
    {
        m_output_stream << "Erepfit: system evaluation error: " << e.what() << endl;
        throw;
    }

    return;

}

void ErepfitSystemEvaluator::setScratchRoot(const QString &path)
{
    QFileInfo l_temp(path);
    if (l_temp.isDir() && l_temp.isWritable())
    {
        scratchDir.setPath(path);
    }
}

void ErepfitSystemEvaluator::init_SK()
{
    m_output_stream << m_input_data->systems.size() << " systems found in the input file." << endl;
    auto required_elec_pairs = m_input_data->required_elec_pairs();
    m_output_stream << endl;

    m_output_stream << "The following pairs of Electronic SK files are required: " << endl;
    bool missing_electronic_sks = false;
    foreach (auto const & pair, required_elec_pairs)
    {
        auto poten = ADPT::PotentialName::fromString(pair);
        auto skfiles = m_input_data->electronic_skfiles->getSKFileName(poten.getElement1(), poten.getElement2());


        if (skfiles.size() > 1 && m_input_data->options.toolchain.name.toLower() == "dc-dftb-k")
        {
            throw std::runtime_error("ERROR! DC-DFTB-K does not support multi-valance SK yet!!");
        }

        if (skfiles.isEmpty())
        {
            m_output_stream << INDENT_SPACES << pair << " = {}" << endl;
            missing_electronic_sks = true;
        }
        else
        {
            m_output_stream << INDENT_SPACES << pair << " = {" << endl;
            foreach (auto const & skfile, skfiles)
            {
                QFileInfo fileinfo(skfile);
                if (!fileinfo.exists() || !fileinfo.isReadable())
                {
                    throw std::runtime_error(fileinfo.absoluteFilePath().toStdString() + " is not found or readable.");
                }
                m_output_stream << INDENT_SPACES << INDENT_SPACES << skfile << endl;
            }
            m_output_stream << INDENT_SPACES << "}" << endl;
        }
    }
    if (missing_electronic_sks)
    {
        throw std::runtime_error("Some electronic SK files are missing.");
    }
    m_output_stream << endl;
}

void ErepfitSystemEvaluator::init_evaluator()
{
    if (m_input_data->options.toolchain.name.toLower() == "dftb+")
    {
        auto evaluator = std::make_shared<DFTBPlusEvaluator>(m_output_stream, m_input_data->options.toolchain.path, m_input_data->options.toolchain.evaluation_timeout);
        m_evaluator = std::static_pointer_cast<AbstractDFTBEvaluator>(evaluator);
    }
    else if (m_input_data->options.toolchain.name.toLower() == "dc-dftb-k")
    {
        auto evaluator = std::make_shared<DCDFTBKEvaluator>(m_output_stream, m_input_data->options.toolchain.path, m_input_data->options.toolchain.evaluation_timeout);
        m_evaluator = std::static_pointer_cast<AbstractDFTBEvaluator>(evaluator);
    }
}

void ErepfitSystemEvaluator::prehandle()
{

}

void ErepfitSystemEvaluator::initializeOptInfo(OptionalEvaluationInformation& opt_info)
{
    opt_info.use_cache = m_input_data->options.use_cache;

    if (m_input_data->dftb_options.contains("hubbard_derivatives"))
    {
        auto hd = m_input_data->dftb_options["hubbard_derivatives"].toMap();
        QMapIterator<QString, QVariant> it(hd);
        while(it.hasNext())
        {
            it.next();
            if (it.value().canConvert<QVariantList>())
            {
                auto list = it.value().toList();
                dVector3 vec;
                vec[0] = list[0].toDouble();
                vec[1] = list[1].toDouble();
                vec[2] = list[2].toDouble();
                opt_info.hubbardDerivatives.insert(it.key(), vec);
                opt_info.orbital_resolved_scc = true;
            }
            else
            {
                dVector3 vec;
                vec[0] = it.value().toDouble();
                vec[1] = vec[0];
                vec[2] = vec[0];
                opt_info.hubbardDerivatives.insert(it.key(), vec);
            }
        }
    }

    if (m_input_data->dftb_options.contains("spin_constants"))
    {
        auto hd = m_input_data->dftb_options["spin_constants"].toMap();
        if (m_input_data->dftb_options.contains("shell_resolved_spin"))
        {
            opt_info.shell_resolved_spin = m_input_data->dftb_options["shell_resolved_spin"].toBool();
        }
        QMapIterator<QString, QVariant> it(hd);
        while(it.hasNext())
        {
            it.next();
            if (it.value().canConvert<QVariantList>())
            {
                opt_info.spinConstants[it.key()] = it.value().toList();
            }
        }
    }
    if (m_input_data->dftb_options.contains("dispersion"))
    {
        opt_info.dispersion = m_input_data->dftb_options["dispersion"].toMap();
    }
}

void ErepfitSystemEvaluator::evaluateSystems()
{
    foreach (auto const & system, m_input_data->systems)
    {
        m_uuid_system_mapping.insert(system.uuid.toString(), std::make_shared<Input::System>(system));
        m_system_uuid_name_mapping.insert(system.uuid.toString(), system.name);
    }

    std::set<QString> systems_to_evaluated;
    foreach (auto const & item, m_input_data->equations.energyEquations)
    {
        {
            systems_to_evaluated.insert(item.uuid.toString());
        }
    }
    foreach (auto const & item, m_input_data->equations.forceEquations)
    {
        {
            systems_to_evaluated.insert(item.uuid.toString());
        }
    }

    foreach (auto const & reaction, m_input_data->equations.reactionEquations)
    {
        {
            foreach (auto const & item, reaction.reactants)
            {
                systems_to_evaluated.insert(item.uuid.toString());
            }
            foreach (auto const & item, reaction.products)
            {
                systems_to_evaluated.insert(item.uuid.toString());
            }
        }
    }

    QMap<QString, QString> name_uuid_mapping;
    int max_len_name = 0;
    for(auto const & str: systems_to_evaluated)
    {
        auto name = m_system_uuid_name_mapping.value(str);
        if (name.size() > max_len_name)
        {
            max_len_name = name.size();
        }
        name_uuid_mapping.insert(name, str);
    }

    m_output_stream << systems_to_evaluated.size() << " system will be evaluated." << endl << endl;
    m_output_stream << QString("%1  %2").arg("Name", -max_len_name).arg("UUID", 38) << endl;
    m_output_stream << QString().fill('=', 100) << endl;
    for(auto const & str: name_uuid_mapping.values())
    {
        auto name = m_system_uuid_name_mapping.value(str);
        m_output_stream << QString("%1  %2").arg(name, -max_len_name).arg(str, 38) << endl;
    }

    m_output_stream << QString().fill('-', 100) << endl;
    m_output_stream << endl;

    OptionalEvaluationInformation opt_info;
    initializeOptInfo(opt_info);


    int index = 0;
    for(auto const & str: name_uuid_mapping.keys())
    {
        auto uuid = name_uuid_mapping[str];
        QDir tempFolder = scratchDir.absoluteFilePath(uuid);
        tempFolder.mkpath(tempFolder.absolutePath());
        ++index ;
        try
        {
            auto name = m_system_uuid_name_mapping.value(uuid);

            m_output_stream << QString("Evaluating system %1 of %2...").arg(index).arg(name_uuid_mapping.size()) << endl;
            m_output_stream << INDENT_SPACES << "Name: " << name << endl;
            m_output_stream << INDENT_SPACES << "UUID: " << uuid << endl;

            auto system_pointer =  m_uuid_system_mapping.value(uuid).get();

            if (system_pointer == nullptr)
            {
                throw std::runtime_error("no mapping found: "+ str.toStdString());
            }

            if (system_pointer->geometry.isPBC)
            {
                m_output_stream << INDENT_SPACES << "PBC: True" << endl;
                if (system_pointer->geometry.kpoints.m_type == ADPT::KPointsSetting::Type::Plain )
                {
                    m_output_stream << INDENT_SPACES << "KPOINTS: Plain"  << endl;
                }
                else if (system_pointer->geometry.kpoints.m_type == ADPT::KPointsSetting::Type::SupercellFolding )
                {
                    m_output_stream << INDENT_SPACES << "KPOINTS: Supercell"  << endl;
                    for (int i=0; i< 3; ++i)
                    {
                        m_output_stream << INDENT_SPACES << INDENT_SPACES ;
                        for(int j=0; j<3;++j)
                        {
                             m_output_stream << system_pointer->geometry.kpoints.m_kpoints_supercell.num_of_cells[i][j] << " ";
                        }
                        m_output_stream << endl;
                    }
                    m_output_stream << INDENT_SPACES << INDENT_SPACES ;
                    for(int j=0; j<3;++j)
                    {
                         m_output_stream << system_pointer->geometry.kpoints.m_kpoints_supercell.shifts[j] << " ";
                    }
                    m_output_stream << endl;
                }

                if (system_pointer->geometry.spin_polarization)
                {
                    m_output_stream << INDENT_SPACES << "Spin polarization: True" << endl;
                    m_output_stream << INDENT_SPACES << QString("Unpaired Electrons: %1").arg(system_pointer->geometry.unpaired_electrons, 0, 'f', 5) << endl;
                    if (system_pointer->opt_upe)
                    {
                        m_output_stream << INDENT_SPACES << "Optimize Unpaired Electrons: True" << endl;
                    }
                    else
                    {
                        m_output_stream << INDENT_SPACES << "Optimize Unpaired Electrons: False" << endl;
                    }
                }
                else
                {
                    m_output_stream << INDENT_SPACES << "Spin polarization: False" << endl;
                }


            }
            else
            {
                m_output_stream << INDENT_SPACES << "PBC: False" << endl;
                m_output_stream << INDENT_SPACES << QString("Charge: %1").arg(system_pointer->geometry.charge) << endl;
                if (system_pointer->geometry.spin_polarization)
                {
                    m_output_stream << INDENT_SPACES << "Spin polarization: True" << endl;
                    m_output_stream << INDENT_SPACES << QString("Spin: %1").arg(system_pointer->geometry.spin) << endl;
                }
                else
                {
                    m_output_stream << INDENT_SPACES << "Spin polarization: False" << endl;
                }
            }

            auto evaluated_system = m_evaluator->evaluate(tempFolder, this->m_input_data->electronic_skfiles.get(), system_pointer, &opt_info);
            m_output_stream << INDENT_SPACES << QString("Total Electronic Energy: %1").arg(evaluated_system.energy, 0, 'f', 8) << endl;

            m_output_stream << INDENT_SPACES << QString("Total Electronic Forces: ") << endl;
            for (int i=0; i< evaluated_system.force.size(); ++i)
            {
                m_output_stream << INDENT_SPACES << QString("%1 %2 %3").arg(evaluated_system.force[i][0], 20, 'f', 12)
                        .arg(evaluated_system.force[i][1], 20, 'f', 12)
                        .arg(evaluated_system.force[i][2], 20, 'f', 12) << endl;
            }



            m_results.insert(uuid, evaluated_system);
            m_output_stream << "done." << endl;

            if (!this->m_input_data->options.debug)
            {
                tempFolder.removeRecursively();
            }

        }
        catch(const std::exception & e)
        {
            m_output_stream << "failed." << endl;
            m_output_stream << e.what() <<  endl;
            m_output_stream << "Working folder: " << tempFolder.absolutePath() << endl;
            throw;
        }
    }
    m_output_stream << QString().fill('-', 100) << endl;
    m_output_stream << endl;
}

void ErepfitSystemEvaluator::evaluateAtoms()
{
    if (m_input_data->equations.energyEquations.isEmpty())
    {
        m_output_stream << "No Atomic Energies Needed, skipped." << endl;
        return;
    }

    auto elements = m_input_data->required_elements();
    m_output_stream << "Element energy needed: [" << elements.join(", ") << "]"<< endl;


    {
        QMapIterator<QString, QString> it(m_input_data->atomicEnergy.atomic_dftbinputs);
        OptionalEvaluationInformation opt_info;
        initializeOptInfo(opt_info);
        while(it.hasNext())
        {
            it.next();

            if (m_input_data->atomicEnergy.atomic_energy.contains(it.key()))
            {
                m_output_stream << "Atomic energy for " << it.key() << " has been specified, skipping." << endl;
                continue;
            }

            QString str = QString("Atomic_%1").arg(it.key());
            QDir tempFolder = scratchDir.absoluteFilePath(str);
            tempFolder.mkpath(tempFolder.absolutePath());

            if (!m_input_data->atomicEnergy.atomic_energy.contains(it.key()))
            {
                QString temp_input = it.value();
                double energy = m_evaluator->evaluateAtomicEnergy(it.key(), tempFolder, temp_input, this->m_input_data->electronic_skfiles.get(), &opt_info);
                m_input_data->atomicEnergy.atomic_energy.insert(it.key(), energy);
            }
        }
        m_input_data->atomicEnergy.atomic_dftbinputs.clear();
    }

    {
        QMapIterator<QString, double> it(m_input_data->atomicEnergy.atomic_energy);
        m_output_stream << "Final Atomic Energies:" << endl;
        while(it.hasNext())
        {
            it.next();
            m_output_stream << "  " << it.key() << ": " << QString("%1").arg(it.value(), 20, 'f', 12) << endl;
        }
    }



}

void ErepfitSystemEvaluator::update_result()
{
    auto results = m_results.keys();

    for(auto & key : results)
    {
        int index = indexUUID(m_input_data->systems, key);
        if (index != -1)
        {
            m_input_data->systems[index].evaluated = true;
            m_input_data->systems[index].elec_data = m_results[key];
        }
    }
}
}
