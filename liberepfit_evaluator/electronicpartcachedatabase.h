#ifndef ELECTRONICPARTCACHEDATABASE_H
#define ELECTRONICPARTCACHEDATABASE_H


#include "Singleton.h"
#include <QHash>
#include <QString>
#include <QVariant>
#include <mutex>

class ElectronicPartCacheDatabase
{
    friend class Singleton<ElectronicPartCacheDatabase>;
private:
    ElectronicPartCacheDatabase();
public:
    std::mutex& setEnable_mut()
    {
        static std::mutex m;
        return m;
    }
    void setEnable(bool value);
    bool hasKey(const QString& key);
    void addKey(const QString& key, const QByteArray &value);
    QByteArray getValue(const QString& key);
    bool isEnable = true;
    void save();
    void load();
private:
    std::mutex& addKey_mut()
    {
        static std::mutex m;
        return m;
    }
    std::mutex& hasKey_mut()
    {
        static std::mutex m;
        return m;
    }
    std::mutex& getValue_mut()
    {
        static std::mutex m;
        return m;
    }
    QHash<QString, QVariant> m_database;
};

#endif // ELECTRONICPARTCACHEDATABASE_H
