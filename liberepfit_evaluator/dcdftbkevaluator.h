#ifndef DCDFTBKEVALUATOR_H
#define DCDFTBKEVALUATOR_H

#include "AbstractDFTBEvaluator.h"
#include <QTextStream>

namespace Erepfit2
{

class DCDFTBKInput
{
public:
    QVariantMap keyword_section;
    QString title_section;
    QString geometry_section;
    QString SK_section;
    QString final_section;
    void parseDCDFTBKInput(const QString& filename);
    void writeInput(QTextStream& os) const;
private:

    QVariantMap parseDCDFTBKKeywordInput(QTextStream& os);
    QString parseDCDFTBKStringSection(QTextStream& os);
    QString parseDCDFTBKTitleInput(QTextStream& os);
    QString parseDCDFTBKSKInput(QTextStream& os);
    QString parseDCDFTBKGeometryInput(QTextStream& os);
    QString parseDCDFTBKFinalInput(QTextStream& os);

};

class DCDFTBKEvaluator : public AbstractDFTBEvaluator
{
public:
    DCDFTBKEvaluator(QTextStream& output_stream, const QString& dftb_path, int timeout);

    // AbstractDFTBEvaluator interface
public:
    virtual Input::SystemElectronicData evaluate(QDir tempFolder, const ADPT::SKFileInfo* skinfo, const Input::System* system, const OptionalEvaluationInformation* opt_info) override;
    virtual double evaluateAtomicEnergy(const QString& elem, QDir tempFolder, const QString& template_input, const ADPT::SKFileInfo* skinfo, const OptionalEvaluationInformation* opt_info) override;
private:
    QList<QPair<QString, QString>> getRequiredElecPairs(const Erepfit2::Input::System* system);
    QString prepare_SK_section(bool usingSpin, QTextStream & sha_stream, const Erepfit2::OptionalEvaluationInformation *opt_info, const ADPT::SKFileInfo* skfileinfo, bool usingORSCC, QDir tempFolder, bool usingDFTB3, const QStringList &elemList);

protected:
    QTextStream& m_output_stream;
    QString m_dftb_path;
};
}

#endif // DCDFTBKEVALUATOR_H
