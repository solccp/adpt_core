#ifndef DFTBPLUSEVALUATOR_H
#define DFTBPLUSEVALUATOR_H

#include "AbstractDFTBEvaluator.h"
#include <QTextStream>

namespace Erepfit2
{

class DFTBPlusEvaluator : public AbstractDFTBEvaluator
{
public:
    DFTBPlusEvaluator(QTextStream& output_stream, const QString& dftb_path, int evaluation_timeout);

    // AbstractDFTBEvaluator interface
public:
    virtual Input::SystemElectronicData evaluate(QDir tempFolder, const ADPT::SKFileInfo* skinfo, const Input::System *system, const OptionalEvaluationInformation *opt_info) override;
    virtual double evaluateAtomicEnergy(const QString& elem, QDir tempFolder, const QString& template_input, const ADPT::SKFileInfo* skinfo, const OptionalEvaluationInformation* opt_info) override;
private:
    QList<QPair<QString, QString> > getRequiredElecPairs(const Erepfit2::Input::System *system);
    void writeInput(QTextStream &final_hsd, QTextStream &sha_stream, const Erepfit2::OptionalEvaluationInformation* opt_info, const ADPT::SKFileInfo* skfileinfo, QDir tempFolder, const Erepfit2::Input::System* system);
    void writeInputSpinPolarization(QTextStream & final_hsd, QTextStream & sha_stream, const Erepfit2::OptionalEvaluationInformation* opt_info, const Erepfit2::Input::System* system, double crystal_upe);
    void writeSKFilesInfo(const QList<QPair<QString, QString>>& pairs, QTextStream& sha_stream, QDir tempFolder, const ADPT::SKFileInfo* skfileinfo, QTextStream& final_hsd);
    
protected:
    QTextStream& m_output_stream;
    QString m_dftb_path;
};



}

#endif // DFTBPLUSEVALUATOR_H
