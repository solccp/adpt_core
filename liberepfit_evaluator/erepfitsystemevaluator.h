#ifndef EREPFITSYSTEMEVALUATOR_H
#define EREPFITSYSTEMEVALUATOR_H

#include <QTextStream>
#include <QDir>

#include "AbstractDFTBEvaluator.h"
#include "erepfit2_inputdata.h"
#include "basic_types.h"


namespace Erepfit2
{

class ErepfitSystemEvaluator
{
public:
    ErepfitSystemEvaluator(QTextStream& output_stream);
    void evaluate(Input::Erepfit2_InputData* input);
    QHash<QString, Input::SystemElectronicData> m_results;
    void setScratchRoot(const QString& path);
protected:
    void init_SK();
    void init_evaluator();
    void prehandle();
    void evaluateSystems();
    void evaluateAtoms();
    void update_result();
    void initializeOptInfo(OptionalEvaluationInformation &opt_info);

private:
    QTextStream& m_output_stream;
    Input::Erepfit2_InputData* m_input_data;
private:
   QHash<QString, std::shared_ptr<Input::System> > m_uuid_system_mapping;
   QHash<QString, QString> m_system_uuid_name_mapping;

private:
   std::shared_ptr<AbstractDFTBEvaluator> m_evaluator;
   QDir scratchDir;
};
}

#endif // EREPFITSYSTEMEVALUATOR_H
