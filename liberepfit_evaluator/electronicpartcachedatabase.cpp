#include "electronicpartcachedatabase.h"
#include <QDebug>
#include <QThread>
#include <QFile>
#include <QDataStream>


ElectronicPartCacheDatabase::ElectronicPartCacheDatabase()
{

}

void ElectronicPartCacheDatabase::setEnable(bool value)
{
    std::lock_guard<std::mutex> _(setEnable_mut());
    isEnable = value;
}

bool ElectronicPartCacheDatabase::hasKey(const QString &key)
{
    std::lock_guard<std::mutex> _(hasKey_mut());
    if (!isEnable)
    {
        return false;
    }
    return m_database.contains(key);
}

void ElectronicPartCacheDatabase::addKey(const QString &key, const QByteArray &value)
{
    std::lock_guard<std::mutex> _(addKey_mut());
    if (!m_database.contains(key))
    {
        if (m_database.size() < 10000)
        {
            m_database.insert(key, value);
            save();
        }
    }
    else
    {
    }
}

QByteArray ElectronicPartCacheDatabase::getValue(const QString &key)
{
    std::lock_guard<std::mutex> _(getValue_mut());
    if (m_database.contains(key))
        return m_database[key].toByteArray();
    return QByteArray();
}

void ElectronicPartCacheDatabase::save()
{
    QFile cache("erepfit.cache");
    if (cache.open(QIODevice::WriteOnly))
    {
        QByteArray array;
        QDataStream stream(&array, QIODevice::WriteOnly);
        stream << this->m_database;
        cache.write(array);
    }
}

void ElectronicPartCacheDatabase::load()
{
    QFile cache("erepfit.cache");
    if (cache.open(QIODevice::ReadOnly))
    {
        QByteArray array = cache.readAll();
        QDataStream stream(array);
        stream >> this->m_database;

        qInfo() << "Erepfit cache loaded: " << m_database.size() << " systems";

    }
}
