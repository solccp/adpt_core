#include <QCoreApplication>
#include <QDebug>

#include "erepfit2_inputdata.h"
#include "erepfit2_inputdata_json.h"

#include <Variant/Variant.h>
#include <Variant/SchemaLoader.h>
#include <variantqt.h>

#include <QString>
#include <QPair>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QCommandLineParser>

#include "erepfitsystemevaluator.h"

void myMessageOutput(QtMsgType , const QMessageLogContext &, const QString &)
{
}

libvariant::Variant readSchema(const QString& uri)
{
    QFile schema_file(uri);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    return libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
}

bool copy_dir_recursive(QString from_dir, QString to_dir, bool replace_on_conflit)
{
    QDir dir;
    dir.setPath(from_dir);

    from_dir += QDir::separator();
    to_dir += QDir::separator();

    foreach (QString copy_file, dir.entryList(QDir::Files))
    {
        QString from = from_dir + copy_file;
        QString to = to_dir + copy_file;

        if (QFile::exists(to))
        {
            if (replace_on_conflit)
            {
                if (QFile::remove(to) == false)
                {
                    return false;
                }
            }
            else
            {
                continue;
            }
        }

        if (QFile::copy(from, to) == false)
        {
            return false;
        }
    }

    foreach (QString copy_dir, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString from = from_dir + copy_dir;
        QString to = to_dir + copy_dir;

        if (dir.mkpath(to) == false)
        {
            return false;
        }

        if (copy_dir_recursive(from, to, replace_on_conflit) == false)
        {
            return false;
        }
    }

    return true;
}



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationVersion(QString(GIT_VERSION));
    a.setApplicationName("ErepfitEvaluator");


    QTextStream stdout_stream(stdout);


    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("input", "Input file");

    QCommandLineOption yamlOption("y", "Parse input file as the YAML format.");
    parser.addOption(yamlOption);

    QCommandLineOption debugOption("d", "Debug output");
    parser.addOption(debugOption);

    QCommandLineOption scratchPathOption(QStringList() << "s" << "scratch-path",
           QCoreApplication::translate("main", "Scratch directory"),
           QCoreApplication::translate("main", "scratch"));
    parser.addOption(scratchPathOption);


    // Process the actual command line arguments given by the user
    parser.process(a);

    if (parser.positionalArguments().empty())
    {
        parser.showHelp(1);
    }

    QString inputfilename = parser.positionalArguments().first();

    bool useJsonInput = true;
    if (parser.isSet(yamlOption))
    {
        useJsonInput = false;
    }

    Erepfit2::Input::Erepfit2_InputData erepfit2_input;


    try
    {
        stdout_stream << "Reading input files..." << endl;

        QFileInfo input(inputfilename);
        if (!input.exists() || !input.isReadable())
        {
            throw std::runtime_error(QString("File %1 is not existing or not readable .").arg(input.absoluteFilePath()).toStdString());
        }
        libvariant::Variant var;
        if (useJsonInput)
        {
            var = libvariant::DeserializeJSONFile(input.absoluteFilePath().toStdString().c_str());
        }
        else
        {
            var = libvariant::DeserializeYAMLFile(input.absoluteFilePath().toStdString().c_str());
        }
        stdout_stream << "Input file parsed." << endl;

        libvariant::Variant schema = readSchema(":/schema/erepfit_input_schema.json");
        libvariant::AdvSchemaLoader loader;
        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);

        if (result.Error())
        {
            auto schema_errors = result.GetSchemaErrors();
            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(std::cout);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(std::cout);
            }
            return 1;
        }

        stdout_stream << "Input file syntax OK." << endl;

        libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::fromVariant(var, erepfit2_input);

        stdout_stream << "Input file parsed." << endl;
    }
    catch(const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }

    if (parser.isSet(debugOption))
    {
        erepfit2_input.options.debug = true;
    }

    QString subfolder_name = QString("%1-%2").arg("erepfit").arg(QCoreApplication::applicationPid());
    QDir scratchDir = QDir::temp().absoluteFilePath(subfolder_name);

    if (parser.isSet(scratchPathOption))
    {
        QString scrPath = parser.value(scratchPathOption);
        QFileInfo l_temp(scrPath);
        if (l_temp.isDir() && l_temp.isWritable())
        {
            scratchDir.setPath(QDir(scrPath).absoluteFilePath(subfolder_name));
        }
    }

    scratchDir.mkpath(scratchDir.absolutePath());

    erepfit2_input.makeAbsolutePath(QDir::current());


    Erepfit2::ErepfitSystemEvaluator erepfit_evaluator(stdout_stream);
    erepfit_evaluator.setScratchRoot(scratchDir.absolutePath());


    try
    {
        erepfit_evaluator.evaluate(&erepfit2_input);

        libvariant::Variant var = libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(erepfit2_input);
        if (useJsonInput)
        {
            libvariant::SerializeJSON(("evaluated_input.json"), var, true );
        }
        else
        {
            libvariant::SerializeYAML(("evaluated_input.yaml"), var);
        }

        if (erepfit2_input.options.debug)
        {
            copy_dir_recursive(scratchDir.absolutePath(), QDir::current().absoluteFilePath(subfolder_name), true);
            scratchDir.removeRecursively();
        }

        stdout_stream << "Normal Termination" << endl;

    }
    catch(std::exception & e)
    {
        stdout_stream << "Error:" << e.what() << endl;
        return 1;
    }

    return 0;
}
