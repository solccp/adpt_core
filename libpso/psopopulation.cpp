#include "psopopulation.h"
#include "psovector.h"
#include <QDebug>
#include <cmath>
#include <algorithm>
#include <QThreadPool>
#include <iostream>

#define SMP

namespace libPSO
{

PSOPopulation::PSOPopulation(int size, const std::shared_ptr<PSOVector>& particle)
{
//    qDebug() << "PSOPopulation::ctor" ;
    m_size = size;
    m_positions.resize(m_size);
    m_velocities.resize(m_size);
    m_values.resize(m_size);

#ifdef DEMO
    m_previous_position.resize(m_size);
    m_previous_velocities.resize(m_size);
    m_velocities_local.resize(m_size);
    m_velocities_global.resize(m_size);
#endif

    for(int i=0; i<m_size;++i)
    {
        m_positions[i] = particle->clone();
        m_velocities[i] = particle->clone();

#ifdef DEMO
    m_previous_position[i] = particle->clone();
    m_previous_velocities[i] = particle->clone();
    m_velocities_local[i] = particle->clone();
    m_velocities_global[i] = particle->clone();
#endif

    }
}

class Eval_g_runnable : public QRunnable
{
public:
    Eval_g_runnable(Evaluator eval, const std::shared_ptr<PSOVector>& vec, PSOFitnessValue& value):m_eval(eval), m_vec(vec), m_value(value)
    {
        int a = 1;
    }
    Evaluator m_eval;
    std::shared_ptr<PSOVector> m_vec;
    PSOFitnessValue &m_value;
    void run()
    {
        m_value = 0.0;
        m_value = m_eval(m_vec);
        for(int i=0; i<m_value.dimension();++i)
        {
            if (std::isnan(m_value[i]))
            {
                 m_value[i] = 1.0e15;
            }
        }

    }
};

void PSOPopulation::evaluate(int iteration, Evaluator eval)
{
    updateGuesses(iteration);

#ifdef SMP
    QThreadPool *pool = QThreadPool::globalInstance();
    for(int i=0; i<m_positions.size();++i)
    {
        Eval_g_runnable *eval_g = new Eval_g_runnable(eval, m_positions[i], m_values[i]);
        eval_g->setAutoDelete(true);
        pool->start(eval_g);
    }
    pool->waitForDone();
#else
    for(int i=0; i<m_positions.size();++i)
    {
        m_values[i] = eval(m_positions[i]);
    }
#endif
}

void PSOPopulation::initialize()
{
    for(int i=0; i<m_size;++i)
    {
        m_positions[i]->randomizePosition();
        m_velocities[i]->randomizeVelocity();
    }
    m_initialized = true;
}

bool PSOPopulation::initialized()
{
    return m_initialized;
}



void PSOPopulation::updatePosition()
{
    if (m_positionUpdater)
    {
        for(int i=0; i<m_size; ++i)
        {           
            Vector<double> icp = m_positions[i]->encode();
            Vector<double> icv = m_velocities[i]->encode();

            if (m_positions[i]->isFailed())
            {
                m_positions[i]->randomizePosition();
                m_velocities[i]->randomizeVelocity();
                m_positions[i]->unsetFailed();
            }
            else
            {
                m_positionUpdater(&icp, &icv);
                m_positions[i]->decode(icp);
                m_velocities[i]->decode(icv);
            }
        }
    }
}

void PSOPopulation::stablizeVelocity()
{
    for(int i=0; i<m_size; ++i)
    {
        m_velocities[i]->stablizeVelocity();
    }
}

void PSOPopulation::stablizePosition()
{
    for(int i=0; i<m_size; ++i)
    {
        m_positions[i]->stablizePosition( m_velocities[i].get());
    }
}

void PSOPopulation::setVelocityUpdater(VelocityUpdater updater)
{
    m_velocityUpdater = updater;
}

void PSOPopulation::setPositionUpdater(PositionUpdater updater)
{
    m_positionUpdater = updater;
}

void PSOPopulation::zeroVelocity()
{
    for(int i=0; i<m_velocities.size();++i)
    {
        Vector<double> vec = m_velocities[i]->encode();
        vec.fill(0.0);
        m_velocities[i]->decode(vec);
    }
}

PSOPopulation::~PSOPopulation()
{
}


void PSOPopulation::printVectors()
{
    for(int i=0; i<m_positions.size();++i)
    {
        qDebug() << m_positions[i]->encode();
    }
}

int PSOPopulation::getNumOfInfeasible() const
{
    int res = 0;
    for(int i=0; i<m_positions.size();++i)
    {
        if (m_positions[i]->isFailed())
            ++res;
    }
    return res;
}

PSOFitnessValue PSOPopulation::getAvgFitness() const
{
    PSOFitnessValue res;
    int nonFailed = 0;
    int maxDimension = 0;
    for(int i=0; i<m_size;++i)
    {
        if (!m_positions[i]->isFailed())
        {
            if (m_values[i].dimension() > maxDimension)
            {
                maxDimension = m_values[i].dimension();
            }
        }
    }
    res.setDimension(maxDimension);
    for(int i=0; i<m_size;++i)
    {
        if (!m_positions[i]->isFailed())
        {
            if (m_values[i].dimension() < maxDimension)
            {
                continue;
            }
            res += m_values[i];
            ++nonFailed;
        }
    }
    if (nonFailed > 0)
        res /= static_cast<double>(nonFailed);

    return res;
}

QList<int> PSOPopulation::getSortedRepoIndexByFitness()
{
    QList<int> indexes;
    QList<int> failed_indexes;
    for(int i=0; i<m_positions.size();++i)
    {
        if (!m_positions[i]->isFailed())
        {
            indexes.append(i);
        }
        else
        {
            failed_indexes.append(i);
        }
    }

    std::stable_sort(indexes.begin(), indexes.end(),
         [&](int a, int b) -> bool
    {
        return m_values[a].fitness()[0] < m_values[b].fitness()[0];
    });

    indexes.append(failed_indexes);
    return indexes;
}



int PSOPopulation::numOfParticles() const
{
    return m_size;
}

const PSOFitnessValue &PSOPopulation::fitness(int index) const
{
    return m_values[index];
}

void PSOPopulation::addGuess(const std::shared_ptr<const PSOVector> &guess, int startingIteration, int numCopies, double spreading)
{
    GuessInfo info;
    info.vector = guess;
    info.spreading = spreading;
    info.startingIteration = startingIteration;
    info.numcopys = numCopies;
    m_guesses.append(info);
}

std::shared_ptr<PSOVector> PSOPopulation::particlePosition(int index)
{
    return m_positions[index];
}

std::shared_ptr<const PSOVector> PSOPopulation::particlePosition(int index) const
{
    return m_positions[index];
}

std::shared_ptr<const PSOVector> PSOPopulation::particleVelocity(int index) const
{
    return m_velocities[index];
}

std::shared_ptr<PSOVector> PSOPopulation::particleVelocity(int index)
{
    return m_velocities[index];
}


void PSOPopulation::updateGuesses(int iteration)
{
    QVector<std::shared_ptr<PSOVector>> vectors = m_positions;

//    std::random_device rd;
//    std::mt19937 g(rd());
//    std::shuffle(vectors.begin(), vectors.end(), g);

    for(int i=0; i<m_guesses.size(); ++i)
    {
        if (m_guesses.at(i).startingIteration == iteration)
        {
            std::cout << "Introducing guess # " << i+1 << "...";
            bool first = true;
            int nCopy = m_guesses.at(i).numcopys;
            while(nCopy > 0 && !vectors.isEmpty())
            {
                auto vector =  vectors.first();
                vector->copyfrom(m_guesses.at(i).vector.get());
                vector->unsetFailed();
                if (first)
                {
                    first = false;
                }
                else
                {
                    vector->spread(m_guesses.at(i).spreading);
                }
                for(int j=0; j<m_positions.size();++j)
                {
                    if (vector == m_positions[j])
                    {
                        auto vec = m_velocities[j]->encode();
                        vec = 0.0;
                        m_velocities[j]->decode(vec);
                    }
                }

                vectors.pop_front();
                nCopy--;
            }
            std::cout << "done" << std::endl;
        }
    }


}


}
