#include "MOPSOAlgorithm.h"
#include "psopopulation.h"
#include "psovector.h"
#include "RandomNumberGenerator.h"
#include "Singleton.h"
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <QDebug>

namespace libPSO
{

MOPSOAlgorithm::MOPSOAlgorithm(const std::shared_ptr<PSOPopulation>& pop, int maxRepoSize) : PSOAlgorithm(pop)
{
    m_MaxRepoSize = maxRepoSize;
    if (m_MaxRepoSize != -1 && m_MaxRepoSize < 10)
    {
        m_MaxRepoSize = 10;
    }

    int m_size = m_pop->numOfParticles();
    m_personalBestPositions.clear();
    for(int i=0; i<m_size; ++i)
    {
        m_personalBestPositions.append(m_pop->particlePosition(i)->clone());
    }
    m_personalBestValues.resize(m_size);
}

MOPSOAlgorithm::~MOPSOAlgorithm()
{
//    for(int i=0; i<m_personalBestPositions.size(); ++i)
//    {
//        delete m_personalBestPositions[i];
//    }
//    for(int j=repository.size()-1; j>=0 ; --j)
//    {
//        delete repository[j].first;
//        repository[j].first = nullptr;
//    }
    repository.clear();
}

void getRandomIndex(QVector<int> &index, int size)
{
    RandomNumberRenerator<int>& gen_int = Singleton<RandomNumberRenerator<int> >::Instance();
    index.resize(size);
    int n(0);
    std::generate(std::begin(index), std::end(index), [&]{ return n++; });
    for(int i=0; i<index.size();++i)
    {
        int ran = gen_int.rand_int(i, size-1);
        std::swap(index[i],index[ran]);
    }
}

void MOPSOAlgorithm::updateVelocity(int iter)
{
    int m_size = m_pop->numOfParticles();
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();

     for(int i=0; i<m_size; ++i)
    {
        Vector<double> icp = m_pop->particlePosition(i)->encode();
        Vector<double> icv = m_pop->particleVelocity(i)->encode();

        Vector<double> gbp = m_globalBestPositions[i]->encode();
        Vector<double> pbp = m_personalBestPositions[i]->encode();

        double w = 0.9 - 0.5*(static_cast<double>(iter)/static_cast<double>(m_maxIter));
        Vector<double> rand1, rand2;
        rand1.resize(icp.size());
        rand2.resize(icp.size());
        std::generate(rand1.begin(), rand1.end(), [&](){return gen.rand_real(0.0,1.0);});
        std::generate(rand2.begin(), rand2.end(), [&](){return gen.rand_real(0.0,1.0);});

        Vector<double> res = (icv)*w + rand1*((pbp)-(icp))*c1 + rand2*((gbp)-(icp))*c2;

        m_pop->particleVelocity(i)->decode(res);
    }
}

void MOPSOAlgorithm::updatePBest(int iter)
{
    RandomNumberRenerator<int>& gen_int = Singleton<RandomNumberRenerator<int> >::Instance();
    Q_UNUSED(iter);
    for(int i=0; i<m_pop->numOfParticles();++i)
    {
        if (m_pop->particlePosition(i)->isFailed() || !m_pop->fitness(i).isOptimalCandidate())
        {
            int gindex = gen_int.rand_int(0,repository.size()-1);
            m_personalBestPositions[i]->copyfrom(repository[gindex].first.get());
            m_personalBestValues[i] = repository[gindex].second;
        }
        else
        {
            const PSOFitnessValue& my_value = m_pop->fitness(i);
            if (my_value.dimension() > m_personalBestValues[i].dimension())
            {
                m_personalBestPositions[i]->copyfrom(m_pop->particlePosition(i).get());
                m_personalBestValues[i] = my_value;
            }
            else if (my_value.dimension() < m_personalBestValues[i].dimension())
            {
                continue;
            }
            else if (my_value < m_personalBestValues[i])
            {
                m_personalBestPositions[i]->copyfrom(m_pop->particlePosition(i).get());
                m_personalBestValues[i] = my_value;
            }
        }

    }
}

void MOPSOAlgorithm::updateGBest(int iter)
{
    findGBest(iter);
}




void MOPSOAlgorithm::mutate(int iter)
{
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();
    RandomNumberRenerator<int>& gen_int = Singleton<RandomNumberRenerator<int> >::Instance();

    double rate = mutationRateStart + (mutationRateEnd-mutationRateStart)*(static_cast<double>(iter)/static_cast<double>(m_maxIter));

    qDebug() << "Current disturbuance rate: " << rate;
    int mutateParticles = m_pop->numOfParticles()*rate;
    if (mutateParticles == 0)
        mutateParticles = 1;
    qDebug() << "Number of disturbed particles: " << mutateParticles;

    QVector<int> randomIndex_particle;
    getRandomIndex(randomIndex_particle, m_pop->numOfParticles());
    int particleIndex = 0;

    while(mutateParticles > 0)
    {
        int index = randomIndex_particle[particleIndex];
        particleIndex++;

        if (particleIndex == m_pop->numOfParticles())
        {
            break;
        }

        if (m_pop->particlePosition(index)->isFailed())
        {
            continue;
        }

        qDebug() << "Current selected particle index: " << index;


        Vector<double> pos = m_pop->particlePosition(index)->encode();
        int k = gen_int.rand_int(1, pos.size());
        qDebug() << "Number of disturbed dimensions: " << k;


        QVector<int> randomIndex_dimension(pos.size());
        getRandomIndex(randomIndex_dimension, pos.size());
        for(int i=0; i<k; ++i)
        {
            int dindex = randomIndex_dimension[i];

            double rn = gen.rand_read_normal(0.5, 1.0, 0.0, 1.0);
            if (rn < 0.5)
            {
                rn = gen.rand_read_normal(0.5, 1.0, 0.0, 1.0);
                double lb = m_pop->particlePosition(index)->getLowerBound(dindex);
                pos[dindex]=pos[dindex]- (pos[dindex]-lb)*rn;
            }
            else
            {
                rn = gen.rand_read_normal(0.5, 1.0, 0.0, 1.0);
                double ub = m_pop->particlePosition(index)->getUpperBound(dindex) ;
                pos[dindex]= pos[dindex] + (ub-pos[dindex])*rn;
            }
        }
        m_pop->particlePosition(index)->decode(pos);
//        m_pop->particlePosition(index)->stablizePosition(nullptr);
        mutateParticles--;
    }

}

void MOPSOAlgorithm::resizeRepo()
{
    qDebug() << "before resizing : " << repository.size();
    if (repository.empty())
        return;

    if (repository.size() <3)
    {
        return;
    }

    QList<double> NRs;

    for(int i=1; i<repository.size()-1; ++i)
    {
        NRs.append( repository[i-1].second.SRD(repository[i].second) + repository[i].second.SRD(repository[i+1].second) );
    }

    while(repository.size() > m_MaxRepoSize)
    {
        int min_index = std::min_element(NRs.begin(), NRs.end())-NRs.begin();
//        delete repository[1+min_index].first;
        repository.removeAt(1+min_index);
        NRs.removeAt(min_index);
    }

    qDebug() << "after resizing : " << repository.size();
}

void MOPSOAlgorithm::evolve()
{
    if (!m_pop->initialized())
    {
        m_pop->initialize();
        m_pop->zeroVelocity();
    }
    if(m_progressUpdater)
    {
        m_progressUpdater(this->shared_from_this(),0);
    }
    for(int i=1; i<=m_maxIter; ++i)
    {
        qDebug() << "Evaluting particles";
        m_pop->evaluate(i,m_evaluator);

        qDebug() << "Updating External Repository";
        updateRepo(i);

        if (m_MaxRepoSize != -1 && repository.size() > m_MaxRepoSize)
        {
            qDebug() << "The number of solutions in repository is excess the limit (" << m_MaxRepoSize << ")";
//            qDebug() << "Removing similar non-dominated solutions...";
            resizeRepo();
        }


        if(m_progressUpdater)
        {
            m_progressUpdater(this->shared_from_this(),i);
        }

        if (repository.empty())
        {
            throw  std::runtime_error("No valid particles in repository. STOP.");
        }

        qDebug() << "Updating PBest with nearest point in repository";
        updatePBest(i);

        qDebug() << "Updating GBest";
        updateGBest(i);

        qDebug() << "Updating Velocities";
        updateVelocity(i);

        m_pop->stablizeVelocity();

        qDebug() << "Updating Positions";
        m_pop->updatePosition();
        m_pop->stablizePosition();

        qDebug() << "Doing mutation";
        mutate(i);



    }
}

void MOPSOAlgorithm::updateRepo(int iter)
{
    Q_UNUSED(iter);

    QVector<QString> headers;

    for(int i=0; i<m_pop->numOfParticles();++i)
    {
        if (m_pop->fitness(i).headers().size() > headers.size())
        {
            headers = m_pop->fitness(i).headers();
        }
    }
    for(int i=0; i<m_pop->numOfParticles();++i)
    {
        if (m_pop->fitness(i).headers().size() < headers.size())
        {
            m_pop->particlePosition(i)->setFailed("Wrong fitness dimension");
        }
    }

    for(int i=0; i<m_pop->numOfParticles();++i)
    {
        if (m_pop->particlePosition(i)->isFailed())
        {
            continue;
        }
        const PSOFitnessValue& value = m_pop->fitness(i);
        if (!value.isOptimalCandidate())
        {
            continue;
        }

        if (m_pop->fitness(i).headers() != headers)
        {
            continue;
        }

        bool shouldBeAdded = true;
        for(int j=0; j<repository.size(); ++j)
        {
            const PSOFitnessValue& repo_value = repository[j].second;
            if ( repo_value < value)
            {
                shouldBeAdded = false;
                break;
            }
        }

        if (shouldBeAdded)
        {
            for(int j=repository.size()-1; j>=0 ; --j)
            {
                const PSOFitnessValue& repo_value = repository[j].second;
                if (value < repo_value)
                {
                    std::cout << "Particle #" << j+1 << " in repository is dominated!" << std::endl;
//                    delete repository[j].first;
                    repository[j].first = nullptr;
                    repository.removeAt(j);
                }
            }
            repository.append(qMakePair(m_pop->particlePosition(i)->clone(), value));
        }
    }
}

void MOPSOAlgorithm::resizeRepo_CHI()
{

    qDebug() << "before resizing : " << repository.size();
    if (repository.empty())
        return;

    int fitness_dim = repository.first().second.dimension();
    double r = 0.0;
    auto getMinMax = [&](int index, double& min, double& max)
    {
        for(int i=0; i<repository.size();++i)
        {
            double tmp = repository[i].second.fitness()[index];
            if (!repository[i].second.isOptimalCandidate())
                continue;
            if ( tmp < min)
            {
                min = tmp;
            }
            if (repository[i].second.fitness()[index] > max)
            {
                max = tmp;
            }
        }
    };

    for(int i=0; i<fitness_dim-1; ++i)
    {
        double x1 = 1.0e12; //fmin_i
        double y1 = 0.0;    //fmax_i+1
        double x2 = 0.0;    //fmax_i
        double y2 = 1.0e12; //fmin_i+1

        getMinMax(i, x1, x2);
        getMinMax(i+1, y2, y1);

        r += sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    }
    r /= (3.0*static_cast<double>(repository.size()));
    qDebug() << "Radius: " << r ;


    QList<QPair<std::shared_ptr<PSOVector>, PSOFitnessValue>> tempRepo;
    tempRepo.swap(repository);

    for(int i=0; i<tempRepo.size();++i)
    {
        bool beingRemoved = false;
        for(int j=0; j<repository.size();++j)
        {
            double dis = tempRepo[i].second.dis(repository[j].second);
//            qDebug() << "d(i, j) = " << dis;
            if (dis <= r)
            {
//                qDebug() << "It belongs to some cluster, removing...";
                beingRemoved = true;
                break;
            }
        }
        if (beingRemoved)
        {

//            delete tempRepo[i].first;
            continue;
        }
        else
        {
            repository.append(tempRepo[i]);
        }
    }

    qDebug() << "after resizing : " << repository.size();
}

void MOPSOAlgorithm::findGBest_random()
{
    int m_size = m_pop->numOfParticles();

    const int MaxN = 4;

    m_globalBestPositions.clear();
    for(int i=0; i<m_size; ++i)
    {
        Vector<double> icp = m_pop->particlePosition(i)->encode();

        QVector<int> randomIndex;
        int N = std::min(repository.size(), MaxN);
        getRandomIndex(randomIndex, N);

        int gindex = 0;
        double min_distance = 1e20;
        for(int j=0; j<randomIndex.size();++j)
        {
            Vector<double> pos_repo = repository.at(randomIndex[j]).first->encode();
            double distance = icp.dis(pos_repo);
            if (distance < min_distance)
            {
                min_distance = distance;
                gindex = randomIndex[j];
            }
        }

        m_globalBestPositions.append(repository.at(gindex).first);
    }
}

void MOPSOAlgorithm::findGBest_old(int iter)
{
    RandomNumberRenerator<int>& gen_int = Singleton<RandomNumberRenerator<int> >::Instance();
    Q_UNUSED(iter);
//    for(int i=0; i<m_pop->numOfParticles();++i)
//    {
//        if (m_pop->particlePosition(i)->isFailed() || !m_pop->fitness(i).isOptimalCandidate())
//        {
//            int gindex = gen_int.rand_int(0,repository.size()-1);
//            m_personalBestPositions[i]->copyfrom(repository[gindex].first);
//            m_personalBestValues[i] = repository[gindex].second;
//        }


    QList<int> particleSortedIndex = m_pop->getSortedRepoIndexByFitness();
    QList<int> repoSortedIndex = getSortedRepoIndexByFitness();
    QList<double> Density;
    QList<int> GBestN;


    int m_size = m_pop->numOfParticles();
    int Np = m_size;
    for(int i=0; i<m_size; ++i)
    {
        if (m_pop->particlePosition(particleSortedIndex[i])->isFailed())
        {
            Np = i;
            break;
        }
    }

    for(int i=0; i<repository.size();++i)
    {
        double first = 0.0;
        double second = 0.0;

        bool boundary = false;

        if (i>0)
        {
            first = repository[repoSortedIndex[i-1]].second.dis(repository[repoSortedIndex[i]].second);

        }
        else
        {
            boundary = true;
        }
        if (i<(repository.size()-1))
        {
            second = repository[repoSortedIndex[i]].second.dis(repository[repoSortedIndex[i+1]].second);
        }
        else
        {
            boundary = true;
        }

        double density = (first+second);
        if (!boundary)
        {
            density /= 2.0;
        }
        Density.append(density);
    }

    double total_density = std::accumulate(Density.begin(), Density.end(), 0.0);
    for(int i=0; i<repository.size();++i)
    {
        double density = Density[i];
        int GBestNi = static_cast<int>(density*Np/total_density);
        GBestN.append(GBestNi);
    }


    int Ngb = std::accumulate(GBestN.begin(), GBestN.end(), 0);
    while(Ngb != Np)
    {
        if (Ngb > Np)
        {
            auto maxpos = std::max_element(GBestN.begin(),GBestN.end());
            --(*maxpos);
        }
        else
        {
            auto minpos = std::min_element(GBestN.begin(),GBestN.end());
            ++(*minpos);
        }
        Ngb = std::accumulate(GBestN.begin(), GBestN.end(), 0);
    }

    qDebug() << "GBestN: " << GBestN ;

    //assigning gbest
    m_globalBestPositions.clear();
    m_globalBestPositions.resize(m_size);

    int index = 0;
    for(int i=0; i<GBestN.size(); ++i)
    {
        for(int j=0; j<GBestN[i];++j)
        {
            m_globalBestPositions[particleSortedIndex[index]] = repository[repoSortedIndex[i]].first;
            ++index;
        }
    }
    for(int i=Np; i<m_size;++i)
    {
        int gindex = gen_int.rand_int(0,repository.size()-1);
        m_globalBestPositions[particleSortedIndex[index]] = repository[gindex].first;
        ++index;
    }
}

void MOPSOAlgorithm::findGBest(int iter)
{
    RandomNumberRenerator<int>& gen_int = Singleton<RandomNumberRenerator<int> >::Instance();
    Q_UNUSED(iter);
    int m_size = m_pop->numOfParticles();
    m_globalBestPositions.clear();
    m_globalBestPositions.resize(m_size);
    for(int i=0; i<m_size;++i)
    {
        if (m_pop->particlePosition(i)->isFailed() || !m_pop->fitness(i).isOptimalCandidate())
        {
            int gindex = gen_int.rand_int(0,repository.size()-1);
            m_globalBestPositions[i] = repository[gindex].first;
            continue;
        }


        int min_index = 0;
        double min_srd = 1e10;
        for(int j=0; j<repository.size();++j)
        {
            double srd = m_pop->fitness(i).SRD(repository[j].second);
            if (srd < min_srd)
            {
                min_index = j;
            }
        }
        m_globalBestPositions[i]= repository[min_index].first;
    }
}

QList<int> MOPSOAlgorithm::getSortedRepoIndexByFitness()
{
    QList<int> indexes;
    for(int i=0; i< repository.size();++i)
    {
        indexes.append(i);
    }

    std::stable_sort(indexes.begin(), indexes.end(),
         [&](int a, int b) -> bool
    {
        return repository[a].second.fitness()[0] < repository[b].second.fitness()[0];
    });

    return indexes;
}

bool MOPSOAlgorithm::isMOPSO() const
{
    return true;
}

const QList<QPair<std::shared_ptr<PSOVector>, PSOFitnessValue> > &MOPSOAlgorithm::getParetoOptimals() const
{
    return repository;
}

}
