#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

#include <random>
#include<type_traits>
#include "Singleton.h"

namespace libPSO
{
template <typename T>
class RandomNumberRenerator
{
    friend class Singleton<RandomNumberRenerator<T> >;
private:
    RandomNumberRenerator(){}
public:
    void setRandomSeed(int seed)
    {
        eng.seed(seed);
    }
    T rand_int(T Min, T Max)
    {
        std::uniform_int_distribution<> dist(Min, Max);
        return dist(eng);
    } 

    T rand_real(T Min,T Max)
    {
        std::uniform_real_distribution<> dist(Min, Max);
        return dist(eng);
    }

    T rand_read_normal(T mean, T stddev, T min, T max)
    {
        std::normal_distribution<> dist(mean, stddev);
        while(true)
        {
            double value = dist(eng);
            if (value >= min && value <= max)
            {
                return value;
            }
        }
    }


private:
    std::mt19937_64 eng;
};
}

#endif // RANDOMNUMBERGENERATOR_H

