#ifndef PSO1DVECTOR_H
#define PSO1DVECTOR_H

#include "vector.h"
#include "psovector.h"
#include "typedefs.h"


namespace libPSO
{

class PSO1DVector : public PSOVector
{
    friend class PSO1DVectorRangeController;
public:
    PSO1DVector(int size, double min, double max);
    PSO1DVector(const PSO1DVector& rhs) = delete;
    virtual int size() const;
    virtual std::shared_ptr<PSOVector> clone();
    virtual ~PSO1DVector();
    virtual Vector<double> encode() const;
    virtual void decode(const Vector<double>& vector);
    double& operator[](int index);
    double at(int index) const;
    virtual void copyfrom(const PSOVector* rhs);
    virtual void spread(double factor);
protected:
    Vector<double> m_data;


//controller part;

public:
    virtual double getPosMin() const;
    virtual double getPosMax() const;
    virtual void setPosMin(double value);
    virtual void setPosMax(double value);

    virtual void adjustVelocityRanges();
protected:
    double m_posmin, m_posmax;
    double m_velmin, m_velmax;
public:
    virtual void randomizePosition() ;
    virtual void randomizeVelocity() ;
    virtual void stablizeVelocity() ;
    virtual void stablizePosition(PSOVector *vel) ;
    virtual void stablizePosition() ;
    virtual void copyControllerFrom(const PSOVector *rhs) ;

    // PSOVector interface
public:
    virtual double getLowerBound(int index);
    virtual double getUpperBound(int index);
};
}

#endif // PSO1DVECTOR_H
