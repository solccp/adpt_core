#ifndef PSOPOPULATION_H
#define PSOPOPULATION_H

#include <QVector>

#include "typedefs.h"
#include "vector.h"
#include "PSOFitnessValue.h"
#include <memory>

//#define DEMO

namespace libPSO
{

class PSOAlgorithm;
class PSOVector;
class Individual;

//typedef std::function<PSOFitnessValue (const std::shared_ptr<PSOVector>)> Evaluator;
typedef std::function<void (int, Vector<double>*, Vector<double>*)> VelocityUpdater;
typedef std::function<void (Vector<double>*, const Vector<double>*)> PositionUpdater;

struct GuessInfo
{
    std::shared_ptr<const PSOVector> vector;
    int startingIteration;
    int numcopys;
    double spreading;
};

class PSOPopulation
{
public:
    PSOPopulation(int size, const std::shared_ptr<PSOVector>& particle);
    void evaluate(int iteration, Evaluator eval);
    void initialize();
    bool initialized();

    void updatePosition();
    void stablizeVelocity();
    void stablizePosition();
    void setVelocityUpdater(VelocityUpdater updater);
    void setPositionUpdater(PositionUpdater updater);
    void zeroVelocity();


    virtual ~PSOPopulation();
    void printVectors();


    int getNumOfInfeasible() const;
    PSOFitnessValue getAvgFitness() const;
    QList<int> getSortedRepoIndexByFitness();


    void addGuess(const std::shared_ptr<const PSOVector>& guess, int startingIteration, int numCopies, double spreading);

    std::shared_ptr<PSOVector> particlePosition(int index);
    std::shared_ptr<const PSOVector> particlePosition(int index) const;

    std::shared_ptr<const PSOVector> particleVelocity(int index) const;
    std::shared_ptr<PSOVector> particleVelocity(int index);

    int numOfParticles() const ;

    const PSOFitnessValue& fitness(int index) const;
private:

    QList<GuessInfo> m_guesses;
    QVector<std::shared_ptr<PSOVector>> m_positions;
    QVector<std::shared_ptr<PSOVector>> m_velocities;
    QVector<PSOFitnessValue> m_values;
    int m_size;
    VelocityUpdater m_velocityUpdater;
    PositionUpdater m_positionUpdater;
    bool m_initialized = false;
    void updateGuesses(int iteration);

#ifdef DEMO
public:
    QVector<PSOVector*> m_previous_position;
    QVector<PSOVector*> m_previous_velocities;
    QVector<PSOVector*> m_velocities_local;
    QVector<PSOVector*> m_velocities_global;
#endif


};
}

#endif // PSOPOPULATION_H
