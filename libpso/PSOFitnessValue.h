#ifndef PSOFITNESSVALUE_H
#define PSOFITNESSVALUE_H

#include <QVector>

namespace libPSO
{

class PSOFitnessValue
{
public:

    PSOFitnessValue(){}
    PSOFitnessValue(int dimension);
    PSOFitnessValue(const PSOFitnessValue& rhs);

    int dimension() const;
    int size() const;
    void setFitness(const QVector<double>& fitness);

    const QVector<double>& fitness() const;
    bool operator<(const PSOFitnessValue& rhs) const;
    PSOFitnessValue& operator=(const PSOFitnessValue& rhs);
    PSOFitnessValue& operator=(const QVector<double>& fitness);
    void setFitness(int index, double value);
    void addFitness(int index, double value);
    double fitness(int index) const;

    PSOFitnessValue& operator+=(const PSOFitnessValue& rhs);
    PSOFitnessValue& operator-=(const PSOFitnessValue& rhs);
    PSOFitnessValue& operator+=(double factor);
    PSOFitnessValue& operator-=(double factor);
    PSOFitnessValue& operator*=(double factor);
    PSOFitnessValue& operator/=(double factor);
    PSOFitnessValue& operator=(double value);

    void setDimension(int size);

    PSOFitnessValue  operator+(const PSOFitnessValue& rhs);
    PSOFitnessValue  operator-(const PSOFitnessValue& rhs);
    PSOFitnessValue  operator*(double factor);
    PSOFitnessValue  operator/(double factor);

    double& operator[](int index);
    const double& operator[](int index) const;

    void setHeader(int index, const QString& title);
    QString header(int index) const;
    const QVector<QString>& headers() const;


    bool isOptimalCandidate() const;
    void setIsOptimalCandidate(bool isOptimalCandidate);

    double dis(const PSOFitnessValue& rhs) const;
    double SRD(const PSOFitnessValue& rhs) const;
    int dominateDegree(const PSOFitnessValue& rhs) const;

    bool isFeasible() const;
    void setIsFeasible(bool isFeasible);

private:
    QVector<double> m_fitness;
    QVector<QString> m_headers;

    bool m_isFeasible = true;
    bool m_isOptimalCandidate = true;
};
}
#endif // PSOFITNESSVALUE_H
