#include "pso1dvector.h"
#include "Singleton.h"
#include "RandomNumberGenerator.h"
#include <QDebug>
#include <iostream>

namespace libPSO
{

PSO1DVector::PSO1DVector(int size, double min, double max)
{
    m_data.resize(size);
    double l_min = min;
    double l_max = max;
    if (l_min > l_max)
        std::swap(l_min, l_max);
    setPosMin(l_min);
    setPosMax(l_max);
    adjustVelocityRanges();
}

//PSO1DVector::PSO1DVector(const PSO1DVector &rhs)
//{
//    this->m_data = rhs.m_data;
//    copyControllerFrom(&rhs);
//}

inline int PSO1DVector::size() const
{
    return m_data.size();
}

std::shared_ptr<PSOVector> PSO1DVector::clone()
{
    auto res = std::make_shared<PSO1DVector>(this->m_data.size(), this->m_posmin, this->m_posmax);
    res->copyfrom(this);
    return std::static_pointer_cast<PSOVector>(res);
}


PSO1DVector::~PSO1DVector()
{
}

Vector<double> PSO1DVector::encode() const
{
    return m_data;
}

void PSO1DVector::decode(const Vector<double> &vector)
{
    m_data = vector;
}

double &PSO1DVector::operator [](int index)
{
    return m_data[index];
}

double PSO1DVector::at(int index) const
{
    return m_data.at(index);
}

void PSO1DVector::copyfrom(const PSOVector *rhs)
{
    if (this==rhs)
        return;
    PSOVector::copyfrom(rhs);
    const PSO1DVector* vector = dynamic_cast<const PSO1DVector*>(rhs);

    if(vector)
    {
        m_data = vector->m_data;
        copyControllerFrom(rhs);
    }
}

void PSO1DVector::spread(double factor)
{
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();
    for(int i=0; i<m_data.size();++i)
    {
        double dd = gen.rand_real(-factor,factor);
        m_data[i] = m_data[i] + m_data[i]*dd;
    }
}

double PSO1DVector::getPosMin() const
{
    return m_posmin;
}

double PSO1DVector::getPosMax() const
{
    return m_posmax;
}

void PSO1DVector::setPosMin(double value)
{
    m_posmin = value;
    adjustVelocityRanges();
}

void PSO1DVector::setPosMax(double value)
{
    m_posmax = value;
    adjustVelocityRanges();
}

void PSO1DVector::adjustVelocityRanges()
{
    m_velmin = -(m_posmax-m_posmin)*0.2;
    m_velmax = -m_velmin;
}

void PSO1DVector::randomizePosition()
{
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();
    for(int i=0; i< size(); ++i)
    {
        m_data[i]= gen.rand_real(m_posmin, m_posmax);
    }
}

void PSO1DVector::randomizeVelocity()
{
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();
    for(int i=0; i< size(); ++i)
    {
        m_data[i]= gen.rand_real(m_velmin, m_velmax);
    }
}

void PSO1DVector::stablizeVelocity()
{
    for(int i=0; i< size(); ++i)
    {
        m_data[i] = qBound(m_velmin, m_data[i], m_velmax);
    }
}

void PSO1DVector::stablizePosition(PSOVector *vel)
{
    PSO1DVector* vel_vector = dynamic_cast<PSO1DVector*>(vel);
    Q_ASSERT_X(vel_vector!=NULL, "PSO1DVector::stablizePosition", "Vector is not PSO1DVector");

    for(int i=0; i< m_data.size(); ++i)
    {
        if (m_data[i] > m_posmax)
        {
            m_data[i] = m_posmax;
            double tmp = m_data[i] - m_posmax;
            m_data[i] = m_posmax - tmp*0.1;

            vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
        }
        else if (m_data[i] < m_posmin)
        {
            m_data[i] = m_posmin;
            double tmp = m_posmin - m_data[i];
            m_data[i] = m_posmin + 0.1*tmp;
            vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
        }
    }
}

void PSO1DVector::stablizePosition()
{
    for(int i=0; i< m_data.size(); ++i)
    {
        if (m_data[i] > this->m_posmax)
            m_data[i] = this->m_posmax;
        if (m_data[i] < this->m_posmin)
            m_data[i] = this->m_posmin;
    }
}

void PSO1DVector::copyControllerFrom(const PSOVector *rhs)
{
    const PSO1DVector* vector = dynamic_cast<const PSO1DVector*>(rhs);
    Q_ASSERT_X(vector!=nullptr, "PSO1DVector::copyControllerFrom", "Vector is not PSO1DVector");

    this->m_posmax = vector->m_posmax;
    this->m_posmin = vector->m_posmin;

    this->m_velmax = vector->m_velmax;
    this->m_velmin = vector->m_velmin;

}

double PSO1DVector::getLowerBound(int index)
{
    Q_UNUSED(index);
    return getPosMin();
}

double PSO1DVector::getUpperBound(int index)
{
    Q_UNUSED(index);
    return getPosMax();
}

}

