#ifndef SIMPLEPSOALGORITHM_H
#define SIMPLEPSOALGORITHM_H
#include "psoalgorithm.h"
#include "PSOFitnessValue.h"

#include <memory>

namespace libPSO
{
class PSOVector;
class SimplePSOAlgorithm : public PSOAlgorithm
{
public:
    SimplePSOAlgorithm(const std::shared_ptr<PSOPopulation>& pop);
    ~SimplePSOAlgorithm();
protected:
    virtual void updateVelocity(int) ;
private:
    double c1 = 1.5;
    double c2 = 1.5;

    // PSOAlgorithm interface
protected:
    void evolve_local(int iter);


public:
    const PSOFitnessValue &getBestValue() const;
    std::shared_ptr<const PSOVector> getBestPosition() const;
    bool globalBestUpdated() const;


protected:
    virtual void updateRepo(int iter);

private:



    void setGlobalBestUpdated(bool globalBestUpdated);

    QVector<std::shared_ptr<PSOVector>> m_personalBestPositions;
    QVector<PSOFitnessValue> m_personalBestValues;
    std::shared_ptr<PSOVector> m_globalBestPosition;
    PSOFitnessValue m_globalBestValue;

    bool m_globalBestUpdated = false;


    // PSOAlgorithm interface
public:
    bool isMOPSO() const;
};
}

#endif // SIMPLEPSOALGORITHM_H
