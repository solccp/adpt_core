#ifndef PSOALGORITHM_H
#define PSOALGORITHM_H

#include "typedefs.h"
#include "vector.h"
#include <memory>

namespace libPSO
{
class PSOPopulation;
class PSOAlgorithm : public std::enable_shared_from_this<PSOAlgorithm>
{
public:
    PSOAlgorithm(const std::shared_ptr<PSOPopulation> &pop);
    void setEvaluator(Evaluator eval);
    void setProgressUpdater(ProgressUpdater prog);
    void setMaxIterations(int n_iter);
    virtual void evolve();

    std::shared_ptr<const PSOPopulation> getPopulation() const;
    virtual bool isMOPSO() const = 0;

    virtual ~PSOAlgorithm();
protected:
    virtual void updateVelocity(int) = 0;
    void updatePosition(Vector<double>* icp, const Vector<double>* icv);
    virtual void updateRepo(int iter){Q_UNUSED(iter);}

    int m_maxIter = { 100 };

    virtual void evolve_local(int iter){Q_UNUSED(iter);}
    Evaluator m_evaluator;
    std::shared_ptr<PSOPopulation> m_pop;
    ProgressUpdater m_progressUpdater;

};
}

#endif // PSOALGORITHM_H
