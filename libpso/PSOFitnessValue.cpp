#include "PSOFitnessValue.h"

#include <stdexcept>
#include <QString>
#include <cmath>

namespace libPSO
{

PSOFitnessValue::PSOFitnessValue(const PSOFitnessValue &rhs)
{
    *this = rhs;
}

PSOFitnessValue::PSOFitnessValue(int dimension)
{
    setDimension(dimension);
    m_fitness.fill(1e15);
}

int PSOFitnessValue::dimension() const
{
    return m_fitness.size();
}

int PSOFitnessValue::size() const
{
    return dimension();
}

void PSOFitnessValue::setFitness(const QVector<double> &fitness)
{
    m_fitness = fitness;
}

const QVector<double> &PSOFitnessValue::fitness() const
{
    return m_fitness;
}


bool PSOFitnessValue::operator<(const PSOFitnessValue &rhs) const
{
    if (this->m_fitness.size() != rhs.m_fitness.size())
    {
        Q_ASSERT(this->m_fitness.size() == rhs.m_fitness.size());
        throw std::runtime_error("size inconsistent in PSOFitnessValue::operator<");
    }
    for(int i=0; i<m_fitness.size(); ++i)
    {
        if (m_fitness[i] > rhs.m_fitness[i])
        {
            return false;
        }
    }
    return true;
}

PSOFitnessValue &PSOFitnessValue::operator=(const PSOFitnessValue &rhs)
{
    m_fitness = rhs.m_fitness;
    m_isOptimalCandidate = rhs.m_isOptimalCandidate;
    m_headers = rhs.m_headers;
    return *this;
}

void PSOFitnessValue::setDimension(int size)
{
    if (m_fitness.size()==size)
        return;
    m_fitness.resize(size);
    if (size > m_headers.size())
    {
        m_headers.resize(size);
    }
}

PSOFitnessValue PSOFitnessValue::operator+(const PSOFitnessValue &rhs)
{
    PSOFitnessValue res(*this);
    res += rhs;
    return res;
}

PSOFitnessValue PSOFitnessValue::operator-(const PSOFitnessValue &rhs)
{
    PSOFitnessValue res(*this);
    res -= rhs;
    return res;
}

PSOFitnessValue PSOFitnessValue::operator*(double factor)
{
    PSOFitnessValue res(*this);
    res *= factor;
    return res;
}

PSOFitnessValue PSOFitnessValue::operator/(double factor)
{
    PSOFitnessValue res(*this);
    res /= factor;
    return res;
}

double &PSOFitnessValue::operator[](int index)
{
    return m_fitness[index];
}

const double &PSOFitnessValue::operator[](int index) const
{
    return m_fitness[index];
}

void PSOFitnessValue::setHeader(int index, const QString &title)
{
    m_headers[index] = title;
}

QString PSOFitnessValue::header(int index) const
{
    if (index>=0 && index < m_headers.size())
    {
        return m_headers[index];
    }
    return "";
}

const QVector<QString> &PSOFitnessValue::headers() const
{
    return m_headers;
}


bool PSOFitnessValue::isOptimalCandidate() const
{
    return m_isOptimalCandidate;
}

void PSOFitnessValue::setIsOptimalCandidate(bool isOptimalCandidate)
{
    m_isOptimalCandidate = isOptimalCandidate;
}

double PSOFitnessValue::dis(const PSOFitnessValue &rhs) const
{
    double res = 0.0;
    if (this->m_fitness.size() != rhs.m_fitness.size())
    {
        Q_ASSERT(this->m_fitness.size() == rhs.m_fitness.size());
        throw std::runtime_error("size inconsistent in PSOFitnessValue::dis");
    }

    for(int i=0; i<m_fitness.size(); ++i)
    {
        double diff = (m_fitness[i] -rhs.m_fitness[i]);
        res += diff*diff;
    }
    return std::sqrt(res);
}

double PSOFitnessValue::SRD(const PSOFitnessValue &rhs) const
{
    if (this->m_fitness.size() != rhs.m_fitness.size())
    {
        Q_ASSERT(this->m_fitness.size() == rhs.m_fitness.size());
        throw std::runtime_error("size inconsistent in PSOFitnessValue::SRD");
    }
    double res = 0.0;
    for(int i=0; i<m_fitness.size(); ++i)
    {
        double diff = (m_fitness[i] -rhs.m_fitness[i]);
        res += std::sqrt(std::abs(diff));
    }
    return res;
}

int PSOFitnessValue::dominateDegree(const PSOFitnessValue &rhs) const
{
    int res = 0;
    for(int i=0;i<m_fitness.size();++i)
    {
        if (m_fitness[i] < rhs.m_fitness[i])
        {
            res++;
        }
    }
    return res;
}
bool PSOFitnessValue::isFeasible() const
{
    return m_isFeasible;
}

void PSOFitnessValue::setIsFeasible(bool isFeasible)
{
    m_isFeasible = isFeasible;
}


PSOFitnessValue &PSOFitnessValue::operator=(const QVector<double> &fitness)
{
    m_fitness = fitness;
    return *this;
}

void PSOFitnessValue::setFitness(int index, double value)
{
    m_fitness[index] = value;
}

void PSOFitnessValue::addFitness(int index, double value)
{
    m_fitness[index] += value;
}

double PSOFitnessValue::fitness(int index) const
{
    return m_fitness[index];
}

PSOFitnessValue &PSOFitnessValue::operator+=(const PSOFitnessValue &rhs)
{
    if (this->m_fitness.size() != rhs.m_fitness.size())
    {
        Q_ASSERT(this->m_fitness.size() == rhs.m_fitness.size());
        throw std::runtime_error("size inconsistent in PSOFitnessValue::operator<");
    }
    for(int i=0; i<m_fitness.size(); ++i)
    {
        m_fitness[i] += rhs.m_fitness[i];
    }
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator-=(const PSOFitnessValue &rhs)
{
    if (this->m_fitness.size() != rhs.m_fitness.size())
    {
        Q_ASSERT(this->m_fitness.size() == rhs.m_fitness.size());
        throw std::runtime_error("size inconsistent in PSOFitnessValue::operator<");
    }
    for(int i=0; i<m_fitness.size(); ++i)
    {
        m_fitness[i] -= rhs.m_fitness[i];
    }
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator+=(double factor)
{
    for(int i=0; i<m_fitness.size();++i)
    {
        m_fitness[i] += factor;
    }
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator-=(double factor)
{
    *this += -factor;
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator*=(double factor)
{
    for(int i=0; i<m_fitness.size();++i)
    {
        m_fitness[i] *= factor;
    }
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator/=(double factor)
{
    for(int i=0; i<m_fitness.size();++i)
    {
        m_fitness[i] /= factor;
    }
    return *this;
}

PSOFitnessValue &PSOFitnessValue::operator=(double value)
{
    m_fitness.fill(value);
    return *this;
}
}
