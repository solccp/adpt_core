#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <functional>
#include <QVector>
#include "PSOFitnessValue.h"
#include <memory>
namespace libPSO
{

    class PSOVector;
    class PSOAlgorithm;

    typedef std::function<PSOFitnessValue (const std::shared_ptr<PSOVector>&)> Evaluator;
    typedef std::function<void (const std::shared_ptr<const PSOAlgorithm>&, int)> ProgressUpdater;
    typedef std::function<void (const std::shared_ptr<PSOVector>&, bool)> Initializer;

}

#endif // TYPEDEFS_H
