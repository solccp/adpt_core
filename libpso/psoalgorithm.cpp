#include "psoalgorithm.h"
#include "psopopulation.h"
#include "vector.h"

#include <QDebug>

namespace libPSO
{

PSOAlgorithm::PSOAlgorithm(const std::shared_ptr<PSOPopulation> & pop)
{
    m_pop = pop;
    m_pop->setPositionUpdater(std::bind(&PSOAlgorithm::updatePosition, this, std::placeholders::_1, std::placeholders::_2));
}

void PSOAlgorithm::setEvaluator(Evaluator eval)
{
    m_evaluator = eval;
}

void PSOAlgorithm::setProgressUpdater(ProgressUpdater prog)
{
    m_progressUpdater = prog;
}

void PSOAlgorithm::setMaxIterations(int n_iter)
{
    m_maxIter = n_iter;
}

void PSOAlgorithm::evolve()
{
    if (!m_pop->initialized())
    {
        m_pop->initialize();
    }
    if(m_progressUpdater)
    {
        m_progressUpdater(this->shared_from_this(),0);
    }
    for(int i=1; i<=m_maxIter; ++i)
    {
        m_pop->evaluate(i,m_evaluator);
        updateRepo(i);
        evolve_local(i);
        if(m_progressUpdater)
        {
            m_progressUpdater(this->shared_from_this(),i);
        }
    }

}

std::shared_ptr<const PSOPopulation> PSOAlgorithm::getPopulation() const
{
    return m_pop;
}

PSOAlgorithm::~PSOAlgorithm()
{
}

void PSOAlgorithm::updatePosition(Vector<double> *icp, const Vector<double> *icv)
{
    (*icp)+=(*icv);
}

}
