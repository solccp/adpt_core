#-------------------------------------------------
#
# Project created by QtCreator 2012-12-23T21:25:18
#
#-------------------------------------------------

QT       -= gui
TARGET = pso
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    simplepsoalgorithm.cpp \
    psovector.cpp \
    psopopulation.cpp \
    psoalgorithm.cpp \
    pso1dvector.cpp \
    MOPSOAlgorithm.cpp \
    PSOFitnessValue.cpp

HEADERS += \
    vector.h \
    typedefs.h \
    simplepsoalgorithm.h \
    psovector.h \
    psopopulation.h \
    psoalgorithm.h \
    pso1dvector.h \
    MOPSOAlgorithm.h \
    RandomNumberGenerator.h \
    Singleton.h \
    PSOFitnessValue.h
