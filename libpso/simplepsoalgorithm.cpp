#include "simplepsoalgorithm.h"
#include "psopopulation.h"
#include "psovector.h"

#include "RandomNumberGenerator.h"
#include "Singleton.h"
#include "PSOFitnessValue.h"

namespace libPSO
{
SimplePSOAlgorithm::SimplePSOAlgorithm(const std::shared_ptr<PSOPopulation>& pop): PSOAlgorithm(pop)
{
    int m_size = m_pop->numOfParticles();
    m_personalBestValues.resize(m_size);

    m_personalBestPositions.resize(m_size);
    for(int i=0; i<m_size; ++i)
    {
        m_personalBestPositions[i] = m_pop->particlePosition(i)->clone();
    }

    m_globalBestPosition = m_pop->particlePosition(0)->clone();
    m_globalBestValue = 1e15;


}

SimplePSOAlgorithm::~SimplePSOAlgorithm()
{
//    for(int i=0 ; i<m_personalBestPositions.size(); ++i)
//    {
//        delete m_personalBestPositions[i];
//    }
//    delete m_globalBestPosition;
}

void SimplePSOAlgorithm::updateVelocity(int n_iter)
{
    int m_size = m_pop->numOfParticles();
    RandomNumberRenerator<double>& gen = Singleton<RandomNumberRenerator<double> >::Instance();
    for(int i=0; i<m_size; ++i)
    {
        Vector<double> icp = m_pop->particlePosition(i)->encode();
        Vector<double> icv = m_pop->particleVelocity(i)->encode();
        Vector<double> gbp = m_globalBestPosition->encode();
        Vector<double> pbp = m_personalBestPositions[i]->encode();

        double w = 0.9 - 0.5*(static_cast<double>(n_iter)/static_cast<double>(m_maxIter));
        Vector<double> rand1, rand2;
        rand1.resize(icp.size());
        rand2.resize(icp.size());
        std::generate(rand1.begin(), rand1.end(), [&](){return gen.rand_real(0.0,1.0);});
        std::generate(rand2.begin(), rand2.end(), [&](){return gen.rand_real(0.0,1.0);});

#ifdef DEMO
       m_pop->m_previous_position[i]->decode(icp);
       m_pop->m_previous_velocities[i]->decode((icv)*w);
       m_pop->m_velocities_local[i]->decode(rand1*((pbp)-(icp))*c1);
       m_pop->m_velocities_global[i]->decode(rand2*((gbp)-(icp))*c2);
#endif

        Vector<double> res = (icv)*w + rand1*((pbp)-(icp))*c1 + rand2*((gbp)-(icp))*c2;

        m_pop->particleVelocity(i)->decode(res);
    }
}

void SimplePSOAlgorithm::evolve_local(int iter)
{
    if (iter<m_maxIter)
    {
        updateVelocity(iter);
        m_pop->stablizeVelocity();
        m_pop->updatePosition();
        m_pop->stablizePosition();
    }
}


const PSOFitnessValue& SimplePSOAlgorithm::getBestValue() const
{
    return m_globalBestValue;
}

std::shared_ptr<const PSOVector> SimplePSOAlgorithm::getBestPosition() const
{
    return m_globalBestPosition;
}

bool SimplePSOAlgorithm::globalBestUpdated() const
{
    return m_globalBestUpdated;
}

void SimplePSOAlgorithm::setGlobalBestUpdated(bool globalBestUpdated)
{
    m_globalBestUpdated = globalBestUpdated;
}


void SimplePSOAlgorithm::updateRepo(int iter)
{
    setGlobalBestUpdated(false);
    int min_index = -1;
    PSOFitnessValue min_value = m_globalBestValue;
    if (iter == 1)
    {
        min_index = 0;
        min_value = m_pop->fitness(0);
    }
    for(int i=0; i< m_pop->numOfParticles();++i)
    {
        const PSOFitnessValue& value = m_pop->fitness(i);
        if (iter == 1)
        {
            m_personalBestValues[i] = value;
            m_personalBestPositions[i]->copyfrom(m_pop->particlePosition(i).get());
        }
        else
        {
            if (value < m_personalBestValues[i])
            {
                m_personalBestValues[i] = value;
                m_personalBestPositions[i]->copyfrom(m_pop->particlePosition(i).get());
            }
        }
        if (value < min_value)
        {
            min_index = i;
            min_value = value;
        }

    }
    if (min_index != -1)
    {
        setGlobalBestUpdated(true);
        m_globalBestValue = min_value;
        m_globalBestPosition->copyfrom(m_pop->particlePosition(min_index).get());
    }
}

bool SimplePSOAlgorithm::isMOPSO() const
{
    return false;
}


}
