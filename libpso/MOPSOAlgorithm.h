#ifndef MOPSOALGORITHM_H
#define MOPSOALGORITHM_H

#include "psoalgorithm.h"
#include "PSOFitnessValue.h"
#include <QPair>

namespace libPSO
{

class PSOVector;
class MOPSOAlgorithm : public PSOAlgorithm
{
public:
    MOPSOAlgorithm(const std::shared_ptr<PSOPopulation> &pop, int maxRepoSize = -1);
    ~MOPSOAlgorithm();
    // PSOAlgorithm interface
protected:
    virtual void updateVelocity(int iter);
    void updatePBest(int iter);
    void updateGBest(int iter);
    void mutate(int iter);
    const double c1 = 1.5;
    const double c2 = 1.5;
    const double mutationRateStart = 0.05;
    const double mutationRateEnd = 0.2;

protected:
    void resizeRepo();
    void resizeRepo_CHI();
    void findGBest_random();
    void findGBest_old(int iter);
    void findGBest(int iter);

private:
    QList<QPair<std::shared_ptr<PSOVector>, PSOFitnessValue>> repository;
    int m_MaxRepoSize;
    QVector<std::shared_ptr<PSOVector>> m_personalBestPositions;
    QVector<PSOFitnessValue> m_personalBestValues;
    QVector<std::shared_ptr<PSOVector>> m_globalBestPositions;

    QList<int> getSortedRepoIndexByFitness();

    // PSOAlgorithm interface
public:
    void evolve();

    // PSOAlgorithm interface
protected:
    void updateRepo(int iter);



    // PSOAlgorithm interface
public:
    bool isMOPSO() const;


public:
    const QList<QPair<std::shared_ptr<PSOVector>, PSOFitnessValue>>& getParetoOptimals() const;

};

}
#endif // MOPSOALGORITHM_H
