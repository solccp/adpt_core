#ifndef VECTOR_H
#define VECTOR_H

#include <QVector>
#include <cmath>

namespace libPSO
{


template <typename T>
class Vector : public QVector<T>
{
public:

    Vector<T>& operator-=(const T& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] -= rhs;
        }
        return *this;
    }

    Vector<T>& operator/=(const T& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] /= rhs;
        }
        return *this;
    }

    Vector<T>& operator*=(const T& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] *= rhs;
        }
        return *this;
    }


    Vector<T> operator*(const Vector<T>& rhs) const
    {
        Vector<T> res(*this);
        res*=rhs;
        return res;
    }


    Vector<T> operator*(const T& rhs) const
    {
        Vector<T> res(*this);
        res*=rhs;
        return res;
    }

    Vector<T>& operator-=(const Vector<T>& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] -= rhs[i];
        }
        return *this;
    }

    Vector<T>& operator+=(const Vector<T>& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] += rhs[i];
        }
        return *this;
    }

    Vector<T>& operator*=(const Vector<T>& rhs)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] *= rhs[i];
        }
        return *this;
    }
    Vector<T>& operator=(double value)
    {
        for(int i=0; i< this->size(); ++i)
        {
            (*this)[i] = value;
        }
        return *this;
    }

    Vector<T>& operator+=(const T& rhs)
    {
        for(int i=0; i<this->size(); ++i)
        {
            (*this)[i] += rhs;
        }
        return *this;
    }

    Vector<T> operator+(const Vector<T>& rhs) const
    {
        Vector<T> res(*this);
        res+=rhs;
        return res;
    }

    Vector<T> operator+(const T& rhs) const
    {
        Vector<T> res(*this);
        res+=rhs;
        return res;
    }

    Vector<T> operator-(const Vector<T>& rhs) const
    {
        Vector<T> res(*this);
        res-=rhs;
        return res;
    }

    Vector<T> operator-(const T& rhs) const
    {
        Vector<T> res(*this);
        res-=rhs;
        return res;
    }

    Vector<T> operator/(const T& rhs) const
    {
        Vector<T> res(*this);
        res/=rhs;
        return res;
    }

    void append(const T& rhs)
    {
        QVector<T>::append(rhs);
    }

    void append(const Vector<T>& rhs)
    {
        QVector<T>::operator +=(rhs);
    }

    double dis(const Vector<T>& rhs) const
    {
        return ((*this)-rhs).r() ;
    }

    double r() const
    {
        double res = 0.0;
        for(int i=0; i<this->size();++i)
        {
            res += this->at(i)*this->at(i);
        }
        return std::sqrt(res);
    }

};

}



#endif // VECTOR_H
