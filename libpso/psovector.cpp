#include "psovector.h"

namespace libPSO
{


PSOVector::PSOVector()
{
}

void PSOVector::copyfrom(const PSOVector *rhs)
{
    m_failed = rhs->m_failed;
    fitness_detail = rhs->fitness_detail;

}

void PSOVector::setFailed(const QString &reason)
{
    m_failed = true;
    m_failedReason = reason;
}

void PSOVector::unsetFailed()
{
    m_failed = false;
}

bool PSOVector::isFailed() const
{
    return m_failed;
}

const QString &PSOVector::getFailedReason() const
{
    return m_failedReason;
}

void PSOVector::setFitnessDetail(const QString &detail)
{
    fitness_detail = detail;
}

QString PSOVector::getFitnessDetail() const
{
    return fitness_detail;
}


}
