#ifndef PSOVECTOR_H
#define PSOVECTOR_H

#include "typedefs.h"

#include "vector.h"
#include <QString>

#include "memory"

namespace libPSO
{

class PSOVector
{
public:
    PSOVector();
    PSOVector(const PSOVector& rhs) = delete;
    virtual std::shared_ptr<PSOVector> clone() = 0;

    virtual int size() const = 0 ;
    virtual Vector<double> encode() const = 0;
    virtual void decode(const Vector<double>& vector) = 0;
    virtual void spread(double factor) = 0;
    virtual void copyfrom(const PSOVector* rhs);
    virtual ~PSOVector(){}

    void setFailed(const QString& reason);
    void unsetFailed();
    bool isFailed() const;
    const QString& getFailedReason() const;


    virtual double getLowerBound(int index) = 0;
    virtual double getUpperBound(int index) = 0;

    virtual void randomizePosition() = 0;
    virtual void randomizeVelocity() = 0;
    virtual void stablizeVelocity() = 0;
    virtual void stablizePosition(PSOVector* vel)  = 0;
    virtual void copyControllerFrom(const PSOVector* rhs) = 0;

    //fixme;
    void setFitnessDetail(const QString& detail);
    QString getFitnessDetail() const;

protected:
    QString fitness_detail;
    QString m_failedReason;
private:
    PSOVector& operator=(const PSOVector& rhs);
    bool m_failed = false;
};

}

#endif // PSOVECTOR_H
