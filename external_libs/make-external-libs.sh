#!/bin/bash

rm -rf build_yaml
cp -r yaml-0.1.5 build_yaml
cd build_yaml
autoreconf --force --install
./configure --prefix=`pwd`/../
make && make install
cd ..
rm -rf build_yaml

rm -rf build_libvariant
mkdir build_libvariant -p
cd build_libvariant
cmake -DLIBVARIANT_BUILD_DEBUG=ON -DCMAKE_INSTALL_PREFIX=../ ../libvariant/ -DLIBYAML_LIBRARIES=../lib/libyaml.a -DLIBYAML_INCLUDE_DIR=../include
make && make test && make install
cd ..
rm -rf build_libvariant

exit

#mkdir yaml-cpp-build -p
#cd yaml-cpp-build
#cmake -DBOOST_ROOT=../boost_for_yaml-cpp ../yaml-cpp
#make yaml-cpp && mkdir -p ../lib/ ; cp libyaml-cpp.a ../lib/
#cd ..
#rm -rf yaml-cpp-build

#mkdir build_ob -p
#cd build_ob
#cmake -DCMAKE_INSTALL_PREFIX=../ -DEIGEN3_INCLUDE_DIR=../Eigen ../openbabel-2.3.2/
#make && make install
#cd ..
#rm -rf build_ob


#mkdir build_nlopt -p
#cd build_nlopt
#../nlopt-2.4.2/configure --prefix=`pwd`/install --disable-shared
#make && make install
#cp -r install/* ../
#cd ..
#rm -rf build_nlopt


