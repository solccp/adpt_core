#include <QCoreApplication>
#include <QDebug>

#include "TextSKFile.h"

#include "Singleton.h"
#include "electronicpartcachedatabase.h"
#include "erepfit/Splcoeff.h"
#include "erepfit2_inputdata.h"
#include "erepfit2_inputdata_json.h"

#include <Variant/Variant.h>
#include <Variant/SchemaLoader.h>
#include <variantqt.h>

#include <QString>
#include <QPair>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QCommandLineParser>

#include "erepfitsystemevaluator.h"

void myMessageOutput(QtMsgType , const QMessageLogContext &, const QString &)
{
}

libvariant::Variant readSchema(const QString& uri)
{
    QFile schema_file(uri);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    return libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
}

bool copy_dir_recursive(QString from_dir, QString to_dir, bool replace_on_conflit)
{
    QDir dir;
    dir.setPath(from_dir);

    from_dir += QDir::separator();
    to_dir += QDir::separator();

    foreach (QString copy_file, dir.entryList(QDir::Files))
    {
        QString from = from_dir + copy_file;
        QString to = to_dir + copy_file;

        if (QFile::exists(to))
        {
            if (replace_on_conflit)
            {
                if (QFile::remove(to) == false)
                {
                    return false;
                }
            }
            else
            {
                continue;
            }
        }

        if (QFile::copy(from, to) == false)
        {
            return false;
        }
    }

    foreach (QString copy_dir, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString from = from_dir + copy_dir;
        QString to = to_dir + copy_dir;

        if (dir.mkpath(to) == false)
        {
            return false;
        }

        if (copy_dir_recursive(from, to, replace_on_conflit) == false)
        {
            return false;
        }
    }

    return true;
}

void writeRepulsive(QDir output_dir, const Erepfit::Spline4 &rep, const Erepfit2::Input::Erepfit2_InputData& erepfit2_input, const QStringList& elec_sks)
{
    foreach(auto & elec_sk, elec_sks)
    {
        TextSKFile skfile;
        QFileInfo fileinfo(elec_sk);
        skfile.loadFile(elec_sk);

        if (erepfit2_input.options.spline_order == 4)
        {
            skfile.setRepulsive(rep.toString());
        }
        else
        {
            QByteArray arr;
            QTextStream stream(&arr);
            Erepfit::Spline4ToSpline3(rep, stream);
            skfile.setRepulsive(QString(arr));
        }
        auto output_filename =  output_dir.absoluteFilePath(fileinfo.fileName());
        QFile file(output_filename);
        if (file.open(QIODevice::Text | QIODevice::WriteOnly))
        {
            QTextStream stream(&file);
            skfile.writeTo(stream);
        }
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationVersion(QString(GIT_VERSION));
    a.setApplicationName("Erepfit");


    QTextStream stdout_stream(stdout);


    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("input", "Input file");

    QCommandLineOption yamlOption("y", "Parse input file as the YAML format.");
    parser.addOption(yamlOption);

    QCommandLineOption debugOption("d", "Debug output");
    parser.addOption(debugOption);

    QCommandLineOption scratchPathOption(QStringList() << "s" << "scratch-path",
           QCoreApplication::translate("main", "Scratch directory"),
           QCoreApplication::translate("main", "scratch"));
    parser.addOption(scratchPathOption);


    // Process the actual command line arguments given by the user
    parser.process(a);

    if (parser.positionalArguments().empty())
    {
        parser.showHelp(1);
    }

    QString inputfilename = parser.positionalArguments().first();

    bool useJsonInput = true;
    if (parser.isSet(yamlOption))
    {
        useJsonInput = false;
    }

    Erepfit2::Input::Erepfit2_InputData erepfit2_input;


    try
    {
        stdout_stream << "Reading input files..." << endl;

        QFileInfo input(inputfilename);
        if (!input.exists() || !input.isReadable())
        {
            throw std::runtime_error(QString("File %1 is not existing or not readable .").arg(input.absoluteFilePath()).toStdString());
        }
        libvariant::Variant var;
        if (useJsonInput)
        {
            var = libvariant::DeserializeJSONFile(input.absoluteFilePath().toStdString().c_str());
        }
        else
        {
            var = libvariant::DeserializeYAMLFile(input.absoluteFilePath().toStdString().c_str());
        }
        stdout_stream << "Input file parsed." << endl;

        libvariant::Variant schema = readSchema(":/schema/erepfit_input_schema.json");
        libvariant::AdvSchemaLoader loader;
        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);

        if (result.Error())
        {
            auto schema_errors = result.GetSchemaErrors();
            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(std::cout);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(std::cout);
            }
            return 1;
        }

        stdout_stream << "Input file syntax OK." << endl;

        libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::fromVariant(var, erepfit2_input);


        libvariant::Variant var_parsed = libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(erepfit2_input);
        auto filename = input.baseName() +"_parsed.json";
        libvariant::SerializeJSON(filename.toStdString(), var_parsed, true);
        filename = input.baseName() +"_parsed.yaml";
        libvariant::SerializeYAML(filename.toStdString(), var_parsed);

        stdout_stream << "Input file parsed." << endl;

        libvariant::SerializeJSON(input.baseName().append("_parsed.json").toStdString(), var, true);
        libvariant::SerializeYAML(input.baseName().append("_parsed.yaml").toStdString(), var);

    }
    catch(const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }

    if (parser.isSet(debugOption))
    {
        erepfit2_input.options.debug = true;
    }


    QString subfolder_name = QString("%1-%2").arg("erepfit").arg(QCoreApplication::applicationPid());
    QDir scratchDir = QDir::temp().absoluteFilePath(subfolder_name);

    if (parser.isSet(scratchPathOption))
    {
        QString scrPath = parser.value(scratchPathOption);
        QFileInfo l_temp(scrPath);
        if (l_temp.isDir() && l_temp.isWritable())
        {
            scratchDir.setPath(QDir(scrPath).absoluteFilePath(subfolder_name));
        }
    }

    scratchDir.mkpath(scratchDir.absolutePath());

    erepfit2_input.makeAbsolutePath(QDir::current());

    ElectronicPartCacheDatabase &db = Singleton<ElectronicPartCacheDatabase>::Instance();
    db.load();


    Erepfit2::ErepfitSystemEvaluator erepfit_evaluator(stdout_stream);
    erepfit_evaluator.setScratchRoot(scratchDir.absolutePath());


    try
    {
        erepfit_evaluator.evaluate(&erepfit2_input);

        libvariant::Variant var = libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(erepfit2_input);
        if (useJsonInput)
        {
            libvariant::SerializeJSON(("evaluated_input.json"), var, true );
        }
        else
        {
            libvariant::SerializeYAML(("evaluated_input.yaml"), var);
        }


        Erepfit::Splcoeff splcoeff(stdout_stream, erepfit2_input.electronic_skfiles);
        splcoeff.debug = erepfit2_input.options.debug;
        splcoeff.setReferenceRepulsivePotentials(erepfit2_input.external_repulsive_potentials);
        bool erepfit_succ = splcoeff.calculate(erepfit2_input);
        splcoeff.writeout(stdout_stream);

        if (erepfit_succ)
        {
            QDir output_dir(QDir::current().absoluteFilePath("skfiles"));
            output_dir.mkpath(output_dir.absolutePath());

            QMap<QPair<QString, QString>, Erepfit::Spline4> repulsivePotentials = splcoeff.getPotentials();
            QMapIterator<QPair<QString, QString>, Erepfit::Spline4> it(repulsivePotentials);
            while(it.hasNext())
            {
                it.next();
                auto poten = it.key();

                {
                    auto elec_sks = erepfit2_input.electronic_skfiles->getSKFileName(poten.first, poten.second);
                    writeRepulsive(output_dir, it.value(), erepfit2_input, elec_sks);
                }
                if (poten.first != poten.second)
                {
                    auto elec_sks = erepfit2_input.electronic_skfiles->getSKFileName(poten.second, poten.first);
                    writeRepulsive(output_dir, it.value(), erepfit2_input, elec_sks);
                }
            }
        }

        if (erepfit2_input.options.debug)
        {
            copy_dir_recursive(scratchDir.absolutePath(), QDir::current().absoluteFilePath(subfolder_name), true);
            scratchDir.removeRecursively();
        }

        stdout_stream << "Normal Termination" << endl;

    }
    catch(std::exception & e)
    {
        stdout_stream << "Error:" << e.what() << endl;
        return 1;
    }

    return 0;
}
