#ifndef TEXTSKFILE_H
#define TEXTSKFILE_H

#include <QString>
#include <QTextStream>

class TextSKFile
{
public:
    TextSKFile();
    bool loadFile(const QString& filename);
    QString repulsive() const;
    void setRepulsive(const QString &repulsive);
    void writeTo(QTextStream& os);
private:
    QString m_electronic;
    QString m_repulsive;
};

#endif // TEXTSKFILE_H





