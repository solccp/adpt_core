#ifndef INPUT_YAML_H
#define INPUT_YAML_H

#include "input.h"
#include <yaml-cpp/yaml.h>
#include "skbuilder/TWNinput_yaml.h"

#include "yaml_node.h"
#include "yaml_utils.h"
#include "yaml_qttypes.h"
#include "DFTBTestSuite/atomicproperties.h"
#include "DFTBTestSuite/input_yaml.h"

#include "erepfit/erepfit_input.h"
#include "erepfit/erepfit_input_yaml.h"

#include <QRegularExpression>

#include <iostream>

namespace YAML
{
    template<>
    struct convert<DFTBPSO_Input::Confining>
    {
        static Node encode(const DFTBPSO_Input::Confining& rhs)
        {
            Node node;
            node["Type"] = rhs.type;
            if (rhs.type.toLower() == "orbital")
            {
                node["OrbitalTypes"] = rhs.orbitalTypes;
                node["OrbitalTypes"].SetStyle(YAML::FlowStyle);
            }
            node["Ranges"] = rhs.ranges;


            if(!rhs.restriction.isEmpty())
            {
                node["Restriction"] = rhs.restriction;
            }
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::Confining& rhs)
        {
            GET_NODE(rhs.type, node, "Type", QString);
            if (rhs.type.toLower() != "orbital" && rhs.type.toLower() != "density"
                    && rhs.type.toLower() != "totalpotential" && rhs.type.toLower() != "coulombpotential")
            {
                return false;
            }
            if (rhs.type.toLower() == "orbital")
            {
                GET_NODE(rhs.orbitalTypes, node, "OrbitalTypes", QStringList);
            }

            typedef QMap<QString, QPair<double, double> > map;
            GET_NODE(rhs.ranges, node, "Ranges", map);

            bool hasValue;
            QStringList restriction;
            GET_NODE_OPT(restriction, node, "Restriction", QStringList, hasValue);
            if (hasValue)
            {
                restriction.removeDuplicates();
                rhs.restriction = restriction.filter(QRegularExpression("[wra]"));
            }
            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::OrbitalEnergyDefinition>
    {
        static Node encode(const DFTBPSO_Input::OrbitalEnergyDefinition& rhs)
        {
            Node node;
            node["Orbital"] = rhs.orbital;
            node["Range"] = rhs.range;
            node["ReferredOrbital"] = rhs.referredOrbital;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::OrbitalEnergyDefinition& rhs)
        {
            typedef QPair<double,double> pdd;
            GET_NODE(rhs.orbital, node, "Orbital", QString);
            GET_NODE(rhs.range, node, "Range", pdd);
            GET_NODE(rhs.referredOrbital, node, "ReferredOrbital", QString);
            return true;
        }
    };
    template<>
    struct convert<DFTBPSO_Input::HubbardDefinition>
    {
        static Node encode(const DFTBPSO_Input::HubbardDefinition& rhs)
        {
            Node node;
            node["Orbital"] = rhs.orbital;
            node["Range"] = rhs.range;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::HubbardDefinition& rhs)
        {
            typedef QPair<double,double> pdd;
            GET_NODE(rhs.orbital, node, "Orbital", QString);
            GET_NODE(rhs.range, node, "Range", pdd);
            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::AtomicDefinition>
    {
        static Node encode(const DFTBPSO_Input::AtomicDefinition& rhs)
        {
            Node node;
            node["Element"] = rhs.element;
            node["Confining"] = rhs.confining;
            node["OrbitalEnergy"] = rhs.orbitalEnergyDefinitions;
            node["Hubbard"] = rhs.hubbardDefinitions;
            node["HubbardDerivs"] = rhs.hubbardDefinitions;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::AtomicDefinition& rhs)
        {
            QString tmp;
            GET_NODE(tmp, node, "Element", QString);
            const DFTBTestSuite::AtomicProperties* atom = DFTBTestSuite::AtomicProperties::fromSymbol(tmp);
            rhs.element = atom->getSymbol();

            bool hasValue;
            GET_NODE_OPT(rhs.confining, node, "Confining", QList<DFTBPSO_Input::Confining>, hasValue);

            GET_NODE_OPT(rhs.orbitalEnergyDefinitions, node, "OrbitalEnergy", QList<DFTBPSO_Input::OrbitalEnergyDefinition>, hasValue);
            GET_NODE_OPT(rhs.hubbardDefinitions, node, "Hubbard", QList<DFTBPSO_Input::HubbardDefinition>, hasValue);
            GET_NODE_OPT(rhs.hubbardDerivativeDefinitions, node, "HubbardDerivs", QList<DFTBPSO_Input::HubbardDefinition>, hasValue);

            if (hasValue && rhs.hubbardDerivativeDefinitions.size() > 1)
            {
                qDebug() << "Current implementation of the DFTB+ code cannot perform orbital-resolved 3rd DFTB.";
                qDebug() << "Only the first HubbardDerivs definition is loaded";
                DFTBPSO_Input::HubbardDefinition ala = rhs.hubbardDerivativeDefinitions.first();
                rhs.hubbardDerivativeDefinitions.clear();
                rhs.hubbardDerivativeDefinitions.append(ala);
            }

            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::ElectronicPart>
    {
        static Node encode(const DFTBPSO_Input::ElectronicPart& rhs)
        {
            Node node;
            node["ConfiningType"] = rhs.confiningType;
            node["AtomicDefinition"] = rhs.atomicDefinitions;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::ElectronicPart& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.atomicDefinitions, node, "AtomicDefinition", QList<DFTBPSO_Input::AtomicDefinition>, hasValue);
            bool hasConfining = false;

            for(int i=0; i<rhs.atomicDefinitions.size();++i)
            {
                if (!rhs.atomicDefinitions.at(i).confining.isEmpty())
                {
                    hasConfining = true;
                    break;
                }
            }

            if (hasConfining)
            {
                GET_NODE(rhs.confiningType, node, "ConfiningType", QString);
            }
            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::ErepfitAdditionalEquation>
    {
        static Node encode(const DFTBPSO_Input::ErepfitAdditionalEquation& rhs)
        {
            Node node;
            node["Potential"] = rhs.potentialName;
            node["Derivative"] = rhs.derivative;
            node["Distance"] = rhs.distance;
            node["Range"] = rhs.range;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::ErepfitAdditionalEquation& rhs)
        {
            typedef QPair<QString, QString> pair_ss;
            typedef QPair<double, double> pair_dd;

            GET_NODE(rhs.potentialName, node, "Potential", pair_ss);
            GET_NODE(rhs.derivative, node, "Derivative", int);
            GET_NODE(rhs.distance, node, "Distance", double);
            GET_NODE(rhs.range, node, "Range", pair_dd);
            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::ErepfitPSOVector>
    {
        static Node encode(const DFTBPSO_Input::ErepfitPSOVector& rhs)
        {
            Node node;
            node["AdditionalEquations"] = rhs.additionalEquations;
//            node["ErepfitInput"] = rhs.erepfit_input;
//            node["RepulsivePotentials"] = rhs.repulsivePotentials;
//            if (!rhs.externalRepulsivePotentials.m_data.isEmpty())
            {
//                node["ExternalRepulsivePotentials"] = rhs.externalRepulsivePotentials;
            }
            node["UseExperimentMethod"] = rhs.useExperimentMethod;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::ErepfitPSOVector& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.additionalEquations, node, "AdditionalEquations", QList<DFTBPSO_Input::ErepfitAdditionalEquation>, hasValue);
//            GET_NODE(rhs.erepfit_input, node, "ErepfitInput", ErepfitInput::Erepfit_InputData);
//            GET_NODE_OPT(rhs.repulsivePotentials, node, "RepulsivePotentials", DFTBPSO_Input::RepulsivePotentials, hasValue);
//            GET_NODE_OPT(rhs.externalRepulsivePotentials, node, "ExternalRepulsivePotentials", DFTBPSO_Input::ExternalRepulsivePotentials, hasValue);
            GET_NODE_OPT(rhs.useExperimentMethod, node, "UseExperimentMethod", bool, hasValue);

            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::RepulsivePart>
    {
        static Node encode(const DFTBPSO_Input::RepulsivePart& rhs)
        {
            Node node;
            node["RepulsiveType"] = rhs.repulsiveType;
            node["FourthOrderRepulsive"] = rhs.fourthOrder;
            if (rhs.repulsiveType.toLower() == "erepfit")
            {
                node["ErepfitPSOVector"] = rhs.erepfitPSOVector;
            }
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::RepulsivePart& rhs)
        {
            GET_NODE(rhs.repulsiveType, node, "RepulsiveType", QString);
            if (rhs.repulsiveType.toLower() == "erepfit")
            {
                GET_NODE(rhs.erepfitPSOVector, node, "ErepfitPSOVector", DFTBPSO_Input::ErepfitPSOVector);
            }
            bool hasValue;
            GET_NODE_OPT(rhs.fourthOrder, node, "FourthOrderRepulsive", bool, hasValue);

            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::LJDispersion>
    {
        static Node encode(const DFTBPSO_Input::LJDispersion& rhs)
        {
            Node node;
            node["DistanceRange"] = rhs.distance_range;
            node["Element"] = rhs.element;
            node["EnergyRange"] = rhs.energy_range;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::LJDispersion& rhs)
        {
            typedef QPair<double,double> pdd;
            GET_NODE(rhs.distance_range, node, "DistanceRange", pdd);
            GET_NODE(rhs.element, node, "Element", QString);
            GET_NODE(rhs.energy_range, node, "EnergyRange", pdd);
            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::IndependentPart>
    {
        static Node encode(const DFTBPSO_Input::IndependentPart& rhs)
        {
            Node node;
            if (!rhs.ljDispersion.empty())
            {
                node["LJDispersion"] = rhs.ljDispersion;
            }
            if (rhs.optDampingFactor)
            {
                node["DampingFactorRange"] = rhs.dampingFactorRange;
                node["DampingFactor"] = rhs.optDampingFactor;
            }
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::IndependentPart& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.ljDispersion, node, "LJDispersion", QList<DFTBPSO_Input::LJDispersion>, hasValue);
            GET_NODE_OPT(rhs.optDampingFactor, node, "DampingFactor", bool, hasValue);
            if (hasValue && rhs.optDampingFactor)
            {
                typedef QPair<double, double> pdd;
                GET_NODE(rhs.dampingFactorRange, node, "DampingFactorRange", pdd);
            }
            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::PSOVector>
    {
        static Node encode(const DFTBPSO_Input::PSOVector& rhs)
        {
            Node node;
            if (rhs.electronicPart.isDefined)
            {
                node["ElectronicPart"] = rhs.electronicPart;
            }
            if (rhs.repulsivePart.isDefined)
            {
                node["RepulsivePart"] = rhs.repulsivePart;
            }
            if (rhs.independendPart.isDefined())
            {
                node["IndependentPart"] = rhs.independendPart;
            }
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::PSOVector& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.electronicPart, node, "ElectronicPart", DFTBPSO_Input::ElectronicPart, hasValue);
            if (hasValue)
            {
                rhs.electronicPart.isDefined = true;
            }
            GET_NODE_OPT(rhs.repulsivePart, node, "RepulsivePart", DFTBPSO_Input::RepulsivePart, hasValue);
            if (hasValue)
            {
                rhs.repulsivePart.isDefined = true;
            }
            GET_NODE_OPT(rhs.independendPart, node, "IndependentPart", DFTBPSO_Input::IndependentPart, hasValue);
            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::GuessVector>
    {
        static Node encode(const DFTBPSO_Input::GuessVector& rhs)
        {
            Node node;
            node["File"] = rhs.File;
            node["StartingIteration"] = rhs.startingIteration;
            node["Spreading"] = rhs.Spreading;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::GuessVector& rhs)
        {
            bool hasValue;
            GET_NODE(rhs.File, node, "File", QString);
            GET_NODE_OPT(rhs.Spreading, node, "Spreading", double, hasValue);
            GET_NODE_OPT(rhs.startingIteration, node, "StartingIteration", int, hasValue);
            return true;
        }
    };

    template<>
    struct convert<DFTBPSO_Input::Guess>
    {
        static Node encode(const DFTBPSO_Input::Guess& rhs)
        {
            Node node;
            node["Occupancy"] = rhs.Occupancy;
            if (!rhs.items.isEmpty())
            {
                node["Vectors"] = rhs.items;
            }
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::Guess& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.Occupancy, node, "Occupancy", double, hasValue);
            GET_NODE_OPT(rhs.items, node, "Vectors", QList<DFTBPSO_Input::GuessVector>, hasValue);
            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::PSO>
    {
        static Node encode(const DFTBPSO_Input::PSO& rhs)
        {
            Node node;
            node["NumOfParticles"] = rhs.numOfParticles;
            node["NumOfIterations"] = rhs.numOfIterations;

//            node["EvaluationDefinitionFile"] = rhs.tester_input.testSuiteInputFile;
//            node["ReferenceDataFile"] = rhs.tester_input.referenceDataFile;

            node["Guess"] = rhs.guess;

            node["Algorithm"] = rhs.algorithm;

//            node["MOPSO"] = rhs.mopso;
//            node["MOPSOMaxRepoSize"] = rhs.MOPSOMaxRepoSize;
//            node["Refine"] = rhs.refine;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::PSO& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.numOfParticles, node, "NumOfParticles", int, hasValue);
            GET_NODE_OPT(rhs.numOfIterations, node, "NumOfIterations", int, hasValue);          
            GET_NODE_OPT(rhs.MOPSOMaxRepoSize, node, "MOPSOMaxRepoSize", int, hasValue);

//            GET_NODE_OPT(rhs.mopso, node, "MOPSO", bool, hasValue);
//            GET_NODE_OPT(rhs.refine, node, "Refine", bool, hasValue);
//            if (hasValue && rhs.refine)
//            {
//                typedef QMap<QString, double> mapsd;
//                GET_NODE(rhs.refining_initials, node, "RefiningInitials", mapsd);
//            }

//            GET_NODE_OPT(rhs.testSuiteInput, node, "EvaluationDefinition", DFTBTestSuite::InputData, hasValue);
//            if (!hasValue)
//            {
//                GET_NODE(rhs.tester_input.testSuiteInputFile, node, "EvaluationDefinitionFile", QString);
//                YAML::Node doc = YAML::LoadFile(DFTBTestSuite::expandpath(rhs.testSuiteInputFile).toStdString());
//                rhs.testSuiteInput = doc.as<DFTBTestSuite::InputData>();
//            }

//            GET_NODE_OPT(rhs.reference, node, "ReferenceData", DFTBTestSuite::ReferenceData, hasValue);
//            if (!hasValue)
//            {
//                GET_NODE(rhs.tester_input.referenceDataFile, node, "ReferenceDataFile", QString);

//                YAML::Node doc = YAML::LoadFile(DFTBTestSuite::expandpath(rhs.referenceDataFile).toStdString());
//                rhs.reference = doc.as<DFTBTestSuite::ReferenceData>();

//            }

            double dummyDOcc;
            double dummyDSpre;
            QString guessFile;

            GET_NODE_OPT(guessFile, node, "GuessFile", QString, hasValue);
            if (hasValue)
            {
                std::cout << "Warning!!!GuessFile has been moved to PSO/Guess/Vectors" << std::endl;
            }


            GET_NODE_OPT(dummyDOcc, node, "GuessOccupancy", double, hasValue);

            if (hasValue)
            {
                std::cout << "Warning!!!GuessOccupancy has been moved to PSO/Guess/Occupancy" << std::endl;
            }

            GET_NODE_OPT(dummyDSpre, node, "GuessSpreading", double, hasValue);
            if (hasValue)
            {
                std::cout << "Warning!!!GuessSpreading has been moved to PSO/Guess/Spreading" << std::endl;
            }


            GET_NODE_OPT(rhs.guess, node, "Guess", DFTBPSO_Input::Guess, hasValue);

            Q_UNUSED(dummyDOcc);
            Q_UNUSED(dummyDSpre);

            return true;
        }
    };



    template<>
    struct convert<DFTBPSO_Input::Control>
    {
        static Node encode(const DFTBPSO_Input::Control& rhs)
        {
            Node node;
            node["ScratchDir"] = rhs.scratchDir;
            node["RandomSeed"] = rhs.randomSeed;
            node["Debug"] = rhs.debug;
            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::Control& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.scratchDir, node, "ScratchDir", QString, hasValue);
            GET_NODE_OPT(rhs.randomSeed, node, "RandomSeed", unsigned int, hasValue);
            GET_NODE_OPT(rhs.debug, node, "Debug", bool, hasValue);
            return true;
        }
    };


    template<>
    struct convert<DFTBPSO_Input::Input>
    {
        static Node encode(const DFTBPSO_Input::Input& rhs)
        {
            Node node;
            node["Control"] = rhs.control;
            node["SKBuilderInputFile"] = rhs.skBuilderInputFile;

            node["PSO"] = rhs.pso;
            node["PSOVector"] = rhs.psoVector;

            if (rhs.skfileInfo.isValid)
                node["SKFileInfo"] = rhs.skfileInfo;

            return node;
        }
        static bool decode(const Node& node, DFTBPSO_Input::Input& rhs)
        {
            bool hasValue;
            GET_NODE_OPT(rhs.control, node, "Control", DFTBPSO_Input::Control, hasValue);
            GET_NODE_OPT(rhs.pso, node, "PSO", DFTBPSO_Input::PSO, hasValue);
            GET_NODE(rhs.psoVector, node, "PSOVector", DFTBPSO_Input::PSOVector);
            GET_NODE_OPT(rhs.skfileInfo, node, "SKFileInfo", DFTBTestSuite::SKFileInfo, hasValue);


//            GET_NODE_OPT(rhs.skBuilderInput, node, "SKBuilderInput", SKBuilder_Input::SKBuilderInput, hasValue);
//            if (!hasValue)
//            {
                GET_NODE(rhs.skBuilderInputFile, node, "SKBuilderInputFile", QString);
//            }
            return true;
        }
    };


//    template<>
//        struct convert<DFTBPSO_Input::RepulsivePotentials>
//        {
//            static Node encode(const DFTBPSO_Input::RepulsivePotentials& rhs)
//            {
//                Node node;
//                QList<QPair<QString, QString> > keys = rhs.m_PotentialRanges.keys();
//                for(int i=0; i<keys.size();++i)
//                {
//                    Node subnode;
//                    subnode["Potential"] = keys[i];
//                    subnode["NumKnots"] = rhs.m_PotentialKnots[keys[i]];
//                    subnode["Range"] = rhs.m_PotentialRanges[keys[i]];
//                    if (rhs.m_aggressive_initializetion.contains(keys[i]))
//                        subnode["AggressiveInitialization"] = keys[i];
//                    else
//                        subnode["AggressiveInitialization"] = false;
//                    node.push_back(subnode);
//                }
//                return node;
//            }
//            static bool decode(const Node& node, DFTBPSO_Input::RepulsivePotentials& rhs)
//            {
//                if (node.IsDefined())
//                {
//                    if (node.IsSequence())
//                    {
//                        typedef QPair<QString, QString> pair_ss;
//                        typedef QPair<double, double> pair_dd;

//                        for(std::size_t i=0; i<node.size();++i)
//                        {
//                            int knots;
//                            const Node& subnode = node[i];
//                            pair_ss poten;
//                            GET_NODE(poten, subnode, "Potential", pair_ss);
//                            pair_dd range;
//                            GET_NODE(range, subnode, "Range", pair_dd);
//                            GET_NODE(knots, subnode, "NumKnots", int);
//                            rhs.m_PotentialRanges.insert(poten, range);
//                            rhs.m_PotentialKnots.insert(poten, knots);
//                            bool aggr_init = false;
//                            bool hasValue;
//                            GET_NODE_OPT(aggr_init, subnode, "AggressiveInitialization", bool, hasValue);
//                            rhs.m_aggressive_initializetion.insert(poten, aggr_init);
//                        }
//                        return true;
//                    }
//                    else
//                    {
//                        return false;
//                    }
//                }
//                return true;
//            }
//        };


        template<>
            struct convert<DFTBPSO_Input::ExternalRepulsivePotentials>
            {
                static Node encode(const DFTBPSO_Input::ExternalRepulsivePotentials& rhs)
                {
                    Node node;
                    QList<QString> keys = rhs.m_data.keys();
                    for(int i=0; i<keys.size();++i)
                    {
                        QString path = rhs.m_data[keys[i]];
                        Node subnode;
                        subnode["Potential"] = keys[i];
                        subnode["Path"] = path;
                        node.push_back(subnode);
                    }
                    return node;
                }
                static bool decode(const Node& node, DFTBPSO_Input::ExternalRepulsivePotentials& rhs)
                {
                    if (node.IsDefined() && node.IsSequence())
                    {
                        for(std::size_t i=0; i<node.size();++i)
                        {
                            QString path;

                            const Node& subnode = node[i];

                            QString name;

                            GET_NODE(name, subnode, "Potential", QString);
                            GET_NODE(path, subnode, "Path", QString);
                            path = DFTBTestSuite::expandpath(path);
                            rhs.m_data.insert(name, path);
                        }
                        return true;
                    }
                    return false;
                }
            };


}

namespace DFTBPSO_Input
{



template<typename T>
struct convert
{
    static libvariant::Variant toVariant(const T& rhs)
    {
       return libvariant::Variant(rhs);
    }
};

template<>
struct convert<QString>
{
    static libvariant::Variant toVariant(const QString& rhs)
    {
        return libvariant::Variant(rhs.toStdString());
    }
};


template<>
struct convert<Control>
{
    static libvariant::Variant toVariant(const Control& rhs)
    {
        libvariant::Variant var;
        var["debug"] = convert<bool>::toVariant(rhs.debug);
        var["seed"] = convert<uint>::toVariant(rhs.randomSeed);
        var["scratch_dir"] = convert<QString>::toVariant(rhs.scratchDir);
        return var;
    }
};


template<>
struct convert<Input>
{
    static libvariant::Variant toVariant(const Input& rhs)
    {
        libvariant::Variant var;
        var["control"] = convert<Control>::toVariant(rhs.control);
//        var["pso"] = convert<PSO>::toVariant(rhs.pso);
        return var;
    }
};
}


#endif // INPUT_YAML_H
