#ifndef EREPFITPSOVECTOR_JSON_H
#define EREPFITPSOVECTOR_JSON_H

#include <Variant/Variant.h>
#include "variantqt.h"
#include "erepfitpsovector.h"
#include "pso1dvector.h"

namespace libvariant
{

inline QString erepfitSymboltoSymbol(const QString& sym)
{
    QString res = sym[0].toUpper();
    if (sym.size() > 1)
    {
        if (sym[1].isLetter())
        {
            res += sym[1];
        }
    }
    return res;
}

template<>
struct VariantConvertor<DFTBPSO::ErepFitPSOVector>
{
    static Variant toVariant(const DFTBPSO::ErepFitPSOVector &rhs)
    {
        using namespace libPSO;
        Variant node;
        const QList<std::shared_ptr<PSO1DVector>> potens = rhs.getPotentials();
        const QList<QPair<QString, QString> > symbols = rhs.getPairSymbols();
        for(int i=0; i< potens.size(); ++i)
        {
            Variant subnode;
            subnode["Potential"] = VariantConvertor<QPair<QString, QString> >::toVariant( qMakePair(erepfitSymboltoSymbol(symbols[i].first),erepfitSymboltoSymbol(symbols[i].second))) ;
            subnode["Range"] = VariantConvertor<QPair<double, double> >::toVariant(qMakePair(potens[i]->getPosMin(), potens[i]->getPosMax()));

            Vector<double> tmp = potens[i]->encode();
            qSort(tmp);
            for(int j=0; j<tmp.size();++j)
            {
                subnode["Knots"].Append(tmp[j]);
            }
            node["RepulsivePotential"].Append(subnode);

        }
        if (!rhs.m_additionalEquationVector.empty())
        {
            int ae_size = rhs.m_inputData.additionalEquations.size();
            Q_ASSERT(ae_size == rhs.m_additionalEquationVector.size());

            Variant addequnode;
            Variant addpsovariable;
            for(int i=0; i<ae_size; ++i)
            {
                Variant item;
                item["Potential"] = VariantConvertor<QPair<QString, QString> >::toVariant(rhs.m_inputData.additionalEquations[i].potentialName) ;
                item["Derivative"] = rhs.m_inputData.additionalEquations[i].derivative ;
                item["Distance"] = rhs.m_inputData.additionalEquations[i].distance ;
                item["Range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.m_inputData.additionalEquations[i].range) ;
                item["Value"] = rhs.m_additionalEquationVector[i]->encode()[0];
                addequnode.Append(item);
            }
            addpsovariable["ErepfitAdditionalEquation"] = addequnode;
            node["AdditionalPSOVariable"] = addpsovariable;
        }

        return node;
    }
    static void fromVariant(const Variant &node, DFTBPSO::ErepFitPSOVector &rhs)
    {
        using namespace libPSO;
        try
        {

            using pdd = QPair<double, double> ;
            using pss = QPair<QString, QString> ;

            if (node.Contains("AdditionalPSOVariable"))
            {
                Variant addequnode = node["AdditionalPSOVariable"];

                if (addequnode.Contains("ErepfitAdditionalEquation"))
                {
                    for(size_t i=0; i<addequnode["ErepfitAdditionalEquation"].Size(); ++i)
                    {
                        try
                        {
                            Variant subnode = addequnode["ErepfitAdditionalEquation"].At(i);
                            Vector<double> tmpa;
                            double tmp;
                            GET_VARIANT(tmp, subnode, "Value", double);
                            tmpa.push_back(tmp);

                            pss pot;
                            pdd range;

                            GET_VARIANT(pot, subnode, "Potential", pss);
                            GET_VARIANT(range, subnode, "Range", pdd);

                            int der;
                            GET_VARIANT(der, subnode, "Derivative", int);

                            double distance;
                            GET_VARIANT(distance, subnode, "Distance", double);


                            for(int j=0; j< rhs.m_inputData.additionalEquations.size();++j)
                            {
                                if (rhs.m_inputData.additionalEquations[j].potentialName == pot &&
                                    rhs.m_inputData.additionalEquations[j].derivative == der &&
                                    qAbs(rhs.m_inputData.additionalEquations[j].distance - distance) < 1.0e-3
                                   )
                                {
                                    qDebug() << "Erepfit Addition Equation loaded." ;
                                    qDebug() << "Potential:" << qPrintable(pot.first) << "-" << qPrintable(pot.second) ;
                                    qDebug() << "Derivative:" << der;
                                    qDebug() << "Distance:" << distance;
                                    qDebug() << "Value:" << tmp;
                                    qDebug() << "Info: Range will not be overrided by the guess.";

                                    rhs.m_additionalEquationVector[j]->decode(tmpa);
                                    rhs.m_inputData.additionalEquations[j].derivative = der;
                                    rhs.m_inputData.additionalEquations[j].distance = distance;
                                    rhs.m_inputData.additionalEquations[j].potentialName = pot;
            //                        rhs.m_inputData->additionalEquations[j].range = range;
                                    break;
                                }
                            }

                        }
                        catch(std::exception& e)
                        {
                            std::cerr << e.what() << std::endl
                                 << "Skipped loading guess." << std::endl;
                        }
                    }
                }
            }

            if (node.Contains("RepulsivePotential"))
            {
               for(size_t i=0; i<node["RepulsivePotential"].Size(); ++i)
                {

                    try
                    {
                        Variant subnode = node["RepulsivePotential"].At(i);
                        pss pot;
                        pdd range;
                        Vector<double> knots;
                        GET_VARIANT(pot, subnode, "Potential", pss);
                        GET_VARIANT(range, subnode, "Range", pdd);

                        QList<double> l_knots;
                        GET_VARIANT(l_knots, subnode, "Knots", QList<double>);

                        for(auto item : l_knots)
                        {
                            knots.append(item);
                        }

                        qDebug() << "Erepfit Repulsive Potential loaded." ;
                        qDebug() << "Potential:" << qPrintable(pot.first) << "-" << qPrintable(pot.second) ;
                        qDebug() << "Knots:" << knots;
                        qDebug() << "Range in guess is not applied.";

                        for(int i=0; i<rhs.m_pairSymbols.size();++i)
                        {
                            if (rhs.m_pairSymbols[i].first == pot.first && rhs.m_pairSymbols[i].second == pot.second )
                            {
                                if (knots.size() == rhs.m_potentialsVector[i]->size())
                                {
                                    rhs.m_potentialsVector[i]->decode(knots);
        //                                rhs.m_potentialsVector[i]->setPosMin(range.first);
        //                                rhs.m_potentialsVector[i]->setPosMax(range.second);
                                }
                                else if (knots.size() <= rhs.m_potentialsVector[i]->size())
                                {
                                    for(int j=0; j<knots.size();++j)
                                    {
                                        rhs.m_potentialsVector[i]->operator [](j) = knots[j];
                                    }
                                }
                                else
                                {
                                    qDebug() << "Guess has more knots then the input. ignored";
                                }
                            }
                        }
                    }
                    catch(std::exception& e)
                    {
                        std::cerr << e.what() << std::endl
                             << "Skipped loading guess." << std::endl;
                    }

                }

            }
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};


}



#endif // EREPFITPSOVECTOR_JSON_H

