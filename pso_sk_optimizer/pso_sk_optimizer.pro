#-------------------------------------------------
#
# Project created by QtCreator 2013-11-18T12:45:52
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = PSOSKOptimizer
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app
GIT_VERSION = $$system(git describe HEAD)
BUILD_DATE = $$system(date +%F)
DEFINES += BUILD_DATE=\\\"$$BUILD_DATE\\\"
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"
message($$DEFINES)

SOURCES += main.cpp  \
    dftbpsovector.cpp \
    atomicpsovector.cpp \
    erepfitpsovector.cpp \
    sopso_functions.cpp \
    utils.cpp \
    pso_functions.cpp \
    mopso_functions.cpp \
    TextSKFile.cpp \
    ErepfitKnotsPositionVector.cpp \
#    input.cpp \
    AggressiveErepfitKnotsPositionVector.cpp \
    confiningpotentialpsovector_nctu.cpp \
#    optimizer.cpp
    confiningpotentialactionpsovector.cpp \
    confiningpotentialpsovector.cpp \
    confiningpotentialpsovector_bccms.cpp \
    LJDispersionPSOVector.cpp D3DispersionPSOVector.cpp



INCLUDEPATH += $$PWD/../includes/

unix:!macx: LIBS += -L$$OUT_PWD/../libpso/ -lpso

INCLUDEPATH += $$PWD/../external_libs/include/
LIBS += $$PWD/../external_libs/lib/libVariant.a $$PWD/../external_libs/lib/libyaml.a

INCLUDEPATH += $$PWD/../libpso/
DEPENDPATH += $$PWD/../libpso/

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libpso/libpso.a

HEADERS += \
#    input.h \
    dftbpsovector.h \
    atomicpsovector.h \
    erepfitpsovector.h \
    sopso_functions.h \
    typesettings.h \
    utils.h \
    pso_functions.h \
    mopso_functions.h \
    TextSKFile.h \
    ErepfitKnotsPositionVector.h \
    AggressiveErepfitKnotsPositionVector.h \
#    input_json.h \
    dftbpsovector_variant.h \
    atomicpsovector_variant.h \
    erepfitpsovector_variant.h \
    confiningpotentialpsovector_nctu.h \
#    optimizer.h
    confiningpotentialpsovector.h \
    confiningpotentialactionpsovector.h \
    confiningpotentialactionpsovector_variant.h \
    confiningpotentialpsovector_bccms.h \
    LJDispersionPSOVector.h \
    LJDispersionPSOVector_variant.h \
    D3DispersionPSOVector.h \
    D3DispersionPSOVector_variant.h

unix:!macx: LIBS += -L$$OUT_PWD/../libskbuilder/ -lskbuilder

INCLUDEPATH += $$PWD/../libskbuilder/
DEPENDPATH += $$PWD/../libskbuilder/

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libskbuilder/libskbuilder.a

unix:!macx: LIBS += -L$$OUT_PWD/../libDFTBTestSuite/ -lDFTBTestSuite

INCLUDEPATH += $$PWD/../libDFTBTestSuite
DEPENDPATH += $$PWD/../libDFTBTestSuite

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/libDFTBTestSuite.a

INCLUDEPATH += $$PWD/../external_libs/Eigen

unix: LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/ -lDFTBPlusAdaptor

INCLUDEPATH += $$PWD/../libDFTBPlusAdaptor
DEPENDPATH += $$PWD/../libDFTBPlusAdaptor

unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/libDFTBPlusAdaptor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../liberepfit/release/ -lerepfit
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../liberepfit/debug/ -lerepfit
else:unix: LIBS += -L$$OUT_PWD/../liberepfit/ -lerepfit

INCLUDEPATH += $$PWD/../liberepfit
DEPENDPATH += $$PWD/../liberepfit

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/release/liberepfit.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/debug/liberepfit.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/release/erepfit.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/debug/erepfit.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../liberepfit/liberepfit.a

RESOURCES += \
    pso_dftb_parameter.qrc



include(../settings.pri)
unix {
    target.path = $$PREFIX/adpt/bin
    INSTALLS += target

    ala.path = $$PREFIX/adpt/lib
    ala.files += $$PWD/../external_libs/lib/libPythonQt*.so
    INSTALLS += ala
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/release/ -lSKBuilderCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/debug/ -lSKBuilderCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKBuilderCommon/ -lSKBuilderCommon

INCLUDEPATH += $$PWD/../libSKBuilderCommon
DEPENDPATH += $$PWD/../libSKBuilderCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/libSKBuilderCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/libSKBuilderCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/SKBuilderCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/SKBuilderCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/libSKBuilderCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/release/ -lSKOptimizerCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/debug/ -lSKOptimizerCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/ -lSKOptimizerCommon

INCLUDEPATH += $$PWD/../libSKOptimizerCommon
DEPENDPATH += $$PWD/../libSKOptimizerCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/release/libSKOptimizerCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/debug/libSKOptimizerCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/release/SKOptimizerCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/debug/SKOptimizerCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/libSKOptimizerCommon.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

MKL_PATH = $$(MKL_ROOT)
!isEmpty(MKL_PATH) {
LIBS +=-L$$(MKL_ROOT)/lib/intel64 -lmkl_rt -lpthread -lm
}


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../liberepfit_evaluator/release/ -lerepfit_evaluator
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../liberepfit_evaluator/debug/ -lerepfit_evaluator
else:unix: LIBS += -L$$OUT_PWD/../liberepfit_evaluator/ -lerepfit_evaluator

INCLUDEPATH += $$PWD/../liberepfit_evaluator
DEPENDPATH += $$PWD/../liberepfit_evaluator

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/release/liberepfit_evaluator.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/debug/liberepfit_evaluator.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/release/erepfit_evaluator.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/debug/erepfit_evaluator.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../liberepfit_evaluator/liberepfit_evaluator.a
