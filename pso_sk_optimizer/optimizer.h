#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include <QString>
#include <QTextStream>

#include <memory>
#include <Variant/Variant.h>

namespace DFTBPSO
{
    namespace Input
    {
        class SKOPTInput;
    }
    class Optimizer
    {
    public:
        enum class InputFormat : int
        {
            JSON = 1,
            YAML = 2
        }
        Optimizer(QTextStream& output_stream);
        void run(InputFormat format, const QString& inputfile);
        bool testMode() const;
        void setTestMode(bool testMode);
    private:
        QTextStream& m_output_stream;
        bool m_testMode = false;
        InputFormat m_input_format = InputFormat::JSON;
        std::shared_ptr<Input::SKOPTInput> inputData;

    private:
        QSet<QString> m_required_elec_skpairs;
        QSet<QString> m_required_rep_skpairs;

        QSet<QString> m_opt_elec_skpairs;
        QSet<QString> m_opt_rep_skpairs;

        QStringList m_required_confinings;

    private:
        void loadMainInput(const QString& inputfile);
        void loadRepulsivePSOVector();


        void loadPSOVectorInput();


        void checkIfExternalSKFiles();
        void checkSKFileRequirements();

        void loadSKBuilderInput();
        void loadElectronicPSOVector();



        libvariant::Variant var_input;
    };
}

#endif // OPTIMIZER_H
