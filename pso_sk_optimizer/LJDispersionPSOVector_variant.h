#ifndef LJDISPERSIONPSOVECTOR_VARIANT_H
#define LJDISPERSIONPSOVECTOR_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"


#include "LJDispersionPSOVector.h"

namespace libvariant
{

template<>
struct VariantConvertor<DFTBPSO::LJDispersionPSOVector>
{
    using pdd = QPair<double, double>;
    static Variant toVariant(const DFTBPSO::LJDispersionPSOVector &rhs)
    {
        Variant root_node;
        Variant node;

        for(int i=0;i<rhs.numberDispersion();++i)
        {
            Variant subnode;
            subnode["DistanceRange"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_distance_vectors[i]->getPosMin(),
                                                 rhs.m_distance_vectors[i]->getPosMax()));

            subnode["EnergyRange"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_energy_vectors[i]->getPosMin(),
                                                 rhs.m_energy_vectors[i]->getPosMax()));

            subnode["Element"] = rhs.getElement(i).toStdString();
            subnode["Distance"] = rhs.getDistance(i);
            subnode["Energy"] = rhs.getEnergy(i);
            node.Append(subnode);
        }
        root_node["LJDisperion"] = node;


        return root_node;
    }
    static void fromVariant(const Variant &node, DFTBPSO::LJDispersionPSOVector &rhs)
    {
        using namespace libPSO;
        try
        {
            if (node.Contains("LJDispersion"))
            {
                const Variant& subnode = node["LJDispersion"];
                rhs.clear();
                for(std::size_t i=0; i<subnode.Size();++i)
                {
                    const Variant& curNode = subnode.At(i);

                    QString elem = QString::fromStdString(curNode["Element"].AsString());

                    pdd distanceRange, energyRange;
                    GET_VARIANT(distanceRange, curNode, "DistanceRange", pdd);
                    GET_VARIANT(energyRange, curNode, "EnergyRange", pdd);

                    double distance = curNode["Distance"].AsDouble();
                    double energy = curNode["Energy"].AsDouble();

                    rhs.addVector(elem, distanceRange, energyRange);
                    {
                        Vector<double> vec;
                        vec.append(distance);
                        rhs.m_distance_vectors[i]->decode(vec);
                    }
                    {
                        Vector<double> vec;
                        vec.append(energy);
                        rhs.m_energy_vectors[i]->decode(vec);
                    }

                }
            }
            throw ("Keyword not found: LJDispersion");
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};


}



#endif // DISPERSIONPSOVECTOR_VARIANT_H

