#include "confiningpotentialpsovector_nctu.h"
#include "variantqt.h"

#include <stdexcept>

namespace DFTBPSO
{

ConfiningPotentialPSOVector_NCTU::ConfiningPotentialPSOVector_NCTU()
{
    w = std::make_shared<PSO1DVector>(1, 0.5, 5);
    a = std::make_shared<PSO1DVector>(1, 3, 5);
    r = std::make_shared<PSO1DVector>(1, 1.0, 10.0);
}

ConfiningPotentialPSOVector_NCTU::~ConfiningPotentialPSOVector_NCTU()
{

}

std::shared_ptr<PSOVector> ConfiningPotentialPSOVector_NCTU::clone()
{
    auto res = std::make_shared<ConfiningPotentialPSOVector_NCTU>();
    res->m_orbitalTypes = m_orbitalTypes;
    res->w = std::dynamic_pointer_cast<PSO1DVector>(w->clone());
    res->a = std::dynamic_pointer_cast<PSO1DVector>(a->clone());
    res->r = std::dynamic_pointer_cast<PSO1DVector>(r->clone());
    return res;
}

int ConfiningPotentialPSOVector_NCTU::size() const
{
    return 3;
}

Vector<double> ConfiningPotentialPSOVector_NCTU::encode() const
{
    Vector<double> res;
    res.append(w->encode());
    res.append(a->encode());
    res.append(r->encode());
    return res;
}

void ConfiningPotentialPSOVector_NCTU::decode(const Vector<double> &vector)
{
    (*w)[0] = vector.at(0);
    (*a)[0] = vector.at(1);
    (*r)[0] = vector.at(2);
}

void ConfiningPotentialPSOVector_NCTU::spread(double factor)
{
    w->spread(factor);
    a->spread(factor);
    r->spread(factor);
}

void ConfiningPotentialPSOVector_NCTU::copyfrom(const PSOVector *rhs)
{
    PSOVector::copyfrom(rhs);
    const ConfiningPotentialPSOVector_NCTU* real_pos = dynamic_cast<const ConfiningPotentialPSOVector_NCTU*>(rhs);
    Q_ASSERT_X(real_pos != nullptr, "ConfiningPotentialPSOVector_NCTU::copyfrom", "TypeError");
    w->copyfrom(real_pos->w.get());
    a->copyfrom(real_pos->a.get());
    r->copyfrom(real_pos->r.get());
    w->copyControllerFrom(real_pos->w.get());
    a->copyControllerFrom(real_pos->a.get());
    r->copyControllerFrom(real_pos->r.get());
}

void ConfiningPotentialPSOVector_NCTU::set_W_Range(double min, double max)
{
    w->setPosMin(min);
    w->setPosMax(max);
}

void ConfiningPotentialPSOVector_NCTU::set_A_Range(double min, double max)
{
    a->setPosMin(min);
    a->setPosMax(max);
}

void ConfiningPotentialPSOVector_NCTU::set_R_Range(double min, double max)
{
    r->setPosMin(min);
    r->setPosMax(max);
}

void ConfiningPotentialPSOVector_NCTU::randomizePosition()
{
    w->randomizePosition();
    a->randomizePosition();
    r->randomizePosition();

}

void ConfiningPotentialPSOVector_NCTU::randomizeVelocity()
{
    w->randomizeVelocity();
    a->randomizeVelocity();
    r->randomizeVelocity();
}

void ConfiningPotentialPSOVector_NCTU::stablizeVelocity()
{
    w->stablizeVelocity();
    a->stablizeVelocity();
    r->stablizeVelocity();
}

void ConfiningPotentialPSOVector_NCTU::stablizePosition(PSOVector *vel)
{
    ConfiningPotentialPSOVector_NCTU* real_vel = dynamic_cast<ConfiningPotentialPSOVector_NCTU*>(vel);
    if (real_vel)
    {
        w->stablizePosition(real_vel->w.get());
        a->stablizePosition(real_vel->a.get());
        r->stablizePosition(real_vel->r.get());
    }
}
void ConfiningPotentialPSOVector_NCTU::copyControllerFrom(const PSOVector *rhs)
{
    const ConfiningPotentialPSOVector_NCTU* vector = dynamic_cast<const ConfiningPotentialPSOVector_NCTU*>(rhs);
    Q_ASSERT_X(vector != nullptr, "ConfiningPotentialPSOVector_NCTU::copyControllerFrom", "TypeError");

    if (vector)
    {
        w->copyControllerFrom(vector->w.get());
        a->copyControllerFrom(vector->a.get());
        r->copyControllerFrom(vector->r.get());
    }
}

double ConfiningPotentialPSOVector_NCTU::getLowerBound(int index)
{
    switch(index)
    {
    case 0:
        return w->getLowerBound(0);
    case 1:
        return a->getLowerBound(0);
    case 2:
        return r->getLowerBound(0);
    default:
        throw std::invalid_argument("ConfiningPotentialPSOVector_NCTU::getLowerBound");
    }
}

double ConfiningPotentialPSOVector_NCTU::getUpperBound(int index)
{
    switch(index)
    {
    case 0:
        return w->getUpperBound(0);
    case 1:
        return a->getUpperBound(0);
    case 2:
        return r->getUpperBound(0);
    default:
        throw std::invalid_argument("ConfiningPotentialPSOVector_NCTU::getUpperBound");
    }
}


void ConfiningPotentialPSOVector_NCTU::fromVariant(const libvariant::Variant &var)
{
    ConfiningPotentialPSOVector::fromVariant(var);
    try
    {
//        GET_VARIANT(m_orbitalTypes, var, "orbital_types", QStringList);
        if (w)
        {
            (*w)[0] = var["w"].AsDouble();
        }
        if (a)
        {
            (*a)[0] = var["a"].AsDouble();
        }
        if (r)
        {
            (*r)[0] = var["r"].AsDouble();
        }
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        throw e;
    }
}

libvariant::Variant ConfiningPotentialPSOVector_NCTU::toVariant() const
{
    libvariant::Variant node = ConfiningPotentialPSOVector::toVariant();
//    node["orbital_types"] = libvariant::VariantConvertor<QStringList>::toVariant(m_orbitalTypes);
    node["w"] = (*w)[0];
    node["a"] = (*a)[0];
    node["r"] = (*r)[0];
    return node;
}

}
















