#include "mopso_functions.h"
#include "pso_functions.h"

#include "MOPSOAlgorithm.h"

#include "psoalgorithm.h"
#include "psopopulation.h"

#include "dftbpsovector.h"

#include "typesettings.h"
#include "utils.h"

#include "dftbpsovector_variant.h"

#include <fstream>

#include <QTextStream>
#include <QStringList>
#include <QFile>

namespace DFTBPSO
{
using namespace libPSO;

void mopso_progressUpdater(const std::shared_ptr<const libPSO::PSOAlgorithm>& pso, int iter)
{
    QTextStream stdout_stream(stdout);


    if (iter == 0)
    {
        stdout_stream << endl
                  << STARLINE << endl
                  << centerString("Optimization Started") << endl
                  << HYPHENLINE << endl;
    }
    else
    {



        auto mypso = std::dynamic_pointer_cast<const MOPSOAlgorithm>(pso);
        if (!mypso)
        {
            return;
        }

        QDir dir(QDir::current().absoluteFilePath("best"));
        if (!dir.exists())
        {
            dir.mkpath(dir.absolutePath());
        }


        const QList<QPair<std::shared_ptr<PSOVector> , PSOFitnessValue> > & paretoOptimials = mypso->getParetoOptimals();

        stdout_stream << "Iteration " << iter << " : " << endl;

        stdout_stream << INDENT_SPACES << "Pareto optimals: " << paretoOptimials.size() << " solutions." << endl;

        QFile file_sol(dir.absoluteFilePath("solutions.txt"));
        file_sol.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream details_stream(&file_sol);

        int hsize;

        if (!paretoOptimials.isEmpty())
        {
            QStringList headers;
            hsize = paretoOptimials.first().second.size();
            headers.append("Index");
            for(int i=0; i<hsize;++i)
            {
                headers.append(QString("%1").arg(paretoOptimials.first().second.header(i),16));
            }
            stdout_stream << INDENT_SPACES << headers.join(" ") << endl;
            details_stream << INDENT_SPACES << headers.join(" ") << endl;
        }


        for(int i=0; i<paretoOptimials.size(); ++i)
        {
            const PSOFitnessValue& bestValues = paretoOptimials[i].second;
            QStringList bestValuesStr;
            bestValuesStr.append(QString("%1").arg(i+1,5));
            for(int j=0; j<bestValues.size();++j)
            {
                bestValuesStr.append(QString("%1").arg(bestValues[j],16, 'F', 6));
            }

            stdout_stream << INDENT_SPACES << bestValuesStr.join(" ") << endl;
            details_stream << INDENT_SPACES << bestValuesStr.join(" ") << endl;
        }
        stdout_stream << endl << endl;

        auto pop = pso->getPopulation();
        for(int i=0; i< pop->numOfParticles(); ++i)
        {
            if (pop->particlePosition(i)->isFailed())
            {
                stdout_stream << INDENT_SPACES << QString("Particle #%1").arg(i+1,-5) << ": " << "Infeasible (Reason: " << pop->particlePosition(i)->getFailedReason() << ")" << endl;
            }
            else
            {
                PSOFitnessValue values = pop->fitness(i);
                QStringList valuesStr;
                for(int i=0; i<values.size();++i)
                {
                    valuesStr.append(QString("%1").arg(values[i],12, 'F', 6));
                }
                if (hsize == valuesStr.size())
                {
                    stdout_stream << INDENT_SPACES << QString("Particle #%1").arg(i+1,-5) << ": " << valuesStr.join(" ") << endl;
                }
                else
                {
                    stdout_stream << INDENT_SPACES << QString("Particle #%1").arg(i+1,-5) << ": " << "Infeasible (Reason: Wrong fitness dimension)" << endl;
                }
            }
        }

        stdout_stream << endl;


        stdout_stream << INDENT_SPACES << "# of infeasible particles: " << pop->getNumOfInfeasible() << endl;

        if ( pop->getNumOfInfeasible() < pop->numOfParticles() )
        {

            PSOFitnessValue avgValues = pop->getAvgFitness();
            QStringList avgValuesStr;
            for(int i=0; i<avgValues.size();++i)
            {
                avgValuesStr.append(QString("%1").arg(avgValues[i],12, 'F', 6));
            }


            stdout_stream << INDENT_SPACES << "Avarage fitness: " << avgValuesStr.join(" ") << endl;
        }
        else
        {
            stdout_stream << INDENT_SPACES << "Avarage fitness: " << "N/A" << endl;
        }
        stdout_stream << HYPHENLINE << endl;



        //! TODO rewrite
        for(int i=0; i<paretoOptimials.size(); ++i)
        {
            QString filename = dir.absoluteFilePath("best_parameters_%1").arg(i+1);

            auto vector = std::dynamic_pointer_cast<const DFTBPSOVector>(paretoOptimials[i].first);
            libvariant::Variant var = libvariant::VariantConvertor<DFTBPSOVector>::toVariant(*vector);
            libvariant::SerializeYAML(dir.absoluteFilePath(filename+".yaml").toStdString().c_str(), var);
            libvariant::SerializeJSON(dir.absoluteFilePath(filename+".json").toStdString().c_str(), var, true);

        }

        for(int i=0; i<paretoOptimials.size(); ++i)
        {
            std::ofstream fout(dir.absoluteFilePath("erepfit_detail_%1.txt").arg(i+1).toStdString().c_str());
            auto vector = std::dynamic_pointer_cast<const DFTBPSOVector>(paretoOptimials[i].first);
            fout << vector->getUserData(100).toString().toStdString() << std::endl;
            fout.close();
        }


        for(int i=0; i<paretoOptimials.size(); ++i)
        {
            std::ofstream fout(dir.absoluteFilePath("fitness_detail_%1.txt").arg(i+1).toStdString().c_str());
            auto vector = std::dynamic_pointer_cast<const DFTBPSOVector>(paretoOptimials[i].first);
            fout << vector->getUserData(300).toString().toStdString() << std::endl;
            fout.close();
        }


        for(int i=0; i<paretoOptimials.size(); ++i)
        {
            auto vector = std::dynamic_pointer_cast<const DFTBPSOVector>(paretoOptimials[i].first);

            QString source_path = vector->getRootPath();
            if (!source_path.isEmpty())
            {
                QDir subdir(dir.absoluteFilePath(QString("best_%1").arg(i+1)));
                subdir.removeRecursively();
                if (!subdir.exists())
                {
                    subdir.mkpath(subdir.absolutePath());
                }


                copyPath(vector->getRootPath(), subdir.absolutePath() );
                vector->setRootPath("");
            }
        }
    }
}


libPSO::PSOFitnessValue mopso_particle_evaluation(const std::shared_ptr<PSOVector> &vector)
{
    return pso_particle_evaluation(vector, true);
}

}
