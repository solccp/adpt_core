#ifndef CONFININGPOTENTIALACTIONPSOVECTOR_H
#define CONFININGPOTENTIALACTIONPSOVECTOR_H

#include "psovector.h"
#include "pso1dvector.h"
#include <QString>
#include <QStringList>

#include <memory>


namespace libvariant
{
template<typename T>
class VariantConvertor;
}

namespace DFTBPSO
{
using namespace libPSO;
class ConfiningPotentialPSOVector;
class ConfiningPotentialActionPSOVector : public PSOVector
{
    friend class DFTBPSOVector;
    friend class AtomicPSOVector;
    friend class libvariant::VariantConvertor<DFTBPSO::ConfiningPotentialActionPSOVector>;
public:

    ConfiningPotentialActionPSOVector();
    virtual ~ConfiningPotentialActionPSOVector();
    // PSOVector interface
public:
    std::shared_ptr<PSOVector> clone() override;
    int size() const override;
    Vector<double> encode() const override;
    void decode(const Vector<double> &vector) override;
    void spread(double factor) override;
    void copyfrom(const PSOVector *rhs) override;

public:

private:
    QStringList m_take;
    QList<std::shared_ptr<ConfiningPotentialPSOVector>> m_confiningVectors;

    // PSOVector interface
public:
    void randomizePosition() override;
    void randomizeVelocity() override;
    void stablizeVelocity() override;
    void stablizePosition(PSOVector *vel) override;
    void copyControllerFrom(const PSOVector *rhs) override;

    // PSOVector interface
public:
    double getLowerBound(int index) override;
    double getUpperBound(int index) override;

    QStringList getTake() const;
    void setTake(const QStringList &take);
};
}

#endif // CONFININGPOTENTIALACTIONPSOVECTOR_H

