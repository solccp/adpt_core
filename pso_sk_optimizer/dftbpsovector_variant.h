#ifndef DFTBPSOVECTOR_VARIANT_H
#define DFTBPSOVECTOR_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"
#include "dftbpsovector.h"

#include "erepfitpsovector_variant.h"
#include "atomicpsovector_variant.h"
#include "LJDispersionPSOVector_variant.h"
#include "D3DispersionPSOVector_variant.h"

namespace libvariant
{
template<>
struct VariantConvertor<DFTBPSO::DFTBPSOVector>
{
    static Variant toVariant(const DFTBPSO::DFTBPSOVector &rhs)
    {
        Variant node;

        //Electronic parameters
        if (!rhs.m_atomicVectors.isEmpty())
        {
            QMap<QString, libvariant::Variant> map;
            for(int i=0; i<rhs.m_atomicVectors.size();++i)
            {
                auto vec = rhs.m_atomicVectors[i];
                if (vec)
                {
                    map.insert(vec->element(), VariantConvertor<DFTBPSO::AtomicPSOVector>::toVariant(*vec));
                }
            }

            {
                node["electronic"] = VariantConvertor<QMap<QString, libvariant::Variant>>::toVariant(map);
            }
        }

        //Repulsive parameters
        if (rhs.erepfitVector())
        {
            Variant subnode;
            subnode = VariantConvertor<DFTBPSO::ErepFitPSOVector>::toVariant(*rhs.erepfitVector());
            node["repulsive"] = subnode;
        }

        //D3Dispersion parameters
        if (rhs.hasLJDispersionVector() || rhs.hasD3DispersionVector())
        {
            Variant subnode;
            if (rhs.hasLJDispersionVector())
            {
                subnode = VariantConvertor<DFTBPSO::LJDispersionPSOVector>::toVariant (*rhs.ljdispersionVector());
            }
            else if (rhs.hasD3DispersionVector())
            {
                subnode = VariantConvertor<DFTBPSO::D3DispersionPSOVector>::toVariant (*rhs.d3dispersionVector());
            }
            node["dispersion"] = subnode;
        }

        if (rhs.hasDampingFactorVector())
        {
            node["damping_factor"] =  rhs.dampingFactorVector()->at(0);
        }

        return node;
    }
    static void fromVariant(const Variant &node, DFTBPSO::DFTBPSOVector &rhs)
    {              
//        try
//        {
            if (node.Contains("electronic") && node["electronic"].IsMap())
            {
                auto subnode = node["electronic"];
                for(auto & item : rhs.m_atomicVectors)
                {
                    auto element = item->element().toStdString();
                    if (subnode.Contains(element))
                    {
                        qDebug() << "Atomic parameter for element " << QString::fromStdString(element) << " is found.";
                        try
                        {
                            GET_VARIANT(*item, subnode, element, DFTBPSO::AtomicPSOVector);
                            qDebug() << "Parameters Loaded";
                        }
                        catch(...)
                        {
                            qDebug() << "Parameters loading failed";
                        }
                    }
                    else
                    {
                        qDebug() << "Atomic parameter for element " << QString::fromStdString(element) << " is not found.";
                    }
                }
            }

            if (node.Contains("repulsive"))
            {
                VariantConvertor<DFTBPSO::ErepFitPSOVector>::fromVariant(node["repulsive"], *rhs.m_erepfitVector);
            }

            if (node.Contains("dispersion"))
            {
                Variant subnode = node["dispersion"];

                if (subnode.Contains("d3dispersion"))
                {
                    GET_VARIANT_PTR(rhs.m_d3dispersionVector, subnode, "d3dispersion", DFTBPSO::D3DispersionPSOVector)
                }
                else if (subnode.Contains("LJDispersion"))
                {
                    GET_VARIANT_PTR(rhs.m_ljdispersionVector, subnode, "LJDispersion", DFTBPSO::LJDispersionPSOVector)
                }
            }

            if (node.Contains("damping_factor"))
            {
                if (rhs.hasDampingFactorVector())
                {
                    double value = node["damping_factor"].AsDouble();
                    rhs.dampingFactorVector()->operator [](0) = value;
                }

            }

//        }
//        catch (const std::exception& e)
//        {
//            std::cout << e.what() << std::endl;
//            throw e;
//        }
    }
};

}


#endif // DFTBPSOVECTOR_VARIANT_H

