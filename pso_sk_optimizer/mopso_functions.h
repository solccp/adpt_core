#ifndef MOPSO_FUNCTIONS_H
#define MOPSO_FUNCTIONS_H

#include "PSOFitnessValue.h"
#include <memory>

namespace libPSO
{
    class PSOAlgorithm;
    class PSOVector;
}

namespace DFTBPSO
{
    void mopso_progressUpdater(const std::shared_ptr<const libPSO::PSOAlgorithm> &pso, int iter);
    libPSO::PSOFitnessValue mopso_particle_evaluation(const std::shared_ptr<libPSO::PSOVector>& vector);
}


#endif // MOPSO_FUNCTIONS_H
