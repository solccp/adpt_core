#ifndef DFTBPSOVECTOR_H
#define DFTBPSOVECTOR_H

#include "psovector.h"
#include "skbuilder/input.h"

#include <QList>
#include <QTemporaryDir>
#include <QHash>
#include <QVariant>

#include "erepfitpsovector.h"

namespace DFTBPSO
{
    namespace Input
    {
        class SKOPTInput;
    }
}

namespace DFTBPSO
{
using namespace libPSO;
class AtomicPSOVector;
class LJDispersionPSOVector;
class D3DispersionPSOVector;
class DFTBPSOVector : public PSOVector
{
    friend class libvariant::VariantConvertor<DFTBPSOVector>;
public:
    DFTBPSOVector();
    ~DFTBPSOVector();

    bool define(const DFTBPSO::Input::SKOPTInput* input_);
    const DFTBPSO::Input::SKOPTInput* m_input;
    DFTBPSOVector& operator=(const DFTBPSOVector &rhs);


    SKBuilder::Input::SKBuilderInput createSKBuilderInput() const;
    QMap<QString, double> getHubbardDerivs() const;

private:
    QList<std::shared_ptr<AtomicPSOVector>> m_atomicVectors;
    std::shared_ptr<ErepFitPSOVector> m_erepfitVector = nullptr;
    std::shared_ptr<LJDispersionPSOVector> m_ljdispersionVector = nullptr;
    std::shared_ptr<D3DispersionPSOVector> m_d3dispersionVector = nullptr;
    std::shared_ptr<PSO1DVector> m_dampingFactorVector = nullptr;

private:
    mutable QString m_rootPath;

    QHash<int, QVariant> m_userData;


    // PSOVector interface
public:
    int size() const;
    Vector<double> encode() const;
    void decode(const Vector<double> &vector);
    void spread(double factor);
    void copyfrom(const PSOVector *rhs);

    void setUserData(int key, const QVariant& value);
    QVariant getUserData(int key) const;

    // PSOVectorRangeController interface
public:
    void randomizePosition(PSOVector *pos) const;
    void randomizeVelocity(PSOVector *vel) const;
    void stablizeVelocity(PSOVector *vel) const;
    void stablizePosition(PSOVector *pos, PSOVector *vel) const;

public:

    QString getRootPath() const;
    void setRootPath(const QString &value) const;
    std::shared_ptr<ErepFitPSOVector> erepfitVector() const;
    std::shared_ptr<LJDispersionPSOVector> ljdispersionVector() const;
    std::shared_ptr<D3DispersionPSOVector> d3dispersionVector() const;
    std::shared_ptr<PSO1DVector> dampingFactorVector () const;

public:
    bool hasOrbitalEnergyVector() const;
    bool hasHubbardVector() const;
    bool hasHubbardDerivativesVector() const;
    bool hasAtomicVector() const;
    bool hasErepfitVector() const;
    bool hasLJDispersionVector()const;
    bool hasD3DispersionVector()const;
    bool hasDampingFactorVector()const;


    // PSOVector interface
public:
    std::shared_ptr<PSOVector> clone() override;
    void randomizePosition();
    void randomizeVelocity();
    void stablizeVelocity();
    void stablizePosition(PSOVector *vel);
    void copyControllerFrom(const PSOVector *rhs);

    // PSOVector interface
public:
    double getLowerBound(int index);
    double getUpperBound(int index);
};

}

#endif // DFTBPSOVECTOR_H
