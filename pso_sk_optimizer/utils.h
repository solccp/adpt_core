#ifndef UTILS_H_a481dcd4_34fc_4fbf_a170_7ffef28bf52d
#define UTILS_H_a481dcd4_34fc_4fbf_a170_7ffef28bf52d

#include <QString>
#include <QPair>

namespace DFTBPSO
{
    void copyPath(const QString& src, const QString& dst);
    QPair<QString, QString> potential2ElementPair(const QString &potential);

    QString switchPotential(const QString &potential);

    QString elementPair2Potential(const QPair<QString, QString> &pair);
}

#endif // UTILS_H
