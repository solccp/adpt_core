#ifndef ATOMICPSOVECTOR_H
#define ATOMICPSOVECTOR_H

#include "psovector.h"
#include "pso1dvector.h"
#include "adpt/input.h"

namespace SKBuilder_Input
{
    class SKBuilderInput;
}

namespace DFTBPSO
{
using namespace libPSO;
class ConfiningPotentialActionPSOVector;


class AtomicPSOVector : public PSOVector
{
    friend class libvariant::VariantConvertor<DFTBPSO::AtomicPSOVector>;
    friend class DFTBPSOVector;
public:
    AtomicPSOVector(const QString &element_);
    ~AtomicPSOVector();
    bool define(const DFTBPSO::Input::AtomicDefinition& input, const SKBuilder::Input::SKBuilderInput &skbuilder_input);

    double getHubbardDerivs() const;
    bool hasHubbardDerivs() const;

private:
    QString m_element;
    QList<std::shared_ptr<ConfiningPotentialActionPSOVector>> m_confiningActions;

    QList<std::shared_ptr<PSO1DVector>> m_orbitalEnergyVectors;
    QList<DFTBPSO::Input::OrbitalEnergyDefinition> m_orbitalEnergyDefinitions;
    QList<QString> m_orbitalEnergyReferredOrbital;
    QList<double> m_orbitalEnergyReferredOrbitalEnergy;

    QList<DFTBPSO::Input::HubbardDefinition> m_hubbardDefinitions;
    QList<DFTBPSO::Input::HubbardDefinition> m_hubbardDerivsDefinitions;

    QList<std::shared_ptr<PSO1DVector>> m_hubbardVectors;
    QList<std::shared_ptr<PSO1DVector>> m_hubbardDerivsVectors;
    // PSOVector interface
public:
    std::shared_ptr<PSOVector> clone() override;
    int size() const override;
    Vector<double> encode() const override;
    void decode(const Vector<double> &vector) override;
    void spread(double factor) override;
    void copyfrom(const PSOVector *rhs) override;

    QString element() const;

    // PSOVector interface
public:
    void randomizePosition() override;
    void randomizeVelocity() override;
    void stablizeVelocity() override;
    void stablizePosition(PSOVector *vel) override;
    void copyControllerFrom(const PSOVector *rhs) override;

    // PSOVector interface
public:
    double getLowerBound(int index) override;
    double getUpperBound(int index) override;
};

}

#endif // ATOMICPSOVECTOR_H
