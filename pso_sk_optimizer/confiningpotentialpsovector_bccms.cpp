#include "confiningpotentialpsovector_bccms.h"
#include "variantqt.h"

#include <stdexcept>

namespace DFTBPSO
{

ConfiningPotentialPSOVector_BCCMS::ConfiningPotentialPSOVector_BCCMS()
{
    n = std::make_shared<PSO1DVector>(1, 2, 5);
    r = std::make_shared<PSO1DVector>(1, 1.0, 10.0);
}

ConfiningPotentialPSOVector_BCCMS::~ConfiningPotentialPSOVector_BCCMS()
{

}

std::shared_ptr<PSOVector> ConfiningPotentialPSOVector_BCCMS::clone()
{
    auto res = std::make_shared<ConfiningPotentialPSOVector_BCCMS>();
    res->m_orbitalTypes = m_orbitalTypes;
    res->n = std::dynamic_pointer_cast<PSO1DVector>(n->clone());
    res->r = std::dynamic_pointer_cast<PSO1DVector>(r->clone());
    return res;
}

int ConfiningPotentialPSOVector_BCCMS::size() const
{
    return 2;
}

Vector<double> ConfiningPotentialPSOVector_BCCMS::encode() const
{
    Vector<double> res;
    res.append(n->encode());
    res.append(r->encode());
    return res;
}

void ConfiningPotentialPSOVector_BCCMS::decode(const Vector<double> &vector)
{
    (*n)[0] = vector.at(0);
    (*r)[0] = vector.at(1);
}

void ConfiningPotentialPSOVector_BCCMS::spread(double factor)
{
    n->spread(factor);
    r->spread(factor);
}

void ConfiningPotentialPSOVector_BCCMS::copyfrom(const PSOVector *rhs)
{
    PSOVector::copyfrom(rhs);
    const ConfiningPotentialPSOVector_BCCMS* real_pos = dynamic_cast<const ConfiningPotentialPSOVector_BCCMS*>(rhs);
    Q_ASSERT_X(real_pos != nullptr, "ConfiningPotentialPSOVector_BCCMS::copyfrom", "TypeError");
    n->copyfrom(real_pos->n.get());
    r->copyfrom(real_pos->r.get());
    n->copyControllerFrom(real_pos->n.get());
    r->copyControllerFrom(real_pos->r.get());
}

void ConfiningPotentialPSOVector_BCCMS::set_N_Range(double min, double max)
{
    n->setPosMin(min);
    n->setPosMax(max);
}

void ConfiningPotentialPSOVector_BCCMS::set_R_Range(double min, double max)
{
    r->setPosMin(min);
    r->setPosMax(max);
}

void ConfiningPotentialPSOVector_BCCMS::randomizePosition()
{
    n->randomizePosition();
    r->randomizePosition();

}

void ConfiningPotentialPSOVector_BCCMS::randomizeVelocity()
{
    n->randomizeVelocity();
    r->randomizeVelocity();
}

void ConfiningPotentialPSOVector_BCCMS::stablizeVelocity()
{
    n->stablizeVelocity();
    r->stablizeVelocity();
}

void ConfiningPotentialPSOVector_BCCMS::stablizePosition(PSOVector *vel)
{
    ConfiningPotentialPSOVector_BCCMS* real_vel = dynamic_cast<ConfiningPotentialPSOVector_BCCMS*>(vel);
    if (real_vel)
    {
        n->stablizePosition(real_vel->n.get());
        r->stablizePosition(real_vel->r.get());
    }
}
void ConfiningPotentialPSOVector_BCCMS::copyControllerFrom(const PSOVector *rhs)
{
    const ConfiningPotentialPSOVector_BCCMS* vector = dynamic_cast<const ConfiningPotentialPSOVector_BCCMS*>(rhs);
    Q_ASSERT_X(vector != nullptr, "ConfiningPotentialPSOVector_BCCMS::copyControllerFrom", "TypeError");

    if (vector)
    {
        n->copyControllerFrom(vector->n.get());
        r->copyControllerFrom(vector->r.get());
    }
}

double ConfiningPotentialPSOVector_BCCMS::getLowerBound(int index)
{
    switch(index)
    {
    case 0:
        return n->getLowerBound(0);
    case 1:
        return r->getLowerBound(0);
    default:
        throw std::invalid_argument("ConfiningPotentialPSOVector_BCCMS::getLowerBound");
    }
}

double ConfiningPotentialPSOVector_BCCMS::getUpperBound(int index)
{
    switch(index)
    {
    case 0:
        return n->getUpperBound(0);
    case 1:
        return r->getUpperBound(0);
    default:
        throw std::invalid_argument("ConfiningPotentialPSOVector_BCCMS::getUpperBound");
    }
}


void ConfiningPotentialPSOVector_BCCMS::fromVariant(const libvariant::Variant &var)
{
    ConfiningPotentialPSOVector::fromVariant(var);
    try
    {
//        GET_VARIANT(m_orbitalTypes, var, "orbital_types", QStringList);
        if (n)
        {
            (*n)[0] = var["n"].AsDouble();
        }
        if (r)
        {
            (*r)[0] = var["r"].AsDouble();
        }
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        throw e;
    }
}

libvariant::Variant ConfiningPotentialPSOVector_BCCMS::toVariant() const
{
    libvariant::Variant node = ConfiningPotentialPSOVector::toVariant();
//    node["orbital_types"] = libvariant::VariantConvertor<QStringList>::toVariant(m_orbitalTypes);
    node["n"] = (*n)[0];
    node["r"] = (*r)[0];
    return node;
}

}
















