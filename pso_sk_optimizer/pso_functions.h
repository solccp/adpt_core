#ifndef PSO_FUNCTIONS_H
#define PSO_FUNCTIONS_H

#include "PSOFitnessValue.h"
#include <memory>

namespace libPSO
{
    class PSOVector;
    class PSOAlgorithm;
}

namespace DFTBPSO
{
    libPSO::PSOFitnessValue pso_particle_evaluation(const std::shared_ptr<libPSO::PSOVector>& vector, bool MOPSO);
}


#endif // PSO_FUNCTIONS_H
