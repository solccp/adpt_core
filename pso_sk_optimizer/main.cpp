#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QThreadPool>

#include "variantqt.h"
#include "basic_types.h"

#include "adpt/input.h"
#include "adpt/input_variant.h"

#include "psopopulation.h"
#include "dftbpsovector.h"
#include "simplepsoalgorithm.h"

#include "dftbpsovector_variant.h"
#include "DFTBPlusAdaptor.h"


#include "RandomNumberGenerator.h"

#include "skbuilder/sktoolkitbase.h"
#include "skbuilder/utils.h"

#include "typesettings.h"
#include "sopso_functions.h"
#include "mopso_functions.h"
#include "MOPSOAlgorithm.h"

#include <Variant/Variant.h>
#include <Variant/SchemaLoader.h>
#include <QCommandLineParser>
#include "electronicpartcachedatabase.h"
#include <QProcessEnvironment>


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type)
    {
    case QtInfoMsg:
        fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();       
    }
}


//using namespace libPSO;
//using namespace DFTBPSO;

void makeAbsolutePath_local(const QDir& rootDir, QString& path)
{
    if (path.isEmpty())
        return;
    QFileInfo info(path);

    if (info.isAbsolute())
    {
        return;
    }
    else
    {
        QString file = ADPT::expandpath(path);
        if (QFileInfo(file).exists())
        {
            path = file;
        }
        else
        {
            path = rootDir.absoluteFilePath(path);
        }
    }
}


void makeAbsolutePath(const QDir& rootDir, DFTBPSO::Input::SKOPTInput& input)
{
    if (input.external_elec_skfileInfo)
    {
        input.external_elec_skfileInfo->makeAbsolutePath(rootDir);
    }

    if (input.psoVector.repulsivePart)
    {
        if (input.psoVector.repulsivePart->erepfitPSOVector)
        {
            input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.makeAbsolutePath(rootDir);
        }
    }

    input.tester.input.makeAbsolutePath(rootDir);


    if (input.pso.additionalFitness.type.toLower() == "script")
    {
        if (!input.pso.additionalFitness.source.isEmpty())
        {
            if (!QFileInfo(input.pso.additionalFitness.source).isAbsolute())
            {
                makeAbsolutePath_local(rootDir, input.pso.additionalFitness.source);
            }
        }
    }

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

    QString rndstr = QString::number(qrand());

    if (input.options.scratchDir.contains("$JOB_ID"))
        input.options.scratchDir = input.options.scratchDir.replace("$JOB_ID", env.value("JOB_ID", rndstr));

    if (input.options.scratchDir.contains("$PBS_JOBID"))
        input.options.scratchDir = input.options.scratchDir.replace("$PBS_JOBID", env.value("PBS_JOBID", rndstr));

}

void UpdatePairs(QSet<QString>& skpairs, const QStringList& elements)
{
    for(int i=0; i<elements.size();++i)
    {
        for(int j=0; j<elements.size();++j)
        {
            skpairs.insert(QString("%1-%2").arg(elements[i]).arg(elements[j]));
        }
    }
}

QPair<QString, QString> potential2ElementPair(const QString& potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    QPair<QString, QString> res;
    res.first = arr[0];
    res.second = arr[1];
    return res;
}

QStringList findSKFile(ADPT::SKFileInfo& skfileInfo, const QString& potential)
{
    QStringList res;

    if (skfileInfo.type2Names)
    {
        QDir tmpDir(skfileInfo.prefix);
        if (!tmpDir.isAbsolute())
        {
            skfileInfo.prefix = tmpDir.absolutePath() + "/";
        }

        auto pair = potential2ElementPair(potential);

        QString fileName = QString("%1%2%3%4%5").arg(skfileInfo.prefix)
                .arg(skfileInfo.lowercase ? pair.first.toLower() : pair.first)
                .arg(skfileInfo.separator)
                .arg(skfileInfo.lowercase ? pair.second.toLower() : pair.second)
                .arg(skfileInfo.suffix);

        QFileInfo info(fileName);
        if (info.exists() && skfileInfo.maxAngularMomentum.contains(pair.first)
            && skfileInfo.maxAngularMomentum.contains(pair.second) )
        {
            QString fullFileName = QDir::current().absoluteFilePath(fileName);
            res.append(fullFileName);
        }
    }
    else
    {
        QMapIterator<QString, QStringList> it(skfileInfo.skfiles);

        while(it.hasNext())
        {
            it.next();
            auto poten = ADPT::PotentialName::fromString(it.key());
            if (potential != poten.toString())
            {
                continue;
            }

            if (skfileInfo.selectedShells.contains(poten.getElement1())
                    && skfileInfo.selectedShells.contains(poten.getElement2()))
            {
                int nshell1 = skfileInfo.selectedShells[poten.getElement1()].size();
                int nshell2 = skfileInfo.selectedShells[poten.getElement2()].size();
                Q_ASSERT(it.value().size() == (nshell1*nshell2));
                for(auto const & item : it.value())
                {
                    QFileInfo info(item);
                    if (!info.exists() || !info.isReadable())
                    {
                        throw std::runtime_error(info.absoluteFilePath().toStdString() + " is not found or readable.");
                    }
                }
                res.append(it.value());
                break;
            }
        }
    }

    return res;
}


bool checkIfSKBuilderInputNeeded(QTextStream& os, DFTBPSO::Input::SKOPTInput& input)
{
    //Get the required electronic pairs testsuite input.

    auto skpairs = input.tester.input.required_elec_pairs(input.tester.reference);
    QSet<QString> required_elec_skpairs = skpairs;
    QSet<QString> all_elec_skpairs;
    QStringList required_confinings;

    {
        QSetIterator<QString> it(required_elec_skpairs);
        while(it.hasNext())
        {
            QString poten = it.next();
            auto pair = potential2ElementPair(poten);
            required_confinings.append(pair.first);
            required_confinings.append(pair.second);
        }

        required_confinings.removeDuplicates();
        required_confinings.sort();

        UpdatePairs(all_elec_skpairs, required_confinings);
    }

    //if there's some existing SK files to load.

    QSet<QString> loaded_elec_skpairs;


    if (input.external_elec_skfileInfo)
    {
        //check input.skfileInfo
        QSetIterator<QString> it(all_elec_skpairs);
        while(it.hasNext())
        {
            QString poten = it.next();
            QStringList skfiles = findSKFile(*input.external_elec_skfileInfo, poten);

            if (!skfiles.isEmpty())
            {
                loaded_elec_skpairs.insert(poten);
                os << "Checking external electronic SK file (" << poten << ") ... Yes" << endl;
            }
            else
            {
                os << "Checking external electronic SK file (" << poten << ") ... No" << endl;
            }
        }
    }

    //unloaded_elec_skpairs are either being regenerated or optimized.
    auto unloaded_elec_skpairs = required_elec_skpairs - loaded_elec_skpairs;
    QStringList unloaded_confinings;

    {
        QSetIterator<QString> it(unloaded_elec_skpairs);
        while(it.hasNext())
        {
            auto pair = potential2ElementPair(it.next());
            unloaded_confinings.append(pair.first);
            unloaded_confinings.append(pair.second);
        }
        unloaded_confinings.removeDuplicates();
        unloaded_confinings.sort();
    }

    if (unloaded_confinings.empty())
    {
        return false;
    }
    else
    {
        return true;
    }

}


bool checkIfRepulsiveInputNeeded(DFTBPSO::Input::SKOPTInput& input)
{
    auto skpairs = input.tester.input.required_rep_pairs(input.tester.reference);

    auto unloaded_skpairs = skpairs - input.external_rep_skfiles.keys().toSet();

    if (unloaded_skpairs.empty())
    {
        return false;
    }
    else
    {
        return true;
    }

}

void check_input(QTextStream& os, DFTBPSO::Input::SKOPTInput& input)
{

    os << EQUALLINE << endl << endl ;
    os << centerString("Input Files Analysis") << endl << endl;


    //Checking required SK pair for both electronic and repulsive part
    //1. Get the required electronic pairs from erepfit equations and testsuite input.
    //2. Generate all electronic pairs from step 1.
    //3. Calculate the require confining potentials
    //4. Load existing electronic confining potentials
    //5. Load existing electronic pairs
    //6. Calculate the missing confining potential


    QSet<QString> all_elec_skpairs;
    QSet<QString> required_elec_skpairs;
    QSet<QString> non_required_elec_skpairs;
    QSet<QString> existing_elec_skpairs;


    auto skpairs = input.tester.input.required_elec_pairs(input.tester.reference);
    required_elec_skpairs += skpairs;
    if (input.psoVector.repulsivePart && input.psoVector.repulsivePart->erepfitPSOVector)
    {
        skpairs = input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.required_elec_pairs();
        required_elec_skpairs += skpairs;
    }


    if (input.skbuilder_input)
    {

        if (input.skbuilder_input->all_pairs)
        {
            auto symbols = input.skbuilder_input->atomicinfo.keys();

            for(int i=0; i<symbols.size();++i)
            {
                for(int j=0; j<symbols.size();++j)
                {
                    required_elec_skpairs.insert(QString("%1-%2").arg(symbols[i]).arg(symbols[j]));
                }
            }
        }
        else
        {
            for(int i=0; i<input.skbuilder_input->desired_pairs.size();++i)
            {
                required_elec_skpairs.insert(input.skbuilder_input->desired_pairs[i]);
            }
        }
    }

    QStringList required_confinings;

    {
        QSetIterator<QString> it(required_elec_skpairs);
        os << "Required Pairs of Electronic part Slater-Koster parameters:" << endl;
        while(it.hasNext())
        {
            QString poten = it.next();
            os << INDENT_SPACES << poten << endl;
            auto pair = potential2ElementPair(poten);
            required_confinings.append(pair.first);
            required_confinings.append(pair.second);
        }

        required_confinings.removeDuplicates();
        required_confinings.sort();

        UpdatePairs(all_elec_skpairs, required_confinings);
        os  << endl;
    }
    // required_elec_skpairs is a subset of all_elec_skpairs

    non_required_elec_skpairs = all_elec_skpairs - required_elec_skpairs;
    if (!non_required_elec_skpairs.isEmpty())
    {
        QSetIterator<QString> it(non_required_elec_skpairs);
        os << "Non-Required Electronic part Slater-Koster parameters:" << endl;
        while(it.hasNext())
        {
            os << INDENT_SPACES << it.next() << endl;
        }
        os  << endl;
    }

    required_confinings.removeDuplicates();
    if (!required_confinings.isEmpty())
    {
        QListIterator<QString> it(required_confinings);
        os << "Required Confining Potentials: " << endl;
        while(it.hasNext())
        {
            os << INDENT_SPACES << it.next() << endl;
        }
        os << endl;
    }


    QSet<QString> required_rep_skpairs;
    QSet<QString> all_rep_skpairs;
    QSet<QString> non_required_rep_skpairs;


    skpairs = input.tester.input.required_rep_pairs(input.tester.reference);
    required_rep_skpairs += skpairs;


    QStringList all_rep_elements;
    {
        QSetIterator<QString> it(required_rep_skpairs);
        os << "Required Repulsive Slater-Koster parameterization pairs:" << endl;
        while(it.hasNext())
        {
            auto pair = potential2ElementPair(it.next());

            os << INDENT_SPACES << QString("%1-%2").arg(pair.first).arg(pair.second) << endl;
            all_rep_elements.append(pair.first);
            all_rep_elements.append(pair.second);
        }
        os << endl;

        all_rep_elements.removeDuplicates();
        all_rep_elements.sort();
        UpdatePairs(all_rep_skpairs, all_rep_elements);
    }
    non_required_rep_skpairs = all_rep_skpairs - required_rep_skpairs;

    os << HYPHENLINE << endl;


    QStringList existing_confinings;

    if (!input.skbuilder_input && !input.external_elec_skfileInfo)
    {
        throw std::runtime_error("Neither Input/external_elec_skfileinfo nor Input/skbuilder_input defined.");
    }

    //if there's some existing SK files to load.

    os << "Checking if there's any external electronic Slater-Koster files..." << endl;
    QMap<QString, QList<QString> > loaded_elec_skpairs;


    if (input.external_elec_skfileInfo)
    {
        //check input.skfileInfo
        QSetIterator<QString> it(all_elec_skpairs);
        while(it.hasNext())
        {
            QString poten = it.next();
            auto pair = potential2ElementPair(poten);
            os << INDENT_SPACES << QString("Checking if %1 exists...").arg(poten);

            //if multi-valence is defined, don't load
            if (input.skbuilder_input)
            {
                if (input.skbuilder_input->atomicinfo.contains(pair.first))
                {
                    if (input.skbuilder_input->atomicinfo[pair.first].orbitals.size() > 1)
                    {
                        continue;
                    }
                }
                if (input.skbuilder_input->atomicinfo.contains(pair.second))
                {
                    if (input.skbuilder_input->atomicinfo[pair.second].orbitals.size() > 1)
                    {
                        continue;
                    }
                }
            }

            QStringList skfiles = findSKFile(*input.external_elec_skfileInfo, poten);
            if (!skfiles.isEmpty())
            {
                loaded_elec_skpairs[poten].append(skfiles);
                os << "Yes" << endl;
            }
            else
            {
                os << "No" << endl;
            }
        }

    }

    //unloaded_elec_skpairs are either being regenerated or optimized.
    auto unloaded_elec_skpairs = required_elec_skpairs - loaded_elec_skpairs.keys().toSet();
    QStringList unloaded_confinings;

    {
        QSetIterator<QString> it(unloaded_elec_skpairs);
        while(it.hasNext())
        {
            auto pair = potential2ElementPair(it.next());
            unloaded_confinings.append(pair.first);
            unloaded_confinings.append(pair.second);
        }
        unloaded_confinings.removeDuplicates();
        unloaded_confinings.sort();
    }


    //check if unloaded_confinings are provided in input, which are going to be regenerated.
    if (input.skbuilder_input)
    {
        QListIterator<QString> it(input.skbuilder_input->atomicinfo.keys());
        while(it.hasNext())
        {
            QString elem = it.next();
            if (input.skbuilder_input->confininginfo.contains(elem))
            {
                if (unloaded_confinings.contains(elem))
                {
                    existing_confinings.append(elem);
                }
                else
                {
                    os << "The confining potential for element " << elem << " is provided." << endl
                       << "Therefore this confining potential is not needed." << endl
                       << "All Electronic related to this confining have been loaded." << endl;
                }
            }
        }
        existing_confinings.removeDuplicates();
        existing_confinings.sort();
        UpdatePairs(existing_elec_skpairs, existing_confinings);
    }

    //update unloaded, the rest confinings should be defined in the opt section.
    auto rest_confinings = (unloaded_confinings.toSet() - existing_confinings.toSet()).toList();


    QStringList opt_confinings;
    if (input.psoVector.electronicPart)
    {
        for(int i=0; i<input.psoVector.electronicPart->atomicDefinitions.size(); ++i)
        {
            QString elem = input.psoVector.electronicPart->atomicDefinitions.at(i).element;
            if (input.skbuilder_input->atomicinfo.contains(elem))
            {
                if (rest_confinings.contains(elem))
                {
                    opt_confinings.append(elem);
                }
                else
                {
                    os << "The confining potential for element " << elem << " is provided." << endl
                       << "Therefore, this confining potential is not needed to optimize." << endl
                       << "All Electronic related to this confining have been loaded." << endl;


                }
            }
            else
            {
                os << "Warning: " << "Confining potential for element " << elem << endl
                   << "is asked for optimization, but its atomic_info is not defined in Input/SKBuilderInput."
                   << endl << "Skipping." << endl ;
            }
        }
    }

    opt_confinings.removeDuplicates();
    opt_confinings.sort();

    if (input.psoVector.electronicPart)
    {
        for(int i=input.psoVector.electronicPart->atomicDefinitions.size()-1; i>=0; --i)
        {
            QString elem = input.psoVector.electronicPart->atomicDefinitions.at(i).element;
            if (!opt_confinings.contains(elem))
            {
                input.psoVector.electronicPart->atomicDefinitions.removeAt(i);
            }
        }
    }


    QStringList missing_confining =
            (unloaded_confinings.toSet() - existing_confinings.toSet() - opt_confinings.toSet()).toList();

    if (!missing_confining.isEmpty())
    {
        os << "The following confining potentials are missing: " << endl
           << missing_confining.join(", ") << endl << endl;

        throw std::runtime_error("Missing electronic part");
    }

    os << endl << centerString("Summary") << endl << endl;
    os << "The following electronic parts are required:" << endl;
    {
        QSetIterator<QString> it(required_elec_skpairs);
        while(it.hasNext())
        {
            os << INDENT_SPACES << it.next() << endl;
        }
    }
    os << endl;



    if (!loaded_elec_skpairs.isEmpty())
    {
        os << "The following electronic parts will be loaded for external files:" << endl;
        QMapIterator<QString, QList<QString> > it(loaded_elec_skpairs);
        while(it.hasNext())
        {
            it.next();
            auto pair = it.key();
            QStringList files(it.value());

            if (files.size() == 1)
            {
                os << INDENT_SPACES << QString("%1 = %2").arg(pair).arg(files.first()) << endl;
            }
            else
            {
                os << INDENT_SPACES << QString("%1 = {").arg(pair) << endl;
                for(int i=0; i<files.size();++i)
                {
                    os << INDENT_SPACES << INDENT_SPACES << files.at(i) << endl;
                }
                os << INDENT_SPACES << "}" << endl;
            }
        }
    }
    os << endl;

    os << "The folllowing electronic confining potentials will be optimized: "
       << opt_confinings.join(", ") << endl << endl;



    if (input.psoVector.electronicPart)
    {
        QList<DFTBPSO::Input::AtomicDefinition> inputAtomicDefinition = input.psoVector.electronicPart->atomicDefinitions;
        input.psoVector.electronicPart->atomicDefinitions.clear();
        QStringList final_elec_opt_elements;
        for(int i = 0; i< inputAtomicDefinition.size(); ++i)
        {
    //        if (opt_confinings.contains(inputAtomicDefinition.at(i).element))
            {
                input.psoVector.electronicPart->atomicDefinitions.append(inputAtomicDefinition.at(i));
                final_elec_opt_elements.append(inputAtomicDefinition.at(i).element);
            }
        }

        if (input.skbuilder_input)
        {
            os << "Atomic Properties and Confining potential details" << endl;
            for(int i=0; i<final_elec_opt_elements.size();++i)
            {
                DFTBPSO::Input::AtomicDefinition& atomDef = input.psoVector.electronicPart->atomicDefinitions[i]; //! BUG check this
                SKBuilder::Input::AtomicInfo& atomInfo = input.skbuilder_input->atomicinfo[atomDef.element];
                os << INDENT_SPACES << "Element: " << atomDef.element << endl;
                QSet<QString> all_orbs;
                for(int ishell = 0 ; ishell<atomInfo.orbitals.size();++ishell)
                {
                    os << INDENT_SPACES << INDENT_SPACES
                       << "Shell #" << ishell+1 << ": [" ;

                    for(int iorb=0; iorb<atomInfo.orbitals[ishell].size();++iorb)
                    {
                        os << QString("%1^%2").arg(atomInfo.orbitals[ishell][iorb])
                              .arg(atomInfo.occupation[atomInfo.orbitals[ishell][iorb]]) << " ";

                        auto qn = getOrbitalQuantumNumbers(atomInfo.orbitals[ishell][iorb]);
                        if (qn.first == -1 || qn.second == -1)
                        {
                            os << endl << endl << atomInfo.orbitals[ishell][iorb] << " is not a valid orbital" << endl;
                            throw std::runtime_error("Not valid orbital");
                        }

                    }
                    os << "] - > (";
                    for(int iorb=0; iorb<atomInfo.orbitals[ishell].size();++iorb)
                    {
                        double occ = atomInfo.occupation[atomInfo.orbitals[ishell][iorb]];
                        if (atomInfo.final_occupation.contains(atomInfo.orbitals[ishell][iorb]))
                        {
                            occ = atomInfo.final_occupation[atomInfo.orbitals[ishell][iorb]];
                        }

                        os << QString("%1^%2").arg(atomInfo.orbitals[ishell][iorb])
                              .arg(occ) << " ";
                    }
                    os  << ")" << endl;
                    all_orbs += atomInfo.orbitals[ishell].toSet();
                }

                {
                    QSet<QString> all_orbs_local = all_orbs;
                    os << INDENT_SPACES << INDENT_SPACES << "Orbital energy: " << endl;
                    QMapIterator<QString, double> it(atomInfo.orbitalEnergy);
                    while(it.hasNext())
                    {
                        it.next();
                        QString orb = it.key();
                        all_orbs_local.remove(orb);

                        double value = it.value();

                        bool foundOpt = false;
                        int orb_index = -1;
                        bool relative_oe = true;
                        for(int j=0; j< atomDef.orbitalEnergyDefinitions.size();++j)
                        {
                            if (atomDef.orbitalEnergyDefinitions[j].orbital == orb
                                    && atomDef.orbitalEnergyDefinitions[j].referredOrbital.toLower() == "none")
                            {
                                foundOpt = true;
                                orb_index = j;
                                relative_oe = false;
                                break;
                            }
                            else if (atomDef.orbitalEnergyDefinitions[j].orbital == orb
                                    && all_orbs.contains(atomDef.orbitalEnergyDefinitions[j].referredOrbital))
                            {
                                foundOpt = true;
                                orb_index = j;
                                break;
                            }
                        }

                        if (foundOpt)
                        {
                            if (relative_oe)
                            {
                                os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                                   << QString("Orbital %1: %2 [%3, %4] relative to %5").arg(orb, 5).arg("OPT",10)
                                   .arg(atomDef.orbitalEnergyDefinitions[orb_index].range.first, 8, 'f', 5)
                                   .arg(atomDef.orbitalEnergyDefinitions[orb_index].range.second, 8, 'f', 5)
                                   .arg(atomDef.orbitalEnergyDefinitions[orb_index].referredOrbital)
                                   << endl;
                            }
                            else
                            {
                                os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                                   << QString("Orbital %1: %2 [%3, %4]").arg(orb, 5).arg("OPT",10)
                                   .arg(atomDef.orbitalEnergyDefinitions[orb_index].range.first, 8, 'f', 5)
                                   .arg(atomDef.orbitalEnergyDefinitions[orb_index].range.second, 8, 'f', 5)
                                   << endl;
                            }
                        }
                        else
                        {
                            os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                               << QString("Orbital %1: %2").arg(orb, 5).arg(value, 10, 'f', 6) << endl;
                        }
                    }
                    for(int j=0; j< atomDef.orbitalEnergyDefinitions.size();++j)
                    {
                        if (!atomInfo.orbitalEnergy.contains(atomDef.orbitalEnergyDefinitions[j].orbital))
                        {
                            if (all_orbs.contains(atomDef.orbitalEnergyDefinitions[j].orbital))
                            {
                                if (all_orbs.contains(atomDef.orbitalEnergyDefinitions[j].referredOrbital))
                                {
                                    os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                                       << QString("Orbital %1: %2 [%3, %4] relative to %5")
                                       .arg(atomDef.orbitalEnergyDefinitions[j].orbital, 5).arg("OPT",10)
                                       .arg(atomDef.orbitalEnergyDefinitions[j].range.first, 8, 'f', 5)
                                       .arg(atomDef.orbitalEnergyDefinitions[j].range.second, 8, 'f', 5)
                                       .arg(atomDef.orbitalEnergyDefinitions[j].referredOrbital)
                                       << endl;

                                }
                                else if (atomDef.orbitalEnergyDefinitions[j].referredOrbital.toLower() == "none")
                                {
                                    os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                                       << QString("Orbital %1: %2 [%3, %4]")
                                       .arg(atomDef.orbitalEnergyDefinitions[j].orbital, 5).arg("OPT",10)
                                       .arg(atomDef.orbitalEnergyDefinitions[j].range.first, 8, 'f', 5)
                                       .arg(atomDef.orbitalEnergyDefinitions[j].range.second, 8, 'f', 5)
                                       << endl;
                                }
                                all_orbs_local.remove(atomDef.orbitalEnergyDefinitions[j].orbital);
                            }
                        }
                    }
                    if (!all_orbs_local.isEmpty())
                    {
                        os << "You are missing some data for orbitals " << static_cast<QStringList>(all_orbs_local.toList()).join(",") << "." << endl;
                        throw std::runtime_error("Missing atomic data");
                    }
                }
                {
                    os << INDENT_SPACES << INDENT_SPACES << "Hubbard parameter: " << endl;
                    QSet<QString> all_orbs_local = all_orbs;
                    QMapIterator<QString, double> it(atomInfo.hubbard);
                    while(it.hasNext())
                    {
                        it.next();
                        QString orb = it.key();
                        all_orbs_local.remove(orb);
                        double value = it.value();

                        bool foundOpt = false;
                        int index = -1;
                        for(int j=0; j< atomDef.hubbardDefinitions.size();++j)
                        {
                            if (atomDef.hubbardDefinitions[j].orbital == orb)
                            {
                                foundOpt = true;
                                index = j;
                                break;
                            }
                        }

                        if (foundOpt)
                        {
                            os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                               << QString("Orbital %1: %2 [%3, %4]").arg(orb, 5).arg("OPT",10)
                                  .arg(atomDef.hubbardDefinitions[index].range.first, 8, 'f', 5)
                                  .arg(atomDef.hubbardDefinitions[index].range.second, 8, 'f', 5)
                               << endl;
                        }
                        else
                        {
                            os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                               << QString("Orbital %1: %2").arg(orb, 5).arg(value, 10, 'f', 6) << endl;
                        }
                    }
                    for(int j=0; j< atomDef.hubbardDefinitions.size();++j)
                    {
                        if (!atomInfo.hubbard.contains(atomDef.hubbardDefinitions[j].orbital) &&
                                all_orbs.contains(atomDef.orbitalEnergyDefinitions[j].orbital))
                        {
                            os << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES
                               << QString("Orbital %1: %2")
                                  .arg(atomDef.hubbardDefinitions[j].orbital, 5).arg("OPT",10) << endl;
                            all_orbs_local.remove(atomDef.hubbardDefinitions[j].orbital);
                        }
                    }
                    if (!all_orbs_local.isEmpty())
                    {
                        os << "You are missing some data for orbitals " << static_cast<QStringList>(all_orbs_local.toList()).join(",") << "." << endl;
                        throw std::runtime_error("Missing atomic data");
                    }
                }
                if (!atomDef.hubbardDerivativeDefinitions.isEmpty())
                {
                    os << INDENT_SPACES << INDENT_SPACES
                       << QString("Hubbard derivatives: %1").arg("OPT", 8) << endl;
                }
            }
            os << endl;
        }

    }




    auto generated_elec_skpairs = required_elec_skpairs - loaded_elec_skpairs.keys().toSet();
    os << "The following electronic parts will be generated:" << endl;
    {
        QSetIterator<QString> it(generated_elec_skpairs);
        while(it.hasNext())
        {
            os << INDENT_SPACES << it.next() << endl;
        }
    }
    os << endl;

    auto generated_elec_skpairs_list = generated_elec_skpairs.toList();
    if (input.skbuilder_input)
    {
        input.skbuilder_input->desired_pairs.clear();
        for(int i=0; i<generated_elec_skpairs_list.size();++i)
        {
            input.skbuilder_input->desired_pairs.append(generated_elec_skpairs_list[i]);
        }
        input.skbuilder_input->all_pairs = false;
    }

    QMapIterator<QString, QList<QString> > it(loaded_elec_skpairs);

    while(it.hasNext())
    {
        it.next();
        input.parsed_external_elec_skpairs.insert(it.key(), it.value());
    }




    //Electronic part ended;


    if (input.psoVector.repulsivePart)
    {

    ///todo: should be like this?
    ///maybe the external repulsive should be provided in the SKFileInfo with the electronic files
    /// after all, we are using the Etot-Erep scheme
    QMap<QString, QString> external_rep_potentials;
    external_rep_potentials = input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials;

//    {
//        QMapIterator<QString, QString> it(input.skbuilder_input->external_repulsive_potentials);
//        while(it.hasNext())
//        {
//            it.next();
//            try
//            {
//                auto poten = ADPT::PotentialName::fromString(it.key());
//                external_rep_potentials.insert(poten.toString(), it.value());
//            }
//            catch(...)
//            {

//            }
//        }

//    }

    if (!external_rep_potentials.isEmpty())
    {
        os << "External Repulsive Potentials: " << endl;
        QMapIterator<QString, QString>  it(external_rep_potentials);
        while(it.hasNext())
        {
            it.next();
            auto pair = it.key();
            QString path  = it.value();
            os << QString("%1 = %2").arg(pair).arg(path) << endl;
        }
    }
    os << endl;

    QList<QString> opt_rep_potentials =
    input.psoVector.repulsivePart->erepfitPSOVector->repulsivePotentials.keys();

    int size = opt_rep_potentials.size();
    for(int i=0; i<size; ++i)
    {
        auto swaped = ADPT::PotentialName::fromString(opt_rep_potentials[i]).swapNames();
        if (!opt_rep_potentials.contains(swaped.toString()))
        {
            opt_rep_potentials.append(swaped.toString());
        }

    }
    qSort(opt_rep_potentials);

    os << "The following repulsive potentials will be optimized: " << endl;
    {
        QListIterator<QString>  it(opt_rep_potentials);
        while(it.hasNext())
        {
            QString poten = it.next();
            if (external_rep_potentials.contains(poten))
            {
                os << "WARNING: The potential " << poten << " was loaded, removing request of optimizing this potential." << endl;
                input.psoVector.repulsivePart->erepfitPSOVector->repulsivePotentials.remove(poten);
            }
            else
            {
                os << INDENT_SPACES << poten << endl;
            }
        }
    }
    os << endl;


    QList<QString> missing_rep_potentials =
            (required_rep_skpairs - external_rep_potentials.keys().toSet() - opt_rep_potentials.toSet()).toList();

    if (!missing_rep_potentials.isEmpty())
    {
        os << "The following repulsive potentials are missing: " << endl;
        QListIterator<QString>  it(missing_rep_potentials);
        while(it.hasNext())
        {
            os << INDENT_SPACES << it.next() << endl;
        }

        os << "WARNING: Missing repulsive part" << endl;
        throw std::runtime_error("Missing repulsive part");
    }

    os << endl;

    }


    //Independent part
    if (input.psoVector.independendPart)
    {
        os << "Independent parameters:" << endl;
        if (input.psoVector.independendPart->optDampingFactor)
        {
            os << "  Damping Factor: OPT" << endl
               << QString("    Range: [%1, %2]").arg(input.psoVector.independendPart->dampingFactorRange.first)
                  .arg(input.psoVector.independendPart->dampingFactorRange.second) << endl;
        }
        if (!input.psoVector.independendPart->ljDispersion.empty())
        {
            os << "  LJ Dispersion:" << endl;
            for(int i=0; i<input.psoVector.independendPart->ljDispersion.size(); ++i)
            {
                os << INDENT_SPACES << input.psoVector.independendPart->ljDispersion[i].element << endl;
                os << INDENT_SPACES << INDENT_SPACES
                   << QString("Energy Range: [%1, %2]").arg(input.psoVector.independendPart->ljDispersion[i].energy_range.first, 8, 'f', 3)
                      .arg(input.psoVector.independendPart->ljDispersion[i].energy_range.second, 8, 'f', 3)
                   << endl;
                os << INDENT_SPACES << INDENT_SPACES
                   << QString("Distance Range: [%1, %2]").arg(input.psoVector.independendPart->ljDispersion[i].distance_range.first, 8, 'f', 3)
                      .arg(input.psoVector.independendPart->ljDispersion[i].distance_range.second, 8, 'f', 3)
                   << endl;
            }
        }
    }

    os << endl;
    os << STARLINE << endl;
}




void print_header_and_version(QTextStream& os)
{
    os << EQUALLINE << endl << endl;
    os << centerString("Particle Swarm Optimizer for the paraterization of the DFTB method") << endl;
    os << endl;
    os << centerString("Powered by Chien-Pin Chou") << endl;
    os << endl << centerString(QString(GIT_VERSION) + " (" + QString(BUILD_DATE) + ")" )//centerString(QString("%1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_REVISION))
       << endl << endl << endl;
    os << EQUALLINE << endl << endl;
}



libvariant::Variant readSchema(const QString& file)
{
    QFile schema_file(file);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    libvariant::Variant schema = libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
    schema_file.close();
    return schema;
}


int loadInput(QTextStream& stdout_stream, DFTBPSO::Input::SKOPTInput& inputData,
               const QString& inputfilename, bool& require_skbuilder_input, bool& require_repulsive_input, bool yaml_input = false)
{
    try
    {
        stdout_stream << "Reading main input files ... ";

        QFileInfo input(inputfilename);
        if (!input.exists() || !input.isReadable())
        {
            throw std::runtime_error(QString("File %1 is not existing or not readable .").arg(input.absoluteFilePath()).toStdString());
        }

        libvariant::Variant var;

        if (yaml_input)
        {
            var = libvariant::DeserializeYAMLFile(inputfilename.toStdString().c_str());
        }
        else
        {
            var = libvariant::DeserializeJSONFile(inputfilename.toStdString().c_str());
        }
        stdout_stream << "done." << endl;

        libvariant::Variant schema = readSchema(":/schema/pso_input_schema.json");

        libvariant::AdvSchemaLoader loader;
        libvariant::Variant empty;
        loader.AddSchema("pso_vector.json", empty);

        stdout_stream << "Validating main input ... ";
        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);
        if (result.Error())
        {
            stdout_stream << "failed." << endl;
            auto schema_errors = result.GetSchemaErrors();
            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(std::cout);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(std::cout);
            }
            return 1;
        }
        stdout_stream << "done." << endl;

        //after varify main input, we need to load tester inputs
        {
            //tester input
            QString tester_inputfile = QString::fromStdString(var["tester"]["input_file"].AsString());;
            stdout_stream << "Reading Tester input file: " <<  tester_inputfile << " ... " ;
            libvariant::Variant var_tester_input;
            if (yaml_input)
            {
                var_tester_input = libvariant::DeserializeYAMLFile(tester_inputfile.toStdString().c_str());
            }
            else
            {
                var_tester_input = libvariant::DeserializeJSONFile(tester_inputfile.toStdString().c_str());
            }
            stdout_stream << "done." << endl;

            //validate

            stdout_stream << "Validating Tester input ... ";

            libvariant::Variant schema = readSchema(":/schema/tester_input_schema.json");
            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_tester_input, &loader);

            if (result.Error())
            {
                stdout_stream << "failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                return 1;
            }
            stdout_stream << "done." << endl;
            var["tester"]["input_data"] = var_tester_input;
        }


        {
            QString tester_referencefile = QString::fromStdString(var["tester"]["reference_file"].AsString());
            stdout_stream << "Reading Tester reference file: " <<  tester_referencefile << " ... " ;
            libvariant::Variant var_tester_reference;
            if (yaml_input)
            {
                var_tester_reference = libvariant::DeserializeYAMLFile(tester_referencefile.toStdString().c_str());
            }
            else
            {
                var_tester_reference = libvariant::DeserializeJSONFile(tester_referencefile.toStdString().c_str());
            }
            stdout_stream << "done." << endl;

            libvariant::Variant schema = readSchema(":/schema/tester_ref_schema.json");

            stdout_stream << "Validating Tester reference ... ";

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_tester_reference, &loader);

            if (result.Error())
            {
                stdout_stream << "failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                return 1;
            }

            stdout_stream << "done." << endl;
            var["tester"]["reference_data"] = var_tester_reference;
        }

        stdout_stream << "Processing main input file ... ";
        libvariant::VariantConvertor<DFTBPSO::Input::SKOPTInput>::fromVariant(var, inputData);
        stdout_stream << "done." << endl;


        require_skbuilder_input = checkIfSKBuilderInputNeeded(stdout_stream, inputData);

        stdout_stream << "SKBuilder input needed? " << require_skbuilder_input << endl;
        if (!require_skbuilder_input)
        {
            //if skbuilder_input is not required by the tester, but if it's specified, then load it anyway.
            if (var.Contains("skbuilder_input_file"))
            {
                QString skbuilder_inputfile = QString::fromStdString(var["skbuilder_input_file"].AsString());
                if (QFileInfo(skbuilder_inputfile).exists())
                {
                    require_skbuilder_input = true;
                }
            }
        }



        if (require_skbuilder_input)
        {
            QString skbuilder_inputfile = QString::fromStdString(var["skbuilder_input_file"].AsString());
            stdout_stream << "Reading SKBuilder input file: " <<  skbuilder_inputfile << " ... ";

            libvariant::Variant var_skbuilder;
            if (yaml_input)
            {
                var_skbuilder = libvariant::DeserializeYAMLFile(skbuilder_inputfile.toStdString().c_str());
            }
            else
            {
                var_skbuilder = libvariant::DeserializeJSONFile(skbuilder_inputfile.toStdString().c_str());
            }

            stdout_stream << "done." << endl;

            libvariant::Variant schema = readSchema(":/schema/skbuilder_input_schema.json");
            loader.AddSchema("onecenter_parameters_nctu.json", readSchema(":/schema/onecenter_parameters_nctu.json"));
            loader.AddSchema("onecenter_parameters_bccms.json", readSchema(":/schema/onecenter_parameters_bccms.json"));
            loader.AddSchema("twocenter_parameters_nctu.json", readSchema(":/schema/twocenter_parameters_nctu.json"));
            loader.AddSchema("twocenter_parameters_bccms.json", readSchema(":/schema/twocenter_parameters_bccms.json"));

            stdout_stream << "Validating SKBuilder input file ... ";

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_skbuilder, &loader);

            if (result.Error())
            {
                stdout_stream << "failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                return 1;
            }

            stdout_stream << "done." << endl;
            var["skbuilder_input_data"] = var_skbuilder;

            GET_VARIANT_PTR(inputData.skbuilder_input, var, "skbuilder_input_data", SKBuilder::Input::SKBuilderInput);

            inputData.skbuilder_input->options.outputPrefix = "";
            inputData.skbuilder_input->options.saveIntermediate = false;
        }



        require_repulsive_input = checkIfRepulsiveInputNeeded(inputData);

        if (var["pso"].Contains("additional_fitness"))
        {
            if (var["pso"]["additional_fitness"].Contains("type"))
            {
                if (QString::fromStdString(var["pso"]["additional_fitness"]["type"].AsString()).toLower() == "erepfit")
                {
                    require_repulsive_input = true;
                }
            }
        }

        if (require_repulsive_input)
        {
            if (var["pso_vector"].Contains("repulsive_part"))
            {
                libvariant::Variant var_rep_part = var["pso_vector"]["repulsive_part"];

                QString rep_type = QString::fromStdString(var_rep_part["repulsive_type"].AsString());
                if (rep_type.toLower() == "erepfit")
                {
                    if (var_rep_part["erepfit_psovector"].Contains("erepfit_input_file"))
                    {
                        QString infile = QString::fromStdString(var_rep_part["erepfit_psovector"]["erepfit_input_file"].AsString());
                        stdout_stream << "Reading Erepfit input file: " <<  infile << " ..." << endl;


                        libvariant::Variant var_infile;
                        if (yaml_input)
                        {
                            var_infile = libvariant::DeserializeYAMLFile(infile.toStdString().c_str());
                        }
                        else
                        {
                            var_infile = libvariant::DeserializeJSONFile(infile.toStdString().c_str());
                        }
                        stdout_stream << "Erepfit input file: " <<  infile << " parsed." << endl;

//                        libvariant::Variant schema = readSchema(":/schema/erepfit_input_schema.json");
//                        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_infile, &loader);

//                        if (result.Error())
//                        {
//                            auto schema_errors = result.GetSchemaErrors();
//                            for(unsigned i=0; i<schema_errors.size();++i)
//                            {
//                                schema_errors[i].PrettyPrintMessage(std::cout);
//                            }
//                            auto validation_errors = result.GetErrors();
//                            for(unsigned i=0; i<validation_errors.size();++i)
//                            {
//                                validation_errors[i].PrettyPrintMessage(std::cout);
//                            }
//                            return 1;
//                        }

//                        stdout_stream << "Erepfit input file schema check passed" << endl;
                        var["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit_input_data"] = var_infile;
                        ElectronicPartCacheDatabase &db = Singleton<ElectronicPartCacheDatabase>::Instance();
                        db.load();
                    }
                    else
                    {
                        throw std::runtime_error("erepfit_input_file not found");
                    }
                }
                else
                {
                    throw std::runtime_error("Repulsive optimization is needed but no PSOVector for repulsive defined.");
                }
            }
            else
            {
                throw std::runtime_error("Repulsive optimization is needed but no PSOVector for repulsive defined.");
            }
            libvariant::Variant erepfit_schema = readSchema(":/schema/erepfit_input_schema.json");
            loader.AddSchema("erepfit_input_schema.json", erepfit_schema);
        }

        libvariant::Variant pso_vector_schema = readSchema(":/schema/pso_vector.json");


//        if (require_skbuilder_input)
//        {
            schema = readSchema(":/schema/confining_range_bccms.json");
            loader.AddSchema("confining_range_bccms.json", schema);
            schema = readSchema(":/schema/confining_range_nctu.json");
            loader.AddSchema("confining_range_nctu.json", schema);
//        }


        libvariant::Variant var_psovector = var["pso_vector"];


        stdout_stream << "Validating PSOVector ... " ;

        result = libvariant::SchemaValidate(pso_vector_schema, var_psovector, &loader);

        if (result.Error())
        {
            stdout_stream << "failed." << endl;
            auto schema_errors = result.GetSchemaErrors();
            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(std::cout);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(std::cout);
            }
            return 1;
        }

        stdout_stream << "done." << endl;

        stdout_stream << "Processing PSOVector ... ";
        if (require_repulsive_input)
        {
            GET_VARIANT_PTR(inputData.psoVector.repulsivePart, var_psovector, "repulsive_part", DFTBPSO::Input::RepulsivePart);
        }
        if (require_skbuilder_input)
        {
            GET_VARIANT_PTR_1(inputData.psoVector.electronicPart, var_psovector, "electronic_part", DFTBPSO::Input::ElectronicPart, inputData.skbuilder_input->toolchain_info.name);
        }

        bool hasValue;
        GET_VARIANT_PTR_OPT(inputData.psoVector.independendPart, var_psovector, "independent_part", DFTBPSO::Input::IndependentPart, hasValue);

        stdout_stream << "done." << endl;

        stdout_stream << "All input file loaded." << endl;

        libvariant::Variant var_parsed = libvariant::VariantConvertor<DFTBPSO::Input::SKOPTInput>::toVariant(inputData);
        auto filename = input.baseName() +"_parsed.json";
        libvariant::SerializeJSON(filename.toStdString(), var_parsed, true);
        filename = input.baseName() +"_parsed.yaml";
        libvariant::SerializeYAML(filename.toStdString(), var_parsed);

        makeAbsolutePath(input.absoluteDir(), inputData);

        check_input(stdout_stream, inputData);

        libPSO::RandomNumberRenerator<double>& gen = Singleton<libPSO::RandomNumberRenerator<double> >::Instance();
        gen.setRandomSeed(inputData.options.randomSeed);

        Q_UNUSED(hasValue);

    }
    catch(KeywordNotFound& e)
    {
        stdout_stream << EXCLALINE << endl;
        stdout_stream << "Keyword not found: " << e.what() << endl
             << "Program Terminated." << endl;
        stdout_stream << EXCLALINE << endl;
        return 1;
    }
    catch(std::exception& e)
    {
        stdout_stream << EXCLALINE << endl;
        stdout_stream << e.what() << endl
             << "Program Terminated." << endl;
        stdout_stream << EXCLALINE << endl;
        return 1;
    }

    return 0;
}




int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    QCoreApplication::setApplicationName("PSOSKCreator");
    QCoreApplication::setApplicationVersion(QString(GIT_VERSION));

    QTextStream stdout_stream(stdout);
    QTextStream stderr_stream(stderr);

    print_header_and_version(stdout_stream);

    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("input", "Input file");

    QCommandLineOption testOption("t", "Testing mode.");
    parser.addOption(testOption);

    QCommandLineOption yamlOption("y", "Parse input file as the YAML format.");
    parser.addOption(yamlOption);

    QCommandLineOption debugOption("d", "Debug output");
    parser.addOption(debugOption);

    parser.process(a);

    if (parser.positionalArguments().empty())
    {
        parser.showHelp(1);
        return 1;
    }



    QString inputfilename = parser.positionalArguments().first();

    DFTBPSO::Input::SKOPTInput inputData;

    if (parser.isSet(debugOption))
    {
        inputData.options.debug = true;
    }
    else
    {
        qInstallMessageHandler(myMessageOutput);
    }

    bool require_skbuilder_input = false;
    bool require_repulsive_input = false;
    bool succ = loadInput(stdout_stream, inputData, inputfilename, require_skbuilder_input, require_repulsive_input, parser.isSet(yamlOption));

    if (succ != 0)
    {
        return 1;
    }

    if (parser.isSet(debugOption))
    {
        inputData.options.debug = true;
    }
    if (parser.isSet(testOption))
    {
        stdout_stream << "Find -t options, running in testing mode." << endl;
        inputData.pso.numOfParticles = 1;
        inputData.pso.numOfIterations = 1;
    }

    std::shared_ptr<DFTBPSO::DFTBPSOVector> vector = std::make_shared<DFTBPSO::DFTBPSOVector>();
    try
    {
        if (!vector->define(&inputData))
        {
            stdout_stream << "Initialization of PSOVector failed" << endl;
            throw std::runtime_error("PSOVector initialization error");
        }
        else
        {
            stdout_stream << "Initialization of PSOVector success" << endl;
        }


        stdout_stream << "Checking essential programs..." << endl;
        if (vector->hasAtomicVector())
        {
            QString toolkit_name = inputData.skbuilder_input->toolchain_info.name;
            auto toolkit = SKBuilder::AbstractSKToolkit::getInstance(toolkit_name, stdout_stream, inputData.skbuilder_input->toolchain_info);
//            stdout_stream << INDENT_SPACES <<  "Checking one-center program...";

            bool succ = toolkit->setOneConterProgram(inputData.skbuilder_input->toolchain_info.onecent_info.path);
            if (!succ)
            {
                stdout_stream << endl;
                throw std::runtime_error("Onecent program is not working");
            }
//            stdout_stream << "works" << endl;

//            stdout_stream << INDENT_SPACES << "Checking two-center program...";
            succ = toolkit->setTwoConterProgram(inputData.skbuilder_input->toolchain_info.twocent_info.path);
            if (!succ)
            {
                stdout_stream << endl;
                throw std::runtime_error("Twocent program is not working");
            }
//            stdout_stream << "works" << endl;
            toolkit.reset();
        }
        else
        {
            if (require_skbuilder_input)
            {
                throw std::runtime_error("SKBuilder needs to be invoked, but no AtomicVectors defined.");
            }
        }

///TODO: check also for DC-DFTB-K
        if (inputData.tester.input.control.DFTBCodeType.toLower() == "dftb+")
        {

            DFTBPlus::DFTBPlusAdaptor& dftbplus = Singleton<DFTBPlus::DFTBPlusAdaptor>::Instance();

            stdout_stream << INDENT_SPACES << "Checking DFTB+ program..." ;


            if (inputData.psoVector.repulsivePart)
            {
                inputData.tester.input.control.DFTBPath = inputData.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.options.toolchain.path;
            }

            stdout_stream << " (" << inputData.tester.input.control.DFTBPath <<  " ) ... " ;
            if (!dftbplus.setProgramPath(inputData.tester.input.control.DFTBPath))
            {
                throw std::runtime_error("DFTB+ implementation is not working.\n"
                                         "Now only the modified opensource version of DFTB+ can be used.\n"
                                         "Download it at https://github.com/solccp/dftbplus/tree/parameterization");
            }
            else
            {
                if (inputData.psoVector.repulsivePart)
                {
    //                inputData.psoVector.repulsivePart.erepfitPSOVector.erepfit2_input.options.toolchain.path = inputData.pso.tester_input.testSuiteInput.control.DFTBPath;

                    if (inputData.psoVector.repulsivePart->fourthOrder)
                    {
                        if (!dftbplus.hasFeature(DFTBPlus::DFTBPlusAdaptor::FourthOrderSplineRepulsiveFeature))
                        {
                            stdout_stream << "WARNING: Fourth-order spline for repulsive potentail was enabled in the input file. " << endl
                                          << "However, the current DFTB implementation is not supporting it. Returning to fallback settings" << endl;
                            inputData.psoVector.repulsivePart->fourthOrder = false;
                        }
                    }


                }
            }
        }

        stdout_stream << "works" << endl;
        stdout_stream << STARLINE << endl;
    }
    catch(std::exception& e)
    {
        stdout_stream << "failed." << endl;
        stderr_stream << e.what() << endl
             << "Program Terminated." << endl;
        stdout_stream << EXCLALINE << endl;
        return 1;
    }

    stdout_stream << "Scratch dir: " << inputData.options.scratchDir << endl;

    auto pop = std::make_shared<libPSO::PSOPopulation>(inputData.pso.numOfParticles,vector);

    pop->initialize();


    QList<std::shared_ptr<DFTBPSO::DFTBPSOVector>> guess_vectors;
    QList<double> guess_spreadings;
    QList<int> guess_si;


    if (!inputData.pso.guess.items.isEmpty())
    {
        stdout_stream << STARLINE << endl;
        stdout_stream << centerString("Initial Guess PSO Vectors") << endl;
        stdout_stream << HYPHENLINE << endl;


        stdout_stream << QString("Loading guess vectors... %1 vectors are found from input")
                         .arg(inputData.pso.guess.items.size()) << endl;

        for(int i=0; i<inputData.pso.guess.items.size(); ++i)
        {
            stdout_stream << INDENT_SPACES << "Loading guess vector index " << i+1 << " ..." << endl << HYPHENLINE << endl;


            std::shared_ptr<DFTBPSO::DFTBPSOVector> guess_vector = nullptr;
            QFileInfo info(inputData.pso.guess.items[i].File);
            QString absPath = info.absoluteDir().absoluteFilePath(info.fileName());

            if (info.exists() && info.isFile() && info.isReadable())
            {
                guess_vector = std::dynamic_pointer_cast<DFTBPSO::DFTBPSOVector>(vector->clone());
                guess_vector->randomizePosition();

                try
                {

                    libvariant::SerializeType stype;
                    if (parser.isSet(yamlOption))
                    {
                        stype = libvariant::SERIALIZE_YAML;
                    }
                    else
                    {
                        stype = libvariant::SERIALIZE_JSON;
                    }
                    libvariant::Variant var = libvariant::DeserializeFile(absPath.toStdString().c_str(), stype);
                    libvariant::VariantConvertor<DFTBPSO::DFTBPSOVector>::fromVariant(var, *guess_vector);
                    stdout_stream << "done." << endl;

                    guess_vectors.append(guess_vector);
                    guess_spreadings.append(inputData.pso.guess.items[i].Spreading);
                    guess_si.append(inputData.pso.guess.items[i].startingIteration);

                }
                catch(std::exception &e)
                {
                    stdout_stream << "failed.";
                    stdout_stream << EXCLALINE << endl;
                    stderr_stream << e.what() << endl;
                    stdout_stream << EXCLALINE << endl;
                    stdout_stream << "Not a valid guess file." << endl;
                }
            }
            else
            {
                stdout_stream << "Not a valid guess file." << endl;
            }
        }

        stdout_stream << HYPHENLINE << endl;

        if (!guess_vectors.isEmpty())
        {


            int guessTotal = inputData.pso.numOfParticles*inputData.pso.guess.Occupancy;

            if (guessTotal==0)
                guessTotal = 1;

            stdout_stream << QString("Maximum # of guess vectors: %1(Population) * %2(Occupancy)= %3")
                             .arg(inputData.pso.numOfParticles).arg(inputData.pso.guess.Occupancy)
                             .arg(guessTotal) << endl << endl;

            if (guessTotal < guess_vectors.size())
            {
                stdout_stream << "The # of guess vectors from input are more than the maximum." << endl
                              << QString("Only the first %1 guess vectors will be used.").arg(guessTotal) << endl << endl;
            }

            QVector<int> guessN(guess_vectors.size(),0);
            int nrest = guessTotal;
            int index = 0;
            while(nrest > 0)
            {
                guessN[index]++;
                nrest--;
                index++;
                if (index>guessN.size()-1)
                {
                    index = 0;
                }
            }


            stdout_stream << endl << EQUALLINE << endl << centerString("Guess Vector Details") << endl << endl;


            stdout_stream << QString("%1 %2 %3 %4")
                             .arg("Index",7).arg("Occupation",13)
                             .arg("StartingIteration", 25).arg("Spreading", 12)
                          << endl << HYPHENLINE << endl;

            for(int i=0; i<guessN.size();++i)
            {
                stdout_stream << QString("%1 %2 %3 %4")
                                 .arg(i+1, 7).arg(guessN[i], 13)
                                 .arg(guess_si[i], 25).arg(guess_spreadings[i], 12,'f',6)
                              << endl;
            }

            for(int i=0; i<guessN.size();++i)
            {
                if (guessN[i] == 0)
                    continue;
                pop->addGuess(guess_vectors[i], guess_si[i], guessN[i], guess_spreadings[i]);
            }
            stdout_stream << HYPHENLINE << endl << endl;
        }
        else
        {
            stdout_stream << "No valid guess loaded, skipping..." << endl << endl;
        }
    }

    QByteArray nslots = qgetenv("NSLOTS");
    QThreadPool *pool = QThreadPool::globalInstance();
    int ncpu = nslots.toInt();

    if (ncpu > inputData.pso.numOfParticles)
    {
        ncpu = inputData.pso.numOfParticles;
    }

    if (ncpu <= 0)
    {
        ncpu = 1;
    }
    else if (ncpu > QThread::idealThreadCount())
    {
        ncpu = -1;
    }

    if (ncpu > 0)
    {
        pool->setMaxThreadCount(ncpu);
    }


    std::shared_ptr<libPSO::PSOAlgorithm> algo = nullptr;


    if (inputData.pso.algorithm.toLower() == "pso")
    {
        stdout_stream << "Algorithm: " << "Single-Objective with meta-multi-objective function" << endl;
        algo = std::make_shared<libPSO::SimplePSOAlgorithm>(pop);
        algo->setProgressUpdater(DFTBPSO::sopso_progressUpdater);
        algo->setEvaluator(DFTBPSO::sopso_particle_evaluation);
    }
    else if (inputData.pso.algorithm.toLower() == "mopso")
    {
        stdout_stream << "Algorithm: " << "Multi-Objective (beta)" << endl;
        algo = std::make_shared<libPSO::MOPSOAlgorithm>(pop, inputData.pso.MOPSOMaxRepoSize);
        algo->setProgressUpdater(DFTBPSO::mopso_progressUpdater);
        algo->setEvaluator(DFTBPSO::mopso_particle_evaluation);
    }
    else
    {
        throw std::runtime_error(inputData.pso.algorithm.toStdString() + " is not implemented");
    }



    algo->setMaxIterations(inputData.pso.numOfIterations);

    stdout_stream << endl << EQUALLINE << endl;
    stdout_stream << "Number of CPUs: " << ncpu << endl;
    stdout_stream << "Random seed: " << inputData.options.randomSeed << endl;
    stdout_stream << "Number of particles: " << inputData.pso.numOfParticles << endl;
    stdout_stream << "Number of Iterations: " << inputData.pso.numOfIterations << endl;


    int returnCode = 0;
    try
    {
        algo->evolve();

        stdout_stream << HYPHENLINE << endl;
        stdout_stream << "Normal Termination" << endl;


    }
    catch(std::exception &e)
    {
        stdout_stream << EXCLALINE << endl;
        stderr_stream << e.what() << endl
             << "Program Terminated." << endl;
        stdout_stream << EXCLALINE << endl;
        returnCode = 1;
    }


//    delete algo;
//    delete pop;
//    for(int i=0; i<guess_vectors.size();++i)
//    {
//        delete guess_vectors[i];
//    }

    return returnCode;
}
