#include "optimizer.h"
#include "typesettings.h"

#include "input.h"
#include "basic_types.h"

#include <QSet>

namespace DFTBPSO
{

    void print_header_and_version(QTextStream& os)
    {
        os << EQUALLINE << endl << endl;
        os << centerString("Particle Swarm Optimizer for the paraterization of the DFTB method") << endl;
        os << endl;
        os << centerString("Powered by Chien-Pin Chou") << endl;
        os << endl << centerString(QString(GIT_VERSION))
           << endl << endl << endl;
        os << EQUALLINE << endl << endl;
    }

    libvariant::Variant readSchemaFile(QFile& schema_file)
    {
        schema_file.open(QIODevice::ReadOnly);
        QString schema_str = schema_file.readAll();
        libvariant::Variant schema = libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);

        return schema;
    }


    Optimizer::Optimizer(QTextStream &output_stream) : m_output_stream(output_stream)
    {

    }

    void Optimizer::run(Optimizer::InputFormat format, const QString &inputfile)
    {
        m_input_format = format;
        print_header_and_version(m_output_stream);

        inputData = std::make_shared<Input::SKOPTInput>();

        if (m_testMode)
        {
            inputData->pso.numOfParticles = 1;
            inputData->pso.numOfIterations = 1;
        }
//! TODO Finish this
        try
        {
            //load main input and tester inputs
            loadMainInput(inputfile);

            checkSKFileRequirements();
            loadPSOVectorInput();
        }
    }


    bool Optimizer::testMode() const
    {
        return m_testMode;
    }

    void Optimizer::setTestMode(bool testMode)
    {
        m_testMode = testMode;
    }
    QSet<QString> Optimizer::getRequired_elec_skpairs() const
    {
        return m_required_elec_skpairs;
    }

    void Optimizer::setRequired_elec_skpairs(const QSet<QString> &value)
    {
        m_required_elec_skpairs = value;
    }


    void Optimizer::loadMainInput(const QString &inputfile)
    {
        QFileInfo input(inputfilename);

        //Parse and verify the main input
        //NOTE: pso_vector section is not varified
        m_output_stream << "Loading the main input file: " <<  inputfilename << " ... ";
        if (!input.exists() || !input.isReadable())
        {
            throw std::runtime_error(QString("File %1 is not existing or not readable .").arg(input.absoluteFilePath()).toStdString());
        }

        try
        {
            libvariant::AdvSchemaLoader loader;
            if (m_input_format == InputFormat::YAML)
            {
                var_input = libvariant::DeserializeYAMLFile(inputfilename.toStdString().c_str());
            }
            else
            {
                var_input = libvariant::DeserializeJSONFile(inputfilename.toStdString().c_str());
            }

            QFile schema_file(":/schema/input_schema_v2.json");
            libvariant::Variant schema = readSchemaFile(schema_file);

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_input, &loader);

            if (result.Error())
            {
                m_output_stream << " failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                throw SchemaError();
            }
            m_output_stream << " done." << endl;
        }
        catch(SchemaError& e)
        {
            throw std::runtime_error("Invalid format of main input file.");
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Invalid format of main input file.");
        }


        //tester inputs
        try
        {

            //Let's check tester reference
            QString tester_referencefile = QString::fromStdString(var_input["tester"]["reference_file"].AsString());
            m_output_stream << "Loading DFTBTester reference file: " <<  tester_referencefile << " ... " ;

            libvariant::Variant var_tester_reference;

            if (m_input_format == InputFormat::YAML)
            {
                var_tester_reference = libvariant::DeserializeYAMLFile(tester_referencefile.toStdString().c_str());
            }
            else
            {
                var_tester_reference = libvariant::DeserializeJSONFile(tester_referencefile.toStdString().c_str());
            }


            libvariant::AdvSchemaLoader loader;

            QFile schema_file(":/schema/tester_ref_schema_v1.json");
            libvariant::Variant schema = readSchemaFile(schema_file);
            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_tester_reference, &loader);

            if (result.Error())
            {
                m_output_stream << " failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                throw SchemaError();
            }

            var_input["tester"]["reference_data"] = var_tester_reference;
            m_output_stream << "done." << endl;

        }
        catch(SchemaError& e)
        {
            throw std::runtime_error("Invalid format of tester reference file.");
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Invalid format of tester reference file.");
        }
        try
        {

            QString tester_inputfile = QString::fromStdString(var_input["tester"]["input_file"].AsString());;
            m_output_stream << "Loading DFTBTester input file: " <<  tester_inputfile << " ... " ;
            libvariant::Variant var_tester_input;

            if (m_input_format == InputFormat::YAML)
            {
                var_tester_input = libvariant::DeserializeYAMLFile(tester_inputfile.toStdString().c_str());
            }
            else
            {
                var_tester_input = libvariant::DeserializeJSONFile(tester_inputfile.toStdString().c_str());
            }

            QFile schema_file(":/schema/tester_input_schema_v1.json");
            libvariant::Variant schema = readSchemaFile(schema_file);

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_tester_input, &loader);

            if (result.Error())
            {
                 m_output_stream << " failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                throw SchemaError();
            }

            m_output_stream << "done." << endl;
            var_input["tester"]["input_data"] = var_tester_input;
        }
        catch(SchemaError& e)
        {
            throw std::runtime_error("Invalid format of tester input file.");
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Invalid format of tester input file.");
        }


        try
        {
            //Convert the main input file, except the pso_vector and skbuilder;
            m_output_stream << "Processing main input file ... " ;

            //NOTE: This will not convert pso_vector section!!
            libvariant::VariantConvertor<DFTBPSO::Input::SKOPTInput>::fromVariant(var_input, inputData);
            inputData->tester.input.makeAbsolutePath(QDir::current());

//            //check the given external skfiles are symmetric
//            checkIfExternalSKFiles();

            m_output_stream << "done." << endl;
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Error reading main input file.");
        }
    }

    void Optimizer::loadRepulsivePSOVector()
    {
//        if (!var_input["pso_vector"].Contains("repulsive_part"))
//        {
//            throw std::runtime_error("No repulsive PSOVector defined.");
//        }

//        m_output_stream << "Processing input for repulsive PSOVector ... ";
//        try
//        {
//            libvariant::AdvSchemaLoader loader;
//            QFile psovector_schema_file(":/schema/pso_vector_repulsive.json");
//            libvariant::Variant psovector_schema = readSchemaFile(psovector_schema_file);

//            libvariant::SchemaResult result = libvariant::SchemaValidate(
//                        psovector_schema, var_input["pso_vector"]["repulsive_part"], &loader);

//            if (result.Error())
//            {
//                m_output_stream << "failed." << endl;
//                auto schema_errors = result.GetSchemaErrors();
//                for(unsigned i=0; i<schema_errors.size();++i)
//                {
//                    schema_errors[i].PrettyPrintMessage(std::cout);
//                }
//                auto validation_errors = result.GetErrors();
//                for(unsigned i=0; i<validation_errors.size();++i)
//                {
//                    validation_errors[i].PrettyPrintMessage(std::cout);
//                }
//                throw SchemaError();
//            }
//            m_output_stream << "done." << endl;

//            QString repulsive_type = QString::fromStdString(var_input["pso_vector"]["repulsive_part"]["repulsive_type"].AsString()).toLower();

//            if (repulsive_type == "erepfit")
//            {
//                QFile repulsive_schema_file(":/schema/repulsive/erepfit_vector.json");
//                libvariant::Variant repulsive_schema = readSchemaFile(repulsive_schema_file);
////                loader.AddSchema("erepfit_input_schema.json", repulsive_schema);

//                libvariant::SchemaResult result = libvariant::SchemaValidate(repulsive_schema, var_infile, &loader);


//                m_output_stream << "Repulsive PSOVector type: Erepfit" << endl;

//                GET_VARIANT_PTR(inputData->psoVector.repulsivePart, var_input["pso_vector"], "repulsive_part", DFTBPSO::Input::RepulsivePart);






//                QString infile = QString::fromStdString(var["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit_input_file"].AsString());
//                libvariant::Variant var_infile;

//                if (m_input_format == InputFormat::YAML)
//                {
//                    var_infile = libvariant::DeserializeYAMLFile(infile.toStdString().c_str());
//                }
//                else
//                {
//                    var_infile = libvariant::DeserializeJSONFile(infile.toStdString().c_str());
//                }
//                m_output_stream << "Erepfit input file: " <<  infile << " parsed." << endl;

//                libvariant::SchemaResult result = libvariant::SchemaValidate(repulsive_schema, var_infile, &loader);



//            }
















//            GET_VARIANT_PTR(inputData->psoVector.repulsivePart, var_input["pso_vector"], "repulsive_part", DFTBPSO::Input::RepulsivePart);


//            m_output_stream << "Repulsive PSOVector type: " << inputData->psoVector.repulsivePart->repulsiveType << endl;


//        }
//        catch(SchemaError& e)
//        {
//            throw std::runtime_error("Invalid format of repulsive_part of PSOVector.");
//        }
//        catch(std::exception &e)
//        {
//            m_output_stream << " failed." << endl;
//            m_output_stream << e.what() << endl;
//            throw std::runtime_error("Invalid format of repulsive_part of PSOVector.");
//        }



//        QString infile = QString::fromStdString(var["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit_input"].AsString());
//        libvariant::Variant var_infile;

//        if (m_input_format == InputFormat::YAML)
//        {
//            var_infile = libvariant::DeserializeYAMLFile(infile.toStdString().c_str());
//        }
//        else
//        {
//            var_infile = libvariant::DeserializeJSONFile(infile.toStdString().c_str());
//        }
//        stdout_stream << "Erepfit input file: " <<  infile << " parsed." << endl;

//        QFile schema_file(":/schema/erepfit_input_schema_v1.json");
//        schema_file.open(QIODevice::ReadOnly);
//        libvariant::AdvSchemaLoader loader;

//        QString schema_str = schema_file.readAll();

//        libvariant::Variant schema = libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
//        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_infile, &loader);

//        if (result.Error())
//        {
//            auto schema_errors = result.GetSchemaErrors();
//            for(unsigned i=0; i<schema_errors.size();++i)
//            {
//                schema_errors[i].PrettyPrintMessage(std::cout);
//            }
//            auto validation_errors = result.GetErrors();
//            for(unsigned i=0; i<validation_errors.size();++i)
//            {
//                validation_errors[i].PrettyPrintMessage(std::cout);
//            }
//            return 1;
//        }

//        stdout_stream << "Erepfit input file schema check passed" << endl;
//        var["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit2_input"] = var_infile;


    }

    void Optimizer::loadPSOVectorInput()
    {

    }

    void Optimizer::checkIfExternalSKFiles()
    {

        if (input.skfileInfo.isValid)
        {
            //check input.skfileInfo
            QSetIterator<QString> it(all_elec_skpairs);
            while(it.hasNext())
            {
                QString poten = it.next();
                auto pair = potential2ElementPair(poten);
                os << INDENT_SPACES << QString("Checking if %1 exists...").arg(poten);

                //if multi-valence is defined, don't load
                if (input.psoVector->electronicPart->skbuilder_input.data->atomicinfo.contains(pair.first))
                {
                    if (input.psoVector->electronicPart->skbuilder_input.data->atomicinfo[pair.first].orbitals.size() > 1)
                    {
                        continue;
                    }
                }
                if (input.psoVector->electronicPart->skbuilder_input.data->atomicinfo.contains(pair.second))
                {
                    if (input.psoVector->electronicPart->skbuilder_input.data->atomicinfo[pair.second].orbitals.size() > 1)
                    {
                        continue;
                    }
                }

                QStringList skfiles = findSKFile(input.skfileInfo, poten);
                if (!skfiles.isEmpty())
                {
                    loaded_elec_skpairs[poten].append(skfiles);
                    os << "Yes" << endl;
                }
                else
                {
                    os << "No" << endl;
                }
            }

        }



        for(auto const & item : inputData->external_rep_skpairs.keys())
        {
            ADPT::PotentialName poten(item);
            ADPT::PotentialName other = poten.swapNames();
            if (!inputData->external_rep_skpairs.contains(other.toString()))
            {
                inputData->external_rep_skpairs.insert(other.toString(), inputData->external_rep_skpairs[item]);
            }
        }

        for(auto const & item : inputData->external_elec_skpairs.keys())
        {
            ADPT::PotentialName poten(item);
            ADPT::PotentialName other = poten.swapNames();
            if (!inputData->external_elec_skpairs.contains(other.toString()))
            {
                throw std::runtime_error(other.toString().toStdString() + " does not exist in the external_elec_pairs.");
            }
        }
    }

    void UpdatePairs(QSet<QString>& skpairs, const QStringList& elements)
    {
        for(int i=0; i<elements.size();++i)
        {
            for(int j=0; j<elements.size();++j)
            {
                skpairs.insert(QString("%1-%2").arg(elements[i]).arg(elements[j]));
            }
        }
    }

    QStringList findSKFile(DFTBTestSuite::SKFileInfo& skfileInfo, const QString& potential)
    {
        QStringList res;

        if (skfileInfo.type2Names)
        {
            QDir tmpDir(skfileInfo.prefix);
            QString prefix = skfileInfo.prefix;
            if (!tmpDir.isAbsolute())
            {
                prefix = tmpDir.absolutePath() + "/";
            }

            auto pair = ADPT::PotentialName(potential);

            QString fileName = QString("%1%2%3%4%5").arg(prefix)
                    .arg(skfileInfo.lowercase ? pair.getElement1().toLower() : pair.getElement1())
                    .arg(skfileInfo.separator)
                    .arg(skfileInfo.lowercase ? pair.getElement1().toLower() : pair.getElement2())
                    .arg(skfileInfo.suffix);

            QFileInfo info(fileName);
            if (info.exists() && skfileInfo.maxAngularMomentum.contains(pair.getElement1())
                && skfileInfo.maxAngularMomentum.contains(pair.getElement2()) )
            {
                QString fullFileName = QDir::current().absoluteFilePath(fileName);
                res.append(fullFileName);
            }
        }
        else
        {
            for(int i=0; i<skfileInfo.skfiles.size();++i)
            {
                const DFTBTestSuite::SKFileEntry& skfileEntry = skfileInfo.skfiles[i];
                const auto& pair = skfileEntry.name;

                if (skfileInfo.selectedShells.contains(pair.first)
                        && skfileInfo.selectedShells.contains(pair.second))
                {
                    int nshell1 = skfileInfo.selectedShells[pair.first].size();
                    int nshell2 = skfileInfo.selectedShells[pair.second].size();
                    Q_ASSERT(skfileEntry.filepaths.size() == (nshell1*nshell2));
                    for(int j=0; j< skfileEntry.filepaths.size();++j)
                    {
                        QFileInfo info(skfileEntry.filepaths[j]);
                        if (!info.exists() || !info.isReadable())
                        {
                            throw std::runtime_error(info.absoluteFilePath().toStdString() + " is not found or readable.");
                        }
                    }
                    res.append(skfileEntry.filepaths);
                }
            }
        }

        return res;
    }


    void Optimizer::checkSKFileRequirements()
    {
        m_output_stream << EQUALLINE << endl << endl ;
        m_output_stream << centerString("Input Files Analysis") << endl << endl;

        auto skpairs = inputData->tester.input.required_rep_pairs(inputData->tester.reference);
        m_required_rep_skpairs += skpairs;

        QStringList all_rep_elements;

        m_output_stream << "Required repulsive Slater-Koster parameterization pairs:" << endl;
        for(auto const& item : m_required_rep_skpairs)
        {
            auto poten = ADPT::PotentialName::fromString(item);
            m_output_stream << INDENT_SPACES << poten.toString() << endl;
            all_rep_elements.append(poten.getElement1());
            all_rep_elements.append(poten.getElement2());
        }
        m_output_stream << endl;


        if (! inputData->external_rep_skfiles.empty())
        {
            m_output_stream << "External repulsive Slater-Koster files: " << endl;
            QMapIterator<QString, QString>  it(inputData->external_rep_skfiles);
            while(it.hasNext())
            {
                it.next();
                auto pair = it.key();
                QString path  = it.value();
                m_output_stream << QString("%1 = %2").arg(pair).arg(path) << endl;
            }
        }
        m_output_stream << endl;

        auto need_repulsive_psovector = !((m_required_rep_skpairs - inputData->external_rep_skfiles.keys().toSet()).empty());

        if (need_repulsive_psovector)
        {
            //! TODO
            loadRepulsivePSOVector();
        }




//        if (! missing_rep_potentials.empty())
//        {
//            m_output_stream << "The following repulsive potentials are missing: " << endl;
//            QListIterator<QString>  it(missing_rep_potentials);
//            while(it.hasNext())
//            {
//                m_output_stream << INDENT_SPACES << it.next() << endl;
//            }

//            m_output_stream << "ERROR: Missing repulsive part" << endl;
//            throw std::runtime_error("Missing repulsive part");
//        }

//        m_output_stream << endl;


        m_output_stream << HYPHENLINE << endl;


        //Electronic part

        //check required elec pairs from tester
        skpairs = inputData->tester.input.required_elec_pairs(inputData->tester.reference);
        m_required_elec_skpairs += skpairs;

        {
            QSetIterator<QString> it(m_required_elec_skpairs);
            m_output_stream << "Required electronic Slater-Koster parameterization pairs:" << endl;
            while(it.hasNext())
            {
                auto poten = ADPT::PotentialName(it.next());
                m_output_stream << INDENT_SPACES << poten.toString() << endl;
            }
            m_output_stream  << endl;
        }



        QSet<QString> external_elec_skfiles;
        if (inputData->external_elec_skfileInfo)
        {
            for(auto const & pair : m_required_elec_skpairs)
            {
                auto skfiles = findSKFile(inputData->external_elec_skfileInfo, pair);
                if (!skfiles.empty())
                {
                    external_elec_skfiles.insert(pair);
                }
            }
        }

        m_opt_elec_skpairs = m_required_elec_skpairs - external_elec_skfiles ;
        // m_opt_elec_skpairs contains pairs either should be generated or optimized.

        //check external elec sk files

        m_output_stream << "External electronic Slater-Koster files..." << endl;

        if (!inputData->external_elec_skpairs.empty())
        {
            QMapIterator<QString, QList<QString> > it(inputData->external_elec_skpairs);
            while(it.hasNext())
            {
                it.next();
                auto pair = it.key();
                if (m_required_elec_skpairs.contains(pair))
                {
                    external_elec_skfiles.insert(pair);
                    QStringList files(it.value());

                    if (files.size() == 1)
                    {
                        m_output_stream << INDENT_SPACES << QString("%1 = %2").arg(pair).arg(files.first()) << endl;
                    }
                    else
                    {
                        m_output_stream << INDENT_SPACES << QString("%1 = {").arg(pair) << endl;
                        for(int i=0; i<files.size();++i)
                        {
                            os << INDENT_SPACES << INDENT_SPACES << files.at(i) << endl;
                        }
                        m_output_stream << INDENT_SPACES << "}" << endl;
                    }
                }
            }
        }
        m_output_stream << endl;


        QSetIterator<QString> it(m_opt_elec_skpairs);
        while(it.hasNext())
        {
            auto poten = ADPT::PotentialName(it.next());
            m_required_confinings.append(poten.getElement1());
            m_required_confinings.append(poten.getElement2());
        }

        m_required_confinings.removeDuplicates();
        m_required_confinings.sort();

        if (!m_required_confinings.empty())
        {
            QListIterator<QString> it(m_required_confinings);
            m_output_stream << "Required Confining Potentials: " << endl;
            while(it.hasNext())
            {
                m_output_stream << INDENT_SPACES << it.next() << endl;
            }
            m_output_stream << endl;
            inputData->skbuilder_input->desired_pairs = m_opt_elec_skpairs.toList();


            loadSKBuilderInput();
            loadElectronicPSOVector();
        }











//        //Check which rep to opt
//        QList<QString> opt_rep_potentials =
//        inputData->psoVector->repulsivePart->erepfitPSOVector->repulsivePotentials.keys();
//        m_output_stream << "The following repulsive potentials will be optimized: " << endl;
//        {
//            QListIterator<QString>  it(opt_rep_potentials);
//            while(it.hasNext())
//            {
//                QString poten = it.next();
//                if (external_rep_potentials.contains(poten))
//                {
//                    inputData->psoVector->repulsivePart->erepfitPSOVector->repulsivePotentials.remove(poten);
//                }
//                else
//                {
//                    m_output_stream << INDENT_SPACES << poten << endl;
//                }
//            }
//        }
//        m_output_stream << endl;


//        QList<QString> missing_rep_potentials =
//                (m_required_rep_skpairs - inputData->external_rep_skpairs.keys().toSet() - opt_rep_potentials.toSet()).toList();

//        if (!missing_rep_potentials.isEmpty())
//        {
//            m_output_stream << "The following repulsive potentials are missing: " << endl;
//            QListIterator<QString>  it(missing_rep_potentials);
//            while(it.hasNext())
//            {
//                m_output_stream << INDENT_SPACES << it.next() << endl;
//            }

//            m_output_stream << "ERROR: Missing repulsive part" << endl;
//            throw std::runtime_error("Missing repulsive part");
//        }

//        m_output_stream << endl;


    }

    void Optimizer::loadSKBuilderInput()
    {
        try
        {
            //after varifying, we can load the skbuilder input
            QString skbuilder_inputfile = var_input["skbuilder_input_file"];


            m_output_stream << "Loading SKBuilder input file: " <<  skbuilder_inputfile << " ... " ;


            libvariant::Variant var_skbuilder;
            if (m_input_format == InputFormat::YAML)
            {
                var_skbuilder = libvariant::DeserializeYAMLFile(skbuilder_inputfile.toStdString().c_str());
            }
            else
            {
                var_skbuilder = libvariant::DeserializeJSONFile(skbuilder_inputfile.toStdString().c_str());
            }


            QFile schema_file(":/schema/skbuilder_input_schema_v2.json");
            libvariant::Variant schema = readSchemaFile(schema_file);

            libvariant::AdvSchemaLoader loader;
            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var_skbuilder, &loader);

            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                throw SchemaError();
            }

            var_input["skbuilder_input_data"] = var_skbuilder;

            GET_VARIANT_PTR(inputData->skbuilder_input, var_input, "skbuilder_input_data", SKBuilder::Input::SKBuilderInput );

            m_output_stream << "done." << endl;
        }
        catch(SchemaError& e)
        {
            throw std::runtime_error("Invalid format of SKBuilder Input.");
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Invalid format of SKBuilder Input.");
        }
    }

    void Optimizer::loadElectronicPSOVector()
    {
        if (!var_input["pso_vector"].Contains("electronic_part"))
        {
            throw std::runtime_error("no electronic PSOVector defined.");
        }

        m_output_stream << "Processing electronic pso_vector ... ";
        try
        {
            QFile psovector_schema_file(":/schema/pso_vector_electronic.json");
            libvariant::Variant psovector_schema = readSchemaFile(psovector_schema_file);


            libvariant::AdvSchemaLoader loader;

            {
                libvariant::Variant schema_confining;
                QFile schema_file;
                if (inputData->skbuilder_input->toolchain_info.name.toLower() == "nctu")
                {
                    schema_file.setFileName(":/schema/nctu/confining_range.json");
                }
                else if (inputData->skbuilder_input->toolchain_info.name.toLower() == "bccms")
                {
                    schema_file.setFileName(":/schema/bccms/confining_range.json");
                }
                else
                {
                    //should not reach here by schema
                }
                schema_confining = readSchemaFile(schema_file);
                loader.AddSchema("confining_range", schema_confining);
            }


            libvariant::SchemaResult result = libvariant::SchemaValidate(
                        psovector_schema, var_input["pso_vector"]["electronic_part"], &loader);

            if (result.Error())
            {
                m_output_stream << "failed." << endl;
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                throw SchemaError();
            }
            m_output_stream << "done." << endl;

            GET_VARIANT_PTR(inputData->psoVector.electronicPart, var_input["pso_vector"], "electronic_part", DFTBPSO::Input::ElectronicPart);
        }
        catch(SchemaError& e)
        {
            throw std::runtime_error("Invalid format of electronic_part of PSOVector.");
        }
        catch(...)
        {
            m_output_stream << " failed." << endl;
            throw std::runtime_error("Invalid format of electronic_part of PSOVector.");
        }


    }

    void Optimizer::loadPSOVectorInput(Optimizer::InputFormat format)
    {








        //check whether we need skbuilder input or erepfit
        bool requires_skbuilder_input = false;
        bool requires_erepfit_input = false;
        {
            auto required_elec = inputData.tester.input.data->required_elec_pairs(
                        *inputData.tester.reference.data);

            if (!required_elec.isEmpty())
            {
                requires_skbuilder_input = true;
            }


            auto required_rep = inputData.tester.input.data->required_rep_pairs(
                        *inputData.tester.reference.data);

            if (!required_rep.isEmpty())
            {
                requires_erepfit_input = true;
            }
        }

        // if we need to run skbuilder, then read the input
        if (requires_skbuilder_input)
        {

        }

        // if we need to run erepfit, then read the input
        if (requires_erepfit_input)
        {
            QFile erepfit_schema_file(":/schema/erepfit_input_schema_v1.json");
            libvariant::Variant erepfit_schema = readSchemaFile(erepfit_schema_file);
            loader.AddSchema("erepfit_input_schema.json", erepfit_schema);
        }



}
    }

