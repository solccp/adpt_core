#include "erepfitpsovector.h"
#include "adpt/input.h"
#include <QDebug>

#include "pso1dvector.h"


#include "ErepfitKnotsPositionVector.h"
#include "AggressiveErepfitKnotsPositionVector.h"
#include "utils.h"

namespace DFTBPSO
{


ErepFitPSOVector::ErepFitPSOVector()
{
}

void ErepFitPSOVector::define(const DFTBPSO::Input::ErepfitPSOVector *inputData)
{
    m_inputData = *inputData;

    //potentials
    QMapIterator<QString, DFTBPSO::Input::RepulsivePotentials> it(m_inputData.repulsivePotentials);

    while(it.hasNext())
    {
        it.next();       

        QString potential_name = it.key();
        auto pair = potential2ElementPair(potential_name);
        const DFTBPSO::Input::RepulsivePotentials& potential = it.value();

        m_pairSymbols.append(pair);
        QPair<double, double> range = potential.m_PotentialRanges;


        bool aggr_init = potential.m_aggressive_initialization;

        std::shared_ptr<PSO1DVector> vec = nullptr;
        if (aggr_init)
        {
            qDebug() << "AggressiveInitialization: " << potential_name;
            vec = std::make_shared<AggressiveErepfitKnotsPositionVector>(potential.m_PotentialKnots, range.first, range.second);
        }
        else
        {
            vec = std::make_shared<ErepfitKnotsPositionVector>(potential.m_PotentialKnots, range.first, range.second);
        }
        m_potentialsVector.append(vec);
    }

    //additional equations
    int ae_size = m_inputData.additionalEquations.size();
    for(int i=0; i<ae_size; ++i)
    {
        const QPair<double, double> &range = m_inputData.additionalEquations[i].range;
        auto vec = std::make_shared<PSO1DVector>(1, range.first, range.second);
        m_additionalEquationVector.append(vec);
    }
}


std::shared_ptr<PSOVector> ErepFitPSOVector::clone()
{
    auto lhs = std::make_shared<ErepFitPSOVector>();
    lhs->m_pairSymbols = this->m_pairSymbols;
    lhs->m_inputData = this->m_inputData;
    for(int i=0; i<this->m_potentialsVector.size(); ++i)
    {
        lhs->m_potentialsVector.append(std::dynamic_pointer_cast<PSO1DVector>(this->m_potentialsVector[i]->clone()));
    }
    for(int i=0; i<this->m_additionalEquationVector.size(); ++i)
    {
        lhs->m_additionalEquationVector.append(this->m_additionalEquationVector[i]->clone());
    }
    return lhs;
}

int ErepFitPSOVector::size() const
{
    int size = 0;
    for(int i=0; i<this->m_potentialsVector.size(); ++i)
    {
        size += this->m_potentialsVector[i]->size();
    }
    for(int i=0; i<this->m_additionalEquationVector.size(); ++i)
    {
        size += this->m_additionalEquationVector[i]->size();
    }

    return size;
}

Vector<double> ErepFitPSOVector::encode() const
{
    Vector<double> res;
    for(int i=0; i<m_potentialsVector.size(); ++i)
    {
        Vector<double> tmp = m_potentialsVector[i]->encode();
        for(int j=0; j<tmp.size();++j)
        {
            res.append(tmp[j]);
        }
    }

    for(int i=0; i<m_additionalEquationVector.size();++i)
    {
        res.append(m_additionalEquationVector[i]->encode()[0]);
    }

    return res;
}

void ErepFitPSOVector::decode(const Vector<double> &vector)
{
    Q_ASSERT_X(vector.size() == this->size(), "ErepFitPSOVector::decode", "size inconsistent");
    int index= 0;
    for(int i=0; i<this->m_potentialsVector.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_potentialsVector[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_potentialsVector[i]->decode(vec);
        index += (m_potentialsVector[i]->size());
    }
    for(int i=0; i<m_additionalEquationVector.size();++i)
    {
        Vector<double> vec;
        vec.append(vector[index]);
        m_additionalEquationVector[i]->decode(vec);
        index+=1;
    }

}


void ErepFitPSOVector::copyfrom(const PSOVector *rhs)
{
    const ErepFitPSOVector *rhs_l = dynamic_cast<const ErepFitPSOVector*>(rhs);

    Q_ASSERT_X( this->size() == rhs_l->size(), "ErepFitPSOVector::copyfrom", "size inconsistent") ;

    for(int i=0; i<this->m_potentialsVector.size(); ++i)
    {
        this->m_potentialsVector[i]->copyfrom(rhs_l->m_potentialsVector[i].get());
        this->m_potentialsVector[i]->copyControllerFrom(rhs_l->m_potentialsVector[i].get());
    }
    for(int i=0; i<this->m_additionalEquationVector.size();++i)
    {
        this->m_additionalEquationVector[i]->copyfrom(rhs_l->m_additionalEquationVector[i].get());
        this->m_additionalEquationVector[i]->copyControllerFrom(rhs_l->m_additionalEquationVector[i].get());
    }

    this->repulsivePotentials = rhs_l->repulsivePotentials;
}

QList<QPair<QString, QString> > ErepFitPSOVector::getPairSymbols() const
{
    return m_pairSymbols;
}

QList<std::shared_ptr<PSO1DVector>> ErepFitPSOVector::getPotentials() const
{
    return m_potentialsVector;
}

QList<std::shared_ptr<PSOVector>> ErepFitPSOVector::getAdditionalEquations() const
{
    return m_additionalEquationVector;
}

const QList<DFTBPSO::Input::ErepfitAdditionalEquation> &ErepFitPSOVector::getAdditionalEquationInfo() const
{
    return m_inputData.additionalEquations;
}
ErepFitPSOVector::~ErepFitPSOVector()
{
//    for(int i=0; i<m_potentialsVector.size();++i)
//    {
//        delete m_potentialsVector[i];
//    }
   m_potentialsVector.clear();
//   for(int i=0; i<m_additionalEquationVector.size();++i)
//   {
//       delete m_additionalEquationVector[i];
//   }
   m_additionalEquationVector.clear();
}

void ErepFitPSOVector::spread(double factor)
{
    for(auto &vec : this->m_potentialsVector)
    {
        vec->spread(factor);
    }
    for(auto &vec : this->m_additionalEquationVector)
    {
        vec->spread(factor);
    }
}

ErepFitPSOVector &ErepFitPSOVector::operator =(const ErepFitPSOVector &rhs)
{
    if (this != &rhs)
    {
        this->copyfrom(&rhs);
    }
    return *this;
}


void ErepFitPSOVector::randomizePosition()
{
    for(int i=0; i< m_potentialsVector.size(); ++i)
    {
        m_potentialsVector[i]->randomizePosition();
    }
    for(int i=0; i< m_additionalEquationVector.size();++i)
    {
        m_additionalEquationVector[i]->randomizePosition();
    }
}

void ErepFitPSOVector::randomizeVelocity()
{
    for(int i=0; i< m_potentialsVector.size(); ++i)
    {
        m_potentialsVector[i]->randomizeVelocity();
    }
    for(int i=0; i< m_additionalEquationVector.size();++i)
    {
        m_additionalEquationVector[i]->randomizeVelocity();
    }
}
void ErepFitPSOVector::stablizeVelocity()
{

    for(int i=0; i< m_potentialsVector.size(); ++i)
    {
        m_potentialsVector[i]->stablizeVelocity();
    }
    for(int i=0; i< m_additionalEquationVector.size(); ++i)
    {
        m_additionalEquationVector[i]->stablizeVelocity();
    }

}

void ErepFitPSOVector::stablizePosition(PSOVector *vel)
{
    ErepFitPSOVector *vel_l = dynamic_cast<ErepFitPSOVector*>(vel);

    Q_ASSERT_X(vel_l != nullptr, "ErepFitPSOVector::stablizePosition", "TypeError");

    for(int i=0; i<m_potentialsVector.size(); ++i)
    {
        m_potentialsVector[i]->stablizePosition(vel_l->m_potentialsVector[i].get());
    }

    for(int i=0; i<m_additionalEquationVector.size(); ++i)
    {
        m_additionalEquationVector[i]->stablizePosition(vel_l->m_additionalEquationVector[i].get());
    }
}

void ErepFitPSOVector::copyControllerFrom(const PSOVector *rhs)
{
    const ErepFitPSOVector *vector = dynamic_cast<const ErepFitPSOVector*>(rhs);
    Q_ASSERT_X(vector != nullptr, "ErepFitPSOVector::copyControllerFrom", "TypeError");

    for(int i=0; i<m_potentialsVector.size(); ++i)
    {
        m_potentialsVector[i]->copyControllerFrom(vector->m_potentialsVector[i].get());
    }

    for(int i=0; i<m_additionalEquationVector.size(); ++i)
    {
        m_additionalEquationVector[i]->copyControllerFrom(vector->m_additionalEquationVector[i].get());
    }

}


double ErepFitPSOVector::getLowerBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<m_potentialsVector.size(); ++i)
    {
        if (index >= curIndex && index < (curIndex+m_potentialsVector[i]->size()))
        {
            return m_potentialsVector[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_potentialsVector[i]->size();
    }

    for(int i=0; i<m_additionalEquationVector.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_additionalEquationVector[i]->size()))
        {
            return m_additionalEquationVector[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_additionalEquationVector[i]->size();
    }

    throw std::invalid_argument("ErepFitPSOVector::getLowerBound");
}

double ErepFitPSOVector::getUpperBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<m_potentialsVector.size(); ++i)
    {
        if (index >= curIndex && index < (curIndex+m_potentialsVector[i]->size()))
        {
            return m_potentialsVector[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_potentialsVector[i]->size();
    }

    for(int i=0; i<m_additionalEquationVector.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_additionalEquationVector[i]->size()))
        {
            return m_additionalEquationVector[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_additionalEquationVector[i]->size();
    }

    throw std::invalid_argument("ErepFitPSOVector::getUpperBound");
}

}










