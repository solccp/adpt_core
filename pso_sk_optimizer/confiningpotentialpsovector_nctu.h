#ifndef CONFININGPOTENTIALPSOVECTOR_NCTU_H
#define CONFININGPOTENTIALPSOVECTOR_NCTU_H


#include "confiningpotentialpsovector.h"

#include "pso1dvector.h"

#include <QString>
#include <QStringList>

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

namespace DFTBPSO
{

    using namespace libPSO;
    class ConfiningPotentialPSOVector_NCTU : public ConfiningPotentialPSOVector
    {
        friend class DFTBPSOVector;
        friend class libvariant::VariantConvertor<DFTBPSO::ConfiningPotentialPSOVector_NCTU>;
    public:

        ConfiningPotentialPSOVector_NCTU();
        ~ConfiningPotentialPSOVector_NCTU();

        // PSOVector interface
    public:
        std::shared_ptr<PSOVector> clone() override;
        int size() const override;
        Vector<double> encode() const override;
        void decode(const Vector<double> &vector) override;
        void spread(double factor) override;
        void copyfrom(const PSOVector *rhs) override;
    public:
        void set_W_Range(double min, double max);
        void set_A_Range(double min, double max);
        void set_R_Range(double min, double max);
    private:
        std::shared_ptr<PSO1DVector> w = nullptr;
        std::shared_ptr<PSO1DVector> a = nullptr;
        std::shared_ptr<PSO1DVector> r = nullptr;

        // PSOVector interface
    public:
        void randomizePosition() override;
        void randomizeVelocity() override;
        void stablizeVelocity() override;
        void stablizePosition(PSOVector *vel) override;
        void copyControllerFrom(const PSOVector *rhs) override;

        // PSOVector interface
    public:
        double getLowerBound(int index) override;
        double getUpperBound(int index) override;

        // ConfiningPotentialPSOVector interface
    public:
        virtual void fromVariant(const libvariant::Variant &var);
        virtual libvariant::Variant toVariant() const;
    };
}
#endif // TWNCONFININGPOTENTIALPSOVECTOR_H
