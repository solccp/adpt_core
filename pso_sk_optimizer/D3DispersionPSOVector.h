#ifndef D3DISPERSIONPSOVECTOR_H
#define D3DISPERSIONPSOVECTOR_H

#include "psovector.h"
#include "pso1dvector.h"

#include <QList>
#include <QHash>
#include <QVariant>
#include <QString>

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

namespace DFTBPSO
{
    using namespace libPSO;

    class D3DispersionPSOVector : public PSOVector
    {
        friend class libvariant::VariantConvertor<D3DispersionPSOVector>;
    public:
        D3DispersionPSOVector(const QString &damping="zero");
        D3DispersionPSOVector(const D3DispersionPSOVector& rhs) = delete;
        ~D3DispersionPSOVector();

        QString getDamping() const;

        double getS6() const;
        double getS8() const;
        double getA1() const;
        double getA2() const;
        double getSr6() const;
        double getAlpha6() const;

        void setDamping(const QString & value);
        void setS6(double min, double max);
        void setS8(double min, double max);
        void setA1(double min, double max);
        void setA2(double min, double max);
        void setSr6(double min, double max);
        void setAlpha6(double min, double max);

        // PSOVector interface
    public:
        std::shared_ptr<PSOVector> clone() override;
        int size() const;
        Vector<double> encode() const;
        void decode(const Vector<double> &vector);
        void spread(double factor);
        double getLowerBound(int index);
        double getUpperBound(int index);
        void randomizePosition();
        void randomizeVelocity();
        void stablizeVelocity();
        void stablizePosition(PSOVector *vel);
        void copyControllerFrom(const PSOVector *rhs);


    private:
        QString m_damping = "zero";
        std::shared_ptr<PSO1DVector> m_s6;
        std::shared_ptr<PSO1DVector> m_s8;

        std::shared_ptr<PSO1DVector> m_a1;
        std::shared_ptr<PSO1DVector> m_a2;

        std::shared_ptr<PSO1DVector> m_sr6;
        std::shared_ptr<PSO1DVector> m_alpha6;


        // PSOVector interface
    public:
        void copyfrom(const PSOVector *rhs);
    };

}


#endif // D3DISPERSIONPSOVECTOR_H
