#ifndef SOPSO_FUNCTIONS_H
#define SOPSO_FUNCTIONS_H

#include "PSOFitnessValue.h"

#include <memory>

namespace libPSO
{
    class PSOAlgorithm;
    class PSOVector;
}

namespace DFTBPSO
{
    void sopso_progressUpdater(const std::shared_ptr<const libPSO::PSOAlgorithm>& pso, int iter);
    libPSO::PSOFitnessValue sopso_particle_evaluation(const std::shared_ptr<libPSO::PSOVector> &vector);
}

#endif // SOPSO_FUNCTIONS_H
