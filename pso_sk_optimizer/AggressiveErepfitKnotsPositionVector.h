#ifndef AGGRESSIVEEREPFITKNOTSPOSITIONVECTOR_H
#define AGGRESSIVEEREPFITKNOTSPOSITIONVECTOR_H

#include "pso1dvector.h"

#include <QVector>

class AggressiveErepfitKnotsPositionVector : public libPSO::PSO1DVector
{
    friend class PSO1DVectorRangeController;
public:
    AggressiveErepfitKnotsPositionVector(int size, double min, double max);
    AggressiveErepfitKnotsPositionVector(const AggressiveErepfitKnotsPositionVector& rhs);
protected:


protected:
    void ensureSeparation();
    void sortKnots(AggressiveErepfitKnotsPositionVector *pos, AggressiveErepfitKnotsPositionVector *vel);
public:
    virtual void randomizePosition();
    virtual void stablizePosition(libPSO::PSOVector *vel);

    // PSO1DVector interface
public:
    void adjustVelocityRanges();

    // PSOVector interface
public:
    double getLowerBound(int index);
    double getUpperBound(int index);

    // PSOVector interface
public:
    std::shared_ptr<libPSO::PSOVector> clone() override;
    void decode(const libPSO::Vector<double> &vector);
};

#endif // AGGRESSIVEEREPFITKNOTSPOSITIONVECTOR_H
