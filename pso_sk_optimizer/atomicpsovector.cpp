#include "atomicpsovector.h"
#include "confiningpotentialactionpsovector.h"
#include "confiningpotentialpsovector_nctu.h"
#include "confiningpotentialpsovector_bccms.h"

#include "skbuilder/input.h"
#include "adpt/input.h"

namespace DFTBPSO {

AtomicPSOVector::AtomicPSOVector(const QString& element_)
{
    m_element = element_;
    qDebug() << "AtomicPSOVector::ctor" << this;
}

AtomicPSOVector::~AtomicPSOVector()
{
    m_confiningActions.clear();
    m_orbitalEnergyVectors.clear();
    m_hubbardVectors.clear();
    m_hubbardDerivsVectors.clear();
    qDebug() << "AtomicPSOVector::dtor" << this;
}


bool AtomicPSOVector::define(const DFTBPSO::Input::AtomicDefinition &input,
                             const SKBuilder::Input::SKBuilderInput& skbuilder_input)
{
    if (!input.confining_actions.empty())
    {
        m_confiningActions.clear();
    }

    //! TODO TEST
    for(auto const & action : input.confining_actions )
    {
        auto action_vec = std::make_shared<ConfiningPotentialActionPSOVector>();
        for(auto const & item : action.confinings)
        {
            std::shared_ptr<ConfiningPotentialPSOVector> conf_vec;
            if (skbuilder_input.toolchain_info.name.toLower() == "nctu")
            {
                auto ranges = std::dynamic_pointer_cast<Input::ConfiningParamaterRange_NCTU>(item.ranges);

                if (!ranges)
                {
                    throw std::runtime_error("AtomicPSOVector definition error");
                }


                auto conf_vec_nctu = std::make_shared<ConfiningPotentialPSOVector_NCTU>();

                conf_vec_nctu->set_W_Range(ranges->w.first, ranges->w.second);
                conf_vec_nctu->set_A_Range(ranges->a.first, ranges->a.second);
                conf_vec_nctu->set_R_Range(ranges->r.first, ranges->r.second);

                conf_vec = conf_vec_nctu;
            }
            else if (skbuilder_input.toolchain_info.name.toLower() == "bccms")
            {
                auto ranges = std::dynamic_pointer_cast<Input::ConfiningParamaterRange_BCCMS>(item.ranges);

                if (!ranges)
                {
                    throw std::runtime_error("AtomicPSOVector definition error");
                }


                auto conf_vec_bccms = std::make_shared<ConfiningPotentialPSOVector_BCCMS>();

                conf_vec_bccms->set_N_Range(ranges->n.first, ranges->n.second);
                conf_vec_bccms->set_R_Range(ranges->r.first, ranges->r.second);

                conf_vec = conf_vec_bccms;
            }
            else
            {

            }
            conf_vec->setOrbitalTypes(item.orbitalTypes);
            action_vec->m_confiningVectors.append(conf_vec);
        }
        action_vec->setTake(action.take);
        m_confiningActions.append(action_vec);
    }


    //Energy section
    m_orbitalEnergyVectors.clear();
    m_orbitalEnergyDefinitions.clear();
    m_orbitalEnergyReferredOrbital.clear();
    m_orbitalEnergyReferredOrbitalEnergy.clear();

    for(int i=0; i<input.orbitalEnergyDefinitions.size();++i)
    {
        const DFTBPSO::Input::OrbitalEnergyDefinition& def = input.orbitalEnergyDefinitions.at(i);
        if (!skbuilder_input.atomicinfo.contains(this->m_element))
        {
            throw std::runtime_error("SkBuilder_Input/AtomicInfo does not contain element " + m_element.toStdString());
        }
        bool orbitalFound = false;

        for(int j=0; j<skbuilder_input.atomicinfo[m_element].orbitals.size();++j)
        {
            if (skbuilder_input.atomicinfo[m_element].orbitals[j].contains(def.orbital))
            {
                orbitalFound = true;
                break;
            }
        }
        if (!orbitalFound)
        {
            throw std::runtime_error("SkBuilder_Input/" + m_element.toStdString() + "/Orbitals does not contain orbital " + def.orbital.toStdString());
        }

        bool optimizingReferredOrbitalFound = false;
        for(int j=0; j<input.orbitalEnergyDefinitions.size();++j)
        {
            if (j==i)
                continue;
            if (def.referredOrbital == input.orbitalEnergyDefinitions[j].orbital)
            {
                optimizingReferredOrbitalFound =true;
                break;
            }
        }



        double referredEnergy = 0.0;
        bool referredOrbitalFound = false;


        if (!optimizingReferredOrbitalFound)
        {

            for(int j=0; j<skbuilder_input.atomicinfo[m_element].orbitals.size();++j)
            {
                if (skbuilder_input.atomicinfo[m_element].orbitals[j].contains(def.referredOrbital))
                {
                    referredEnergy = skbuilder_input.atomicinfo[m_element].orbitalEnergy[def.referredOrbital];
                    referredOrbitalFound = true;
                    break;
                }
            }
            if (def.referredOrbital.toLower() == "none")
            {
                referredOrbitalFound = true;
            }
        }

        if (!referredOrbitalFound && !optimizingReferredOrbitalFound)
        {
            QString msg = "SkBuilder_Input/" + m_element + "/Orbitals does not contain orbital " + def.referredOrbital + "\nOr it is not being optimized.";
            throw std::runtime_error(msg.toStdString());
        }

        if (referredOrbitalFound)
        {
            //referred orbital is not being optimized, use orbitalEnergyReferredOrbitalEnergy
            m_orbitalEnergyReferredOrbital.append("none");
        }
        else if (optimizingReferredOrbitalFound)
        {
            //referred orbital is not being optimized
            m_orbitalEnergyReferredOrbital.append(def.referredOrbital);
        }
        m_orbitalEnergyVectors.append(std::make_shared<PSO1DVector>(1, def.range.first, def.range.second));
        m_orbitalEnergyReferredOrbitalEnergy.append(referredEnergy);
        m_orbitalEnergyDefinitions.append(def);
    }


    m_hubbardVectors.clear();
    m_hubbardDefinitions.clear();
    for(int i=0; i<input.hubbardDefinitions.size();++i)
    {
        const DFTBPSO::Input::HubbardDefinition& def = input.hubbardDefinitions.at(i);
        if (!skbuilder_input.atomicinfo.contains(this->m_element))
        {
            throw std::runtime_error("Hubbard Vector Error! SkBuilder_Input/AtomicInfo does not contain element " + m_element.toStdString());
        }
        bool orbitalFound = false;

        for(int j=0; j<skbuilder_input.atomicinfo[m_element].orbitals.size();++j)
        {
            if (skbuilder_input.atomicinfo[m_element].orbitals[j].contains(def.orbital))
            {
                orbitalFound = true;
                break;
            }
        }
        if (!orbitalFound)
        {
            throw std::runtime_error("SkBuilder_Input/" + m_element.toStdString() + "/Orbitals does not contain orbital " + def.orbital.toStdString());
        }

        m_hubbardVectors.append(std::make_shared<PSO1DVector>(1, def.range.first, def.range.second));
        m_hubbardDefinitions.append(def);
    }

    m_hubbardDerivsVectors.clear();
    m_hubbardDerivsDefinitions.clear();
    for(int i=0; i<input.hubbardDerivativeDefinitions.size();++i)
    {
        const DFTBPSO::Input::HubbardDefinition& def = input.hubbardDerivativeDefinitions.at(i);
        if (!skbuilder_input.atomicinfo.contains(this->m_element))
        {
            throw std::runtime_error("Hubbard Vector Error! SkBuilder_Input/AtomicInfo does not contain element " + m_element.toStdString());
        }
        bool orbitalFound = false;

        for(int j=0; j<skbuilder_input.atomicinfo[m_element].orbitals.size();++j)
        {
            if (skbuilder_input.atomicinfo[m_element].orbitals[j].contains(def.orbital))
            {
                orbitalFound = true;
                break;
            }
        }
        if (!orbitalFound)
        {
            throw std::runtime_error("SkBuilder_Input/" + m_element.toStdString() + "/Orbitals does not contain orbital " + def.orbital.toStdString());
        }

        m_hubbardDerivsVectors.append(std::make_shared<PSO1DVector>(1, def.range.first, def.range.second));
        m_hubbardDerivsDefinitions.append(def);

    }
    return true;
}

double AtomicPSOVector::getHubbardDerivs() const
{
    if (!m_hubbardDerivsVectors.isEmpty())
    {
        return m_hubbardDerivsVectors.first()->at(0);
    }
    throw std::runtime_error("No HubbardDerivs defined.");
}

bool AtomicPSOVector::hasHubbardDerivs() const
{
    return !m_hubbardDerivsVectors.isEmpty();
}
QString AtomicPSOVector::element() const
{
    return m_element;
}

std::shared_ptr<PSOVector> AtomicPSOVector::clone()
{
    auto res = std::make_shared<AtomicPSOVector>(m_element);
    for(int i=0; i<this->m_confiningActions.size();++i)
    {
        auto newvec = std::dynamic_pointer_cast<ConfiningPotentialActionPSOVector>(m_confiningActions[i]->clone());
        res->m_confiningActions.append(newvec);
    }
    for(int i=0; i<this->m_orbitalEnergyVectors.size();++i)
    {
        res->m_orbitalEnergyVectors.append(std::dynamic_pointer_cast<PSO1DVector>(m_orbitalEnergyVectors[i]->clone()));
    }
    res->m_orbitalEnergyDefinitions = this->m_orbitalEnergyDefinitions;
    res->m_orbitalEnergyReferredOrbitalEnergy = this->m_orbitalEnergyReferredOrbitalEnergy;
    res->m_orbitalEnergyReferredOrbital = this->m_orbitalEnergyReferredOrbital;

    for(int i=0; i<this->m_hubbardVectors.size();++i)
    {
        res->m_hubbardVectors.append(std::dynamic_pointer_cast<PSO1DVector>(m_hubbardVectors[i]->clone()));
    }
    for(int i=0; i<this->m_hubbardDerivsVectors.size();++i)
    {
        res->m_hubbardDerivsVectors.append(std::dynamic_pointer_cast<PSO1DVector>(m_hubbardDerivsVectors[i]->clone()));
    }

    res->m_hubbardDefinitions = this->m_hubbardDefinitions;
    res->m_hubbardDerivsDefinitions = this->m_hubbardDerivsDefinitions;

    return res;
}

int AtomicPSOVector::size() const
{
    int size = 0;
    for(int i=0; i<m_confiningActions.size();++i)
    {
        size += m_confiningActions[i]->size();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        size += m_orbitalEnergyVectors[i]->size();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        size += m_hubbardVectors[i]->size();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        size += m_hubbardDerivsVectors[i]->size();
    }

    return size;
}

Vector<double> AtomicPSOVector::encode() const
{
    Vector<double> res;
    for(int i=0; i<m_confiningActions.size();++i)
    {
        Vector<double> temp = m_confiningActions[i]->encode();
        res.append(temp);
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        Vector<double> temp = m_orbitalEnergyVectors[i]->encode();
        res.append(temp);
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        Vector<double> temp = m_hubbardVectors[i]->encode();
        res.append(temp);
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        Vector<double> temp = m_hubbardDerivsVectors[i]->encode();
        res.append(temp);
    }
    return res;
}

void AtomicPSOVector::decode(const Vector<double> &vector)
{
    Q_ASSERT_X(vector.size() == this->size(), "AtomicPSOVector::decode", "size inconsistent");
    int index= 0;
    for(int i=0; i<this->m_confiningActions.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_confiningActions[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_confiningActions[i]->decode(vec);
        index += (m_confiningActions[i]->size());
    }
    for(int i=0; i<this->m_orbitalEnergyVectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_orbitalEnergyVectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_orbitalEnergyVectors[i]->decode(vec);
        index += (m_orbitalEnergyVectors[i]->size());
    }
    for(int i=0; i<this->m_hubbardVectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_hubbardVectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_hubbardVectors[i]->decode(vec);
        index += (m_hubbardVectors[i]->size());
    }
    for(int i=0; i<this->m_hubbardDerivsVectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_hubbardDerivsVectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_hubbardDerivsVectors[i]->decode(vec);
        index += (m_hubbardDerivsVectors[i]->size());
    }
}

void AtomicPSOVector::spread(double factor)
{
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->spread(factor);
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->spread(factor);
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->spread(factor);
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->spread(factor);
    }
}

void AtomicPSOVector::copyfrom(const PSOVector *rhs)
{
    PSOVector::copyfrom(rhs);
    const AtomicPSOVector* real_pos = dynamic_cast<const AtomicPSOVector*> (rhs);
    if (real_pos)
    {
        Q_ASSERT_X(real_pos->m_confiningActions.size() == m_confiningActions.size()
                   , "AtomicPSOVector::copyfrom", "size inconsistent");
        Q_ASSERT_X(real_pos->m_orbitalEnergyVectors.size() == m_orbitalEnergyVectors.size()
                   , "AtomicPSOVector::copyfrom", "size inconsistent");
        Q_ASSERT_X(real_pos->m_hubbardVectors.size() == m_hubbardVectors.size()
                   , "AtomicPSOVector::copyfrom", "size inconsistent");
        Q_ASSERT_X(real_pos->m_hubbardDerivsVectors.size() == m_hubbardDerivsVectors.size()
                   , "AtomicPSOVector::copyfrom", "size inconsistent");
        for(int i=0; i<real_pos->m_confiningActions.size(); ++i)
        {
            this->m_confiningActions[i]->copyfrom(real_pos->m_confiningActions[i].get());
            this->m_confiningActions[i]->copyControllerFrom(real_pos->m_confiningActions[i].get());
        }
        for(int i=0; i<real_pos->m_orbitalEnergyVectors.size(); ++i)
        {
            this->m_orbitalEnergyVectors[i]->copyfrom(real_pos->m_orbitalEnergyVectors[i].get());
            this->m_orbitalEnergyVectors[i]->copyControllerFrom(real_pos->m_orbitalEnergyVectors[i].get());
        }
        for(int i=0; i<real_pos->m_hubbardVectors.size(); ++i)
        {
            this->m_hubbardVectors[i]->copyfrom(real_pos->m_hubbardVectors[i].get());
            this->m_hubbardVectors[i]->copyControllerFrom(real_pos->m_hubbardVectors[i].get());
        }
        for(int i=0; i<real_pos->m_hubbardDerivsVectors.size(); ++i)
        {
            this->m_hubbardDerivsVectors[i]->copyfrom(real_pos->m_hubbardDerivsVectors[i].get());
            this->m_hubbardDerivsVectors[i]->copyControllerFrom(real_pos->m_hubbardDerivsVectors[i].get());
        }
    }
}

void AtomicPSOVector::randomizePosition()
{
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->randomizePosition();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->randomizePosition();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->randomizePosition();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->randomizePosition();
    }
}


void AtomicPSOVector::randomizeVelocity()
{
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->randomizeVelocity();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->randomizeVelocity();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->randomizeVelocity();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->randomizeVelocity();
    }

}

void AtomicPSOVector::stablizeVelocity()
{
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->stablizeVelocity();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->stablizeVelocity();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->stablizeVelocity();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->stablizeVelocity();
    }
}

void AtomicPSOVector::stablizePosition(PSOVector *vel)
{
    const AtomicPSOVector* real_vel = dynamic_cast<const AtomicPSOVector*> (vel);
    Q_ASSERT_X(real_vel != nullptr, "AtomicPSOVector::stablizePosition", "typeError");
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->stablizePosition(real_vel->m_confiningActions[i].get());
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->stablizePosition(real_vel->m_orbitalEnergyVectors[i].get());
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->stablizePosition(real_vel->m_hubbardVectors[i].get());
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->stablizePosition(real_vel->m_hubbardDerivsVectors[i].get());
    }
}

void AtomicPSOVector::copyControllerFrom(const PSOVector *rhs)
{
    const AtomicPSOVector* vector = dynamic_cast<const AtomicPSOVector*> (rhs);
    Q_ASSERT_X(vector != nullptr, "AtomicPSOVector::copyControllerFrom", "typeError");
    for(int i=0; i<m_confiningActions.size();++i)
    {
        m_confiningActions[i]->copyControllerFrom(vector->m_confiningActions[i].get());
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        m_orbitalEnergyVectors[i]->copyControllerFrom(vector->m_orbitalEnergyVectors[i].get());
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        m_hubbardVectors[i]->copyControllerFrom(vector->m_hubbardVectors[i].get());
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        m_hubbardDerivsVectors[i]->copyControllerFrom(vector->m_hubbardDerivsVectors[i].get());
    }

}
double AtomicPSOVector::getLowerBound(int index)
{
    int curIndex = 0;

    for(int i=0; i<m_confiningActions.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_confiningActions[i]->size()))
        {
            return m_confiningActions[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_confiningActions[i]->size();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_orbitalEnergyVectors[i]->size()))
        {
            return m_orbitalEnergyVectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_orbitalEnergyVectors[i]->size();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_hubbardVectors[i]->size()))
        {
            return m_hubbardVectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_hubbardVectors[i]->size();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_hubbardDerivsVectors[i]->size()))
        {
            return m_hubbardDerivsVectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_hubbardDerivsVectors[i]->size();
    }
    throw std::invalid_argument("AtomicPSOVector::getLowerBound");
}

double AtomicPSOVector::getUpperBound(int index)
{
    int curIndex = 0;

    for(int i=0; i<m_confiningActions.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_confiningActions[i]->size()))
        {
            return m_confiningActions[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_confiningActions[i]->size();
    }
    for(int i=0; i<m_orbitalEnergyVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_orbitalEnergyVectors[i]->size()))
        {
            return m_orbitalEnergyVectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_orbitalEnergyVectors[i]->size();
    }
    for(int i=0; i<m_hubbardVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_hubbardVectors[i]->size()))
        {
            return m_hubbardVectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_hubbardVectors[i]->size();
    }
    for(int i=0; i<m_hubbardDerivsVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_hubbardDerivsVectors[i]->size()))
        {
            return m_hubbardDerivsVectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_hubbardDerivsVectors[i]->size();
    }
    throw std::invalid_argument("AtomicPSOVector::getUpperBound");
}

}













