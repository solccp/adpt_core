#ifndef DFTBPSOVECTOR_YAML_H
#define DFTBPSOVECTOR_YAML_H

#include "dftbpsovector.h"
#include <yaml-cpp/yaml.h>
#include "yaml_node.h"
#include "yaml_qttypes.h"
#include "yaml_utils.h"

#include "atomicpsovector.h"
#include "atomicpsovector_yaml.h"

#include "DispersionPSOVector_yaml.h"

namespace YAML
{

    template<>
    struct convert<DFTBPSO::DFTBPSOVector>
    {

        static Node encode(const DFTBPSO::DFTBPSOVector& rhs)
        {
            Node node;

            //Electronic parameters
            {
                Node subnode;
                for(int i=0; i<rhs.m_atomicVectors.size();++i)
                {
                    DFTBPSO::AtomicPSOVector *vec = rhs.m_atomicVectors[i];
                    if (vec)
                    {
                        subnode["AtomicParameters"][vec->element()] = (*vec);
                    }
                }
                if (!rhs.m_atomicVectors.isEmpty())
                {
                    node["Electronic"] = subnode;
                }
            }

            //Repulsive parameters
            if (rhs.erepfitVector())
            {
                Node subnode;
                subnode = *rhs.erepfitVector();
                node["Repulsive"] = subnode;
            }

            //Dispersion parameters
            if (rhs.hasDispersionVector())
            {
                Node subnode;
                subnode = *rhs.dispersionVector();
                node["Dispersion"] = subnode;
            }

            if (rhs.hasDampingFactorVector())
            {
                node["DampingFactor"] =  rhs.dampingFactorVector()->at(0);
            }

            return node;
        }

        static bool decode(const Node& node, DFTBPSO::DFTBPSOVector& rhs)
        {
            if (node["Electronic"].IsDefined())
            {
                if (node["Electronic"]["AtomicParameters"].IsDefined() && node["Electronic"]["AtomicParameters"].IsMap())
                {
                    Node subnode = node["Electronic"]["AtomicParameters"];
                    for(int j=0; j<rhs.m_atomicVectors.size();++j)
                    {
                        QString elem = rhs.m_atomicVectors[j]->element();
                        if (subnode[elem].IsDefined())
                        {
                            qDebug() << "Loading Electronic/AtomicParameters/" + elem;
                            DFTBPSO::AtomicPSOVector& ala =  *rhs.m_atomicVectors[j];
                            Node subsubnode = subnode[elem.toStdString()];
                            update_node(subsubnode, ala);
                        }
                        else
                        {
                            qDebug() << "Electronic/AtomicParameters/" + elem << "is not define in the PSOVector, skipping.";
                        }
                    }
                }
            }
            if (node["Repulsive"].IsDefined())
            {
                ErepFitPSOVector& ala = *rhs.m_erepfitVector;
                Node subnode = node["Repulsive"];
                update_node(subnode, ala);
            }

            if (node["Dispersion"].IsDefined())
            {
                DispersionPSOVector& ala = *rhs.m_dispersionVector;
                Node subnode = node["Dispersion"];
                update_node(subnode, ala);
            }

            if (node["DampingFactor"].IsDefined())
            {
                if (rhs.hasDampingFactorVector())
                {
                    double value = node["DampingFactor"].as<double>();
                    rhs.dampingFactorVector()->operator [](0) = value;
                }

            }

            return true;
        }


    };


}


#endif // DFTBPSOVECTOR_YAML_H
