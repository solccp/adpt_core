#ifndef DISPERSIONPSOVECTOR_YAML_H
#define DISPERSIONPSOVECTOR_YAML_H
#include "DispersionPSOVector.h"

#include "yaml_qttypes.h"
#include "yaml_utils.h"

namespace YAML
{
    using namespace DFTBPSO;
    template<>
    struct convert<DispersionPSOVector>
    {
        static Node encode(const DispersionPSOVector& rhs)
        {
            Node root_node;
            Node node;

            for(int i=0;i<rhs.numberDispersion();++i)
            {
                Node subnode;
                subnode["DistanceRange"] = qMakePair(rhs.m_distance_vectors[i]->getPosMin(),
                                                     rhs.m_distance_vectors[i]->getPosMax());

                subnode["EnergyRange"] = qMakePair(rhs.m_energy_vectors[i]->getPosMin(),
                                                     rhs.m_energy_vectors[i]->getPosMax());

                subnode["Element"] = rhs.getElement(i);
                subnode["Distance"] = rhs.getDistance(i);
                subnode["Energy"] = rhs.getEnergy(i);
                node.push_back(subnode);
            }
            root_node["LJDisperion"] = node;
            return root_node;
        }
        static bool decode(const Node& node, DispersionPSOVector& rhs)
        {
            if (node.IsDefined() && node["LJDispersion"].IsDefined())
            {
                const Node& subnode = node["LJDispersion"];
                if (!subnode.IsSequence())
                {
                    return false;
                }
                rhs.clear();
                for(std::size_t i=0; i<subnode.size();++i)
                {
                    const Node& curNode = subnode[i];

                    QString elem = curNode["Element"].as<QString>();
                    typedef QPair<double, double> pdd;
                    pdd distanceRange = curNode["DistanceRange"].as<pdd>();
                    pdd energyRange = curNode["EnergyRange"].as<pdd>();

                    double distance = curNode["Distance"].as<double>();
                    double energy = curNode["Energy"].as<double>();

                    rhs.addVector(elem, distanceRange, energyRange);
                    {
                        Vector<double> vec;
                        vec.append(distance);
                        rhs.m_distance_vectors[i]->decode(vec);
                    }
                    {
                        Vector<double> vec;
                        vec.append(energy);
                        rhs.m_energy_vectors[i]->decode(vec);
                    }

                }
                return true;
            }
            return false;
        }
    };
}

#endif // DISPERSIONPSOVECTOR_YAML_H
