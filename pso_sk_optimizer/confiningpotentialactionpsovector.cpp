#include "confiningpotentialactionpsovector.h"
#include "confiningpotentialpsovector.h"

#include <QDebug>

namespace DFTBPSO
{

    ConfiningPotentialActionPSOVector::ConfiningPotentialActionPSOVector()
    {
        qDebug() << "ConfiningPotentialActionPSOVector::def_ctor() " << this ;
    }

    ConfiningPotentialActionPSOVector::~ConfiningPotentialActionPSOVector()
    {
        qDebug() << "ConfiningPotentialActionPSOVector::dtor() " << this ;
    }

    std::shared_ptr<PSOVector> ConfiningPotentialActionPSOVector::clone()
    {
        auto new_ptr = std::make_shared<ConfiningPotentialActionPSOVector>();
        new_ptr->m_take = this->m_take;
        for(auto const& item : this->m_confiningVectors)
        {
            auto item_type_ptr = std::dynamic_pointer_cast<ConfiningPotentialPSOVector>(item->clone());
            new_ptr->m_confiningVectors.append(item_type_ptr);
        }
        return new_ptr;
    }

    int ConfiningPotentialActionPSOVector::size() const
    {
        int size_ = 0;
        for(auto const& item : m_confiningVectors)
        {
            size_ += item->size();
        }
        return size_;
    }

    Vector<double> ConfiningPotentialActionPSOVector::encode() const
    {
        Vector<double> res;
        for(auto const& item : m_confiningVectors)
        {
            Vector<double> temp = item->encode();
            res.append(temp);
        }
        return res;
    }

    void ConfiningPotentialActionPSOVector::decode(const Vector<double> &vector)
    {
        Q_ASSERT_X(vector.size() == this->size(), "ConfiningPotentialActionPSOVector::decode", "size inconsistent");
        int index= 0;
        for(int i=0; i<this->m_confiningVectors.size(); ++i)
        {
            Vector<double> vec;
            for(int j=0; j<m_confiningVectors[i]->size(); ++j)
            {
                vec.append(vector[index+j]);
            }
            m_confiningVectors[i]->decode(vec);
            index += (m_confiningVectors[i]->size());
        }
    }

    void ConfiningPotentialActionPSOVector::spread(double factor)
    {
        for(auto const& item : m_confiningVectors)
        {
            item->spread(factor);
        }
    }

    void ConfiningPotentialActionPSOVector::copyfrom(const PSOVector *rhs)
    {
        PSOVector::copyfrom(rhs);
        const ConfiningPotentialActionPSOVector* real_pos = dynamic_cast<const ConfiningPotentialActionPSOVector*> (rhs);
        if (real_pos)
        {
            Q_ASSERT_X(real_pos->m_confiningVectors.size() == m_confiningVectors.size()
                       , "ConfiningPotentialActionPSOVector::copyfrom", "size inconsistent");

            for(int i=0; i<real_pos->m_confiningVectors.size(); ++i)
            {
                this->m_confiningVectors[i]->copyfrom(real_pos->m_confiningVectors[i].get());
                this->m_confiningVectors[i]->copyControllerFrom(real_pos->m_confiningVectors[i].get());
            }
        }
    }

    void ConfiningPotentialActionPSOVector::randomizePosition()
    {
        for(auto const& item : m_confiningVectors)
        {
            item->randomizePosition();
        }
    }

    void ConfiningPotentialActionPSOVector::randomizeVelocity()
    {
        for(auto const& item : m_confiningVectors)
        {
            item->randomizeVelocity();
        }
    }

    void ConfiningPotentialActionPSOVector::stablizeVelocity()
    {
        for(auto const& item : m_confiningVectors)
        {
            item->stablizeVelocity();
        }
    }

    void ConfiningPotentialActionPSOVector::stablizePosition(PSOVector *vel)
    {
        for(auto const& item : m_confiningVectors)
        {
            item->stablizePosition(vel);
        }
    }

    void ConfiningPotentialActionPSOVector::copyControllerFrom(const PSOVector *rhs)
    {
        const ConfiningPotentialActionPSOVector* vector = dynamic_cast<const ConfiningPotentialActionPSOVector*> (rhs);
        Q_ASSERT_X(vector != nullptr, "ConfiningPotentialActionPSOVector::copyControllerFrom", "typeError");
        for(int i=0; i<m_confiningVectors.size();++i)
        {
            m_confiningVectors[i]->copyControllerFrom(vector->m_confiningVectors[i].get());
        }
    }

    double ConfiningPotentialActionPSOVector::getLowerBound(int index)
    {
        int curIndex = 0;

        for(int i=0; i<m_confiningVectors.size();++i)
        {
            if (index >= curIndex && index < (curIndex+m_confiningVectors[i]->size()))
            {
                return m_confiningVectors[i]->getLowerBound(index-curIndex);
            }
            curIndex += m_confiningVectors[i]->size();
        }
        throw std::invalid_argument("ConfiningPotentialActionPSOVector::getLowerBound");
    }

    double ConfiningPotentialActionPSOVector::getUpperBound(int index)
    {
        int curIndex = 0;

        for(int i=0; i<m_confiningVectors.size();++i)
        {
            if (index >= curIndex && index < (curIndex+m_confiningVectors[i]->size()))
            {
                return m_confiningVectors[i]->getUpperBound(index-curIndex);
            }
            curIndex += m_confiningVectors[i]->size();
        }
        throw std::invalid_argument("ConfiningPotentialActionPSOVector::getUpperBound");
    }
    QStringList ConfiningPotentialActionPSOVector::getTake() const
    {
        return m_take;
    }

    void ConfiningPotentialActionPSOVector::setTake(const QStringList &take)
    {
        m_take = take;
    }


}
