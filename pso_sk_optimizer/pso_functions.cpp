#include "pso_functions.h"
#include <fstream>

#include <QCoreApplication>
#include <QMap>
#include <QTemporaryFile>
#include <QVariantMap>

#include "DFTBPlusAdaptor.h"

#include "dftbpsovector.h"
#include "LJDispersionPSOVector.h"
#include "D3DispersionPSOVector.h"

#include "skbuilder/SlaterKosterFileColletion.h"
#include "skbuilder/slaterkosterfile.h"
#include "skbuilder/skbuilder.h"

#include "atomicproperties.h"
#include "Singleton.h"

#include "DFTBTestSuite/util.h"
#include "DFTBTestSuite/dftbtester.h"
#include "DFTBTestSuite/statisticanalyser.h"
#include "DFTBTestSuite/input_variant.h"

#include "erepfit/Splcoeff.h"
#include "erepfitsystemevaluator.h"
#include "erepfit2_inputdata_json.h"
#include "skbuilder/input_variant.h"

#include <Variant/Variant.h>

#include "DFTBTestSuite/InputFile.h"

#include "TextSKFile.h"

#include <QProcess>
#include <QStringList>
#include <QString>

using namespace libPSO;
using namespace DFTBTestSuite;

namespace DFTBPSO
{

    typedef QMap<QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *>, SKBuilder::SlaterKosterFileColletion > SKFilesCollectionType;
    typedef QMapIterator<QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *>, SKBuilder::SlaterKosterFileColletion > SKFilesCollectionIteratorType;


void skfilesToSKFileInfo(ADPT::SKFileInfo& skfileinfo_elec, SKFilesCollectionType& skfiles, QDir skfolder)
{
    SKFilesCollectionIteratorType it(skfiles);
    while (it.hasNext())
    {

        QMap<int, QString> atom1_selected_shells_map;
        QMap<int, QString> atom2_selected_shells_map;


        it.next();
        QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *> pair = it.key();

        const SKBuilder::SlaterKosterFileColletion& files = it.value();

        QStringList filepaths;
        QMapIterator<QPair<int, int>, std::shared_ptr<SKBuilder::SlaterKosterFile>> skfile_iter(files.getSKFiles());
        while(skfile_iter.hasNext())
        {
            skfile_iter.next();
            std::shared_ptr<SKBuilder::SlaterKosterFile> file = skfile_iter.value();

            QString path = skfolder.absoluteFilePath(file->getFilename(false));
            filepaths.append(path);

            if (!atom1_selected_shells_map.contains(file->atom1_shell()))
            {
                atom1_selected_shells_map.insert(file->atom1_shell(), file->atom1_selectedShell());
            }

            if (!atom2_selected_shells_map.contains(file->atom2_shell()))
            {
                atom2_selected_shells_map.insert(file->atom2_shell(), file->atom2_selectedShell());
            }

        }
        auto poten = ADPT::PotentialName(pair.first->getSymbol(), pair.second->getSymbol());

        skfileinfo_elec.skfiles.insert(poten.toString(), filepaths);



        QStringList atom1_selected_shells = atom1_selected_shells_map.values();
        QStringList atom2_selected_shells = atom2_selected_shells_map.values();
        skfileinfo_elec.selectedShells.insert(pair.first->getSymbol(),atom1_selected_shells);
        skfileinfo_elec.selectedShells.insert(pair.second->getSymbol(),atom2_selected_shells);

        skfileinfo_elec.type2Names = false;
    }
}

QByteArray loadRepulsivePotential(const QString& filename)
{
    QByteArray res;
    QFile file(filename);
    if (!file.open(QIODevice::Text|QIODevice::ReadOnly))
    {
        return res;
    }

    QTextStream stream(&file);
    QString line;
    while(!stream.atEnd())
    {
        line = stream.readLine();
        if (line.startsWith("Spline"))
        {
            break;
        }
    }

    QTextStream stream_out(&res);
    stream_out << line << endl;
    line = stream.readLine();
    stream_out << line << endl;
    QStringList arr = line.split(" ", QString::SkipEmptyParts);
    int nspline = arr.at(0).toInt();
    for(int i=0; i<=nspline; ++i)
    {
        line = stream.readLine();
        stream_out << line << endl;
    }

    return res;
}


//void writeSkFileInfoToHSD(QTextStream& os, const ADPT::SKFileInfo& skfileinfo)
//{
//    if (skfileinfo.type2Names)
//    {
//        os << "+Hamiltonian = +DFTB {" << endl;

//        if (!skfileinfo.maxAngularMomentum.isEmpty())
//        {
//            os << "    !MaxAngularMomentum = {" << endl;
//            QMapIterator<QString, QString> it(skfileinfo.maxAngularMomentum);
//            while(it.hasNext())
//            {
//                it.next();
//                os << it.key() << " = " << "\"" << it.value() << "\"" << endl;
//            }
//            os << "    }" << endl;
//        }

//        os << "    !SlaterKosterFiles = !Type2FileNames{" << endl
//           << "        !Prefix = \"" << skfileinfo.prefix << "\""<< endl
//           << "        !Suffix = \"" << skfileinfo.suffix << "\"" << endl
//           << "        !Separator = \"" <<  skfileinfo.separator << "\"" << endl
//           << "        !LowerCaseTypeName = " << ( skfileinfo.lowercase ? "Yes" : "No") << endl
//           << "    }" << endl;


//        os << "}" << endl;
//    }
//    else
//    {

//        os << "+Hamiltonian = +DFTB {" << endl;

//        if (!skfileinfo.selectedShells.isEmpty())
//        {
//            os << "    !MaxAngularMomentum = {" << endl;
//            QMapIterator<QString, QStringList> it(skfileinfo.selectedShells);
//            while(it.hasNext())
//            {
//                it.next();
//                os << it.key() << " = " << "SelectedShells{" << it.value().join(" ") << "}" << endl;
//            }
//            os << "    }" << endl;
//        }

//        os << "    !SlaterKosterFiles = {" << endl;
//        QMapIterator<QString, QStringList> it(skfileinfo.skfiles);

//        while(it.hasNext())
//        {
//            it.next();
//            os << "      " << it.key() << " = " << it.value().join(" ") << endl;
//        }
//        os << "    }" << endl;

//        os   << "}" << endl; //Hamiltonian
//    }
//}



PSOFitnessValue pso_particle_evaluation(const std::shared_ptr<PSOVector> &vector, bool MOPSO)
{
    PSOFitnessValue fitness;

    try
    {

        if (MOPSO)
        {
            // we don't know the final demension before actually evaluation.
            //  fitness.setDimension();
        }
        else
        {
            fitness.setDimension(1);
        }


        auto dftbvector = std::dynamic_pointer_cast<DFTBPSOVector>(vector);


        QDir inputScratchDirRoot(dftbvector->m_input->options.scratchDir);
        if (inputScratchDirRoot.isAbsolute())
        {
            inputScratchDirRoot.mkpath(inputScratchDirRoot.absolutePath());
        }

        QString scratchPath;
        if (inputScratchDirRoot.exists())
        {
            scratchPath = inputScratchDirRoot.absolutePath();
        }


        if (inputScratchDirRoot.isRelative())
        {
            scratchPath = QDir::temp().absoluteFilePath(inputScratchDirRoot.path());
        }

        if (scratchPath.isEmpty())
        {
            scratchPath = QDir::tempPath();
        }

        QString oldPath = dftbvector->getRootPath();
        if (!oldPath.isEmpty())
        {
            QDir oldDir(oldPath);
            if (oldDir.exists())
            {
                oldDir.removeRecursively();
            }
        }

        QTemporaryDir tmpDir(scratchPath + QDir::separator()
                             + "DFTBPSO_" +
                             QString::number(QCoreApplication::applicationPid())
                             + "_XXXXXX");
        tmpDir.setAutoRemove(false);
        dftbvector->setRootPath(tmpDir.path());

        if (!tmpDir.isValid())
        {
            throw std::runtime_error("Cannot create temporary folder for calculation");
        }

        QDir tmpRootDir(tmpDir.path());

        QFile output_file(QDir(tmpDir.path()).absoluteFilePath("output.log"));

        output_file.open(QIODevice::WriteOnly | QIODevice::Text);

//        QByteArray evalData;
        QTextStream stdout_stream(&output_file);



        //QTextStream stderr_stream(&evalData);

        //initialization ends
        DFTBTestSuite::InputData testSuiteInput = dftbvector->m_input->tester.input;
        SKFilesCollectionType skfiles;

        std::shared_ptr<ADPT::SKFileInfo> skfileinfo = std::make_shared<ADPT::SKFileInfo>();

        //! TODO check this
        if (dftbvector->m_input->external_elec_skfileInfo)
        {
            *skfileinfo = *dftbvector->m_input->external_elec_skfileInfo;
        }


    std::shared_ptr<SKBuilder::Builder> builder = nullptr;

    //starting of electronic part;
    if (dftbvector->hasAtomicVector())
    {

        stdout_stream << "PSOVector contains AtomicVector, running SKBuilder..." << endl;

        QDir elecDir(tmpRootDir.absoluteFilePath("Electronic"));

        SKBuilder::Input::SKBuilderInput final_input = dftbvector->createSKBuilderInput();

        libvariant::Variant var = libvariant::VariantConvertor<SKBuilder::Input::SKBuilderInput>::toVariant(final_input);
        elecDir.mkpath(elecDir.absolutePath());


        libvariant::SerializeJSON(elecDir.absoluteFilePath("skbuilder_input.json").toStdString(), var, true);
        libvariant::SerializeYAML(elecDir.absoluteFilePath("skbuilder_input.yaml").toStdString(), var);


        builder = std::make_shared<SKBuilder::Builder>(final_input, stdout_stream);

        builder->setRootDir(elecDir);
        builder->setOutputDir(elecDir.absoluteFilePath("skfiles"));

        skfileinfo->type2Names = false;

        try
        {
            builder->init(false);

        }
        catch(SKBuilder::BuilderInitializationError& e)
        {
            vector->setFailed("Electronic part initialization error");
            fitness = 100000000.0;
            fitness.setIsFeasible(false);
            return fitness;
        }

        try
        {
            builder->build();
        }
        catch(...)
        {
            vector->setFailed("Electronic part infeasible");
            fitness = 100000000.0;
            fitness.setIsFeasible(false);
            return fitness;
        }
        try
        {
            builder->write();

            {
                skfiles = builder->getSKFiles();
                skfilesToSKFileInfo(*skfileinfo, skfiles, builder->getOutputDir() );

                libvariant::Variant var_parsed = libvariant::VariantConvertor<ADPT::SKFileInfo>::toVariant(*skfileinfo);
                QString filename = "skbuilder_out.yaml";
                libvariant::SerializeYAML(filename.toStdString(), var_parsed);
                filename = "skbuilder_out.json";
                libvariant::SerializeJSON(filename.toStdString(), var_parsed, true);
            }
        }
        catch(...)
        {
            vector->setFailed("SK Files written error");
            fitness = 100000000.0;
            fitness.setIsFeasible(false);
            return fitness;
        }


        stdout_stream << "Finishing SKBuilder..." << endl;

    }//end of electronic part

    //copy external electronic files;
    if (!dftbvector->m_input->parsed_external_elec_skpairs.isEmpty())
    {
        QMapIterator<QString, QStringList> it(dftbvector->m_input->parsed_external_elec_skpairs);
        while(it.hasNext())
        {
            it.next();
            if (skfileinfo->skfiles.contains(it.key()))
            {
                skfileinfo->skfiles[it.key()] = it.value();
            }
            else
            {
                skfileinfo->skfiles.insert(it.key(), it.value());
            }
        }
    }

    if (dftbvector->hasErepfitVector())
    {       
        stdout_stream << "PSOVector contains Repulsive..." << endl;

        QDir repulDir(tmpRootDir.absoluteFilePath("Repulsive"));
        repulDir.mkpath(repulDir.absolutePath());

        QDir full_sk_output(repulDir.absoluteFilePath("skfiles"));
        full_sk_output.mkpath(full_sk_output.absolutePath());

        //Erepfit
        auto rep_vector = dftbvector->erepfitVector();

        if (!dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->repulsivePotentials.empty())
        {

        auto pairSymbols = rep_vector->getPairSymbols();
        auto potentials = rep_vector->getPotentials();

        Erepfit2::Input::Erepfit2_InputData erepfit2_input =
                dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data;

//        std::shared_ptr<ADPT::SKFileInfo> new_skfileinfo = std::make_shared<ADPT::SKFileInfo>(*skfileinfo);





        for(int i=0; i<potentials.size(); ++i)
        {
            QPair<QString, QString> pair = pairSymbols[i];
            Erepfit2::Input::PotentialGrid grid2;
            auto potential_name = QString("%1-%2").arg(pair.first).arg(pair.second);
            Vector<double> pos = potentials[i]->encode();
            qSort(pos);
            grid2.knots.append(potentials[i]->getPosMin());
            for(int j = 0; j<pos.size(); ++j)
            {
               grid2.knots.append(pos[j]);
            }
            grid2.knots.append(potentials[i]->getPosMax());
            erepfit2_input.potential_grids.insert(potential_name, grid2);
        }

        auto addequvec = rep_vector->getAdditionalEquations();
        for(int i=0; i<addequvec.size();++i)
        {

            DFTBPSO::Input::ErepfitAdditionalEquation erepaddequ = rep_vector->getAdditionalEquationInfo().at(i);

            Vector<double> vec = addequvec[i]->encode();

            Erepfit2::Input::AdditionalEquation new_addeq;

            new_addeq.derivative = erepaddequ.derivative;
            new_addeq.distance = erepaddequ.distance;
            new_addeq.potential = QString("%1-%2").arg(erepaddequ.potentialName.first)
                    .arg(erepaddequ.potentialName.second);

            new_addeq.value = vec[0];
            new_addeq.weight = 1.0;

            auto eqs = erepfit2_input.equations.additionalEquations.value(new_addeq.potential,
                                                                          QList<Erepfit2::Input::AdditionalEquation>());
            eqs.append(new_addeq);
            erepfit2_input.equations.additionalEquations[new_addeq.potential] = eqs;
        }


        // if new electronic part generated, then set skfile_info as generated ones
//        if (!dftbvector->hasAtomicVector())
//        {
//            skfileinfo = dftbvector->m_input->skfileInfo;

//        }


        bool hasEnergyEquation = false;
        for(int i=0; i< erepfit2_input.equations.energyEquations.size(); ++i)
        {
            if (erepfit2_input.equations.energyEquations[i].weight > 0.0001)
            {
                hasEnergyEquation = true;
                break;
            }
        }

        if (hasEnergyEquation)
        {
            //prepare the atom energy
            // if FitAllAtomEnergy is defined, then use an empty list
            if (erepfit2_input.options.FitAllAtomEnergy)
            {
                erepfit2_input.atomicEnergy.atomic_energy.clear();
            }
//            else
//            {
//                //1. check if the skfiles for element is loaded here(not optimizing)
//                //   if so, then compute the atom energy here for save some time.
//                //   assuming that the atom energy is constant during the optimization

//                QStringList all_erepfit_elements = erepfit2_input.required_elements();

//                for(int i=0; i<all_erepfit_elements.size();++i)
//                {
//                    QString elem = all_erepfit_elements.at(i);

//                    if (erepfit2_input.atomicEnergy.atomic_energy.contains(elem))
//                    {
//                        continue;
//                    }
//                    else
//                    {
//                        bool computedFromSK = false;
//                        SKFilesCollectionIteratorType it(skfiles);
//                        while(it.hasNext())
//                        {
//                            it.next();
//                            if (it.key().first->getSymbol() == elem && it.key().second->getSymbol() == elem)
//                            {
//                                QMapIterator<QPair<int, int>, std::shared_ptr<SKBuilder::SlaterKosterFile>>  it2(it.value().getSKFiles());
//                                double AtomEnergy = 0.0;
//                                while (it2.hasNext())
//                                {
//                                    it2.next();
//                                    if (it2.key().first == it2.key().second)
//                                    {
//                                        AtomEnergy += it2.value()->getAtomEnergy();
//                                    }
//                                }

//                                erepfit2_input.atomicEnergy.atomic_energy.insert(elem, AtomEnergy);
//                                computedFromSK = true;
//                                break;
//                                qCritical() << "WARNING: Atom energy for element " << elem << " is computed directly from SK file.";
//                            }
//                        }
//                        if (!computedFromSK)
//                        {
//                            qCritical() << "FitAllAtomEnergy was not set in the input file and not all input file for computing atom energy were provided.";
//                            qCritical() << "Check input file for ErepfitInput/AtomEnergy section";
//                            throw std::runtime_error(("Cannot find atom energy or input file for atom energy: " + elem).toStdString());
//                        }
//                    }
//                }
//            }
        }


        {
            if (!erepfit2_input.electronic_skfiles)
            {
                erepfit2_input.electronic_skfiles = std::make_shared<ADPT::SKFileInfo>();
            }
            *erepfit2_input.electronic_skfiles = *skfileinfo;

            libvariant::Variant var = libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(erepfit2_input);
            QString filename = repulDir.absoluteFilePath("erepfit_input.json");
            libvariant::SerializeJSON(filename.toStdString(), var, true);
            filename = repulDir.absoluteFilePath("erepfit_input.yaml");
            libvariant::SerializeYAML(filename.toStdString(), var);
        }


        bool erepfit_succ = false;

        Erepfit2::ErepfitSystemEvaluator erepfit_evaluator(stdout_stream);
        erepfit_evaluator.setScratchRoot(repulDir.absolutePath());

///TODO: Hubbard derivatives
//        splcoeff.setHubbardDerivs(dftbvector->getHubbardDerivs());


        if (dftbvector->hasLJDispersionVector())
        {
            auto dispersion_vector = dftbvector->ljdispersionVector();

            QVariantMap lists;
            for(int i=0; i<dispersion_vector->numberDispersion();++i)
            {
                QVariantMap entry;
                QString element = dispersion_vector->getElement(i);
                double distance = dispersion_vector->getDistance(i);
                double energy = dispersion_vector->getEnergy(i);

                entry["distance"] = distance;
                entry["energy"] = energy;

                lists[element] = entry;
            }
            QVariantMap dispersion;
            dispersion["parameters"] = lists;
            dispersion["type"] = "lj";
            erepfit2_input.dftb_options["dispersion"] = dispersion;
        }
        else if (dftbvector->hasD3DispersionVector())
        {
            auto dispersion_vector = dftbvector->d3dispersionVector();
            QVariantMap d3map;
            d3map["damping"] = dispersion_vector->getDamping();
            d3map["s6"] = dispersion_vector->getS6();
            d3map["s8"] = dispersion_vector->getS8();
            if ( dispersion_vector->getDamping() == "bj")
            {
                d3map["a1"] = dispersion_vector->getA1();
                d3map["a2"] = dispersion_vector->getA2();
            }
            else if (dispersion_vector->getDamping() == "zero")
            {
                d3map["sr6"] = dispersion_vector->getSr6();
                d3map["alpha6"] = dispersion_vector->getAlpha6();
            }
            QVariantMap dispersion;
            dispersion["parameters"] = d3map;
            dispersion["type"] = "d3";
            erepfit2_input.dftb_options["dispersion"] = dispersion;
        }



        try
        {
             erepfit_evaluator.evaluate(&erepfit2_input);
        }
        catch (const std::exception& e)
        {
            vector->setFailed("Evaluation Exception: " + QString(e.what()));
            throw;
        }


        Erepfit::Splcoeff splcoeff(stdout_stream, erepfit2_input.electronic_skfiles);
        splcoeff.debug = dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.options.debug;




        if (dftbvector->hasDampingFactorVector())
        {
            auto dampingVector = dftbvector->dampingFactorVector();
            splcoeff.useDampingFactor = true;
            splcoeff.dampingFactor = dampingVector->at(0);
        }

        splcoeff.setReferenceRepulsivePotentials(dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials);
//        splcoeff.setUseTotalMinusRepulsive(dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.options.useTotalMinusRepulsive);
//        splcoeff.setHubbardDerivs(dftbvector->getHubbardDerivs());


        erepfit_succ = false;

        try
        {
             erepfit_succ = splcoeff.calculate(erepfit2_input);
        }
        catch (const std::exception& e)
        {
            vector->setFailed("Erepfit Solve Exception: " + QString(e.what()));
            throw;
        }

        if (erepfit_succ)
        {


            splcoeff.writeout(stdout_stream);

            if (dftbvector->m_input->pso.additionalFitness.type.toLower() == "erepfit")
            {
                auto source = dftbvector->m_input->pso.additionalFitness.source.toLower();

                double add_fit = 0.0;
                if (source == "rms")
                {
                    add_fit = splcoeff.getRMSResidual()*627.5095*dftbvector->m_input->pso.additionalFitness.weight;
                }
//                else if (source == "mad")
//                {
//                    add_fit = splcoeff.getMADResidual()*dftbvector->m_input->pso.additionalFitness.weight;
//                }

                fitness.setDimension(1);
                fitness.addFitness(0, add_fit);
            }



            for(auto p = erepfit2_input.atomicEnergy.atomic_energy.begin(); p!= erepfit2_input.atomicEnergy.atomic_energy.end();++p)
            {
                testSuiteInput.evaluation.evaluationMolecule.atomicEnergy.atomic_energy.insert(p.key(), p.value());
            }
            auto atomEnergyMap = splcoeff.getFittedAtomEnergies();
            for(auto p = atomEnergyMap.begin(); p!= atomEnergyMap.end() ; ++p)
            {
                testSuiteInput.evaluation.evaluationMolecule.atomicEnergy.atomic_energy.insert(p.key(), p.value());
            }

            QMap<QPair<QString, QString>, Erepfit::Spline4> repulsivePotentials = splcoeff.getPotentials();
            rep_vector->repulsivePotentials = repulsivePotentials;

            if (!skfiles.isEmpty())
            {

                //If there are some SK files from SKBuilder


                QMapIterator<QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *>, SKBuilder::SlaterKosterFileColletion > it(skfiles);
                while (it.hasNext())
                {

                    it.next();
                    QPair<const ADPT::AtomicProperties *, const ADPT::AtomicProperties *> pair = it.key();

                    QList<std::shared_ptr<SKBuilder::SlaterKosterFile>>  files = it.value().getSKFiles().values();


                    QPair<QString, QString> symbol_pair_1 = qMakePair(pair.first->getSymbol(), pair.second->getSymbol());
                    QPair<QString, QString> symbol_pair_2 = qMakePair(pair.second->getSymbol(), pair.first->getSymbol());

                    QByteArray repulsive_content;
                    QTextStream stream(&repulsive_content);


                    bool rep_fitted = true;
                    if (repulsivePotentials.contains(symbol_pair_1))
                    {
                        if (dftbvector->m_input->psoVector.repulsivePart->fourthOrder)
                        {
                            //write 4th order splines
                            repulsive_content.clear();
                            repulsive_content.append(repulsivePotentials[symbol_pair_1].toString());
                        }
                        else
                        {
                            Erepfit::Spline4ToSpline3(repulsivePotentials[symbol_pair_1], stream);
                        }
                    }
                    else if (repulsivePotentials.contains(symbol_pair_2))
                    {
                        if (dftbvector->m_input->psoVector.repulsivePart->fourthOrder)
                        {
                            //write 4th order splines
                            repulsive_content.clear();
                            repulsive_content.append(repulsivePotentials[symbol_pair_2].toString());
                        }
                        else
                        {
                            Erepfit::Spline4ToSpline3(repulsivePotentials[symbol_pair_2], stream);
                        }
                    }
                    else
                    {                       
                        stdout_stream << "The repulsive potential " << pair.first->getSymbol() << " - " << pair.second->getSymbol() << " is not found from Erepfit output" << endl;
                        rep_fitted = false;
//                        throw std::runtime_error("ERROR");
                    }

                    if (rep_fitted)
                    {

                        writeFile(repulDir.absoluteFilePath(QString("rep_%1_%2.txt").arg(pair.first->getSymbol())
                                                        .arg(pair.second->getSymbol())), QString(repulsive_content));
                    }
                    else
                    {
                        auto symbol_pair_1_name = ADPT::PotentialName(symbol_pair_1.first, symbol_pair_1.second);
                        if (dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials.contains(symbol_pair_1_name.toString()))
                        {
                            repulsive_content = loadRepulsivePotential(dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials[symbol_pair_1_name.toString()]);
                        }
                        else if (dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials.contains(symbol_pair_1_name.swapNames().toString()))
                        {
                            repulsive_content = loadRepulsivePotential(dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.external_repulsive_potentials[symbol_pair_1_name.swapNames().toString()]);
                        }
                        else
                        {
                            stdout_stream << "Using zero potential" ;

                            if (dftbvector->m_input->psoVector.repulsivePart->fourthOrder)
                            {
                                repulsive_content = "Spline4\n1 3\n0.0 0.0 0.0\n0.2 3.0 0.0 0.0 0.0 0.0 0.0";
                            }
                            else
                            {
                                repulsive_content = "Spline\n1 3\n0.0 0.0 0.0\n0.2 3.0 0.0 0.0 0.0 0.0 0.0 0.0";
                            }
                        }
                    }

                    for(int i=0; i<files.size();++i)
                    {
                        files[i]->setRepulsive(QString(repulsive_content));
                        files[i]->writeToFile(full_sk_output, false);
                    }

                }
//                skfileinfo->skfiles.clear();
                skfilesToSKFileInfo(*skfileinfo, skfiles, full_sk_output );
            }
            else
            {
                auto list = (dftbvector->m_input->psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.required_elec_pairs()+
                             dftbvector->m_input->tester.input.required_elec_pairs(dftbvector->m_input->tester.reference)).toList();

                //If no generated SK files, then it must be fed by the external electronic.


                if (dftbvector->m_input->external_elec_skfileInfo)
                {
                    skfileinfo->skfiles.clear();
                    skfileinfo->type2Names = false;

                    for(int i=0; i<list.size();++i)
                    {
                        auto pair = potential2ElementPair(list[i]);

                        QString elem1 = pair.first;
                        QString elem2 = pair.second;

                        QPair<QString, QString> symbol_pair_1 = qMakePair(elem1, elem2);
                        QPair<QString, QString> symbol_pair_2 = qMakePair(elem2, elem1);

                        QStringList filepaths;

                        QStringList files = dftbvector->m_input->external_elec_skfileInfo->getSKFileName(elem1, elem2);

                        QByteArray repulsive_content;
                        QTextStream stream(&repulsive_content);

                        bool hasRepulsive = false;

                        if (repulsivePotentials.contains(symbol_pair_1))
                        {
                            if (dftbvector->m_input->psoVector.repulsivePart->fourthOrder)
                            {
                                //write 4th order splines
                                repulsive_content.clear();
                                repulsive_content.append(repulsivePotentials[symbol_pair_1].toString()) ;
                            }
                            else
                            {
                                Erepfit::Spline4ToSpline3(repulsivePotentials[symbol_pair_1], stream);
                            }
                            hasRepulsive = true;
                        }
                        else if (repulsivePotentials.contains(symbol_pair_2))
                        {
                            if (dftbvector->m_input->psoVector.repulsivePart->fourthOrder)
                            {
                                //write 4th order splines
                                repulsive_content.clear();
                                repulsive_content.append(repulsivePotentials[symbol_pair_2].toString()) ;
                            }
                            else
                            {
                                Erepfit::Spline4ToSpline3(repulsivePotentials[symbol_pair_2], stream);
                            }
                            hasRepulsive = true;
                        }

                        for(int j=0; j<files.size();++j)
                        {
                            QString filename = QFileInfo(files[j]).fileName();

                            TextSKFile skfile;
                            if (!skfile.loadFile(files[j]))
                            {
                                throw std::runtime_error("FILE NOT FOUND ERROR");
                            }

                            if (hasRepulsive)
                            {
                                skfile.setRepulsive(QString(repulsive_content));
                            }


                            QString outfilename = full_sk_output.absoluteFilePath(filename);
                            QFile outfile(outfilename);
                            if (outfile.open(QIODevice::WriteOnly))
                            {
                                QTextStream outstream(&outfile);
                                skfile.writeTo(outstream);
                            }
                            else
                            {
                                throw std::runtime_error("IO ERROR");
                            }
                            filepaths.append(outfilename);
                        }

                        skfileinfo->skfiles.insert(list[i], filepaths);
                    }

                    QMapIterator<QString, QString> it(dftbvector->m_input->external_elec_skfileInfo->maxAngularMomentum);
                    const QStringList orbL = {"s", "p", "d", "f", "g", "h"};
                    while(it.hasNext())
                    {
                        it.next();
                        QStringList orbs;
                        int index = orbL.indexOf(it.value());
                        orbs.append(orbL.mid(0,index+1));
                        QStringList shells;
                        shells.append(orbs.join(""));
                        skfileinfo->selectedShells.insert(it.key(), shells);
                    }

                }
                else
                {
                    throw std::runtime_error("No opt conf and no skfileinfo");
                }
            }
            libvariant::Variant var_parsed = libvariant::VariantConvertor<ADPT::SKFileInfo>::toVariant(*skfileinfo);
            QString filename = "erepfit_out.yaml";
            libvariant::SerializeYAML(filename.toStdString(), var_parsed);
            filename = "erepfit_out.json";
            libvariant::SerializeJSON(filename.toStdString(), var_parsed, true);
        }
        else
        {
            vector->setFailed(splcoeff.getLastError());
            fitness = 100000000.0;
            return fitness;
        }
        }
    }

    // additional fitness script
    if (!MOPSO)
    {

        if (dftbvector->m_input->pso.additionalFitness.type.toLower() == "script")
        {

            if (! dftbvector->m_input->pso.additionalFitness.source.isEmpty())
            {

                QProcess m_process;

                m_process.setWorkingDirectory(tmpRootDir.absolutePath());
    //            m_process.setStandardOutputFile("addfit.out");
    //            m_process.setStandardErrorFile("addfit.err");
                m_process.start(dftbvector->m_input->pso.additionalFitness.source);
                m_process.waitForFinished(-1);


                QByteArray result = m_process.readAll();
                if (m_process.exitCode()==0)
                {
                    QString t(result);

                    bool ok;
                    double add_fit = t.toDouble(&ok);
                    if (ok)
                    {
                        fitness.setDimension(1);
                        fitness.addFitness(0,add_fit);
                    }
                }
            }
        }
    }


    testSuiteInput.evaluation.SKInfo = skfileinfo;

    //Evaluation
///TODO remove optimization on hubbard derivatives for a moment
//    testSuiteInput.evaluation.HubbardDerivs = dftbvector->getHubbardDerivs();

    if (dftbvector->hasLJDispersionVector())
    {
        auto dispersion_vector = dftbvector->ljdispersionVector();

        QVariantMap lists;
        for(int i=0; i<dispersion_vector->numberDispersion();++i)
        {
            QVariantMap entry;
            QString element = dispersion_vector->getElement(i);
            double distance = dispersion_vector->getDistance(i);
            double energy = dispersion_vector->getEnergy(i);

            entry["distance"] = distance;
            entry["energy"] = energy;

            lists[element] = entry;
        }
        QVariantMap dispersion;
        dispersion["parameters"] = lists;
        dispersion["type"] = "lj";
        testSuiteInput.evaluation.dftb_options["dispersion"] = dispersion;

    }

    if (dftbvector->hasD3DispersionVector())
    {
        auto dispersion_vector = dftbvector->d3dispersionVector();
        QVariantMap d3map;
        d3map["damping"] = dispersion_vector->getDamping();
        d3map["s6"] = dispersion_vector->getS6();
        d3map["s8"] = dispersion_vector->getS8();
        if ( dispersion_vector->getDamping() == "bj")
        {
            d3map["a1"] = dispersion_vector->getA1();
            d3map["a2"] = dispersion_vector->getA2();
        }
        else if (dispersion_vector->getDamping() == "zero")
        {
            d3map["sr6"] = dispersion_vector->getSr6();
            d3map["alpha6"] = dispersion_vector->getAlpha6();
        }
        QVariantMap dispersion;
        dispersion["parameters"] = d3map;
        dispersion["type"] = "d3";
        testSuiteInput.evaluation.dftb_options["dispersion"] = dispersion;
    }


    if (dftbvector->hasDampingFactorVector())
    {
        auto dampingVector = dftbvector->dampingFactorVector();
        testSuiteInput.evaluation.useDampingFactor = true;
        testSuiteInput.evaluation.dampingFactor = dampingVector->at(0);
    }


    DFTBTestSuite::DFTBTester tester(testSuiteInput,
                                     &dftbvector->m_input->tester.reference
                                     );

    QDir evalDir(tmpRootDir.absoluteFilePath("Testing"));
    evalDir.mkpath(evalDir.absolutePath());
    tester.setAutoRemove(false);
    tester.setTempDir(evalDir);

    testSuiteInput.evaluation.SKInfo->makeRelativePath(evalDir.absolutePath());


    {
        libvariant::Variant var = libvariant::VariantConvertor<DFTBTestSuite::InputData>::toVariant(testSuiteInput);
        QString filename = evalDir.absoluteFilePath("tester_input.json");
        libvariant::SerializeJSON(filename.toStdString(), var, true);
        filename = evalDir.absoluteFilePath("tester_input.yaml");
        libvariant::SerializeYAML(filename.toStdString(), var);
    }




    tester.evaluate(stdout_stream);


    if (MOPSO)
    {
        if (!tester.getStatistics()->getUnconvergedTarget().isEmpty())
        {
            fitness.setIsOptimalCandidate(false);
        }

        QList<QString> error_types = tester.getStatistics()->getTypes();


        fitness.setDimension(error_types.size()+1);
        for(int i=0; i<error_types.size();++i)
        {
            double type_fitness = tester.getStatistics()->getError_MUE(error_types[i]);
            if (error_types[i].toLower() == "frequencies")
            {
                type_fitness = tester.getStatistics()->getError_MAPE(error_types[i]);
            }

            fitness[i] = type_fitness;
            fitness.setHeader(i, error_types[i]);
        }

//            fitness[error_types.size()] = tester.getStatistics()->getNumberOfImaginaryFrequencyTargets();
//            fitness.setHeader(error_types.size(), "#Imag. Freq.");
        fitness[error_types.size()] = tester.getStatistics()->getUnconvergedTarget().size();
        fitness.setHeader(error_types.size(), "UnconvergedTarget");

    }
    else
    {
//     Single-objective PSO


//        if (dftbvector->m_input->pso.algorithm.toLower() == "refine")
//        {
//            auto types = tester.getStatistics()->getTypes();
//            double value = 0.0;
//            int nfitness = 0;
//            for(int i=0; i<types.size();++i)
//            {
//                if (dftbvector->m_input->pso.refining_initials.contains(types[i]))
//                {
//                    double ref_value = dftbvector->m_input->pso.refining_initials[types[i]];
//                    double l_fitness = tester.getStatistics()->getError_RMS(types[i])/ref_value;
//                    value += l_fitness*l_fitness;
//                    ++nfitness;
//                }
//            }
//            if (nfitness == 0)
//            {
//                throw std::runtime_error("No valid fitness function defined");
//            }
//            fitness.setDimension(1);

//            value += 1.0e6*tester.getStatistics()->getUnconvergedTarget().size();
//            fitness = value;
//        }
//        else
//        {
            double value = tester.getStatistics()->getFitness();
            fitness.setDimension(1);

            auto unconverged = tester.getStatistics()->getUnconvergedTarget();
            fitness += (value + 100000*unconverged.size());
//            if (!unconverged.empty())
//            {
//                vector->setFailed(QString("%1 calculations failed in fitting set.").arg(unconverged.size()));
//                fitness = 1.0e12;
//            }

//        }
    }

    //clean
    {

        skfileinfo->skfiles.clear();
        skfiles.clear();
    }

    }
    catch(std::exception &e)
    {
        qDebug() << e.what();
        vector->setFailed("Evaluation Exception: " + QString(e.what()));
//        vector->setFailed(splcoeff.getLastError());
        fitness = 100000000.0;
        fitness.setIsFeasible(false);
        return fitness;
    }

    return fitness;
}
}
