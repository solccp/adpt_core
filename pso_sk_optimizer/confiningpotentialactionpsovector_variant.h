#ifndef CONFININGPOTENTIALACTIONPSOVECTOR_VARIANT_H
#define CONFININGPOTENTIALACTIONPSOVECTOR_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"

#include "confiningpotentialactionpsovector.h"
#include "confiningpotentialpsovector.h"


namespace libvariant
{
template<>
struct VariantConvertor<DFTBPSO::ConfiningPotentialActionPSOVector>
{
    static Variant toVariant(const DFTBPSO::ConfiningPotentialActionPSOVector &rhs)
    {
        //! TODO Rewrite
        Variant node;
        node["take"] = VariantConvertor<QStringList>::toVariant(rhs.m_take);
        for(auto const & item : rhs.m_confiningVectors)
        {
            auto var = item->toVariant();
            node["confinings"].Append(var);
        }
        return node;
    }
    static void fromVariant(const Variant &var, DFTBPSO::ConfiningPotentialActionPSOVector &rhs)
    {
        try
        {
            GET_VARIANT(rhs.m_take, var, "take", QStringList);
            if (var.Contains("confinings") && var["confinings"].IsList())
            {
                for(auto j=0u; j<var["confinings"].Size();++j)
                {
                    Variant subnode = var["confinings"].At(j);
                    QStringList types;
                    GET_VARIANT(types, subnode, "orbital_types", QStringList);
                    for(auto i=0; i<rhs.m_confiningVectors.size();++i)
                    {
                        if (types.toSet() == rhs.m_confiningVectors[i]->orbitalTypes().toSet())
                        {
                            rhs.m_confiningVectors[i]->fromVariant(subnode);
                            break;
                        }
                    }
                }
            }
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};

}


#endif // CONFININGPOTENTIALACTIONPSOVECTOR_VARIANT_H

