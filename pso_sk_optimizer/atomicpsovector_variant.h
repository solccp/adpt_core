#ifndef ATOMICPSOVECTOR_VARIANT_H
#define ATOMICPSOVECTOR_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"

#include "adpt/input_variant.h"

#include "atomicpsovector.h"
#include "confiningpotentialactionpsovector_variant.h"

namespace libvariant
{



template<>
struct VariantConvertor<DFTBPSO::AtomicPSOVector>
{
    static Variant toVariant(const DFTBPSO::AtomicPSOVector &rhs)
    {
        Variant node;

        for(int i=0; i<rhs.m_confiningActions.size();++i)
        {
            Variant subnode;
            subnode = VariantConvertor<DFTBPSO::ConfiningPotentialActionPSOVector>::toVariant(*rhs.m_confiningActions[i]);
            node["confining_actions"].Append(subnode);
        }


        for(int i=0; i<rhs.m_orbitalEnergyVectors.size();++i)
        {            
            Variant subnode = VariantConvertor<DFTBPSO::Input::OrbitalEnergyDefinition>::toVariant(rhs.m_orbitalEnergyDefinitions[i]);
            subnode["value"] = rhs.m_orbitalEnergyVectors[i]->at(0);
            subnode["real_orbital_energy"] = rhs.m_orbitalEnergyVectors[i]->at(0)+rhs.m_orbitalEnergyReferredOrbitalEnergy[i];
            node["orbital_energy"].Append(subnode);
        }

        for(int i=0; i<rhs.m_hubbardVectors.size();++i)
        {
            Variant subnode = VariantConvertor<DFTBPSO::Input::HubbardDefinition>::toVariant(rhs.m_hubbardDefinitions[i]);
            subnode["value"] = rhs.m_hubbardVectors[i]->at(0);
            node["hubbard"].Append(subnode);
        }

        for(int i=0; i<rhs.m_hubbardDerivsVectors.size();++i)
        {
            Variant subnode = VariantConvertor<DFTBPSO::Input::HubbardDefinition>::toVariant(rhs.m_hubbardDerivsDefinitions[i]);
            subnode["value"] = rhs.m_hubbardDerivsVectors[i]->at(0);
            node["hubbard_derivs"].Append(subnode);
        }

        return node;
    }
    static void fromVariant(const Variant &node, DFTBPSO::AtomicPSOVector &rhs)
    {
        try
        {
            //! TODO write for action
            if (node.Contains("confining_actions") && node["confining_actions"].IsList())
            {
                for(auto i=0u; i<node["confining_actions"].Size();++i)
                {
                    Variant subnode = node["confining_actions"].At(i);
                    QStringList takes;

                    GET_VARIANT(takes, subnode, "take", QStringList);
                    for(auto & action : rhs.m_confiningActions)
                    {
                        if (action->getTake().toSet() == takes.toSet())
                        {
                            libvariant::VariantConvertor<DFTBPSO::ConfiningPotentialActionPSOVector>::fromVariant(subnode, *action);
                            break;
                        }
                    }
                }
            }
            if (node.Contains("orbital_energy") && node["orbital_energy"].IsList())
            {
                for(std::size_t j=0; j< node["orbital_energy"].Size();++j)
                {
                    Variant subnode = node["orbital_energy"].At(j);
                    QString orb = QString::fromStdString(subnode["orbital"].AsString());
                    QString refOrb = QString::fromStdString(subnode["referred_orbital"].AsString());
                    double value = subnode["value"].AsDouble();

                    for(int i=0; i<rhs.m_orbitalEnergyDefinitions.size();++i)
                    {
                        if (rhs.m_orbitalEnergyDefinitions.at(i).orbital == orb && rhs.m_orbitalEnergyDefinitions.at(i).referredOrbital == refOrb)
                        {
                            rhs.m_orbitalEnergyVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }

            if (node.Contains("hubbard") && node["hubbard"].IsList())
            {
                for(std::size_t j=0; j< node["hubbard"].Size();++j)
                {
                    Variant subnode = node["hubbard"].At(j);
                    QString orb = QString::fromStdString(subnode["orbital"].AsString());
                    double value = subnode["value"].AsDouble();

                    for(int i=0; i<rhs.m_hubbardDefinitions.size();++i)
                    {
                        if (rhs.m_hubbardDefinitions.at(i).orbital == orb)
                        {
                            rhs.m_hubbardVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }

            if (node.Contains("hubbard_derivs") && node["hubbard_derivs"].IsList())
            {
                for(std::size_t j=0; j< node["hubbard_derivs"].Size();++j)
                {
                    Variant subnode = node["hubbard_derivs"].At(j);
                    QString orb = QString::fromStdString(subnode["orbital"].AsString());
                    double value = subnode["value"].AsDouble();

                    for(int i=0; i<rhs.m_hubbardDerivsDefinitions.size();++i)
                    {
                        if (rhs.m_hubbardDerivsDefinitions.at(i).orbital == orb)
                        {
                            rhs.m_hubbardDerivsVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }

        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};

}

#endif // ATOMICPSOVECTOR_VARIANT_H

