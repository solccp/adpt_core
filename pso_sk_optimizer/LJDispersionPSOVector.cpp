#include "LJDispersionPSOVector.h"

#include "pso1dvector.h"
#include "vector.h"

#include <QDebug>
#include <stdexcept>


namespace DFTBPSO
{
LJDispersionPSOVector::LJDispersionPSOVector()
{
}

LJDispersionPSOVector::~LJDispersionPSOVector()
{
    clear();
}

void LJDispersionPSOVector::addVector(const QString &element, const QPair<double, double> &distance_range, const QPair<double, double> &energy_range)
{
    auto d_vector = std::make_shared<PSO1DVector>(1, distance_range.first, distance_range.second);
    auto e_vector = std::make_shared<PSO1DVector>(1, energy_range.first, energy_range.second);

    m_distance_vectors.append(d_vector);
    m_energy_vectors.append(e_vector);
    m_elements.append(element);
    m_vectors.insert(element, qMakePair(d_vector, e_vector));
}

int LJDispersionPSOVector::numberDispersion() const
{
    return m_elements.size();
}

QString LJDispersionPSOVector::getElement(int index) const
{
    return m_elements[index];
}

double LJDispersionPSOVector::getDistance(int index) const
{
    Vector<double> vec = m_distance_vectors[index]->encode();
    return vec[0];
}

double LJDispersionPSOVector::getEnergy(int index) const
{
    Vector<double> vec = m_energy_vectors[index]->encode();
    return vec[0];
}


std::shared_ptr<PSOVector> LJDispersionPSOVector::clone()
{
    auto newvector = std::make_shared<LJDispersionPSOVector>();
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        auto dvec = std::dynamic_pointer_cast<PSO1DVector>(m_distance_vectors[i]->clone());
        newvector->m_distance_vectors.append(dvec);
        auto evec = std::dynamic_pointer_cast<PSO1DVector>(m_energy_vectors[i]->clone());
        newvector->m_energy_vectors.append(evec);
        newvector->m_elements.append(this->m_elements[i]);
        newvector->m_vectors.insert(this->m_elements[i], qMakePair(dvec, evec));
    }
    return newvector;
}

int LJDispersionPSOVector::size() const
{
    int size = m_distance_vectors.size()+m_energy_vectors.size();
    return size;
}

Vector<double> LJDispersionPSOVector::encode() const
{
    Vector<double> res;
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        Vector<double> vec = m_distance_vectors[i]->encode();
        res.append(vec);
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        Vector<double> vec = m_energy_vectors[i]->encode();
        res.append(vec);
    }
    return res;
}

void LJDispersionPSOVector::decode(const Vector<double> &vector)
{
    Q_ASSERT_X(vector.size() == this->size(), "DispersionPSOVector::decode", "size inconsistent");
    int index = 0;
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_distance_vectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_distance_vectors[i]->decode(vec);
        index += (m_distance_vectors[i]->size());

    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_energy_vectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_energy_vectors[i]->decode(vec);
        index += (m_energy_vectors[i]->size());
    }
}

void LJDispersionPSOVector::spread(double factor)
{
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->spread(factor);
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->spread(factor);
    }
}

double LJDispersionPSOVector::getLowerBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        if (index >= curIndex && index < (curIndex+m_distance_vectors[i]->size()))
        {
            return m_distance_vectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_distance_vectors[i]->size();
    }

    for(int i=0; i<m_energy_vectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_energy_vectors[i]->size()))
        {
            return m_energy_vectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_energy_vectors[i]->size();
    }

    throw std::invalid_argument("DispersionPSOVector::getLowerBound");
}

double LJDispersionPSOVector::getUpperBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        if (index >= curIndex && index < (curIndex+m_distance_vectors[i]->size()))
        {
            return m_distance_vectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_distance_vectors[i]->size();
    }

    for(int i=0; i<m_energy_vectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_energy_vectors[i]->size()))
        {
            return m_energy_vectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_energy_vectors[i]->size();
    }

    throw std::invalid_argument("DispersionPSOVector::getUpperBound");
}

void LJDispersionPSOVector::randomizePosition()
{
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->randomizePosition();
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->randomizePosition();
    }
}

void LJDispersionPSOVector::randomizeVelocity()
{
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->randomizeVelocity();
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->randomizeVelocity();
    }
}

void LJDispersionPSOVector::stablizeVelocity()
{
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->stablizeVelocity();
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->stablizeVelocity();
    }
}

void LJDispersionPSOVector::stablizePosition(libPSO::PSOVector *vel)
{
    const LJDispersionPSOVector* real_vel = dynamic_cast<const LJDispersionPSOVector*> (vel);
    Q_ASSERT_X(real_vel != nullptr, "DispersionPSOVector::stablizePosition", "typeError");
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->stablizePosition(real_vel->m_distance_vectors[i].get());
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->stablizePosition(real_vel->m_energy_vectors[i].get());
    }
}

void LJDispersionPSOVector::copyControllerFrom(const libPSO::PSOVector *rhs)
{
    const LJDispersionPSOVector *vector = dynamic_cast<const LJDispersionPSOVector*>(rhs);
    Q_ASSERT_X(vector != nullptr, "DispersionPSOVector::copyControllerFrom", "TypeError");
    for(int i=0; i<m_distance_vectors.size(); ++i)
    {
        m_distance_vectors[i]->copyControllerFrom(vector->m_distance_vectors[i].get());
    }
    for(int i=0; i<m_energy_vectors.size(); ++i)
    {
        m_energy_vectors[i]->copyControllerFrom(vector->m_energy_vectors[i].get());
    }
}

void LJDispersionPSOVector::clear()
{
    m_elements.clear();
    m_distance_vectors.clear();
    m_energy_vectors.clear();
    m_vectors.clear();
}

}
