#ifndef TYPESETTINGS_H
#define TYPESETTINGS_H

#include <QString>

inline QString centerString(const QString& str, int width = 80)
{
    int len = str.size();
    int rest = width - len;
    if (rest < 0)
    {
        return str;
    }
    int leftpad = rest / 2;
    int rightpad = rest - leftpad;
    return (QString("").fill(' ', leftpad) + str + QString("").fill(' ', rightpad));
}

const QString HYPHENLINE = QString("").fill('-',80);
const QString EQUALLINE  = QString("").fill('=',80);
const QString STARLINE   = QString("").fill('*',80);
const QString EXCLALINE   = QString("").fill('!',80);
const QString INDENT_SPACES = "    " ;


#endif // TYPESETTINGS_H
