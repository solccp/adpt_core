#ifndef EREPFITKNOTSPOSITIONVECTOR_H
#define EREPFITKNOTSPOSITIONVECTOR_H

#include "pso1dvector.h"

#include <QVector>

class ErepfitKnotsPositionVector : public libPSO::PSO1DVector
{
    friend class PSO1DVectorRangeController;
public:
    ErepfitKnotsPositionVector(int size, double min, double max);
    ErepfitKnotsPositionVector(const ErepfitKnotsPositionVector& rhs);
protected:
    QVector<double> m_init_knot_position;
    double m_interval;

protected:
    void ensureSeparation();
    void sortKnots(ErepfitKnotsPositionVector *pos, ErepfitKnotsPositionVector *vel);
public:
    virtual void randomizePosition();
    virtual void stablizePosition(libPSO::PSOVector *vel);

    // PSO1DVector interface
public:
    void adjustVelocityRanges();

    // PSOVector interface
public:
    double getLowerBound(int index);
    double getUpperBound(int index);

    // PSOVector interface
public:
    std::shared_ptr<libPSO::PSOVector> clone() override;
    void decode(const libPSO::Vector<double> &vector);
};

#endif // EREPFITKNOTSPOSITIONVECTOR_H
