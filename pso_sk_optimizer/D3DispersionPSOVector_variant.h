#ifndef D3DISPERSIONPSOVECTOR_VARIANT_H
#define D3DISPERSIONPSOVECTOR_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"


#include "D3DispersionPSOVector.h"

namespace libvariant
{

template<>
struct VariantConvertor<DFTBPSO::D3DispersionPSOVector>
{
    using pdd = QPair<double, double>;
    static Variant toVariant(const DFTBPSO::D3DispersionPSOVector &rhs)
    {
        Variant node;
        Variant root_node;

        node["s6"] = rhs.getS6();
        node["s8"] = rhs.getS8();
        if (rhs.getDamping() == "bj")
        {
            node["damping"] = "bj";
            node["a1"] = rhs.getA1();
            node["a2"] = rhs.getA2();

            node["range_a1"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_a1->getPosMin(),
                                                 rhs.m_a1->getPosMax()));

            node["range_a2"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_a2->getPosMin(),
                                                 rhs.m_a2->getPosMax()));
        }
        else
        {
            node["damping"] = "zero";
            node["sr6"] = rhs.getSr6();
            node["alpha6"] = rhs.getAlpha6();

            node["range_sr6"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_sr6->getPosMin(),
                                                 rhs.m_sr6->getPosMax()));

            node["range_alpha6"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_alpha6->getPosMin(),
                                                 rhs.m_alpha6->getPosMax()));
        }

        node["range_s6"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_s6->getPosMin(),
                                             rhs.m_s6->getPosMax()));

        node["range_s8"] = VariantConvertor<pdd>::toVariant(qMakePair(rhs.m_s8->getPosMin(),
                                             rhs.m_s8->getPosMax()));

        root_node["d3dispersion"] = node;
        return root_node;
    }
    static void fromVariant(const Variant &node, DFTBPSO::D3DispersionPSOVector &rhs)
    {
        using namespace libPSO;
        try
        {
            QString str_value;
            GET_VARIANT(str_value, node, "damping", QString);

            rhs.setDamping(str_value);

            double value;

            Vector<double> vec;

            GET_VARIANT(value, node, "s6", double);
            vec.append(value);
            GET_VARIANT(value, node, "s8", double);
            vec.append(value);

            pdd pair;

            GET_VARIANT(pair, node, "range_s6", pdd);
            rhs.m_s6->setPosMin(pair.first);
            rhs.m_s6->setPosMax(pair.second);
            GET_VARIANT(pair, node, "range_s8", pdd);
            rhs.m_s8->setPosMin(pair.first);
            rhs.m_s8->setPosMax(pair.second);

            if (rhs.getDamping() == "bj")
            {
                GET_VARIANT(value, node, "a1", double);
                vec.append(value);
                GET_VARIANT(value, node, "a2", double);
                vec.append(value);


                GET_VARIANT(pair, node, "range_a1", pdd);
                rhs.m_a1->setPosMin(pair.first);
                rhs.m_a1->setPosMax(pair.second);
                GET_VARIANT(pair, node, "range_a2", pdd);
                rhs.m_a2->setPosMin(pair.first);
                rhs.m_a2->setPosMax(pair.second);



            }
            else
            {
                GET_VARIANT(value, node, "sr6", double);
                vec.append(value);
                GET_VARIANT(value, node, "alpha6", double);
                vec.append(value);


                GET_VARIANT(pair, node, "range_sr6", pdd);
                rhs.m_sr6->setPosMin(pair.first);
                rhs.m_sr6->setPosMax(pair.second);
                GET_VARIANT(pair, node, "range_alpha6", pdd);
                rhs.m_alpha6->setPosMin(pair.first);
                rhs.m_alpha6->setPosMax(pair.second);
            }

            rhs.decode(vec);
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << std::endl;
            throw e;
        }
    }
};


}



#endif // DISPERSIONPSOVECTOR_VARIANT_H

