#include "TextSKFile.h"

#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringBuilder>

TextSKFile::TextSKFile()
{
}

bool TextSKFile::loadFile(const QString &filename)
{
    QFile infile(filename);
    if (!infile.open(QIODevice::ReadOnly))
    {
        return false;
    }

    QTextStream stream(&infile);
    bool elec_part = true;
    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        if (line.startsWith("Spline"))
        {
            elec_part = false;
        }
        if (elec_part)
        {
            m_electronic += (line + "\n");
        }
        else
        {
            m_repulsive += (line + "\n");
        }
    }
    return true;
}
QString TextSKFile::repulsive() const
{
    return m_repulsive;
}

void TextSKFile::setRepulsive(const QString &repulsive)
{
    m_repulsive = repulsive;
}

void TextSKFile::writeTo(QTextStream &os)
{
    os << m_electronic << m_repulsive << endl;
}

