#ifndef TWNCONFININGPOTENTIALPSOVECTOR_YAML_H
#define TWNCONFININGPOTENTIALPSOVECTOR_YAML_H

#include <yaml-cpp/yaml.h>
#include "twnconfiningpotentialpsovector.h"

namespace YAML
{
    template<>
    struct convert<DFTBPSO::TWNConfiningPotentialPSOVector>
    {

        static Node encode(const DFTBPSO::TWNConfiningPotentialPSOVector& rhs)
        {
            Node node;
            if (rhs.m_type == DFTBPSO::TWNConfiningPotentialPSOVector::Density)
            {
                node["Type"] = "density";
            }
            else if (rhs.m_type == DFTBPSO::TWNConfiningPotentialPSOVector::TotalPotential)
            {
                node["Type"] = "totalpotential";
            }
            else if (rhs.m_type == DFTBPSO::TWNConfiningPotentialPSOVector::CoulombPotential)
            {
                node["Type"] = "coulombpotential";
            }
            else
            {
                node["Type"] = "orbital";
                node["OrbitalTypes"] = rhs.m_orbitalTypes;
            }

            node["w"] = (*rhs.w)[0];
            node["a"] = (*rhs.a)[0];
            node["r"] = (*rhs.r)[0];
            return node;
        }
        static bool decode(const Node& node, DFTBPSO::TWNConfiningPotentialPSOVector& rhs)
        {
            if (node["Type"].as<QString>().toLower() == "density")
            {
                rhs.m_type = DFTBPSO::TWNConfiningPotentialPSOVector::Density;
            }
            else if (node["Type"].as<QString>().toLower() == "totalpotential")
            {
                rhs.m_type = DFTBPSO::TWNConfiningPotentialPSOVector::TotalPotential;
            }
            else if (node["Type"].as<QString>().toLower() == "coulombpotential")
            {
                rhs.m_type = DFTBPSO::TWNConfiningPotentialPSOVector::CoulombPotential;
            }
            else
            {
                rhs.m_type = DFTBPSO::TWNConfiningPotentialPSOVector::Orbital;
                rhs.m_orbitalTypes = node["OrbitalTypes"].as<QStringList>();
            }
            if (rhs.w)
            {
                (*rhs.w)[0] = node["w"].as<double>();
            }
            if (rhs.a)
            {
                (*rhs.a)[0] = node["a"].as<double>();
            }
            if (rhs.r)
            {
                (*rhs.r)[0] = node["r"].as<double>();
            }
            return true;
        }
    };

}


#endif // TWNCONFININGPOTENTIALPSOVECTOR_YAML_H
