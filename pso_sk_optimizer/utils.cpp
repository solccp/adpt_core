#include "utils.h"

#include <QString>
#include <QDir>
#include <QFile>

namespace DFTBPSO
{

void copyPath(const QString& src, const QString& dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile newfile(dst + QDir::separator() + f);
        if (newfile.exists())
        {
            newfile.remove();
        }

        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

QPair<QString, QString> potential2ElementPair(const QString &potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    QPair<QString, QString> res;
    res.first = arr[0];
    res.second = arr[1];
    return res;
}

QString elementPair2Potential(const QPair<QString, QString> &pair)
{
    return QString("%1-%2").arg(pair.first).arg(pair.second);
}

QString switchPotential(const QString &potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    return QString("%1-%2").arg(arr[1]).arg(arr[0]);
}


}
