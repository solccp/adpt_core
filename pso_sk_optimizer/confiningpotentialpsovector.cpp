#include "confiningpotentialpsovector.h"
#include "variantqt.h"

namespace DFTBPSO
{

void ConfiningPotentialPSOVector::fromVariant(const libvariant::Variant &var)
{
    GET_VARIANT(m_orbitalTypes, var, "orbital_types", QStringList);
}

libvariant::Variant ConfiningPotentialPSOVector::toVariant() const
{
    libvariant::Variant node;
    node["orbital_types"] = libvariant::VariantConvertor<QStringList>::toVariant(m_orbitalTypes);
    return node;
}

QStringList ConfiningPotentialPSOVector::orbitalTypes() const
    {
        return m_orbitalTypes;
    }


    void ConfiningPotentialPSOVector::setOrbitalTypes(const QStringList &orbitalTypes)
    {
        m_orbitalTypes = orbitalTypes;
    }

}
