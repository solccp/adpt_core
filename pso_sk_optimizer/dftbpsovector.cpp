#include "dftbpsovector.h"
#include "adpt/input.h"

#include "atomicpsovector.h"

#include "erepfitpsovector.h"
#include "LJDispersionPSOVector.h"
#include "D3DispersionPSOVector.h"

#include "confiningpotentialactionpsovector.h"
#include "confiningpotentialpsovector.h"

#include "skbuilder/factory.h"
#include "skbuilder/input.h"


namespace DFTBPSO
{


DFTBPSOVector::DFTBPSOVector()
{
}

DFTBPSOVector::~DFTBPSOVector()
{
    if (!m_rootPath.isEmpty())
    {
        if (QDir(m_rootPath).exists())
        {
            if (!m_input->options.debug)
            {
                QDir(m_rootPath).removeRecursively();
            }
        }
    }
//    delete m_erepfitVector;
//    for(int i=0; i<m_atomicVectors.size();++i)
//    {
//        delete m_atomicVectors[i];
//    }
    m_atomicVectors.clear();
//    if (m_dispersionVector)
//    {
//        delete m_dispersionVector;
//    }
//    if (m_dampingFactorVector)
//    {
//        delete m_dampingFactorVector;
//    }
}


bool DFTBPSOVector::define(const Input::SKOPTInput *input_)
{
    m_input = input_;


    if (input_->psoVector.electronicPart)
    {

        for(int i=0; i<input_->psoVector.electronicPart->atomicDefinitions.size();++i)
        {
            const DFTBPSO::Input::AtomicDefinition& def = input_->psoVector.electronicPart->atomicDefinitions[i];
            const SKBuilder::Input::SKBuilderInput& skbuilder_input = *input_->skbuilder_input;
            auto newatomic = std::make_shared<AtomicPSOVector>(def.element);
            if (newatomic->define(def, skbuilder_input))
            {
                m_atomicVectors.append(newatomic);
            }
            else
            {
                throw std::runtime_error("Wrong definition of AtomicDefinition: " + def.element.toStdString());
            }

        }

    }


    if (input_->psoVector.repulsivePart)
    {
        if (input_->psoVector.repulsivePart->repulsiveType.toLower() == "erepfit")
        {
            auto newvec = std::make_shared<ErepFitPSOVector>();
            newvec->define(m_input->psoVector.repulsivePart->erepfitPSOVector.get());
            m_erepfitVector = newvec;
        }
    }

    if (input_->psoVector.independendPart)
    {
        if (!input_->psoVector.independendPart->ljDispersion.empty())
        {
            m_ljdispersionVector = std::make_shared<LJDispersionPSOVector>();
            for(int i=0; i<input_->psoVector.independendPart->ljDispersion.size();++i)
            {
                m_ljdispersionVector->addVector(input_->psoVector.independendPart->ljDispersion[i].element,
                                              input_->psoVector.independendPart->ljDispersion[i].distance_range,
                                              input_->psoVector.independendPart->ljDispersion[i].energy_range);
            }
        }

        if (input_->psoVector.independendPart->d3Dispersion)
        {
            m_d3dispersionVector = std::make_shared<D3DispersionPSOVector>(input_->psoVector.independendPart->d3Dispersion->damping);

            m_d3dispersionVector->setS6(input_->psoVector.independendPart->d3Dispersion->s6.first, input_->psoVector.independendPart->d3Dispersion->s6.second);
            m_d3dispersionVector->setS8(input_->psoVector.independendPart->d3Dispersion->s8.first, input_->psoVector.independendPart->d3Dispersion->s8.second);


            if (m_d3dispersionVector->getDamping() == "bj")
            {
                m_d3dispersionVector->setA1(input_->psoVector.independendPart->d3Dispersion->a1.first,input_->psoVector.independendPart->d3Dispersion->a1.second);
                m_d3dispersionVector->setA2(input_->psoVector.independendPart->d3Dispersion->a2.first,input_->psoVector.independendPart->d3Dispersion->a2.second);
            }
            else if (m_d3dispersionVector->getDamping() == "zero")
            {
                m_d3dispersionVector->setSr6(input_->psoVector.independendPart->d3Dispersion->sr6.first,input_->psoVector.independendPart->d3Dispersion->sr6.second);
                m_d3dispersionVector->setAlpha6(input_->psoVector.independendPart->d3Dispersion->alpha6.first,input_->psoVector.independendPart->d3Dispersion->alpha6.second);
            }

        }

        if (input_->psoVector.independendPart->optDampingFactor)
        {
            m_dampingFactorVector = std::make_shared<PSO1DVector>(1, input_->psoVector.independendPart->dampingFactorRange.first,
                                                    input_->psoVector.independendPart->dampingFactorRange.second);
        }
    }


    return true;
}


std::shared_ptr<PSOVector> DFTBPSOVector::clone()
{
    auto newvec = std::make_shared<DFTBPSOVector>();
    newvec->m_input = this->m_input;
    newvec->m_rootPath = this->m_rootPath;
    newvec->m_userData = this->m_userData;

    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        auto tmp = std::dynamic_pointer_cast<AtomicPSOVector>(this->m_atomicVectors[i]->clone());
        newvec->m_atomicVectors.append(tmp);
    }

    if (this->m_erepfitVector)
    {
        newvec->m_erepfitVector = std::dynamic_pointer_cast<ErepFitPSOVector>(this->m_erepfitVector->clone());
    }

    if (this->m_ljdispersionVector)
    {
        newvec->m_ljdispersionVector = std::dynamic_pointer_cast<LJDispersionPSOVector>(this->m_ljdispersionVector->clone());
    }
    if (this->m_d3dispersionVector)
    {
        newvec->m_d3dispersionVector = std::dynamic_pointer_cast<D3DispersionPSOVector>(this->m_d3dispersionVector->clone());
    }
    if (this->m_dampingFactorVector)
    {
        newvec->m_dampingFactorVector = std::dynamic_pointer_cast<PSO1DVector>(this->m_dampingFactorVector->clone());
    }

    return std::static_pointer_cast<PSOVector>(newvec);
}

int DFTBPSOVector::size() const
{
    int size = 0;
    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        size += this->m_atomicVectors[i]->size();
    }
    if (m_erepfitVector)
    {
        size += m_erepfitVector->size();
    }
    if (this->m_ljdispersionVector)
    {
        size += m_ljdispersionVector->size();
    }
    if (this->m_d3dispersionVector)
    {
        size += m_d3dispersionVector->size();
    }
    if (this->m_dampingFactorVector)
    {
        size += m_dampingFactorVector->size();
    }
    return size;
}

Vector<double> DFTBPSOVector::encode() const
{
    Vector<double> res;
    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        Vector<double> tmp = this->m_atomicVectors[i]->encode();
        res.append(tmp);
    }

    if (m_erepfitVector)
    {
        Vector<double> tmp = this->m_erepfitVector->encode();
        res.append(tmp);
    }

    if (m_ljdispersionVector)
    {
        Vector<double> tmp = this->m_ljdispersionVector->encode();
        res.append(tmp);
    }
    if (m_d3dispersionVector)
    {
        Vector<double> tmp = this->m_d3dispersionVector->encode();
        res.append(tmp);
    }

    if (m_dampingFactorVector)
    {
        Vector<double> tmp = this->m_dampingFactorVector->encode();
        res.append(tmp);
    }

    return res;
}

void DFTBPSOVector::decode(const Vector<double> &vector)
{
    Q_ASSERT_X(vector.size() == this->size(), "AtomicPSOVector::decode", "size inconsistent");
    int index= 0;
    for(int i=0; i<this->m_atomicVectors.size(); ++i)
    {
        Vector<double> vec;
        for(int j=0; j<m_atomicVectors[i]->size(); ++j)
        {
            vec.append(vector[index+j]);
        }
        m_atomicVectors[i]->decode(vec);
        index += (m_atomicVectors[i]->size());
    }
    if (m_erepfitVector)
    {
        Vector<double> vec;
        for(int j=0; j<m_erepfitVector->size(); ++j)
        {
            vec.append(vector[index+j]);
        }

        m_erepfitVector->decode(vec);
        index += m_erepfitVector->size();
    }
    if (m_ljdispersionVector)
    {
        Vector<double> vec;
        for(int j=0; j<m_ljdispersionVector->size(); ++j)
        {
            vec.append(vector[index+j]);
        }

        m_ljdispersionVector->decode(vec);
        index += m_ljdispersionVector->size();
    }
    if (m_d3dispersionVector)
    {
        Vector<double> vec;
        for(int j=0; j<m_d3dispersionVector->size(); ++j)
        {
            vec.append(vector[index+j]);
        }

        m_d3dispersionVector->decode(vec);
        index += m_d3dispersionVector->size();
    }
    if (m_dampingFactorVector)
    {
        Vector<double> vec;
        for(int j=0; j<m_dampingFactorVector->size(); ++j)
        {
            vec.append(vector[index+j]);
        }

        m_dampingFactorVector->decode(vec);
        index += m_dampingFactorVector->size();
    }
}

void DFTBPSOVector::spread(double factor)
{
    for(int i=0; i<this->m_atomicVectors.size(); ++i)
    {
        m_atomicVectors[i]->spread(factor);
    }
    if (m_erepfitVector)
    {
        m_erepfitVector->spread(factor);
    }
    if (m_ljdispersionVector)
    {
        m_ljdispersionVector->spread(factor);
    }
    if (m_d3dispersionVector)
    {
        m_d3dispersionVector->spread(factor);
    }
    if (m_dampingFactorVector)
    {
        m_dampingFactorVector->spread(factor);
    }
}

void DFTBPSOVector::copyfrom(const PSOVector *rhs)
{
    PSOVector::copyfrom(rhs);
    const DFTBPSOVector* real_pos = dynamic_cast<const DFTBPSOVector*>(rhs);
    Q_ASSERT_X(real_pos != nullptr, "DFTBPSOVector::copyfrom", "dynamic_cast failed");
    Q_ASSERT_X(this->m_atomicVectors.size() == real_pos->m_atomicVectors.size(), "DFTBPSOVector::copyfrom", "size inconsistent") ;

    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        this->m_atomicVectors[i]->copyfrom(real_pos->m_atomicVectors[i].get());
        this->m_atomicVectors[i]->copyControllerFrom(real_pos->m_atomicVectors[i].get());
    }
    this->m_userData = real_pos->m_userData;
    this->m_rootPath = real_pos->m_rootPath;
    if (m_erepfitVector)
    {
        this->m_erepfitVector->copyfrom(real_pos->m_erepfitVector.get());
        this->m_erepfitVector->copyControllerFrom(real_pos->m_erepfitVector.get());
    }
    if (m_ljdispersionVector)
    {
        this->m_ljdispersionVector->copyfrom(real_pos->m_ljdispersionVector.get());
        this->m_ljdispersionVector->copyControllerFrom(real_pos->m_ljdispersionVector.get());
    }
    if (m_d3dispersionVector)
    {
        this->m_d3dispersionVector->copyfrom(real_pos->m_d3dispersionVector.get());
        this->m_d3dispersionVector->copyControllerFrom(real_pos->m_d3dispersionVector.get());
    }
    if (m_dampingFactorVector)
    {
        this->m_dampingFactorVector->copyfrom(real_pos->m_dampingFactorVector.get());
        this->m_dampingFactorVector->copyControllerFrom(real_pos->m_dampingFactorVector.get());
    }
}

void DFTBPSOVector::setUserData(int key, const QVariant &value)
{
    m_userData[key] = value;
}

QVariant DFTBPSOVector::getUserData(int key) const
{
    return m_userData[key];
}
DFTBPSOVector& DFTBPSOVector::operator=(const DFTBPSOVector &rhs)
{
    if (this != &rhs)
    {
        this->copyfrom(&rhs);
    }
    return *this;
}

SKBuilder::Input::SKBuilderInput DFTBPSOVector::createSKBuilderInput() const
{
//! TODO rewrite this
    SKBuilder::Input::SKBuilderInput res = *m_input->skbuilder_input;

    for(auto& atomic : m_atomicVectors)
    {
        //confining
        if (!atomic->m_confiningActions.empty())
        {
            SKBuilder::Input::ConfiningInfo cinfo;
            for(auto & action : atomic->m_confiningActions)
            {
                SKBuilder::Input::ConfiningAction caction;
                caction.take = action->m_take;

                for(auto & confining : action->m_confiningVectors)
                {
                    SKBuilder::Input::Confining cconfining;
                    cconfining.parameters = SKBuilder::Input::ConfiningParameterFactory::make(res.toolchain_info.name);
                    auto var = confining->toVariant();
                    cconfining.parameters->fromVariant(var);
                    cconfining.orbital_types = confining->orbitalTypes();
                    caction.confinings.append(cconfining);
                }

                cinfo.confining_actions.append(caction);
            }
            res.confininginfo.insert(atomic->element(), cinfo);
        }

        if (res.atomicinfo.contains(atomic->m_element))
        {
            QMap<QString, double> oes;

            for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
            {
                QString referredOrbital = atomic->m_orbitalEnergyReferredOrbital[j];

                if (referredOrbital == "none")
                {
                    //use referred energy
                    double oe = atomic->m_orbitalEnergyReferredOrbitalEnergy[j] + atomic->m_orbitalEnergyVectors[j]->at(0);
                    oes.insert(atomic->m_orbitalEnergyDefinitions[j].orbital, oe);
                }
            }
            //maximum 8 times linking check: e.g. 4f->4d->4p->4s->3d->3p-3s
            for(int ti = 0; ti<8; ++ti)
            {
                for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
                {
                    if (oes.contains(atomic->m_orbitalEnergyDefinitions[j].orbital))
                    {
                        continue;
                    }
                    QString referredOrbital = atomic->m_orbitalEnergyReferredOrbital[j];

                    if (referredOrbital != "none" && oes.contains(referredOrbital))
                    {
                        //use optimized energy
                        double oe = oes[referredOrbital] + atomic->m_orbitalEnergyVectors[j]->at(0);
                        oes.insert(atomic->m_orbitalEnergyDefinitions[j].orbital, oe);
                    }
                }
            }
            //final check
            for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
            {
                if (!oes.contains(atomic->m_orbitalEnergyDefinitions[j].orbital))
                {
                    qCritical() << "You have a loop in OrbitalEnergy optimization";
                }
            }

            QMapIterator<QString, double> it(oes);
            while(it.hasNext())
            {
                it.next();
                res.atomicinfo[atomic->m_element].orbitalEnergy.insert(it.key(), it.value());
            }


            for(int j=0; j<atomic->m_hubbardVectors.size();++j)
            {
                double hubbard = atomic->m_hubbardVectors[j]->at(0);
                res.atomicinfo[atomic->m_element].hubbard.insert(
                            atomic->m_hubbardDefinitions[j].orbital, hubbard);
            }

        }

    }











//    for(int i=0; i<m_atomicVectors.size();++i)
//    {
//        AtomicPSOVector* atomic = dynamic_cast<AtomicPSOVector*>(m_atomicVectors[i]);
//        if (atomic)
//        {
//            if (!atomic->m_confiningVectors.empty())
//            {
//                SKBuilder::Input::ConfiningInfo cinfo;
//                for(int j=0; j<atomic->m_confiningVectors.size();++j)
//                {
//                    TWNConfiningPotentialPSOVector* conf = dynamic_cast<TWNConfiningPotentialPSOVector*>(atomic->m_confiningVectors[j]);
//                    if (conf)
//                    {
//                        SKBuilder::Input::ConfiningSet co;

//                        if (conf->m_type == TWNConfiningPotentialPSOVector::Density)
//                        {
//                            co.type = "density";
//                        }
//                        else if (conf->m_type == TWNConfiningPotentialPSOVector::TotalPotential)
//                        {
//                            co.type = "totalpotential";
//                        }
//                        else if (conf->m_type == TWNConfiningPotentialPSOVector::CoulombPotential)
//                        {
//                            co.type = "coulombpotential";
//                        }
//                        else
//                        {
//                            co.type = "orbital";
//                            co.orbital_types = conf->m_orbitalTypes;
//                        }
//                        co.w = conf->w->operator [](0);
//                        co.a = conf->a->operator [](0);
//                        co.r = conf->r->operator [](0);
//                        cinfo.confining.append(co);
//                    }
//                }
//                res.confininginfo.insert(atomic->m_element, cinfo);
//            }

//            if (res.atomicinfo.contains(atomic->m_element))
//            {
//                QMap<QString, double> oes;

//                for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
//                {
//                    QString referredOrbital = atomic->m_orbitalEnergyReferredOrbital[j];

//                    if (referredOrbital == "none")
//                    {
//                        //use referred energy
//                        double oe = atomic->m_orbitalEnergyReferredOrbitalEnergy[j] + atomic->m_orbitalEnergyVectors[j]->at(0);
//                        oes.insert(atomic->m_orbitalEnergyDefinitions[j].orbital, oe);
//                    }
//                }
//                //maximum 8 times linking check: e.g. 4f->4d->4p->4s->3d->3p-3s
//                for(int ti = 0; ti<8; ++ti)
//                {
//                    for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
//                    {
//                        if (oes.contains(atomic->m_orbitalEnergyDefinitions[j].orbital))
//                        {
//                            continue;
//                        }
//                        QString referredOrbital = atomic->m_orbitalEnergyReferredOrbital[j];

//                        if (referredOrbital != "none" && oes.contains(referredOrbital))
//                        {
//                            //use optimized energy
//                            double oe = oes[referredOrbital] + atomic->m_orbitalEnergyVectors[j]->at(0);
//                            oes.insert(atomic->m_orbitalEnergyDefinitions[j].orbital, oe);
//                        }
//                    }
//                }
//                //final check
//                for(int j=0; j< atomic->m_orbitalEnergyVectors.size();++j)
//                {
//                    if (!oes.contains(atomic->m_orbitalEnergyDefinitions[j].orbital))
//                    {
//                        qCritical() << "You have a loop in OrbitalEnergy optimization";
//                    }
//                }

//                QMapIterator<QString, double> it(oes);
//                while(it.hasNext())
//                {
//                    it.next();
//                    res.atomicinfo[atomic->m_element].orbitalEnergy.insert(it.key(), it.value());
//                }


//                for(int j=0; j<atomic->m_hubbardVectors.size();++j)
//                {
//                    double hubbard = atomic->m_hubbardVectors[j]->at(0);
//                    res.atomicinfo[atomic->m_element].hubbard.insert(
//                                atomic->m_hubbardDefinitions[j].orbital, hubbard);
//                }

//            }

//        }
//    }
    return res;
}

QMap<QString, double> DFTBPSOVector::getHubbardDerivs() const
{
    QMap<QString, double> res;
    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        QString elem = m_atomicVectors.at(i)->element();
        if (m_atomicVectors.at(i)->hasHubbardDerivs())
        {
            double hubbardDeriv = m_atomicVectors.at(i)->getHubbardDerivs();
            res.insert(elem, hubbardDeriv);
        }
    }
    return res;
}
std::shared_ptr<ErepFitPSOVector> DFTBPSOVector::erepfitVector() const
{
    return m_erepfitVector;
}

std::shared_ptr<LJDispersionPSOVector> DFTBPSOVector::ljdispersionVector() const
{
    return m_ljdispersionVector;
}

std::shared_ptr<D3DispersionPSOVector> DFTBPSOVector::d3dispersionVector() const
{
    return m_d3dispersionVector;
}

std::shared_ptr<PSO1DVector> DFTBPSOVector::dampingFactorVector() const
{
    return m_dampingFactorVector;
}

bool DFTBPSOVector::hasAtomicVector() const
{
    return !m_atomicVectors.isEmpty();
}

bool DFTBPSOVector::hasOrbitalEnergyVector() const
{
    return false;
}

bool DFTBPSOVector::hasHubbardVector() const
{
    return false;
}

bool DFTBPSOVector::hasHubbardDerivativesVector() const
{
    return false;
}

bool DFTBPSOVector::hasErepfitVector() const
{
    return (m_erepfitVector != nullptr);
}

bool DFTBPSOVector::hasLJDispersionVector() const
{
    return (m_ljdispersionVector != nullptr);
}

bool DFTBPSOVector::hasD3DispersionVector() const
{
    return (m_d3dispersionVector != nullptr);
}

bool DFTBPSOVector::hasDampingFactorVector() const
{
    return (m_dampingFactorVector != nullptr);
}


QString DFTBPSOVector::getRootPath() const
{
    return m_rootPath;
}

void DFTBPSOVector::setRootPath(const QString &value) const
{
    m_rootPath = value;
}

void DFTBPSOVector::randomizePosition()
{
    for(int i=0; i<m_atomicVectors.size(); ++i)
    {
        m_atomicVectors[i]->randomizePosition();
    }

    if (m_erepfitVector)
    {
        m_erepfitVector->randomizePosition();
    }

    if (m_ljdispersionVector)
    {
        m_ljdispersionVector->randomizePosition();
    }
    if (m_d3dispersionVector)
    {
        m_d3dispersionVector->randomizePosition();
    }
    if (m_dampingFactorVector)
    {
        m_dampingFactorVector->randomizePosition();
    }
}

void DFTBPSOVector::randomizeVelocity()
{
    for(int i=0; i<m_atomicVectors.size(); ++i)
    {
        m_atomicVectors[i]->randomizeVelocity();
    }

    if (m_erepfitVector)
    {
        m_erepfitVector->randomizeVelocity();
    }

    if (m_ljdispersionVector)
    {
        m_ljdispersionVector->randomizeVelocity();
    }
    if (m_d3dispersionVector)
    {
        m_d3dispersionVector->randomizeVelocity();
    }
    if (m_dampingFactorVector)
    {
        m_dampingFactorVector->randomizeVelocity();
    }
}

void DFTBPSOVector::stablizeVelocity()
{
    for(int i=0; i<m_atomicVectors.size(); ++i)
    {
        m_atomicVectors[i]->stablizeVelocity();
    }

    if (m_erepfitVector)
    {
        m_erepfitVector->stablizeVelocity();
    }
    if (m_ljdispersionVector)
    {
        m_ljdispersionVector->stablizeVelocity();
    }
    if (m_d3dispersionVector)
    {
        m_d3dispersionVector->stablizeVelocity();
    }
    if (m_dampingFactorVector)
    {
        m_dampingFactorVector->stablizeVelocity();
    }
}

void DFTBPSOVector::stablizePosition(libPSO::PSOVector *vel)
{
    DFTBPSOVector* real_vel = dynamic_cast<DFTBPSOVector*>(vel);
    Q_ASSERT_X(real_vel != nullptr, "DFTBPSOVector::stablizePosition", "dynamic_cast failed");

    if (real_vel)
    {
        for(int i=0; i<m_atomicVectors.size(); ++i)
        {
            m_atomicVectors[i]->stablizePosition(real_vel->m_atomicVectors[i].get());
        }
    }

    if (m_erepfitVector)
    {
        m_erepfitVector->stablizePosition(real_vel->m_erepfitVector.get());
    }
    if (m_ljdispersionVector)
    {
        m_ljdispersionVector->stablizePosition(real_vel->m_ljdispersionVector.get());
    }
    if (m_d3dispersionVector)
    {
        m_d3dispersionVector->stablizePosition(real_vel->m_d3dispersionVector.get());
    }
    if (m_dampingFactorVector)
    {
        m_dampingFactorVector->stablizePosition(real_vel->m_dampingFactorVector.get());
    }
}

void DFTBPSOVector::copyControllerFrom(const libPSO::PSOVector *rhs)
{
    const DFTBPSOVector* vector = dynamic_cast<const DFTBPSOVector*>(rhs);
    Q_ASSERT_X(vector != nullptr, "DFTBPSOVector::copyControllerFrom", "dynamic_cast failed");
    Q_ASSERT_X(vector->m_atomicVectors.size() == m_atomicVectors.size() , "DFTBPSOVector::copyControllerFrom", "size inconsistent");

    if (vector)
    {
        for(int i=0; i<m_atomicVectors.size(); ++i)
        {
            m_atomicVectors[i]->copyControllerFrom(vector->m_atomicVectors[i].get());
        }
    }

    if (m_erepfitVector && vector->m_erepfitVector )
    {
        m_erepfitVector->copyControllerFrom(vector->m_erepfitVector.get());
    }

    if (m_ljdispersionVector && vector->m_ljdispersionVector )
    {
        m_ljdispersionVector->copyControllerFrom(vector->m_ljdispersionVector.get());
    }
    if (m_d3dispersionVector && vector->m_d3dispersionVector )
    {
        m_d3dispersionVector->copyControllerFrom(vector->m_d3dispersionVector.get());
    }
    if (m_dampingFactorVector && vector->m_dampingFactorVector )
    {
        m_dampingFactorVector->copyControllerFrom(vector->m_dampingFactorVector.get());
    }

}

double DFTBPSOVector::getLowerBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_atomicVectors[i]->size()))
        {
            return m_atomicVectors[i]->getLowerBound(index-curIndex);
        }
        curIndex += m_atomicVectors[i]->size();
    }
    if (m_erepfitVector)
    {
        if (index >= curIndex && index < (curIndex+m_erepfitVector->size()))
        {
            return m_erepfitVector->getLowerBound(index-curIndex);
        }
        curIndex += m_erepfitVector->size();
    }
    if (m_ljdispersionVector)
    {
        if (index >= curIndex && index < (curIndex+m_ljdispersionVector->size()))
        {
            return m_ljdispersionVector->getLowerBound(index-curIndex);
        }
        curIndex += m_ljdispersionVector->size();
    }
    if (m_d3dispersionVector)
    {
        if (index >= curIndex && index < (curIndex+m_d3dispersionVector->size()))
        {
            return m_d3dispersionVector->getLowerBound(index-curIndex);
        }
        curIndex += m_d3dispersionVector->size();
    }
    if (m_dampingFactorVector)
    {
        if (index >= curIndex && index < (curIndex+m_dampingFactorVector->size()))
        {
            return m_dampingFactorVector->getLowerBound(index-curIndex);
        }
        curIndex += m_dampingFactorVector->size();
    }
    throw std::invalid_argument("DFTBPSOVector::getLowerBound");
}

double DFTBPSOVector::getUpperBound(int index)
{
    int curIndex = 0;
    for(int i=0; i<this->m_atomicVectors.size();++i)
    {
        if (index >= curIndex && index < (curIndex+m_atomicVectors[i]->size()))
        {
            return m_atomicVectors[i]->getUpperBound(index-curIndex);
        }
        curIndex += m_atomicVectors[i]->size();
    }
    if (m_erepfitVector)
    {
        if (index >= curIndex && index < (curIndex+m_erepfitVector->size()))
        {
            return m_erepfitVector->getUpperBound(index-curIndex);
        }
        curIndex += m_erepfitVector->size();
    }
    if (m_ljdispersionVector)
    {
        if (index >= curIndex && index < (curIndex+m_ljdispersionVector->size()))
        {
            return m_ljdispersionVector->getUpperBound(index-curIndex);
        }
        curIndex += m_ljdispersionVector->size();
    }
    if (m_d3dispersionVector)
    {
        if (index >= curIndex && index < (curIndex+m_d3dispersionVector->size()))
        {
            return m_d3dispersionVector->getUpperBound(index-curIndex);
        }
        curIndex += m_d3dispersionVector->size();
    }
    if (m_dampingFactorVector)
    {
        if (index >= curIndex && index < (curIndex+m_dampingFactorVector->size()))
        {
            return m_dampingFactorVector->getUpperBound(index-curIndex);
        }
        curIndex += m_dampingFactorVector->size();
    }
    throw std::invalid_argument("DFTBPSOVector::getUpperBound");
}

}




