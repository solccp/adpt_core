#include "sopso_functions.h"

#include "psoalgorithm.h"
#include "simplepsoalgorithm.h"
#include "psopopulation.h"

#include "dftbpsovector.h"

#include "typesettings.h"
#include "utils.h"

#include "dftbpsovector_variant.h"

#include <fstream>

#include <QTextStream>
#include <QStringList>

#include "pso_functions.h"
#include <Variant/Variant.h>
#include "variantqt.h"


namespace DFTBPSO
{

using namespace libPSO;



void sopso_progressUpdater(const std::shared_ptr<const libPSO::PSOAlgorithm>& pso, int iter)
{

    QTextStream stdout_stream(stdout);


    if (iter == 0)
    {
        stdout_stream << endl
                  << STARLINE << endl
                  << centerString("Optimization Started") << endl
                  << HYPHENLINE << endl;
    }
    else
    {

        auto mypso = std::dynamic_pointer_cast<const SimplePSOAlgorithm>(pso);
        if (!mypso)
        {
            return;
        }
        auto pop = pso->getPopulation();
        auto vector = std::dynamic_pointer_cast<const DFTBPSOVector>(mypso->getBestPosition());
        PSOFitnessValue bestValues = mypso->getBestValue();
        QStringList bestValuesStr;
        for(int i=0; i<bestValues.size();++i)
        {
            bestValuesStr.append(QString("%1").arg(bestValues[i],20, 'F', 12).trimmed());
        }

        stdout_stream << "Iteration " << iter << " : " << bestValuesStr.join(", ");
        if (mypso->globalBestUpdated())
        {
            stdout_stream << " (updated)" << endl;
        }
        else
        {
            stdout_stream << endl;
        }

        for(int i=0; i< pop->numOfParticles(); ++i)
        {
            if (pop->particlePosition(i)->isFailed())
            {
                stdout_stream << INDENT_SPACES << QString("Particle #%1").arg(i+1,-5) << ": " << "Infeasible (Reason: " << pop->particlePosition(i)->getFailedReason() << ")" << endl;
            }
            else
            {
                PSOFitnessValue values = pop->fitness(i);
                QStringList valuesStr;
                for(int i=0; i<values.size();++i)
                {
                    valuesStr.append(QString("%1").arg(values[i],20, 'F', 12).trimmed());
                }
                stdout_stream << INDENT_SPACES << QString("Particle #%1").arg(i+1,-5) << ": " << valuesStr.join(", ") << endl;
            }
        }

        stdout_stream << endl;


        stdout_stream << INDENT_SPACES << "# of infeasible particles: " << pop->getNumOfInfeasible() << endl;

        if ( pop->getNumOfInfeasible() < pop->numOfParticles() )
        {

            PSOFitnessValue avgValues = pop->getAvgFitness();
            QStringList avgValuesStr;
            for(int i=0; i<avgValues.size();++i)
            {
                avgValuesStr.append(QString("%1").arg(avgValues[i],20, 'F', 12).trimmed());
            }


            stdout_stream << INDENT_SPACES << "Avarage fitness: " << avgValuesStr.join(", ") << endl;
        }
        else
        {
            stdout_stream << INDENT_SPACES << "Avarage fitness: " << "N/A" << endl;
        }
        stdout_stream << HYPHENLINE << endl;


        if (mypso->globalBestUpdated())
        {

            QDir dir(QDir::current().absoluteFilePath("best"));
            if (QDir(vector->getRootPath()).exists())
            {
                dir.removeRecursively();
                if (!dir.exists())
                {
                    dir.mkpath(dir.absolutePath());
                }

                copyPath(vector->getRootPath(), dir.absolutePath() );
            }

            if (!dir.exists())
            {
                dir.mkpath(dir.absolutePath());
            }

            {
                libvariant::Variant var = libvariant::VariantConvertor<DFTBPSOVector>::toVariant(*vector);
                libvariant::SerializeYAML(dir.absoluteFilePath("best_parameters.yaml").toStdString().c_str(), var);
                libvariant::SerializeJSON(dir.absoluteFilePath("best_parameters.json").toStdString().c_str(), var, true);
            }
        }
    }
}

PSOFitnessValue sopso_particle_evaluation(const std::shared_ptr<PSOVector>& vector)
{
    return pso_particle_evaluation(vector, false);
}


}

