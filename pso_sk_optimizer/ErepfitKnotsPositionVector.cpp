#include "ErepfitKnotsPositionVector.h"
#include "Singleton.h"
#include "RandomNumberGenerator.h"
#include <QDebug>

ErepfitKnotsPositionVector::ErepfitKnotsPositionVector(int size, double min, double max)
    : libPSO::PSO1DVector(size, min, max)
{
     double rangesize = this->getPosMax() - this->getPosMin();
     m_interval = rangesize / static_cast<double>(size+1);
     m_init_knot_position.resize(size);
     for(int i=0; i<size; ++i)
     {
         m_init_knot_position[i] = this->getPosMin()+m_interval*(i+1);
     }
     adjustVelocityRanges();
}

ErepfitKnotsPositionVector::ErepfitKnotsPositionVector(const ErepfitKnotsPositionVector& rhs)
    : libPSO::PSO1DVector(rhs.m_data.size(), rhs.m_posmin, rhs.m_posmax)
{
    m_interval = rhs.m_interval;
    m_init_knot_position = rhs.m_init_knot_position;
}

void ErepfitKnotsPositionVector::ensureSeparation()
{
    if (m_data.empty())
        return;

    const double minimum = 0.01;
    bool somePointMoved = false;

    int times = 0;
    do
    {
        somePointMoved = false;
        times++;
        if (times > 100)
            break;
        double& first = m_data.first();
        if ((first - this->getPosMin()) < minimum)
        {
            first = this->getPosMin() + minimum;
            somePointMoved = true;
        }

        double& last = m_data.last();
        if ((this->getPosMax() - last) < minimum)
        {
            last = this->getPosMax() - minimum;
            somePointMoved = true;
        }

//        if (m_data.size() >= 2)
//        {
//            for(int i=0; i<m_data.size()-1;++i)
//            {
////                if ( (m_data[i]-m_data[i-1]) < minimum )
////                {
////                    m_data[i-1] -= (minimum / 2.0);
////                    m_data[i] += (minimum / 2.0);
////                    somePointMoved = true;
////                }
//                if (m_data[i] > m_data[i+1])
//                {
//                    std::swap(m_data[i], m_data[i+1]);
//                    somePointMoved = true;
//                }
//                else if ( (m_data[i+1]-m_data[i]) < minimum )
//                {
//                    m_data[i] -= (minimum / 2.0);
//                    m_data[i+1] += (minimum / 2.0);
//                    qDebug() << "Too close knots...moved";
//                    somePointMoved = true;
//                }
//            }
//        }
    }while(somePointMoved);
}

void ErepfitKnotsPositionVector::sortKnots(ErepfitKnotsPositionVector* pos, ErepfitKnotsPositionVector* vel)
{
    for(int i=0; i<pos->m_data.size()-1;++i)
    {
        for(int j=0; j<pos->m_data.size()-1-i; ++j)
        {
            if (pos->m_data[i] > pos->m_data[i+1])
            {
                std::swap(pos->m_data[i], pos->m_data[i+1]);
                std::swap(vel->m_data[i], vel->m_data[i+1]);
            }
        }
    }
}

void ErepfitKnotsPositionVector::randomizePosition()
{
    libPSO::RandomNumberRenerator<double>& gen = Singleton<libPSO::RandomNumberRenerator<double> >::Instance();
    for(int i=0; i< size(); ++i)
    {
        m_data[i]= m_init_knot_position[i] + gen.rand_real(-m_interval*0.5, m_interval*0.5);
    }
    if (size() > 0)
    {
        ensureSeparation();
    }
}

void ErepfitKnotsPositionVector::stablizePosition(libPSO::PSOVector *vel)
{
    bool hasVel = true;
    ErepfitKnotsPositionVector* vel_vector = nullptr;
    if (vel == nullptr)
    {
        hasVel = false;
    }
    else
    {
        vel_vector = dynamic_cast<ErepfitKnotsPositionVector*>(vel);
        Q_ASSERT_X(vel_vector!=NULL, "ErepfitKnotsPositionVector::stablizePosition", "Vector is not ErepfitKnotsPositionVector");
    }

    for(int i=0; i< m_data.size(); ++i)
    {
        if (m_data[i] > m_posmax)
        {
            m_data[i] = m_posmax;
            double tmp = m_data[i] - m_posmax;
            m_data[i] = m_posmax - tmp*0.1;

            if (hasVel)
            {
                vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
            }
        }
        else if (m_data[i] < m_posmin)
        {
            m_data[i] = m_posmin;
            double tmp = m_posmin - m_data[i];
            m_data[i] = m_posmin + 0.1*tmp;

            if (hasVel)
            {
                vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
            }
        }
    }



    sortKnots(this, vel_vector);

    ensureSeparation();


    const double minimum = 0.01;
    libPSO::RandomNumberRenerator<double>& gen = Singleton<libPSO::RandomNumberRenerator<double> >::Instance();

    bool somePointMoved = false;
    do
    {
        somePointMoved = false;
        if (m_data.size() >= 2)
        {
            for(int i=0; i<m_data.size()-1;++i)
            {
                if ( std::abs(m_data[i+1]-m_data[i]) < minimum )
                {
                    qDebug() << "Too close knots...combining";
                    m_data[i] = gen.rand_real(this->m_posmin, this->m_posmax);
                    vel_vector->m_data[i] = gen.rand_real(this->m_velmin, this->m_velmax);
                    somePointMoved = true;
                }
            }
        }
        sortKnots(this, vel_vector);
    }while(somePointMoved);



}


void ErepfitKnotsPositionVector::adjustVelocityRanges()
{
    m_velmin = -m_interval*0.2;
    m_velmax  = -m_velmin;
}


double ErepfitKnotsPositionVector::getLowerBound(int index)
{
    return m_init_knot_position[index]-m_interval;
}

double ErepfitKnotsPositionVector::getUpperBound(int index)
{
    return m_init_knot_position[index]+m_interval;
}


std::shared_ptr<libPSO::PSOVector> ErepfitKnotsPositionVector::clone()
{
    auto res = std::make_shared<ErepfitKnotsPositionVector>(*this);
    return res;
}

void ErepfitKnotsPositionVector::decode(const libPSO::Vector<double> &vector)
{
    PSO1DVector::decode(vector);
}

