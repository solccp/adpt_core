#include "AggressiveErepfitKnotsPositionVector.h"
#include "Singleton.h"
#include "RandomNumberGenerator.h"
#include <QDebug>

AggressiveErepfitKnotsPositionVector::AggressiveErepfitKnotsPositionVector(int size, double min, double max)
    : libPSO::PSO1DVector(size, min, max)
{
     adjustVelocityRanges();
     qDebug() << "AggressiveErepfitKnotsPositionVector::ctor" << this;
}

AggressiveErepfitKnotsPositionVector::AggressiveErepfitKnotsPositionVector(const AggressiveErepfitKnotsPositionVector& rhs)
    : libPSO::PSO1DVector(rhs.m_data.size(), rhs.m_posmin, rhs.m_posmax)
{
    qDebug() << "AggressiveErepfitKnotsPositionVector::ctor" << this;
}

void AggressiveErepfitKnotsPositionVector::ensureSeparation()
{
    if (m_data.empty())
        return;

    const double minimum = 0.01;
    bool somePointMoved = false;

    int times = 0;
    do
    {
        somePointMoved = false;
        times++;
        if (times > 100)
            break;
        double& first = m_data.first();
        if ((first - this->getPosMin()) < minimum)
        {
            first = this->getPosMin() + minimum;
            somePointMoved = true;
        }

        double& last = m_data.last();
        if ((this->getPosMax() - last) < minimum)
        {
            last = this->getPosMax() - minimum;
            somePointMoved = true;
        }

//        if (m_data.size() >= 2)
//        {
//            for(int i=0; i<m_data.size()-1;++i)
//            {
////                if ( (m_data[i]-m_data[i-1]) < minimum )
////                {
////                    m_data[i-1] -= (minimum / 2.0);
////                    m_data[i] += (minimum / 2.0);
////                    somePointMoved = true;
////                }
//                if (m_data[i] > m_data[i+1])
//                {
//                    std::swap(m_data[i], m_data[i+1]);
//                    somePointMoved = true;
//                }
//                else if ( (m_data[i+1]-m_data[i]) < minimum )
//                {
//                    m_data[i] -= (minimum / 2.0);
//                    m_data[i+1] += (minimum / 2.0);
//                    qDebug() << "Too close knots...moved";
//                    somePointMoved = true;
//                }
//            }
//        }
    }while(somePointMoved);
}

void AggressiveErepfitKnotsPositionVector::sortKnots(AggressiveErepfitKnotsPositionVector* pos, AggressiveErepfitKnotsPositionVector* vel)
{
    for(int i=0; i<pos->m_data.size()-1;++i)
    {
        for(int j=0; j<pos->m_data.size()-1-i; ++j)
        {
            if (pos->m_data[i] > pos->m_data[i+1])
            {
                std::swap(pos->m_data[i], pos->m_data[i+1]);
                std::swap(vel->m_data[i], vel->m_data[i+1]);
            }
        }
    }
}

void AggressiveErepfitKnotsPositionVector::randomizePosition()
{
    libPSO::RandomNumberRenerator<double>& gen = Singleton<libPSO::RandomNumberRenerator<double> >::Instance();
    for(int i=0; i< size(); ++i)
    {
        m_data[i]= gen.rand_real(m_posmin, m_posmax);
    }
    if (size() > 0)
    {
        ensureSeparation();
    }
}

void AggressiveErepfitKnotsPositionVector::stablizePosition(libPSO::PSOVector *vel)
{
    bool hasVel = true;
    AggressiveErepfitKnotsPositionVector* vel_vector = nullptr;
    if (vel == nullptr)
    {
        hasVel = false;
    }
    else
    {
        vel_vector = dynamic_cast<AggressiveErepfitKnotsPositionVector*>(vel);
        Q_ASSERT_X(vel_vector!=NULL, "AggressiveErepfitKnotsPositionVector::stablizePosition", "Vector is not AggressiveErepfitKnotsPositionVector");
    }

    for(int i=0; i< m_data.size(); ++i)
    {
        if (m_data[i] > m_posmax)
        {
            m_data[i] = m_posmax;
            double tmp = m_data[i] - m_posmax;
            m_data[i] = m_posmax - tmp*0.1;

            if (hasVel)
            {
                vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
            }
        }
        else if (m_data[i] < m_posmin)
        {
            m_data[i] = m_posmin;
            double tmp = m_posmin - m_data[i];
            m_data[i] = m_posmin + 0.1*tmp;

            if (hasVel)
            {
                vel_vector->m_data[i] = -vel_vector->m_data[i]*0.1;
            }
        }
    }



    sortKnots(this, vel_vector);

    ensureSeparation();


    const double minimum = 0.01;
    libPSO::RandomNumberRenerator<double>& gen = Singleton<libPSO::RandomNumberRenerator<double> >::Instance();

    bool somePointMoved = false;
    do
    {
        somePointMoved = false;
        if (m_data.size() >= 2)
        {
            for(int i=0; i<m_data.size()-1;++i)
            {
                if ( std::abs(m_data[i+1]-m_data[i]) < minimum )
                {
                    qDebug() << "Too close knots...combining";
                    m_data[i] = gen.rand_real(this->m_posmin, this->m_posmax);
                    vel_vector->m_data[i] = gen.rand_real(this->m_velmin, this->m_velmax);
                    somePointMoved = true;
                }
            }
        }
        sortKnots(this, vel_vector);
    }while(somePointMoved);



}


void AggressiveErepfitKnotsPositionVector::adjustVelocityRanges()
{
    m_velmin = std::abs(m_posmax-m_posmin)*0.1;
    m_velmax  = -m_velmin;
}


double AggressiveErepfitKnotsPositionVector::getLowerBound(int index)
{
    return m_posmin;
}

double AggressiveErepfitKnotsPositionVector::getUpperBound(int index)
{
    return m_posmax;
}


std::shared_ptr<libPSO::PSOVector> AggressiveErepfitKnotsPositionVector::clone()
{
    auto res = std::make_shared<AggressiveErepfitKnotsPositionVector>(*this);
    return res;
}

void AggressiveErepfitKnotsPositionVector::decode(const libPSO::Vector<double> &vector)
{
    PSO1DVector::decode(vector);
}

