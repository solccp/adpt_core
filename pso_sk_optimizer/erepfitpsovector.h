#ifndef EREPFITPSOVECTOR_H
#define EREPFITPSOVECTOR_H

#include "pso1dvector.h"

#include <QList>
#include <QPair>
#include <memory>
#include "erepfit/splinetool.h"

#include "adpt/input.h"

namespace DFTBPSO
{
    using namespace libPSO;
    class ErepFitPSOVector : public PSOVector
    {
    public:
        friend class libvariant::VariantConvertor<DFTBPSO::ErepFitPSOVector>;
        ErepFitPSOVector();
        void define(const DFTBPSO::Input::ErepfitPSOVector* inputData);

        //from PSOVector
        virtual std::shared_ptr<PSOVector> clone() override;
        virtual int size() const;
        virtual Vector<double> encode() const;
        virtual void decode(const Vector<double>& vector);
        virtual void copyfrom(const PSOVector* rhs);
        QList<QPair<QString, QString> > getPairSymbols() const;

        QList<std::shared_ptr<PSO1DVector> > getPotentials() const;
        QList<std::shared_ptr<PSOVector>> getAdditionalEquations() const;

        const QList<DFTBPSO::Input::ErepfitAdditionalEquation>& getAdditionalEquationInfo() const;

        DFTBPSO::Input::ErepfitPSOVector m_inputData;


        QMap<QPair<QString, QString>, Erepfit::Spline4> repulsivePotentials;
//        std::vector<std::tuple<QString, QString, Erepfit::Spline4> > repulsivePotentials;

        virtual ~ErepFitPSOVector();
        virtual void spread(double factor);

        ErepFitPSOVector& operator=(const ErepFitPSOVector& rhs);


    private:
        QList<QPair<QString, QString> > m_pairSymbols;
        QList<std::shared_ptr<PSO1DVector>> m_potentialsVector;
        QList<std::shared_ptr<PSOVector>> m_additionalEquationVector;

        // PSOVector interface
    public:
        void randomizePosition();
        void randomizeVelocity();
        void stablizeVelocity();
        void stablizePosition(PSOVector *vel);
        void copyControllerFrom(const PSOVector *rhs);

        // PSOVector interface
    public:
        double getLowerBound(int index);
        double getUpperBound(int index);
    };
}





#endif // EREPFITPSOVECTOR_H
