#ifndef ATOMICPSOVECTOR_YAML_H
#define ATOMICPSOVECTOR_YAML_H


#include <atomicpsovector.h>

#include "twnconfiningpotentialpsovector.h"
#include "twnconfiningpotentialpsovector_yaml.h"

#include <yaml-cpp/yaml.h>


#include "yaml_node.h"
#include "yaml_qttypes.h"
#include "yaml_utils.h"


namespace YAML
{

    template<>
    struct convert<DFTBPSO::AtomicPSOVector>
    {

        static Node encode(const DFTBPSO::AtomicPSOVector& rhs)
        {
            Node node;

            for(int i=0; i<rhs.m_confiningVectors.size();++i)
            {
                Node subnode;
                DFTBPSO::TWNConfiningPotentialPSOVector *vec = dynamic_cast<DFTBPSO::TWNConfiningPotentialPSOVector*>(rhs.m_confiningVectors[i]);
                if (vec)
                {
                    subnode = (*vec);
                }
                node["Confining"].push_back(subnode);
            }

            for(int i=0; i<rhs.m_orbitalEnergyVectors.size();++i)
            {
                Node subnode;
                subnode["Orbital"] = rhs.m_orbitalEnergyDefinitions[i].orbital;
                subnode["ReferredOrbital"] = rhs.m_orbitalEnergyDefinitions[i].referredOrbital;
                subnode["Range"] = rhs.m_orbitalEnergyDefinitions[i].range;
                subnode["Value"] = rhs.m_orbitalEnergyVectors[i]->at(0);
                subnode["RealOrbitalEnergy"] = rhs.m_orbitalEnergyVectors[i]->at(0)+rhs.m_orbitalEnergyReferredOrbitalEnergy[i];
                node["OrbitalEnergy"].push_back(subnode);
            }

            for(int i=0; i<rhs.m_hubbardVectors.size();++i)
            {
                Node subnode;
                subnode["Orbital"] = rhs.m_hubbardDefinitions[i].orbital;
                subnode["Range"] = rhs.m_hubbardDefinitions[i].range;
                subnode["Value"] = rhs.m_hubbardVectors[i]->at(0);
                node["Hubbard"].push_back(subnode);
            }

            for(int i=0; i<rhs.m_hubbardDerivsVectors.size();++i)
            {
                Node subnode;
                subnode["Orbital"] = rhs.m_hubbardDerivsDefinitions[i].orbital;
                subnode["Range"] = rhs.m_hubbardDerivsDefinitions[i].range;
                subnode["Value"] = rhs.m_hubbardDerivsVectors[i]->at(0);
                node["HubbardDerivs"].push_back(subnode);
            }


            return node;
        }
        static bool decode(const Node& node, DFTBPSO::AtomicPSOVector& rhs)
        {
            if (!node.IsDefined())
            {
                return false;
            }

            if (node["Confining"].IsDefined() && node["Confining"].IsSequence())
            {
                for(std::size_t i=0; i<node["Confining"].size();++i)
                {
                    Node subnode = node["Confining"][i];
                    QString type = subnode["Type"].as<QString>();
                    if (type.toLower() == "orbital")
                    {
                        QStringList orbitals = subnode["OrbitalTypes"].as<QStringList>();

                        for(int j=0; j<rhs.m_confiningVectors.size();++j)
                        {
                            if (rhs.m_confiningVectors[j]->confiningType() == DFTBPSO::TWNConfiningPotentialPSOVector::Orbital
                                    && rhs.m_confiningVectors[j]->orbitalTypes().toSet() == orbitals.toSet())
                            {
                                update_node(subnode, *rhs.m_confiningVectors[j]);
                                break;
                            }
                        }
                    }
                    else if (type.toLower() == "totalpotential")
                    {
                        for(int j=0; j<rhs.m_confiningVectors.size();++j)
                        {
                            if (rhs.m_confiningVectors[j]->confiningType() == DFTBPSO::TWNConfiningPotentialPSOVector::TotalPotential)
                            {
                                update_node(subnode, *rhs.m_confiningVectors[j]);
                                break;
                            }
                        }
                    }
                    else if (type.toLower() == "coulombpotential")
                    {
                        for(int j=0; j<rhs.m_confiningVectors.size();++j)
                        {
                            if (rhs.m_confiningVectors[j]->confiningType() == DFTBPSO::TWNConfiningPotentialPSOVector::CoulombPotential)
                            {
                                update_node(subnode, *rhs.m_confiningVectors[j]);
                                break;
                            }
                        }
                    }
                    else if (type.toLower() == "density")
                    {
                        for(int j=0; j<rhs.m_confiningVectors.size();++j)
                        {
                            if (rhs.m_confiningVectors[j]->confiningType() == DFTBPSO::TWNConfiningPotentialPSOVector::Density)
                            {
                                update_node(subnode, *rhs.m_confiningVectors[j]);
                                break;
                            }
                        }
                    }
                    else
                    {
                        qDebug() << "Unknown confining type: " << type << ", skipping.";
                        continue;
                    }
                }
            }
            if (node["OrbitalEnergy"].IsDefined() && node["OrbitalEnergy"].IsSequence())
            {
                for(std::size_t j=0; j< node["OrbitalEnergy"].size();++j)
                {
                    Node subnode = node["OrbitalEnergy"][j];
                    QString orb = subnode["Orbital"].as<QString>();
                    QString refOrb = subnode["ReferredOrbital"].as<QString>();
                    double value = subnode["Value"].as<double>();

                    for(int i=0; i<rhs.m_orbitalEnergyDefinitions.size();++i)
                    {
                        if (rhs.m_orbitalEnergyDefinitions.at(i).orbital == orb && rhs.m_orbitalEnergyDefinitions.at(i).referredOrbital == refOrb)
                        {
                            rhs.m_orbitalEnergyVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }

            if (node["Hubbard"].IsDefined() && node["Hubbard"].IsSequence())
            {
                for(std::size_t j=0; j< node["Hubbard"].size();++j)
                {
                    Node subnode = node["Hubbard"][j];
                    QString orb = subnode["Orbital"].as<QString>();
                    double value = subnode["Value"].as<double>();

                    for(int i=0; i<rhs.m_hubbardDefinitions.size();++i)
                    {
                        if (rhs.m_hubbardDefinitions.at(i).orbital == orb)
                        {
                            rhs.m_hubbardVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }

            if (node["HubbardDerivs"].IsDefined() && node["HubbardDerivs"].IsSequence())
            {
                for(std::size_t j=0; j< node["HubbardDerivs"].size();++j)
                {
                    Node subnode = node["HubbardDerivs"][j];
                    QString orb = subnode["Orbital"].as<QString>();
                    double value = subnode["Value"].as<double>();

                    for(int i=0; i<rhs.m_hubbardDerivsDefinitions.size();++i)
                    {
                        if (rhs.m_hubbardDerivsDefinitions.at(i).orbital == orb)
                        {
                            rhs.m_hubbardDerivsVectors[i]->operator [](0) = value;
                        }
                    }
                }

            }



            return true;
        }
    };


}

#endif // ATOMICPSOVECTOR_YAML_H
