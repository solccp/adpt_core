#ifndef CONFININGPOTENTIALPSOVECTOR_H
#define CONFININGPOTENTIALPSOVECTOR_H

#include "psovector.h"
#include <Variant/Variant.h>
#include <QStringList>

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

namespace DFTBPSO
{
    using namespace libPSO;
    class ConfiningPotentialPSOVector : public PSOVector
    {
        friend class AtomicPSOVector;
        friend class libvariant::VariantConvertor<ConfiningPotentialPSOVector>;
    public:
        ConfiningPotentialPSOVector(){}
        virtual ~ConfiningPotentialPSOVector(){}

        virtual void fromVariant(const libvariant::Variant& var) ;
        virtual libvariant::Variant toVariant() const ;
        QStringList orbitalTypes() const;
        void setOrbitalTypes(const QStringList &orbitalTypes);

    protected:
        QStringList m_orbitalTypes;
    };
}

namespace libvariant
{
    template<>
    struct VariantConvertor<DFTBPSO::ConfiningPotentialPSOVector>
    {
        static Variant toVariant(const DFTBPSO::ConfiningPotentialPSOVector &rhs)
        {
            Variant node = rhs.toVariant();
            return node;
        }
        static void fromVariant(const Variant &node, DFTBPSO::ConfiningPotentialPSOVector &rhs)
        {
            rhs.fromVariant(node);
        }
    };

}
#endif // CONFININGPOTENTIALPSOVECTOR_H

