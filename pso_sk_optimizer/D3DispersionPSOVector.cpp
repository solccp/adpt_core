#include "D3DispersionPSOVector.h"

#include "pso1dvector.h"
#include "vector.h"

#include <QDebug>
#include <stdexcept>


namespace DFTBPSO
{
D3DispersionPSOVector::D3DispersionPSOVector(const QString& damping)
{
    setDamping(damping);


    m_s6 = std::make_shared<PSO1DVector>(1, 0, 0);
    m_s8 = std::make_shared<PSO1DVector>(1, 0, 0);
}

D3DispersionPSOVector::~D3DispersionPSOVector()
{

}

QString D3DispersionPSOVector::getDamping() const
{
    return m_damping;
}

double D3DispersionPSOVector::getS6() const
{
    Vector<double> vec = m_s6->encode();
    return vec[0];
}

double D3DispersionPSOVector::getS8() const
{
    Vector<double> vec = m_s8->encode();
    return vec[0];
}

double D3DispersionPSOVector::getA1() const
{
    Q_ASSERT_X(m_damping == "bj", "D3DispersionPSOVector::getA1", "damping is not bj");
    Vector<double> vec = m_a1->encode();
    return vec[0];
}

double D3DispersionPSOVector::getA2() const
{
    Q_ASSERT_X(m_damping == "bj", "D3DispersionPSOVector::getA2", "damping is not bj");
    Vector<double> vec = m_a2->encode();
    return vec[0];
}

double D3DispersionPSOVector::getSr6() const
{
    Q_ASSERT_X(m_damping == "zero", "D3DispersionPSOVector::getSr6", "damping is not zero");
    Vector<double> vec = m_sr6->encode();
    return vec[0];
}

double D3DispersionPSOVector::getAlpha6() const
{
    Q_ASSERT_X(m_damping == "zero", "D3DispersionPSOVector::getAlpha6", "damping is not zero");
    Vector<double> vec = m_alpha6->encode();
    return vec[0];
}

void D3DispersionPSOVector::setDamping(const QString &value)
{
    Q_ASSERT_X(value == "zero" || value == "bj", "D3DispersionPSOVector::setDamping", "damping is not either zero or bj");
    m_damping = value;
    if (m_damping == "bj")
    {
        m_a1 = std::make_shared<PSO1DVector>(1, 0, 0);
        m_a2 = std::make_shared<PSO1DVector>(1, 0, 0);
    }
    else if (m_damping == "zero")
    {
        m_sr6 = std::make_shared<PSO1DVector>(1, 0, 0);
        m_alpha6 = std::make_shared<PSO1DVector>(1, 0, 0);
    }
}

void D3DispersionPSOVector::setS6(double min, double max)
{
    m_s6->setPosMin(min);
    m_s6->setPosMax(max);
}

void D3DispersionPSOVector::setS8(double min, double max)
{
    m_s8->setPosMin(min);
    m_s8->setPosMax(max);
}

void D3DispersionPSOVector::setA1(double min, double max)
{
    Q_ASSERT_X(m_damping == "bj", "D3DispersionPSOVector::setA1", "damping is not bj");
    m_a1->setPosMin(min);
    m_a1->setPosMax(max);
}

void D3DispersionPSOVector::setA2(double min, double max)
{
    Q_ASSERT_X(m_damping == "bj", "D3DispersionPSOVector::setA2", "damping is not bj");
    m_a2->setPosMin(min);
    m_a2->setPosMax(max);
}

void D3DispersionPSOVector::setSr6(double min, double max)
{
    Q_ASSERT_X(m_damping == "zero", "D3DispersionPSOVector::setSr6", "damping is not zero");
    m_sr6->setPosMin(min);
    m_sr6->setPosMax(max);
}

void D3DispersionPSOVector::setAlpha6(double min, double max)
{
    Q_ASSERT_X(m_damping == "zero", "D3DispersionPSOVector::setAlpha6", "damping is not zero");
    m_alpha6->setPosMin(min);
    m_alpha6->setPosMax(max);
}


std::shared_ptr<PSOVector> D3DispersionPSOVector::clone()
{
    auto newvector = std::make_shared<D3DispersionPSOVector>(m_damping);
//    newvector->m_damping = m_damping;

    newvector->m_s6 = std::dynamic_pointer_cast<PSO1DVector>(m_s6->clone());
    newvector->m_s8 = std::dynamic_pointer_cast<PSO1DVector>(m_s8->clone());
    if (m_damping == "bj")
    {
        newvector->m_a1 = std::dynamic_pointer_cast<PSO1DVector>(m_a1->clone());
        newvector->m_a2 = std::dynamic_pointer_cast<PSO1DVector>(m_a2->clone());
    }
    else
    {
        newvector->m_sr6 = std::dynamic_pointer_cast<PSO1DVector>(m_sr6->clone());
        newvector->m_alpha6 = std::dynamic_pointer_cast<PSO1DVector>(m_alpha6->clone());
    }

    return newvector;
}

int D3DispersionPSOVector::size() const
{
    return 4;
}

Vector<double> D3DispersionPSOVector::encode() const
{
    Vector<double> res;
    res.append(m_s6->encode());
    res.append(m_s8->encode());

    if (m_damping == "bj")
    {
        res.append(m_a1->encode());
        res.append(m_a2->encode());
    }
    else
    {
        res.append(m_sr6->encode());
        res.append(m_alpha6->encode());
    }

    return res;
}

void D3DispersionPSOVector::decode(const Vector<double> &vector)
{
    Q_ASSERT_X(vector.size() == this->size(), "D3DispersionPSOVector::decode", "size inconsistent");
    int index = 0;

    {
        Vector<double> vec;
        vec.append(vector[0]);
        m_s6->decode(vec);
    }

    {
        Vector<double> vec;
        vec.append(vector[1]);
        m_s8->decode(vec);
    }

    if (m_damping == "bj")
    {
        {
            Vector<double> vec;
            vec.append(vector[2]);
            m_a1->decode(vec);
        }
        {
            Vector<double> vec;
            vec.append(vector[3]);
            m_a2->decode(vec);
        }
    }
    else
    {
        {
            Vector<double> vec;
            vec.append(vector[2]);
            m_sr6->decode(vec);
        }

        {
            Vector<double> vec;
            vec.append(vector[3]);
            m_alpha6->decode(vec);
        }
    }
}

void D3DispersionPSOVector::spread(double factor)
{
    m_s6->spread(factor);
    m_s8->spread(factor);
    m_s6->stablizePosition();
    m_s8->stablizePosition();
    if (m_damping == "bj")
    {
        m_a1->spread(factor);
        m_a1->stablizePosition();
        m_a2->spread(factor);
        m_a2->stablizePosition();
    }
    else
    {
        m_sr6->spread(factor);
        m_sr6->stablizePosition();
        m_alpha6->spread(factor);
        m_alpha6->stablizePosition();
    }
}

double D3DispersionPSOVector::getLowerBound(int index)
{
    if (index == 0)
    {
        return m_s6->getLowerBound(0);
    }
    if (index == 1)
    {
        return m_s8->getLowerBound(0);
    }

    if (index == 2)
    {
        if (m_damping == "bj")
        {
            return m_a1->getLowerBound(0);
        }
        else
        {
            return m_sr6->getLowerBound(0);
        }
    }

    if (index == 3)
    {
        if (m_damping == "bj")
        {
            return m_a2->getLowerBound(0);
        }
        else
        {
            return m_alpha6->getLowerBound(0);
        }
    }

    throw std::invalid_argument("D3DispersionPSOVector::getLowerBound");
}

double D3DispersionPSOVector::getUpperBound(int index)
{
    if (index == 0)
    {
        return m_s6->getUpperBound(0);
    }
    if (index == 1)
    {
        return m_s8->getUpperBound(0);
    }

    if (index == 2)
    {
        if (m_damping == "bj")
        {
            return m_a1->getUpperBound(0);
        }
        else
        {
            return m_sr6->getUpperBound(0);
        }
    }

    if (index == 3)
    {
        if (m_damping == "bj")
        {
            return m_a2->getUpperBound(0);
        }
        else
        {
            return m_alpha6->getUpperBound(0);
        }
    }

    throw std::invalid_argument("D3DispersionPSOVector::getUpperBound");

}

void D3DispersionPSOVector::randomizePosition()
{
    m_s6->randomizePosition();
    m_s8->randomizePosition();
    if (m_damping == "bj")
    {
        m_a1->randomizePosition();
        m_a2->randomizePosition();
    }
    else
    {
        m_sr6->randomizePosition();
        m_alpha6->randomizePosition();
    }
}

void D3DispersionPSOVector::randomizeVelocity()
{
    m_s6->randomizePosition();
    m_s8->randomizePosition();
    if (m_damping == "bj")
    {
        m_a1->randomizePosition();
        m_a2->randomizePosition();
    }
    else
    {
        m_sr6->randomizePosition();
        m_alpha6->randomizePosition();
    }
}

void D3DispersionPSOVector::stablizeVelocity()
{
    m_s6->stablizeVelocity();
    m_s8->stablizeVelocity();
    if (m_damping == "bj")
    {
        m_a1->stablizeVelocity();
        m_a2->stablizeVelocity();
    }
    else
    {
        m_sr6->stablizeVelocity();
        m_alpha6->stablizeVelocity();
    }
}

void D3DispersionPSOVector::stablizePosition(libPSO::PSOVector *vel)
{
    const D3DispersionPSOVector* real_vel = dynamic_cast<const D3DispersionPSOVector*> (vel);
    Q_ASSERT_X(real_vel != nullptr, "D3DispersionPSOVector::stablizePosition", "typeError");

    m_s6->stablizePosition(real_vel->m_s6.get());
    m_s8->stablizePosition(real_vel->m_s8.get());
    if (m_damping == "bj")
    {
        m_a1->stablizePosition(real_vel->m_a1.get());
        m_a2->stablizePosition(real_vel->m_a2.get());
    }
    else
    {
        m_sr6->stablizePosition(real_vel->m_sr6.get());
        m_alpha6->stablizePosition(real_vel->m_alpha6.get());
    }
}

void D3DispersionPSOVector::copyControllerFrom(const libPSO::PSOVector *rhs)
{
    const D3DispersionPSOVector *vector = dynamic_cast<const D3DispersionPSOVector*>(rhs);
    Q_ASSERT_X(vector != nullptr, "D3DispersionPSOVector::copyControllerFrom", "TypeError");

    m_s6->copyControllerFrom(vector->m_s6.get());
    m_s8->copyControllerFrom(vector->m_s8.get());
    if (m_damping == "bj")
    {
        m_a1->copyControllerFrom(vector->m_a1.get());
        m_a2->copyControllerFrom(vector->m_a2.get());
    }
    else
    {
        m_sr6->copyControllerFrom(vector->m_sr6.get());
        m_alpha6->copyControllerFrom(vector->m_alpha6.get());
    }

}

void D3DispersionPSOVector::copyfrom(const PSOVector *rhs)
{
    const D3DispersionPSOVector *rhs_l = dynamic_cast<const D3DispersionPSOVector*>(rhs);

    Q_ASSERT_X( this->size() == rhs_l->size(), "D3DispersionPSOVector::copyfrom", "size inconsistent") ;

    this->m_damping = rhs_l->m_damping;

    this->m_s6->copyfrom(rhs_l->m_s6.get());
    this->m_s6->copyControllerFrom(rhs_l->m_s6.get());

    this->m_s8->copyfrom(rhs_l->m_s8.get());
    this->m_s8->copyControllerFrom(rhs_l->m_s8.get());

    if (this->m_damping == "bj")
    {
        this->m_a1->copyfrom(rhs_l->m_a1.get());
        this->m_a1->copyControllerFrom(rhs_l->m_a1.get());

        this->m_a2->copyfrom(rhs_l->m_a2.get());
        this->m_a2->copyControllerFrom(rhs_l->m_a2.get());
    }
    else if (this->m_damping == "zero")
    {
        this->m_sr6->copyfrom(rhs_l->m_sr6.get());
        this->m_sr6->copyControllerFrom(rhs_l->m_sr6.get());

        this->m_alpha6->copyfrom(rhs_l->m_alpha6.get());
        this->m_alpha6->copyControllerFrom(rhs_l->m_alpha6.get());
    }
}



}
