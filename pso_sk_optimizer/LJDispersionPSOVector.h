#ifndef LJDISPERSIONPSOVECTOR_H
#define LJDISPERSIONPSOVECTOR_H

#include "psovector.h"
#include "pso1dvector.h"

#include <QList>
#include <QHash>
#include <QVariant>
#include <QString>

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

namespace DFTBPSO
{
    using namespace libPSO;

    class LJDispersionPSOVector : public PSOVector
    {
        friend class libvariant::VariantConvertor<LJDispersionPSOVector>;
    public:
        LJDispersionPSOVector();
        ~LJDispersionPSOVector();

        void addVector(const QString& element, const QPair<double, double>& distance_range,
                       const QPair<double, double>& energy_range);
        int numberDispersion() const;
        QString getElement(int index) const;
        double getDistance(int index) const;
        double getEnergy(int index) const;

        // PSOVector interface
    public:
        std::shared_ptr<PSOVector> clone() override;
        int size() const;
        Vector<double> encode() const;
        void decode(const Vector<double> &vector);
        void spread(double factor);
        double getLowerBound(int index);
        double getUpperBound(int index);
        void randomizePosition();
        void randomizeVelocity();
        void stablizeVelocity();
        void stablizePosition(PSOVector *vel);
        void copyControllerFrom(const PSOVector *rhs);


    private:
        QList<std::shared_ptr<PSO1DVector>> m_distance_vectors;
        QList<std::shared_ptr<PSO1DVector>> m_energy_vectors;
        QStringList m_elements;
        QMap<QString, QPair<std::shared_ptr<PSO1DVector>, std::shared_ptr<PSO1DVector>> > m_vectors;
        void clear();
    };

}


#endif // DISPERSIONPSOVECTOR_H
