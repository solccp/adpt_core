#-------------------------------------------------
#
# Project created by QtCreator 2013-08-28T19:33:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SKBuilder
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app
GIT_VERSION = $$system(git describe HEAD)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

SOURCES += \
    main.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../libskbuilder/ -lskbuilder

INCLUDEPATH += $$PWD/../libskbuilder/
DEPENDPATH += $$PWD/../libskbuilder/

INCLUDEPATH += $$PWD/../external_libs/include
LIBS += $$PWD/../external_libs/lib/libVariant.a $$PWD/../external_libs/lib/libyaml.a

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libskbuilder/libskbuilder.a

RESOURCES += skbuilder.qrc


include(../settings.pri)
unix {
    target.path = $$PREFIX/adpt/bin
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/release/ -lSKBuilderCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/debug/ -lSKBuilderCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKBuilderCommon/ -lSKBuilderCommon

INCLUDEPATH += $$PWD/../libSKBuilderCommon
DEPENDPATH += $$PWD/../libSKBuilderCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/libSKBuilderCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/libSKBuilderCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/SKBuilderCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/SKBuilderCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/libSKBuilderCommon.a
