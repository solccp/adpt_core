#include <QCoreApplication>
#include <QTemporaryDir>
#include <QCommandLineParser>
#include <QString>

#include "atomicproperties.h"

#include "skbuilder/input.h"
#include "skbuilder/input_variant.h"
#include "skbuilder/sktoolkitbase.h"
#include "skbuilder/skbuilder.h"

#include "variantqt.h"
#include <Variant/SchemaLoader.h>

void myMessageOutput(QtMsgType , const QMessageLogContext &, const QString &)
{
}





libvariant::Variant readSchema(const QString& uri)
{
    QFile schema_file(uri);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    return libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationVersion(QString(GIT_VERSION));
    a.setApplicationName("SKBuilder");

    QTextStream stdout_stream(stdout);



    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("input", "Input file");

    QCommandLineOption yamlOption("y", "Parse input file as the YAML format.");
    parser.addOption(yamlOption);

    QCommandLineOption debugOption("d", "Debug output");
    parser.addOption(debugOption);

    QCommandLineOption scratchPathOption(QStringList() << "s" << "scratch-path",
           QCoreApplication::translate("main", "Scratch directory"),
           QCoreApplication::translate("main", "scratch"));
    parser.addOption(scratchPathOption);


    // Process the actual command line arguments given by the user
    parser.process(a);

    if (parser.positionalArguments().empty())
    {
        parser.showHelp(1);
    }


    QString inputfilename = parser.positionalArguments().first();

    bool useJsonInput = true;
    if (parser.isSet(yamlOption))
    {
        useJsonInput = false;
    }


    SKBuilder::Input::SKBuilderInput input;

    if (parser.isSet(debugOption))
    {
        input.options.debug = true;
    }
    else
    {
        qInstallMessageHandler(myMessageOutput);
    }


    QFileInfo input_filename(inputfilename);

    libvariant::Variant var;

    if (useJsonInput)
    {
        try
        {
            var = libvariant::DeserializeFile(input_filename.absoluteFilePath().toStdString().c_str(), libvariant::SERIALIZE_JSON);
            stdout_stream << "JSON input detected and loaded." << endl;
        }
        catch(...)
        {
            stdout_stream << "Unknown input format." << endl;
            return 1;
        }
    }
    else
    {
        try
        {
            var = libvariant::DeserializeFile(input_filename.absoluteFilePath().toStdString().c_str(), libvariant::SERIALIZE_YAML);
            stdout_stream << "YAML input detected and loaded." << endl;
        }
        catch(std::exception & e)
        {
            stdout_stream << e.what() << endl;
            stdout_stream << "Unknown input format." << endl;
            return 1;
        }
    }



    try
    {       
        libvariant::AdvSchemaLoader loader;


        libvariant::Variant schema = readSchema(":/schema/skbuilder_input_schema.json");
        loader.AddSchema("twocenter_parameters_nctu.json", readSchema(":/schema/twocenter_parameters_nctu.json"));
        loader.AddSchema("twocenter_parameters_bccms.json", readSchema(":/schema/twocenter_parameters_bccms.json"));
        loader.AddSchema("onecenter_parameters_nctu.json", readSchema(":/schema/onecenter_parameters_nctu.json"));
        loader.AddSchema("onecenter_parameters_bccms.json", readSchema(":/schema/onecenter_parameters_bccms.json"));

        stdout_stream << "Validating input file...";

        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);

        if (result.Error())
        {
            stdout_stream << "failed." << endl;

            auto schema_errors = result.GetSchemaErrors();
            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(std::cout);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(std::cout);
            }
            return 1;
        }
        else
        {
            stdout_stream << "passed." << endl;
        }


        try
        {
            stdout_stream << "Processing input file...";
            libvariant::VariantConvertor<SKBuilder::Input::SKBuilderInput>::fromVariant(var, input);
            stdout_stream << "done." << endl;
            {
                QString filename = QString("%1_parsed.json").arg(input_filename.baseName());
                libvariant::SerializeJSON(filename.toStdString(), var, true);
                stdout_stream << "Parsed json input file written" << endl;
            }
            {
                QString filename = QString("%1_parsed.yaml").arg(input_filename.baseName());
                libvariant::SerializeYAML(filename.toStdString(), var);
                stdout_stream << "Parsed yaml input file written" << endl;
            }
        }
        catch(const std::exception &e)
        {
            stdout_stream << "failed." << endl;
            stdout_stream << e.what() << endl;
            return 1;
        }
    }
    catch(const std::exception &e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }




    QDir rootDir = input_filename.absoluteDir();

    input.makeAbsoluatePath(rootDir);

    auto builder = std::make_shared<SKBuilder::Builder>(input, stdout_stream);



    QDir output;
    if (QDir(input.options.outputPrefix).isAbsolute())
    {
        output.setPath(QDir(input.options.outputPrefix).absolutePath());
    }
    else
    {
        output.setPath(rootDir.absoluteFilePath(input.options.outputPrefix));
    }


    builder->setOutputDir(output);

    QString scratchPath = parser.value(scratchPathOption);
    bool scratchSet = false;
    if (!scratchPath.isEmpty())
    {
        auto dir = QFileInfo(scratchPath);
        if (dir.exists() && dir.isDir() && dir.isWritable())
        {
            QTemporaryDir tempDir(dir.absoluteFilePath()+"/SKBuilder-XXXXXX");
            tempDir.setAutoRemove(false);
            if (tempDir.isValid())
            {
                builder->setRootDir(tempDir.path());
                scratchSet = true;
            }
        }
    }
    if (!scratchSet)
    {
        QTemporaryDir tempDir;
        tempDir.setAutoRemove(false);
        builder->setRootDir(QDir(tempDir.path()));
    }


    try
    {
        builder->init();
        builder->build();
        builder->write();
        stdout_stream << endl << "Normal Termination." << endl;
        builder->removeFiles();
    }
    catch(std::exception & e)
    {
        stdout_stream << e.what() << endl;
        return 1;
    }
    return 0;
}
