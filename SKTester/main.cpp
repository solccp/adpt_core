#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>

#include <limits>
#include <iomanip>
#include <iostream>
#include <string>
#include <fstream>
#include <memory>

#include <QDebug>

#include <QDir>


#include "atomicproperties.h"
#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/InputFile.h"
#include "DFTBTestSuite/input_variant.h"
#include "DFTBTestSuite/dftbtester.h"
#include "DFTBTestSuite/ReferenceDataType/referencedata_variant.h"


#include <QTemporaryDir>
#include <QCommandLineParser>

#include <QFileInfo>

#include <Variant/Variant.h>
#include <Variant/SchemaLoader.h>
#include "variantqt.h"



using namespace std;
using namespace DFTBTestSuite;


void myMessageOutput(QtMsgType , const QMessageLogContext &, const QString &)
{
}

libvariant::Variant readSchema(const QString& uri)
{
    QFile schema_file(uri);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    return libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
}

int main(int argc, char** argv)
{
    QCoreApplication a(argc, argv);
    a.setApplicationName("SKTester");
    a.setApplicationVersion(QString(GIT_VERSION));

    QCommandLineParser parser;
    parser.setApplicationDescription("DFTBTester helper");
    parser.addHelpOption();
    parser.addVersionOption();

    // An option with a value
    QCommandLineOption inputFileOption(QStringList() << "i" << "input-file",
           QCoreApplication::translate("main", "The evaluation and tester definition input file"),
           QCoreApplication::translate("main", "input"));
    parser.addOption(inputFileOption);

    // An option with a value
    QCommandLineOption referenceFileOption(QStringList() << "r" << "reference-file",
           QCoreApplication::translate("main", "The reference data file"),
           QCoreApplication::translate("main", "reference"));
    parser.addOption(referenceFileOption);

    // An option with a value
    QCommandLineOption outputFileOption(QStringList() << "o" << "output-file",
           QCoreApplication::translate("main", "The output file"),
           QCoreApplication::translate("main", "output"));
    parser.addOption(outputFileOption);


    QCommandLineOption yamlOption("y", "Parse input file as the YAML format.");
    parser.addOption(yamlOption);

    QCommandLineOption debugOption("d", "Debug output");
    parser.addOption(debugOption);

    QCommandLineOption scratchPathOption(QStringList() << "s" << "scratch-path",
           QCoreApplication::translate("main", "Scratch directory"),
           QCoreApplication::translate("main", "scratch"));
    parser.addOption(scratchPathOption);


    // Process the actual command line arguments given by the user
    parser.process(a);


    if (!parser.isSet(inputFileOption) || !parser.isSet(referenceFileOption))
    {
        parser.showHelp();
    }


    QString input_filename = parser.value(inputFileOption);
    QString reference_filename = parser.value(referenceFileOption);


    QString output_filename;
    bool hasOutput = false;


    if(parser.isSet(outputFileOption))
    {
        hasOutput = true;
        output_filename = parser.value(outputFileOption);
    }


    QTextStream stdout_stream(stdout, QIODevice::WriteOnly | QIODevice::Text);

    bool useJsonInput = true;
    if (parser.isSet(yamlOption))
    {
        useJsonInput = false;
    }

    if (parser.isSet(debugOption))
    {

    }
    else
    {
        qInstallMessageHandler(myMessageOutput);
    }



    ReferenceData ref_data;
    InputData input_data;


        {
            libvariant::Variant var;
            if (useJsonInput)
            {
                var = libvariant::DeserializeJSONFile(input_filename.toStdString().c_str());
            }
            else
            {
                var = libvariant::DeserializeYAMLFile(input_filename.toStdString().c_str());
            }

            libvariant::Variant schema = readSchema(":/schema/tester_input_schema.json");
            libvariant::AdvSchemaLoader loader;
            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);

            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                return 1;
            }

            stdout_stream << "Input schema checked" << endl;
            libvariant::VariantConvertor<InputData>::fromVariant(var, input_data);
            stdout_stream << "Input file loaded" << endl;
        }
        {
            libvariant::Variant var;
            if (useJsonInput)
            {
                var = libvariant::DeserializeJSONFile(reference_filename.toStdString().c_str());
            }
            else
            {
                var = libvariant::DeserializeYAMLFile(reference_filename.toStdString().c_str());
            }

            libvariant::Variant schema = readSchema(":/schema/tester_ref_schema.json");
            libvariant::AdvSchemaLoader loader;
            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);

            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(std::cout);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(std::cout);
                }
                return 1;

            }

            stdout_stream << "Ref. data schema checked" << endl;
            libvariant::VariantConvertor<ReferenceData>::fromVariant(var, ref_data);
            stdout_stream << "Ref. file loaded" << endl;

//            QFileInfo info(reference_filename);
//            QString parsed_filename;

//            libvariant::Variant var2 = libvariant::VariantConvertor<ReferenceData>::toVariant(ref_data);

//            parsed_filename = info.baseName() + "_parsed.json";
//            libvariant::SerializeJSON(parsed_filename.toStdString(), var2, true);

//            parsed_filename = info.baseName() + "_parsed.yaml";
//            libvariant::SerializeYAML(parsed_filename.toStdString(), var2);
        }

    QDir rootDir(QFileInfo(input_filename).absoluteDir());

    input_data.makeAbsolutePath(rootDir);
    auto tester = std::make_shared<DFTBTester>(input_data, &ref_data, &a);


    QString scratchPath = parser.value(scratchPathOption);
    bool scratchSet = false;
    if (!scratchPath.isEmpty())
    {
        auto dir = QFileInfo(scratchPath);
        if (dir.exists() && dir.isDir() && dir.isWritable())
        {
            QTemporaryDir tempDir(dir.absoluteFilePath()+"/SKTester-XXXXXX");
            tempDir.setAutoRemove(false);
            if (tempDir.isValid())
            {
                tester->setTempRoot(QDir(tempDir.path()));
                scratchSet = true;
            }
        }
    }



    tester->setAutoRemove(false);
    tester->setIntermidiate_suffix(QFileInfo(input_filename).baseName());


    QFileInfo info(input_filename);
    QString parsed_filename;

    libvariant::Variant var = libvariant::VariantConvertor<InputData>::toVariant(input_data);

    parsed_filename = info.baseName() + "_parsed.json";
    libvariant::SerializeJSON(parsed_filename.toStdString(), var, true);
    stdout_stream << "The parsed input file has been wriiten as " << parsed_filename << endl;

    parsed_filename = info.baseName() + "_parsed.yaml";
    libvariant::SerializeYAML(parsed_filename.toStdString(), var);
    stdout_stream << "The parsed input file has been wriiten as " << parsed_filename << endl;

    QFile outputfile(rootDir.absoluteFilePath(output_filename));
    if (hasOutput)
    {
        if (outputfile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            stdout_stream.setDevice(&outputfile);
        }
    }
    bool succ = tester->evaluate(stdout_stream);


    if (succ)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
