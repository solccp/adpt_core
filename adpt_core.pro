TEMPLATE = subdirs
SUBDIRS = libadpt_common \
    libDFTBTestSuiteCommon libDFTBTestSuite\
    libHSDParser libDFTBPlusAdaptor libpso\
    libErepfitCommon liberepfit_evaluator liberepfit \
    libSKBuilderCommon libskbuilder libSKOptimizerCommon\
    SKBuilder SKTester \
    erepfit_evaluator erepfit pso_sk_optimizer \ 
#    liberepfit_solver erepfit_solver

!win32 {
  SUBDIRS +=
}
    
build_tests {
    SUBDIRS += tests
}
CONFIG += ordered

