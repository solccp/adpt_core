#-------------------------------------------------
#
# Project created by QtCreator 2013-10-02T22:14:33
#
#-------------------------------------------------

QT       -= gui
TARGET = DFTBTestSuite
CONFIG += c++11
CONFIG += staticlib
TEMPLATE = lib
QMAKE_CXXFLAGS+=-Wunused-but-set-variable
DEFINES += ELPP_THREAD_SAFE
DEFINES += NDEBUG

GIT_VERSION = $$system(git --work-tree $$PWD describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

SOURCES += \
    DFTBTestSuite/evaluatorfactory.cpp \
    DFTBTestSuite/dftbplusevaluator.cpp \
    DFTBTestSuite/formatter.cpp \
    DFTBTestSuite/textformatter.cpp \
    DFTBTestSuite/statisticanalyser.cpp \
    DFTBTestSuite/dftbtester.cpp \
    DFTBTestSuite/Evaluator.cpp \
    DFTBTestSuite/Testing/MolecularDataComparator.cpp \
    DFTBTestSuite/Testing/propertytester.cpp \
    DFTBTestSuite/dcdftbkevaluator.cpp \
    DFTBTestSuite/newuoa.cpp

HEADERS += \
    DFTBTestSuite/typesettings.h \
    DFTBTestSuite/Evaluator.h \
    DFTBTestSuite/evaluatorfactory.h \
    DFTBTestSuite/EvaluatedData.h \
    DFTBTestSuite/dftbplusevaluator.h \
    DFTBTestSuite/formatter.h \
    DFTBTestSuite/textformatter.h \
    DFTBTestSuite/statisticanalyser.h \
    DFTBTestSuite/dftbtester.h \
    DFTBTestSuite/Testing/MolecularDataComparator.h \
    DFTBTestSuite/Testing/propertytester.h \
    DFTBTestSuite/constants.h \
    DFTBTestSuite/dcdftbkevaluator.h \
    DFTBTestSuite/optimizer.h \
    DFTBTestSuite/newuoa.h



INCLUDEPATH += $$PWD/../external_libs/include/
LIBS += $$PWD/../external_libs/libs/libVariant.a



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a
