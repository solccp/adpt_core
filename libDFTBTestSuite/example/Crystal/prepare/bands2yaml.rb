#!/usr/bin/env ruby

require 'matrix'
require './styled_yaml.rb'

rows = []

bands_file = ARGV[0]

File.open(bands_file) do |file|
    while line = file.gets
        rows << line.split[1..-1].map{|x| x.to_f }
    end
end

docs = {}
temp = []


matrix = Matrix.rows(rows)
matrix.transpose.row_vectors.each do |row|
    temp << StyledYAML.inline( row.to_a )
end

docs["Bands"] = temp

StyledYAML.dump docs, $stdout
