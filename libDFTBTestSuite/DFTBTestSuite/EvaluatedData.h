#ifndef EVALUATEDDATA_H
#define EVALUATEDDATA_H

#include "DFTBTestSuite/ReferenceDataType/AtomicData.h"
#include "DFTBTestSuite/ReferenceDataType/MolecularData.h"
#include "DFTBTestSuite/ReferenceDataType/CrystalData.h"
#include "DFTBTestSuite/ReferenceDataType/ReactionData.h"

namespace DFTBTestSuite{

class EvaluatedData
{
public:
    QMap<QString, BandStructureData> bandstructureDataMapping;
    QMap<QString, ReferenceReactionData> reactionDataMapping;
    QMap<QString, AtomicData> atomicDataMapping;
    QMap<QString, MolecularData>  molecularDataMapping;
    QMap<QString, CrystalEnergyData>  crystalDataMapping;
public:
    void clear()
    {
        bandstructureDataMapping.clear();
        reactionDataMapping.clear();
        atomicDataMapping.clear();
        molecularDataMapping.clear();
        crystalDataMapping.clear();
    }
};

}

#endif // EVALUATEDDATA_H
