#ifndef DFTBPLUSEVALUATOR_H
#define DFTBPLUSEVALUATOR_H

#include "newuoa.h"
#include "Evaluator.h"
typedef std::array<int, 3> iVector3;
typedef std::array<double, 3> dVector3;

namespace DFTBTestSuite
{
template <class F>
NewuoaClosure make_closure(F &function) {
    struct Wrap {
        static double call(void *data, long n, const double *values) {
            return reinterpret_cast<F *>(data)->operator()(n, values);
        }
    };
    return NewuoaClosure {&function, &Wrap::call};
}
class DFTBPlusEvaluator : public Evaluator
{


public:
    DFTBPlusEvaluator(QTextStream& output_stream, const Evaluation& eval_opts, const QString& codePath);
    ~DFTBPlusEvaluator();

    // Evaluator interface
public:
    void EvaluateAtomicEnergy(AtomicData &calc, const QString &template_input_filename, const OptionalEvaluationInformation &opt_info, DFTBResultInfo &info) const override;
    void EvaluateGeometry(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const override;
    void EvaluateFrequency(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const;
    void EvaluateMolecularEnergy(const MolecularData &ref,const OptionalEvaluationInformation& opt_info,  MolecularData &calc, DFTBResultInfo &info) const override;
    void EvaluateBandStructure(const CrystalGeometryData &ref_geom, const BandStructureData &ref, const CrystalEnergyData& calc_energy_data, const OptionalEvaluationInformation& opt_info, BandStructureData &calc_data, DFTBResultInfo &info) const override;
    void EvaluateCrystalEnergy(const CrystalGeometryData &ref_geom, const OptionalEvaluationInformation& opt_info, CrystalEnergyData &calc, DFTBResultInfo &info) const override;
private:


    void EvaluateSpinUnpolarizedBandStructure(const CrystalGeometryData &ref_geom, const BandStructureData &ref, const CrystalEnergyData& calc_energy_data, const OptionalEvaluationInformation &opt_info, BandStructureData &calc_data, DFTBResultInfo &info) const;
    void EvaluateSpinPolarizedBandStructure(const CrystalGeometryData &ref_geom, const BandStructureData &ref, const CrystalEnergyData& calc_energy_data, const OptionalEvaluationInformation &opt_info, BandStructureData &calc_data, DFTBResultInfo &info) const;

    void writeInputGeometry(QTextStream& os, const QString& genstring) const;
    void writeInputEnergy(QTextStream &os, int charge, const QString& prefix, bool writeCharge=true) const;

    void writeSKFileInfo(QTextStream &os, const QDir &rootDir = QDir()) const;
    void writeParseOptions(QTextStream &os) const;
    void writeOptInfo(QTextStream &os, const OptionalEvaluationInformation& opt_info) const;

    QString findDriverName(const QString& filename) const;

    QString getGoodFilename(const QString& oriFilename) const;

    bool parseDFTBFirstPointEnergyInGOPT(const QString& filename, double& energy) const;
    void writeInputDriverOpt(QTextStream &os, const QString& optMethod) const;

    void writeInputForces(QTextStream &os) const;
    void writeInputFreq(QTextStream &os) const;

    void writeInputKPoints(QTextStream& os, bool doSCF, bool readCharge, const ADPT::KPointsSetting &kpoints) const;

    bool checkoutKPointDefinition(const QString& filename) const;
    void parseDFTBBandstructure(const QString& filename, BandStructureData &calc_data, DFTBResultInfo &info) const;
    void parseSpinPolarizedDFTBBandstructure(const QString& filename, BandStructureData &calc_data, DFTBResultInfo &info) const;

    void writeParsedBandStructure(const QString& filename, const QVector<QVector<double> > &bands, double shift = 0.0) const;
    void writeInputFile(QTextStream &fout, const QString& template_filename, const CrystalGeometryData &ref_geom, bool doSCF, bool readInitialCharges, const ADPT::KPointsSetting& kpoints, const OptionalEvaluationInformation& opt_info, QDir workingDir, bool spinpol, double upe) const;
    void writeInputHubbardDerivatives(QTextStream &fout, const ADPT::SKFileInfo *skfileinfo, bool orbital_resolved, const QMap<QString, dVector3> & hubbardDerivatives) const;

    // Evaluator interface
public:
    bool supportDirectFrequencyCalculation() const override;
};
}


#endif // DFTBPLUSEVALUATOR_H
