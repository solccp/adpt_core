#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>

constexpr double H_kcalmol = 627.509469;
constexpr double H_eV = 27.2107;

inline double EnergyToAU(const QString& ori_unit, double value)
{
    if (ori_unit.toLower() == "h")
    {
        return value;
    }
    if (ori_unit.toLower() == "kcal/mol")
    {
        return value/H_kcalmol;
    }
    else if (ori_unit.toLower() == "ev")
    {
        return value/H_eV;
    }
    else
    {
        throw std::invalid_argument("Unit conversion from " + ori_unit.toStdString() + " is not implemented.");
    }
}

inline double EnergyAUToAny(const QString& target_unit, double value)
{
    if (target_unit.toLower() == "kcal/mol")
    {
        return value*H_kcalmol;
    }
    else if (target_unit.toLower() == "ev")
    {
        return value*H_eV;
    }
    else
    {
        throw std::invalid_argument("Unit conversion to " + target_unit.toStdString() + " is not implemented.");
    }
}


#endif // CONSTANTS_H
