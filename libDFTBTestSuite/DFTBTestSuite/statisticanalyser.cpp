#include "statisticanalyser.h"

#include "DFTBTestSuite/InputFile.h"
#include "basic_types.h"

namespace DFTBTestSuite{


Statistics::Statistics(const ErrorUnit *errorUnit)
{
    units.insert("TorsionAngle","Degree(s)");
    units.insert("BondLength","Angstrom(s)");
    units.insert("BondAngle","Degree(s)");
    units.insert("ReactionEnergy","kcal/mol");
    units.insert("AtomizationEnergy","kcal/mol");
    if (errorUnit->useFrequencyPercentage)
    {
        units.insert("Frequencies","|%|");
    }
    else
    {
        units.insert("Frequencies","wavenumber(cm^-1)");
    }
    units.insert("EnthaphyOfFormation","kcal/mol");
    units.insert("BandStructure","eV");
    units.insert("RMSForce","a.u.");

    error_units.insert("AtomizationEnergy", errorUnit->AtomizationEnergy);
    error_units.insert("BandStructure", errorUnit->BandStructure);
    error_units.insert("BondAngle", errorUnit->BondAngle);
    error_units.insert("BondLength", errorUnit->BondLength);
    if (errorUnit->useFrequencyPercentage)
    {
        error_units.insert("Frequencies", errorUnit->Frequency_Percentage);
    }
    else
    {
        error_units.insert("Frequencies", errorUnit->Frequency);
    }

    error_units.insert("ReactionEnergy", errorUnit->ReactionEnergy);
    error_units.insert("TorsionAngle", errorUnit->TorsionAngle);

    this->m_error_unit = errorUnit;
}

void Statistics::beginGroup(const QString &type, const QString& uuid, const QString &name, double weight)
{
    if (!inGroup)
    {
        inGroup = true;
        currentName = name;
        currentType = type;
        currentWeight = weight;
        currentUUID = uuid;
    }
    else
    {
        throw GroupAlreadyBeganException();
    }
}

void Statistics::endGroup()
{
    if (inGroup)
    {
        inGroup = false;
        currentName = "";
        currentType = "";
        currentUUID = ADPT::UUID().toString();
        currentWeight = 1.0;
    }
    else
    {
        throw GroupNotBegunException();
    }
}

void Statistics::addValue(double ref_value, double calc_value, double weight=1.0)
{
    if (!inGroup)
    {
        throw GroupNotBegunException();
    }

    if (currentWeight < 0.0001)
        return;

    double deviation = (calc_value-ref_value)*weight;

    if (result.contains(currentType))
    {
        if (!result[currentType].contains(currentUUID))
        {
            QList<ErrorEntry> ala;
            result[currentType][currentUUID] = ala;
            m_uuid_name[currentUUID] = currentName;
        }
        result[currentType][currentUUID].append(ErrorEntry(ref_value, calc_value, currentWeight*weight));
        if (mue_max[currentType].first*mue_max[currentType].second < std::abs(deviation)*currentWeight)
        {
            mue_max[currentType].first = std::abs( deviation );
            mue_max[currentType].second = currentWeight;
            mue_max_name[currentType] = currentUUID;
        }
    }
    else
    {
        QMap<QString, QList<ErrorEntry> >ala;
        QList<ErrorEntry > ala2;

        ala2.append(ErrorEntry(ref_value, calc_value, currentWeight));
        ala[currentUUID] = ala2;
        result.insert(currentType, ala);
        mue_max[currentType].first = std::abs(deviation);
        mue_max[currentType].second = currentWeight;
        mue_max_name[currentType] = currentUUID;
        m_uuid_name[currentUUID] = currentName;
    }
}

void Statistics::compute()
{
    eu = 0.0;
    eu_mue_max = 0.0;

    QMapIterator<QString, QMap<QString, QList<ErrorEntry>>> it(result);
    while(it.hasNext())
    {
        it.next();
        QString type = it.key();

        const QMap<QString, QList<ErrorEntry>> name = it.value();
        QMapIterator<QString, QList<ErrorEntry>> it2(name);

        mse_type[type] = 0.0;
        mue_type[type] = 0.0;
        rms_type[type] = 0.0;


        if ( (type.toLower() == "frequencies") && (this->m_error_unit->useFrequencyPercentage))
        {
            mape_type.insert(type,0.0);
            mape_type_group[type].clear();
        }

        mse_type_group[type].clear();
        mue_type_group[type].clear();
        rms_type_group[type].clear();
        max_type_group[type].clear();


        int count_type = 0;
        double totalWeight = 0.0;
        while(it2.hasNext())
        {
            it2.next();
            QString groupUUID = it2.key();
            const QList<ErrorEntry>& data = it2.value();
            int count_group = 0;

            double totalGroupWeight = 0.0;
            for(int i=0; i<data.size() ;++i)
            {
                double weight = data[i].weight;
                double refValue = data[i].ref_value;
                double calcValue = data[i].calc_value;
                double value = refValue - calcValue;

                ++count_type;
                ++count_group;

                totalGroupWeight += weight;

                mse_type[type] += (value*weight);
                mue_type[type] += std::abs((value*weight));
                rms_type[type] += value*value*weight;


                mse_type_group[type][groupUUID] += (value*weight);
                mue_type_group[type][groupUUID] += std::abs((value*weight));
                rms_type_group[type][groupUUID] += value*value*weight;

                if (type.toLower() == "frequencies" && this->m_error_unit->useFrequencyPercentage)
                {
                    mape_type[type] += weight*100.0*qAbs((refValue-calcValue)/refValue); // value*value*weight;
                    mape_type_group[type][groupUUID] += weight*100.0*qAbs((refValue-calcValue)/refValue);
                }

                if (std::abs((value*weight)) > std::abs(max_type_group[type][groupUUID]))
                {
                    max_type_group[type][groupUUID] = (value*weight);
                }

            }
            mse_type_group[type][groupUUID] /= totalGroupWeight; //static_cast<double>(count_group);
            mue_type_group[type][groupUUID] /= totalGroupWeight; //static_cast<double>(count_group);
            rms_type_group[type][groupUUID] /= totalGroupWeight; //static_cast<double>(count_group);
            rms_type_group[type][groupUUID] = sqrt(rms_type_group[type][groupUUID]);

            if (type.toLower() == "frequencies" && this->m_error_unit->useFrequencyPercentage)
            {
                mape_type_group[type][groupUUID] /= totalGroupWeight;
            }

            totalWeight += totalGroupWeight;
        }

        mse_type[type] /= totalWeight; //static_cast<double>(count_type);
        mue_type[type] /= totalWeight; //static_cast<double>(count_type);
        rms_type[type] /= totalWeight; //static_cast<double>(count_type);
        rms_type[type] = sqrt(rms_type[type]);


        if (type.toLower() == "frequencies" && m_error_unit->useFrequencyPercentage)
        {
            mape_type[type] /= totalWeight;
            eu_comp[type] = mape_type[type]/error_units[type];
        }
        else
        {
            eu_comp[type] = mue_type[type]/error_units[type];
            eu_mue_max += (mue_max[type].first*mue_max[type].second/error_units[type]);
        }
        eu += eu_comp[type];

    }

}

double Statistics::getError_MSE(const QString &type) const
{
    if (mse_type.contains(type))
        return mse_type[type];
    throw WrongArgumentException();
}

double Statistics::getError_MUE(const QString &type) const
{
    if (mue_type.contains(type))
        return mue_type[type];
    throw WrongArgumentException();
}

double Statistics::getError_RMS(const QString &type) const
{
    if (rms_type.contains(type))
        return rms_type[type];
    throw WrongArgumentException();
}

double Statistics::getError_MAPE(const QString &type) const
{
    if (mape_type.contains(type))
        return mape_type[type];
    throw WrongArgumentException();
}

double Statistics::getError_MSE(const QString &type, const QString &groupUUID) const
{
    if (mse_type_group.contains(type) && mse_type_group[type].contains(groupUUID))
    {
        return mse_type_group[type][groupUUID];
    }
    throw WrongArgumentException();
}

double Statistics::getError_MUE(const QString &type, const QString &groupUUID) const
{
    if (mue_type_group.contains(type) && mue_type_group[type].contains(groupUUID))
    {
        return mue_type_group[type][groupUUID];
    }
    throw WrongArgumentException();
}

double Statistics::getError_RMS(const QString &type, const QString &groupUUID) const
{
    if (rms_type_group.contains(type) && rms_type_group[type].contains(groupUUID))
    {
        return rms_type_group[type][groupUUID];
    }
    throw WrongArgumentException();
}

double Statistics::getError_MAX(const QString &type, const QString &groupUUID) const
{
    if (max_type_group.contains(type) && max_type_group[type].contains(groupUUID))
    {
        return max_type_group[type][groupUUID];
    }
    throw WrongArgumentException();
}

QPair<double, double> Statistics::getError_MAX(const QString &type) const
{
    if (mue_max.contains(type))
    {
        return mue_max[type];
    }
    throw WrongArgumentException();
}

QList<QString> Statistics::getError_UUIDs(const QString &type) const
{
    if (mse_type_group.contains(type))
    {
        return mse_type_group[type].keys();
    }
    return QList<QString>();
}

QString Statistics::getError_Name(const QString &UUID) const
{
    return m_uuid_name[UUID];
}

int Statistics::getError_Count(const QString &type) const
{
    if (result.contains(type))
    {
        QMapIterator<QString, QList<ErrorEntry>> it(result[type]);
        int counts = 0;
        while(it.hasNext())
        {
            it.next();
            counts += it.value().size();
        }
        return counts;
    }
    throw WrongArgumentException();
}

QList<QString> Statistics::getTypes() const
{
    return result.keys();
}

void Statistics::reset()
{
    mse_type.clear();
    mue_type.clear();
    rms_type.clear();
    result.clear();
    unconverged.clear();
    converged.clear();
    m_uuid_name.clear();
}



double Statistics::getError_EU() const
{
    return eu;
}

double Statistics::getErrorComp_EU(const QString &type) const
{
    if (eu_comp.contains(type))
        return eu_comp[type];
    throw WrongArgumentException();
}


void Statistics::addUnconvergedTarget(const QString &name)
{
    unconverged.append(name);
}

void Statistics::addConvergedTarget(const QString &name)
{
    converged.append(name);
}

const QList<QString> &Statistics::getUnconvergedTarget() const
{
    return unconverged;
}

const QList<QString> &Statistics::getConvergedTarget() const
{
    return converged;
}

QPair<QPair<double, double>, QString> Statistics::getError_MUEMAX(const QString &type) const
{
    if (mue_max.contains(type))
    {
        return QPair<QPair<double, double>, QString>(mue_max[type], mue_max_name[type]);
    }
    return QPair<QPair<double, double>, QString>(qMakePair<double, double>(0.0,0.0),"");
}

double Statistics::getError_EUMUEMAX() const
{
    return eu_mue_max;
}

QString Statistics::getUnit(const QString &type) const
{
    if (units.contains(type))
    {
        return units[type];
    }
    throw WrongArgumentException();
}

double Statistics::getErrorUnit(const QString &type) const
{
    if (error_units.contains(type))
    {
        return error_units[type];
    }
    throw WrongArgumentException();
}

double Statistics::getFitness() const
{
    double value = eu*(1.0-maxErrorRatio)+eu_mue_max*maxErrorRatio;
//    int nunconverged = unconverged.size();
//    value += nunconverged*1000000;
    return value;
}
int Statistics::getNumberOfImaginaryFrequencyTargets() const
{
    return numberOfImaginaryFrequencyTargets;
}

void Statistics::setNumberOfImaginaryFrequencyTargets(int value)
{
    numberOfImaginaryFrequencyTargets = value;
}

void Statistics::addImaginaryFrequencyTarget()
{
    numberOfImaginaryFrequencyTargets++;
}

void Statistics::clearImaginaryFrequencyTarget()
{
    numberOfImaginaryFrequencyTargets = 0;
}
double Statistics::getMaxErrorRatio() const
{
    return maxErrorRatio;
}

void Statistics::setMaxErrorRatio(double value)
{
    maxErrorRatio = value;
}



}






