#include "MolecularDataComparator.h"
#include "DFTBTestSuite/InputFile.h"
#include <QStringList>

namespace DFTBTestSuite
{
MolecularDataComparator::MolecularDataComparator(QObject *parent) :
    QObject(parent)
{
    reset();
}
void MolecularDataComparator::reset()
{
    m_isFrequencyResultAvailable = false;
    m_isAtomizationEnergyResultAvailable = false;
    m_isGeometryResultAvailable = false;
    bondLengthResults.clear();
    bondAngleResults.clear();
    torsionAngleResults.clear();
}

void MolecularDataComparator::compareGeometry(const GeometryProperty &gp, const QStringList& interestedElements, const GeometryData &calc_data)
{
    if (!calc_data.optimized)
    {
        m_isGeometryResultAvailable = false;
        return;
    }

    {
        for(int i=0; i<gp.bondLengths.size();++i)
        {
            int index1 = gp.bondLengths[i].start;
            int index2 = gp.bondLengths[i].end;

            double refValue = gp.bondLengths[i].length;
            double calcValue = calc_data.geometry.getDistance(index1, index2);
            QString elem1 = calc_data.geometry.getAtomSymbol(index1-1);
            QString elem2 = calc_data.geometry.getAtomSymbol(index2-1);

            double weight = 1.0;
            if (!interestedElements.contains(elem1) and !interestedElements.contains(elem2))
                weight = 0.0;

            auto pair = std::make_tuple(gp.bondLengths[i], ScalarResultDouble{refValue, calcValue}, weight);

            bondLengthResults.append(pair);

        }

    }
    {
        for(int i=0; i<gp.nonCovalentBondLengths.size();++i)
        {
            int index1 = gp.nonCovalentBondLengths[i].start;
            int index2 = gp.nonCovalentBondLengths[i].end;

            double refValue = gp.nonCovalentBondLengths[i].length;
            double calcValue = calc_data.geometry.getDistance(index1, index2);
            QString elem1 = calc_data.geometry.getAtomSymbol(index1-1);
            QString elem2 = calc_data.geometry.getAtomSymbol(index2-1);

            double weight = 1.0;
            if (!interestedElements.contains(elem1) and !interestedElements.contains(elem2))
                weight = 0.0;


            auto pair = std::make_tuple(gp.nonCovalentBondLengths[i], ScalarResultDouble{refValue, calcValue}, weight);

            nonCovalentBondLengthResults.append(pair);

        }

    }
    {
        for(int i=0; i<gp.bondAngles.size();++i)
        {
            int index1 = gp.bondAngles[i].start;
            int index2 = gp.bondAngles[i].middle;
            int index3 = gp.bondAngles[i].end;


            double refValue = gp.bondAngles[i].angle;
            double calcValue = calc_data.geometry.getAngle(index1, index2, index3);
            QString elem1 = calc_data.geometry.getAtomSymbol(index1-1);
            QString elem2 = calc_data.geometry.getAtomSymbol(index2-1);
            QString elem3 = calc_data.geometry.getAtomSymbol(index3-1);

            double weight = 1.0;
            if (!interestedElements.contains(elem1) and !interestedElements.contains(elem2)
                    and !interestedElements.contains(elem3))
                weight = 0.0;

            auto pair = std::make_tuple(gp.bondAngles[i], ScalarResultDouble{refValue, calcValue}, weight);

            bondAngleResults.append(pair);

        }
    }
    {
        for(int i=0; i< gp.torsionAngles.size();++i)
        {
            int index1 = gp.torsionAngles[i].start;
            int index2 = gp.torsionAngles[i].start_middle;
            int index3 = gp.torsionAngles[i].middle_end;
            int index4 = gp.torsionAngles[i].end;

            double refValue = gp.torsionAngles[i].angle;
            double calcValue = calc_data.geometry.getTorsion(index1, index2, index3, index4);
            QString elem1 = calc_data.geometry.getAtomSymbol(index1-1);
            QString elem2 = calc_data.geometry.getAtomSymbol(index2-1);
            QString elem3 = calc_data.geometry.getAtomSymbol(index3-1);
            QString elem4 = calc_data.geometry.getAtomSymbol(index4-1);

            double weight = 1.0;
            if (!interestedElements.contains(elem1) and !interestedElements.contains(elem2)
                    and !interestedElements.contains(elem3) and !interestedElements.contains(elem4))
                weight = 0.0;

//            double error;
//            double tmp_CV, tmp_RV;
//            tmp_CV = calcValue + 180.0;
//            tmp_RV = refValue + 180.0;
//            error = std::min(std::abs<double>(tmp_CV-tmp_RV), (360.0-std::abs<double>(tmp_CV-tmp_RV)));


            auto pair = std::make_tuple(gp.torsionAngles[i], ScalarResultDouble{refValue, calcValue}, weight);

            torsionAngleResults.append(pair);
        }
    }
    m_isGeometryResultAvailable = true;
}

void MolecularDataComparator::compareAtomizationEnergy(const AtomizationEnergyData &ref_data, const AtomizationEnergyData &calc_data)
{
    if (calc_data.isValid && ref_data.isValid)
    {
        double refValue = ref_data.energy;
        double calcValue = calc_data.energy;

        m_isAtomizationEnergyResultAvailable = true;
        atomizationEnergyResult.reference_result = refValue;
        atomizationEnergyResult.calculated_result = calcValue;
        return;
    }
    m_isAtomizationEnergyResultAvailable = false;
}
bool MolecularDataComparator::punishImaginaryFrequency() const
{
    return m_punishImaginaryFrequency;
}

void MolecularDataComparator::setPunishImaginaryFrequency(bool punishImaginaryFrequency)
{
    m_punishImaginaryFrequency = punishImaginaryFrequency;
}

ScalarResultDouble MolecularDataComparator::getAtomizationEnergyResult() const
{
    return atomizationEnergyResult;
}
ScalarResultDouble MolecularDataComparator::getZPEResult() const
{
    return ZPEResult;
}
ScalarListResultDouble MolecularDataComparator::getFrequencyResult() const
{
    return frequencyResult;
}
QList<std::tuple<TorsionAngleEntry, ScalarResultDouble, double> > MolecularDataComparator::getTorsionAngleResults() const
{
    return torsionAngleResults;
}
QList<std::tuple<BondAngleEntry, ScalarResultDouble, double> > MolecularDataComparator::getBondAngleResults() const
{
    return bondAngleResults;
}
QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > MolecularDataComparator::getBondLengthResults() const
{
    return bondLengthResults;
}

QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > MolecularDataComparator::getNonCovalentBondLengthResults() const
{
    return nonCovalentBondLengthResults;
}

void MolecularDataComparator::compareFrequency(const FrequencyData &ref_data, const FrequencyData &calc_data)
{
    if (calc_data.isValid && ref_data.isValid)
    {
        m_isFrequencyResultAvailable = true;

        if (calc_data.frequencies.size() == ref_data.frequencies.size())
        {
            const QList<double>& calcValues = calc_data.frequencies;
            const QList<double>& refValues = ref_data.frequencies;

//            const QList<double>& weights = ref_data.weights;

            double refZPE = CalcZPE(refValues);
            double calcZPE = CalcZPE(calcValues);


            frequencyResult.reference_result = refValues;
            frequencyResult.calculated_result = calcValues;

            ZPEResult.calculated_result = calcZPE;
            ZPEResult.reference_result = refZPE;
            m_isZPEResultAvailable = true;

        }
        else
        {
            m_isZPEResultAvailable = false;
        }
        return;
    }
    m_isFrequencyResultAvailable = false;
}
bool MolecularDataComparator::isZPEResultAvailable() const
{
    return m_isZPEResultAvailable;
}

bool MolecularDataComparator::isGeometryResultAvailable() const
{
    return m_isGeometryResultAvailable;
}

bool MolecularDataComparator::compare(const MolecularData &ref_data, const MolecularData &calc_data, const TestMolecule& testMolecule)
{
    reset();
    GeometryProperty gp;

    for(int i=0; i<testMolecule.testers.size();++i)
    {
        if (testMolecule.testers[i].name == "atomization_energy")
        {
            compareAtomizationEnergy(ref_data.atomizationEnergyData, calc_data.atomizationEnergyData);
        }
        if (testMolecule.testers[i].name == "frequency")
        {
            compareFrequency(ref_data.frequencyData, calc_data.frequencyData);
        }
        if (testMolecule.testers[i].name == "molecular_geometry")
        {
            QStringList interestedElements;
            if (testMolecule.testers[i].options.contains("interested_elements"))
            {
                if (testMolecule.testers[i].options["interested_elements"].canConvert(QMetaType::QStringList))
                {
                    interestedElements = testMolecule.testers[i].options["interested_elements"].toStringList();
                }
            }

            calcGeometryProperties(ref_data.geometryData.geometry, gp);
            compareGeometry(gp, interestedElements, calc_data.geometryData);
        }

    }
    return true;
}
bool MolecularDataComparator::isAtomizationEnergyResultAvailable() const
{
    return m_isAtomizationEnergyResultAvailable;
}
bool MolecularDataComparator::isFrequencyResultAvailable() const
{
    return m_isFrequencyResultAvailable;
}


}
