#ifndef MOLECULARDATACOMPARATOR_H
#define MOLECULARDATACOMPARATOR_H

#include <QObject>
#include "DFTBTestSuite/ReferenceDataType/MolecularData.h"
#include <tuple>

namespace DFTBTestSuite
{
    template <typename T>
    struct ScalarResult
    {
        T reference_result;
        T calculated_result;
    };

    template <typename T>
    struct ScalarListResult
    {
        QList<T> reference_result;
        QList<T> calculated_result;
    };

    template <typename T>
    struct WeightedScalarListResult : public ScalarListResult<T>
    {
        QList<T> weight;
    };


    typedef ScalarResult<double> ScalarResultDouble;
    typedef ScalarListResult<double> ScalarListResultDouble;
    typedef WeightedScalarListResult<double> WeightedScalarListResultDouble;

    class TestMolecule;
class MolecularDataComparator : public QObject
{
    friend class Formatter;
    Q_OBJECT
public:
    explicit MolecularDataComparator(QObject *parent = 0);

    bool isFrequencyResultAvailable() const;
    bool isAtomizationEnergyResultAvailable() const;
    bool isGeometryResultAvailable() const;
    bool isZPEResultAvailable() const;

    QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > getBondLengthResults() const;
    QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > getNonCovalentBondLengthResults() const;
    QList<std::tuple<BondAngleEntry, ScalarResultDouble, double> > getBondAngleResults() const;
    QList<std::tuple<TorsionAngleEntry, ScalarResultDouble, double> > getTorsionAngleResults() const;
    ScalarListResultDouble getFrequencyResult() const;
    ScalarResultDouble getZPEResult() const;
    ScalarResultDouble getAtomizationEnergyResult() const;


    bool punishImaginaryFrequency() const;
    void setPunishImaginaryFrequency(bool punishImaginaryFrequency);

signals:

public slots:
    bool compare(const MolecularData& ref_data, const MolecularData& calc_data, const TestMolecule& testMolecule);

private:
    void reset();
    void compareGeometry(const GeometryProperty& gp, const QStringList& interestedElements, const GeometryData& calc_data);
    void compareFrequency(const FrequencyData& ref_data, const FrequencyData& calc_data);
    void compareAtomizationEnergy(const AtomizationEnergyData& ref_data, const AtomizationEnergyData& calc_data);
    bool m_isFrequencyResultAvailable = false;
    bool m_isAtomizationEnergyResultAvailable = false;
    bool m_isGeometryResultAvailable = false;
    bool m_isZPEResultAvailable = false;

    QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > bondLengthResults;
    QList<std::tuple<BondLengthEntry, ScalarResultDouble, double> > nonCovalentBondLengthResults;
    QList<std::tuple<BondAngleEntry, ScalarResultDouble, double> > bondAngleResults;
    QList<std::tuple<TorsionAngleEntry, ScalarResultDouble, double> > torsionAngleResults;
    ScalarListResultDouble frequencyResult;
    ScalarResultDouble ZPEResult;
    ScalarResultDouble atomizationEnergyResult;

    bool m_punishImaginaryFrequency = false;

};

}

#endif // MOLECULARDATACOMPARATOR_H
