#ifndef PROPERTYTESTER
#define PROPERTYTESTER

#include "Singleton.h"
#include <QString>
#include <memory>
#include <QVariant>


namespace DFTBTestSuite
{
    class PropertyTester
    {
        friend class PropertyTesterFactory;
    private:
        PropertyTester();
    protected:
        QVariantMap options;
        double weight = 1.0;
    public:
        virtual QString getName() const = 0;
        virtual void setWeight(double weight)
        {
            this->weight = weight;
        }

        virtual double getWeight() const
        {
            return weight;
        }
        void setOptions(const QVariantMap& options)
        {
            this->options = options;
        }

        virtual QVariantMap getOptions() const
        {
            return this->options;
        }


    public:
        virtual bool doTest() = 0;
    };

    class MolecularGeometryTester : public PropertyTester
    {
        // PropertyTester interface
    public:
        virtual QString getName() const;
        virtual bool doTest();
    };

    class PropertyTesterFactory
    {
        friend class Singleton<PropertyTesterFactory>;
    private:
        PropertyTesterFactory(){}
    public:
        std::shared_ptr<PropertyTester> getTester(const QString& name);
    };

}


#endif // PROPERTYTESTER

