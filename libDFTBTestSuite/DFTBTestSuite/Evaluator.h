#ifndef EVALUATOR_H
#define EVALUATOR_H

#include "DFTBTestSuite/InputFile.h"
#include "DFTBTestSuite/ReferenceDataType/AtomicData.h"
#include "DFTBTestSuite/ReferenceDataType/MolecularData.h"
#include "DFTBTestSuite/ReferenceDataType/CrystalData.h"

namespace DFTBTestSuite
{

class DFTBResultInfo
{
public:
    bool SCF_Converged = false;
    bool Geometry_Converged = false;
    bool UnknownError = false;
    bool failed = false;
    QString detail;
};

class OptionalEvaluationInformation
{
    using dVector3 = std::array<double, 3> ;
public:
    QMap<QString, dVector3> hubbardDerivatives;
    QMap<QString, QVariantList> spinConstants;
    QVariantMap dispersion;
    bool orbital_resolved_scc = false;
    bool shell_resolved_spin = true;
};

class Evaluation;
class Evaluator
{
public:
    explicit Evaluator(QTextStream& output_stream, const Evaluation& eval_opts, const QString& codePath);
    virtual ~Evaluator();
public:
    virtual bool supportDirectFrequencyCalculation() const = 0;
public:
    virtual void EvaluateAtomicEnergy(AtomicData& calc, const QString& template_input_filename, const OptionalEvaluationInformation& opt_info, DFTBResultInfo& info) const = 0 ;
    virtual void EvaluateGeometry(const MolecularData& ref, const OptionalEvaluationInformation &opt_info, MolecularData& calc, DFTBResultInfo& info) const = 0 ;
    virtual void EvaluateFrequency(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData& calc, DFTBResultInfo& info) const = 0 ;
    virtual void EvaluateMolecularEnergy(const MolecularData& ref, const OptionalEvaluationInformation& opt_info, MolecularData& calc, DFTBResultInfo& info) const = 0;
    virtual void EvaluateBandStructure(const CrystalGeometryData& ref_geom, const BandStructureData& ref_data, const CrystalEnergyData& calc_energy_data, const OptionalEvaluationInformation& opt_info, BandStructureData& calc_data, DFTBResultInfo& info) const = 0;
    virtual void EvaluateCrystalEnergy(const CrystalGeometryData& ref_geom, const OptionalEvaluationInformation& opt_info, CrystalEnergyData& calc, DFTBResultInfo& info) const = 0 ;

public:
    QDir workDir() const;
    void setWorkDir(const QDir &workDir);

protected:
    QTextStream& m_output_stream;
    Evaluation m_evaluation;
    QDir m_workDir = QDir::temp();
    QString m_program_path;
};

}

#endif // EVALUATOR_H


