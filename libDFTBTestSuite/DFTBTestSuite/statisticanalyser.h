#ifndef STATISTICANALYSER_H
#define STATISTICANALYSER_H

#include <QList>
#include <QPair>
#include <QMap>
#include <QString>

#include <QException>


namespace DFTBTestSuite{

class ErrorUnit;

class ErrorEntry
{
public:
    double ref_value;
    double calc_value;
    double weight = 1.0;
    ErrorEntry(double ref_value_, double calc_value_, double weight_ = 1.0)
    {
        ref_value = ref_value_;
        calc_value = calc_value_;
        weight = weight_;
    }
};


class Statistics
{
public:
    class GroupAlreadyBeganException : public QException
    {
    public:
        void raise() const
        {
            throw *this;
        }
        QException *clone() const
        {
            return new GroupAlreadyBeganException(*this);
        }
    };
    class GroupNotBegunException : public QException
    {
    public:
        void raise() const
        {
            throw *this;
        }
        QException *clone() const
        {
            return new GroupNotBegunException(*this);
        }
    };

    class WrongArgumentException : public QException
    {
    public:
        void raise() const
        {
            throw *this;
        }
        QException *clone() const
        {
            return new WrongArgumentException(*this);
        }
    };


public:
    Statistics(const ErrorUnit* errorUnit);

    void beginGroup(const QString& type, const QString &uuid, const QString& name, double weight = 1.0);
    void endGroup();
    void addValue(double ref_value, double calc_value, double weight);


    void compute();
    double getError_MSE(const QString& type) const;
    double getError_MUE(const QString& type) const;
    double getError_RMS(const QString& type) const;
    double getError_MAPE(const QString& type) const;

    double getError_MSE(const QString& type, const QString& groupUUID) const;
    double getError_MUE(const QString& type, const QString& groupUUID) const;
    double getError_RMS(const QString& type, const QString& groupUUID) const;

    double getError_MAX(const QString& type, const QString& groupUUID) const;
    QPair<double, double> getError_MAX(const QString& type) const;


    QList<QString> getError_UUIDs(const QString& type) const;
    QString getError_Name(const QString& UUID) const;



    int getError_Count(const QString& type) const;
    QList<QString> getTypes() const;
    void reset();

    double getError_EUMSE() const;
    double getError_EU() const;
    double getError_EURMS() const;

    double getErrorComp_EUMSE(const QString& type) const;
    double getErrorComp_EU(const QString& type) const;
    double getErrorComp_EURMS(const QString& type) const;

//    const ErrorUnit* errorUnit;



    void addUnconvergedTarget(const QString& name);
    void addConvergedTarget(const QString& name);

    const QList<QString>& getUnconvergedTarget() const;

    const QList<QString>& getConvergedTarget() const;

    QPair<QPair<double, double>, QString> getError_MUEMAX(const QString& type) const;
    double getError_EUMUEMAX() const;
    QString getUnit(const QString& type) const;
    double getErrorUnit(const QString& type) const;
    double getFitness() const;


    int getNumberOfImaginaryFrequencyTargets() const;
    void setNumberOfImaginaryFrequencyTargets(int value);
    void addImaginaryFrequencyTarget();
    void clearImaginaryFrequencyTarget();

    double getMaxErrorRatio() const;
    void setMaxErrorRatio(double value);

    const ErrorUnit* m_error_unit;

private:
    double maxErrorRatio = 0.05;
    QString currentType;
    QString currentName;
    QString currentUUID;
    double currentWeight;
    bool inGroup = false;
    QMap<QString, QString> m_uuid_name;



    QMap<QString, QMap<QString, QList<ErrorEntry> > > result;
    QMap<QString, double > mse_type;
    QMap<QString, double > mue_type;
    QMap<QString, double > rms_type;
    QMap<QString, double > mape_type;


    QMap<QString, QMap<QString, double > > mse_type_group;
    QMap<QString, QMap<QString, double > > mue_type_group;
    QMap<QString, QMap<QString, double > > rms_type_group;
    QMap<QString, QMap<QString, double > > mape_type_group;
    QMap<QString, QMap<QString, double > > max_type_group;


    double eu = 0.0;
    QMap<QString, double> eu_comp;


    QList<QString> unconverged;
    QList<QString> converged;

    QMap<QString, QPair<double, double> > mue_max;
    QMap<QString, QString> mue_max_name;
    QMap<QString, QString> units;

    QMap<QString, double> error_units;

    double eu_mue_max = 0.0;

    int numberOfImaginaryFrequencyTargets = 0;

    bool m_isComputed = false;
};

}
#endif // STATISTICANALYSER_H
