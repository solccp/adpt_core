#include "textformatter.h"
#include "typesettings.h"

#include "statisticanalyser.h"

#include "DFTBTestSuite/ReferenceDataType/MolecularData.h"
#include "DFTBTestSuite/ReferenceDataType/ReactionData.h"

#include "Evaluator.h"
#include <QDebug>

#include "Testing/MolecularDataComparator.h"

#include "Testing/propertytester.h"

#include "constants.h"
#include <tuple>

namespace DFTBTestSuite{

TextFormatter::TextFormatter(Statistics* stat, QObject *parent) :
    Formatter(parent), m_stat(stat)
{
}

void TextFormatter::writeMolecule(const MolecularData& ref_data, const MolecularData& calc_data, const MolecularDataComparator *comparator, const TestMolecule &testMolecule, const DFTBResultInfo &info)
{
    m_stream << EQUALLINE << endl << endl
             << INDENT_SPACES << "Molecule: " << ref_data.name << endl << endl
             << INDENT_SPACES << "Charge: " << ref_data.charge << endl
             << INDENT_SPACES << "Spin Multiplicity: " << ref_data.spin << endl << endl;



    if (calc_data.totalEnergyData.isValid)
    {
        m_stream << INDENT_SPACES << QString("Total Energy: %1").arg(calc_data.totalEnergyData.energy, 14, 'f', 8) << " " << calc_data.totalEnergyData.energy_unit << endl << endl;
    }

    bool TestGeometry = false;
    bool TestFrequency = false;
    bool TestAtomizationEnergy = false;
    double WeightGeometry = 1.0;
    double WeightFrequency = 1.0;
    double WeightAtomizationEnergy = 1.0;


    for(int i=0; i<testMolecule.testers.size();++i)
    {
        if (testMolecule.testers[i].name == "atomization_energy")
        {
            TestAtomizationEnergy = true;
            WeightAtomizationEnergy = testMolecule.testers[i].weight;
        }
        if (testMolecule.testers[i].name == "frequency")
        {
            TestFrequency = true;
            WeightFrequency = testMolecule.testers[i].weight;
        }
        if (testMolecule.testers[i].name == "molecular_geometry")
        {
            TestGeometry = true;
            WeightGeometry = testMolecule.testers[i].weight;
        }

    }


    if (comparator->isGeometryResultAvailable() && (TestGeometry || TestFrequency))
    {
        if (WeightGeometry > 0.0001)
        {
            this->m_stat->addConvergedTarget(ref_data.name);
        }

        QString methodbasis;
        if (ref_data.geometryData.basisSet.isEmpty())
        {
            methodbasis = QString("%1").arg(ref_data.geometryData.method);
        }
        else
        {
            methodbasis = QString("%1/%2").arg(ref_data.geometryData.method, ref_data.geometryData.basisSet);
        }


        m_stream << endl << INDENT_SPACES << "Geometry in XYZ format" << endl
                    << HYPHENLINE << endl
                    << INDENT_SPACES << "Reference:(" << methodbasis << ")" << endl;

        m_stream << ref_data.geometryData.geometry.toXYZ();


        m_stream << endl;

        m_stream << INDENT_SPACES << "Calculated:" << endl;

        m_stream << calc_data.geometryData.geometry.toXYZ();

        m_stream << HYPHENLINE << endl;



        m_stream << endl;

        if (TestGeometry)
        {

            m_stream << INDENT_SPACES
                     << "Geometrical Parameters(Angstrom, Degrees): Weight: " << WeightGeometry << endl;


            m_stream << INDENT_SPACES
                     << QString("%1%2 (%4 )%3 %5").arg("",-28).arg("Calculated",13).arg("Reference",13).arg("Error",8).arg("Weight",10) << endl;

            QList<QString> elements = ref_data.geometryData.geometry.getElements();


            {
                const auto& list = comparator->getBondLengthResults() ;

                m_stat->beginGroup("BondLength", ref_data.uuid.toString(), ref_data.name, WeightGeometry);
                for(int i=0; i<list.size();++i)
                {
                    const auto& pair = list.at(i);

                    int index1 = std::get<0>(pair).start;
                    int index2 = std::get<0>(pair).end;
                    QString sym1 = elements[index1-1];
                    QString sym2 = elements[index2-1];

                    double refValue = std::get<1>(pair).reference_result;
                    double calcValue = std::get<1>(pair).calculated_result;
                    double weight = std::get<2>(pair);

                    QString header = QString("R[%1(%2),%3(%4)]:")
                            .arg(sym1).arg(index1)
                            .arg(sym2).arg(index2);

                    QString error_str;
                    error_str.sprintf("%+8.3lf", calcValue-refValue);
                    m_stream << INDENT_SPACES
                             << QString("%1%2 (%4 )%3 %5").arg(header,-28)
                                .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(error_str,8)
                                .arg(weight, 10, 'f', 4)<< endl;

                    m_stat->addValue(refValue, calcValue, weight);
                }
                m_stat->endGroup();

            }
            {
                const auto& list = comparator->getNonCovalentBondLengthResults() ;

                m_stat->beginGroup("BondLength", ref_data.uuid.toString(), ref_data.name, WeightGeometry);
                for(int i=0; i<list.size();++i)
                {
                    const auto& pair = list.at(i);


                    int index1 = std::get<0>(pair).start;
                    int index2 = std::get<0>(pair).end;
                    QString sym1 = elements[index1-1];
                    QString sym2 = elements[index2-1];

                    double refValue = std::get<1>(pair).reference_result;
                    double calcValue = std::get<1>(pair).calculated_result;
                    double weight = std::get<2>(pair);

                    QString header = QString("HB-R[%1(%2),%3(%4)]:")
                            .arg(sym1).arg(index1)
                            .arg(sym2).arg(index2);

                    QString error_str;
                    error_str.sprintf("%+8.3lf", calcValue-refValue);
                    m_stream << INDENT_SPACES
                             << QString("%1%2 (%4 )%3 %5").arg(header,-28)
                                .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(error_str,8)
                                .arg(weight, 10, 'f', 4) << endl;

                    m_stat->addValue(refValue, calcValue, weight);
                }
                m_stat->endGroup();

            }
            {
                const auto& list = comparator->getBondAngleResults() ;
                m_stat->beginGroup("BondAngle", ref_data.uuid.toString(), ref_data.name, WeightGeometry);
                for(int i=0; i<list.size();++i)
                {
                    const auto& pair = list.at(i);


                    int index1 = std::get<0>(pair).start;
                    int index2 = std::get<0>(pair).middle;
                    int index3 = std::get<0>(pair).end;
                    QString sym1 = elements[index1-1];
                    QString sym2 = elements[index2-1];
                    QString sym3 = elements[index3-1];

                    double refValue = std::get<1>(pair).reference_result;
                    double calcValue = std::get<1>(pair).calculated_result;
                    double weight = std::get<2>(pair);

                    QString header = QString("A[%1(%2),%3(%4),%5(%6)]:")
                            .arg(sym1).arg(index1)
                            .arg(sym2).arg(index2)
                            .arg(sym3).arg(index3);

                    QString error_str;
                    error_str.sprintf("%+8.3lf", calcValue-refValue);
                    m_stream << INDENT_SPACES
                             << QString("%1%2 (%4 )%3 %5").arg(header,-28)
                                .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(error_str,8)
                             .arg(weight, 10, 'f', 4)<< endl;

                    m_stat->addValue(refValue, calcValue, weight);
                }
                m_stat->endGroup();
            }
            {

                const auto& list = comparator->getTorsionAngleResults() ;
                m_stat->beginGroup("TorsionAngle", ref_data.uuid.toString(), ref_data.name, WeightGeometry);
                for(int i=0; i< list.size();++i)
                {
                    const auto& pair = list.at(i);

                    int index1 = std::get<0>(pair).start;
                    int index2 = std::get<0>(pair).start_middle;
                    int index3 = std::get<0>(pair).middle_end;
                    int index4 = std::get<0>(pair).end;
                    QString sym1 = elements[index1-1];
                    QString sym2 = elements[index2-1];
                    QString sym3 = elements[index3-1];
                    QString sym4 = elements[index4-1];

                    double refValue = std::get<1>(pair).reference_result;
                    double calcValue = std::get<1>(pair).calculated_result;
                    double weight = std::get<2>(pair);


                    QString header = QString("D[%1(%2),%3(%4),%5(%6),%7(%8)]:")
                            .arg(sym1).arg(index1)
                            .arg(sym2).arg(index2)
                            .arg(sym3).arg(index3)
                            .arg(sym4).arg(index4);

                    double tmp_CV, tmp_RV;
                    tmp_CV = calcValue;
                    tmp_RV = refValue;

                    if (qAbs(tmp_RV - tmp_CV) > 180.0)
                    {
                        if (tmp_CV < 0 )
                            tmp_CV += 360.0;
                        else
                            tmp_CV -= 360.0;
                    }


                    double error = -(tmp_RV - tmp_CV);

    //                bool minus = (error < 0.0);
                    QString error_str;
                    error_str.sprintf("%+8.3lf", error);
                    m_stream << INDENT_SPACES
                             << QString("%1%2 (%4 )%3 %5").arg(header,-28)
                                .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(error_str,8)
                                .arg(weight, 10, 'f', 4) << endl;




                    m_stat->addValue(tmp_RV, tmp_CV, weight);
                }
                m_stat->endGroup();
            }
        }
    } //end of geometry
    else
    {
        if (TestGeometry)
        {
            this->m_stat->addUnconvergedTarget(ref_data.name);
            m_stream << endl << INDENT_SPACES << "Warning:"
                     << INDENT_SPACES << INDENT_SPACES << "This geometry optimization of this molecule is NOT converged" << endl
                     << INDENT_SPACES << INDENT_SPACES << "or performed, comparason will be skipped." << endl;
        }
    }


    if (comparator->isFrequencyResultAvailable() && TestFrequency)
    {

        const auto& result = comparator->getFrequencyResult();


        if (comparator->isZPEResultAvailable())
        {
            QString methodbasis;
            if (ref_data.frequencyData.basisSet.isEmpty())
            {
                methodbasis = QString("(%1)").arg(ref_data.frequencyData.method);
            }
            else
            {
                methodbasis = QString("(%1/%2)").arg(ref_data.frequencyData.method, ref_data.frequencyData.basisSet);
            }

            m_stream << endl
                     << INDENT_SPACES
                     << "Frequencies(cm^-1): " <<  "Weight: " << WeightFrequency << endl;
            m_stream << INDENT_SPACES
                     << QString("%5 %1 (%3)%2%4")
                        .arg("Calculated",13)
                        .arg("Reference",13)
                        .arg("Error[Percentage]",22).arg(methodbasis).arg("Weight", 6)  << endl;

            const QList<double>& calcValues = result.calculated_result;
            const QList<double>& refValues = result.reference_result;
            const QList<double>& weights = ref_data.frequencyData.weights;


            double weight = 1.0;

            int size = calcValues.size();
            m_stat->beginGroup("Frequencies", ref_data.uuid.toString(), ref_data.name, WeightFrequency);
            for(int i=0; i<size;++i)
            {
                if (!weights.empty() && weights.size() == refValues.size())
                {
                    weight = weights[i];
                }
                QString error_str;
                QString error_str2;
                double error_scaled = 100.0*(refValues[i]-calcValues[i])/refValues[i];
                double error = calcValues[i]-refValues[i];

                error_str.sprintf("%+10.3lf", error);
                error_str2.sprintf("%+10.3lf", error_scaled);
                m_stream << INDENT_SPACES
                         << QString("%5 %1 (%3 [%4 ])%2")
                            .arg(calcValues[i],13,'f',4).arg(refValues[i],13,'f',4)
                            .arg(error_str,10)
                            .arg(error_str2,10)
                            .arg(weight, 6) << endl;
                if (weight > 1.0e-4)
                {
                    m_stat->addValue(refValues[i], calcValues[i], weight);
                }
            }
            m_stat->endGroup();

            m_stream << endl
                     << INDENT_SPACES << "Zero-point Energy(kcal/mol):" << endl;
            m_stream << QString("%1 (%3)%2").arg("Calculated",13).arg("Reference",13).arg("Error",10) << endl;

            double refZPE = comparator->getZPEResult().reference_result;
            double calcZPE = comparator->getZPEResult().calculated_result;


            QString error_str;
            error_str.sprintf("%+10.3lf", calcZPE-refZPE);

            m_stream << QString("%1 (%3 )%2")
                        .arg(calcZPE,13,'f',4).arg(refZPE,13,'f',4).arg(error_str,10) << endl;


            for(int i=0; i<calcValues.size();++i)
            {
                if (calcValues[i]<0.0)
                {
                    m_stat->addImaginaryFrequencyTarget();
                    break;
                }
            }
            if (comparator->punishImaginaryFrequency())
            {
                for(int i=0; i<calcValues.size();++i)
                {
                    if (calcValues[i]<0.0)
                    {
                        m_stream << "Imaginary frequency found!";
                        m_stat->addUnconvergedTarget(ref_data.name + "(Imaginary Frequency)");
                        break;
                    }
                }
            }
        }
        else
        {
            m_stream << "The number of frequency of the reference and the calculated molecule dismatched." << endl;
            m_stat->addUnconvergedTarget(ref_data.name + "(# of Normal Modes)");
        }
    }

    if (comparator->isAtomizationEnergyResultAvailable() && TestAtomizationEnergy)
    {
        QString methodbasis;
        if (ref_data.atomizationEnergyData.basisSet.isEmpty())
        {
            methodbasis = QString("(%1)").arg(ref_data.atomizationEnergyData.method);
        }
        else
        {
            methodbasis = QString("(%1/%2)").arg(ref_data.atomizationEnergyData.method, ref_data.atomizationEnergyData.basisSet);
        }
        double refValue = comparator->getAtomizationEnergyResult().reference_result;
        double calcValue = EnergyAUToAny(ref_data.atomizationEnergyData.energy_unit, comparator->getAtomizationEnergyResult().calculated_result);

        m_stream << endl
                 << INDENT_SPACES << QString("Atomization Energy without ZPE correction(%1):").arg(ref_data.atomizationEnergyData.energy_unit) << " Weight: " << WeightAtomizationEnergy
                 << endl;

        m_stream << QString("%1 (%3)%2%4").arg("Calculated",13).arg("Reference",13).arg("Error",10).arg(methodbasis) << endl;

        QString error_str;
        error_str.sprintf("%+10.3lf", calcValue-refValue);

        m_stream << QString("%1 (%3 )%2")
                    .arg(calcValue,13,'f',4).arg(refValue,13,'f',4).arg(error_str,10) << endl;

        m_stat->beginGroup("AtomizationEnergy", ref_data.uuid.toString(), ref_data.name, WeightAtomizationEnergy);
        m_stat->addValue(refValue, calcValue, 1.0);
        m_stat->endGroup();
    }

    m_stream << EQUALLINE << endl;
}

//void TextFormatter::writeMolecule(const MolecularData &ref_data, const MolecularData &calc_data, const DFTBResultInfo &info, bool TestGeometry = true)
//{
//    m_stream << EQUALLINE << endl << endl
//        << INDENT_SPACES << "Molecule: " << ref_data.name << endl << endl;

//    if (calc_data.totalEnergyData.isValid)
//    {
//        m_stream << INDENT_SPACES << QString("Total Energy: %1 H").arg(calc_data.totalEnergyData.energy,20,'f',12) << endl;
//    }

//    if (calc_data.geometryData.optimized)
//    {
//        this->m_stat->addConvergedTarget(calc_data.name);



//        QString methodbasis;
//        if (ref_data.geometryData.basisSet.isEmpty())
//        {
//            methodbasis = QString("%1").arg(ref_data.geometryData.method);
//        }
//        else
//        {
//            methodbasis = QString("%1/%2").arg(ref_data.geometryData.method, ref_data.geometryData.basisSet);
//        }


//        if (TestGeometry)
//        {
//            m_stream << endl;
//            m_stream << INDENT_SPACES
//                     << "Geometrical Parameters(Angstrom, Degrees):" << endl
//                     << INDENT_SPACES << "Reference: " << methodbasis << endl;

//            m_stream << INDENT_SPACES
//                << QString("%1%2(%4)%3").arg("",-28).arg("Calculated",13).arg("Reference",13).arg("Error",8) << endl;

//            GeometryProperty gp;
//            calcGeometryProperties(ref_data.geometryData.geometry, gp);

//            QList<QString> elements = ref_data.geometryData.geometry.getElements();
//            {
//                for(int i=0; i<gp.bondLengths.size();++i)
//                {
//                    int index1 = gp.bondLengths[i].start;
//                    int index2 = gp.bondLengths[i].end;
//                    QString sym1 = elements[index1-1];
//                    QString sym2 = elements[index2-1];

//                    double refValue = gp.bondLengths[i].length;
//                    double calcValue = calc_data.geometryData.geometry.getDistance(index1, index2);

//                    QString header = QString("R[%1(%2),%3(%4)]:")
//                            .arg(sym1).arg(index1)
//                            .arg(sym2).arg(index2);

//                    m_stream << INDENT_SPACES
//                        << QString("%1%2(%4)%3").arg(header,-28)
//                           .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(calcValue-refValue,8,'f',3) << endl;

//                    m_stat->addValue("BondLength", calcValue-refValue, calc_data.name);

//                }

//            }
//            {
//                for(int i=0; i<gp.bondAngles.size();++i)
//                {
//                    int index1 = gp.bondAngles[i].start;
//                    int index2 = gp.bondAngles[i].middle;
//                    int index3 = gp.bondAngles[i].end;
//                    QString sym1 = elements[index1-1];
//                    QString sym2 = elements[index2-1];
//                    QString sym3 = elements[index3-1];

//                    double refValue = gp.bondAngles[i].angle;
//                    double calcValue = calc_data.geometryData.geometry.getAngle(index1, index2, index3);

//                    QString header = QString("A[%1(%2),%3(%4),%5(%6)]:")
//                            .arg(sym1).arg(index1)
//                            .arg(sym2).arg(index2)
//                            .arg(sym3).arg(index3);

//                    m_stream << INDENT_SPACES
//                        << QString("%1%2(%4)%3").arg(header,-28)
//                           .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(calcValue-refValue,8,'f',3) << endl;

//                    m_stat->addValue("BondAngle", calcValue-refValue, calc_data.name);
//                }
//            }
//            {
//                for(int i=0; i< gp.torsionAngles.size();++i)
//                {
//                    int index1 = gp.torsionAngles[i].start;
//                    int index2 = gp.torsionAngles[i].start_middle;
//                    int index3 = gp.torsionAngles[i].middle_end;
//                    int index4 = gp.torsionAngles[i].end;
//                    QString sym1 = elements[index1-1];
//                    QString sym2 = elements[index2-1];
//                    QString sym3 = elements[index3-1];
//                    QString sym4 = elements[index4-1];

//                    double refValue = gp.torsionAngles[i].angle;
//                    double calcValue = calc_data.geometryData.geometry.getTorsion(index1, index2, index3, index4);


//                    QString header = QString("D[%1(%2),%3(%4),%5(%6),%7(%8)]:")
//                            .arg(sym1).arg(index1)
//                            .arg(sym2).arg(index2)
//                            .arg(sym3).arg(index3)
//                            .arg(sym4).arg(index4);

//                    double error;
//                    double tmp_CV, tmp_RV;
//                    tmp_CV = calcValue + 180.0;
//                    tmp_RV = refValue + 180.0;
//                    error = std::min(std::abs<double>(tmp_CV-tmp_RV), (360.0-std::abs<double>(tmp_CV-tmp_RV)));

//                    m_stream << INDENT_SPACES
//                        << QString("%1%2(%4)%3").arg(header,-28)
//                           .arg(calcValue,13,'f',6).arg(refValue,13,'f',6).arg(error,8,'f',3) << endl;

//                    m_stat->addValue("TorsionAngle", error, calc_data.name);
//                }
//            }

//        }
//    } //end of geometry
//    else
//    {
//        this->m_stat->addUnconvergedTarget(calc_data.name);
//        m_stream << endl << INDENT_SPACES << "Warning:"
//            << INDENT_SPACES << INDENT_SPACES << "This geometry optimization of this molecule is NOT converged" << endl
//            << INDENT_SPACES << INDENT_SPACES << "or performed, comparason will be skipped." << endl;
//    }


//    if (calc_data.frequencyData.isValid && ref_data.frequencyData.isValid)
//    {
//        if (calc_data.frequencyData.frequencies.size() == ref_data.frequencyData.frequencies.size())
//        {
//            QString methodbasis;
//            if (ref_data.frequencyData.basisSet.isEmpty())
//            {
//                methodbasis = QString("(%1)").arg(ref_data.frequencyData.method);
//            }
//            else
//            {
//                methodbasis = QString("(%1/%2)").arg(ref_data.frequencyData.method, ref_data.frequencyData.basisSet);
//            }

//            m_stream << endl
//                << INDENT_SPACES
//                << "Frequencies(cm^-1): " <<  endl;
//            m_stream << QString("%1(%3)%2%4").arg("Calculated",13).arg("Reference",13).arg("Error",10).arg(methodbasis)  << endl;

//            const QList<double>& calcValues = calc_data.frequencyData.frequencies;
//            const QList<double>& refValues = ref_data.frequencyData.frequencies;


//            int size = calcValues.size();
//            for(int i=0; i<size;++i)
//            {
//                m_stream << QString("%1(%3)%2")
//                       .arg(calcValues[i],13,'f',4).arg(refValues[i],13,'f',4).arg(calcValues[i]-refValues[i],10,'f',3) << endl;
//                m_stat->addValue("Frequencies", calcValues[i]-refValues[i], calc_data.name);
//            }
//            m_stream << endl
//                << INDENT_SPACES << "Zero-point Energy(kcal/mol):" << endl;
//            m_stream << QString("%1(%3)%2").arg("Calculated",13).arg("Reference",13).arg("Error",10) << endl;
//            double refZPE = CalcZPE(refValues);
//            double calcZPE = CalcZPE(calcValues);
//            m_stream << QString("%1(%3)%2")
//                   .arg(calcZPE,13,'f',4).arg(refZPE,13,'f',4).arg(calcZPE-refZPE,10,'f',3) << endl;
//        }
//        else
//        {
//            m_stream << "The number of frequency of the reference and the calculated molecule dismatched." << endl;
//            m_stat->addUnconvergedTarget(calc_data.name + "(Freq)");
//        }
//    }

//    if (calc_data.atomizationEnergyData.isValid && ref_data.atomizationEnergyData.isValid)
//    {
//        QString methodbasis;
//        if (ref_data.atomizationEnergyData.basisSet.isEmpty())
//        {
//            methodbasis = QString("(%1)").arg(ref_data.atomizationEnergyData.method);
//        }
//        else
//        {
//            methodbasis = QString("(%1/%2)").arg(ref_data.atomizationEnergyData.method, ref_data.atomizationEnergyData.basisSet);
//        }
//        double refValue = ref_data.atomizationEnergyData.energy;
//        double calcValue = calc_data.atomizationEnergyData.energy;

//        m_stream << endl
//                 << INDENT_SPACES << "Atomization Energy without ZPE correction(kcal/mol):" << endl;

//        m_stream << QString("%1(%3)%2%4").arg("Calculated",13).arg("Reference",13).arg("Error",10).arg(methodbasis) << endl;
//        m_stream << QString("%1(%3)%2")
//               .arg(calcValue,13,'f',4).arg(refValue,13,'f',4).arg(calcValue-refValue,10,'f',3) << endl;

//        m_stat->addValue("AtomizationEnergy", calcValue-refValue, calc_data.name);

//    }


//}


void TextFormatter::writeBandStructure(const CrystalGeometryData& ref_geom, const TestBandStructure& testBS, const BandStructureData &ref_data, const BandStructureData &calc_data)
{
    int nBands = calc_data.bands.size();
    if (nBands > ref_data.bands.size())
    {
        m_stream << "Warning:" << " The # of bands from reference data is less than # of bands from calculated.";
        nBands = ref_data.bands.size();
    }
    int nPoints = calc_data.bands.first().size();

    m_stream << endl << INDENT_SPACES \
             << "Testing Bandstructure: " << ref_geom.name << ", Weight: " << testBS.weight << endl;


    m_stream << INDENT_SPACES << "Number of bands from the reference: " << ref_data.bands.size() << endl
             << INDENT_SPACES << "Number of bands from the calculated: " << calc_data.bands.size() << endl;

    QPair<int, int> fermiIndex = ref_data.fermiIndex;
    double fermi_energy_ref = 0.0;
    if (fermiIndex.first != -1 && fermiIndex.second != -1)
    {
        if (fermiIndex.first < ref_data.bands.size() && fermiIndex.second < ref_data.bands[fermiIndex.first].size())
        {
            fermi_energy_ref = ref_data.bands[fermiIndex.first][fermiIndex.second];
        }

    }


    double totalError = 0.0;
    for (int i=0; i< testBS.testRegion.size(); ++i)
    {
        if (testBS.testRegion[i].type.toLower() == "index_based")
        {

            int bStart = testBS.testRegion[i].bandIndex_start;
            if (bStart < 0)
            {
                bStart += nBands;
            }
            int bEnd = testBS.testRegion[i].bandIndex_end;
            if (bEnd < 0)
            {
                bEnd += nBands;
            }

            int pStart = testBS.testRegion[i].pointIndex_start;
            if (pStart < 0)
            {
                pStart += nPoints;
            }
            int pEnd = testBS.testRegion[i].pointIndex_end;
            if (pEnd < 0)
            {
                pEnd += nPoints;
            }

            double weight = testBS.testRegion[i].weight;

            m_stat->beginGroup("BandStructure", ref_data.uuid.toString(), ref_data.name, weight*testBS.weight);


            m_stream << INDENT_SPACES << "Test region " << i+1 << ":" << endl;

            m_stream << INDENT_SPACES << INDENT_SPACES
                     << QString("%1").arg("Band index(1-based):  [",-25) << QString("%1").arg(bStart+1,6) << ", " << QString("%1").arg(bEnd+1,6) << "]" << endl;
            m_stream << INDENT_SPACES << INDENT_SPACES
                     << QString("%1").arg("Point index(1-based): [", -25) << QString("%1").arg(pStart+1,6) << ", " << QString("%1").arg(pEnd+1,6) << "]" << endl;

            m_stream << INDENT_SPACES << INDENT_SPACES << QString("%1").arg("Weight: ", -25) << QString::number(weight,'f',6) << endl;

            //        int npoints = 0;

            m_stream << INDENT_SPACES << INDENT_SPACES << QString("%1").arg("Error: ", -25) << endl;


            for(int bi = bStart; bi <= bEnd; ++bi)
            {
                double error = 0.0;
                int errorPoints = 0;
                double refValue = 0.0;
                double calcValue = 0.0;

                for(int pi = pStart; pi <= pEnd; ++pi)
                {
                    refValue = ref_data.bands[bi][pi]-fermi_energy_ref;
                    calcValue = calc_data.bands[bi][pi];

                    error += qAbs(calcValue-refValue);
                    m_stat->addValue(ref_data.bands[bi][pi]-fermi_energy_ref, calc_data.bands[bi][pi], 1.0);
                    ++errorPoints;
                }
                error = (error*weight)/static_cast<double>(errorPoints);

                m_stream << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES<< QString("Band %1: ").arg(bi+1) << error << endl;
                totalError += error;
            }
            m_stream << INDENT_SPACES << INDENT_SPACES << INDENT_SPACES<< QString("Total : ") << totalError << endl;

            m_stat->endGroup();
        }
        else if (testBS.testRegion[i].type.toLower() == "energy_based")
        {
            double weight = testBS.testRegion[i].weight;


            m_stream << INDENT_SPACES << "Test region " << i+1 << " between energy level: [" << testBS.testRegion[i].lower_energy
                     << ", " << testBS.testRegion[i].upper_energy << "] eV" << endl;
            m_stream << INDENT_SPACES << INDENT_SPACES << QString("%1").arg("Weight: ", -25) << QString::number(weight,'f',6) << endl;


            double error = 0.0;
            int errorPoints = 0;
            double refValue = 0.0;
            double calcValue = 0.0;

            m_stat->beginGroup("BandStructure", ref_data.uuid.toString(), ref_data.name, weight*testBS.weight);
            for(int iband=0; iband<calc_data.bands.size(); ++iband )
            {
                for(int ipoint = 0; ipoint < calc_data.bands[iband].size(); ++ipoint)
                {
                    if (iband < ref_data.bands.size())
                    {
                        if (ipoint < ref_data.bands[iband].size())
                        {
                            if (ref_data.bands[iband][ipoint] >= testBS.testRegion[i].lower_energy &&
                                ref_data.bands[iband][ipoint] <= testBS.testRegion[i].upper_energy )
                            {
                                refValue = ref_data.bands[iband][ipoint]-fermi_energy_ref;
                                calcValue = calc_data.bands[iband][ipoint];
                                ++errorPoints;
                                error += qAbs(calcValue-refValue);
                                m_stat->addValue(ref_data.bands[iband][ipoint]-fermi_energy_ref, calc_data.bands[iband][ipoint], 1.0);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            error = (error*weight)/static_cast<double>(errorPoints);
            m_stat->endGroup();

            m_stream << INDENT_SPACES << INDENT_SPACES << QString("%1%2 (Weight: %3)").arg("Error: ", -25)
                        .arg(error, 12, 'f', 6)
                        .arg(weight) << endl;
        }

    }


    if (testBS.weight > 0.0001)
    {
        m_stat->addConvergedTarget("Bandstructure: " + ref_data.name);
    }
}


void TextFormatter::writeReaction(const QMap<QString, QString> moleNames, const ReferenceReactionData &reaction, double calcValue, const TestReaction& testReaction, bool GOptComp, DFTBResultInfo &info)
{
    QString methodbasis;
    if (reaction.basis.isEmpty())
    {
        methodbasis = QString("(%1)").arg(reaction.method);
    }
    else
    {
        methodbasis = QString("(%1/%2)").arg(reaction.method, reaction.basis);
    }

    m_stream << EQUALLINE << endl
             << INDENT_SPACES << "Testing Reaction: " << reaction.name << endl;

    QString GOptCompStr = "No";
    if (GOptComp)
    {
        GOptCompStr = "Yes";
    }

    m_stream << INDENT_SPACES << INDENT_SPACES << "Geometry Optimization on Components: " << GOptCompStr << endl;

    QStringList line;
    for(int i=0; i<reaction.reactants.size();++i)
    {
        line.append(QString("%1 %2").arg(reaction.reactants[i].coefficient).arg(moleNames[reaction.reactants[i].geom_uuid.toString()]));
    }
    m_stream << INDENT_SPACES << INDENT_SPACES << line.join(" + ");
    line.clear();
    for(int i=0; i<reaction.products.size();++i)
    {
        line.append(QString("%1 %2").arg(reaction.products[i].coefficient).arg(moleNames[reaction.products[i].geom_uuid.toString()]));
    }
    m_stream << " -> " << line.join(" + ") << endl;

    m_stream << endl;

    if (info.failed)
    {
        m_stream << INDENT_SPACES << info.detail << endl;
    }
    else
    {


        m_stream << INDENT_SPACES
                 << QString("Reaction Energy(%1): ").arg(reaction.energy_unit) <<  " Weight: " << testReaction.weight << endl;
        m_stream << QString("%1 (%3)%2%4").arg("Calculated",13).arg("Reference",13).arg("Error",10).arg(methodbasis) << endl;

        double refValue = reaction.energy;
        double realCalcValue = EnergyAUToAny(reaction.energy_unit, calcValue);

        QString error_str;
        error_str.sprintf("%+10.3lf", realCalcValue-refValue);
        m_stream << QString("%1 (%3)%2")
                    .arg(realCalcValue,13,'f',4).arg(refValue,13,'f',4).arg(error_str,10) << endl;

        m_stream << EQUALLINE << endl;


        m_stat->beginGroup("ReactionEnergy", reaction.uuid.toString(), reaction.name, testReaction.weight);
        double refValueInKcalmol = EnergyAUToAny("kcal/mol", EnergyToAU(reaction.energy_unit, refValue));
        double calValueInKcalmol = EnergyAUToAny("kcal/mol", calcValue);
        m_stat->addValue(refValueInKcalmol, calValueInKcalmol, 1.0);
        m_stat->endGroup();
    }

}


void TextFormatter::writeHeader()
{
    m_stream << STARLINE << endl
             << INDENT_SPACES << "DFTB TestSuite Testing Report" << endl
             << INDENT_SPACES << INDENT_SPACES
             << QDateTime::currentDateTime().toString("MMMM d yyyy hh:mm:ss.zzz") << endl
             << HYPHENLINE << endl;
}


void TextFormatter::writeStatistics(bool showErrorUnit)
{
    m_stat->compute();
    QList<QString> types = m_stat->getTypes();

    m_stream << HYPHENLINE << endl << endl
             << INDENT_SPACES << "Total number of targets required: " << (m_stat->getConvergedTarget().size() + m_stat->getUnconvergedTarget().size()) << endl
             << INDENT_SPACES << "Number of targets failed: " << m_stat->getUnconvergedTarget().size()
             << endl << endl;


    m_stream << INDENT_SPACES << "Statistics:" << endl;

    if (!m_stat->getUnconvergedTarget().empty())
    {
        m_stream << INDENT_SPACES << INDENT_SPACES << "Failed molecules:" << endl ;

        QListIterator<QString> it(m_stat->getUnconvergedTarget());
        while(it.hasNext())
        {
            m_stream << INDENT_SPACES << INDENT_SPACES << it.next() << endl;
        }
    }

    m_stream << endl
             << INDENT_SPACES << "Mean Standard Errors " << endl;
    for(int i=0; i<types.size();++i)
    {
        m_stream << INDENT_SPACES << INDENT_SPACES
                 << QString("%1%2 in %3 measurement(s)").arg(types[i]+":",-22)
                    .arg(m_stat->getError_MSE(types[i]),16,'f',6)
                    .arg(m_stat->getError_Count(types[i]),4) << endl;
    }

    m_stream << endl
             << INDENT_SPACES << "Mean Unsigned Errors " << endl;

    for(int i=0; i<types.size();++i)
    {
        m_stream << INDENT_SPACES << INDENT_SPACES
                 << QString("%1%2 in %3 measurement(s)").arg(types[i]+":",-22)
                    .arg(m_stat->getError_MUE(types[i]),16,'f',6)
                    .arg(m_stat->getError_Count(types[i]),4) << endl;
    }

    m_stream << endl
             << INDENT_SPACES << "Root Mean Suqare Errors " << endl;
    for(int i=0; i<types.size();++i)
    {
        m_stream << INDENT_SPACES << INDENT_SPACES
                 << QString("%1%2 in %3 measurement(s)").arg(types[i]+":",-22)
                    .arg(m_stat->getError_RMS(types[i]),16,'f',6)
                    .arg(m_stat->getError_Count(types[i]),4) << endl;
    }

    m_stream << endl
             << INDENT_SPACES << "Mean Absolute Percentage Errors " << endl;
    for(int i=0; i<types.size();++i)
    {
        try
        {
            m_stream << INDENT_SPACES << INDENT_SPACES
                     << QString("%1%2 in %3 measurement(s)").arg(types[i]+":",-22)
                        .arg(m_stat->getError_MAPE(types[i]),16,'f',6)
                        .arg(m_stat->getError_Count(types[i]),4) << endl;
        }
        catch(...)
        {
            m_stream << INDENT_SPACES << INDENT_SPACES
                     << QString("%1%2.").arg(types[i]+":",-22)
                        .arg("Not Computed",16) << endl;
        }
    }

    m_stream << endl
             << INDENT_SPACES
             << "Maximal Unsigned Errors" << endl;

    for(int i=0; i<types.size();++i)
    {
        QPair<QPair<double, double> , QString> tmp = m_stat->getError_MUEMAX(types[i]);
        m_stream << INDENT_SPACES << INDENT_SPACES
                 << QString("%1%2 with weight: %3 in %4").arg(types[i]+":",-22)
                    .arg(tmp.first.first,16,'f',6)
                    .arg(tmp.first.second,16,'f',6)
                    .arg(m_stat->getError_Name(tmp.second)) << endl;
    }


    if (showErrorUnit)
    {

    m_stream << endl
             << INDENT_SPACES
             << "ErrorUnits:" << endl;

    for(int i=0; i<types.size();++i)
    {
        m_stream << INDENT_SPACES << INDENT_SPACES
                 << QString("%1 :%2 %3").arg(types[i],-22).arg(m_stat->getErrorUnit(types[i]),16,'f',6).arg(m_stat->getUnit(types[i])) << endl;
    }

    m_stream << endl << INDENT_SPACES
             << "Errors in ErrorUnit:" << endl;

    m_stream << INDENT_SPACES << INDENT_SPACES << QString("%1: %2").arg("Total:",-28).arg(m_stat->getError_EU(),16,'f',6) << endl;
    for(int i=0; i<types.size();++i)
    {
        m_stream << INDENT_SPACES << INDENT_SPACES << QString("  %1: %2").arg(types[i], -26).arg(m_stat->getErrorComp_EU(types[i]),16,'f',6) << endl;
    }
    m_stream << endl;

    m_stream << endl << INDENT_SPACES
             << "Final Fitness in ErrorUnit:  " << QString("%1").arg(m_stat->getFitness(), 16,'f',6) <<  endl
             << INDENT_SPACES << INDENT_SPACES<< QString("Formula: {M.U.E.}*%1 + {Max.U.E}*%2 + 1000000*{# of Unconverged Targets}.")
                .arg((1.0-m_stat->getMaxErrorRatio()),0,'f',4)
                .arg((m_stat->getMaxErrorRatio()),0,'f',4)
             << endl;

    }

    m_stream << HYPHENLINE << endl;
}

}
