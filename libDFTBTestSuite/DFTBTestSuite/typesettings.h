#ifndef TYPESETTINGS_H
#define TYPESETTINGS_H

#include <QString>

namespace DFTBTestSuite{

const QString HYPHENLINE = QString("").fill('-',80);
const QString EQUALLINE  = QString("").fill('=',80);
const QString STARLINE   = QString("").fill('*',80);
const QString INDENT_SPACES = "  " ;


}


#endif // TYPESETTINGS_H
