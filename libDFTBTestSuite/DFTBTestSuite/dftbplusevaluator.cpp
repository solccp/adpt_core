#include "dftbplusevaluator.h"

#include "DFTBTestSuite/util.h"
#include "typesettings.h"

#include <QDebug>

#include "DFTBTestSuite/ReferenceDataType/CrystalData.h"
#include <QRegExp>
#include <QRegularExpression>
#include <QString>
#include <fstream>

#include "newuoa.h"

namespace DFTBTestSuite
{

struct EnergyData
{
    double fermi_a;
    double fermi_b;
    double total_energy;
};


DFTBPlusEvaluator::DFTBPlusEvaluator(QTextStream& output_stream,
    const Evaluation& eval_opts,
    const QString& codePath)
    : Evaluator(output_stream, eval_opts, codePath)
{
}

DFTBPlusEvaluator::~DFTBPlusEvaluator() {}

void writeAutotestTagOptions(QTextStream& os)
{
    os << "*Options {" << endl
       << "    !WriteAutotestTag = Yes" << endl
       << "}" << endl;
}

void writeUPE(QTextStream& os, double upe, bool shell_resolved_spin,
    const QMap<QString, QVariantList>& spinConstants)
{
    os << "+Hamiltonian = +DFTB {" << endl
       << "    !SpinPolarisation = !Colinear {" << endl
       << "         !UnpairedElectrons = " << upe << endl
       << "    }" << endl;
    os << "!SCC = Yes" << endl
       << "!SCCTolerance = 1.0e-8" << endl
       << "    *SpinConstants = {" << endl;
    if (shell_resolved_spin)
    {
        os   << "       ShellResolvedSpin = Yes" << endl;
    }
    else
    {
        os   << "       ShellResolvedSpin = No" << endl;
    }
    QMapIterator<QString, QVariantList> it(spinConstants);
    while (it.hasNext())
    {
        it.next();
        os << "        !" << it.key() << " = ";
        for (auto& value : it.value())
        {
            os << " " << value.toDouble();
        }
        os << endl;
    }
    os << "    }" << endl;
    os << "}" << endl;
}

void parseDFTBResult(const QString& filename, MolecularData& result_data,
    DFTBResultInfo& result_info)
{
    result_info.SCF_Converged = false;
    result_info.Geometry_Converged = false;
    result_info.failed = true;
    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        qCritical() << "Cannot open file for reading:" << filename << " "
                    << qPrintable(detailed.errorString()) << endl;
        result_info.failed = true;
        return;
    }
    QTextStream fin(&detailed);
    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Total energy:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;
            result_data.totalEnergyData.method = "DFTB";
            result_data.totalEnergyData.energy = tokens.at(2).toDouble(&ok);
            if (!ok)
            {
                result_info.failed = true;
                result_info.UnknownError = true;
                break;
            }
            else
            {
                if (std::isnan(result_data.totalEnergyData.energy))
                {
                    result_info.failed = true;
                    result_info.Geometry_Converged = false;
                    break;
                }
                else
                {
                    result_info.failed = false;
                    result_data.totalEnergyData.isValid = true;
                }
            }
        }
        //        else if ( str.contains("Total Forces"))
        //        {
        //            result_data->geometryData.forces.clear();
        //            for(int i=0;
        //            i<result_data->geometryData.geometry.NumAtom();++i)
        //            {
        //                QString str = fin.readLine();
        //                QStringList tokens = str.split(" ",
        //                QString::SkipEmptyParts);
        //                if (tokens.size()!=3)
        //                {
        //                    result_data->geometryData.forces.clear();
        //                    break;
        //                }
        //                else
        //                {
        //                    for(int j=0; j<3; ++j)
        //                    {
        //                        result_data->geometryData.forces.push_back(tokens[j].toDouble());
        //                    }
        //                }
        //            }
        //        }
        else if (str.contains("SCC converged"))
        {
            result_info.SCF_Converged = true;
        }
        else if (str.contains("Geometry converged"))
        {
            result_info.Geometry_Converged = true;
        }
    }
}



void parseDFTBFermiEnergies(const QString& filename, EnergyData& data, DFTBResultInfo& result_info)
{
    result_info.SCF_Converged = false;
    result_info.Geometry_Converged = false;
    result_info.failed = true;

    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        qCritical() << "Cannot open file for reading:" << filename << " "
                    << qPrintable(detailed.errorString()) << endl;
        result_info.failed = true;
        return;
    }
    QTextStream fin(&detailed);
    int spin_stage = 0;
    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Fermi energy:") || str.contains("Fermi level:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;

            double efermi = tokens.at(4).toDouble(&ok);
            if (!ok)
            {
                result_info.failed = true;
                result_info.UnknownError = true;
                break;
            }
            else
            {
                if (std::isnan(efermi))
                {
                    result_info.failed = true;
                    break;
                }
                else
                {
                    result_info.failed = false;
                }
            }
            if (spin_stage == 0)
            {
                data.fermi_a = efermi;
                data.fermi_b = efermi;
            }
            else
            {
                data.fermi_b = efermi;
            }

            spin_stage += 1;
        }
        else if (str.contains("Total energy:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;

            data.total_energy = tokens.at(2).toDouble(&ok);
            if (!ok)
            {
                result_info.failed = true;
                result_info.UnknownError = true;
                break;
            }
            else
            {
                if (std::isnan(data.total_energy ))
                {
                    result_info.failed = true;
                    break;
                }
                else
                {
                    result_info.failed = false;
                }
            }
        }
        else if (str.contains("SCC converged"))
        {
            result_info.SCF_Converged = true;
        }
    }
}

void parseDFTBOptGeom(const QString& filename, MolecularData& result_data,
    DFTBResultInfo& result_info)
{
    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        qCritical() << "Cannot open file for reading:" << qPrintable(filename)
                    << endl;
        result_info.failed = true;
        return;
    }

    //    QStringList resultLines;

    //    QTextStream fin(&detailed);
    //    QString dummy = fin.readLine();
    //    int nat = dummy.toInt();
    //    resultLines.append(dummy);
    //    dummy = fin.readLine();
    //    resultLines.append("");
    //    for(int i=0; i<nat;++i)
    //    {
    //        dummy = fin.readLine();
    //        QStringList arr = dummy.split(" ", QString::SkipEmptyParts);
    ////        arr.pop_back();
    //        resultLines.append(arr.join(" "));
    //    }

    const double Bohr_AA = 0.529177249;

    QTextStream fin(&detailed);
    bool succ = false;
    while (!fin.atEnd())
    {
        QString line = fin.readLine();
        if (line.contains("end_coords"))
        {
            for (int i = 0; i < result_data.geometryData.geometry.NumAtom(); ++i)
            {
                double x, y, z;
                fin >> x >> y >> z;
                x *= Bohr_AA;
                y *= Bohr_AA;
                z *= Bohr_AA;
                result_data.geometryData.geometry.setCoord(i, 0, x);
                result_data.geometryData.geometry.setCoord(i, 1, y);
                result_data.geometryData.geometry.setCoord(i, 2, z);
            }
            succ = true;
            break;
        }
    }

    //    bool succ =
    //    result_data.geometryData.geometry.readString(resultLines.join("\n"));

    result_data.geometryData.method = "DFTB";
    result_data.geometryData.optimized = true;
    if (!succ)
    {
        result_info.failed = true;
        result_info.detail = "Failed to parse the XYZ file: " + filename;
    }
}

bool parseDFTBFreq(const QString& filename, MolecularData& result_data,
    DFTBResultInfo& result_info)
{
    QFile detailed(filename);
    if (!detailed.open(QIODevice::ReadOnly))
    {
        qCritical() << "Cannot open file for reading:" << filename << " "
                    << qPrintable(detailed.errorString()) << endl;
        result_info.failed = true;
        return false;
    }
    QTextStream fin(&detailed);
    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Vibrational modes (cm-1):"))
        {
            while (!fin.atEnd())
            {
                str = fin.readLine();
                if (str.trimmed().length() == 0)
                    break;
                bool ok;
                double freq = str.toDouble(&ok);
                if (ok)
                {
                    result_data.frequencyData.frequencies.push_back(freq);
                }
            }
            result_data.frequencyData.isValid = true;
            result_data.frequencyData.method = "DFTB";
            return true;
        }
    }
    return false;
}

void DFTBPlusEvaluator::EvaluateAtomicEnergy(
    AtomicData& calc, const QString& template_input_filename,
    const OptionalEvaluationInformation& opt_info, DFTBResultInfo& info) const
{
    QDir scratchDir(
        this->m_workDir.absoluteFilePath(QString("Atomic_%1").arg(calc.symbol)));
    scratchDir.mkpath(scratchDir.absolutePath());
    QByteArray output;

    QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));

    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
        info.failed = true;
        return;
    }

    QTextStream fout(&file);

    if (QFileInfo(template_input_filename).exists())
    {
        insertFile(fout, template_input_filename);
    }
    else
    {
        info.detail = "Error: No DFTB Input for atomic calculation: " + calc.symbol;
        info.failed = true;
        return;
    }

    QByteArray array;

    QTextStream geom(&array);
    geom << "1 C" << endl
         << calc.symbol << endl
         << "1 1 0.0 0.0 0.0" << endl;

    writeInputGeometry(fout, QString(array));

    writeSKFileInfo(fout);

    writeInputEnergy(fout, 0, "+");

    writeOptInfo(fout, opt_info);
    writeParseOptions(fout);

    fout.flush();
    file.close();

    info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), m_program_path,
        QStringList(), output, QString(), m_evaluation.timeout);
    DFTBTestSuite::writeFile(scratchDir.absoluteFilePath("dftb.out"),
        QString(output));

    if (info.failed)
    {
        info.detail = QString(output);
    }

    QFile detailed(scratchDir.absoluteFilePath("detailed.out"));
    if (!detailed.open(QIODevice::ReadOnly))
    {
        info.detail = "Cannot open file for reading: detailed.out" + detailed.errorString();
        info.failed = true;
        return;
    }
    QTextStream fin(&detailed);
    while (!fin.atEnd())
    {
        QString str = fin.readLine();
        if (str.contains("Total energy:"))
        {
            QStringList tokens = str.split(" ", QString::SkipEmptyParts);
            bool ok;
            calc.energy = tokens.at(2).toDouble(&ok);
            if (!ok)
            {
                info.failed = true;
                info.UnknownError = true;
                info.detail = "Cannot parse the total energy from detailed.out";
            }
            else
            {
                info.failed = false;
            }
            return;
        }
    }
    info.failed = true;

    return;
}

void DFTBPlusEvaluator::EvaluateGeometry(const MolecularData& ref, const OptionalEvaluationInformation &opt_info,
    MolecularData& calc,
    DFTBResultInfo& info) const
{
    QDir scratchDir(this->m_workDir.absoluteFilePath(getGoodFilename(
        QString("Mole_Geom_%1_%2").arg(calc.name).arg(calc.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));

    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
        info.failed = true;
        return;
    }

    calc.totalEnergyData.isValid = false;

    calc.geometryData.optimized = false;

    QTextStream fout(&file);

    // prepare the input
    writeInputGeometry(fout, XYZ2Gen(ref.geometryData.geometry.toXYZ()));

    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets,
        calc.uuid.toString());
    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            insertFile(fout, localinput.template_energy);
            writeInputEnergy(fout, ref.charge, "+");
        }
        else
        {
            if (!this->m_evaluation.mole_default_dftbinput.template_energy
                     .isEmpty()
                && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
                       .exists())
            {
                insertFile(fout, m_evaluation.mole_default_dftbinput.template_energy);
                writeInputEnergy(fout, ref.charge, "+");
            }
            else
            {
                writeInputEnergy(fout, ref.charge, "");
            }
        }

        writeSKFileInfo(fout, scratchDir);

        // Driver part
        if (!localinput.template_opt.isEmpty() && QFileInfo(localinput.template_opt).exists())
        {
            insertFile(fout, localinput.template_opt);
            QString driver = findDriverName(localinput.template_opt);
            if (driver.isEmpty())
            {
                driver = "LBFGS";
            }
            this->writeInputDriverOpt(fout, driver);
        }
        else
        {
            if (!this->m_evaluation.mole_default_dftbinput.template_opt.isEmpty() && QFileInfo(m_evaluation.mole_default_dftbinput.template_opt).exists())
            {
                insertFile(fout, m_evaluation.mole_default_dftbinput.template_opt);
                QString driver = findDriverName(m_evaluation.mole_default_dftbinput.template_opt);
                if (driver.isEmpty())
                {
                    driver = "LBFGS";
                }
                this->writeInputDriverOpt(fout, driver);
            }
            else
            {
                this->writeInputDriverOpt(fout, "LBFGS");
            }
        }

        writeInputHubbardDerivatives(fout, m_evaluation.SKInfo.get(),  opt_info.orbital_resolved_scc, opt_info.hubbardDerivatives);

        if (ref.spin > 1)
        {
            double upe = ref.spin-1.0;
            writeUPE(fout, upe, opt_info.shell_resolved_spin, opt_info.spinConstants);
        }

        writeParseOptions(fout);
        writeAutotestTagOptions(fout);
        writeOptInfo(fout, opt_info);


        fout.flush();
        file.close();

        // run the dftb
        info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
            QStringList(), output, QString(), m_evaluation.timeout);

        //! NOTE: rewrite this
        QString output_str = QString(output);
        QStringList output_list = output_str.split("\n");
        int errorindex = output_list.lastIndexOf("ERROR!");
        if (errorindex > -1)
        {
            info.failed = true;
        }

        writeFile(scratchDir.absoluteFilePath("dftb.out"), output_str);

        if (info.failed)
        {
            if (errorindex > -1)
            {
                QStringList detail;
                detail.append(STARLINE);
                detail.append(output.mid(errorindex));
                detail.append(STARLINE);
                info.detail = detail.join("\n");
            }
            return;
        }
        double energy_first_iteration;
        bool succ = parseDFTBFirstPointEnergyInGOPT(
            scratchDir.absoluteFilePath("dftb.out"), energy_first_iteration);

        parseDFTBResult(scratchDir.absoluteFilePath("detailed.out"), calc, info);

        calc.geometryData.geometry = ref.geometryData.geometry;
        parseDFTBOptGeom(scratchDir.absoluteFilePath("autotest.tag"), calc, info);

        calc.totalEnergyData.optEnergy = calc.totalEnergyData.energy;
        calc.totalEnergyData.isOptimized = true;
        calc.totalEnergyData.energy = energy_first_iteration;
    }
    else
    {
        info.failed = true;
        info.detail = "Molecule not found. This should not happen.";
    }
    return;
}

void DFTBPlusEvaluator::EvaluateFrequency(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData& calc,
    DFTBResultInfo& info) const
{
    // Assuming the geometry from calc is optimized.

    QDir scratchDir(this->m_workDir.absoluteFilePath(getGoodFilename(
        QString("Mole_Freq_%1_%2").arg(calc.name).arg(calc.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));
    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
        info.failed = true;
        return;
    }

    calc.frequencyData.isValid = false;

    QTextStream fout(&file);

    // prepare the input
    writeInputGeometry(fout, XYZ2Gen(calc.geometryData.geometry.toXYZ()));

    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets,
        calc.uuid.toString());
    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            insertFile(fout, localinput.template_energy);
            writeInputEnergy(fout, calc.charge, "+");
        }
        else
        {
            if (!this->m_evaluation.mole_default_dftbinput.template_energy
                     .isEmpty()
                && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
                       .exists())
            {
                insertFile(fout, m_evaluation.mole_default_dftbinput.template_energy);
                writeInputEnergy(fout, calc.charge, "+");
            }
            else
            {
                writeInputEnergy(fout, calc.charge, "");
            }
        }

        writeSKFileInfo(fout, scratchDir);

        // Driver part
        if (!localinput.template_frequency.isEmpty() && QFileInfo(localinput.template_frequency).exists())
        {
            insertFile(fout, localinput.template_frequency);
        }
        else
        {
            if (!this->m_evaluation.mole_default_dftbinput.template_frequency
                     .isEmpty()
                && QFileInfo(m_evaluation.mole_default_dftbinput.template_frequency)
                       .exists())
            {
                insertFile(fout,
                    m_evaluation.mole_default_dftbinput.template_frequency);
            }
        }

        writeInputFreq(fout);
        writeInputHubbardDerivatives(fout, m_evaluation.SKInfo.get(),  opt_info.orbital_resolved_scc, opt_info.hubbardDerivatives);

        if (ref.spin > 1)
        {
            double upe = ref.spin-1.0;
            writeUPE(fout, upe, opt_info.shell_resolved_spin, opt_info.spinConstants);
        }

        writeParseOptions(fout);
        writeOptInfo(fout, opt_info);

        fout.flush();
        file.close();

        // run the dftb
        info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
            QStringList(), output, QString(), m_evaluation.timeout);
        //! NOTE: rewrite this
        QString output_str = QString(output);

        writeFile(scratchDir.absoluteFilePath("dftb_freq.out"), output_str);

        if (info.failed)
        {
            info.detail = output_str;
            return;
        }

        if (parseDFTBFreq(scratchDir.absoluteFilePath("FREQ.DAT"), calc,
                info))
        {
            calc.frequencyData.isValid = true;
        }
        else
        {
            info.failed = true;
            info.detail = "Frequency calculation failed";
        }
    }
    else
    {
        info.failed = true;
        info.detail = "Molecule not found. This should not happen.";
    }
    return;
}

void DFTBPlusEvaluator::EvaluateMolecularEnergy(const MolecularData& ref, const OptionalEvaluationInformation &opt_info,
    MolecularData& calc,
    DFTBResultInfo& info) const
{
    QDir scratchDir(this->m_workDir.absoluteFilePath(getGoodFilename(
        QString("Mole_Energy_%1_%2").arg(calc.name).arg(calc.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));
    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
        info.failed = true;
        return;
    }

    calc.totalEnergyData.isValid = false;

    QTextStream fout(&file);

    // prepare the input
    writeInputGeometry(fout, XYZ2Gen(ref.geometryData.geometry.toXYZ()));

    DFTBInput localinput = m_evaluation.mole_default_dftbinput;
    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets,
        calc.uuid.toString());
    if (index > -1)
    {
        auto temp_input = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;
        if (!temp_input.template_energy.isEmpty())
        {
            localinput.template_energy = temp_input.template_energy;
        }
        if (!temp_input.template_frequency.isEmpty())
        {
            localinput.template_frequency = temp_input.template_frequency;
        }
        if (!temp_input.template_opt.isEmpty())
        {
            localinput.template_opt = temp_input.template_opt;
        }

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            insertFile(fout, localinput.template_energy);
            writeInputEnergy(fout, ref.charge, "+");
        }
        else
        {
            if (!this->m_evaluation.mole_default_dftbinput.template_energy
                     .isEmpty()
                && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
                       .exists())
            {
                insertFile(fout, m_evaluation.mole_default_dftbinput.template_energy);
                writeInputEnergy(fout, ref.charge, "+");
            }
            else
            {
                writeInputEnergy(fout, ref.charge, "");
            }
        }


        writeInputHubbardDerivatives(fout, m_evaluation.SKInfo.get(),  opt_info.orbital_resolved_scc, opt_info.hubbardDerivatives);

        if (ref.spin > 1)
        {
            double upe = ref.spin-1.0;
            writeUPE(fout, upe, opt_info.shell_resolved_spin, opt_info.spinConstants);
        }

        writeSKFileInfo(fout, scratchDir);
        writeOptInfo(fout, opt_info);
        writeParseOptions(fout);

        fout.flush();
        file.close();

        // run the dftb
        info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
            QStringList(), output, QString(), m_evaluation.timeout);

        if (info.failed)
        {
            info.detail = QString(output);
            return;
        }

        parseDFTBResult(scratchDir.absoluteFilePath("detailed.out"), calc, info);
    }
    else
    {
        info.failed = true;
        info.detail = "Molecule not found. This should not happen.";
    }
    return;
}

void DFTBPlusEvaluator::EvaluateBandStructure(
    const CrystalGeometryData& ref_geom, const BandStructureData& ref,
    const CrystalEnergyData& calc_energy_data,
    const OptionalEvaluationInformation& opt_info, BandStructureData& calc,
    DFTBResultInfo& info) const
{

    int index = indexUUID(m_evaluation.evaluationCrystal.crystalstructureTargets, ref_geom.uuid.toString());

    bool spinpol = ref_geom.spinpol;

    if (index != -1)
    {
        auto const & setting = m_evaluation.evaluationCrystal.crystalstructureTargets[index];
        if (setting.hasSpinPolarSetting)
        {
            spinpol = setting.spinpol_setting.spinpol;
        }
    }


    if (!spinpol)
    {
        EvaluateSpinUnpolarizedBandStructure(ref_geom, ref, calc_energy_data,
            opt_info, calc, info);
    }
    else
    {
        EvaluateSpinPolarizedBandStructure(ref_geom, ref, calc_energy_data,
            opt_info, calc, info);
    }
}

void DFTBPlusEvaluator::writeInputHubbardDerivatives(
        QTextStream& fout,  const ADPT::SKFileInfo* skfileinfo,
        bool orbital_resolved,
    const QMap<QString, dVector3>& hubbardDerivatives) const
{
    if (!hubbardDerivatives.isEmpty())
    {
        fout << QString("+Hamiltonian = +DFTB{") << endl;

        if (orbital_resolved)
        {
            fout << "    !OrbitalResolvedSCC = Yes" << endl
                 << "    *HubbardDerivs = {" << endl;


            QMapIterator<QString, dVector3> it(hubbardDerivatives);
            while (it.hasNext())
            {
                it.next();
                int maxL = skfileinfo->getMaxAngularMomentum(it.key());
                fout << "        !" << it.key() << " = " ;

                for(int i=0; i<=maxL; ++i)
                {
                    fout << it.value()[i] << " " ;
                }
                fout << endl;
            }
            fout << "    }" << endl;
        }
        else
        {
            fout << "    !OrbitalResolvedSCC = No" << endl
                 << "    *HubbardDerivs = {" << endl;
            QMapIterator<QString, dVector3> it(hubbardDerivatives);
            while (it.hasNext())
            {
                it.next();
                fout << "        !" << it.key() << " = " << it.value()[2] << endl;
            }
            fout << "    }" << endl;
        }
        fout << "}" << endl;
    }
}

///
/// \brief DFTBPlusEvaluator::writeInputFile
/// Write the DFTB+ input file
///
/// \param file output file
/// \param template_filename template input file name
/// \param ref_geom reference geometry
/// \param kpoints kpoint settings
/// \param opt_info optional information for the evaluator, such as hubbard
/// derivatives and spin constants
/// \param scratchDir the working directory of the calculation
/// \param upe unpaired electronic if ref_geom.spinpol is true
///
void DFTBPlusEvaluator::writeInputFile(
    QTextStream& fout, const QString& template_filename,
    const CrystalGeometryData& ref_geom, bool doSCF, bool readInitialCharges,
    const ADPT::KPointsSetting& kpoints,
    const OptionalEvaluationInformation& opt_info, QDir workingDir,
    bool spinpol, double upe) const
{
    // prepare the input
    writeInputGeometry(fout, ref_geom.toGen());

    if (!template_filename.isEmpty())
    {
        insertFile(fout, template_filename);
        writeInputEnergy(fout, ref_geom.charge, "+", true);
    }
    else
    {
        writeInputEnergy(fout, ref_geom.charge, "", true);
    }

    writeInputKPoints(fout, doSCF, readInitialCharges, kpoints);
    writeInputHubbardDerivatives(fout, this->m_evaluation.SKInfo.get(), opt_info.orbital_resolved_scc, opt_info.hubbardDerivatives);

    if (spinpol)
    {
        writeUPE(fout, upe, opt_info.shell_resolved_spin, opt_info.spinConstants);
    }

    writeSKFileInfo(fout, workingDir);
    writeParseOptions(fout);
    writeOptInfo(fout, opt_info);

    fout.flush();
}

void DFTBPlusEvaluator::EvaluateCrystalEnergy(
    const CrystalGeometryData& ref_geom,
    const OptionalEvaluationInformation& opt_info, CrystalEnergyData& calc,
    DFTBResultInfo& info) const
{
    calc.uuid = ref_geom.uuid;
    calc.name = ref_geom.name;
    calc.method = "DFTB";

    QDir scratchDir(this->m_workDir.absoluteFilePath(
        getGoodFilename(QString("Crystal_Energy_%1_%2")
                            .arg(calc.name)
                            .arg(calc.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    calc.path = scratchDir.absolutePath();

    // use kpoints sampling info from geometry
    auto kpoints = ref_geom.kpoints;

    DFTBInput localinput = m_evaluation.crystal_default_dftbinput;


    int index = indexUUID(m_evaluation.evaluationCrystal.crystalstructureTargets,
        calc.uuid.toString());


    auto const & setting = m_evaluation.evaluationCrystal.crystalstructureTargets[index];

    bool spinpol = ref_geom.spinpol;
    bool opt_upe = ref_geom.opt_upe;
    double upe = ref_geom.upe;


    if (index != -1)
    {
        auto temp_input = setting.template_input;

        if (!temp_input.template_energy.isEmpty())
        {
            localinput.template_energy = temp_input.template_energy;
        }

        if (setting.hasKpointsetting)
        {
            if (setting.kpoints.m_type == ADPT::KPointsSetting::Type::Plain)
            {
                if (!setting.kpoints.m_kpoints_plain.empty())
                {
                    kpoints = setting.kpoints;
                }
            }
            else if (setting.kpoints.m_type== ADPT::KPointsSetting::Type::SupercellFolding)
            {
                bool hasValue = false;
                for (auto i : { 0, 1, 2 })
                {
                    for (auto j : { 0, 1, 2 })
                    {
                        if (setting.kpoints.m_kpoints_supercell.num_of_cells[i][j]
                            > 0)
                        {
                            hasValue = true;
                            break;
                        }
                    }
                }
                if (hasValue)
                {
                    kpoints = setting.kpoints;
                }
            }
        }
        if (setting.hasSpinPolarSetting)
        {
            spinpol = setting.spinpol_setting.spinpol;
            if (setting.spinpol_setting.has_upe)
                upe = setting.spinpol_setting.upe;
            opt_upe = setting.spinpol_setting.opt_upe;
        }
    }


    {
        QByteArray output;

        QString template_filename = m_evaluation.crystal_default_dftbinput.template_energy;
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            template_filename = localinput.template_energy;
        }

        if (spinpol)
        {
            int iter = 1;

            const long variables_count = 1;
            const long number_of_interpolation_conditions = (variables_count + 1) * (variables_count + 2) / 2;
            double variables_values[] = { upe };
            const double initial_trust_region_radius = 0.2;
            const double final_trust_region_radius = 0.001;
            const long max_function_calls_count = 30;
            const size_t working_space_size = NEWUOA_WORKING_SPACE_SIZE(
                variables_count, number_of_interpolation_conditions);
            double working_space[working_space_size];

            std::size_t function_calls_count = 0;

            m_output_stream << endl;

            m_output_stream << QString("%1 %2 %3 %4 %5 %6")
                                   .arg("Iter", 4)
                                   .arg("UPE", 8)
                                   .arg("Energy", 20)
                                   .arg("EFermi(a)", 9)
                                   .arg("EFermi(b)", 9)
                                   .arg("Delta EF", 8)
                            << endl;

            calc.energy_unit = "H";
            calc.init_upe = upe;
            calc.spinpol = true;

            auto function = [&](long n, const double* x) -> double {
                ++function_calls_count;
                upe = std::abs(x[0]);

                QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));
                if (!file.open(QIODevice::WriteOnly))
                {
                    info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
                    info.failed = true;
                    throw std::runtime_error("Error");
                }
                QTextStream fout(&file);

                writeInputFile(fout, template_filename, ref_geom, true, false, kpoints, opt_info,
                    scratchDir, true, upe);
                file.close();

                // run the dftb
                info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(),
                    this->m_program_path, QStringList(),
                    output, QString(), m_evaluation.timeout);
                QString output_str = QString(output);
                writeFile(scratchDir.absoluteFilePath("dftb.out"), output_str);


                EnergyData calc_energy_data;
                parseDFTBFermiEnergies(scratchDir.absoluteFilePath("detailed.out"),
                    calc_energy_data, info);


                calc.final_energy = calc_energy_data.total_energy;
                calc.final_fermi_energy = (calc_energy_data.fermi_a+calc_energy_data.fermi_b)*0.5;
                calc.final_upe = upe;

                if (iter == 1)
                {
                    calc.init_fermi_energy = calc.final_fermi_energy;
                    calc.init_energy = calc.final_energy;
                }

                if (info.failed)
                {
                    info.detail = output_str;
                    throw std::runtime_error("Error");
                }
                double score = calc.final_energy;

                m_output_stream << QString("%1 %2 %3 %4 %5 %6")
                                       .arg(iter, 4)
                                       .arg(upe, 8, 'f', 4)
                                       .arg(score, 20, 'f', 12)
                                       .arg(calc_energy_data.fermi_a, 9, 'f', 4)
                                       .arg(calc_energy_data.fermi_b, 9, 'f', 4)
                                       .arg(std::abs(calc_energy_data.fermi_a-calc_energy_data.fermi_b), 8, 'f', 4)
                                << endl;



                iter++;
                return score;
            };

            if (opt_upe)
            {
                auto closure = make_closure(function);
                calc.opt_upe = true;
                newuoa_closure(
                    &closure, variables_count, number_of_interpolation_conditions,
                    variables_values, initial_trust_region_radius,
                    final_trust_region_radius, max_function_calls_count, working_space);
            }
            else
            {
                calc.opt_upe = false;
                function(1, &calc.init_upe);
            }
            m_output_stream << QString("Final UPE: %1, mag= %2")
                                   .arg(calc.final_upe, 10, 'f', 4)
                                   .arg(std::sqrt(calc.final_upe * (calc.final_upe + 2.0)), 10,
                                       'f', 5)
                            << endl;
            m_output_stream << "Finished." << endl;
        }
        else
        {
            QFile file(scratchDir.absoluteFilePath("dftb_in.hsd"));
            if (!file.open(QIODevice::WriteOnly))
            {
                info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
                info.failed = true;
                return;
            }

            QTextStream fout(&file);
            writeInputFile(fout, template_filename, ref_geom, true, false, kpoints, opt_info,
                scratchDir, false, 0.0);
            file.close();

            // run the dftb
            info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
                QStringList(), output, QString(), m_evaluation.timeout);
            QString output_str = QString(output);
            writeFile(scratchDir.absoluteFilePath("dftb.out"), output_str);

            EnergyData calc_energy_data;
            parseDFTBFermiEnergies(scratchDir.absoluteFilePath("detailed.out"), calc_energy_data,
                info);


            calc.final_energy = calc_energy_data.total_energy;
            calc.final_fermi_energy = calc_energy_data.fermi_a;
            calc.init_energy = calc.final_energy ;
            calc.final_fermi_energy = calc.final_fermi_energy;


            if (info.failed)
            {
                info.detail = output_str;
                return;
            }
            calc.init_upe = 0.0;
            calc.final_upe = 0.0;

            calc.spinpol = false;
        }
    }

    return;
}

void DFTBPlusEvaluator::EvaluateSpinUnpolarizedBandStructure(
    const CrystalGeometryData& ref_geom, const BandStructureData& ref,
    const CrystalEnergyData& calc_energy_data,
    const OptionalEvaluationInformation& opt_info, BandStructureData& calc_data,
    DFTBResultInfo& info) const
{
    calc_data.uuid = ref.uuid;
    calc_data.name = ref.name;
    calc_data.method = "DFTB";
    calc_data.geom_uuid = ref.geom_uuid;

    calc_data.kpoints = ref.kpoints;

    if (ref.geom_uuid != ref_geom.uuid)
    {
        info.failed = true;
        info.detail = "geom uuid dismatch";
    }

    QDir scratchDir(this->m_workDir.absoluteFilePath(
        getGoodFilename(QString("Crystal_BandStructure_%1_%2")
                            .arg(calc_data.name)
                            .arg(calc_data.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    QFile charge_file(
        QDir(calc_energy_data.path).absoluteFilePath("charges.bin"));
    charge_file.copy(scratchDir.absoluteFilePath("charges.bin"));

    DFTBInput localinput;
    // check if specific evaluation details avaliable.
    int index = indexUUID(m_evaluation.evaluationCrystal.bandstructureTargets,
        calc_data.uuid.toString());

    localinput = m_evaluation.crystal_default_dftbinput;
    if (index > -1)
    {
        localinput = m_evaluation.evaluationCrystal.bandstructureTargets[index]
                         .template_input;
    }


    QString template_filename = m_evaluation.crystal_default_dftbinput.template_energy;

    if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
    {
        template_filename = localinput.template_energy;
    }

    double fermi_energy = calc_energy_data.final_fermi_energy;

    // TODO: klines;
    if (!info.failed)
    {
        QByteArray output;
        QFile file(scratchDir.absoluteFilePath("klines.hsd"));
        if (!file.open(QIODevice::WriteOnly))
        {
            info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
            info.failed = true;
            return;
        }

        QTextStream fout(&file);


        writeInputFile(fout, template_filename, ref_geom,
                       false, true, ref.kpoints, opt_info, scratchDir.absolutePath(), false, 0.0);

        QFile(scratchDir.absoluteFilePath("dftb_in.hsd")).remove();

        file.copy(scratchDir.absoluteFilePath("dftb_in.hsd"));

        // run the dftb
        info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
            QStringList(), output, QString(), m_evaluation.timeout);
        QString output_str = QString(output);
        DFTBTestSuite::writeFile(scratchDir.absoluteFilePath("klines.out"),
            output_str);

        // parse result
        QPair<int, int> fermiIndex = ref.fermiIndex;
        parseDFTBBandstructure(scratchDir.absoluteFilePath("band.out"), calc_data,
            info);

        if (!info.failed)
        {
            writeParsedBandStructure(scratchDir.absoluteFilePath("band.dat"),
                calc_data.bands, 0.0);

            calc_data.fermiIndex = ref.fermiIndex;

            double fermi_energy_ref = 0.0;
            if (fermiIndex.first != -1 && fermiIndex.second != -1)
            {
                if (fermiIndex.first < calc_data.bands.size() && fermiIndex.second < calc_data.bands[fermiIndex.first].size())
                {
                    fermi_energy = calc_data.bands[fermiIndex.first][fermiIndex.second];
                }
                if (fermiIndex.first < ref.bands.size() && fermiIndex.second < ref.bands[fermiIndex.first].size())
                {
                    fermi_energy_ref = ref.bands[fermiIndex.first][fermiIndex.second];
                }
            }

            writeParsedBandStructure(scratchDir.absoluteFilePath("band-ref.dat"),
                ref.bands, fermi_energy_ref);

            for (int i = 0; i < calc_data.bands.size(); ++i)
            {
                for (int j = 0; j < calc_data.bands[i].size(); ++j)
                {
                    calc_data.bands[i][j] -= fermi_energy;
                }
            }
            writeParsedBandStructure(scratchDir.absoluteFilePath("band-shifted.dat"),
                calc_data.bands, 0.0);

            int size = std::min(ref.bands.size(), calc_data.bands.size());
            for (int i = 0; i < size; ++i)
            {
                if (ref.bands[i].size() != calc_data.bands[i].size())
                {
                    info.detail = "Num of points dismatched.";
                    info.failed = true;
                }
            }
        }
        if (info.failed)
        {
            info.detail = output_str;
            return;
        }
    }
    return;
}

void DFTBPlusEvaluator::EvaluateSpinPolarizedBandStructure(
    const CrystalGeometryData& ref_geom, const BandStructureData& ref,
    const CrystalEnergyData& calc_energy_data,
    const OptionalEvaluationInformation& opt_info, BandStructureData& calc_data,
    DFTBResultInfo& info) const
{
    calc_data.uuid = ref.uuid;
    calc_data.name = ref.name;
    calc_data.method = "DFTB";
    calc_data.geom_uuid = ref.geom_uuid;

    calc_data.kpoints = ref.kpoints;

    if (ref.geom_uuid != ref_geom.uuid)
    {
        info.failed = true;
        info.detail = "geom uuid dismatch";
    }

    QDir scratchDir(this->m_workDir.absoluteFilePath(
        getGoodFilename(QString("Crystal_BandStructure_%1_%2")
                            .arg(calc_data.name)
                            .arg(calc_data.uuid.toString()))));
    scratchDir.mkpath(scratchDir.absolutePath());

    QFile charge_file(
        QDir(calc_energy_data.path).absoluteFilePath("charges.bin"));
    charge_file.copy(scratchDir.absoluteFilePath("charges.bin"));

    DFTBInput localinput;
    // check if specific evaluation details avaliable.
    int index = indexUUID(m_evaluation.evaluationCrystal.bandstructureTargets, calc_data.uuid.toString());
    auto const & setting = m_evaluation.evaluationCrystal.bandstructureTargets[index];



    localinput = m_evaluation.crystal_default_dftbinput;
    if (index > -1)
    {
        localinput = setting.template_input;
    }

    // TODO: klines;
    if (!info.failed)
    {
        QByteArray output;
        QFile file(scratchDir.absoluteFilePath("klines.hsd"));
        if (!file.open(QIODevice::WriteOnly))
        {
            info.detail = "Error: Cannot open " + file.fileName() + " for writting.";
            info.failed = true;
            return;
        }

        QString template_filename = m_evaluation.crystal_default_dftbinput.template_energy;

        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            template_filename = localinput.template_energy;
        }


        QTextStream fout(&file);

        writeInputFile(fout, template_filename, ref_geom,
                       false, true, ref.kpoints, opt_info, scratchDir.absolutePath(), true, calc_energy_data.final_upe);

        QFile(scratchDir.absoluteFilePath("dftb_in.hsd")).remove();
        file.copy(scratchDir.absoluteFilePath("dftb_in.hsd"));

        // run the dftb
        info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
            QStringList(), output, QString(), m_evaluation.timeout);
        QString output_str = QString(output);
        DFTBTestSuite::writeFile(scratchDir.absoluteFilePath("klines.out"),
            output_str);

        double fermi_energy = calc_energy_data.final_fermi_energy;

        // parse result
        QPair<int, int> fermiIndex = ref.fermiIndex;
        parseSpinPolarizedDFTBBandstructure(scratchDir.absoluteFilePath("band.out"),
            calc_data, info);

        if (!info.failed)
        {
            writeParsedBandStructure(scratchDir.absoluteFilePath("band-alpha.dat"),
                calc_data.bands, 0.0);

            calc_data.fermiIndex = ref.fermiIndex;

            double fermi_energy_ref = 0.0;
            if (fermiIndex.first != -1 && fermiIndex.second != -1)
            {
                if (fermiIndex.first < calc_data.bands.size() && fermiIndex.second < calc_data.bands[fermiIndex.first].size())
                {
                    fermi_energy = calc_data.bands[fermiIndex.first][fermiIndex.second];
                }
                if (fermiIndex.first < ref.bands.size() && fermiIndex.second < ref.bands[fermiIndex.first].size())
                {
                    fermi_energy_ref = ref.bands[fermiIndex.first][fermiIndex.second];
                }
            }

            writeParsedBandStructure(
                scratchDir.absoluteFilePath("band-ref-alpha.dat"), ref.bands,
                fermi_energy_ref);

            for (int i = 0; i < calc_data.bands.size(); ++i)
            {
                for (int j = 0; j < calc_data.bands[i].size(); ++j)
                {
                    calc_data.bands[i][j] -= fermi_energy;
                }
            }
            writeParsedBandStructure(
                scratchDir.absoluteFilePath("band-shifted-alpha.dat"),
                calc_data.bands, 0.0);

            int size = std::min(ref.bands.size(), calc_data.bands.size());
            for (int i = 0; i < size; ++i)
            {
                if (ref.bands[i].size() != calc_data.bands[i].size())
                {
                    info.detail = "Num of points dismatched.";
                    info.failed = true;
                }
            }

            //======================================================================
            // beta
            writeParsedBandStructure(scratchDir.absoluteFilePath("band-beta.dat"),
                calc_data.bands_beta, 0.0);

            calc_data.fermiIndex = ref.fermiIndex;

            fermi_energy_ref = 0.0;
            if (fermiIndex.first != -1 && fermiIndex.second != -1)
            {
                if (fermiIndex.first < calc_data.bands_beta.size() && fermiIndex.second < calc_data.bands_beta[fermiIndex.first].size())
                {
                    fermi_energy = calc_data.bands_beta[fermiIndex.first][fermiIndex.second];
                }
                if (fermiIndex.first < ref.bands_beta.size() && fermiIndex.second < ref.bands_beta[fermiIndex.first].size())
                {
                    fermi_energy_ref = ref.bands_beta[fermiIndex.first][fermiIndex.second];
                }
            }

            writeParsedBandStructure(scratchDir.absoluteFilePath("band-ref-beta.dat"),
                ref.bands_beta, fermi_energy_ref);

            for (int i = 0; i < calc_data.bands_beta.size(); ++i)
            {
                for (int j = 0; j < calc_data.bands_beta[i].size(); ++j)
                {
                    calc_data.bands_beta[i][j] -= fermi_energy;
                }
            }
            writeParsedBandStructure(
                scratchDir.absoluteFilePath("band-shifted-beta.dat"),
                calc_data.bands_beta, 0.0);

            size = std::min(ref.bands_beta.size(), calc_data.bands_beta.size());
            for (int i = 0; i < size; ++i)
            {
                if (ref.bands_beta[i].size() != calc_data.bands_beta[i].size())
                {
                    info.detail = "Num of points dismatched.";
                    info.failed = true;
                }
            }
        }
        if (info.failed)
        {
            info.detail = output_str;
            return;
        }
    }

    return;
}

void DFTBPlusEvaluator::writeInputGeometry(QTextStream& os,
    const QString& genstring) const
{
    os << "!Geometry = !GenFormat {" << endl;
    os << genstring;
    os << "}" << endl;
}

void DFTBPlusEvaluator::writeInputEnergy(QTextStream& os, int charge,
    const QString& prefix, bool writeCharge) const
{
    os << prefix << "Hamiltonian = " << prefix << "DFTB {" << endl;

    if (writeCharge)
        os << "    !Charge = " << charge << endl;

    if (m_evaluation.useDampingFactor)
    {
        os << "    !DampXH = Yes" << endl
           << "    !DampXHExponent =" << m_evaluation.dampingFactor << endl;
    }

    os << "}" << endl;

    os << "?Driver = {}" << endl;
}

void DFTBPlusEvaluator::writeSKFileInfo(QTextStream& os,
    const QDir& rootDir) const
{
    bool rootSet = (rootDir.absolutePath() != QDir::currentPath());

    if (m_evaluation.SKInfo->type2Names)
    {
        os << "+Hamiltonian = +DFTB {" << endl;

        if (!m_evaluation.SKInfo->maxAngularMomentum.isEmpty())
        {
            os << "    !MaxAngularMomentum = {" << endl;
            QMapIterator<QString, QString> it(
                m_evaluation.SKInfo->maxAngularMomentum);
            while (it.hasNext())
            {
                it.next();
                os << it.key() << " = "
                   << "\"" << it.value() << "\"" << endl;
            }
            os << "    }" << endl;
        }

        os << "    !SlaterKosterFiles = !Type2FileNames{" << endl
           << "        !Prefix = \"" << m_evaluation.SKInfo->prefix << "\"" << endl
           << "        !Suffix = \"" << m_evaluation.SKInfo->suffix << "\"" << endl
           << "        !Separator = \"" << m_evaluation.SKInfo->separator << "\""
           << endl
           << "        !LowerCaseTypeName = "
           << (m_evaluation.SKInfo->lowercase ? "Yes" : "No") << endl
           << "    }" << endl;

        os << "}" << endl;
    }
    else
    {
        os << "+Hamiltonian = +DFTB {" << endl;

        if (!m_evaluation.SKInfo->selectedShells.isEmpty())
        {
            os << "    !MaxAngularMomentum = {" << endl;
            QMapIterator<QString, QStringList> it(
                m_evaluation.SKInfo->selectedShells);
            while (it.hasNext())
            {
                it.next();
                os << "        " << it.key() << " = "
                   << "SelectedShells{" << it.value().join(" ") << "}" << endl;
            }
            os << "    }" << endl;
        }

        os << "    !SlaterKosterFiles = {" << endl;
        QMapIterator<QString, QStringList> it(m_evaluation.SKInfo->skfiles);
        while (it.hasNext())
        {
            it.next();
            os << "      " << it.key() << " = ";
            if (rootSet)
            {
                QStringList final_paths;
                for (auto const& item : it.value())
                {
                    QString temp = rootDir.relativeFilePath(item);
                    final_paths << temp;
                }
                os << final_paths.join(" ") << endl;
            }
            else
            {
                os << it.value().join(" ") << endl;
            }
        }
        os << "    }" << endl;

        os << "}" << endl; // Hamiltonian
    }
}

void DFTBPlusEvaluator::writeParseOptions(QTextStream& os) const
{
    os << "!ParserOptions {" << endl
       << "    !IgnoreUnprocessedNodes = Yes" << endl
       << "}" << endl;
}

void DFTBPlusEvaluator::writeOptInfo(QTextStream &fout, const OptionalEvaluationInformation &opt_info) const
{
    if (!opt_info.dispersion.empty())
    {
        if (opt_info.dispersion["type"] == "lj")
        {
            fout << QString("+Hamiltonian = +DFTB{") << endl
                      << "    *Dispersion = *LennardJones{" << endl
                      << "      *Parameters {" << endl;

            auto ljlist = opt_info.dispersion["parameters"].toMap();

            QMapIterator<QString, QVariant> it(ljlist);
            while (it.hasNext())
            {
                it.next();
                auto keyvalue = it.value().toMap();
                double dis = keyvalue["distance"].toDouble();
                double energy = keyvalue["distance"].toDouble();
                fout << "          *" << it.key() << " {" << endl
                          << "              *Distance [AA] = " << dis << endl
                          << "              *Energy [kcal/mol] = " << energy << endl
                          << "          }" << endl;
            }
            fout << "          }"
                      << "      }"
                      << "}" << endl;
        }
        else if (opt_info.dispersion["type"] == "d3")
        {
            auto param = opt_info.dispersion["parameters"].toMap();
            fout << QString("+Hamiltonian = +DFTB{") << endl
                      << "    *Dispersion = *DftD3{" << endl
                      << "       /s6 = " << param["s6"].toDouble() << endl
                      << "       /s8 = " << param["s8"].toDouble() << endl;

            if (param["damping"].toString() == "bj")
            {
                fout << "       *Damping = *BeckeJohnson{" << endl
                          << "         /a1 = " << param["a1"].toDouble() << endl
                          << "         /a2 = " << param["a2"].toDouble() << endl
                          << "       }" << endl;
            }
            else if (param["damping"].toString() == "zero")
            {
                fout << "       *Damping = *ZeroDamping{" << endl
                          << "         /sr6 = " << param["sr6"].toDouble() << endl
                          << "         /alpha6 = " << param["alpha6"].toDouble() << endl
                          << "       }" << endl;
            }
            fout << "    }" << endl;
            fout << "}" << endl;
        }
    }

}

QString DFTBPlusEvaluator::findDriverName(const QString& filename) const
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&file);
        QString content = stream.readAll();
        QRegExp rx(".*Driver\\s*=\\s*(\\w*).*");
        if (rx.exactMatch(content))
        {
            return rx.capturedTexts().at(1);
        }
    }
    return "";
}

QString DFTBPlusEvaluator::getGoodFilename(const QString& oriFilename) const
{
    QString newFileName = oriFilename;
    newFileName.replace(QRegExp("[^a-zA-Z0-9_+\\-]"), "");
    return newFileName;
}

bool DFTBPlusEvaluator::parseDFTBFirstPointEnergyInGOPT(const QString& filename,
    double& energy) const
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return false;
    }

    QTextStream stream(&file);
    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        if (line.contains("Geometry step: 0"))
        {
            while (!stream.atEnd())
            {
                line = stream.readLine();
                if (line.contains("Geometry step:"))
                {
                    return false;
                }
                if (line.contains("Total Energy:"))
                {
                    QStringList arr = line.split(" ", QString::SkipEmptyParts);
                    bool ok;
                    double l_energy = arr[2].toDouble(&ok);
                    if (ok)
                    {
                        energy = l_energy;
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

void DFTBPlusEvaluator::writeInputDriverOpt(QTextStream& os,
    const QString& optMethod) const
{
    os << "*Driver = *" << optMethod << "{" << endl
       << "    !OutputPrefix = \"geom.out\"" << endl
       << "    !MaxSteps = 500" << endl
       << "    !AppendGeometries = Yes" << endl
       << "}" << endl;
}
void DFTBPlusEvaluator::writeInputFreq(QTextStream& os) const
{
    os << "+Hamiltonian = +DFTB {" << endl
       << "    !SCCTolerance = " << 1.0e-12 << endl
       << "}" << endl;

    os << "*Driver = *SecondDerivatives {" << endl
       << "}" << endl;
}

void DFTBPlusEvaluator::writeInputKPoints(
    QTextStream& os, bool doSCF, bool readCharge,
    const ADPT::KPointsSetting& kpoints) const
{



    if (kpoints.m_type == ADPT::KPointsSetting::Type::KLines)
    {
        os << "+Hamiltonian = +DFTB {" << endl;
        if (readCharge)
        {
            os << "    !ReadInitialCharges = Yes" << endl;
        }
        else
        {
            os << "    !ReadInitialCharges = No" << endl;
        }

        if (!doSCF)
        {
            os << "    !MaxSCCIterations = 1" << endl;
        }

        os << "    !KPointsAndWeights = !Klines {" << endl;

        for (int i = 0; i < kpoints.m_kpoints_klines.size(); ++i)
        {
            os << QString("%1 %2 %3 %4")
                      .arg(kpoints.m_kpoints_klines[i].num_of_points)
                      .arg(kpoints.m_kpoints_klines[i].kpoint[0], 0, 'f', 6)
                      .arg(kpoints.m_kpoints_klines[i].kpoint[1], 0, 'f', 6)
                      .arg(kpoints.m_kpoints_klines[i].kpoint[2], 0, 'f', 6)
               << endl;
        }
        os << "    }";
        os << "}" << endl;

        os << "?Driver = !{}" << endl;
    }
    else if (kpoints.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
    {
        os << "+Hamiltonian = +DFTB {" << endl;
        if (readCharge)
        {
            os << "    !ReadInitialCharges = Yes" << endl;
        }
        else
        {
            os << "    !ReadInitialCharges = No" << endl;
        }

        if (!doSCF)
        {
            os << "    !MaxSCCIterations = 1" << endl;
        }

        os << "    !KPointsAndWeights = !SupercellFolding {" << endl;

        for (auto i : { 0, 1, 2 })
        {
            for (auto j : { 0, 1, 2 })
            {
                os << " " << kpoints.m_kpoints_supercell.num_of_cells[i][j];
            }
            os << endl;
        }
        for (auto i : { 0, 1, 2 })
        {
            os << " " << kpoints.m_kpoints_supercell.shifts[i];
        }
        os << endl;

        os << "    }";

        os << "}" << endl;

        os << "?Driver = !{}" << endl;
    }
    else
    {
        os << "+Hamiltonian = +DFTB {" << endl;
        if (readCharge)
        {
            os << "    !ReadInitialCharges = Yes" << endl;
        }
        else
        {
            os << "    !ReadInitialCharges = No" << endl;
        }

        if (!doSCF)
        {
            os << "    !MaxSCCIterations = 1" << endl;
        }

        os << "    !KPointsAndWeights = !{" << endl;

        for (auto const& item : kpoints.m_kpoints_plain)
        {
            os << item.kpoint[0] << " " << item.kpoint[1] << " " << item.kpoint[2]
               << " " << item.weight << endl;
        }

        os << "    }";

        os << "}" << endl;

        os << "?Driver = !{}" << endl;
    }
}

bool DFTBPlusEvaluator::checkoutKPointDefinition(
    const QString& filename) const
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        QByteArray array = file.readAll();
        QTextStream stream(&array, QIODevice::ReadOnly | QIODevice::Text);
        QString content = stream.readAll();

        QRegExp rx(".*KPointsAndWeights\\s*=\\s*(\\w*)\\s*\\{.*",
            Qt::CaseInsensitive);

        if (rx.exactMatch(content))
        {
            if (rx.capturedTexts().size() > 0 && rx.capturedTexts().at(1).toLower() != "supercellfolding")
            {
                return false;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void DFTBPlusEvaluator::parseDFTBBandstructure(const QString& filename,
    BandStructureData& calc_data,
    DFTBResultInfo& info) const
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        info.failed = true;
        info.detail = "Cannot open bands.out for reading the band structure data.";
        return;
    }
    QTextStream stream(&file);
    QMap<int, QVector<double>> local_bands;

    int numPoints = 0;
    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        if (line.contains("KPT"))
        {
            QStringList line_array = line.split(" ", QString::SkipEmptyParts);
            if (line_array.size() != 6)
            {
                info.detail = "ERROR: bands.out format";
                info.failed = true;
                return;
            }
            int current_spin = line_array[3].toInt() - 1;
            if (current_spin != 0)
            {
                break;
            }


            int indexBand = 0;
            while (!stream.atEnd())
            {
                QString subline = stream.readLine();
                if (subline.trimmed().isEmpty())
                {
                    break;
                }
                QStringList subline_array = subline.split(" ", QString::SkipEmptyParts);
                if (subline_array.size() != 3)
                {
                    info.detail = "ERROR: bands.out entry does not have 3 columns";
                    info.failed = true;
                    return;
                }
                bool ok;
                double energy = subline_array[1].toDouble(&ok);
                if (!ok)
                {
                    info.detail = "ERROR: bands.out entry read error";
                    info.failed = true;
                    return;
                }

                if (!local_bands.contains(indexBand))
                {
                    QVector<double> newBand;
                    newBand.append(energy);
                    local_bands.insert(indexBand, newBand);
                }
                else
                {
                    local_bands[indexBand].append(energy);
                }
                ++indexBand;
            }
            ++numPoints;
        }
    }

    if (local_bands.isEmpty())
    {
        info.detail = "No valid band.out produced";
        info.failed = true;
        calc_data.bands.clear();
        return;
    }

    int supposedNumPoints = calc_data.kpoints.numOfPoints();

    QMapIterator<int, QVector<double>> it(local_bands);

    while (it.hasNext())
    {
        it.next();
        const QVector<double> value = it.value();
        if (value.size() != supposedNumPoints)
        {
            info.detail = "Num of points in bands dismatched.";
            info.failed = true;
            calc_data.bands.clear();
            return;
        }
        calc_data.bands.append(value);
    }
}

void DFTBPlusEvaluator::parseSpinPolarizedDFTBBandstructure(
    const QString& filename, BandStructureData& calc_data,
    DFTBResultInfo& info) const
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        info.failed = true;
        info.detail = "Cannot open bands.out for reading the band structure data.";
        return;
    }
    QTextStream stream(&file);

    std::array<QMap<int, QVector<double>>, 2> local_bands;
    int numPoints = 0;
    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        if (line.contains("KPT"))
        {
            QStringList line_array = line.split(" ", QString::SkipEmptyParts);
            if (line_array.size() != 6)
            {
                info.detail = "ERROR: bands.out format";
                info.failed = true;
                return;
            }
            int current_spin = line_array[3].toInt() - 1;

            int indexBand = 0;
            while (!stream.atEnd())
            {
                QString subline = stream.readLine();
                if (subline.trimmed().isEmpty())
                {
                    break;
                }
                QStringList subline_array = subline.split(" ", QString::SkipEmptyParts);
                if (subline_array.size() != 3)
                {
                    info.detail = "ERROR: bands.out entry does not have 3 columns";
                    info.failed = true;
                    return;
                }
                bool ok;
                double energy = subline_array[1].toDouble(&ok);
                if (!ok)
                {
                    info.detail = "ERROR: bands.out entry read error";
                    info.failed = true;
                    return;
                }

                if (!local_bands[current_spin].contains(indexBand))
                {
                    QVector<double> newBand;
                    newBand.append(energy);
                    local_bands[current_spin].insert(indexBand, newBand);
                }
                else
                {
                    local_bands[current_spin][indexBand].append(energy);
                }
                ++indexBand;
            }
            ++numPoints;
        }
    }

    int supposedNumPoints = calc_data.kpoints.numOfPoints();

    {
        QMapIterator<int, QVector<double>> it(local_bands[0]);
        while (it.hasNext())
        {
            it.next();
            const QVector<double> value = it.value();
            if (value.size() != supposedNumPoints)
            {
                info.detail = "Num of points in bands dismatched.";
                info.failed = true;
                calc_data.bands.clear();
                return;
            }
            calc_data.bands.append(value);
        }
    }
    {
        QMapIterator<int, QVector<double>> it(local_bands[1]);
        while (it.hasNext())
        {
            it.next();
            const QVector<double> value = it.value();
            if (value.size() != supposedNumPoints)
            {
                info.detail = "Num of points in bands dismatched.";
                info.failed = true;
                calc_data.bands_beta.clear();
                return;
            }
            calc_data.bands_beta.append(value);
        }
    }
}

void DFTBPlusEvaluator::writeParsedBandStructure(
    const QString& filename, const QVector<QVector<double>>& bands,
    double shift) const
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        return;
    }
    QTextStream stream(&file);

    int columns = bands.size();
    int rows = bands[0].size();

    for (int i = 0; i < rows; ++i)
    {
        stream << i + 1;
        for (int j = 0; j < columns; ++j)
        {
            stream << QString(" %1").arg(bands[j][i] - shift, 0, 'f', 6);
        }
        stream << endl;
    }
}
}


bool DFTBTestSuite::DFTBPlusEvaluator::supportDirectFrequencyCalculation() const
{
    return false;
}
