#ifndef EVALUATORFACTORY_H
#define EVALUATORFACTORY_H

#include <QString>
#include <QTextStream>
namespace DFTBTestSuite{


class Evaluation;
class Evaluator;
class EvaluatorFactory
{
public:
    static Evaluator* getInstance(const QString& keyword, QTextStream& output_stream, const QString& code_path, const Evaluation &eval_opts);
private:
    EvaluatorFactory();
    static EvaluatorFactory m_factory;
};


}
#endif // EVALUATORFACTORY_H
