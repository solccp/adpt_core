#ifndef DFTBTESTER_H
#define DFTBTESTER_H

#include <QObject>
#include <QDir>
#include <QScopedPointer>
#include <QTemporaryDir>

#include <QMutex>

#include "DFTBTestSuite/InputFile.h"

class QTextStream;

namespace DFTBTestSuite{
class Statistics;
class ReferenceData;
class Evaluator;
class Formatter;
class EvaluatedData;
class OptionalEvaluationInformation;

class DFTBTester : public QObject
{
    Q_OBJECT
public:
    DFTBTester() = delete;

    DFTBTester(const InputData& inputData, const ReferenceData* referenceData, QObject *parent = 0);
    bool evaluate(QTextStream& stdout_stream);

    bool copyIntermediateFiles(const QString& srcPath, const QString& destPath);


    const Statistics* getStatistics() const;
    const EvaluatedData *getEvalutedData() const;


    ~DFTBTester();
    const InputData& getInputData();

    QDir tempRoot() const;
    void setTempRoot(const QDir &tempRoot);

    QString getTempPath() const;

    void setAutoRemove(bool value);

    QDir tempDir() const;
    void setTempDir(const QDir &tempDir);

    QString intermidiate_suffix() const;
    void setIntermidiate_suffix(const QString &intermidiate_suffix);

    bool showErrorUnitResult() const;
    void setShowErrorUnitResult(bool showErrorUnitResult);

signals:

    void progressUpdate(const QString& msg);
public slots:
    void stop();
private:

    void initializeOptInfo(OptionalEvaluationInformation& opt_info);

    Statistics *m_statistic_analyser = nullptr;
    InputData m_inputData;
    const ReferenceData* m_referenceData = nullptr;
    Evaluator* m_evaluator = nullptr;
    Formatter* m_formatter = nullptr;

    QDir m_tempRoot;
    QString m_tempPath;

    QString m_intermidiate_suffix;

    bool isAutoRemove = true;
    QScopedPointer<QTemporaryDir> m_autoTempDir;

    bool m_showErrorUnitResult = true;
    bool m_stop = false;

    QMutex m_mutex;

    EvaluatedData* m_calc_data = nullptr;
};

}

#endif // DFTBTESTER_H
