#include "dcdftbkevaluator.h"


#include "DFTBTestSuite/util.h"
#include "typesettings.h"
#include "io_utils.h"

#include <QDebug>

#include <QString>
#include <QRegExp>
#include <fstream>
#include "DFTBTestSuite/ReferenceDataType/CrystalData.h"
#include <QRegularExpression>

namespace DFTBTestSuite
{

DCDFTBKEvaluator::DCDFTBKEvaluator(QTextStream & output_stream, const Evaluation& eval_opts, const QString &codePath) : Evaluator(output_stream, eval_opts, codePath)
{


}

DCDFTBKEvaluator::~DCDFTBKEvaluator()
{

}


int getL(const QString& orbtype)
{
    QString orb_str = "spdfghijkl";
    return orb_str.indexOf(orbtype) + 1;
}

void DCDFTBKEvaluator::EvaluateAtomicEnergy(AtomicData &calc, const QString &template_input, const OptionalEvaluationInformation &opt_info, DFTBResultInfo &info) const
{
    QDir tempFolder(
        this->m_workDir.absoluteFilePath(QString("Atomic_%1").arg(calc.symbol)));
    tempFolder.mkpath(tempFolder.absolutePath());

    DCDFTBKInput input;

    input.parseDCDFTBKInput(template_input);

    auto& keyword_input = input.keyword_section;

    keyword_input["DC"] = false;
    keyword_input.remove("OPT");
    keyword_input.remove("PBC");
    keyword_input.remove("FORCE");

    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;
    if (keyword_input["SCC"].canConvert<QVariantMap>())
    {
        auto map = keyword_input["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }

    input.title_section = calc.symbol;
    QStringList elemList;
    elemList << calc.symbol;
    input.SK_section = prepare_SK_section(usingSpin, &opt_info, this->m_evaluation.SKInfo.get(), usingORSCC, tempFolder, usingDFTB3, elemList);
    //        input.geometry_section = system->geometry.getDCDFTBKXYZ();

    double elecE = 0.0;
    {
        QFile fout(tempFolder.absoluteFilePath("dftb.inp"));
        if (!fout.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            throw std::runtime_error("Cannot open " + tempFolder.absoluteFilePath("dftb.inp").toStdString() + " for writing.");
        }

        QTextStream stream(&fout);
        input.writeInput(stream);

        {

            QByteArray output;
            ADPT::ExecuteCommandStream(tempFolder.absolutePath(), this->m_program_path, QStringList(), output, QByteArray(), m_evaluation.timeout);

            QFile autotest_tag(tempFolder.absoluteFilePath("dftb.out"));
            if (!autotest_tag.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                QByteArray errorMsg;
                QTextStream error_stream(&errorMsg);
                error_stream << autotest_tag.fileName();
                error_stream << endl
                             << "ERROR: " << endl
                             << endl;

                throw std::runtime_error(QString(errorMsg).toStdString());
            }

            QTextStream tin(&autotest_tag);
            bool foundEEl = false;

            while (!tin.atEnd())
            {
                QString line = tin.readLine();
                if (line.contains("Final SCC-DFTB Energy =") ||
                    line.contains("Final DFTB-3rd Energy =") ||
                    line.contains("Final DC-SCC-DFTB Energy =") ||
                    line.contains("Final DC-DFTB-3rd Energy =")
                        )
                {
                    QStringList arr = line.split(" ", QString::SkipEmptyParts);
                    elecE = arr[4].toDouble();
                    foundEEl = true;
                }
                else if (line.contains("NCC-DFTB Energy =") ||
                         line.contains("DC-NCC-DFTB Energy"))
                     {
                         QStringList arr = line.split(" ", QString::SkipEmptyParts);
                         elecE = arr[3].toDouble();
                         foundEEl = true;
                     }
            }
            if (!foundEEl)
            {
                throw std::runtime_error("Error reading electronic energy");
            }
        }
    }
    calc.energy = elecE;
    info.failed = false;
}

void DCDFTBKEvaluator::EvaluateGeometry(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const
{
    QDir scratchDir(this->m_workDir.absoluteFilePath(
                        getGoodFilename(
                            QString("Mole_Geom_%1_%2").arg(calc.name)
                            .arg(calc.uuid.toString())
                            )
                        )
                    );

    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb.inp"));

    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting." ;
        info.failed = true;
        return ;
    }

    calc.totalEnergyData.isValid = false;
    calc.geometryData.optimized = false;

    QTextStream fout(&file);

    // prepare the input

    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets, calc.uuid.toString());


    DCDFTBKInput final_input;
    final_input.keyword_section["DC"] = false;
    final_input.title_section = ref.name;

    //first, read the template input for energy
    if (!this->m_evaluation.mole_default_dftbinput.template_energy
             .isEmpty()
        && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
               .exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_energy);
    }

    // if specified input is provided
    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_energy);
        }
    }

    //optimization part

    QVariantMap opt_section;
    opt_section["OPTTYPE"] = 3;
    opt_section["MAXITER"] = 500;
    final_input.keyword_section["OPT"] = opt_section;

    if (!m_evaluation.mole_default_dftbinput.template_opt.isEmpty() && QFileInfo(m_evaluation.mole_default_dftbinput.template_opt).exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_opt);
    }

    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        if (!localinput.template_opt.isEmpty() && QFileInfo(localinput.template_opt).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_opt);
        }
    }


    //check the template input
    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;

    if (ref.spin > 1)
    {
        if (!final_input.keyword_section["SCC"].canConvert<QVariantMap>())
        {
            QVariantMap scc_section;
            scc_section["SPIN"] = true;
            final_input.keyword_section["SCC"] = scc_section;
        }
    }

    if (final_input.keyword_section["SCC"].canConvert<QVariantMap>())
    {
        auto map = final_input.keyword_section["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }






    final_input.SK_section = prepare_SK_section(usingSpin, &opt_info, m_evaluation.SKInfo.get(),
                                                usingORSCC, scratchDir, usingDFTB3,
                                                ref.geometryData.geometry.getElements()
                                                );




    final_input.geometry_section = getInputGeometry(ref.geometryData.geometry.NumAtom(), ref.charge, ref.spin, ref.geometryData.geometry.toNoHeaderXYZ());

    final_input.writeInput(fout);
    fout.flush();
    file.close();


    info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
        QStringList(), output, QString(), m_evaluation.timeout);

    calc.geometryData.geometry = ref.geometryData.geometry;
    parseDFTBResult(scratchDir.absoluteFilePath("dftb.out"), calc, info);
}

void DCDFTBKEvaluator::EvaluateFrequency(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const
{
    QDir scratchDir(this->m_workDir.absoluteFilePath(
                        getGoodFilename(
                            QString("Mole_Geom_%1_%2").arg(calc.name)
                            .arg(calc.uuid.toString())
                            )
                        )
                    );

    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb.inp"));

    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting." ;
        info.failed = true;
        return ;
    }

    calc.totalEnergyData.isValid = false;
    calc.geometryData.optimized = false;

    QTextStream fout(&file);

    // prepare the input

    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets, calc.uuid.toString());


    DCDFTBKInput final_input;
    final_input.keyword_section["DC"] = false;
    final_input.title_section = ref.name;


    //first, read the template input for energy
    if (!this->m_evaluation.mole_default_dftbinput.template_energy
             .isEmpty()
        && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
               .exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_energy);
    }

    // if specified input is provided
    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_energy);
        }
    }

    //optimization part

    QVariantMap opt_section;
    opt_section["OPTTYPE"] = 3;
    opt_section["MAXITER"] = 500;
    final_input.keyword_section["OPT"] = opt_section;

    if (!m_evaluation.mole_default_dftbinput.template_opt.isEmpty() && QFileInfo(m_evaluation.mole_default_dftbinput.template_opt).exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_opt);
    }

    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        if (!localinput.template_opt.isEmpty() && QFileInfo(localinput.template_opt).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_opt);
        }
    }

    final_input.keyword_section["FREQ"] = true;
    if (!m_evaluation.mole_default_dftbinput.template_frequency.isEmpty() && QFileInfo(m_evaluation.mole_default_dftbinput.template_frequency).exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_frequency);
    }

    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        if (!localinput.template_frequency.isEmpty() && QFileInfo(localinput.template_frequency).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_frequency);
        }
    }





    //check the template input
    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;

    if (ref.spin > 1)
    {
        if (!final_input.keyword_section["SCC"].canConvert<QVariantMap>())
        {
            QVariantMap scc_section;
            scc_section["SPIN"] = true;
            final_input.keyword_section["SCC"] = scc_section;
        }
    }

    if (final_input.keyword_section["SCC"].canConvert<QVariantMap>())
    {
        auto map = final_input.keyword_section["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }


    final_input.SK_section = prepare_SK_section(usingSpin, &opt_info, m_evaluation.SKInfo.get(),
                                                usingORSCC, scratchDir, usingDFTB3,
                                                ref.geometryData.geometry.getElements()
                                                );




    final_input.geometry_section = getInputGeometry(ref.geometryData.geometry.NumAtom(), ref.charge, ref.spin, ref.geometryData.geometry.toNoHeaderXYZ());

    final_input.writeInput(fout);
    fout.flush();
    file.close();


    info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
        QStringList(), output, QString(), m_evaluation.timeout);

    parseDFTBResult(scratchDir.absoluteFilePath("dftb.out"), calc, info);

}

void DCDFTBKEvaluator::EvaluateMolecularEnergy(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const
{
    QDir scratchDir(this->m_workDir.absoluteFilePath(
                        getGoodFilename(
                            QString("Mole_Geom_%1_%2").arg(calc.name)
                            .arg(calc.uuid.toString())
                            )
                        )
                    );

    scratchDir.mkpath(scratchDir.absolutePath());

    QByteArray output;
    QFile file(scratchDir.absoluteFilePath("dftb.inp"));

    if (!file.open(QIODevice::WriteOnly))
    {
        info.detail = "Error: Cannot open " + file.fileName() + " for writting." ;
        info.failed = true;
        return ;
    }

    calc.totalEnergyData.isValid = false;
    calc.geometryData.optimized = false;

    QTextStream fout(&file);

    // prepare the input

    int index = indexUUID(m_evaluation.evaluationMolecule.moleculeTargets, calc.uuid.toString());


    DCDFTBKInput final_input;

    //first, read the template input for energy
    if (!this->m_evaluation.mole_default_dftbinput.template_energy
             .isEmpty()
        && QFileInfo(m_evaluation.mole_default_dftbinput.template_energy)
               .exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_energy);
    }

    // if specified input is provided
    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        // the Hamiltonian part
        if (!localinput.template_energy.isEmpty() && QFileInfo(localinput.template_energy).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_energy);
        }
    }

    //optimization part

    final_input.keyword_section["OPT"] = false;

    if (!m_evaluation.mole_default_dftbinput.template_opt.isEmpty() && QFileInfo(m_evaluation.mole_default_dftbinput.template_opt).exists())
    {
        final_input.parseDCDFTBKInput(m_evaluation.mole_default_dftbinput.template_opt);
    }

    if (index > -1)
    {
        const DFTBInput& localinput = m_evaluation.evaluationMolecule.moleculeTargets[index].template_input;

        if (!localinput.template_opt.isEmpty() && QFileInfo(localinput.template_opt).exists())
        {
            final_input.parseDCDFTBKInput(localinput.template_opt);
        }
    }


    //check the template input
    bool usingDFTB3 = false;
    bool usingORSCC = false;
    bool usingSpin = false;
    if (final_input.keyword_section["SCC"].canConvert<QVariantMap>())
    {
        auto map = final_input.keyword_section["SCC"].toMap();
        usingDFTB3 = map.value("THIRDDIAG", QVariant(false)).toBool();
        if (!usingDFTB3)
            usingDFTB3 = map.value("THIRDFULL", QVariant(false)).toBool();

        usingORSCC = map.value("ORSCC", QVariant(false)).toBool();
        usingSpin = map.value("SPIN", QVariant(false)).toBool();
    }

    final_input.SK_section = prepare_SK_section(usingSpin, &opt_info, m_evaluation.SKInfo.get(),
                                                usingORSCC, scratchDir, usingDFTB3,
                                                ref.geometryData.geometry.getElements()
                                                );




    final_input.geometry_section = getInputGeometry(ref.geometryData.geometry.NumAtom(), ref.charge, ref.spin, ref.geometryData.geometry.toNoHeaderXYZ());

    final_input.writeInput(fout);
    fout.flush();
    file.close();


    info.failed = !ADPT::ExecuteCommand(scratchDir.absolutePath(), this->m_program_path,
        QStringList(), output, QString(), m_evaluation.timeout);

    calc.geometryData.geometry = ref.geometryData.geometry;

    parseDFTBResult(scratchDir.absoluteFilePath("dftb.out"), calc, info);

    if (!calc.totalEnergyData.isValid)
    {
        info.failed = true;
        info.detail = "No valid energy found";
    }

}

QString DCDFTBKEvaluator::prepare_SK_section(bool usingSpin,
                                          const OptionalEvaluationInformation* opt_info,
                                          const ADPT::SKFileInfo* skfileinfo,
                                          bool usingORSCC, QDir tempFolder, bool usingDFTB3,
                                          const QStringList& elemList_) const
{
    QByteArray data;
    QTextStream final_input(&data);

    auto elemList = elemList_;
    elemList.removeDuplicates();

    int nelems = elemList.size();

    final_input << nelems << endl;
    for (int i = 0; i < elemList.size(); ++i)
    {
        auto elem = elemList[i];
        auto maxl = skfileinfo->maxAngularMomentum[elem];
        int l = getL(maxl);
        final_input << elem << " " << l;
        if (usingDFTB3)
        {
            if (!opt_info->hubbardDerivatives.contains(elem))
            {
                throw std::runtime_error("ERROR! Requested DFTB3 calculation but hubbard derivatives not provided!");
            }
            auto hubb = opt_info->hubbardDerivatives[elem];
            if (usingORSCC)
            {
                final_input << " " << hubb[0] << " " << hubb[1] << " " << hubb[2];
            }
            else
            {
                final_input << " " << hubb[2];
            }
        }
        if (usingSpin)
        {
            if (!opt_info->spinConstants.contains(elem))
            {
                throw std::runtime_error("ERROR! Request spin-polarized calculation but spin constants not provided!");
            }

            auto fname = QString("%1.wll").arg(elem);
            QStringList spin_constants;
            for (auto value : opt_info->spinConstants[elem])
            {
                spin_constants.append(value.toString());
            }
            ADPT::writeFile(tempFolder.absoluteFilePath(fname), spin_constants.join("\n") + "\n");
            final_input << " " << fname;
        }
        final_input << endl;
        for (int j = 0; j < elemList.size(); ++j)
        {
            auto skfiles = prepare_SKFiles(tempFolder.absolutePath(), elem, elemList[j], skfileinfo);
            final_input << " " << skfiles.join(" ");
        }
        final_input << endl;
    }
    return QString(data);
}

QString DCDFTBKEvaluator::getInputGeometry(int natom, int charge, int spin, const QString& xyzstring) const
{
    QByteArray data;
    QTextStream os(&data);
    os << natom << " " << charge <<  " " << spin << endl;
    os << xyzstring << endl;
    os << endl;
    return QString(data);
}



QStringList DCDFTBKEvaluator::prepare_SKFiles(const QString &scratch, const QString &elem1, const QString &elem2,
                                             const ADPT::SKFileInfo* skfileinfo) const
{
    QDir dir(scratch);
    QString final_elem1 = elem1;
    QString final_elem2 = elem2;
    QStringList res;

    if (skfileinfo->type2Names)
    {
        if (skfileinfo->lowercase)
        {
            final_elem1 = elem1.toLower();
            final_elem2 = elem2.toLower();
        }
        QFileInfo info(QString("%1%2%3%4%5")
                       .arg(skfileinfo->prefix)
                       .arg(final_elem1).arg(skfileinfo->separator)
                       .arg(final_elem2).arg(skfileinfo->suffix));
        if (info.exists())
        {
            res.append(dir.relativeFilePath(info.absoluteFilePath()));
        }
    }
    else
    {
        QMapIterator<QString, QStringList> it(skfileinfo->skfiles);
        while(it.hasNext())
        {
            it.next();
            auto poten = ADPT::PotentialName::fromString(it.key());
            if (elem1 == poten.getElement1() && elem2 == poten.getElement2() )
            {
                for(auto const& item : it.value())
                {
                    QFileInfo info(item);
                    res.append(dir.relativeFilePath(info.absoluteFilePath()));
                }
            }
        }
    }

    if (res.isEmpty())
    {
        throw std::runtime_error(QString("Cannot find potential %1-%2").arg(elem1).arg(elem2).toStdString());
    }

    return res;
}




QString DCDFTBKEvaluator::getGoodFilename(const QString &oriFilename) const
{
    QString newFileName = oriFilename;
    newFileName.replace(QRegExp("[^a-zA-Z0-9_+\\-]"), "");
    return newFileName;

}

bool DCDFTBKEvaluator::parseDFTBResult(const QString &filename, MolecularData &calc, DFTBResultInfo &info) const
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        m_output_stream << "ERROR: Cannot open DC-DFTB-K output file: " << filename << endl;
        return false;
    }

    QTextStream tin(&file);
    calc.geometryData.optimized = false;
    calc.totalEnergyData.isOptimized = false;
    calc.totalEnergyData.isValid = false;

    bool first_energy = true;


    while (!tin.atEnd())
    {
        QString line = tin.readLine();
        if (line.contains("Final SCC-DFTB Energy =") ||
            line.contains("Final DFTB-3rd Energy =") ||
            line.contains("Final DC-SCC-DFTB Energy =") ||
            line.contains("Final DC-DFTB-3rd Energy =")
                )
        {
            QStringList arr = line.split(" ", QString::SkipEmptyParts);
            if (first_energy)
            {
                calc.totalEnergyData.energy = arr[4].toDouble();
                first_energy = false;
            }
            else
            {
                calc.totalEnergyData.optEnergy = arr[4].toDouble();
            }
            calc.totalEnergyData.isValid = true;
        }
        else if (line.contains("NCC-DFTB Energy =") ||
                 line.contains("DC-NCC-DFTB Energy"))
        {
            QStringList arr = line.split(" ", QString::SkipEmptyParts);
            if (first_energy)
            {
                calc.totalEnergyData.energy = arr[3].toDouble();
                first_energy = false;
            }
            else
            {
                calc.totalEnergyData.optEnergy = arr[3].toDouble();
            }
            calc.totalEnergyData.isValid = true;
        }
        else if (line.contains("Final molecular coordinate (angs)"))
        {
            calc.totalEnergyData.isOptimized = true;
            calc.geometryData.optimized = true;

            calc.geometryData.geometry.clear();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            while(true)
            {
                line = tin.readLine();
                if (line.contains("-------------------------"))
                {
                    break;
                }
                auto arr = line.split(" ", QString::SkipEmptyParts);
                calc.geometryData.geometry.addAtom(arr[0], arr[1].toDouble(), arr[2].toDouble(), arr[3].toDouble());
            }
        }
        else if (line.contains("Normal coordinate analysis in harmonic approximation"))
        {
            calc.frequencyData.isValid = true;
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();
            line = tin.readLine();

            while(true)
            {
                line = tin.readLine();
                if (line.contains("-------------------------"))
                {
                    break;
                }
                auto arr = line.split(" ", QString::SkipEmptyParts);
                if (arr[4].contains("Vibration"))
                {
                    double freq = arr[1].toDouble();
                    calc.frequencyData.frequencies.append(freq);
                }
                calc.geometryData.geometry.addAtom(arr[0], arr[1].toDouble(), arr[2].toDouble(), arr[3].toDouble());
            }
        }

    }



    return true;
}


void DCDFTBKInput::parseDCDFTBKInput(const QString& filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString errMsg = "ERROR: Cannot open template DFTB input file: " + filename;
        throw std::runtime_error(errMsg.toStdString());
    }

    QTextStream fin(&file);

    parseDCDFTBKKeywordInput(this->keyword_section, fin);

    QString res = parseDCDFTBKTitleInput(fin);
    if (!res.isEmpty())
    {
        this->title_section = res;
    }

    res = parseDCDFTBKSKInput(fin);
    if (!res.isEmpty())
    {
        this->SK_section = res;
    }

    res = parseDCDFTBKGeometryInput(fin);
    if (!res.isEmpty())
    {
        this->geometry_section = res;
    }

    res = parseDCDFTBKFinalInput(fin);
    if (!res.isEmpty())
    {
        this->final_section = res;
    }
    file.close();
}

void DCDFTBKInput::parseDCDFTBKKeywordInput(QVariantMap& keywords, QTextStream& os)
{
    QRegularExpression keywordline_false("^\\s*(\\w+)\\s*=\\s*(FALSE|TRUE)\\s*$");
    QRegularExpression keywordline_subsection("\\s*(\\w+)=\\((.*)\\)");

    QVariantMap input = keywords;
    while (!os.atEnd())
    {
        QString line;
        line = os.readLine();
        if (line.trimmed().isEmpty())
        {
            break;
        }
        QRegularExpressionMatch match = keywordline_false.match(line);
        if (match.hasMatch())
        {
            auto value = match.captured(2).toUpper();
            if (value == "FALSE")
                input[match.captured(1)] = false;
            else
                input[match.captured(1)] = true;
        }
        match = keywordline_subsection.match(line);
        if (match.hasMatch())
        {
            auto section = match.captured(1);
            auto keyline = match.captured(2);
            QVariantMap dict;
            auto keys = keyline.split(" ", QString::SkipEmptyParts);
            for (auto item : keys)
            {
                auto values = item.split("=", QString::SkipEmptyParts);
                dict[values[0].trimmed()] = values[1].trimmed();
            }
            input[section] = dict;
        }
    }

    keywords = input;
    return;
}

QString DCDFTBKInput::parseDCDFTBKStringSection(QTextStream& os)
{
    QByteArray data;
    QTextStream stream(&data);
    while (!os.atEnd())
    {
        QString line;
        line = os.readLine();
        if (line.trimmed().isEmpty())
        {
            break;
        }
        stream << line << endl;
    }
    return QString(data);
}

QString DCDFTBKInput::parseDCDFTBKTitleInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKSKInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKGeometryInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

QString DCDFTBKInput::parseDCDFTBKFinalInput(QTextStream& os)
{
    try
    {
        return parseDCDFTBKStringSection(os);
    }
    catch (...)
    {
        return QString();
    }
}

void DCDFTBKInput::writeInput(QTextStream& os) const
{

    QMapIterator<QString, QVariant> it(keyword_section);
    while (it.hasNext())
    {
        it.next();
        if (it.value().canConvert<bool>())
        {
            bool value = it.value().toBool();
            if (value)
            {
                os << QString("%1=TRUE").arg(it.key()) << endl;
            }
            else
            {
                os << QString("%1=FALSE").arg(it.key()) << endl;
            }
        }
        else if (it.value().canConvert<QVariantMap>())
        {
            QStringList values;
            QMapIterator<QString, QVariant> it2(it.value().toMap());
            while (it2.hasNext())
            {
                it2.next();
                values.append(QString("%1=%2").arg(it2.key()).arg(it2.value().toString()));
            }
            os << QString("%1=(%2)").arg(it.key()).arg(values.join(" ")) << endl;
        }
    }

    os << endl;

    os << title_section.trimmed() << endl;
    os << endl;

    os << SK_section.trimmed() << endl;
    os << endl;

    os << geometry_section.trimmed() << endl;
    os << endl;

    os << final_section.trimmed() << endl;
    os << endl;
}

void DCDFTBKInput::clear()
{
    title_section = "";
    SK_section = "";
    geometry_section = "";
    final_section = "";
    keyword_section.clear();
}


}


void DFTBTestSuite::DCDFTBKEvaluator::EvaluateBandStructure(const CrystalGeometryData &ref_geom, const BandStructureData &ref_data, const CrystalEnergyData &calc_energy_data, const OptionalEvaluationInformation &opt_info, BandStructureData &calc_data, DFTBResultInfo &info) const
{
    throw std::runtime_error("Band structure calculation is not implemented in DC-DFTB-K");
}

void DFTBTestSuite::DCDFTBKEvaluator::EvaluateCrystalEnergy(const CrystalGeometryData &ref_geom, const OptionalEvaluationInformation &opt_info, CrystalEnergyData &calc, DFTBResultInfo &info) const
{
    throw std::runtime_error("Crystal energy calculation is not implemented in DC-DFTB-K");
    return ;
}


bool DFTBTestSuite::DCDFTBKEvaluator::supportDirectFrequencyCalculation() const
{
    return true;
}
