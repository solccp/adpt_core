#ifndef FORMATTER_H
#define FORMATTER_H

#include <QObject>
#include <QByteArray>
#include <QTextStream>

namespace DFTBTestSuite{


class DFTBResultInfo;
class BandStructureData;
class MolecularData;
class ReferenceReactionData;
class CrystalGeometryData;
class TestBandStructure;
class TestMolecule;
class TestReaction;
class MolecularDataComparator;
class Formatter : public QObject
{
    Q_OBJECT
public:
    explicit Formatter(QObject *parent = 0);
signals:
public slots:
    virtual void writeMolecule(const MolecularData& ref_data, const MolecularData& calc_data,
                               const MolecularDataComparator* comparator,
                               const TestMolecule& testMolecule,const DFTBResultInfo& info) = 0;
    virtual void writeBandStructure(const CrystalGeometryData& ref_geom, const TestBandStructure& testBS, const BandStructureData &ref_data, const BandStructureData &calc_data) = 0;
    virtual void writeReaction(const QMap<QString, QString> moleNames, const ReferenceReactionData& ref_data, double energy, const TestReaction& testReaction, bool GOptComp, DFTBResultInfo& info) = 0;
    virtual void writeHeader() = 0;
    virtual void writeStatistics(bool showErrorUnit) = 0;
protected:
    QByteArray m_array;
    QTextStream m_stream;
public:
    QString getContent();

};


}
#endif // FORMATTER_H
