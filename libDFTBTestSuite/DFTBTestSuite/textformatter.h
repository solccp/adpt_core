#ifndef TEXTFORMATTER_H
#define TEXTFORMATTER_H

#include "formatter.h"

namespace DFTBTestSuite{

class DFTBResultInfo;
class BandStructureData;
class Statistics;





class TextFormatter : public Formatter
{
    Q_OBJECT
public:
    TextFormatter(Statistics* stat, QObject *parent = 0);

signals:

public slots:


    // Formatter interface
public slots:
    void writeMolecule(const MolecularData& ref_data, const MolecularData& calc_data,
                       const MolecularDataComparator* comparator,
                       const TestMolecule& testMolecule, const DFTBResultInfo& info);
//    void writeMolecule(const MolecularData &ref_data, const MolecularData &calc_data, const DFTBResultInfo& info, bool TestGeometry);

    // Formatter interface
public slots:
    void writeBandStructure(const CrystalGeometryData& ref_geom, const TestBandStructure& testBS, const BandStructureData &ref_data, const BandStructureData &calc_data);

    // Formatter interface
public slots:
    void writeReaction(const QMap<QString, QString> moleNames, const ReferenceReactionData &ref_data, double energy,const TestReaction& testReaction, bool GOptComp, DFTBResultInfo &info);

    // Formatter interface
public slots:
    void writeHeader();

private:
    Statistics* m_stat;

    // Formatter interface
public slots:
    void writeStatistics(bool showErrorUnit);
};

}

#endif // TEXTFORMATTER_H
