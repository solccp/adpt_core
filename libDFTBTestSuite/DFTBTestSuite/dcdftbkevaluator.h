#ifndef DCDFTBKEVALUATOR_H
#define DCDFTBKEVALUATOR_H

#include "Evaluator.h"
typedef std::array<int, 3> iVector3;
typedef std::array<double, 3> dVector3;

namespace DFTBTestSuite
{


class DCDFTBKInput
{
public:
    QVariantMap keyword_section;
    QString title_section;
    QString geometry_section;
    QString SK_section;
    QString final_section;
    void parseDCDFTBKInput(const QString& filename);
    void writeInput(QTextStream& os) const;
    void clear();
private:

    void parseDCDFTBKKeywordInput(QVariantMap &keywords, QTextStream& os);
    QString parseDCDFTBKStringSection(QTextStream& os);
    QString parseDCDFTBKTitleInput(QTextStream& os);
    QString parseDCDFTBKSKInput(QTextStream& os);
    QString parseDCDFTBKGeometryInput(QTextStream& os);
    QString parseDCDFTBKFinalInput(QTextStream& os);

};


class DCDFTBKEvaluator : public Evaluator
{
public:
    DCDFTBKEvaluator(QTextStream & output_stream, const Evaluation& eval_opts, const QString& codePath);
    ~DCDFTBKEvaluator();

    // Evaluator interface
public:
    void EvaluateAtomicEnergy(AtomicData &calc, const QString &template_input, const OptionalEvaluationInformation &opt_info, DFTBResultInfo &info) const override;
    void EvaluateGeometry(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc,  DFTBResultInfo &info) const override;
    void EvaluateFrequency(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const override;
    void EvaluateMolecularEnergy(const MolecularData &ref, const OptionalEvaluationInformation &opt_info, MolecularData &calc, DFTBResultInfo &info) const override;

private:
    QString prepare_SK_section(bool usingSpin,
                            const OptionalEvaluationInformation* opt_info,
                            const ADPT::SKFileInfo* skfileinfo,
                            bool usingORSCC, QDir tempFolder, bool usingDFTB3,
                            const QStringList& elemList_) const;

    QStringList prepare_SKFiles(const QString &scratch, const QString &elem1, const QString &elem2,
                                                 const ADPT::SKFileInfo* skfileinfo) const;

    QString getInputGeometry(int natom, int charge, int spin, const QString& xyzstring) const;
    QString getGoodFilename(const QString& oriFilename) const;
    bool parseDFTBResult(const QString &filename, MolecularData &calc, DFTBResultInfo &info) const;
    void writeInputDriverOpt(QTextStream &os, const QString& optMethod) const;
    void writeUnpairedElectrons(QTextStream &os, double unpaired_e) const;
    void writeInputForces(QTextStream &os) const;
    void writeInputFreq(QTextStream &os) const;


    // Evaluator interface
public:
    void EvaluateBandStructure(const CrystalGeometryData &ref_geom, const BandStructureData &ref_data, const CrystalEnergyData &calc_energy_data, const OptionalEvaluationInformation &opt_info, BandStructureData &calc_data, DFTBResultInfo &info) const override;
    void EvaluateCrystalEnergy(const CrystalGeometryData &ref_geom, const OptionalEvaluationInformation &opt_info, CrystalEnergyData &calc, DFTBResultInfo &info) const override;

    // Evaluator interface
public:
    bool supportDirectFrequencyCalculation() const override;
};
}


#endif // DCDFTBKEVALUATOR_H
