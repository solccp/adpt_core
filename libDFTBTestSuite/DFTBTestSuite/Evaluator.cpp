#include "Evaluator.h"

namespace DFTBTestSuite
{

Evaluator::Evaluator(QTextStream & output_stream, const Evaluation &eval_opts, const QString &codePath): m_output_stream(output_stream)
{
    m_evaluation = eval_opts;
    m_program_path = codePath;
}

Evaluator::~Evaluator()
{
}

QDir Evaluator::workDir() const
{
    return m_workDir;
}

void Evaluator::setWorkDir(const QDir &workDir)
{
    m_workDir = workDir;
}

}
