#include "dftbtester.h"

#include "typesettings.h"
#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/InputFile.h"
#include "evaluatorfactory.h"
#include "Evaluator.h"
#include "statisticanalyser.h"
#include "textformatter.h"
#include "EvaluatedData.h"

#include "constants.h"

#include <QMap>
#include <QTextStream>
#include <QDebug>
#include <QCoreApplication>
#include <QTemporaryDir>
#include <QRegularExpression>

#include "Testing/MolecularDataComparator.h"

namespace DFTBTestSuite{


using namespace std;


bool printReferenceOverview(QTextStream& out, const ReferenceData& ref_data, QMap<QString, QString>& mole_uuid2Name, QMap<QString, QString>& crystal_uuid2Name)
{
    mole_uuid2Name.clear();
    crystal_uuid2Name.clear();
    endl(out);
    endl(out);
    out << STARLINE << endl;
    out << "Reference Data" << endl;

    out << INDENT_SPACES << "Molecule: " << ref_data.molecularDataSet.size() << " loaded." << endl;

    {
        //Molecule section
        const int name_width = 25;
        const int formula_width = 10;
        const int charge_width = 6;
        const int spin_width = 4;
        const int uuid_width = 36;
        out << QString("%1 %2 %3 %4 %5")
               .arg("Name",name_width)
               .arg("Formula",formula_width)
               .arg("Charge",charge_width)
               .arg("Spin", spin_width)
               .arg("UUID",uuid_width-(uuid_width/2)) << endl;
        out << EQUALLINE << endl;
        for(int i=0; i<ref_data.molecularDataSet.size(); ++i)
        {
            out << QString("%1 %2 %3 %4 %5").arg(ref_data.molecularDataSet[i].name, name_width)
                   .arg(ref_data.molecularDataSet[i].geometryData.geometry.getFormula(),formula_width)
                   .arg(ref_data.molecularDataSet[i].charge,charge_width)
                   .arg(ref_data.molecularDataSet[i].spin, spin_width)
                   .arg(ref_data.molecularDataSet[i].uuid.toString(), uuid_width) << endl;
            if (!mole_uuid2Name.contains(ref_data.molecularDataSet[i].uuid.toString()))
            {
                mole_uuid2Name.insert(ref_data.molecularDataSet[i].uuid.toString(), ref_data.molecularDataSet[i].name);
            }
            else
            {
                qCritical() << ref_data.molecularDataSet[i].uuid.toString() << "is already loaded with the name" << ref_data.molecularDataSet[i].name;
                return false;
            }
        }
        out << EQUALLINE << endl;
        out << endl;
    }
    {
        const int type_width = 20;
        const int method_width = 10;
        const int basis_width = 10;
        const int value_width = 20;
        const int value_precision = 6;
        for(int i=0; i<ref_data.molecularDataSet.size(); ++i)
        {
            out << QString("%1   %2").arg(ref_data.molecularDataSet[i].name, type_width)
                   .arg(ref_data.molecularDataSet[i].uuid.toString(), 36)
                << endl;

            out << HYPHENLINE << endl;
            out << QString("%1:  %2  %3").arg("Geometry",type_width)
                   .arg(ref_data.molecularDataSet[i].geometryData.method, method_width)
                   .arg(ref_data.molecularDataSet[i].geometryData.basisSet, basis_width)
                << endl;


            if (ref_data.molecularDataSet[i].frequencyData.isValid)
            {
                out << QString("%1:  %2  %3").arg("Frequency", type_width)
                       .arg(ref_data.molecularDataSet[i].frequencyData.method, method_width)
                       .arg(ref_data.molecularDataSet[i].frequencyData.basisSet, basis_width)
                    << endl;
            }

            if (ref_data.molecularDataSet[i].totalEnergyData.isValid)
            {
                out << QString("%1:  %2  %3  %4 %5")
                       .arg("TotalEnergy",type_width)
                       .arg(ref_data.molecularDataSet[i].totalEnergyData.method, method_width)
                       .arg(ref_data.molecularDataSet[i].totalEnergyData.basisSet, basis_width)
                       .arg(ref_data.molecularDataSet[i].totalEnergyData.energy, value_width,'f', value_precision)
                       .arg(ref_data.molecularDataSet[i].totalEnergyData.energy_unit)
                    << endl;
            }

            if (ref_data.molecularDataSet[i].atomizationEnergyData.isValid)
            {
                out << QString("%1:  %2  %3  %4 %5")
                       .arg("AtomizationEnergy",type_width)
                       .arg(ref_data.molecularDataSet[i].atomizationEnergyData.method, method_width)
                       .arg(ref_data.molecularDataSet[i].atomizationEnergyData.basisSet, basis_width)
                       .arg(ref_data.molecularDataSet[i].atomizationEnergyData.energy, value_width,'f', value_precision)
                       .arg(ref_data.molecularDataSet[i].atomizationEnergyData.energy_unit)
                    << endl;
            }


            out << endl;
        }
        out << STARLINE << endl;
        out << endl;
    }

    out << endl;
    out << "Crystal: " << endl
        << INDENT_SPACES << "     Geometry Data: " << ref_data.crystalData.geometryData.size() << " loaded."
        << endl
        << INDENT_SPACES << "BandStructure Data: " << ref_data.crystalData.bandstructureData.size() << " loaded."
        << endl;
    {
        out << QString("%1").arg("Crystal Geometry Data",50) << endl;
        out << EQUALLINE << endl;

        const int name_width = -19;
        const int method_basis_width = -23;
        const int uuid_width = -36;
        const int upe_width = -10;
        const int mm_width = -10;
        out << QString("%1 %2 %3 %4 %5")
                .arg("Name",name_width)
                .arg("Method/Basis Set",method_basis_width)
                .arg("UPE", upe_width)
                .arg("MM", mm_width)
                .arg("UUID",uuid_width-(uuid_width/2)) << endl;

        out << HYPHENLINE << endl;
        for(int i=0; i<ref_data.crystalData.geometryData.size(); ++i)
        {
            QString methodbasis = ref_data.crystalData.geometryData[i].method;
            if (!ref_data.crystalData.geometryData[i].basis.isEmpty())
            {
                methodbasis = methodbasis + "/" + ref_data.crystalData.geometryData[i].basis;
            }

            out << QString("%1 %2 %3 %4 %5").arg(ref_data.crystalData.geometryData[i].name, name_width)
                   .arg(methodbasis, method_basis_width )
                   .arg(ref_data.crystalData.geometryData[i].upe)
                   .arg(std::sqrt(ref_data.crystalData.geometryData[i].upe*(ref_data.crystalData.geometryData[i].upe+2.0)))
                   .arg(ref_data.crystalData.geometryData[i].uuid.toString(), uuid_width) << endl;
            if (!crystal_uuid2Name.contains(ref_data.crystalData.geometryData[i].uuid.toString()))
            {
                crystal_uuid2Name.insert(ref_data.crystalData.geometryData[i].uuid.toString(), ref_data.crystalData.geometryData[i].name);
            }
            else
            {
                qCritical() << ref_data.crystalData.geometryData[i].uuid.toString() << "is already loaded with the name" << ref_data.crystalData.geometryData[i].name;
                return false;
            }
        }
        out << HYPHENLINE << endl;
        out << endl;
    }
    {
        out << QString("%1").arg("Crystal Bandstructure Data",50) << endl;
        out << EQUALLINE << endl;

        const int name_width = -25;
        const int method_basis_width = -28;
        const int refgeom_width = -25;

        out << QString("%1 %2 %3")
               .arg("Name", name_width)
               .arg("Method/Basis Set", method_basis_width)
               .arg("Reference Crystal", refgeom_width)
            << endl;

        out << HYPHENLINE << endl;
        for(int i=0; i<ref_data.crystalData.bandstructureData.size(); ++i)
        {
            QString methodbasis = ref_data.crystalData.bandstructureData[i].method;
            if (!ref_data.crystalData.bandstructureData[i].basis.isEmpty())
            {
                methodbasis = methodbasis + "/" + ref_data.crystalData.bandstructureData[i].basis;
            }

            QString refgeom_name;
            if (crystal_uuid2Name.contains(ref_data.crystalData.bandstructureData[i].geom_uuid.toString()))
            {
                refgeom_name = crystal_uuid2Name[ref_data.crystalData.bandstructureData[i].geom_uuid.toString()];
            }
            else
            {
                qCritical() << "The crystal structure with UUID =" << ref_data.crystalData.bandstructureData[i].geom_uuid.toString() << "is not defined.";
                return false;
            }
            out << QString("%1 %2 %3").arg(ref_data.crystalData.bandstructureData[i].name, name_width)
                   .arg(methodbasis,method_basis_width)
                   .arg(refgeom_name, refgeom_width)
                << endl;

        }
        out << HYPHENLINE << endl;
        out << endl;
    }

    out << INDENT_SPACES << "Reactions: " << ref_data.reactionDataSet.size() << " loaded." << endl;
    {
        const int type_width = 30;
        const int method_width = 10;
        const int basis_width = 10;
        const int factor_width = 5;
        const int molename_width = 20;

        for (int i=0; i<ref_data.reactionDataSet.size(); ++i)
        {
            out << QString("%1:  %2  %3   %4 %5").arg(ref_data.reactionDataSet[i].name, type_width)
                   .arg(ref_data.reactionDataSet[i].method, method_width )
                   .arg(ref_data.reactionDataSet[i].basis, basis_width)
                   .arg(ref_data.reactionDataSet[i].energy, 10, 'f', 3)
                   .arg(ref_data.reactionDataSet[i].energy_unit)
                << endl;
            out << HYPHENLINE << endl;
            int maxentry = max(ref_data.reactionDataSet[i].reactants.size(), ref_data.reactionDataSet[i].products.size());
            for(int j=0; j<maxentry;++j)
            {
                QString reactant, product, arrow;
                QString factor_reactant;
                QString factor_product;
                QString prefix_r, prefix_p;


                if (j<ref_data.reactionDataSet[i].reactants.size())
                {
                    if (mole_uuid2Name.contains(ref_data.reactionDataSet[i].reactants[j].geom_uuid.toString()))
                    {
                        reactant = mole_uuid2Name[ref_data.reactionDataSet[i].reactants[j].geom_uuid.toString()];
                        factor_reactant = QString::number(ref_data.reactionDataSet[i].reactants[j].coefficient);
                    }
                    else if (crystal_uuid2Name.contains(ref_data.reactionDataSet[i].reactants[j].geom_uuid.toString()))
                    {
                        reactant = crystal_uuid2Name[ref_data.reactionDataSet[i].reactants[j].geom_uuid.toString()];
                        factor_reactant = QString::number(ref_data.reactionDataSet[i].reactants[j].coefficient);
                    }
                    else
                    {
                        qCritical() << "The molecule or crystal with UUID=" << ref_data.reactionDataSet[i].reactants[j].geom_uuid.toString() << "is not defined.";
                        return false;
                    }
                    if (j>0)
                        prefix_r = "+";
                }
                arrow = "=>";
                if (j<ref_data.reactionDataSet[i].products.size())
                {
                    if (mole_uuid2Name.contains(ref_data.reactionDataSet[i].products[j].geom_uuid.toString()))
                    {
                        product = mole_uuid2Name[ref_data.reactionDataSet[i].products[j].geom_uuid.toString()];
                        factor_product = QString::number(ref_data.reactionDataSet[i].products[j].coefficient);
                    }
                    else if (crystal_uuid2Name.contains(ref_data.reactionDataSet[i].products[j].geom_uuid.toString()))
                    {
                        product = crystal_uuid2Name[ref_data.reactionDataSet[i].products[j].geom_uuid.toString()];
                        factor_product = QString::number(ref_data.reactionDataSet[i].products[j].coefficient);
                    }
                    else
                    {
                        qCritical() << "The molecule or crystal with UUID=" << ref_data.reactionDataSet[i].products[j].geom_uuid.toString() << "is not defined.";
                        return false;
                    }
                    if (j>0)
                        prefix_p = "+";
                }
                out << QString("%6%1 %2 %3 %7%4 %5").arg(factor_reactant, factor_width)
                       .arg(reactant, -molename_width)
                       .arg(arrow, 4)
                       .arg(factor_product, factor_width)
                       .arg(product, -molename_width)
                       .arg(prefix_r, 1)
                       .arg(prefix_p, 1)
                       << endl;
            }
            out << HYPHENLINE << endl;

        }
    }

    return true;
}

bool checkMoleculeInReaction(QTextStream& stdout_stream, QList<QString>& temp_mole_uuid, const QList<ReactionItem>& reactionItems, const InputData& input_data, const ReferenceData& ref_data, bool GOptComp)
{
    for (int j=0; j<reactionItems.size(); ++j)
    {
        QString target = reactionItems[j].geom_uuid.toString();

        int evalIndex = indexUUID(input_data.evaluation.evaluationMolecule.moleculeTargets, target);

        //if not found in target, then put it in the target;
        if (evalIndex < 0)
        {
            int refIndex = indexUUID(ref_data.molecularDataSet, target);

            if (refIndex >= 0)
            {
                stdout_stream << STARLINE << endl
                              << "The molecule " << ref_data.molecularDataSet[refIndex].name << " with UUID: " << target << endl
                              << "is not found in the evaluation target but it is found in the reference file." << endl
                              << "This molecule will be evaluate for the reaction." << endl
                              << STARLINE << endl;
                temp_mole_uuid.append(target);
            }
            else
            {
                return false;
            }
        }
        //if not required optimized, then ask for geometry opt;
        else if (GOptComp && input_data.evaluation.evaluationMolecule.moleculeTargets[evalIndex].evalutationLevel < MoleculeTarget::Geometry)
        {
            int refIndex = indexUUID(ref_data.molecularDataSet, target);
            stdout_stream << STARLINE << endl
                          << "The molecule " << ref_data.molecularDataSet[refIndex].name << " with UUID: " << target << endl
                          << "is found in the evaluation target but not requested for geometry optimization." << endl
                          << "The geometry optimization will be performed." << endl
                          << STARLINE << endl;
            temp_mole_uuid.append(target);
        }
        else if (input_data.evaluation.evaluationMolecule.moleculeTargets[evalIndex].evalutationLevel < MoleculeTarget::Energy)
        {
            temp_mole_uuid.append(target);
        }
    }
    return true;
}

bool checkCrystalInReaction(QTextStream& stdout_stream, QList<QString>& temp_mole_uuid, const QList<ReactionItem>& reactionItems, const InputData& input_data, const ReferenceData& ref_data)
{
    for (int j=0; j<reactionItems.size(); ++j)
    {
        QString target = reactionItems[j].geom_uuid.toString();

        int evalIndex = indexUUID(input_data.evaluation.evaluationCrystal.crystalstructureTargets, target);

        //if not found in target, then put it in the target;
        if (evalIndex < 0)
        {
            int refIndex = indexUUID(ref_data.crystalData.geometryData, target);

            if (refIndex >= 0)
            {
                stdout_stream << STARLINE << endl
                              << "The molecule " << ref_data.crystalData.geometryData[refIndex].name << " with UUID: " << target << endl
                              << "is not found in the evaluation target but it is found in the reference file." << endl
                              << "This molecule will be evaluate for the reaction." << endl
                              << STARLINE << endl;
                temp_mole_uuid.append(target);
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}



DFTBTester::DFTBTester(const InputData &inputData, const ReferenceData* referenceData, QObject *parent) :
    QObject(parent)
{

    m_tempRoot = QDir::temp();

    m_inputData = inputData;
    if (!m_inputData.control.ScratchDir.isEmpty())
    {
        QDir scratch(m_inputData.control.ScratchDir);
        if (scratch.exists())
        {
            m_tempRoot = scratch;
        }
    }



    m_referenceData = referenceData;

}

inline QString centerString(const QString& str, int width = 80)
{
    int len = str.size();
    int rest = width - len;
    if (rest < 0)
    {
        return str;
    }
    int leftpad = rest / 2;
    int rightpad = rest - leftpad;
    return (QString("").fill(' ', leftpad) + str + QString("").fill(' ', rightpad));
}


void print_header_and_version(QTextStream& os)
{
    os << EQUALLINE << endl << endl;
    os << centerString("SKTest: Tool for evaluating DFTB Slater-Koster (SK) files") << endl;
    os << endl;
    os << centerString("Powered by Chien-Pin Chou") << endl;
    os << endl << centerString(QString(GIT_VERSION))
       << endl << endl << endl;
    os << EQUALLINE << endl << endl;
}

bool DFTBTester::evaluate(QTextStream &stdout_stream)
{
    print_header_and_version(stdout_stream);

    m_stop = false;

    QDir myworkingDir = QDir(m_tempPath);
    if(m_tempPath.isEmpty() && m_autoTempDir.isNull())
    {
        m_autoTempDir.reset(new QTemporaryDir(m_tempRoot.absoluteFilePath("DFTBTestSuite_XXXXXX")));
        m_autoTempDir->setAutoRemove(this->isAutoRemove);
        myworkingDir = QDir(m_autoTempDir->path());
    }

    m_inputData.evaluation.SKInfo->makeAbsolutePath(myworkingDir);

    QMap<QString, QString> mole_uuid2Name;
    QMap<QString, QString> crystal_uuid2Name;
    printReferenceOverview(stdout_stream, (*m_referenceData), mole_uuid2Name, crystal_uuid2Name);


//Examination
    //Check if the energy
    QStringList elements;
    for(int i=0; i<m_inputData.evaluation.evaluationMolecule.moleculeTargets.size(); ++i)
    {
        QString target = m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].uuid.toString();
        if (m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel >= MoleculeTarget::Energy)
        {
            int index = (*m_referenceData).molecularDataIndexMapping[target];
            QStringList elems = (*m_referenceData).molecularDataSet[index].geometryData.geometry.getElements();
            elems.removeDuplicates();
            for(int j=0; j<elems.size();++j)
            {
                // if the atom energy or a proper input for calculating atom energy is provided.
                if (m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_energy.contains(elems[j]))
                {
                    elements.append(elems[j]);
                }
                else if (m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_dftbinputs.contains(elems[j]))
                {
                    elements.append(elems[j]);
                }
                else
                {
                    //here we will calculate the atom energy in an inaccurated way - no espin.
                    stdout_stream << "WARNING!! The AtomicEnergy does not contain the energy or input for element " <<  elems[j] << endl;
                    stdout_stream << "The AtomizationEnergy of molecule " <<  (*m_referenceData).molecularDataSet[index].name << " will be computed in an inaccurated way." << endl;

//                    stderr_stream << "The AtomizationEnergy of molecule " <<  (*m_referenceData).molecularDataSet[index].name << " will not be computed." << endl;
//                    if (m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel == MoleculeTarget::Energy)
//                    {
//                        m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel = MoleculeTarget::None;
//                    }
//                    break;


                    QStringList skfiles = m_inputData.evaluation.SKInfo->getSKFileName(elems[j], elems[j]);
                    QString mainSK = skfiles.first();
                    QFile mainSKfile(mainSK);
                    if (mainSKfile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        double energy = 0.0;

                        QTextStream stream(&mainSKfile);
                        QString dummy;
                        bool extended = false;
                        while(!stream.atEnd())
                        {
                            dummy = stream.readLine();
                            if (dummy.startsWith('#'))
                            {
                                continue;
                            }
                            else if (dummy.startsWith('@'))
                            {
                                extended = true;
                                dummy = stream.readLine();
                                break;
                            }
                            else
                            {
                                break;
                            }
                        }
                        QStringList header = stream.readLine().split(QRegularExpression("[ ,]"),QString::SkipEmptyParts);
                        if (extended)
                        {
                            if (header.size() != 13)
                                break;
                            energy = header[0].toDouble()*header[9].toDouble() +
                                    header[1].toDouble()*header[10].toDouble() +
                                    header[2].toDouble()*header[11].toDouble() +
                                    header[3].toDouble()*header[12].toDouble() +
                                    header[4].toDouble();
                        }
                        else
                        {
                            if (header.size() != 10)
                                break;
                            energy = header[0].toDouble()*header[7].toDouble() +
                                    header[1].toDouble()*header[8].toDouble() +
                                    header[2].toDouble()*header[9].toDouble() +
                                    header[3].toDouble();
                        }

                        m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_energy.insert(elems[j], energy);
                        elements.append(elems[j]);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    stdout_stream << endl;
    elements.removeDuplicates();
    stdout_stream << "The atomic energy of the following elements will be computed: " << elements.join(", ") << endl;



    //examine Reactions;
    QList<ReactionTarget> final_reactionTarget;
    for(int i=0; i< m_inputData.evaluation.evaluationReaction.reactionTargets.size();++i)
    {

        QString uuid = m_inputData.evaluation.evaluationReaction.reactionTargets[i].uuid.toString();
        int index = indexUUID((*m_referenceData).reactionDataSet, uuid);

        if (index < 0)
        {
            stdout_stream << "Reaction with uuid is not defined in the reference file, skipping.";
            continue;
        }

        const ReferenceReactionData& reaction = (*m_referenceData).reactionDataSet[index];
        bool GOptComp = m_inputData.evaluation.evaluationReaction.reactionTargets[i].GeomOptComponents;
        QList<QString> temp_mole_uuid;
        QList<QString> temp_crystal_uuid;

        bool succ_mole = checkMoleculeInReaction(stdout_stream, temp_mole_uuid, reaction.reactants, m_inputData, (*m_referenceData), GOptComp);
        if (succ_mole)
        {
            succ_mole = checkMoleculeInReaction(stdout_stream, temp_mole_uuid, reaction.products, m_inputData, (*m_referenceData), GOptComp);
        }

        bool succ_crystal = checkCrystalInReaction(stdout_stream, temp_crystal_uuid, reaction.reactants, m_inputData, (*m_referenceData));
        if (succ_crystal)
        {
            succ_crystal = checkCrystalInReaction(stdout_stream, temp_crystal_uuid, reaction.products, m_inputData, (*m_referenceData));
        }

        if (!succ_mole && !succ_crystal)
        {
            temp_mole_uuid.clear();
            temp_crystal_uuid.clear();

            stdout_stream << STARLINE << endl
                          << "The system in the reaction with the UUID: " << reaction.uuid.toString() << endl
                          << "is not found either in the evaluation target or the reference file." << endl
                          << "This reaction will not be evaluated." << endl
                          << STARLINE << endl;

        }
        else
        {
            final_reactionTarget.append(m_inputData.evaluation.evaluationReaction.reactionTargets[i]);
        }


        for(int j=0; j<temp_mole_uuid.size();++j)
        {
            MoleculeTarget target;
            if (GOptComp)
            {
                target.evalutationLevel = MoleculeTarget::Geometry;
            }
            else
            {
                target.evalutationLevel = MoleculeTarget::Energy;
            }
            target.uuid = temp_mole_uuid[j];

            int moleIndex = indexUUID(m_inputData.evaluation.evaluationMolecule.moleculeTargets, target.uuid.toString());
            if (moleIndex < 0)
            {
                m_inputData.evaluation.evaluationMolecule.moleculeTargets.append(target);
            }
            else
            {
                if (GOptComp && m_inputData.evaluation.evaluationMolecule.moleculeTargets[moleIndex].evalutationLevel < MoleculeTarget::Geometry)
                {
                    m_inputData.evaluation.evaluationMolecule.moleculeTargets[moleIndex].evalutationLevel = MoleculeTarget::Geometry;
                }
                else if (!GOptComp && m_inputData.evaluation.evaluationMolecule.moleculeTargets[moleIndex].evalutationLevel < MoleculeTarget::Energy)
                {
                    m_inputData.evaluation.evaluationMolecule.moleculeTargets[moleIndex].evalutationLevel = MoleculeTarget::Energy;
                }
            }
        }

        for(int j=0; j<temp_crystal_uuid.size();++j)
        {
            CrystalStructureTarget target;
            target.uuid = temp_crystal_uuid[j];
            int moleIndex = indexUUID(m_inputData.evaluation.evaluationCrystal.crystalstructureTargets, target.uuid.toString());
            if (moleIndex < 0)
            {
                m_inputData.evaluation.evaluationCrystal.crystalstructureTargets.append(target);
            }
        }

    }

    m_inputData.evaluation.evaluationReaction.reactionTargets = final_reactionTarget;

    //Finish example reaction section;


    //Examination for Bandstructure;
    //We should start with the testing targets.
    QList<TestBandStructure> final_testBandstructures;

    for(auto& item : m_inputData.testingOption.testBandStructure)
    {
        int ref_index = indexUUID((*m_referenceData).crystalData.bandstructureData, item.uuid.toString());
        if (ref_index == -1)
        {
            stdout_stream << STARLINE << endl
                          << "Warning: The target UUID " << item.uuid.toString() << endl
                          << " is not found." << endl
                          << "The band structure calculation will not be performed." << endl
                          << STARLINE << endl;
            continue;
        }

        //check if the testing region is empty
        bool hasTestRegion = false;
        if (item.testRegion.isEmpty())
        {
            item.testRegion = (*m_referenceData).crystalData.bandstructureData[ref_index].defaultTestRegion;
            if (!item.testRegion.empty())
            {
                hasTestRegion = true;
            }
        }
        else
        {
            hasTestRegion = true;
        }

        if (!hasTestRegion)
        {

            stdout_stream << STARLINE << endl
                      << "Warning: The test region of the band structure " << (*m_referenceData).crystalData.bandstructureData[ref_index].name << endl
                      << " with UUID: "  << (*m_referenceData).crystalData.bandstructureData[ref_index].uuid.toString() << " is not found." << endl
                      << "The band structure calculation will not be performed." << endl
                      << STARLINE << endl;
            continue;
        }



        auto geom_uuid = (*m_referenceData).crystalData.bandstructureData[ref_index].geom_uuid.toString();
        int geom_index = indexUUID((*m_referenceData).crystalData.geometryData, geom_uuid);
        if (geom_index == -1)
        {
            stdout_stream << STARLINE << endl
                          << "Warning: The target UUID " << geom_uuid << endl
                          << " is not found." << endl
                          << "The band structure calculation will not be performed." << endl
                          << STARLINE << endl;
            continue;
        }


        final_testBandstructures.append(item);

    }
    m_inputData.testingOption.testBandStructure = final_testBandstructures;


// Evaluation Part;
    m_evaluator = EvaluatorFactory::getInstance(m_inputData.control.DFTBCodeType, stdout_stream, m_inputData.control.DFTBPath, m_inputData.evaluation);

    if (m_evaluator == nullptr)
    {
        stdout_stream << "Unable to initialize DFTB implementation type: " << m_inputData.control.DFTBCodeType << ",  located at " << m_inputData.control.DFTBPath << endl;
        return false;
    }

    m_evaluator->setWorkDir(myworkingDir);

    stdout_stream << "Working directory: " << m_evaluator->workDir().absolutePath() << endl;

//    QMap<QString, DFTBResultInfo> evaluated_infos;


//    Formatter *formatter = FormatterFactory::getInstance(m_inputData.control.ReportFormatter);
    m_statistic_analyser = new Statistics(&m_inputData.testingOption.errorUnit);
    m_statistic_analyser->setMaxErrorRatio(m_inputData.testingOption.maxErrorRatio);
    m_formatter = new TextFormatter(m_statistic_analyser, this);


    OptionalEvaluationInformation opt_info;
    initializeOptInfo(opt_info);


    if (m_calc_data)
    {
        delete m_calc_data;
    }
    m_calc_data = new EvaluatedData;
    EvaluatedData& calc_data = *m_calc_data;
    calc_data.clear(); 


    // Evaluate energy for atoms
    for(int i=0; i<elements.size(); ++i)
    {
        AtomicData calc_atomicData;
        calc_atomicData.method = "DFTB";
        calc_atomicData.symbol = elements[i];
        calc_atomicData.energy_unit = "H";

        if (m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_energy.contains(elements[i]))
        {
            calc_atomicData.energy = m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_energy[elements[i]];
            stdout_stream << "Atomic energy of " << elements[i] << " is read from the input file." << endl;
        }
        else
        {
            DFTBResultInfo info;
            stdout_stream << "Evaluating the atomic energy of " << elements[i] << "...";

            if (m_stop)
            {
                return false;
            }
            emit progressUpdate(QString("Calculating AtomizationEnergy of %1").arg(elements[i]));

            m_evaluator->EvaluateAtomicEnergy(calc_atomicData, m_inputData.evaluation.evaluationMolecule.atomicEnergy.atomic_dftbinputs[elements[i]], opt_info, info);
            if (info.failed)
            {
                stdout_stream << "failed" << endl;
                stdout_stream << STARLINE << endl
                              << "Atomic energy calculation failed for element " << elements[i] << ":" << endl
                              << info.detail << endl
                              << STARLINE << endl;
            }
            else
            {
                stdout_stream << "done" << endl;
            }
        }
        calc_data.atomicDataMapping.insert(calc_atomicData.symbol, calc_atomicData);
    }

    stdout_stream << endl;
    stdout_stream << EQUALLINE << endl;
    if (calc_data.atomicDataMapping.size() > 0)
    {
        QMapIterator<QString, AtomicData> it(calc_data.atomicDataMapping);
        stdout_stream << INDENT_SPACES << "Atomic energy:" << endl;
        while(it.hasNext())
        {
            it.next();
            stdout_stream << INDENT_SPACES << INDENT_SPACES << QString("%1 : %2").arg(it.value().symbol, 3).arg(it.value().energy,14,'f',8)<< " " << it.value().energy_unit << endl;
        }
    }



    stdout_stream << endl;

    // Evaluate molecules;
    for(int i=0; i<m_inputData.evaluation.evaluationMolecule.moleculeTargets.size(); ++i)
    {
        MolecularData calc_molecularData;

        QString target = m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].uuid.toString();
        int index = (*m_referenceData).molecularDataIndexMapping[target];
        const MolecularData& ref_molecularData = (*m_referenceData).molecularDataSet[index];

        calc_molecularData.uuid = ref_molecularData.uuid;
        calc_molecularData.name = ref_molecularData.name;
        calc_molecularData.charge = ref_molecularData.charge;


        calc_molecularData.totalEnergyData.method = "DFTB";
        calc_molecularData.geometryData.method = "DFTB";
        calc_molecularData.frequencyData.method = "DFTB";
        calc_molecularData.atomizationEnergyData.method = "DFTB";


        DFTBResultInfo info;

        stdout_stream << HYPHENLINE << endl;
        stdout_stream << "Computing properties of molecule " << calc_molecularData.name << "..." << endl;

        stdout_stream << INDENT_SPACES << "EvaluationLevel: " << MoleculeTarget::modeToString(m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel) << endl;

        stdout_stream << endl;

        if (m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel == MoleculeTarget::Frequency && ref_molecularData.frequencyData.isValid)
        {

            if (m_evaluator->supportDirectFrequencyCalculation())
            {
                stdout_stream << INDENT_SPACES << "Frequency calculation for " << calc_molecularData.name << "...";
                if (m_stop)
                {
                    return false;
                }
                emit progressUpdate(QString("Calculating Frequency of %1").arg(ref_molecularData.name));
                m_evaluator->EvaluateFrequency(ref_molecularData,  opt_info, calc_molecularData, info);

                if (info.failed)
                {
                    stdout_stream << "failed" << endl;
                    stdout_stream << STARLINE << endl
                                  << "Frequency calculation for " << calc_molecularData.name << " failed" << endl
                                  << info.detail << endl
                                  << STARLINE << endl;
                        m_statistic_analyser->addUnconvergedTarget(calc_molecularData.name + " (freq)");
                }
                else
                {
                    stdout_stream << "done" << endl;
                    calc_data.molecularDataMapping.insert(calc_molecularData.uuid.toString(), calc_molecularData);
                }
            }
            else
            {
                stdout_stream << INDENT_SPACES << "Geometry optimization for " << calc_molecularData.name << "...";
                if (m_stop)
                {
                    return false;
                }
                emit progressUpdate(QString("Calculating Geometry of %1").arg(ref_molecularData.name));
                m_evaluator->EvaluateGeometry(ref_molecularData,  opt_info, calc_molecularData, info);

                if (info.failed)
                {
                    stdout_stream << "failed" << endl;
                    stdout_stream << STARLINE << endl
                                  << "Geometry optimization for " << calc_molecularData.name << " failed" << endl
                                  << info.detail << endl
                                  << STARLINE << endl;
                    stdout_stream << INDENT_SPACES << "Frequency calculation for " << calc_molecularData.name << " will not be performed." << endl;
                    calc_molecularData.geometryData.optimized = false;
                }
                else
                {
                    stdout_stream << "done" << endl;

                    stdout_stream << INDENT_SPACES << "Frequency calculation for " << calc_molecularData.name << "...";
                    if (m_stop)
                    {
                        return false;
                    }
                    emit progressUpdate(QString("Calculating Frequency of %1").arg(ref_molecularData.name));
                    m_evaluator->EvaluateFrequency(ref_molecularData,  opt_info, calc_molecularData, info);

                    if (info.failed)
                    {
                        stdout_stream << "failed" << endl;
                        stdout_stream << STARLINE << endl
                                      << "Frequency calculation for " << calc_molecularData.name << " failed" << endl
                                      << info.detail << endl
                                      << STARLINE << endl;
                        m_statistic_analyser->addUnconvergedTarget(calc_molecularData.name + " (freq)");
                    }
                    else
                    {
                        stdout_stream << "done" << endl;
                        calc_data.molecularDataMapping.insert(calc_molecularData.uuid.toString(), calc_molecularData);
                    }
                }
            }



        }
        else if (m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel == MoleculeTarget::Geometry)
        {
            stdout_stream << INDENT_SPACES << "Geometry optimization for " << calc_molecularData.name << "...";
            if (m_stop)
            {
                return false;
            }
            emit progressUpdate(QString("Calculating Geometry of %1").arg(ref_molecularData.name));
            m_evaluator->EvaluateGeometry(ref_molecularData, opt_info, calc_molecularData, info);

            if (info.failed)
            {
                stdout_stream << "failed" << endl;
                stdout_stream << STARLINE << endl
                              << "Geometry optimization for " << calc_molecularData.name << " failed" << endl
                              << info.detail << endl
                              << STARLINE << endl;
                m_statistic_analyser->addUnconvergedTarget(calc_molecularData.name + " (geom.opt)");
            }
            else
            {
                stdout_stream << "done" << endl;
                calc_data.molecularDataMapping.insert(calc_molecularData.uuid.toString(), calc_molecularData);
            }
        }
        else if (m_inputData.evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel == MoleculeTarget::Energy )
        {
            //&& ref_molecularData.atomizationEnergyData.isValid (need?)

            stdout_stream << INDENT_SPACES << "Energy calculation for " << calc_molecularData.name << "...";
            if (m_stop)
            {
                return false;
            }
            m_evaluator->EvaluateMolecularEnergy(ref_molecularData, opt_info, calc_molecularData, info);

            if (info.failed)
            {
                stdout_stream << "failed" << endl;
                stdout_stream << STARLINE << endl
                              << "Energy optimization for " << calc_molecularData.name << " failed" << endl
                              << info.detail << endl
                              << STARLINE << endl;
            }
            else
            {
                stdout_stream << "done" << endl;
                calc_data.molecularDataMapping.insert(calc_molecularData.uuid.toString(), calc_molecularData);
                calc_molecularData.geometryData = ref_molecularData.geometryData;
            }
        }
        else
        {
            stdout_stream << INDENT_SPACES << "No evaluation needed for " << calc_molecularData.name << ", skipped." << endl;
        }

        stdout_stream << HYPHENLINE << endl;

        if (!info.failed)
        {
            const QList<QString>& elems = calc_molecularData.geometryData.geometry.getElements();
            bool calc = true;
            for(int i=0; i<elems.size();++i)
            {
                if (!calc_data.atomicDataMapping.contains(elems.at(i)))
                {

                    calc  = false;
                    break;
                }
            }

            if (calc)
            {
                CalcAtomizationEnergy(calc_molecularData, calc_data.atomicDataMapping);
            }
        }

/// TODO : fix the atomization energy
        int test_index = indexUUID(m_inputData.testingOption.testMolecules, target);
        MolecularDataComparator *mdc = new MolecularDataComparator(this) ;
        mdc->setPunishImaginaryFrequency(this->m_inputData.testingOption.punishImaginaryFrequency);

        TestMolecule testMolecule;
        testMolecule.uuid = target;

        if (test_index > -1)
        {
            testMolecule = m_inputData.testingOption.testMolecules.at(test_index);
            mdc->compare(ref_molecularData, calc_molecularData, testMolecule);

        }

        if (m_stop)
        {
            return false;
        }
        m_formatter->writeMolecule(ref_molecularData, calc_molecularData, mdc, testMolecule, info);
    }

    //Evaluate Crystal
    for(int i=0; i<m_inputData.evaluation.evaluationCrystal.crystalstructureTargets.size(); ++i)
    {
        DFTBResultInfo info;
        CrystalEnergyData calc_crystalData;

        QString target = m_inputData.evaluation.evaluationCrystal.crystalstructureTargets[i].uuid.toString();
        int index = indexUUID((*m_referenceData).crystalData.geometryData, target);
        if (index > -1)
        {
            const CrystalGeometryData& ref_crystalData = (*m_referenceData).crystalData.geometryData[index];

            stdout_stream << "Evaluating crystal energy for " << ref_crystalData.name << " ..." << endl;
            stdout_stream.flush();


            m_evaluator->EvaluateCrystalEnergy(ref_crystalData, opt_info, calc_crystalData, info);

            if (info.failed)
            {
                stdout_stream << "Evaluation failed." << endl;
                m_statistic_analyser->addUnconvergedTarget(ref_crystalData.name + " (cry. ene.)");
            }
            else
            {
                stdout_stream << "Evaluation success." << endl;
                calc_data.crystalDataMapping.insert(calc_crystalData.uuid.toString(), calc_crystalData);
            }
        }
        else
        {
            qCritical() << "Error: no crystal data found in ref.yaml (how)?";
        }

    }


    //Evaluate and Test reactions;
    for(int i=0; i< m_inputData.evaluation.evaluationReaction.reactionTargets.size();++i)
    {
        if (m_stop)
        {
            return false;
        }
        QString uuid = m_inputData.evaluation.evaluationReaction.reactionTargets[i].uuid.toString();
        int testIndex = indexUUID(m_inputData.testingOption.testReactions, uuid);
        if (testIndex == -1)
        {
            continue;
        }


        bool GOptComp = m_inputData.evaluation.evaluationReaction.reactionTargets[i].GeomOptComponents;

        int index = indexUUID((*m_referenceData).reactionDataSet, uuid);
        DFTBResultInfo info;
        const ReferenceReactionData& reaction = (*m_referenceData).reactionDataSet[index];

        double reaction_energy = 0.0;
        double reactants_energy = 0.0;
        double product_energy = 0.0;

        for (int j=0; j<reaction.products.size(); ++j)
        {          
            if (calc_data.molecularDataMapping.contains(reaction.products[j].geom_uuid.toString()))
            {
                const MolecularData& mole = calc_data.molecularDataMapping[reaction.products[j].geom_uuid.toString()];
                if (!mole.totalEnergyData.isValid)
                {                   
                    info.failed = true;
                    info.detail = "The total energy data of molecule: " + mole.name +  " is not available. Skipping.";
                    break;
                }
                else if ( GOptComp && !mole.geometryData.optimized )
                {
                    info.failed = true;
                    info.detail = "The optimized total energy data of molecule: " + mole.name +  " is not available. Skipping.";
                    break;
                }
                if (GOptComp)
                {
                    auto auEnergy = EnergyToAU(mole.totalEnergyData.energy_unit, mole.totalEnergyData.optEnergy);
                    product_energy += auEnergy*reaction.products[j].coefficient;
                }
                else
                {
                    auto auEnergy = EnergyToAU(mole.totalEnergyData.energy_unit, mole.totalEnergyData.energy);
                    product_energy += auEnergy*reaction.products[j].coefficient;
                }
            }
            else if (calc_data.crystalDataMapping.contains(reaction.products[j].geom_uuid.toString()))
            {
                auto crystalData = calc_data.crystalDataMapping[reaction.products[j].geom_uuid.toString()];
                auto auEnergy = EnergyToAU(crystalData.energy_unit, crystalData.final_energy);
                product_energy += auEnergy*reaction.products[j].coefficient;
            }
            else
            {

                info.failed = true;
                info.detail = "The total energy data of molecule is not available. Skipping.";
                break;
            }
        }
        if (!info.failed)
        {
            for (int j=0; j<reaction.reactants.size(); ++j)
            {
                if (calc_data.molecularDataMapping.contains(reaction.reactants[j].geom_uuid.toString()))
                {
                    const MolecularData& mole = calc_data.molecularDataMapping[reaction.reactants[j].geom_uuid.toString()];
                    if (!mole.totalEnergyData.isValid)
                    {
                        info.failed = true;
                        info.detail = "The total energy data of molecule: " + mole.name +  " is not available. Skipping.";
                        break;
                    }
                    else if ( GOptComp && !mole.geometryData.optimized)
                    {
                        info.failed = true;
                        info.detail = "The optimized total energy data of molecule: " + mole.name +  " is not available. Skipping.";
                        break;
                    }

                    if (GOptComp)
                    {
                        auto auEnergy = EnergyToAU(mole.totalEnergyData.energy_unit, mole.totalEnergyData.optEnergy);
                        reactants_energy += auEnergy*reaction.reactants[j].coefficient;

                    }
                    else
                    {
                        auto auEnergy = EnergyToAU(mole.totalEnergyData.energy_unit, mole.totalEnergyData.energy);
                        reactants_energy += auEnergy*reaction.reactants[j].coefficient;
                    }
                }
                else if (calc_data.crystalDataMapping.contains(reaction.reactants[j].geom_uuid.toString()))
                {
                    auto crystalData = calc_data.crystalDataMapping[reaction.reactants[j].geom_uuid.toString()];
                    auto auEnergy = EnergyToAU(crystalData.energy_unit, crystalData.final_energy);
                    reactants_energy += auEnergy*reaction.reactants[j].coefficient;
                }
                else
                {
                    info.failed = true;
                    info.detail = "The total energy data of molecule is not available. Skipping.";
                    break;
                }
            }           
        }
        if (info.failed)
        {
            stdout_stream << info.detail << endl;
            m_statistic_analyser->addUnconvergedTarget(reaction.name);
        }

        emit progressUpdate(QString("Calculating ReactionEnergy of %1").arg(reaction.name));
        reaction_energy = (product_energy - reactants_energy);

        if (testIndex > -1)
        {
            const TestReaction& testReaction = m_inputData.testingOption.testReactions.at(testIndex);
            auto allnames = mole_uuid2Name;
            allnames.unite(crystal_uuid2Name);
            m_formatter->writeReaction(allnames, reaction, reaction_energy, testReaction, GOptComp, info);
        }
    }

    stdout_stream << HYPHENLINE << endl;

    // Evaluate bandstructure;
    QMap<QString, int> crystalIndex;
    for(int i = 0; i<  (*m_referenceData).crystalData.geometryData.size();++i)
    {
        crystalIndex.insert((*m_referenceData).crystalData.geometryData[i].uuid.toString(), i);
    }
    QMap<QString, int> bandstructureIndex;
    for(int i = 0; i<  (*m_referenceData).crystalData.bandstructureData.size();++i)
    {
        bandstructureIndex.insert((*m_referenceData).crystalData.bandstructureData[i].uuid.toString(), i);
    }


    for(auto& item : m_inputData.testingOption.testBandStructure)
    {
        int bsindex = bandstructureIndex.value(item.uuid.toString());

        const BandStructureData& ref_bandstructureData = (*m_referenceData).crystalData.bandstructureData[bsindex];
        if (!crystalIndex.contains(ref_bandstructureData.geom_uuid.toString()))
        {
            qWarning() << ref_bandstructureData.geom_uuid.toString() << " is not found in ref data file";
            continue;
        }

        int index = crystalIndex.value(ref_bandstructureData.geom_uuid.toString());

        BandStructureData calc_bandstructureData;
        DFTBResultInfo info;


        calc_bandstructureData.uuid = ref_bandstructureData.uuid;
        calc_bandstructureData.name = ref_bandstructureData.name;
        calc_bandstructureData.method = "DFTB";

        if (!calc_data.crystalDataMapping.contains(ref_bandstructureData.geom_uuid.toString()))
        {

            CrystalEnergyData calc_crystalData;
            const CrystalGeometryData& ref_crystalData = (*m_referenceData).crystalData.geometryData[index];

            stdout_stream << "Evaluating crystal energy for " << ref_crystalData.name << " ..." << endl;
            stdout_stream.flush();

            m_evaluator->EvaluateCrystalEnergy(ref_crystalData, opt_info, calc_crystalData, info);

            if (info.failed)
            {
                stdout_stream << "failed" << endl;
            }
            else
            {
                stdout_stream << "done" << endl;
                calc_data.crystalDataMapping.insert(calc_crystalData.uuid.toString(), calc_crystalData);
            }
        }


        stdout_stream << "Bandstructure calculation for " << ref_bandstructureData.name << "...";
        stdout_stream.flush();

        if (m_stop)
        {
            return false;
        }
        emit progressUpdate(QString("Calculating Bandstructure of %1").arg(ref_bandstructureData.name));

        m_evaluator->EvaluateBandStructure((*m_referenceData).crystalData.geometryData.at(index),
                                         ref_bandstructureData,
                                         calc_data.crystalDataMapping[ref_bandstructureData.geom_uuid.toString()],
                                         opt_info,
                                         calc_bandstructureData, info);
        if (info.failed)
        {
            stdout_stream << "failed" << endl;
            m_statistic_analyser->addUnconvergedTarget("Bandstructure: " + ref_bandstructureData.name);
        }
        else
        {
            stdout_stream << "done" << endl;
            calc_data.bandstructureDataMapping.insert(calc_bandstructureData.uuid.toString(), calc_bandstructureData );
            m_formatter->writeBandStructure((*m_referenceData).crystalData.geometryData.at(index), item, ref_bandstructureData, calc_bandstructureData);
        }

    }

    m_formatter->writeStatistics(m_showErrorUnitResult);

    stdout_stream << endl << m_formatter->getContent() << endl;


    if (m_inputData.control.CopyIntermediateFiles)
    {
        if (m_stop)
        {
            return false;
        }
        copyIntermediateFiles(myworkingDir.absolutePath(), QDir::current().absoluteFilePath("IntermediateFiles"+m_intermidiate_suffix));
    }

    return true;
}

void copyPath(const QString& src, const QString& dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile newfile(dst + QDir::separator() + f);
        if (newfile.exists())
        {
            newfile.remove();
        }

        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

bool DFTBTester::copyIntermediateFiles(const QString& srcPath, const QString &destPath)
{
    copyPath(srcPath, destPath);
    return true;
}

const Statistics * DFTBTester::getStatistics() const
{
    return m_statistic_analyser;
}

const EvaluatedData *DFTBTester::getEvalutedData() const
{
    return m_calc_data;
}

DFTBTester::~DFTBTester()
{
    delete m_evaluator;
    delete m_statistic_analyser;
    delete m_formatter;
}

const InputData &DFTBTester::getInputData()
{
    return m_inputData;
}
QDir DFTBTester::tempRoot() const
{
    return m_tempRoot;
}

void DFTBTester::setTempRoot(const QDir &tempRoot)
{
    m_tempRoot = tempRoot;
}

QString DFTBTester::getTempPath() const
{
    return m_autoTempDir->path();
}

//remove the tempdir
void DFTBTester::setAutoRemove(bool value)
{
    this->isAutoRemove = value;
}
QDir DFTBTester::tempDir() const
{
    return m_tempPath;
}

void DFTBTester::setTempDir(const QDir &tempDir)
{
    if (!tempDir.exists())
        return;
    if (tempDir.isAbsolute())
    {
        m_tempPath = tempDir.absolutePath();
    }
    else
    {
        QDir ala(QDir(m_tempPath).absoluteFilePath(tempDir.path()));
        if (!ala.mkpath(ala.absolutePath()))
        {
            m_tempPath = ala.absolutePath();
        }
    }
}
QString DFTBTester::intermidiate_suffix() const
{
    return m_intermidiate_suffix;
}

void DFTBTester::setIntermidiate_suffix(const QString &intermidiate_suffix)
{
    m_intermidiate_suffix = intermidiate_suffix;
}
bool DFTBTester::showErrorUnitResult() const
{
    return m_showErrorUnitResult;
}

void DFTBTester::setShowErrorUnitResult(bool showErrorUnitResult)
{
    m_showErrorUnitResult = showErrorUnitResult;
}

void DFTBTester::stop()
{
    m_mutex.lock();
    m_stop = true;
    m_mutex.unlock();
}

void DFTBTester::initializeOptInfo(OptionalEvaluationInformation& opt_info)
{

    if (m_inputData.evaluation.dftb_options.contains("hubbard_derivatives"))
    {
        auto hd = m_inputData.evaluation.dftb_options["hubbard_derivatives"].toMap();
        QMapIterator<QString, QVariant> it(hd);
        while(it.hasNext())
        {
            it.next();
            if (it.value().canConvert<QVariantList>())
            {
                auto list = it.value().toList();
                dVector3 vec;
                vec[0] = list[0].toDouble();
                vec[1] = list[1].toDouble();
                vec[2] = list[2].toDouble();
                opt_info.hubbardDerivatives.insert(it.key(), vec);
                opt_info.orbital_resolved_scc = true;
            }
            else
            {
                dVector3 vec;
                vec[0] = it.value().toDouble();
                vec[1] = vec[0];
                vec[2] = vec[0];
                opt_info.hubbardDerivatives.insert(it.key(), vec);
            }
        }
    }

    if (m_inputData.evaluation.dftb_options.contains("spin_constants"))
    {
        if (m_inputData.evaluation.dftb_options.contains("shell_resolved_spin"))
        {
            opt_info.shell_resolved_spin = m_inputData.evaluation.dftb_options["shell_resolved_spin"].toBool();
        }

        auto hd = m_inputData.evaluation.dftb_options["spin_constants"].toMap();
        QMapIterator<QString, QVariant> it(hd);
        while(it.hasNext())
        {
            it.next();
            if (it.value().canConvert<QVariantList>())
            {
                opt_info.spinConstants[it.key()] = it.value().toList();
            }
        }
    }

    if (m_inputData.evaluation.dftb_options.contains("dispersion"))
    {
        opt_info.dispersion = m_inputData.evaluation.dftb_options["dispersion"].toMap();
    }

}






}
