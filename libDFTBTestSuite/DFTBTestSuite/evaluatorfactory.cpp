#include "evaluatorfactory.h"
#include "dftbplusevaluator.h"
#include "dcdftbkevaluator.h"

namespace DFTBTestSuite
{

EvaluatorFactory EvaluatorFactory::m_factory;


Evaluator *EvaluatorFactory::getInstance(const QString &keyword, QTextStream& output_stream, const QString &code_path, const Evaluation& eval_opts)
{
    if (keyword.toLower() == "dftb+")
    {
        return new DFTBPlusEvaluator(output_stream, eval_opts, code_path);
    }
    else if (keyword.toLower() == "dc-dftb-k")
    {
        return new DCDFTBKEvaluator(output_stream, eval_opts, code_path);
    }


    return nullptr;
}

EvaluatorFactory::EvaluatorFactory()
{
}


};
