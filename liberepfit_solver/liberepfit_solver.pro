#-------------------------------------------------
#
# Project created by QtCreator 2014-05-08T19:20:25
#
#-------------------------------------------------

QT       -= gui
TARGET = erepfit_solver
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    erepfit/splinetool.cpp \
    erepfit/Splcoeff.cpp \
    erepfit/Reaction.cpp \
    erepfit/Potential.cpp \
    erepfit/Molecule.cpp \
    erepfit/RepulsivePotential.cpp \
    erepfit/slaterkosterfile.cpp \
    erepfit/atomicproperties.cpp \
    erepfit/systemdistmatrix.cpp \
    erepfit/utils.cpp

HEADERS += \
    erepfit/splinetool.h \
    erepfit/Splcoeff.h \
    erepfit/Reaction.hpp \
    erepfit/Potential.hpp \
    erepfit/Molecule.hpp \
    erepfit/RepulsivePotential.h \
    erepfit/Singleton.h \
    erepfit/slaterkosterfile.h \
    erepfit/systemdistmatrix.h \
    erepfit/types.h \
    erepfit/atomicproperties.h \
    erepfit/utils.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

#INCLUDEPATH += $$PWD/../../external_libs/yaml-cpp/include/ $$PWD/../../external_libs/boost_for_yaml-cpp/
INCLUDEPATH += $$PWD/../external_libs/Eigen
#LIBS += $$PWD/../../external_libs/lib/libyaml-cpp.a
INCLUDEPATH += $$PWD/../libDFTBTestSuite
DEPENDPATH += $$PWD/../libDFTBTestSuite

MKL_PATH = $$(MKL_ROOT)
!isEmpty(MKL_PATH) {
INCLUDEPATH += $$(MKL_ROOT)/include
DEFINES += USE_MKL
}



#win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../libQtYAML/ -lQtYAML
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../libQtYAML/ -lQtYAMLd
#else:unix: LIBS += -L$$OUT_PWD/../../libQtYAML/ -lQtYAML

#INCLUDEPATH += $$PWD/../../libQtYAML
#DEPENDPATH += $$PWD/../../libQtYAML

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libQtYAML/libQtYAML.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libQtYAML/libQtYAMLd.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libQtYAML/QtYAML.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libQtYAML/QtYAMLd.lib
#else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../libQtYAML/libQtYAML.a

INCLUDEPATH += $$PWD/../external_libs/include/


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuite/release/ -lDFTBTestSuite
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuite/debug/ -lDFTBTestSuite
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuite/ -lDFTBTestSuite

INCLUDEPATH += $$PWD/../libDFTBTestSuite
DEPENDPATH += $$PWD/../libDFTBTestSuite

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/release/libDFTBTestSuite.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/debug/libDFTBTestSuite.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/release/DFTBTestSuite.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/debug/DFTBTestSuite.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuite/libDFTBTestSuite.a

DISTFILES +=



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a
