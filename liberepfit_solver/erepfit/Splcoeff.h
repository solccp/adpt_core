#ifndef SPLCOEFF_H
#define SPLCOEFF_H

#include <QSharedPointer>
#include <QString>
#include <QMap>
#include <QList>

#include "Molecule.hpp"
#include "Potential.hpp"
#include "Reaction.hpp"
#include "splinetool.h"

#include "Eigen/Core"

#include "erepfit2_inputdata.h"
#include "RepulsivePotential.h"


namespace Erepfit
{


struct Colind {
  public:
    int  k,c,p,e; 		  // knot, column, potential, error
};


class LJDispersionEntry
{
public:
    double distance;
    double energy;
};

class Dispersion
{
public:
    QMap<QString, LJDispersionEntry> m_ljdispersion;
};

struct EnegyEquationInfo
{
    double ediss;
    double weight;
    int index;
};
struct ForceEquationInfo
{
    double weight;
    int index;
};



class Splcoeff
{
    friend class Erepfit::Molecule;
public:
	class ExclPot {
	  public:
        QString potname;
        int     ndist;	   // number of distances which could not be calculated
        double  mindist;   // minimum of distance b/w the two atoms of the potential
        QString molname;   // name where mindist appears
	};
    class AddEq {		   // additional equation
	  public:
        QString potname;    // e.g. c_c_
//        QString comment;	// to know what this additional equation is for
        int    deriv;       // which derivative 0st,1st,2nd or 3rd (0,1,2,3)
        double dist,val;    // distance and value of deriv
        double weight;      // weight of condition
	};

    int                 nrows, ncols, nspleq, neeq, nfeq, naddeq, nreaeq, nsmootheq, nsplcoeff, nelem, natom;
    int                 natfit;   // number of elements where eatom is to be fitted
    int                 nzl;      // number of zero-lines in case of not enough equations
    int                 nfree;    // number of spline parameters that are free for fitting (one per interval)
    QString		        dftbversion;

    QVector<Molecule>   vmol;     // sorted by the way they appear in the inputfile
    QVector<EnegyEquationInfo>   energyInfos;
    QVector<ForceEquationInfo>   forceInfos;     // sorted by the way they appear in the inputfile


    QVector<Potential>  vpot;     // sorted by the way they appear in the inputfile
    QVector<AddEq>	    vadd;     // sorted by the way they appear in the inputfile
    QList<QString>      velem;    // sorted by the way they appear
    QVector<double> 	veatom;   // first entries sorted as in velem (energy of atoms)
    QVector<ExclPot>    vexclpot; // in calculation excluded potentials because no input

    QVector<Molecule>   vreamol;  // molecules of reactions
    QVector<Reaction>   vrea;     // vector of reactions
	
    double              restotS,reseS,resfS,resaddS,resreaS,ressmoothS ; // MSE of residuals (mean signed error)
    double              restotU,reseU,resfU,resaddU,resreaU,ressmoothU ; // MUE of residuals (mean unsigned error)
    double              restot2,rese2,resf2,resadd2,resrea2,ressmooth2;  // RMS of residuals (root mean square)
	
	int                 svderror;
	double  		    cond1, cond2; // condition number of sigma before and after truncation
    int 	    	    trunc;        // number of singular values which are kept, veqmat.size2()-trunc are number of truncated singular values



    Eigen::VectorXd vunknown;
    Eigen::VectorXd vref;
    typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXXd;
    MatrixXXd eqmat;
    Eigen::VectorXd vweight;
    Eigen::VectorXd vres;
    Eigen::VectorXd sigma;
    Eigen::VectorXd sigma_e_f;

    void setReferenceRepulsivePotentials(const QMap<QString, QString>& set);

    bool debug = false;
    bool use_cache = false;




    double getRMSResidual() const
    {
        return sqrt(restot2/static_cast<double>(nrows-nspleq));
    }


private:
    bool readinp(const Erepfit2::Input::Erepfit2_InputData& input);

	void   sort_inputdist();
	void   get_autogrids();
	void   sizeeqsys();
    int    include_splineeq(const int  ieq_);
    int    include_energyeq(const int  ieq_);
    int    include_forceeq (const int  ieq_);
    int    include_addeq   (const int  ieq_);
    int    include_reaeq   (const int  ieq_);

    void   weighteqsys();
    bool   svd_fulfill_spleq();
    void   get_residual();

    double myfac(const int  a, const int  b);
    void   add_energy_lhs(const int  ieq, const Molecule& mol,const double coeff);
    Colind findcolind(const QString &atomname1, const QString &atomname2, const double dist,
                      const QString &molname,   const int at1,          const int at2     );
    QString findpotname(const QString &at1, const QString &at2) const;
    void   excludepot(const QString &potname, const double dist, const QString &molname);
    void   ifnotfile(const QString &filename) const;
    void   addextef(const int ieq, const Molecule& mol, const int at1,
                    const int at2, const int ef, const double coeff);

    void   addextef_crystal(const int ieq, const QString& extname1, const QString& extname2, double dist, int half, const int ef, const double coeff);

    QTextStream& m_output_stream;
    std::shared_ptr<const ADPT::SKFileInfo> skfileinfo;
    bool UseTotalMinusRepulsive = false;


    QMap<QString, double> m_hubbardDerivs;
    QString m_lastError;


    QMap<QString, QSharedPointer<RepulsivePotential> > m_externalRepulsivePotentials;

public:
    Dispersion dispersion;
    bool useDampingFactor = false;
    double dampingFactor ;


public:
    Splcoeff(QTextStream& output_stream, const std::shared_ptr<const ADPT::SKFileInfo>& skfileinfo_);


    bool calculate(const Erepfit2::Input::Erepfit2_InputData &input);
    void writeout(QTextStream &out) const;
    std::tuple<double, double, double> getResidual();
    QMap<QPair<QString, QString>, Spline4> getPotentials() const;
    QMap<QString, double> getAtomEnergy() const;

    QMap<QString, QString> referenceRepulsivePotentials;
    QMap<QString, double> getFittedAtomEnergies();
    QString getLastError() const;

    bool getAutoRemoved() const;
    void setAutoRemoved(bool value);



private:
    bool autoRemoved = true;
    const int continous_order = 3;

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                      //
// expression to be minimized:                                                                          //
//                                                                                                      //
// | eqmat * vector(unknowns) - vector(references) | = 0     (after weighting eqmat and vref)           //
//                                                                                                      //
//                                                                                                      //
//            spline equations            ( here references are 0)                                      //
//            energy equations		  ( here references are reference dissoziation energies)        //
// eqmat  =   force  equations      	  ( here references are "-F_el" )                               //
//            ...                                                                                       //
//                                                                                                      //
//                                                                                                      //
// unknowns = spline coefficients, atomic energies                                                      //
//                                                                                                      //
// 		spline:  f_i(x) = a_i^0 + a_i^1 * (x-x_i) + a_i^2 * (x-x_i)^2 + ...                     //
//                                                                                                      //
// 		spline coefficients: a_1^0(potential 1), a_1^1(potential 1),... for all potentials      //
//                                                                                                      //
// 		atomic energies: e.g. E_Carbon, E_Hydrogen,...                                          //
//                                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////////////////////

}
#endif

