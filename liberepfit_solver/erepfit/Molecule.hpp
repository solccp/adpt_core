#ifndef MOLECULE_HPP
#define MOLECULE_HPP

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cstring>
#include <cmath>
#include <map>

#include <cstdio>
#include <QTextStream>
#include <QString>


//#include "util.h"

#include <QDebug>
#include <QList>
#include <QVector>

#include "types.h"
#include "systemdistmatrix.h"

#include <basic_types.h>
#include "erepfit2_inputdata.h"


#include <QMap>


namespace Erepfit
{



class Splcoeff;
//using namespace std;

class Molecule
{
public:


    MatrixX3d fel;      //force calculated
    MatrixX3d ftotal;   //total force
    MatrixX3d frep;     //total reference repulsive force

    double eel;         //total electronic energy calculated
    double ebindmel;    //the repulsive energy it should reproduce

    QString uuid;       //uuid
    QString name;       //name
    QString abbr;       //abbreviation

    int nelem;          //# of elements
    int natom;          //# of atoms


    QList<QString>    atomname;       //Atom name in Erepfit format, i.e. h_, cl, etc.
    QList<QString>      elemname;       //Element name in Erepfit format, i.e. h_, cl, etc.
    QList<QString>      elemnameext;    //Correct Element Symbol, i.e. C, H, Cl, etc.
    QList<int>        atomindex;      //Element index for each atom


    MatrixXXd dist;                     //Diatomic distances
    SystemDistMatrix dist_new;


    Erepfit2::Input::System m_system;

    bool init(Erepfit2::Input::System system,
              const QString& abbr_);

    bool loadTotalForce(const QString& forceinputfile);
    bool buildDistanceMatrix();
};
}
#endif /* MOLECULE_HPP */


