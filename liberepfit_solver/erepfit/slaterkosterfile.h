#ifndef SLATERKOSTERFILE_H
#define SLATERKOSTERFILE_H

#include <QList>
#include <QString>

class SlaterKosterFile
{
public:
    SlaterKosterFile();
    ~SlaterKosterFile();
private:
    QList<QString> m_files;
};

#endif // SLATERKOSTERFILE_H
