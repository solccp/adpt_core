#ifndef REACTION_HPP
#define REACTION_HPP

#include <QString>
#include <QList>
#include <QVector>
#include <QTextStream>
#include "Molecule.hpp"

namespace Erepfit
{

class Reaction 
{
public:
    QVector<double> 	vcoeff;
    QList<QString>	vabbr;
    QVector<int>		vmol;		// index of mol in order to find infos in Reamol
    double          reaE;
    double          reaweight;
    QString          reastr;

    void init(const QString& reastr_, const QVector<Molecule> &vreamol);
    void addReactant(double coeff, int index, const QString& name);
    void addProduct(double coeff, int index, const QString& name);
    void setEnergyAndWeight(double energy, double weight = 1.0);
    void buildReactionString();
};

}

#endif /* REACTION_HPP */

