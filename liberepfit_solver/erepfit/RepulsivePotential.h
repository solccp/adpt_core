#ifndef REPULSIVEPOTENTIAL_H
#define REPULSIVEPOTENTIAL_H

#include <Eigen/Core>
#include <QList>
#include <QPair>

class Spline
{
public:
    Spline(int order, double start, double end);
    Eigen::VectorXd coeffs() const;
    void setCoeffs(const Eigen::VectorXd &coeffs);
    double eval(double r, int derivs = 0) const;
    bool inRange(double r) const;
    double getX1() const;
    double getX2() const;
private:
    int m_order;
    double m_x1, m_x2;
    Eigen::VectorXd m_coeffs;

    double OrderDerivs(int order, int derivs) const;
    Eigen::VectorXd makeSplineValues(double delta_x, int derivs = 0) const;
};

class RepulsivePotential
{
public:
    RepulsivePotential();
    QPair<double, double> getRange();
    void addSpline(const Spline& spline);
    void clearSplines();
    double eval(double r, int derivs = 0);
    void loadfile(const QString &potname, const QString& filename);
private:
    int m_order = 3;
    double expA = 0.0;
    double expB = 0.0;
    double expC = 0.0;
    QList<Spline> m_splines;

};

#endif // REPULSIVEPOTENTIAL_H
