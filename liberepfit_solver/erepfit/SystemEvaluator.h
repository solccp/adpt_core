#ifndef SYSTEMEVALUATOR_H
#define SYSTEMEVALUATOR_H

#include "system.hpp"
#include "erepfit2_inputdata.h"
#include "Splcpeff2.h"


namespace Erepfit2
{

class SystemEvaluator
{
public:
    SystemEvaluator();
    SystemResults evaluate(const Splcoeff2* splcoeff, const Erepfit2Input::System& system, const QString& scratch_dir);
    ~SystemEvaluator();   
};

}

#endif // SYSTEMEVALUATOR_H
