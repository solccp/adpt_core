#ifndef TYPES_472640d2_ae85_425a_abfb_0ad9def0b92c
#define TYPES_472640d2_ae85_425a_abfb_0ad9def0b92c

#include <array>
#include "Eigen/Core"

typedef Eigen::Matrix<double, Eigen::Dynamic, 3> MatrixX3d;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXXd;

typedef std::array<int, 3> iVector3;
typedef std::array<double, 3> dVector3;
typedef std::array<dVector3, 3> dMatrix3x3;


#endif // TYPES

