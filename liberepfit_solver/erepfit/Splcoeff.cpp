#include "Splcoeff.h"
#include "Eigen/LU"
#include "utils.h"
#include <Eigen/SVD>
#include <QString>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include <QSharedPointer>

#ifdef USE_MKL
#include "mkl.h"
#endif

namespace Erepfit
{

bool sortAddition(const Splcoeff::AddEq& i, const Splcoeff::AddEq& j)
{
    if (!(i.potname == j.potname))
        return (i.potname < j.potname);
    return i.dist < j.dist;
}
bool myfunction(const Potential& i, const Potential& j) { return (i.potname < j.potname); }

Splcoeff::Splcoeff(QTextStream& output_stream, const std::shared_ptr<const ADPT::SKFileInfo>& skfileinfo_)
    : m_output_stream(output_stream)
    , skfileinfo(skfileinfo_)
{
}

bool Splcoeff::calculate(const Erepfit2::Input::Erepfit2_InputData& input)
{
    int ieq = 0;

    if (!readinp(input))
    {
        return false;
    }
    sort_inputdist();

    sizeeqsys();
    ieq = include_splineeq(0);
    ieq = include_energyeq(ieq);
    ieq = include_forceeq(ieq);
    ieq = include_addeq(ieq);
    ieq = include_reaeq(ieq);

    weighteqsys();
    bool succ = svd_fulfill_spleq();
    if (!succ)
    {
        m_lastError = "Erepfit fitting error";
    }
    get_residual();
    return succ;
}

//vector<double> Splcoeff::get_coeff() const {
//	vector<double> coeff;
//	coeff = ........ take values out of vunknown;
//	return coeff;
//}

//vector<double> Splcoeff::get_vediss() const {
//	vector<double> veatom;
//	vediss = ......... take values out of vunknown;
//	return vediss
//}

void Splcoeff::sizeeqsys()
{

    nrows = 0;
    ncols = 0;
    nspleq = 0;
    neeq = 0;
    nfeq = 0;
    naddeq = 0;
    nreaeq = 0;
    nsmootheq = 0;
    nsplcoeff = 0;
    nfree = 0;
    nelem = 0;
    natom = 0;
    natfit = 0;
    nzl = 0;

    // find out numbers of spline-coefficients
    // f and derivatives up to (order-1) are assumed to be continuous
    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        nspleq += (vpot[ipot].nknots - 1) * (continous_order + 1);
        nsplcoeff += (vpot[ipot].nknots - 1) * (vpot[ipot].ordspl + 1);
    }
    nfree = (nsplcoeff - nspleq);

    // find out number of elements
    for (int imol = 0; imol < vmol.size(); imol++)
    {
        for (int ielem = 0; ielem < vmol[imol].nelem; ielem++)
        {
            bool newElem = true;
            for (int jelem = 0; jelem < velem.size(); jelem++)
            {
                if (velem[jelem] == vmol[imol].elemname[ielem])
                {
                    newElem = false;
                    break;
                }
            }
            if (newElem)
                velem.push_back(vmol[imol].elemname[ielem]);
        }
    }

    // nelem: total # elements in the system.
    // veatom: total # of elements provided in input.
    nelem = velem.size();
    natfit = nelem - veatom.size();

    // find out total number of atoms, number of energy- and force-equations
    //
    for (int i = 0; i < vmol.size(); i++)
    {
        natom += vmol[i].natom;
    }
    for (int j = 0; j < this->energyInfos.size(); ++j)
    {
        if (energyInfos[j].weight > 1.0e-4)
        {
            neeq += 1;
        }
    }
    if (neeq == 0)
    {
        natfit = 0;
    }
    for (int j = 0; j < this->forceInfos.size(); ++j)
    {
        if (forceInfos[j].weight > 1.0e-4)
        {
            if (vmol[forceInfos[j].index].m_system.geometry.isPBC)
            {
                nfeq += 9;
            }
            else
            {
                nfeq += vmol[forceInfos[j].index].natom * 3;
            }
        }
    }

    // find out number of additional equations and reaction equations and smoothing equations
    naddeq = vadd.size();
    nreaeq = vrea.size();

    // sum nrows and ncols
    nrows = nspleq + neeq + nfeq + naddeq + nreaeq + nsmootheq;
    ncols = nsplcoeff + natfit;

    if (ncols > nrows)
    {
        nzl = ncols - nrows;
        nrows += ncols - nrows;
    }

    // size equation system
    eqmat.resize(nrows, ncols);
    vunknown.resize(ncols);
    vref.resize(nrows);
    vweight.resize(nrows);
    sigma.resize(ncols);
    sigma_e_f.resize(nfree + natfit);
    vres.resize(nrows);

    // initialize eqmat, vref, vunkown, sigmas to 0, vweight to 1
    eqmat.setZero();
    vunknown.setZero();
    vref.setZero();
    sigma.setZero();
    sigma_e_f.setZero();
    vres.setZero();
    vweight.setOnes();
}

int Splcoeff::include_splineeq(const int ieq_)
{
    // gets number of equation already set up
    // returns total number of equation after this funtion
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // f_i(x) = a_i^0 + a_i^1 * (x-x_i) + a_i^2 * (x-x_i)^2 + ...
    // example 4th order spline
    // eqmat =    1   (x_2 - x_1)    (x_2 - x_1)^2    (x_2 - x_1)^3      (x_2 - x_1)^4   ...  -1    0    0    0   0 ... { f_1(x_2) - f_2(x_2) = 0 }
    // 	      0        1       2*(x_2 - x_1)    3*(x_2 - x_1)^2    4*(x_2 - x_1)^3   ...   0   -1    0    0   0 ... { f'_1(x_2) - f'_2(x_2) = 0 }
    //            0        0              2         6*(x_2 - x_1)^2   12*(x_2 - x_1)^2   ...   0    0   -2    0   0 ... { f''_1(x_2) - f''_2(x_2) = 0 }
    //            0        0              0                6          24*(x_2 - x_1)     ...   0    0    0   -6   0 ... { f'''_1(x_2) - f'''_2(x_2) = 0 }
    //	      0        0              0                0                  0          ...   1  (x_3-x_2) ...
    //            ...       ...           ...             ...                ...         ...   ........................
    //
    //            for all inner knots,
    //            than cutoff equation is included { f_last(x_cutoff) = 0 } and derivatives
    //            than the equations of the next potential follow...
    /////////////////////////////////////////////////////////////////////////////////////////////////////

    int ieq = ieq_; // position variable for row of matrix (current spline eqation)
    int icol = 0; // position variable for column of matrix
    int hlp;

    // for each potential
    // for all knots: f_i(x_i+1) - f_i+1(x_i+1) = 0 and derivatives
    // for all coefficient within one knot
    // for all derivatives (0 till order-1) // update to continous_order
    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        for (int iknot = 0; iknot < vpot[ipot].nknots - 2; iknot++)
        {
            for (int icoeff = 0; icoeff <= vpot[ipot].ordspl; icoeff++)
            {
                for (int ider = 0; ider <= continous_order; ider++)
                {
                    eqmat(ieq + ider, icol) += myfac(icoeff, ider) * pow(vpot[ipot].vr[iknot + 1] - vpot[ipot].vr[iknot], icoeff - ider);
                }
                icol++;
            }
            for (int ider = 0; ider <= continous_order; ider++)
            {
                eqmat(ieq + ider, icol + ider) = -myfac(ider, ider);
            }
            ieq += (continous_order + 1);
        }
        // equation for last knot: f_last(x_cutoff) = 0 and derivatives
        hlp = vpot[ipot].nknots - 1;
        for (int icoeff = 0; icoeff <= vpot[ipot].ordspl; icoeff++)
        {
            for (int ider = 0; ider <= continous_order; ider++)
            {
                eqmat(ieq + ider, icol) += myfac(icoeff, ider) * pow(vpot[ipot].vr[hlp] - vpot[ipot].vr[hlp - 1], icoeff - ider);
            }
            icol++;
        }
        ieq += (continous_order + 1);
    }
    return ieq;
}

int Splcoeff::include_energyeq(const int ieq_)
{
    //////////////////////////////////////////////////////////////////////////////////////////
    //     Ebind       = Etot - sum(Eat) = Eel + Erep - sum(Eat)				//
    // =>  Ebind - Eel = Erep - sum(Eat) = sum_(A!=B)( Erep_AB(R_AB) ) - sum(Eat)  		//
    //     equations added to eqmat and vref respectively					//
    //////////////////////////////////////////////////////////////////////////////////////////

    int ieq = ieq_; // position variable for row of matrix (current spline eqation)

    for (int i = 0; i < energyInfos.size(); ++i)
    {
        int imol = energyInfos[i].index;
        // add left hand side of equation system (eqmat)
        add_energy_lhs(ieq, vmol[imol], 1);

        // add -1 to coefficient of Eatoms (only those which are getting fitted)
        for (int iat = 0; iat < vmol[imol].natom; iat++)
        {
            int j;
            for (j = 0; velem[j] != vmol[imol].atomname[iat]; j++)
            {
            }
            if (j < veatom.size())
            {
                vref[ieq] += veatom[j];
            }
            else
            {
                eqmat(ieq, vunknown.size() + j - nelem) -= 1;
            }
        }
        // add rhs and weight
        vref[ieq] += (vmol[imol].ebindmel + energyInfos[i].ediss);
        vweight[ieq] = energyInfos[i].weight;
        ieq++;
    }

    return ieq;
}

double cellvol(const dMatrix3x3& latt)
{
    double res = 0.0;
    res = latt[0][0] * (latt[1][1] * latt[2][2] - latt[2][1] * latt[1][2]);
    res -= latt[0][1] * (latt[1][0] * latt[2][2] - latt[2][0] * latt[1][2]);
    res += latt[0][2] * (latt[1][0] * latt[2][1] - latt[2][0] * latt[1][1]);
    return std::abs(res);
}

int Splcoeff::include_forceeq(const int ieq_)
{
    //////////////////////////////////////////////////////////////////////////////////////////
    //     F_k       = 0   = Fel_k + Frep_k       , k is coord of every atom                //
    // =>  Frep_k    = - Fel_k                                                              //
    //               = sum_(at1!=at2) f'_at1at2 * (r_k_at1 - r_k_at2) / r_at1at2		//
    //     f'_at1at2 = ...									//
    //     											//
    //     equations added to eqmat and vref respectively					//
    //////////////////////////////////////////////////////////////////////////////////////////

    int ieq = ieq_;
    double dist;
    Colind colind;

    for (int i = 0; i < forceInfos.size(); ++i)
    {
        int imol = forceInfos[i].index;
        double fweight = forceInfos[i].weight;

        if (fweight < 1.0e-4)
            continue;

        if (vmol[imol].m_system.geometry.isPBC)
        {
            auto& mol = vmol[imol];
            auto keys = mol.dist_new.getKeys();
            for (const auto& key : keys)
            {
                int sp1 = key.first;
                int sp2 = key.second;

                const auto& dist_items = mol.dist_new.getDist(sp1, sp2);

                for (const auto& dist_item : dist_items)
                {

                    dist = std::get<0>(dist_item);
                    colind = findcolind(mol.elemname[sp1], mol.elemname[sp2], dist, mol.name, sp1, sp2);
                    if (colind.e == 2)
                    {
                        continue; // if distance is greater than cutoff
                    }

                    double coeff = 1.0;
                    if (std::get<1>(dist_item))
                    {
                        coeff = 0.5;
                    }
                    if (colind.e == 1)
                    { // if pot is not selected to be optimized
                        addextef_crystal(ieq, mol.elemnameext[sp1], mol.elemnameext[sp2], std::get<0>(dist_item), coeff, 1, 1.0);
                        continue;
                    }

                    const auto& coord1 = std::get<2>(dist_item);
                    const auto& coord2 = std::get<3>(dist_item);

                    double vol = cellvol(mol.m_system.geometry.lattice_vectors) / AA_Bohr / AA_Bohr / AA_Bohr;
                    for (int xyz = 0; xyz < 3; ++xyz)
                    {
                        double dx = (coord1[xyz] - coord2[xyz]);
                        for (int xyz2 = 0; xyz2 < 3; ++xyz2)
                        {
                            double dy = (coord1[xyz2] - coord2[xyz2]);
                            int shift = 3 * xyz + xyz2;
                            double dr = dist - vpot[colind.p].vr[colind.k - 1];

                            for (int j = 1; j <= vpot[colind.p].ordspl; j++)
                            {
                                eqmat(ieq + shift, colind.c + j) += -coeff * dx * dy / vol / dist * j * pow(dr, j - 1); //add force to lhs, not gradient!
                            }
                        }
                    }
                }
            }

            // add rhs and weight
            vref[ieq] = -vmol[imol].m_system.elec_data.stress[0][0];
            vref[ieq + 1] = -vmol[imol].m_system.elec_data.stress[0][1];
            vref[ieq + 2] = -vmol[imol].m_system.elec_data.stress[0][2];
            vref[ieq + 3] = -vmol[imol].m_system.elec_data.stress[1][0];
            vref[ieq + 4] = -vmol[imol].m_system.elec_data.stress[1][1];
            vref[ieq + 5] = -vmol[imol].m_system.elec_data.stress[1][2];
            vref[ieq + 6] = -vmol[imol].m_system.elec_data.stress[2][0];
            vref[ieq + 7] = -vmol[imol].m_system.elec_data.stress[2][1];
            vref[ieq + 8] = -vmol[imol].m_system.elec_data.stress[2][2];

            vweight[ieq] = fweight;
            vweight[ieq + 1] = fweight;
            vweight[ieq + 2] = fweight;
            vweight[ieq + 3] = fweight;
            vweight[ieq + 4] = fweight;
            vweight[ieq + 5] = fweight;
            vweight[ieq + 6] = fweight;
            vweight[ieq + 7] = fweight;
            vweight[ieq + 8] = fweight;

            ieq += 9;
        }
        else
        {
            for (int at1 = 0; at1 < vmol[imol].natom; at1++)
            {
                for (int at2 = 0; at2 < vmol[imol].natom; at2++)
                {
                    if (at1 == at2)
                    {
                        continue;
                    }
                    else if (at1 > at2)
                    {
                        dist = vmol[imol].dist(at2, at1);
                    }
                    else
                    {
                        dist = vmol[imol].dist(at1, at2); //////// create a n x n distmatrix??????
                    }

                    colind = findcolind(vmol[imol].atomname[at1], vmol[imol].atomname[at2], dist,
                        vmol[imol].name, at1, at2);
                    if (colind.e == 2)
                    {
                        continue; // if distance is greater than cutoff
                    }
                    if (colind.e == 1)
                    { // if potential not selected to optimize
                        addextef(ieq, vmol[imol], at1, at2, 1, 1.0);
                        continue;
                    }
                    // add lhs
                    for (int xyz = 0; xyz < 3; xyz++)
                    {
                        double x1 = vmol[imol].m_system.geometry.coordinates[at1].coord[xyz] * AA_Bohr;
                        double x2 = vmol[imol].m_system.geometry.coordinates[at2].coord[xyz] * AA_Bohr;
                        double fac = (x1 - x2) / dist;
                        for (int j = 1; j <= vpot[colind.p].ordspl; j++)
                        {
                            eqmat(ieq + xyz, colind.c + j) += -fac * j * pow(dist - vpot[colind.p].vr[colind.k - 1], j - 1); //add force to lhs, not gradient!
                        }
                    }
                }
                // add rhs and weight
                vref[ieq] += vmol[imol].frep(at1, 0);
                vref[ieq + 1] += vmol[imol].frep(at1, 1);
                vref[ieq + 2] += vmol[imol].frep(at1, 2);
                vweight[ieq] = fweight;
                vweight[ieq + 1] = fweight;
                vweight[ieq + 2] = fweight;

                ieq += 3;
            }
        }
    }
    return ieq;
}

int Splcoeff::include_addeq(const int ieq_)
{
    int ieq = ieq_;
    int iadd, ipot, icol, knot;

    for (iadd = 0; iadd < vadd.size(); iadd++)
    {
        // determine icol for the spline coefficient
        icol = 0;
        for (ipot = 0; ipot < vpot.size(); ipot++)
        {
            if (vpot[ipot].potname == vadd[iadd].potname)
                break;
            icol += (vpot[ipot].nknots - 1) * (vpot[ipot].ordspl + 1);
        }
        if (ipot == vpot.size())
        {
            m_output_stream << endl
                            << "ERROR:" << endl
                            << endl
                            << "The potential \"" << vadd[iadd].potname << "\" was not asked to optimize, "
                            << "thus there cannot be added an additional equation for mentioned potential."
                            << endl
                            << "exit erepfit" << endl
                            << endl;
            throw std::runtime_error("Additional equation error");
        }
        for (knot = 0; knot < vpot[ipot].vr.size() && vadd[iadd].dist >= vpot[ipot].vr[knot]; knot++)
        {
        }
        if (knot == 0)
        {
            m_output_stream << endl
                            << "ERROR: " << endl
                            << endl
                            << vadd[iadd].potname << "\t distance(bohr,AA):  " << vadd[iadd].dist << "," << vadd[iadd].dist * AA_Bohr
                            << endl
                            << "This distance in this additional equation is smaller than the value "
                            << "of the first knot of the respective potential. "
                            << endl
                            << "exit erepfit" << endl
                            << endl;
            throw std::runtime_error("Additional equation error");
        }
        if (knot >= vpot[ipot].vr.size())
        {
            m_output_stream << endl
                            << "ERROR: " << endl
                            << endl
                            << vadd[iadd].potname << "\t distance in AA " << vadd[iadd].dist << endl
                            << "This distance in this additional equation is greater than the cutoff-distance of the respective potential."
                            << endl
                            << "exit erepfit" << endl
                            << endl;
            throw std::runtime_error("Additional equation error");
        }
        icol += (knot - 1) * (vpot[ipot].ordspl + 1);
        // icol determined, now add additional equation in eqmat
        for (int i = 0; i <= vpot[ipot].ordspl; i++)
        {
            if (vadd[iadd].deriv == 0)
            {
                eqmat(ieq, icol) += pow(vadd[iadd].dist - vpot[ipot].vr[knot - 1], i);
            }
            else if (vadd[iadd].deriv == 1)
            {
                eqmat(ieq, icol) += i * pow(vadd[iadd].dist - vpot[ipot].vr[knot - 1], i - 1);
            }
            else if (vadd[iadd].deriv == 2)
            {
                eqmat(ieq, icol) += i * (i - 1) * pow(vadd[iadd].dist - vpot[ipot].vr[knot - 1], i - 2);
            }
            else if (vadd[iadd].deriv == 3)
            {
                eqmat(ieq, icol) += i * (i - 1) * (i - 2) * pow(vadd[iadd].dist - vpot[ipot].vr[knot - 1], i - 3);
            }
            icol++;
        }
        vref[ieq] = vadd[iadd].val;
        vweight[ieq] = vadd[iadd].weight;
        ieq++;
    }

    return ieq;
}

int Splcoeff::include_reaeq(const int ieq_)
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //													//
    //	Erea = sum (E_tot^prod) - sum(E_tot^reactants)							//
    //													//
    //	e.g.  3 c2h2 --> c6h6										//
    //	Erea = Etot(c6h6) - 3 Etot(c2h2) = Eel(c6h6) + Erep(c6h6) - 3 Eel(c2h2) - 3 Erep(c2h2)		//
    //	=>     Erep(c6h6) - 3 Erep(c2h2) = Erea - Eel(c6h6) + 3 Erep(c2h2)				//
    //	now express Erep in Splinecoefficient as described in the energyequation			//
    //													//
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    int ieq = ieq_;
    int ind;

    for (int irea = 0; irea < vrea.size(); irea++)
    {
        for (int imol = 0; imol < vrea[irea].vmol.size(); imol++)
        {
            ind = vrea[irea].vmol[imol];
            add_energy_lhs(ieq, vreamol[ind], vrea[irea].vcoeff[imol]);

            vref[ieq] -= vrea[irea].vcoeff[imol] * vreamol[ind].eel;
        }

        vref[ieq] += vrea[irea].reaE;
        vweight[ieq] = vrea[irea].reaweight;
        ieq++;
    }

    return ieq;
}

void Splcoeff::weighteqsys()
{

    for (int irow = 0; irow < nrows; irow++)
    {
        for (int icol = 0; icol < ncols; icol++)
        {
            eqmat(irow, icol) = eqmat(irow, icol) * vweight[irow];
        }
        vref[irow] = vref[irow] * vweight[irow];
    }
}

template <typename A, typename B>
double diffMatrix(int rows, int cols, const A& matA, const B& matB)
{
    double diff = 0.0;
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            double tmp = matA(i, j) - matB(i, j);
            if (std::abs(tmp) > 1.0e-8)
            {
                std::cerr << "(" << i << "," << j << "): " << tmp << std::endl;
            }
            diff += std::abs(tmp);
        }
    }
    return diff;
}

template <typename A>
void printBoostMatrixInMathetica(const A& mat)
{
    QStringList all;
    for (int i = 0; i < mat.size1(); ++i)
    {
        QStringList ala;
        for (int j = 0; j < mat.size2(); ++j)
        {
            ala.append(QString::number(mat(i, j)));
        }
        all << "{" + ala.join(",") + "}";
    }
    std::cerr << "{" << all.join(",").toStdString() << "}" << std::endl;
}

bool Splcoeff::svd_fulfill_spleq()
{
    //
    // split up eqmat in T, W, E and vunknown in a3 and a4 as follows:
    // a3 contains only the unknowns of (order-1) of the unknowns of each spline-segment (vector)
    // a4 contains the remaining unknowns (vector)
    //
    // all spline-equations:
    // 	T * a3  =  W * a4   ==>  a3 = T^(-1) * W * a4
    // remaining equations:
    // 	E * vunknown = vref'    (' means excluded the spline-equations where vref =0)
    //
    // 	further split-up of E
    // 	e3 * a3 + e4 * a4 = vref'
    // 	(e3 * T^(-1) * W + e4) * a4 = vref'
    //
    // now singular value decomposition for (e3 * T^(-1) * W + e4) = A (as denoted in code)
    //      A * a4 = vref' = u * sigma_e_f * vt * a4
    //      a4 = v * sigma_e_f^(-1) * ut * vref'
    //
    // 	calculation of a4 done as follows:
    //      a4[i] = v * u_coli * vref' / sigma_e_f(i)
    //
    // resubsitute to yield a3
    // 	a3 = T^(-1) * W * a4
    //
    // write a3 and a4 in vunknown
    //
    // calculate exponential coefficient for every potential (vunknown needed)
    //

    if (debug)
    {
        {
            ofstream fout("eqmat.dat");
            fout << eqmat << endl;
        }
        {
            ofstream fout("pot.dat");
            for (int i = 0; i < vpot.size(); ++i)
            {
                fout << vpot[i].potname.toStdString() << " ";
                for (int j = 0; j < vpot[i].vr.size(); ++j)
                {
                    fout << vpot[i].vr[j] << " ";
                }
                fout << endl;
            }
        }
    }

//try using dgglse of LAPACK to solve
// minimize ||c - A*x||2 subject to B*x = d
#ifdef USE_MKL
    if (debug)
    {
        int m = eqmat.rows() - nspleq;
        int p = nspleq;
        int n = ncols;
        Eigen::VectorXd x(n);
        MatrixXXd A(m, n);
        MatrixXXd B(p, n);
        Eigen::VectorXd c(m);
        Eigen::VectorXd d(p);

        for (int ieq = 0; ieq < nspleq; ieq++)
        {
            B.row(ieq) = eqmat.row(ieq);
            d[ieq] = vref[ieq];
        }
        for (int ieq = nspleq; ieq < eqmat.rows(); ieq++)
        {
            A.row(ieq - nspleq) = eqmat.row(ieq);
            c[ieq - nspleq] = vref[ieq];
        }

        int info = LAPACKE_dgglse(LAPACK_COL_MAJOR, m, n, p, A.data(), m, B.data(), p, c.data(), d.data(), x.data());

        {
            ofstream fout("x.dat");
            fout << "#" << info << endl;
            fout << x << endl;
        }
    }
#endif

    // a_svd is which parameter of each ploynomial is the free potential dependent parameter
    // since it has almost no influence we use a fixed coding...
    int a_svd = continous_order + 1;
    const int spline_order = 4;

    // setup T and W
    MatrixXXd T(nspleq, nspleq);
    MatrixXXd W(nspleq, nfree);

    T.setZero();
    W.setZero();

    int totalblock = nspleq / (a_svd);
    int freeblocksize = nfree / totalblock;

    for (int iRow = 0; iRow < totalblock; iRow++)
    {
        for (int iCol = iRow; iCol < totalblock; iCol++)
        {
            if (iCol > (iRow + 1))
            {
                break;
            }
            T.block(iRow * a_svd, iCol * a_svd, a_svd, a_svd) = eqmat.block(iRow * (a_svd), iCol * (spline_order + 1), a_svd, a_svd);
            W.block(iRow * a_svd, iCol * freeblocksize, a_svd, freeblocksize) = -eqmat.block(iRow * (a_svd), (iCol + 1) * (spline_order + 1) - freeblocksize, a_svd, freeblocksize);
        }
    }

    if (debug)
    {
        {
            ofstream fout("W.dat");
            fout << W << endl;
        }
        {
            ofstream fout("T.dat");
            fout << T << endl;
        }
    }

    // solve  T * W_out1 = W_in1   ( w gets overwritten )
    Eigen::FullPivLU<MatrixXXd> lu(T);

    // now apply to all other eqations but the splineequations

    // first seperate energyandforce - matrix to e3 and e4
    int rest_num_rows = eqmat.rows() - nspleq;
    MatrixXXd e3(rest_num_rows, nspleq);
    MatrixXXd e4(rest_num_rows, nfree);

    e3.setZero();
    e4.setZero();

    for (int iCol = 0; iCol < totalblock; iCol++)
    {
        e3.block(0, iCol * a_svd, rest_num_rows, a_svd) = eqmat.block(nspleq, iCol * (spline_order + 1), rest_num_rows, a_svd);
        e4.block(0, iCol * freeblocksize, rest_num_rows, freeblocksize) = eqmat.block(nspleq, (iCol + 1) * (spline_order + 1) - freeblocksize, rest_num_rows, freeblocksize);
    }

    if (debug)
    {
        ofstream fout("e3.dat");
        fout << e3 << endl;
    }
    if (debug)
    {
        ofstream fout("e4.dat");
        fout << e4 << endl;
    }

    // now add e_atom contribution
    MatrixXXd A(eqmat.rows() - nspleq, nfree);

    A = e3 * lu.inverse() * W + e4;

    A.conservativeResize(A.rows(), A.cols() + natfit);
    for (int iel = -natfit; iel < 0; iel++)
    {
        for (int irow = 0; irow < A.rows(); irow++)
        {
            A(irow, A.cols() + iel) = eqmat(nspleq + irow, eqmat.cols() + iel);
        }
    }

    if (debug)
    {
        ofstream fout("A.dat");
        fout << A << endl;
    }

    // then svd for (e3 * w_in2 + e4)
    Eigen::JacobiSVD<MatrixXXd, Eigen::FullPivHouseholderQRPreconditioner> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
    svd.setThreshold(1.0e-4);
    sigma_e_f = svd.singularValues();

    //calculate condition numbers and truncation of sigma
    double zero = 1.0e-4;
    for (trunc = 0; trunc < sigma_e_f.size(); trunc++)
    {
        if (sigma_e_f[trunc] < zero)
            break;
    }
    cond1 = 0;
    cond2 = 0;
    if (sigma_e_f[sigma_e_f.size() - 1] > 0)
    {
        cond1 = sigma_e_f[0] / sigma_e_f[sigma_e_f.size() - 1];
    }
    cond2 = sigma_e_f[0] / sigma_e_f[trunc - 1];

    // calculate unknowns: a4 = v * sigma_e_f^(-1) * ut * vref'
    Eigen::VectorXd vref_ = vref.segment(nspleq, vref.size() - nspleq);

    if (debug)
    {
        {
            ofstream fout("sigma.dat");
            fout << sigma_e_f << endl;
        }
        {
            ofstream fout("vref_transformed.dat");
            fout << vref_ << endl;
        }
        {
            ofstream fout("vref.dat");
            fout << vref << endl;
        }
    }

    Eigen::VectorXd hlp1(nfree + natfit);

    hlp1.setZero();
    for (int i = 0; i < trunc; i++)
    {
        hlp1[i] = svd.matrixU().col(i).dot(vref_) / sigma_e_f[i];
    }

    Eigen::VectorXd a4_el = svd.matrixV() * hlp1;
    Eigen::VectorXd a4 = a4_el.head(nfree);
    Eigen::VectorXd a3 = lu.inverse() * W * a4;

    if (debug)
    {
        ofstream fout("a4.dat");
        fout << a4 << endl;
    }

    if (debug)
    {
        ofstream fout("a3.dat");
        fout << a3 << endl;
    }

    // write a3 and a4 in vunknown
    int icolunknown = 0;
    int icola3 = 0;
    int icola4 = 0;
    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        for (int iknot = 0; iknot < vpot[ipot].nknots - 1; iknot++)
        {
            for (int ia = 0; ia < a_svd; ia++)
            {
                vunknown[icolunknown + ia] = a3[icola3];
                icola3++;
            }
            for (int ia = a_svd; ia <= vpot[ipot].ordspl; ia++)
            {
                vunknown[icolunknown + ia] = a4_el[icola4];
                icola4++;
            }

            icolunknown += vpot[ipot].ordspl + 1;
        }
    }

    //    vunknown.block(0,0,n,1) = x;

    for (int iel = -natfit; iel < 0; iel++)
    {
        vunknown[vunknown.size() + iel] = a4_el[a4_el.size() + iel];
    }

    if (debug)
    {
        ofstream fout("vunknown.dat");
        fout << vunknown << endl;
    }

    // calculate exponential for each potential ( Erep=exp(-Ar+B)+C )
    int icoeff = 0;
    for (int i = 0; i < vpot.size(); i++)
    {
        vpot[i].expA = -2 * vunknown[icoeff + 2] / vunknown[icoeff + 1];
        vpot[i].expB = log(-vunknown[icoeff + 1] / vpot[i].expA) + vpot[i].expA * vpot[i].vr[0];
        vpot[i].expC = vunknown[icoeff] - exp(-vpot[i].expA * vpot[i].vr[0] + vpot[i].expB);
        icoeff += (vpot[i].vr.size() - 1) * (vpot[i].ordspl + 1);
    }

    if (std::isnan(cond2))
    {
        return false;
    }
    return true;
}

void Splcoeff::get_residual()
{

    vres = vref - eqmat * vunknown;

    restotS = 0;
    reseS = 0;
    resfS = 0;
    resaddS = 0;
    resreaS = 0;
    ressmoothS = 0;
    restotU = 0;
    reseU = 0;
    resfU = 0;
    resaddU = 0;
    resreaU = 0;
    ressmoothU = 0;
    restot2 = 0;
    rese2 = 0;
    resf2 = 0;
    resadd2 = 0;
    resrea2 = 0;
    ressmooth2 = 0;

    int irow = nspleq;
    for (int i = 0; i < neeq; i++)
    {
        reseS += vres[irow];
        reseU += std::abs(vres[irow]);
        rese2 += vres[irow] * vres[irow];
        irow++;
    }
    for (int i = 0; i < nfeq; i++)
    {
        resfS += vres[irow];
        resfU += std::abs(vres[irow]);
        resf2 += vres[irow] * vres[irow];
        irow++;
    }
    for (int i = 0; i < naddeq; i++)
    {
        resaddS += vres[irow];
        resaddU += std::abs(vres[irow]);
        resadd2 += vres[irow] * vres[irow];
        irow++;
    }
    for (int i = 0; i < nreaeq; i++)
    {
        resreaS += vres[irow];
        resreaU += std::abs(vres[irow]);
        resrea2 += vres[irow] * vres[irow];
        irow++;
    }
    for (int i = 0; i < nsmootheq; i++)
    {
        ressmoothS += vres[irow];
        ressmoothU += std::abs(vres[irow]);
        ressmooth2 += vres[irow] * vres[irow];
        irow++;
    }
    restotS = reseS + resfS + resaddS + resreaS + ressmoothS;
    restotU = reseU + resfU + resaddU + resreaU + ressmoothU;
    restot2 = rese2 + resf2 + resadd2 + resrea2 + ressmooth2;
}

void Splcoeff::setReferenceRepulsivePotentials(const QMap<QString, QString>& set)
{
    referenceRepulsivePotentials = set;
    QMapIterator<QString, QString> it(referenceRepulsivePotentials);
    while (it.hasNext())
    {
        it.next();
        QSharedPointer<RepulsivePotential> rep(new RepulsivePotential);
        rep->loadfile(it.key(), it.value());
        m_externalRepulsivePotentials.insert(it.key(), rep);
    }
}

bool Splcoeff::readinp(const Erepfit2::Input::Erepfit2_InputData& input)
{
    dftbversion = input.options.toolchain.path;
    int npot, nadd, nmol, nabbr, nrea;
    QMapIterator<QString, double> it(input.atomicEnergy.atomic_energy);

    while (it.hasNext())
    {
        it.next();
        velem.push_back(it.key());
        veatom.push_back(it.value());
    }

    //initalize the potentials
    npot = 0;
    {
        QMapIterator<QString, Erepfit2::Input::PotentialGrid> it(input.potential_grids);
        while (it.hasNext())
        {
            it.next();
            vpot.resize(npot + 1);
            auto pair = ADPT::PotentialName::fromString(it.key());
            QString sym1 = (pair.getElement1());
            QString sym2 = (pair.getElement2());

            if (sym1 > sym2)
            {
                std::swap(sym1, sym2);
            }

            auto final_potname = ADPT::PotentialName(sym1, sym2).toString();

            vpot[npot].init(sym1, sym2, final_potname, it.value().knots, input.options.spline_order);
            ++npot;
        }
        std::sort(vpot.begin(), vpot.end(), myfunction);
    }

    //additional equations
    nadd = 0;
    {
        QMapIterator<QString, QList<Erepfit2::Input::AdditionalEquation>> it(input.equations.additionalEquations);
        while (it.hasNext())
        {
            it.next();
            auto pair = ADPT::PotentialName::fromString(it.key());
            QString sym1 = (pair.getElement1());
            QString sym2 = (pair.getElement2());

            if (sym1 > sym2)
            {
                std::swap(sym1, sym2);
            }
            QString potname = ADPT::PotentialName(sym1, sym2).toString();

            for (auto equ : it.value())
            {
                vadd.resize(nadd + 1);
                vadd[nadd].potname = potname;
                vadd[nadd].dist = getDistanceValue(equ.distance, equ.unit);
                vadd[nadd].deriv = equ.derivative;
                vadd[nadd].val = equ.value;
                vadd[nadd].weight = equ.weight;
                if (vadd[nadd].deriv > 3)
                {
                    m_output_stream << "ERROR: " << endl
                                    << endl;
                    m_output_stream << "additional equations can only be added for 0st,1st,2nd or 3rd dervative (0,1,2,3)!"
                                    << endl
                                    << endl
                                    << "exit erepfit" << endl
                                    << endl;
                    return false;
                }
                ++nadd;
            }
        }
        std::sort(vadd.begin(), vadd.end(), sortAddition);
    }

    // molecular base equation
    nmol = 0;
    {
        for (int i = 0; i < input.equations.energyEquations.size(); ++i)
        {
            const auto& equ = input.equations.energyEquations[i];

            int index = indexUUID(input.systems, equ.uuid.toString());

            if (index < 0)
            {
                m_lastError = "Erepfit molecule init error: " + equ.uuid.toString();
                return false;
            }
            vmol.resize(nmol + 1);
            if (!vmol[nmol].init(input.systems[index], " "))
            {
                m_lastError = "Erepfit molecule init error: " + equ.uuid.toString();
                return false;
            }
            EnegyEquationInfo info;
            info.ediss = -getEnergyValue(equ.energy, equ.unit);
            info.weight = equ.weight;
            info.index = nmol;
            energyInfos.append(info);
            ++nmol;
        }
    }

    for (int i = 0; i < input.equations.forceEquations.size(); ++i)
    {

        const auto& equ = input.equations.forceEquations[i];

        int index = indexUUID(input.systems, equ.uuid.toString());

        if (index < 0)
        {
            m_lastError = "Erepfit molecule init error: " + equ.uuid.toString();
            return false;
        }
        vmol.resize(nmol + 1);

        if (!vmol[nmol].init(input.systems[index], " "))
        {
            m_lastError = "Erepfit molecule init error: " + equ.uuid.toString();
            return false;
        }
        ForceEquationInfo info;
        info.index = nmol;
        info.weight = equ.weight;
        forceInfos.append(info);
        ++nmol;
    }

    {
        //reactions reactions
        QMap<QString, int> names;
        nabbr = 0;
        nrea = 0;
        for (int irea = 0; irea < input.equations.reactionEquations.size(); ++irea)
        {
            vrea.resize(nrea + 1);

            const auto& equ = input.equations.reactionEquations[irea];

            double energy = getEnergyValue(equ.energy, equ.unit);
            vrea[nrea].setEnergyAndWeight(energy, equ.weight);

            for (int imol = 0; imol < equ.reactants.size(); ++imol)
            {
                auto mol = equ.reactants[imol];

                int index = indexUUID(input.systems, mol.uuid.toString());

                if (index < 0)
                {
                    m_lastError = "Erepfit molecule init error: " + mol.name;
                    return false;
                }

                if (!names.contains(mol.uuid.toString()))
                {
                    vreamol.resize(nabbr + 1);
                    vreamol[nabbr].abbr = mol.name;

                    if (!vreamol[nabbr].init(input.systems[index], mol.name))
                    {
                        m_lastError = "Erepfit molecule init error: " + mol.uuid.toString();
                        return false;
                    }

                    names.insert(mol.uuid.toString(), nabbr);
                    ++nabbr;
                }
                int nindex = names[mol.uuid.toString()];
                vrea[nrea].addReactant(mol.coefficient, nindex, mol.name);
            }

            for (int imol = 0; imol < equ.products.size(); ++imol)
            {
                const auto& mol = equ.products[imol];

                int index = indexUUID(input.systems, mol.uuid.toString());

                if (index < 0)
                {
                    m_lastError = "Erepfit molecule init error: " + mol.name;
                    return false;
                }

                if (!names.contains(mol.uuid.toString()))
                {
                    vreamol.resize(nabbr + 1);
                    vreamol[nabbr].abbr = mol.name;

                    if (!vreamol[nabbr].init(input.systems[index], mol.name))
                    {
                        m_lastError = "Erepfit molecule init error: " + mol.uuid.toString();
                        return false;
                    }

                    names.insert(mol.uuid.toString(), nabbr);
                    ++nabbr;
                }
                int nindex = names[mol.uuid.toString()];
                vrea[nrea].addProduct(mol.coefficient, nindex, mol.name);
            }

            vrea[nrea].buildReactionString();
            ++nrea;
        }
    }

    // check if a_b_ potential and b_a_ potential was given to optimize, if yes then only one will be considered
    for (int ipot = 0; ipot < vpot.size() - 1; ipot++)
    {
        for (int jpot = ipot + 1; jpot < vpot.size(); jpot++)
        {
            if (vpot[ipot].potname == vpot[jpot].potname)
            {
                m_output_stream << "CAUTION: The potentials \"" << vpot[ipot].potname << "\" and \"" << vpot[jpot].potname
                                << "\" will not be treated seperately. " << endl
                                << "         Only the input data from potential \"" << vpot[ipot].potname
                                << "\" will be regarded in this erepfit." << endl;
                vpot.erase(vpot.begin() + jpot);
            }
        }
    }
    return true;
}

void Splcoeff::sort_inputdist()
{
    // set dummy vpot[ipot].vmoldist[0].dist =-1
    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        vpot[ipot].init_vmoldist();
    }

    // use vmol to fill vpot[ipot].vmoldist
    for (int imol = 0; imol < vmol.size(); imol++)
    {
        if (vmol[imol].m_system.geometry.isPBC)
        {
            auto keys = vmol[imol].dist_new.getKeys();
            for (const auto& key : keys)
            {
                int sp1 = key.first;
                int sp2 = key.second;

                for (int ipot = 0; ipot < vpot.size(); ipot++)
                {
                    auto potname = findpotname(vmol[imol].elemname[sp1], vmol[imol].elemname[sp2]);
                    if (potname == vpot[ipot].potname)
                    {
                        for (const auto& dis_half : vmol[imol].dist_new.getDist(sp1, sp2))
                        {
                            vpot[ipot].insert_moldist(vmol[imol].name, std::get<0>(dis_half));
                        }
                    }
                }
            }
        }
        else
        {

            for (int iat1 = 0; iat1 < vmol[imol].natom; iat1++)
            {
                for (int iat2 = iat1 + 1; iat2 < vmol[imol].natom; iat2++)
                {
                    for (int ipot = 0; ipot < vpot.size(); ipot++)
                    {
                        auto potname = findpotname(vmol[imol].atomname[iat1], vmol[imol].atomname[iat2]);
                        if (potname == vpot[ipot].potname)
                        {

                            vpot[ipot].insert_moldist(vmol[imol].name, vmol[imol].dist(iat1, iat2));
                            break;
                        }
                    }
                }
            }
        }
    }

    // use vreamol to fill vpot[ipot].vmoldist
    for (int imol = 0; imol < vreamol.size(); imol++)
    {
        if (vreamol[imol].m_system.geometry.isPBC)
        {
            auto keys = vreamol[imol].dist_new.getKeys();
            for (const auto& key : keys)
            {
                int sp1 = key.first;
                int sp2 = key.second;

                for (int ipot = 0; ipot < vpot.size(); ipot++)
                {

                    auto potname = findpotname(vreamol[imol].elemname[sp1], vreamol[imol].elemname[sp2]);
                    if (potname == vpot[ipot].potname)
                    //                    if(vreamol[imol].elemname[sp1]+vreamol[imol].elemname[sp2]==vpot[ipot].potname ||
                    //                            vreamol[imol].elemname[sp2]+vreamol[imol].elemname[sp1]==vpot[ipot].potname)
                    {
                        for (const auto& dis_half : vreamol[imol].dist_new.getDist(sp1, sp2))
                        {
                            vpot[ipot].insert_moldist(vreamol[imol].abbr, std::get<0>(dis_half));
                        }
                    }
                }
            }
        }
        else
        {
            for (int iat1 = 0; iat1 < vreamol[imol].natom; iat1++)
            {
                for (int iat2 = iat1 + 1; iat2 < vreamol[imol].natom; iat2++)
                {
                    for (int ipot = 0; ipot < vpot.size(); ipot++)
                    {
                        auto potname = findpotname(vreamol[imol].atomname[iat1], vreamol[imol].atomname[iat2]);
                        if (potname == vpot[ipot].potname)
                        //                        if(vreamol[imol].atomname[iat1]+vreamol[imol].atomname[iat2]==vpot[ipot].potname ||
                        //                                vreamol[imol].atomname[iat2]+vreamol[imol].atomname[iat1]==vpot[ipot].potname)
                        {

                            vpot[ipot].insert_moldist(vreamol[imol].abbr, vreamol[imol].dist(iat1, iat2));
                            break;
                        }
                    }
                }
            }
        }
    }
}

void Splcoeff::writeout(QTextStream& out) const
{

    out << "** erepfit **" << endl
        << endl;

    if (vexclpot.size() > 0)
    {
        out << "WARNING: The following potentials were not asked to optimize in the input! " << endl;
        out << "         Repulsive energy and forces resulting from these potentials are calculated from potentials" << endl
            << endl;
        out << "         potential, number of tried access (energy and force eq.), minimum distance (A, a.u.) in molecule" << endl
            << endl;
        for (int i = 0; i < vexclpot.size(); i++)
        {
            out << "         " << vexclpot[i].potname << "  " << vexclpot[i].ndist << "  " << vexclpot[i].mindist * Bohr_AA
                << "  " << vexclpot[i].mindist << "  " << vexclpot[i].molname << endl;
        }
        out << endl
            << endl;
    }

    if (nzl > 0)
    {
        out << "WARNING: There are not enough conditions to solve the equation system." << endl;
        out << "         " << nzl << " zero-lines added" << endl
            << endl;
    }

    out << "interpreted input: " << endl
        << endl;
    out << "dftb-version used: " << endl;
    out << dftbversion << endl;
    out << endl;
    out << "atom , eatom" << endl;
    for (int ieat = 0; ieat < veatom.size(); ieat++)
    {
        out << velem[ieat] << "     " << QString("%1").arg(veatom[ieat], 0, 'f', 12) << endl;
    }
    out << endl;
    out << "potentials to optimize , corresponding file of knot-vector , order of spline " << endl;
    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        out << vpot[ipot].potname << "    " << vpot[ipot].ordspl << endl;
    }
    out << endl;
    out << "potential , distance (AA) , distance (bohr) , derivative(0-3) , value(a.u.) , weight , [comment for additional equation]" << endl;
    for (int iadd = 0; iadd < vadd.size(); iadd++)
    {
        QString line = QString("%1 %2 %3 %4 %5 %6").arg(vadd[iadd].potname).arg(vadd[iadd].dist * Bohr_AA, 12, 'f', 5).arg(vadd[iadd].dist, 12, 'f', 5).arg(vadd[iadd].deriv).arg(vadd[iadd].val, 12, 'f', 6).arg(vadd[iadd].weight, 12, 'f', 6);

        out << line << endl;
    }
    out << endl;

    //    out << "Fitting systems:" << endl;
    //    for(int i=0; i<vmol.size(); ++i)
    //    {
    //        out << "  " <<  vmol[i].name << endl;
    //        out << "    " << "Type: " ;
    //        if (vmol[i].m_system.geometry.isPBC)
    //        {
    //            out << "PBC" << endl;
    //            out << "      " << "KPoints: ";
    //            if (vmol[i].kpoints.m_type == ADPT::KPointsSetting::Type::Plain)
    //            {
    //                out << "Plain" << endl;
    //                for(int j=0; i<vmol[i].kpoints.m_kpoints_plain.size() ; ++j)
    //                {
    //                    out << "        " << vmol[i].kpoints.m_kpoints_plain[j].kpoint[0]
    //                        << " " << vmol[i].kpoints.m_kpoints_plain[j].kpoint[1]
    //                        << " " << vmol[i].kpoints.m_kpoints_plain[j].kpoint[2]
    //                        << " " << vmol[i].kpoints.m_kpoints_plain[j].weight << endl;
    //                }
    //            }
    //            else if (vmol[i].kpoints.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
    //            {
    //                out << "Supercell Folding" << endl;
    //                for(int li = 0; li <3; ++li)
    //                {
    //                    out << "        " ;
    //                    for(int lj=0; lj<3; ++lj)
    //                    {
    //                        out << " " << vmol[i].kpoints.m_kpoints_supercell.num_of_cells[li][lj] ;
    //                    }
    //                    out << endl;
    //                }
    //                out << "        " << vmol[i].kpoints.m_kpoints_supercell.shifts[0]
    //                        << " " << vmol[i].kpoints.m_kpoints_supercell.shifts[1]
    //                        << " " << vmol[i].kpoints.m_kpoints_supercell.shifts[2]
    //                        << " " << endl;
    //            }

    //        }
    //        else
    //        {
    //            out << "  Molecule" << endl;
    //            out << "  Charge:" << vmol[i].charge << " ";
    //        }
    //        out << "  DFTB inputfile: " << vmol[i].dftbin << endl;
    //    }
    //    for(int i=0; i<vreamol.size(); ++i)
    //    {
    //        out << "******************************" << endl;
    //        out << "  " << vreamol[i].abbr << endl
    //            << "  Geometry file: " << vreamol[i].name << endl;
    //        out << "    " << "Type: " ;
    //        if (vreamol[i].PBC)
    //        {
    //            out << "PBC" << endl;
    //            out << "      " << "KPoints: ";
    //            if (vreamol[i].kpoints.m_type == ADPT::KPointsSetting::Type::Plain)
    //            {
    //                out << "Plain" << endl;
    //                for(int j=0; i<vreamol[i].kpoints.m_kpoints_plain.size() ; ++j)
    //                {
    //                    out << "        " << vreamol[i].kpoints.m_kpoints_plain[j].kpoint[0]
    //                        << " " << vreamol[i].kpoints.m_kpoints_plain[j].kpoint[1]
    //                        << " " << vreamol[i].kpoints.m_kpoints_plain[j].kpoint[2]
    //                        << " " << vreamol[i].kpoints.m_kpoints_plain[j].weight << endl;
    //                }
    //            }
    //            else if (vreamol[i].kpoints.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
    //            {
    //                out << "Supercell Folding" << endl;
    //                for(int li = 0; li <3; ++li)
    //                {
    //                    out << "        " ;
    //                    for(int lj=0; lj<3; ++lj)
    //                    {
    //                        out << " " << vreamol[i].kpoints.m_kpoints_supercell.num_of_cells[li][lj] ;
    //                    }
    //                    out << endl;
    //                }
    //                out << "        " << vreamol[i].kpoints.m_kpoints_supercell.shifts[0]
    //                        << " " << vreamol[i].kpoints.m_kpoints_supercell.shifts[1]
    //                        << " " << vreamol[i].kpoints.m_kpoints_supercell.shifts[2]
    //                        << " " << endl;
    //            }
    //            out << "Electronic energy: " << QString("%1").arg(vreamol[i].eel, 0, 'E', 12) << endl;
    //        }
    //        else
    //        {
    //            out << "  Molecule" << endl;
    //            out << "  Charge:" << vreamol[i].charge << " ";
    //        }
    //        out << "  DFTB inputfile: " << vreamol[i].dftbin << endl;
    //    }

    out << "Energy equations:" << endl;
    for (int i = 0; i < energyInfos.size(); ++i)
    {
        int imol = energyInfos[i].index;
        out << vmol[imol].name << ", energy: " << -energyInfos[i].ediss * H_kcal << " kcal/mol, weight: " << energyInfos[i].weight << endl;
    }
    out << endl;
    for (int i = 0; i < forceInfos.size(); ++i)
    {
        int imol = forceInfos[i].index;
        out << vmol[imol].name << ", weight : " << forceInfos[i].weight << endl;
    }
    out << endl;
    //    out << "namedefinition for reactions: abbreviation , filename , dftbinp, charge" << endl;
    //    for (int  imol=0; imol<vreamol.size(); imol++)
    //    {
    //        out << vreamol[imol].abbr << "    " << vreamol[imol].name << "  " << vreamol[imol].dftbin
    //            << "   " << vreamol[imol].charge << endl;
    //    }
    //    out << endl;

    out << "reaction , \" -> \" , Reactionenergy [kcal/mol] , reaweight , comment" << endl;
    for (int irea = 0; irea < vrea.size(); irea++)
    {
        out << vrea[irea].reastr << endl;
    }
    out << endl;

    out << "_____________________________________________________________________________________" << endl
        << endl;

    out << "knot-vector for each potential (grid)" << endl
        << endl;

    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        out << vpot[ipot].potname << "    ";
        for (int ir = 0; ir < vpot[ipot].nknots; ir++)
        {
            out << vpot[ipot].vr[ir] << " ";
        }
        out << endl;
    }

    out << "_____________________________________________________________________________________" << endl
        << endl;

    out << "Distances in a.u. and AA between all atoms in all molecules appearing in the input (duplicates are removed)"
        << endl
        << endl
        << fixed;

    for (int ipot = 0; ipot < vpot.size(); ipot++)
    {
        for (auto& kv : vpot[ipot].vmoldist)
        {
            out << vpot[ipot].potname << QString("%1").arg(kv.first, 8, 'f', 3)
                << QString("%1").arg(kv.first * Bohr_AA, 8, 'f', 3) << "   " << QString::fromStdString(kv.second) << endl;
        }
        //        for (int  idist=0;idist<vpot[ipot].vmoldist.size();idist++)
        //        {
        //            out << vpot[ipot].potname << QString("%1").arg(vpot[ipot].vmoldist[idist].dist, 8,'f',3)
        //                << QString("%1").arg(vpot[ipot].vmoldist[idist].dist*AA_Bohr, 8, 'f', 3) << "   " << vpot[ipot].vmoldist[idist].molname << endl;
        //        }
        out << endl;
    }

    out << "_____________________________________________________________________________________" << endl
        << endl;

    out << "equation-matrix:" << endl;
    out << "number of rows: " << nrows << endl;
    out << "                                      number of spline-equations:     " << QString("%1").arg(nspleq, 10) << endl;
    out << "                                      number of energy-equations:     " << QString("%1").arg(neeq, 10) << endl;
    out << "                                      number of force-equations:      " << QString("%1").arg(nfeq, 10) << endl;
    out << "                                      number of additional equations: " << QString("%1").arg(naddeq, 10) << endl;
    out << "                                      number of reaction-equations:   " << QString("%1").arg(nreaeq, 10) << endl;
    out << "                                      number of smoothing-equations:  " << QString("%1").arg(nsmootheq, 10) << endl;
    out << "                                      number of zero-lines:           " << QString("%1").arg(nzl, 10) << endl;
    out << "number of cols: " << ncols << endl;
    out << "                                      number of spline-coefficients:  " << QString("%1").arg(nsplcoeff, 10) << endl;
    out << "                                      number of elements:             " << QString("%1").arg(nelem, 10) << endl;
    out << "                                      number of eatom to be fitted:   " << QString("%1").arg(natfit, 10) << endl;
    out << endl
        << endl;

    out << "sigma:" << endl;
    out << "[";
    QStringList sigma_e_f_str;
    for (int i = 0; i < sigma_e_f.size(); ++i)
    {
        sigma_e_f_str << QString::number(sigma_e_f[i], 'E', 6);
    }
    out << sigma_e_f_str.join(", ") << " ]";
    out << endl
        << endl;

    out << "sigma truncated to first             " << trunc << " values" << endl;

    out << "condition number:                    " << cond1 << endl;
    out << "condition number of truncated sigma: " << cond2 << endl;
    out << endl;

    out << "_____________________________________________________________________________________" << endl
        << endl;

    if (std::isnan(cond2))
    {
        out << "ERROR!" << endl
            << "The grid points are not feasible." << endl;
        return;
    }

    int ires = 0;
    out << "residual:" << endl
        << endl;

    out << "    component          MSE       MUE       RMS        in kcal/mol:  MSE    MUE    RMS" << endl
        << endl
        << fixed;
    if (neeq > 0)
    {
        out << "    energies        "
            << QString("%1").arg(reseS / static_cast<double>(neeq), 12, 'f', 8)
            << QString("%1").arg(reseU / static_cast<double>(neeq), 12, 'f', 8)
            << QString("%1").arg(sqrt(rese2 / static_cast<double>(neeq)), 12, 'f', 8)
            << "              "
            << QString("%1").arg(reseS / static_cast<double>(neeq) * H_kcal, 7, 'f', 1)
            << QString("%1").arg(reseU / static_cast<double>(neeq) * H_kcal, 7, 'f', 1)
            << QString("%1").arg(sqrt(rese2 / static_cast<double>(neeq)) * H_kcal, 7, 'f', 1) << endl;
    }
    if (nfeq > 0)
    {
        out << "    forces          "
            << QString("%1").arg(resfS / static_cast<double>(nfeq), 12, 'f', 8)
            << QString("%1").arg(resfU / static_cast<double>(nfeq), 12, 'f', 8)
            << QString("%1").arg(sqrt(resf2 / static_cast<double>(nfeq)), 12, 'f', 8) << endl;
    }
    if (naddeq > 0)
    {
        out << "    additional eq   "
            << QString("%1").arg(resaddS / static_cast<double>(naddeq), 12, 'f', 8)
            << QString("%1").arg(resaddU / static_cast<double>(naddeq), 12, 'f', 8)
            << QString("%1").arg(sqrt(resadd2 / static_cast<double>(naddeq)), 12, 'f', 8) << endl;
    }
    if (nreaeq > 0)
    {
        out << "    reactions       "
            << QString("%1").arg(resreaS / static_cast<double>(nreaeq), 12, 'f', 8)
            << QString("%1").arg(resreaU / static_cast<double>(nreaeq), 12, 'f', 8)
            << QString("%1").arg(sqrt(resrea2 / static_cast<double>(nreaeq)), 12, 'f', 8)
            << "              "
            << QString("%1").arg(resreaS / static_cast<double>(nreaeq) * H_kcal, 7, 'f', 1)
            << QString("%1").arg(resreaU / static_cast<double>(nreaeq) * H_kcal, 7, 'f', 1)
            << QString("%1").arg(sqrt(resrea2 / static_cast<double>(nreaeq)) * H_kcal, 7, 'f', 1) << endl;
    }
    if (nsmootheq > 0)
    {
        out << "    smoothing       "
            << QString("%1").arg(ressmoothS / static_cast<double>(nsmootheq), 12, 'f', 8)
            << QString("%1").arg(ressmoothU / static_cast<double>(nsmootheq), 12, 'f', 8)
            << QString("%1").arg(sqrt(ressmooth2 / static_cast<double>(nsmootheq)), 12, 'f', 8) << endl;
    }
    out << "    ----------------" << endl;
    out << "    total           "
        << QString("%1").arg(restotS / static_cast<double>(nrows - nspleq), 12, 'f', 8)
        << QString("%1").arg(restotU / static_cast<double>(nrows - nspleq), 12, 'f', 8)
        << QString("%1").arg(sqrt(restot2 / static_cast<double>(nrows - nspleq)), 12, 'f', 8) << endl
        << endl
        << endl;

    ires = nspleq;
    if (neeq > 0)
    {
        out << "energy equations (residual in kcal/mol)" << endl;
    }

    for (int i = 0; i < energyInfos.size(); ++i)
    {
        if (energyInfos[i].weight > 1.0e-4)
        {
            out << "  " << QString("%1").arg(vres[ires] * H_kcal, 6, 'f', 3) << "   " << vmol[energyInfos[i].index].name << endl;
            ires++;
        }
    }
    if (nfeq > 0)
    {
        out << endl
            << "force equations (residual in atomic units)" << endl;
    }

    for (int i = 0; i < forceInfos.size(); ++i)
    {
        if (forceInfos[i].weight > 1.0e-4)
        {
            int imol = forceInfos[i].index;
            out << "  " << vmol[imol].name << endl;

            if (vmol[imol].m_system.geometry.isPBC)
            {
                for (int j = 0; j < 3; j++)
                {
                    out << "    "
                        << QString("%1").arg(vres[ires], 10, 'f', 6)
                        << QString("%1").arg(vres[ires + 1], 10, 'f', 6)
                        << QString("%1").arg(vres[ires + 2], 10, 'f', 6) << endl;
                    ires += 3;
                }
            }
            else
            {
                for (int j = 0; j < vmol[imol].natom; j++)
                {
                    out << "    " << vmol[imol].atomname[j]
                        << QString("%1").arg(vres[ires], 10, 'f', 6)
                        << QString("%1").arg(vres[ires + 1], 10, 'f', 6)
                        << QString("%1").arg(vres[ires + 2], 10, 'f', 6) << endl;
                    ires += 3;
                }
            }
        }
    }
    if (naddeq > 0)
    {
        out << endl
            << "additional equations (residual in atomic units)" << endl;
    }
    for (int i = 0; i < naddeq; i++)
    {
        out << "  " << QString("%1").arg(vres[ires], 10, 'f', 6) << "  " << vadd[i].potname << endl;
        ires++;
    }
    if (nreaeq > 0)
    {
        out << endl
            << "reaction equations (residual in kcal/mol)" << endl;
    }
    for (int i = 0; i < nreaeq; i++)
    {
        out << "  " << QString("%1").arg(vres[ires] * H_kcal, 6, 'f', 3) << "  " << vrea[i].reastr << endl;
        ires++;
    }

    if (nreaeq > 0)
    {
        out << endl;
    }

    out << "_____________________________________________________________________________________" << endl
        << endl;

    if (natfit > 0)
    {
        out << "Atom  Energy(Hartree) " << endl;
    }
    else
    {
        out << "Atomic energies were not fitted." << endl;
    }
    for (int i = 0; i < natfit; i++)
    {
        out << velem[nelem - natfit + i] << "    " << QString("%1").arg(vunknown[vunknown.size() - natfit + i], 0, 'f', 12) << endl;
    }

    out << "_____________________________________________________________________________________" << endl
        << endl;

    int icoeff = 0;
    out << "calculated potentials: " << endl
        << endl;
    //for each potential
    for (int i = 0; i < vpot.size(); i++)
    {
        // header and first three lines
        out << vpot[i].potname << "   r1,r2,Splcoeff ( " << vpot[i].ordspl << " th order splines)" << endl;
        if (vpot[i].ordspl == 4)
        {
            out << "Spline4" << endl;
        }
        else
        {
            out << "Spline" << endl;
        }
        out << vpot[i].nknots - 1 << "  " << QString("%1").arg(vpot[i].vr[vpot[i].nknots - 1], 0, 'f', 6) << endl;
        out << QString("%1").arg(vpot[i].expA, 0, 'E', 15) << QString("%1").arg(vpot[i].expB, 23, 'E', 15) << QString("%1").arg(vpot[i].expC, 24, 'E', 15) << endl;
        // r1, r2, a, b, c, d, e
        for (int j = 0; j < vpot[i].nknots - 1; j++)
        {
            out << QString("%1").arg(vpot[i].vr[j], -10, 'f', 6) << "  " << QString("%1").arg(vpot[i].vr[j + 1], -10, 'f', 6) << "   ";
            for (int k = 0; k <= vpot[i].ordspl; k++)
            {
                out << QString("%1").arg(vunknown[icoeff], 23, 'E', 15) << " ";
                icoeff++;
            }
            out << endl;
        }
        out << endl;
    }

    if (vexclpot.size() > 0)
    {
        out << "WARNING: The following potentials were not asked to optimize in the input! " << endl;
        out << "         Repulsive energy and forces resulting from these potentials are calculated from potentials"
            << endl
            << endl;
        out << "         potential, number of tried access (energy and force eq.), minimum distance (A, a.u.) in molecule" << endl
            << endl;
        for (int i = 0; i < vexclpot.size(); i++)
        {
            out << "         " << vexclpot[i].potname << "  " << vexclpot[i].ndist << "  " << vexclpot[i].mindist * Bohr_AA
                << "  " << vexclpot[i].mindist << "  " << vexclpot[i].molname << endl;
        }
        out << endl
            << endl;
    }

    if (nzl > 0)
    {
        out << "WARNING: There are not enough conditions to solve the equation system." << endl;
        out << "         " << nzl << " zero-lines added" << endl
            << endl;
    }

    //cout << endl << endl << "** erepfit normal termination **" << endl << endl;
}

std::tuple<double, double, double> Splcoeff::getResidual()
{
    double denom = (nrows - nspleq);
    return make_tuple(restotS / denom, restotU / denom, restot2 / denom);
}

QMap<QPair<QString, QString>, Spline4> Splcoeff::getPotentials() const
{
    QMap<QPair<QString, QString>, Spline4> res;
    int icoeff = 0;
    for (int i = 0; i < vpot.size(); i++)
    {
        // header and first three lines
        Spline4 spline;
        spline.nspl = vpot[i].nknots - 1;
        spline.cutoff = vpot[i].vr[vpot[i].nknots - 1];
        spline.A = vpot[i].expA;
        spline.B = vpot[i].expB;
        spline.C = vpot[i].expC;
        //        out << vpot[i].potname  << "   r1,r2,Splcoeff ( " << vpot[i].ordspl << " th order splines)"  << endl;
        for (int j = 0; j < vpot[i].nknots - 1; j++)
        {
            spline.xstarts.push_back(vpot[i].vr[j]);
            std::array<double, 5> coeffarray;
            for (int k = 0; k <= vpot[i].ordspl; k++)
            {
                coeffarray[k] = vunknown[icoeff];
                icoeff++;
            }
            spline.splcoeffs.push_back(coeffarray);
        }
        res.insert(qMakePair(vpot[i].at1name, vpot[i].at2name), spline);
    }
    return res;
}

QMap<QString, double> Splcoeff::getFittedAtomEnergies()
{
    QMap<QString, double> res;
    for (int i = 0; i < natfit; i++)
    {
        res.insert((velem[nelem - natfit + i]), vunknown[vunknown.size() - natfit + i]);
    }
    return res;
}

double Splcoeff::myfac(const int a, const int b)
{
    // returns coefficient z=a*(a-1)*(a-2)*... for the b'th derivative
    // (x^a)^[b] = a*(x^{a-1})^[b-1] = ... = z*x^{a-b}
    if (b == 0)
        return 1;
    else if (b > a)
        return 0;
    else
        return a * myfac(a - 1, b - 1);
}

void Splcoeff::add_energy_lhs(const int ieq, const Molecule& mol, const double coeff)
{
    if (mol.m_system.geometry.isPBC)
    {
        auto keys = mol.dist_new.getKeys();
        for (const auto& key : keys)
        {
            int sp1 = key.first;
            int sp2 = key.second;

            const auto& dist_items = mol.dist_new.getDist(sp1, sp2);
            for (const auto& dis_half : dist_items)
            {

                Colind i = findcolind(mol.elemname[sp1], mol.elemname[sp2], std::get<0>(dis_half), mol.name, sp1, sp2);
                if (i.e == 2)
                {
                    continue; // if distance is greater than cutoff
                }
                if (i.e == 1)
                { // if pot is not selected to be optimized
                    addextef_crystal(ieq, mol.elemnameext[sp1], mol.elemnameext[sp2], std::get<0>(dis_half), std::get<1>(dis_half), 0, coeff);
                    continue;
                }
                double fac = 1.0;
                if (std::get<1>(dis_half) == 1)
                    fac = 0.5;

                for (int j = 0; j <= vpot[i.p].ordspl; j++)
                {
                    eqmat(ieq, i.c) += fac * coeff * pow(std::get<0>(dis_half) - vpot[i.p].vr[i.k - 1], j);
                    //                    if (std::get<1>(dis_half) == 1)
                    //                    {
                    //                        eqmat(ieq,i.c) += 0.5*coeff * pow(  std::get<0>(dis_half) - vpot[i.p].vr[i.k-1]  ,  j  );
                    //                    }
                    //                    else
                    //                    {
                    //                        eqmat(ieq,i.c) += coeff * pow(  std::get<0>(dis_half) - vpot[i.p].vr[i.k-1]  ,  j  );
                    //                    }
                    i.c++;
                }
            }
        }
    }
    else
    {
        for (int at1 = 0; at1 < mol.natom - 1; at1++)
        {
            for (int at2 = at1 + 1; at2 < mol.natom; at2++)
            {
                Colind i = findcolind(mol.atomname[at1], mol.atomname[at2], mol.dist(at1, at2), mol.name, at1, at2);
                if (i.e == 2)
                {
                    continue; // if distance is greater than cutoff
                }
                if (i.e == 1)
                { // if pot is not selected to be optimized
                    addextef(ieq, mol, at1, at2, 0, coeff);
                    continue;
                }
                for (int j = 0; j <= vpot[i.p].ordspl; j++)
                {
                    double dr = mol.dist(at1, at2) - vpot[i.p].vr[i.k - 1];
                    double value = coeff * pow(dr, j);
                    eqmat(ieq, i.c) += value;
                    i.c++;
                }
            }
        }
    }
}

Colind Splcoeff::findcolind(const QString& atomname1, const QString& atomname2, const double dist,
    const QString& molname, const int at1, const int at2)
{
    Colind i;
    i.c = 0;
    i.p = 0;
    i.k = 0;
    i.e = 0;
    QString potname;

    potname = findpotname(atomname1, atomname2);
    for (i.p = 0; i.p < vpot.size(); i.p++)
    {
        if (vpot[i.p].potname == potname)
            break;
        i.c += (vpot[i.p].nknots - 1) * (vpot[i.p].ordspl + 1);
    }
    if (i.p >= vpot.size())
    { // if potential is not selected to be optimized
        excludepot(potname, dist, molname);
        i.e = 1;
    }
    else
    {
        for (i.k = 0; i.k < vpot[i.p].vr.size() && dist >= vpot[i.p].vr[i.k]; i.k++)
        {
        }
        if (i.k == 0)
        {
            QByteArray array;
            QTextStream stream(&array);
            qCritical() << endl
                        << "ERROR: " << endl
                        << endl
                        << atomname1 << at1 + 1
                        << atomname2 << at2 + 1 << "    " << dist * Bohr_AA << " AA   "
                        << molname << endl
                        << "This distance in this molecule is smaller than the value "
                        << "of the first knot of the respective potential. " << endl
                        << "Solution: rerun with first knot shifted to a smaller value"
                        << endl
                        << "exit erepfit" << endl
                        << endl;
            stream << "ERROR: " << endl
                   << endl
                   << atomname1 << at1 + 1
                   << atomname2 << at2 + 1 << "    " << dist * Bohr_AA << " AA   "
                   << molname << endl
                   << "This distance in this molecule is smaller than the value "
                   << "of the first knot of the respective potential. " << endl
                   << "Solution: rerun with first knot shifted to a smaller value"
                   << endl
                   << "exit erepfit" << endl
                   << endl;
            throw std::runtime_error(QString(array).toStdString());
        }
        if (i.k >= vpot[i.p].vr.size())
        {
            i.e = 2; // if distance is greater than cutoff
        }
        else
        {
            i.c += (i.k - 1) * (vpot[i.p].ordspl + 1);
        }
    }

    return i;
}

QString Splcoeff::findpotname(const QString& at1, const QString& at2) const
{
    // returns potname if it exists otherwise returns a string = at1+at2
    QString potname, at1at2, at2at1;
    bool potEXIST = false;

    at1at2 = ADPT::PotentialName(at1, at2).toString();
    at2at1 = ADPT::PotentialName(at2, at1).toString();
    for (int i = 0; i < vpot.size(); i++)
    {
        if (vpot[i].potname == at1at2)
        {
            potEXIST = true;
            potname = at1at2;
            break;
        }
        if (vpot[i].potname == at2at1)
        {
            potEXIST = true;
            potname = at2at1;
            break;
        }
    }
    if (potEXIST)
    {
        return potname;
    }
    else
    {
        return at1at2;
    }
}

void Splcoeff::excludepot(const QString& potname, const double dist, const QString& molname)
{
    // keeps track of excluded potentials due to double occurence e.g. c_h_ and h_c_
    bool newExclPot = true;
    int ipot;
    int size = vexclpot.size();

    for (ipot = 0; ipot < size; ipot++)
    {
        if (potname == vexclpot[ipot].potname)
        {
            newExclPot = false;
            break;
        }
    }
    if (newExclPot)
    {
        vexclpot.resize(size + 1);
        vexclpot[size].potname = potname;
        vexclpot[size].ndist = 1;
        vexclpot[size].mindist = dist;
        vexclpot[size].molname = molname;
    }
    else
    {
        vexclpot[ipot].ndist++;
        if (dist < vexclpot[ipot].mindist)
        {
            vexclpot[ipot].mindist = dist;
            vexclpot[ipot].molname = molname;
        }
    }
}

void Splcoeff::ifnotfile(const QString& filename) const
{
    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qCritical() << endl
                    << "ERROR: " << endl
                    << endl;
        qCritical() << filename << ": file not found"
                    << endl
                    << "exit erepfit" << endl
                    << endl;
        exit(1);
    }
    file.close();
}

void Splcoeff::addextef_crystal(const int ieq, const QString& extname1, const QString& extname2,
    double dist, int half, const int ef, const double coeff)
{

    QString potential_name = QString("%1-%2").arg(extname1).arg(extname2);

    // change this
    if (m_externalRepulsivePotentials.contains(potential_name))
    {

        QSharedPointer<RepulsivePotential> rep = m_externalRepulsivePotentials[potential_name];

        if (ef == 0)
        {
            if (half == 1)
            {
                vref[ieq] -= 0.5 * half * coeff * rep->eval(dist, 0);
            }
            else
            {
                vref[ieq] -= coeff * rep->eval(dist, 0);
            }
        }
        else if (ef == 1)
        {
            std::cerr << ("ef = 1 in addextef_crystal not support") << endl;
        }
    }
    else
    {
        std::cerr << ("Reference repulsive for " + extname1.toStdString()
                         + " - " + extname2.toStdString() + " not found")
                  << endl;
        throw std::runtime_error("Reference repulsive for " + extname1.toStdString()
            + " - " + extname2.toStdString() + " not found");
    }
}

void Splcoeff::addextef(const int ieq, const Molecule& mol, const int at1,
    const int at2, const int ef, const double coeff)
{

    // writes energy or force contributions from external potentials to vref[ieq]
    // external potentials mean potentials which are not subject to optimize!

    QList<QPair<double, bool>> dists;

    double dist;
    if (mol.m_system.geometry.isPBC)
    {
        //        qDebug() << "Crystal external repulsive";

        QList<SystemDistMatrix::item_type> l_dists;
        if (at1 > at2)
        {
            l_dists = mol.dist_new.getDist(at2, at1);
        }
        else
        {
            l_dists = mol.dist_new.getDist(at1, at2);
        }

        for (const auto& item : l_dists)
        {
            dists.append(qMakePair(std::get<0>(item), std::get<1>(item)));
        }
    }
    else
    {
        if (at1 > at2)
        {
            dist = mol.dist(at2, at1);
        }
        else
        {
            dist = mol.dist(at1, at2);
        }
        if (dist < 0.01)
        {
            return;
        }
        dists.append(qMakePair(dist, false));
    }

    // find external potential file
    int ind1 = mol.atomindex[at1];
    int ind2 = mol.atomindex[at2];
    //int iext = ind1*mol.nelem + ind2 + 3;

    QString elem1 = mol.elemnameext[ind1];
    QString elem2 = mol.elemnameext[ind2];

    QString potential_name = QString("%1-%2").arg(elem1).arg(elem2);

    // change this
    if (m_externalRepulsivePotentials.contains(potential_name))
    {

        QSharedPointer<RepulsivePotential> rep = m_externalRepulsivePotentials[potential_name];

        if (ef == 0)
        {
            for (int i = 0; i < dists.size(); ++i)
            {
                dist = dists[i].first;
                if (dists[i].second)
                {
                    vref[ieq] -= 0.5 * coeff * rep->eval(dist, 0);
                }
                else
                {
                    vref[ieq] -= coeff * rep->eval(dist, 0);
                }
                qDebug() << potential_name << dist << rep->eval(dist, 0);
            }
        }
        else if (ef == 1)
        {
            for (int i = 0; i < dists.size(); ++i)
            {
                dist = dists[i].first;
                double half_coeff = 1.0;
                if (dists[i].second)
                    half_coeff = 0.5;
                for (int xyz = 0; xyz < 3; xyz++)
                {
                    double x1 = mol.m_system.geometry.coordinates[at1].coord[xyz] * AA_Bohr;
                    double x2 = mol.m_system.geometry.coordinates[at2].coord[xyz] * AA_Bohr;
                    double fac = (x1 - x2) / dist;
                    vref[ieq + xyz] -= half_coeff * coeff * -fac * rep->eval(dist, 1);
                }
            }
        }
    }
    else
    {
        std::cerr << ("Reference repulsive for " + elem1.toStdString()
                         + " - " + elem2.toStdString() + " not found")
                  << endl;
        throw std::runtime_error("Reference repulsive for " + elem1.toStdString()
            + " - " + elem2.toStdString() + " not found");
    }
}

QString Splcoeff::getLastError() const
{
    return m_lastError;
}
bool Splcoeff::getAutoRemoved() const
{
    return autoRemoved;
}

void Splcoeff::setAutoRemoved(bool value)
{
    autoRemoved = value;
}
}
