#include "RepulsivePotential.h"


#include <QFile>
#include <QTextStream>
#include <QString>
#include <QDebug>

#include <stdexcept>
#include <exception>


RepulsivePotential::RepulsivePotential()
{

}

void RepulsivePotential::addSpline(const Spline &spline)
{
    m_splines.append(spline);
}

void RepulsivePotential::clearSplines()
{
    m_splines.clear();
}

double RepulsivePotential::eval(double r, int derivs)
{
    if (m_splines.isEmpty() || r < m_splines.first().getX1())
    {
        if (derivs == 0)
        {
            return (std::exp(-expA*r+expB)+expC);
        }
        else
        {
            return std::pow(-1, derivs)*expA*std::exp(-expA*r+expB);
        }
    }

    if (r >= m_splines.last().getX2())
        return 0.0;


    double res = 0.0;
    bool isEvaled = false;
    for(int i=m_splines.size()-1; i>=0; --i)
    {
        if (r >= m_splines[i].getX1())
        {
            res = m_splines[i].eval(r, derivs);
            isEvaled = true;
            break;
        }
    }
    if (!isEvaled)
    {
        throw std::runtime_error("wrong spline distance");
    }
    return res;
}

void RepulsivePotential::loadfile(const QString& potname, const QString &filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qCritical() << "Cannot open file:" << qPrintable(filename) << "for the reference repulsive potential:" << potname ;
    }

    QTextStream fin(&file);

    // find spline and calculate erep

    QString str;
    while (!fin.atEnd())
    {
        str = fin.readLine().trimmed();
        if (str == "Spline")
        {
            m_order=3;
            break;
        }
        if (str == "Spline4") {
            m_order=4;
            break;
        }
    }
    int nspl;
    double cutoff;
    fin >> nspl >> cutoff;
    fin >> expA >> expB >> expC;
    if ( m_order == 3 )
    {		// traditional 3rd order spline representation

        double r1, r2, a, b, c, d, e, f;
        for(int i=0; i<nspl-1; ++i)
        {

            fin >> r1 >> r2 >> a >> b >> c >> d;
            Spline spl(m_order, r1, r2);
            Eigen::VectorXd coeff(4);
            coeff(0) = a;
            coeff(1) = b;
            coeff(2) = c;
            coeff(3) = d;
            spl.setCoeffs(coeff);
            this->addSpline(spl);
        }
        fin >> r1 >> r2 >> a >> b >> c >> d >> e >> f;
        Spline spl(5, r1, r2);
        Eigen::VectorXd coeff(6);
        coeff(0) = a;
        coeff(1) = b;
        coeff(2) = c;
        coeff(3) = d;
        coeff(4) = e;
        coeff(5) = f;
        spl.setCoeffs(coeff);
        this->addSpline(spl);
    }
    else
    {
        // if ordspl ==4	// 4th order representation
        double r1, r2, a, b, c, d, e;
        for(int i=0; i<nspl; ++i)
        {

            fin >> r1 >> r2 >> a >> b >> c >> d >> e;
            Spline spl(m_order, r1, r2);
            Eigen::VectorXd coeff(5);
            coeff(0) = a;
            coeff(1) = b;
            coeff(2) = c;
            coeff(3) = d;
            coeff(4) = e;
            spl.setCoeffs(coeff);
            this->addSpline(spl);
        }
    }
    fin.flush();
    file.close();
}


Spline::Spline(int order, double start, double end) : m_order(order), m_x1(start) , m_x2(end)
{
    m_coeffs.resize(order+1);
}

Eigen::VectorXd Spline::coeffs() const
{
    return m_coeffs;
}

void Spline::setCoeffs(const Eigen::VectorXd &coeffs)
{
    assert(coeffs.size() == m_order+1);
    m_coeffs = coeffs;
}

double Spline::eval(double r, int derivs) const
{
    assert(r>=m_x1);
    assert(r<=m_x2);

    double dx = r-m_x1;
    auto dxs = makeSplineValues(dx, derivs);
    double res = dxs.dot(m_coeffs);
    return res;
}

bool Spline::inRange(double r) const
{
    if (r >=m_x1 && r<=m_x2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

double Spline::getX1() const
{
    return m_x1;
}

double Spline::getX2() const
{
    return m_x2;
}

double Spline::OrderDerivs(int order, int derivs) const
{
    double res = 1.0;
    int l_order = order;
    for(int i=0; i<derivs; ++i)
    {
        res *= static_cast<double>(l_order);
        if (l_order == 0)
        {
            break;
        }
        l_order -= 1;
    }
    return res;

}

Eigen::VectorXd Spline::makeSplineValues(double delta_x, int derivs) const
{
    Eigen::VectorXd rVector(m_order+1);
    for(int i=0; i<=m_order;++i)
    {
        double coeff = std::pow(delta_x, i-derivs);
        if (i<=derivs)
            coeff = 1.0;
        rVector(i) = coeff*OrderDerivs(i,derivs);
    }
    return rVector;
}
