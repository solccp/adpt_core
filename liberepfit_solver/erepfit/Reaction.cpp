#include "Reaction.hpp"
#include "utils.h"
#include <QStringList>

void Erepfit::Reaction::init(const QString &reastr_, const QVector<Erepfit::Molecule> &vreamol)
{
    QString readstr = reastr_;

    QTextStream ss(&readstr, QIODevice::ReadOnly | QIODevice::Text);


    QString   	str;
    int 		nreamol;
    int ind;

    reastr = reastr_;

    // use chemical notation for stochiometric factor!  3 c2h2 - 1 c6h6 -> rea_energy
    // => coeff(c2h2)=-3    coeff(c6h6)=+1
    for (nreamol=0;  ; nreamol++)
    {
        ss >> str;
        if (str == "->")
        {
            break;
        }

        double cof;
        bool ok;
        cof = str.toDouble(&ok);
        vcoeff.push_back(-cof);
        ss >> str;
        vabbr.push_back(str);

        for (ind=0; ind < vreamol.size(); ind++)
        {
            if (vabbr[nreamol] == vreamol[ind].abbr )
            {
                break;
            }
        }
        if ( ind == vreamol.size() )
        {
            qDebug()  << endl << "ERROR: " << endl << endl
                  << "Molecule \"" << vabbr[nreamol] << "\" used in the reaction section needs a definition!"
                  << endl << "exit erepfit" << endl << endl;
            exit(1);
        }
        vmol.push_back(ind);
    }
    nreamol++;
    ss >> reaE >> reaweight;
    reaE = reaE/H_kcal;
}

void Erepfit::Reaction::addReactant(double coeff, int index, const QString &name)
{
    vabbr.append(name);
    vcoeff.append(-1.0*std::abs(coeff));
    vmol.append(index);
}

void Erepfit::Reaction::addProduct(double coeff, int index, const QString &name)
{
    vabbr.append(name);
    vcoeff.append(std::abs(coeff));
    vmol.append(index);
}

void Erepfit::Reaction::setEnergyAndWeight(double energy, double weight)
{
    reaE =  energy;
    reaweight = weight;
}

void Erepfit::Reaction::buildReactionString()
{
    QStringList moles;
    for(int i=0; i<vcoeff.size();++i)
    {
        QString text;
        text.sprintf("%+lf", -vcoeff[i]);
        moles.append(QString("%1 %2").arg(text).arg(vabbr[i]));
    }
    reastr = QString("%1 -> %2 %3").arg(moles.join(" ")).arg(reaE*H_kcal, 0, 'f', 6).arg(reaweight);
    if (reastr.at(0) == '+')
    {
        reastr.remove(0, 1);
    }
}
