#ifndef SYSTEMDISTMATRIX_H
#define SYSTEMDISTMATRIX_H

#include <QMap>
#include <QList>
#include <QPair>
#include <algorithm>
#include <QTextStream>
#include <tuple>

#include "types.h"


class SystemDistMatrix
{

public:
    using item_type = std::tuple<int, int, double, bool, dVector3, dVector3> ;
    void addDist(int elem1, int elem2, int atom1, int atom2, double value, bool half, const dVector3& coord1, const dVector3& coord2 );
    QList<item_type> getDist(int elem1, int elem2) const;


    void sort();

    void print();
    QList<QPair<int, int> > getKeys() const;
    void clear()
    {
        m_dis.clear();
    }

private:
    QMap<QPair<int, int>, QList<item_type> > m_dis;
};

#endif // SYSTEMDISTMATRIX_H

