#include "Molecule.hpp"
#include <QDir>
#include <QString>
#include <QByteArray>
#include <QRegularExpression>
#include <QTemporaryDir>

#include "Splcoeff.h"

#include <QFileInfo>
#include <QProcess>


#include "utils.h"

namespace Erepfit
{
bool Molecule::init(Erepfit2::Input::System system, const QString &abbr_)
{
    m_system = system;
    name    = system.name;
    abbr    = abbr_;

    elemnameext = system.geometry.getElements();
    nelem   = elemnameext.size();
    natom   = system.geometry.coordinates.size();

    for(auto elem : elemnameext)
    {
        elemname.append(elem);
    }

    for(int i=0; i<natom; ++i)
    {
        atomname.append(system.geometry.coordinates[i].symbol);
        int index = elemnameext.indexOf(system.geometry.coordinates[i].symbol);
        atomindex.append(index);
    }


    // size force matrices and initialize to 0
    fel.resize(natom,3);
    ftotal.resize(natom,3);
    frep.resize(natom,3);
    for (int i=0;i<natom;i++)
    {
        for (int j=0;j<3;j++)
        {
            fel(i,j)=0;
            ftotal(i,j)=0;
            frep(i,j)=0;
        }
    }

    eel = system.elec_data.energy;
    ebindmel = -eel;
    for(int i=0; i<system.elec_data.force.size();++i)
    {
        for (int j=0;j<3;j++)
        {
            fel(i,j) = system.elec_data.force[i][j];
            frep(i,j)=ftotal(i,j)-fel(i,j);
        }
    }




    // setup distance matrix (upper triangular)
    dist.resize(natom,natom);
    dist.setZero();

    buildDistanceMatrix();
    return true;
}

bool Molecule::loadTotalForce(const QString &forceinputfile)
{
    QFileInfo finfo(forceinputfile);
    if (!finfo.exists() || !finfo.isReadable())
    {
        return false;
    }
    MatrixX3d temp;
    temp.resize(natom, 3);
    QFile file(forceinputfile);
    int nat = 0;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream fin(&file);
        for (int iat=0; iat<natom ; iat++)
        {
            if (fin.atEnd())
                break;
            fin >> temp(iat,0) >> temp(iat,1) >> temp(iat,2);
            ++nat;
        }
    }
    else
    {
        return false;
    }

    if (nat != natom)
    {
        return false;
    }

    ftotal = temp;

    for (int i=0; i<natom ; i++)
    {
        for (int j=0;j<3;j++)
        {
            frep(i,j)=ftotal(i,j)-fel(i,j);
        }
    }

    return true;
}

bool Molecule::buildDistanceMatrix()
{
    //PBC supercell
    if (m_system.geometry.isPBC)
    {
        dist_new.clear();
        const int max_n_cell = 3;
        const double max_pair_distance = 10.0;
        int nat = this->natom;

        for(int ic=-max_n_cell; ic<=max_n_cell; ++ic)
        {
            for(int jc=-max_n_cell; jc<=max_n_cell; ++jc)
            {
                for(int kc=-max_n_cell; kc<=max_n_cell; ++kc)
                {
                    for(int at1 = 0; at1<nat; ++at1)
                    {                        
                        dVector3 at1_coords_v = m_system.geometry.getCoordinate(at1);
                        for(int at2 = at1; at2<nat; ++at2)
                        {
                            if (ic == 0 && jc == 0 && kc == 0 && at1 == at2)
                            {
                                continue;
                            }
                            dVector3 at2_coords = m_system.geometry.getCoordinate(at2);;
                            double dis = 0.0;
                            for (int i=0; i<3; ++i)
                            {
                                at2_coords[i] += ic*m_system.geometry.lattice_vectors[0][i]*m_system.geometry.scaling_factor;
                                at2_coords[i] += jc*m_system.geometry.lattice_vectors[1][i]*m_system.geometry.scaling_factor;
                                at2_coords[i] += kc*m_system.geometry.lattice_vectors[2][i]*m_system.geometry.scaling_factor;
                                dis += std::pow(at2_coords[i]-at1_coords_v[i], 2.0);
                            }
                            dis = std::sqrt(dis);
                            if (dis > max_pair_distance)
                            {
                                continue;
                            }
                            else if (dis < 0.1)
                            {
                                continue;
                            }

                            dis = dis*AA_Bohr;

                            if (at1 == at2)
                            {
                                dist_new.addDist(atomindex[at1], atomindex[at2], at1, at2, dis, 1, at1_coords_v, at2_coords);
                            }
                            else
                            {
                                dist_new.addDist(atomindex[at1], atomindex[at2], at1, at2, dis, 0, at1_coords_v, at2_coords);
                            }
                        }
                    }
                }
            }
        }

        dist_new.sort();
    }
    else
    {
        double hlp;
        for (int at1=0; at1 < natom-1; at1++)
        {
            for (int at2=at1+1; at2 < natom; at2++)
            {
                hlp = 0;
                for (int i=0; i<3; i++)
                {
                    double r = m_system.geometry.coordinates[at1].coord[i] - m_system.geometry.coordinates[at2].coord[i];
                    hlp += r*r;
                }
                dist(at1,at2) = sqrt(hlp)*AA_Bohr;
                dist(at2,at1) = dist(at1,at2);
            }
        }
    }
    return true;
}


}
