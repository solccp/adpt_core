#include "systemdistmatrix.h"

struct pairSort
{
    int index=0;
    pairSort()
    {

    }

    bool operator()(const SystemDistMatrix::item_type& v1, const SystemDistMatrix::item_type& v2 )
    {
         if (index == 1)
         {
             return std::get<1>(v1) < std::get<1>(v2);
         }
         else
         {
             return std::get<0>(v1) < std::get<0>(v2);
         }
    }
};

void SystemDistMatrix::addDist(int elem1, int elem2, int atom1, int atom2, double value, bool half, const dVector3 &coord1, const dVector3 &coord2)
{
    auto pair = qMakePair<int, int>(elem1, elem2);
    if (elem1 > elem2)
    {
        pair = qMakePair<int, int>(elem2, elem1);
    }
    if (m_dis.contains(pair))
    {
        m_dis[pair].append(std::make_tuple(atom1, atom2, value, half, coord1, coord2));
    }
    else
    {
        QList<item_type> dis;
        dis.append(std::make_tuple(atom1, atom2, value, half, coord1, coord2));
        m_dis[pair] = dis;
    }
}

QList<SystemDistMatrix::item_type> SystemDistMatrix::getDist(int elem1, int elem2) const
{
    auto pair = qMakePair<int, int>(elem1, elem2);
    if (elem1>elem2)
    {
        pair = qMakePair<int, int>(elem2, elem1);
    }
    return m_dis[pair];
}

void SystemDistMatrix::sort()
{
    QMutableMapIterator<QPair<int, int>, QList<item_type>>  iter(m_dis);
    while(iter.hasNext())
    {
        iter.next();
        auto& diss = iter.value();
        std::sort(diss.begin(), diss.end(), pairSort());
    }
}

void SystemDistMatrix::print()
{
    QTextStream stream(stdout);

    QMapIterator<QPair<int, int>, QList<item_type> > iter(m_dis);

    while(iter.hasNext())
    {
        iter.next();
        stream << iter.key().first << " " << iter.key().second << endl;
        auto& diss = iter.value();
        for (auto & value : diss)
        {
            stream << std::get<0>(value) << " " << std::get<1>(value) << endl;
        }
    }
    stream << endl;
}

QList<QPair<int, int> > SystemDistMatrix::getKeys() const
{
    return m_dis.keys();
}
