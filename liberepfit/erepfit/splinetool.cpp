#include <cmath>
#include "splinetool.h"
#include <QTextStream>
#include "Eigen/Core"
#include "Eigen/LU"



namespace Erepfit
{
using namespace std;
void calc3rd(MatrixX4d& coeffs, int npoints, const vector<double> &vr, const vector<double> &va,
          int derivative = 1, const double c1 = 0.0, const double cn = 0.0)
{
    int npoint = npoints;
    Eigen::VectorXd h(npoint - 1);
    Eigen::VectorXd d(npoint - 1);
    Eigen::VectorXd u(npoint);

    MatrixXXd   A(npoint-2,npoint-2);

    A.setZero(npoint-2, npoint-2);

    for(int i=0; i<npoint-1; ++i)
    {
        h[i] = vr[i+1] - vr[i];
        d[i] = (va[i+1]-va[i])/h[i];
        if (i==0)
        {
            continue;
        }
        u[i] = 6.0*(d[i]-d[i-1]);
    }

    if (derivative == 1 )
    {
        u[1] -= 3.0*(d[0]-c1);
        u[npoint-2] -= 3.0*(cn-d[npoint-2]);
        A(0,0) = 1.5*h[0] + 2.0*h[1];
        A(npoint-3, npoint-3) = 2.0*h[npoint-4] + 1.5*h[npoint-3];
    }
    else if (derivative == 2)
    {
        u[1] -= h[0]*c1;
        u[npoint-2] -= h[npoint-2]*cn;

        A(0,0) = 2.0*(h[0] + h[1]);
        A(npoint-3, npoint-3) = 2.0*(h[npoint-4] + h[npoint-3]);
    }

    A(0,1) = h[1];
    for(int i=2; i<npoint - 2; ++i)
    {
        A(i-1,i-2) = h[i-2];
        A(i-1,i-1) = 2.0*(h[i-2]+h[i-1]);
        A(i-1,i) = h[i+1];
    }
    A(npoint-3, npoint-4) = h[npoint-4];

    Eigen::FullPivLU<MatrixXXd> lu(A);
    u.segment(1,npoint-2) = lu.solve(u.segment(1,npoint-2)).eval();

    if (derivative == 1 )
    {
        u[0] = (3.0/h[0])*(d[0]-c1)-u[1]/2.0;
        u[npoint-1] = (3.0/h[npoint-2])*(cn-d[npoint-2])-u[npoint-2]/2.0;
    }
    else if (derivative == 2)
    {
        u[0] = c1;
        u[npoint-1] = cn;
    }

    coeffs.resize(npoint-1, 4);
    coeffs.setZero(npoint-1, 4);
    for(int i=0; i<npoint-1;++i)
    {
        coeffs(i,0) = va[i];
        coeffs(i,1) = d[i] - (h[i]*(2.0*u[i]+u[i+1]))/6.0;
        coeffs(i,2) = u[i]/2.0;
        coeffs(i,3) = (u[i+1]-u[i])/(6.0*h[i]);
    }
}

void Spline4ToSpline3(const Spline4& spline, QTextStream& os)
{
    vector<double> vr_neu;
    vector<double> va;
    uint	       nspl_neu=0;
    double         c1=0,cn=0;

    vr_neu.clear();

    double start = spline.xstarts[0];
    while(start <= (spline.cutoff-0.025))
    {
        vr_neu.push_back(start);
        start += 0.05;
    }
    nspl_neu = vr_neu.size();


    va.resize(nspl_neu+1);
    for (uint i=0; i < nspl_neu; i++)
    {
        va[i] = 0;
        va[i] = spline.eval(vr_neu[i]);
    }
    va[nspl_neu] = 0.0;

    c1 = spline.eval(vr_neu.front(),1);
    cn = spline.eval(spline.cutoff,1);

    OrigRepr origrepr;
    origrepr.nspl = nspl_neu;
    origrepr.cutoff = spline.cutoff;
    origrepr.xstarts = vr_neu;

    vr_neu.push_back(spline.cutoff);

    MatrixX4d coeffs;

    calc3rd(coeffs, nspl_neu+1, vr_neu, va, 1, c1, cn);

    origrepr.splcoeff3 = coeffs.block(0,0,nspl_neu-1,4);

    origrepr.A = -2.0*(origrepr.splcoeff3(0,2))/origrepr.splcoeff3(0,1);
    origrepr.C = origrepr.splcoeff3(0,1)/origrepr.A + origrepr.splcoeff3(0,0);

    if ( (origrepr.splcoeff3(0,0) - origrepr.C) < 0.0)
    {
        origrepr.A = 0.0;
        origrepr.C = origrepr.splcoeff3(0,0);
    }
    else
    {
        origrepr.B = std::log(origrepr.splcoeff3(0,0) - origrepr.C)+origrepr.A*origrepr.xstarts.front();
    }


    Eigen::Matrix<double, 6, 6> mat;
    Eigen::Matrix<double, 6, 1> vecB;
    Eigen::Matrix<double, 6, 1> vecX;
    double dx = origrepr.cutoff - origrepr.xstarts.back();

    mat.setZero(6,6);
    mat(0,0) = 1.0;
    mat(1,1) = 1.0;
    mat(2,2) = 2.0;
    mat(3,0) = 1.0;
    mat(3,1) = dx;
    mat(3,2) = dx*dx;
    mat(3,3) = mat(3,2)*dx; //dx**3
    mat(3,4) = mat(3,3)*dx; //dx**4
    mat(3,5) = mat(3,4)*dx; //dx**5

    mat(4,1) = 1.0;
    mat(4,2) = 2.0*dx;
    mat(4,3) = 3.0*mat(3,2);
    mat(4,4) = 4.0*mat(3,3);
    mat(4,5) = 5.0*mat(3,4);

    mat(5,2) = 2.0;
    mat(5,3) = 6.0*dx;
    mat(5,4) = 12.0*mat(4,3);
    mat(5,5) = 20.0*mat(4,4);

    vecB(0) = coeffs(nspl_neu-1, 0);
    vecB(1) = coeffs(nspl_neu-1, 1);
    vecB(2) = 2.0*coeffs(nspl_neu-1, 2);

    vecB(3) = coeffs(nspl_neu-1, 0)+ coeffs(nspl_neu-1, 1)*dx+coeffs(nspl_neu-1, 2)*dx*dx
            + coeffs(nspl_neu-1, 3)*dx*dx*dx;

    vecB(4) = 0.0; //spline.eval(origrepr.cutoff,1);
    vecB(5) = 0.0; //spline.eval(origrepr.cutoff,2);

    Eigen::FullPivLU<MatrixXXd> lu(mat);
    vecX = lu.solve(vecB);
    for(int i=0; i<6; ++i)
    {
        origrepr.splcoeff5[i] = vecX[i];
    }
    origrepr.writeOut(os);
}

OrigRepr::OrigRepr()
{
    nspl    = 0;
    cutoff  = 0;
    A	    = 0;
    B       = 0;
    C       = 0;

    splcoeff5.resize(6);
    for (uint i=0;i<6;i++)
    {
        splcoeff5[i] = 0;
    }
}

void OrigRepr::writeOut(QTextStream &os)
{
    os << "Spline" << endl;
    os << nspl << "  " << cutoff << endl;

    os << QString("%1 %2 %3").arg(A, 0, 'E', 15).arg(B, 0, 'E', 15).arg(C, 0, 'E', 15) << endl;

    // 3rd order splines
    for (uint i=0; i < nspl-1; i++ )
    {
        os << QString("%1 %2").arg(xstarts[i], 9,'f',6).arg(xstarts[i+1], 9, 'f', 6);
        for (uint j=0; j <= 3; j++)
        {
            os << QString("%1").arg(splcoeff3(i,j), 23, 'E', 15) << " ";
        }
        os << endl;
    }

    // 5th order spline (last)
    os << QString("%1 %2").arg(xstarts[xstarts.size()-1], 9,'f',6).arg(cutoff, 9, 'f', 6);
    for (uint i=0; i <= 5; i++)
    {
        os << QString("%1").arg(splcoeff5[i], 23, 'E', 15) << " ";
    }
    os << endl;
}

void Spline4::load(istream &in)
{
    const int ordspl = 4;
    std::string str;
    while ( getline(in,str) )
    {
        if ( str == "Spline" || str == "Spline4")
        {
            break;
        }
    }

    in >> nspl >> cutoff;
    getline(in,str);
    in >> A >> B >> C;
    getline(in,str);

    double tmpx;
    std::array<double, 5> tmpy;

    for (uint i=0; i < nspl; i++)
    {
        in >> tmpx >> str;
        for (int j=0; j <= ordspl; j++)
        {
            in >> tmpy[j];
        }
        getline(in,str);
        xstarts.push_back(tmpx);
        splcoeffs.push_back(tmpy);
    }
    //        xstarts.push_back(cutoff);
}

double Spline4::eval(double r, int deriv) const
{
    if (r < xstarts.front())
    {
        if (deriv == 0)
        {
            return std::exp(-A*r+B)+C;
        }
        else if (deriv == 1)
        {
            return -A*std::exp(B-A*r);
        }
        else if (deriv == 2)
        {
            return A*A*std::exp(B-A*r);
        }
    }
    else if (r >= cutoff)
    {
        return 0.0;
    }
    else
    {
        for(int i=xstarts.size()-1; i>=0; --i)
        {
            if (r >= xstarts[i])
            {
                double dx = (r-xstarts[i]);
                double res = 0.0;
                if (deriv == 0)
                {
                    for(int j = 0; j<5 ; ++j)
                    {
                        res += std::pow(dx, j)*splcoeffs[i][j];
                    }

                }
                else if (deriv == 1)
                {
                    for(int j = 1; j<5 ; ++j)
                    {
                        res += j*std::pow(dx, j-1)*splcoeffs[i][j];
                    }
                }
                else if (deriv == 2)
                {
                    for(int j = 2; j<5 ; ++j)
                    {
                        res += j*(j-1)*std::pow(dx, j-2)*splcoeffs[i][j];
                    }
                }
                return res;
            }
        }
    }
    return -1;
}

QString Spline4::toString() const
{
    QByteArray array;
    QTextStream stream(&array);


    stream << "Spline4" << endl
           << this->nspl << " " << this->cutoff << endl;
    stream << this->A << " " << this->B << " " << this->C << endl;
    for(int i=0; i<this->nspl; ++i)
    {
        if (i == this->nspl-1)
        {
            stream << QString("%1 %2").arg(this->xstarts[i], 0, 'f', 5).arg(this->cutoff, 0, 'f', 5);
        }
        else
        {
            stream << QString("%1 %2").arg(this->xstarts[i], 0, 'f', 5).arg(this->xstarts[i+1], 0, 'f', 5);
        }
        for(int j=0; j<5; ++j)
        {
            stream << " " << QString("%1").arg(this->splcoeffs[i][j], 20, 'E', 12);
        }
        stream << endl;
    }
    return array;
}




}

