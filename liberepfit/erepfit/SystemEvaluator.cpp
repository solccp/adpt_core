#include "SystemEvaluator.h"

#include <QTemporaryDir>

namespace Erepfit2
{

    QList<QPair<QString, QString> > getRequiredElecPairs(const Erepfit2Input::System &system) const
    {
        QStringList elemts = system.geometry.getElements();
        elemts.removeDuplicates();
        QList<QPair<QString, QString> > res;
        for(int i=0; i<elemts.size();++i)
        {
            for(int j=0; j<elemts.size();++j)
            {
                res.append(qMakePair(elemts.at(i),elemts.at(j)));
            }
        }
        return res;
    }

    SystemResults SystemEvaluator::evaluate(const Splcoeff2 *splcoeff, const Erepfit2Input::System &system, const QString &scratch_dir)
    {

            QTemporaryDir scratch_tempdir(scratch_parent+'/');
            scratch_tempdir.setAutoRemove(!splcoeff->debug);
            QDir scratch_dir(scratch_tempdir.path());


            QStringList output;

            QFile file(system.template_input);
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text) )
            {
                qCritical() << endl << "ERROR: " << endl << endl;
                qCritical() << "Cannot open template DFTB input file: " << dftbin ;
                throw std::runtime_error("Cannot open template DFTB input file: " + system.template_input.toStdString());
            }

            QTextStream fin(&file);

            QByteArray out_data;
            QTextStream final_hsd(&out_data);

            while(!fin.atEnd())
            {
                QString line;
                line = fin.readLine();
                final_hsd << line << endl;
            }
            file.close();

            final_hsd << endl;


            final_hsd << "#" << this->name << endl ;
            final_hsd << QString("!Geometry = !GenFormat{") << endl ;
            final_hsd << system.geometry.getGen();
            final_hsd << "}" << endl;


            QList<QPair<QString, QString> > pairs = getRequiredElecPairs(system);

            if (splcoeff->skfileinfo->type2Names)
            {
                final_hsd << "+Hamiltonian = +DFTB {" << endl;
                final_hsd << "  !Charge = " << charge << endl;
                if (!splcoeff->skfileinfo->maxAngularMomentum.isEmpty())
                {
                    final_hsd << "    !MaxAngularMomentum = {" << endl;
                    QMapIterator<QString, QString> it(splcoeff->skfileinfo->maxAngularMomentum);
                    while(it.hasNext())
                    {
                        it.next();
                        final_hsd << it.key() << " = " << "\"" << it.value() << "\"" << endl;
                    }
                    final_hsd << "    }" << endl;
                }

                final_hsd << "    !SlaterKosterFiles = {" << endl;
                for(int i=0; i<pairs.size();++i)
                {
                    final_hsd << writeTempElecPart(scratch_dir.absolutePath(), pairs[i].first, pairs[i].second,  splcoeff) << endl;
                }

                final_hsd << "    }" << endl;
                final_hsd << "}" << endl;
            }
            else
            {
                final_hsd << "+Hamiltonian = +DFTB {" << endl;
                final_hsd << "  !Charge = " << charge << endl;
                if (!splcoeff->skfileinfo->selectedShells.isEmpty())
                {
                    final_hsd << "    !MaxAngularMomentum = {" << endl;
                    QMapIterator<QString, QStringList> it(splcoeff->skfileinfo->selectedShells);
                    while(it.hasNext())
                    {
                        it.next();
                        final_hsd << it.key() << " = " << "SelectedShells{" << it.value().join(" ") << "}" << endl;
                    }
                    final_hsd << "    }" << endl;
                }

                final_hsd << "    !SlaterKosterFiles = {" << endl;
                for(int i=0; i<pairs.size();++i)
                {
                    final_hsd << writeTempElecPart(scratch_dir.absolutePath(), pairs[i].first, pairs[i].second,  splcoeff) << endl;
                }
                final_hsd << "    }" << endl;
                final_hsd   << "}" << endl; //Hamiltonian
            }


            final_hsd << QString("*Options = {") << endl
                      << "    !CalculateForces = Yes" << endl
                      << "    !WriteAutotestTag = Yes" << endl
                      <<   "}"  << endl;


            if (!splcoeff->m_hubbardDerivs.isEmpty())
            {
                final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                          << "    *HubbardDerivs = {" << endl;

                QMapIterator<QString, double> it(splcoeff->m_hubbardDerivs);
                while(it.hasNext())
                {
                    it.next();
                    final_hsd << "        !" << it.key() << " = " << it.value() << endl;
                }
                final_hsd << "    }"  << endl;
                final_hsd << "}"  << endl;
            }

            if (!splcoeff->dispersion.m_ljdispersion.empty())
            {
                final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                          << "    *Dispersion = *LennardJones{" << endl
                          << "      *Parameters {" << endl;
                QMapIterator<QString, LJDispersionEntry> it(splcoeff->dispersion.m_ljdispersion);
                while(it.hasNext())
                {
                    it.next();
                    final_hsd << "          *" << it.key() << " {" << endl
                              << "              *Distance [AA] = " << it.value().distance << endl
                              << "              *Energy [kcal/mol] = " << it.value().energy << endl
                              << "          }" << endl;
                }
                final_hsd << "          }" << "      }" << "}"  << endl;
            }

            if (splcoeff->useDampingFactor)
            {
                final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                          << "    !DampXH = Yes" << endl
                          << "    !DampXHExponent =" << splcoeff->dampingFactor << endl;
                final_hsd << "}" << endl;
            }


            if (PBC)
            {
                final_hsd << QString("+Hamiltonian = +DFTB{") << endl
                          << "    !KPointsAndWeights = !SupercellFolding{" << endl;
                for(int i=0; i<3;++i)
                {
                    final_hsd << "      ";
                    for(int j=0; j<3;++j)
                    {
                        if (j==i)
                        {
                            final_hsd << kpoints[j] << " ";
                        }
                        else
                        {
                            final_hsd << "0 ";
                        }
                    }
                    final_hsd << endl;
                }
                for(int i=0; i<3;++i)
                {
                    final_hsd << " " << kpoint_shifts[i] << " ";
                }
                final_hsd << endl;
                final_hsd << "    }" << endl;
                final_hsd << "}" << endl;
            }



            final_hsd << "!ParserOptions {" << endl
               << "    !IgnoreUnprocessedNodes = Yes" << endl
               << "}" << endl;

            ElectronicPartCacheDatabase &db = Singleton<ElectronicPartCacheDatabase>::Instance();
            QByteArray sha1 = QCryptographicHash::hash(out_data, QCryptographicHash::Sha1);
            SHA1Key = QString(sha1.toHex());

            if (splcoeff->use_cache && db.hasKey(SHA1Key))
            {
                QByteArray data_array = db.getValue(SHA1Key);
                QDataStream dstream(data_array);
                dstream >> *this;
            }
            else
            {

                DFTBTestSuite::writeFile(scratch_dir.absoluteFilePath("dftb_in.hsd"), QString(out_data));


                DFTBTestSuite::ExecuteCommandStream(scratch_dir.absolutePath(), dftbversion, QStringList(), output, QByteArray());

                {
                    QFile outfile(scratch_dir.absoluteFilePath("dftb.out"));
                    outfile.open(QIODevice::WriteOnly);
                    QTextStream fout(&outfile);
                    fout << output.join("\n");
                }


                int indexError = -1;
                for(indexError = output.size()-1; indexError>=0 ; --indexError)
                {
                    if (output[indexError].contains("ERROR!"))
                    {
                        break;
                    }
                }

                if ( indexError != -1)
                {
                    QString str;
                    QTextStream stream(&str);
                    stream << name << endl;
                    QStringList errMsg = output.mid(indexError);
                    stream  << errMsg.join("\n") << endl;
                    throw std::runtime_error(str.toStdString());
                }


                QFile autotest_tag(scratch_dir.absoluteFilePath("autotest.tag"));
                if (!autotest_tag.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    QString str;
                    QTextStream stream(&str);
                    stream  << autotest_tag.fileName() ;
                    stream  << endl << "ERROR: " << endl << endl;
                    stream << "The system call: \"" << dftbversion << "\" did not produce the file: \"autotest.tag\" necessary for running erepfit"
                         << endl << "calculation of electronic energy for molecule \"" << name << "\" failed."
                         << endl << "exit erepfit" << endl << endl;
                    throw std::runtime_error(str.toStdString());
                }

                QTextStream tin(&autotest_tag);
                bool foundEForce = false;
                bool foundTotalForce = false;
                bool foundRepulsiveForce = false;
                bool foundEEl = false;
                bool foundTotalEnergy = false;
                bool foundRepulsiveEnergy = false;
                double totalE = 0.0;
                double repulsiveE;
                double elecE;


                MatrixX3d f_total;
                MatrixX3d f_repulsive;

                while(!tin.atEnd())
                {
                    QString line = tin.readLine();
                    if (line.contains("electronic_forces"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                        if (dim == 2 && sizes.size() == dim)
                        {
                            int nat = sizes[1].toInt();
                            if (nat == natom)
                            {
                                for(int i=0; i<nat;++i)
                                {
                                    double x,y,z;
                                    tin >> x >> y >> z;
                                    fel(i,0) = x;
                                    fel(i,1) = y;
                                    fel(i,2) = z;
                                    //tin >> fel(i,0) >> fel(i,1) >> fel(i,2);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                        foundEForce = true;
                    }
                    else if (line.contains("forces"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                        if (dim == 2 && sizes.size() == dim)
                        {
                            int nat = sizes[1].toInt();
                            if (nat == natom)
                            {
                                f_total.resize(natom,3);
                                for(int i=0; i<nat;++i)
                                {
                                    double x,y,z;
                                    tin >> x >> y >> z;
                                    f_total(i,0) = x;
                                    f_total(i,1) = y;
                                    f_total(i,2) = z;
                                    //tin >> fel(i,0) >> fel(i,1) >> fel(i,2);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                        foundTotalForce = true;
                    }
                    else if (line.contains("repulsive_forces"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);
                        if (dim == 2 && sizes.size() == dim)
                        {
                            int nat = sizes[1].toInt();
                            if (nat == natom)
                            {
                                f_repulsive.resize(natom,3);
                                for(int i=0; i<nat;++i)
                                {
                                    double x,y,z;
                                    tin >> x >> y >> z;
                                    f_repulsive(i,0) = x;
                                    f_repulsive(i,1) = y;
                                    f_repulsive(i,2) = z;
                                    //tin >> fel(i,0) >> fel(i,1) >> fel(i,2);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                        foundRepulsiveForce = true;
                    }
                    else if (line.contains("rep_pairs"))
                    {

                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);

                        if (dim != 2 || sizes[0] != "2")
                        {
                            return false;
                        }

                        if (dim == 2 && sizes.size() == dim)
                        {
                            this->rep_pairs.clear();
                            int rows = sizes[1].toInt();
                            for(int i=0; i<rows;++i)
                            {
                                int p1, p2;
                                tin >> p1 >> p2;
                                this->rep_pairs.append(qMakePair(p1-1, p2-1));
                            }
                        }
                    }
                    else if (line.contains("rep_neighbor"))
                    {

                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);

                        if (dim != 2 || sizes[0] != "2")
                        {
                            return false;
                        }

                        if (dim == 2 && sizes.size() == dim)
                        {
                            this->rep_neighbor.clear();
                            int rows = sizes[1].toInt();
                            for(int i=0; i<rows;++i)
                            {
                                int p1, p2;
                                tin >> p1 >> p2;
                                this->rep_neighbor.append(qMakePair(p1-1, p2-1));
                            }
                        }
                    }
                    else if (line.contains("rep_type"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);

                        if (dim != 1)
                        {
                            return false;
                        }

                        if (dim == 1 && sizes.size() == dim)
                        {
                            this->rep_half.clear();
                            int rows = sizes[dim-1].toInt();
                            for(int i=0; i<rows;++i)
                            {
                                int value;
                                tin >> value;
                                this->rep_half.append(value);
                            }
                        }
                    }
                    else if (line.contains("rep_distances"))
                    {
                        QStringList arr = line.split(":", QString::SkipEmptyParts);
                        int dim = arr[2].toInt();
                        QStringList sizes = arr[3].split(",", QString::SkipEmptyParts);

                        if (dim != 1)
                        {
                            return false;
                        }

                        if (dim == 1 && sizes.size() == dim)
                        {
                            this->rep_distance.clear();
                            int rows = sizes[dim-1].toInt();
                            for(int i=0; i<rows;++i)
                            {
                                double value;
                                tin >> value;
                                this->rep_distance.append(value);
                            }
                        }
                    }
                    else if (line.contains("total_elec_energy"))
                    {
                        tin >> elecE;
                        eel = elecE;
                        foundEEl = true;
                    }
                    else if (line.contains("repulsive_energy"))
                    {
                        tin >> repulsiveE;
                        foundRepulsiveEnergy = true;
                    }
                    else if (line.contains("total_energy"))
                    {
                        tin >> totalE;
                        foundTotalEnergy = true;
                    }
                }

                if (splcoeff->UseTotalMinusRepulsive)
                {
                    if (foundTotalEnergy && foundRepulsiveEnergy)
                    {
            //            qDebug() << "UseTotalMinusRepulsive";
                        eel = totalE - repulsiveE;
                    }

                    if (foundTotalForce && foundRepulsiveForce)
                    {
                        fel = f_total - f_repulsive;
                    }
                }
                else
                {
                    if (!foundEForce || !foundEEl)
                    {
                        return false;
                    }
                }

                if (PBC &&
                        (rep_pairs.size() != rep_distance.size() ||
                         rep_pairs.size() != rep_half.size()  ||
                         rep_pairs.size() != rep_neighbor.size() )
                )
                {
                    return false;
                }
                QByteArray data_array;
                QDataStream dstream(&data_array, QIODevice::WriteOnly);
                dstream << *this;
                db.addKey(SHA1Key, data_array);
            }




            return true;



    }

}



