#ifndef SPLINETOOL_H
#define SPLINETOOL_H
#include <cmath>
//#include <iostream>
//#include <iomanip>
#include <array>
#include <QTextStream>
#include "Eigen/Core"




namespace Erepfit
{
using namespace std;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXXd;
typedef Eigen::Matrix<double, Eigen::Dynamic, 4> MatrixX4d;

class Spline4
{
public:
    uint           nspl;
    double         cutoff;
    double         A,B,C;   //exponential coefficient
    std::vector<double> xstarts;
    std::vector<std::array<double, 5> > splcoeffs;
    void load(istream& in);
    double eval(double r, int deriv = 0) const;
    QString toString() const;
};



class OrigRepr
{
    // original representation of erep as in dftb 1998
public:
    uint           nspl;
    double         cutoff;
    double         A,B,C;   //exponential coefficient
    MatrixX4d splcoeff3;
    vector<double> xstarts;
    vector<double> splcoeff5;

    OrigRepr();
    void writeOut(QTextStream& os);

};
void Spline4ToSpline3(const Spline4& spline, QTextStream &os);
}

#endif // SPLINETOOL_H
