#ifndef LIBEREPFIT_UTILS_H
#define LIBEREPFIT_UTILS_H

#include <QString>

namespace Erepfit
{

const double Bohr_AA  = 0.5291772085936 ;
const double AA_Bohr  = 1.0/Bohr_AA ;
const double H_kcal   = 627.5095469 ;

using namespace std;
double getDistanceValue(double value, const QString& unit);
//QString erepfitSymboltoSymbol(const QString& sym);
//QString symbolToErepfitSymbol(const QString& sym);

template<class Container>
int indexUUID(const Container& C, const QString& uuid)
{
    for(int i=0; i<C.size(); ++i)
    {
        if (C[i].uuid.toString() == uuid)
        {
            return i;
        }
    }
    return -1;
}

double getEnergyValue(double value, const QString& unit);

}

#endif // UTILS_H
