#include "utils.h"
#include <stdexcept>

namespace Erepfit
{


double getDistanceValue(double value, const QString &unit)
{
    if (unit.toLower() == "bohr")
    {
        return value;
    }
    else if (unit.toLower() == "aa")
    {
        return value*AA_Bohr;
    }
    else
    {
        throw std::runtime_error("unit " + unit.toStdString() + " is not defined.");
    }
}

//QString erepfitSymboltoSymbol(const QString &sym)
//{
//    QString res = sym[0].toUpper();
//    if (sym.size() > 1)
//    {
//        if (sym[1].isLetter())
//        {
//            res += sym[1];
//        }
//    }
//    return res;
//}

//QString symbolToErepfitSymbol(const QString &sym)
//{
//    QString res = sym.toLower();
//    if (res.size() == 1)
//    {
//        res += "_";
//    }
//    return res;
//}

double getEnergyValue(double value, const QString &unit)
{
    if (unit.toLower() == "kcal/mol")
    {
        return value/H_kcal;
    }
    else if (unit.toLower() == "h")
    {
        return value;
    }
    else if (unit.toLower() == "ev")
    {
        return value/27.2113845;
    }
    else
    {
        throw std::runtime_error("unit " + unit.toStdString() + " is not defined.");
    }
}

}
