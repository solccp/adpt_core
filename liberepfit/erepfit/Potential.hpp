#ifndef POTENTIAL_HPP
#define POTENTIAL_HPP

//#include<iostream>
//#include<vector>
//#include <boost/algorithm/string.hpp>

#include <QString>
#include <QList>
#include <QVector>

#include <map>

namespace Erepfit
{
using namespace std;

class FiniteNumberComparor
{
public:
    FiniteNumberComparor(double eps = 1.0e-4)
    {
        m_eps = eps;
    }

    bool operator()(double a, double b)
    {
        return a + m_eps < b;
    }

private:
    double m_eps = 1.0e-4;
};


class Potential
{
protected:
private: 
public:
    struct MolDist
    {
    public:
        QString molname;
        double dist;
    };

    QString          at1name;
    QString          at2name;
    QString          potname;
    int             ordspl;
    int             nknots;     // number of knots, cutoff included
    QList<double>  vr;
    std::map<double, std::string, FiniteNumberComparor> vmoldist;

    double          expA,expB,expC;

    void init(const QString& at1name_, const QString& at2name_, const QString& potname_,
              const QList<double>& knots_, const int ordspl);



    void insert_moldist(const QString& molname, const double dist);

    void init_vmoldist();
};

}

#endif //POTENTIAL_HPP
