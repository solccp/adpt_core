#include "Potential.hpp"

#include <QFile>
#include <QTextStream>
#include <QDebug>

#include <cmath>

void Erepfit::Potential::init(const QString &at1name_, const QString &at2name_, const QString &potname_,
                              const QList<double> &knots_, const int ordspl_)
{
    potname  = potname_;
    at1name = at1name_;
    at2name = at2name_;

    vr = knots_;
    ordspl   = ordspl_;
    nknots = vr.size();

}

void Erepfit::Potential::insert_moldist(const QString &molname, const double dist)
{
    MolDist dummymoldist;
    dummymoldist.molname = molname;
    dummymoldist.dist    = dist;

    vmoldist.insert(std::make_pair(dist, molname.toStdString()));
}

void Erepfit::Potential::init_vmoldist()
{
    vmoldist.clear();
}

