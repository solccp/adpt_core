#ifndef SPLCPEFF2_H
#define SPLCPEFF2_H

#include "erepfit2_inputdata.h"
#include <QSharedPointer>
#include "system.hpp"

namespace Erepfit2
{

class Splcoeff2
{
public:
    Splcoeff2(QSharedPointer<Erepfit2Input::Erepfit2_InputData> input_);

    ~Splcoeff2();

    bool calculate(const QString &scratch_dir, QTextStream& os);

    bool debug = false;

private:
    QSharedPointer<Erepfit2Input::Erepfit2_InputData> m_input;

    QMap<QString, QSharedPointer<Erepfit2Input::System> > m_systems;
    QMap<QString, QSharedPointer<SystemResults> > m_system_results;

    void evaluate_systems();
};

}

#endif // SPLCPEFF2_H
