#ifndef DLL_H_62B23520_7C8E_11DE_8A39_0800200C9A66
#define DLL_H_62B23520_7C8E_11DE_8A39_0800200C9A66

#if defined(_MSC_VER) || (defined(__GNUC__) && (__GNUC__ == 3 && __GNUC_MINOR__ >= 4) || (__GNUC__ >= 4)) // GCC supports "pragma once" correctly since 3.4
#pragma once
#endif

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the hsd_cpp_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// HSD_CPP_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#undef HSD_CPP_API

#ifdef HSD_CPP_DLL // Using or Building HSD-CPP DLL (definition defined manually)
	#ifdef hsd_cpp_EXPORTS // Building HSD-CPP DLL (definition created by CMake or defined manually)
	//	#pragma message( "Defining HSD_CPP_API for DLL export" )
		#define HSD_CPP_API __declspec(dllexport)
	#else // hsd_cpp_EXPORTS
	//	#pragma message( "Defining HSD_CPP_API for DLL import" )
		#define HSD_CPP_API __declspec(dllimport)
	#endif // hsd_cpp_EXPORTS
#else //HSD_CPP_DLL
#define HSD_CPP_API
#endif // HSD_CPP_DLL

#endif // DLL_H_62B23520_7C8E_11DE_8A39_0800200C9A66
