
#include "hsd-cpp/parser.h"
#include "HSDTokenizer.h"

#include <deque>
#include <stack>

#include <iostream>

#include <QDebug>

namespace HSD
{

    void restoreTokens(std::stack<HSDToken>& previousTokens, std::deque<HSDToken>& tokens)
    {
        while(!previousTokens.empty())
        {
            tokens.push_front(previousTokens.top());
            previousTokens.pop();
        }
    }

    bool parseMethod(std::deque<HSDToken>& tokens, Node& node);
    bool parseScalarList(std::deque<HSDToken>& tokens, Node& node);
    bool parseScalar(std::deque<HSDToken>& tokens, Node& node);

    bool parsePropertyList(std::deque<HSDToken>& tokens, Node& node)
    {

        HSDToken nextToken = tokens.front();

        if (nextToken.id != HSD::ID_LEFTBRACE)
        {
            return false;
        }

        std::stack<HSDToken> previousTokensAll;
        tokens.pop_front();
        previousTokensAll.push(nextToken);

        std::stack<HSDToken> previousTokens;

        while(!tokens.empty())
        {
            std::string modifier;
            nextToken = tokens.front();
            tokens.pop_front();
            previousTokensAll.push(nextToken);

            if (nextToken.id == HSD::ID_RIGHTBRACE)
            {
                //if all nodes are processed.
                if (previousTokens.empty())
                {
                    previousTokens.push(nextToken);
                    break;
                }
                else
                {
                    //some unprocessed notes;
                    restoreTokens(previousTokensAll, tokens);
                    return false;
                }
            }
            else if (nextToken.id == HSD::ID_EOL)
            {
                //do nothing and continue;
                continue;
            }
            else if (nextToken.id == HSD::ID_WORD || nextToken.id == HSD::ID_MODIFIER)
            {
                if (nextToken.id == HSD::ID_MODIFIER &&
                        (previousTokens.top().id != HSD::ID_WORD) )
                {
                    restoreTokens(previousTokensAll, tokens);
                    return false;
                }
                previousTokens.push(nextToken);
                continue;

            }
            else if (nextToken.id == HSD::ID_ASSIGN_OP)
            {

            }
            else
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }

            //nextToken is "=";

            if (previousTokens.size() > 2)
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }
            else if (previousTokens.size() == 2 && previousTokens.top().id != HSD::ID_MODIFIER)
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }
            else if (previousTokens.size() == 1 && previousTokens.top().id != HSD::ID_WORD)
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }

            //ignore modifier for now
            if (previousTokens.top().id == HSD::ID_MODIFIER)
            {
                modifier = previousTokens.top().value;
                previousTokens.pop();
            }

            std::string key = previousTokens.top().value;
            previousTokens.pop();

            Node subnode;
            bool childParsed = false;

            if (parseScalar(tokens, subnode))
            {
                node[key] = subnode;
                childParsed = true;
            }
            else if (parseScalarList(tokens, subnode))
            {
                node[key] = subnode;
                childParsed = true;
            }
            else if (parsePropertyList(tokens, subnode))
            {
                node[key] = subnode;
                childParsed = true;
            }
            else if (parseMethod(tokens, subnode))
            {
                node[key] = subnode;
                childParsed = true;
            }
            else
            {
                childParsed = false;
            }

            if (childParsed)
            {
                if (!modifier.empty())
                {
                    node[key].SetModifier(modifier);
                }
                while(!previousTokens.empty())
                {
                    previousTokens.pop();
                }
            }
        }

        if (previousTokens.size() == 1 && previousTokens.top().id == HSD::ID_RIGHTBRACE)
        {
            return true;
        }
        return false;
    }

    bool parseScalarList(std::deque<HSDToken>& tokens, Node& node)
    {
        std::stack<HSDToken> previousTokensAll;
        std::list<HSDToken> validTokens;

        bool hasBrace = false;

        if (tokens.front().id == HSD::ID_LEFTBRACE)
        {
            hasBrace = true;
            previousTokensAll.push(tokens.front());
            tokens.pop_front();
        }

        while(!tokens.empty())
        {
            HSDToken nextToken = tokens.front();
            previousTokensAll.push(nextToken);
            tokens.pop_front();

            if (nextToken.id == HSD::ID_WORD || nextToken.id == HSD::ID_QUOTESTRING)
            {
                validTokens.push_back(nextToken);
                continue;
            }
            else if (nextToken.id == HSD::ID_EOL)
            {
                if (hasBrace)
                {
                    continue;
                }
                break;
            }
            else if (nextToken.id == HSD::ID_RIGHTBRACE)
            {
                if (hasBrace)
                {
                    hasBrace = false;
                    break;
                }
                else
                {
                    restoreTokens(previousTokensAll, tokens);
                    return false;
                }
            }
            else
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }
        }
        if (validTokens.empty() || hasBrace)
        {
            restoreTokens(previousTokensAll, tokens);
            return false;
        }

        for(auto p = validTokens.begin(); p != validTokens.end(); ++p)
        {
            Node subnode;
            subnode = (*p).value;
            node.push_back(subnode);
        }
        return true;
    }

    //parse the next token as a scalar, after parsing, the next token must be EOL
    bool parseScalar(std::deque<HSDToken>& tokens, Node& node)
    {
        HSDToken nextToken = tokens.front();

        if (nextToken.id != HSD::ID_WORD && nextToken.id != HSD::ID_QUOTESTRING)
        {
            return false;
        }

        tokens.pop_front();

        if (tokens.empty())
        {
            node = nextToken.value;
            return true;
        }
        else if (tokens.front().id == HSD::ID_EOL)
        {
            node = nextToken.value;
            return true;
        }
        else
        {
            tokens.push_front(nextToken);
        }
        return false;
    }


    bool parseMethod(std::deque<HSDToken>& tokens, Node& node)
    {
        std::stack<HSDToken> previousTokensAll;
        std::stack<HSDToken> previousTokens;


        while(!tokens.empty())
        {
            HSDToken nextToken = tokens.front();
            tokens.pop_front();
            previousTokensAll.push(nextToken);


            if (nextToken.id == HSD::ID_WORD || nextToken.id == HSD::ID_QUOTESTRING )
            {
                previousTokens.push(nextToken);
                continue;
            }
            else if (nextToken.id == HSD::ID_LEFTBRACE)
            {
                break;
            }
            else if (nextToken.id == HSD::ID_EOL)
            {
                continue;
            }
            else
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }
        }

        //invalid method name
        if (previousTokens.size() > 1 || previousTokens.top().id != HSD::ID_WORD)
        {
            restoreTokens(previousTokensAll, tokens);
            return false;
        }

        //if the mothod doesn't have method name, i.e. property list
        if (previousTokens.size() == 0)
        {
            return false;
        }

        HSDToken methodname_token = previousTokens.top();
        previousTokens.pop();
        node.SetMethodName(methodname_token.value);

        tokens.push_front(previousTokensAll.top());
        previousTokensAll.pop();

        //try parse method as scalar list
        bool succ = parseScalarList(tokens, node);
        if (!succ)
        {
            //if not, then try to parse as property list
            succ = parsePropertyList(tokens, node);
            if (!succ)
            {
                restoreTokens(previousTokensAll, tokens);
                return false;
            }
        }
        return true;
    }


    bool parseHSD(std::deque<HSDToken>& tokens, Node& node)
    {

        std::stack<HSDToken> previousTokens;
        std::string modifiles;

        while(!tokens.empty())
        {

            HSDToken nextToken = tokens.front();
            tokens.pop_front();


            if (nextToken.id == HSD::ID_WORD)
            {
                previousTokens.push(nextToken);
                continue;
            }
            else if (nextToken.id == HSD::ID_ASSIGN_OP)
            {

            }
            else if (nextToken.id == HSD::ID_EOL)
            {
                continue;
            }
            else if (nextToken.id == HSD::ID_MODIFIER)
            {
                modifiles = nextToken.value;
                continue;
            }
            else
            {
                return false;
            }


            //should reach here by Token.id = ID_ASSIGN_OP;

            //space contained key
            if (previousTokens.size() != 1)
            {
                return false;
            }

            std::string key = previousTokens.top().value;
            previousTokens.pop();

            Node subnode;

            if (parseScalar(tokens, subnode))
            {
                node[key] = subnode;
            }
            else if (parseScalarList(tokens, subnode))
            {
                node[key] = subnode;
            }
            else if (parsePropertyList(tokens, subnode))
            {
                node[key] = subnode;
            }
            else if (parseMethod(tokens, subnode))
            {
                node[key] = subnode;
            }
            else
            {
                return false;
            }
        }
        if (!tokens.empty() || !previousTokens.empty())
        {
            return false;
        }
        return true;
    }


    void printNode(const Node& node, int level)
    {
        if (node.IsDefined())
        {
            qDebug() << "level: " << level;
            if (node.IsPropertyList())
            {
                if (!node.MethodName().empty())
                {
                    qDebug() << "Method: " << QString::fromStdString(node.MethodName());
                }
                else
                {
                    qDebug() << "PropertyList" ;
                }
                for(auto p=node.begin() ; p!= node.end(); ++p)
                {
                    qDebug() << QString::fromStdString((*p).first.as<std::string>()) ;
                    printNode((*p).second, level+1);
                }
            }
            else if (node.IsScalarList())
            {
                qDebug() << "[";
                for(int i=0; i<node.size();++i)
                {
                    qDebug() << QString::fromStdString(node[i].as<std::string>());
                }
                qDebug() << "]";
            }
            else if (node.IsScalar())
            {
                qDebug() << QString::fromStdString(node.as<std::string>());
            }
            else if (node.IsNull())
            {
                qDebug() << "{}";
            }
        }
    }

    HSD::Node parseString(std::string str)
    {
        Node node;

        std::deque<HSDToken> tokens;
        bool succ = tokenizeHSD(str, tokens);

        if (!succ)
        {
            return Node();
        }

        if (!parseHSD(tokens, node))
        {
            return Node();
        }
        else
        {
        }

        return node;
    }

}
